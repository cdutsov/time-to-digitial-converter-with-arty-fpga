# About the project
This is a Time-to-Digital converter with 25 ps timing resolution implemented on
the Digilent Arty board with Artix-7 FPGA. The board measures the time between
two signals and prints the result to a UART interface. The TDC was greatly
inspired by https://github.com/gonzagab/tdc

More info and pictures can be found here: https://physica.dev
