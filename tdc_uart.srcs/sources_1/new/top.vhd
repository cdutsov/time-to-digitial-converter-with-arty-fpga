----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/25/2019 09:13:51 AM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_arith.conv_std_logic_vector;
library WORK;
use WORK.all;

entity top is
    Port (
           BTN  : in STD_LOGIC_VECTOR(3 downto 0);
--           JA   : out STD_LOGIC_VECTOR(7 downto 0);
--           JD   : out STD_LOGIC_VECTOR(7 downto 0);
           LED  : out STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
           CLK  : in std_logic;
           UART_TXD : out STD_LOGIC;
           START: in std_logic;
           STOP : in std_logic
           );
           attribute io_buffer_type : string;
           attribute io_buffer_type of start : signal is "IBUF";
end top;

architecture Behavioral of top is

constant TRAIN_LENGTH : integer := 56;
constant DATA_LENGTH : integer := 56-16;

component edge_detector
Port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		BUTTON_IN : in std_logic;          
		PULSE_OUT : out std_logic
		);
end component;

component phase_detector_block
Port(
    fast_clk : in std_logic;
    slow_clk : in std_logic;
    pd_out   : out std_logic
);
end component;

component UART_TX_CTRL
Port (     SEND : in  STD_LOGIC;
           DATA : in std_logic_vector(7 downto 0);
           CLK : in  STD_LOGIC;
           READY : out  STD_LOGIC;
           UART_TX : out  STD_LOGIC);
end component;

component tdc is
    generic (
        output_length: integer := DATA_LENGTH;
        cnter_length : integer := 8;
        tdl_length   : integer := 4*26;
        bit_cnt_length: integer:= 10
        );
	port (
            --INPUTS--
		start	: in std_logic;
        stop    : in std_logic;
        clk     : in std_logic;
            --OUTPUTS--
        pulse_width : out std_logic_vector(output_length - 1 downto 0)
	);
end component;


component clk_wiz_0
port
(   
    clk_out1          : out    std_logic;
    reset             : in     std_logic;
    clk_in1           : in     std_logic
);
end component;


component addf
port
(
    d : in std_logic;
    q : out std_logic;
    clr :  in std_logic;
    clk : in std_logic;
    ce : in std_logic;
    pre : in std_logic
);
end component;


-- NCO signals
signal rst_clk_fast : std_logic := '0';
signal clk_fast : std_logic := '0';
signal rst_clk_slow : std_logic := '0';
signal clk_slow : std_logic := '0';
signal clk_f_pd : std_logic := '0';
signal clk_s_pd : std_logic := '0';
--signal start    : std_logic := '0';
--signal stop     : std_logic := '0';
signal dff_sout : std_logic := '0';
signal dff_fout : std_logic := '0';
signal clear_clocks : std_logic := '0';
signal set_clocks : std_logic := '1';
signal measuring : std_logic := '0';
signal measured : std_logic := '0';
signal first_arrived : std_logic := '0';
signal second_arrived : std_logic := '0';
signal timeout : integer := 0;
constant max_to : integer := 30;
signal meas_end : std_logic := '0';
signal stop_end : std_logic := '0';
signal start_end : std_logic := '0';
signal kill_start : std_logic := '0';
signal start_pulse : std_logic := '0';
signal start_pulse2 : std_logic := '0';

type tdc_state_type is (tdc_load, tdc_clear, tdc_idle);
signal tdc_state :tdc_state_type := tdc_idle;

type histogram_state is (hist_ready, hist_writing);
signal hist_state : histogram_state := hist_ready;

-- TDC signals
signal tdc_result : std_logic_vector(TRAIN_LENGTH-1 downto 0) := "10101011000000000000000000000000000000000000000010101011";
signal pulse_width : std_logic_vector(DATA_LENGTH-1 downto 0) := (others => '0');

signal btn_detect: std_logic;

constant CLK_period : time := 10ns;


-- UART stuff
signal uart_send : std_logic;
signal DATA_UART : std_logic_vector(7 downto 0) := (others => '0');
signal uart_ready: std_logic;
signal uart_tx   : std_logic;
signal start_uart : std_logic := '0';
type uart_state_type is (ready, load_data, prepare_send, send_data, busy);
signal uart_state : uart_state_type := ready;

-- Histogram stuff
constant ADDR_COUNTER_SIZE : integer := 48;
constant addr_bytes : integer := ADDR_COUNTER_SIZE / 8;
signal addr: integer := 0;
--type RAM_T is array (0 to RAM_SIZE - 1) of std_logic_vector(ADDR_COUNTER_SIZE downto 0);
--signal RAM : RAM_T:=(others => (others => '0'));
signal load_hist : std_logic := '0';
signal hist_loaded : std_logic := '0';

begin

----------------------------------------------------------
------              Button Control                 -------
----------------------------------------------------------
--Buttons are debounced and their rising edges are detected
--to trigger UART messages
Inst_btn_debounce: edge_detector 
    port map(
		CLOCK => CLK,
		RESET => BTN(1),
		BUTTON_IN => BTN(0),
		PULSE_OUT => btn_detect
	);				  
------------------------------------------------------------
----------          End Button Control          ------------
------------------------------------------------------------

------------------------------------------------------------
----------          Button Counter              ------------
------------------------------------------------------------

count_process : process (CLK)
begin
    if (rising_edge(CLK)) then
        if btn_detect = '1' then
            start_uart <= '1';
        else
            start_uart <= '0';
        end if;
    end if;
end process;

------------------------------------------------------------
----------          End Button Counter          ------------
------------------------------------------------------------

----------------------------------------------------------
------              Simple TDC                     -------
----------------------------------------------------------
inst_tdc: tdc
port map (
    start => start,
    stop => stop,
    clk => clk_fast,
    pulse_width => pulse_width
);

inst_clk: clk_wiz_0
port map(
    clk_out1 => clk_fast,
    clk_in1 => clk,
    reset => '0'
);
    
inst_dff1: addf
port map (
    d => '1',    
    ce => '1',
    pre => '0',
    clk => start_pulse,
    q => dff_sout,
    clr => meas_end
);

inst_dff2: addf
port map (
    d => '1',    
    ce => '1',
    pre => '0',
    clk => start_pulse,
    q => first_arrived,
    clr => kill_start
);

kill_start_counter: entity work.binary_counter
generic map ( n => 1 )
port map (
    clk => clk_fast,
    cnt_en => start_pulse,
    rst_bar => start_pulse,
            --Output--
    max_cnt => kill_start
);

stop_counter: entity work.binary_counter
generic map ( n => 4 )
port map (
    clk => clk_fast,
    cnt_en => second_arrived,
    rst_bar => second_arrived,
            --Output--
    max_cnt => stop_end
);

start_counter: entity work.binary_counter
generic map ( n => 8 )
port map (
    clk => clk_fast,
    cnt_en => dff_sout,
    rst_bar => dff_sout,
            --Output--
    max_cnt => start_end
);

start_pulse2 <= start;
start_pulse <= start_pulse2;
second_arrived <= stop and dff_sout;
meas_end <= stop_end or start_end;
--inst_dff2: addf
--port map (
--    d => '1',
--    ce => '1',
--    pre => '0',
--    clk => stop,
--    q => dff_fout,
--    clr => hist_loaded
--);

--measuring <= dff_sout and stop;



--clear_clocks <= not set_clocks;

--JA(0) <= first_arrived;
--JA(1) <= second_arrived;
--JA(2) <= dff_sout;
--JA(3) <= stop_end;
led <= pulse_width(3 downto 0);
--start <= JA(2);
--stop  <= JA(3);


--conv: process(pd_out)
--begin
--end process;

				  
------------------------------------------------------------
----------          End Simple TDC              ------------
------------------------------------------------------------

------------------------------------------------------------
----------         Histogram Logic              ------------
------------------------------------------------------------

--histogram_process: process (CLK)
--begin
--    if (rising_edge(CLK) ) then
--        case hist_state is
--        when hist_ready =>
--            hist_loaded <= '0';
--            if start = '1' then
--                hist_state <= hist_writing;
--            end if;
--        when hist_writing =>
--            hist_loaded <= '1';
--            hist_state <= hist_ready;
--    end case;
--    end if;
--end process;
------------------------------------------------------------
----------       End Histogram Logic            ------------
------------------------------------------------------------


------------------------------------------------------------
----------             Send Uart                ------------
------------------------------------------------------------

Inst_UART_TX_CTRL: UART_TX_CTRL port map(
		SEND => uart_send,
		DATA => DATA_UART,
		CLK => CLK,
		READY => uart_ready,
		UART_TX => uart_txd
	);
	
next_uartState_process : process (CLK)
variable temp_dat : std_logic_vector (7 downto 0) := (others => '0');
variable slice : integer range 0 to addr_bytes - 1 := 0;

begin
    if (rising_edge(CLK)) then
        case uart_state is
        when ready =>
            if stop_end = '1' then
                uart_state <= load_data;
                tdc_result(DATA_LENGTH + 8 - 1 downto 8) <= pulse_width(DATA_LENGTH-1 downto 0);
            end if;
         when load_data => 
            if uart_ready = '1' then
                uart_state <= prepare_send;
            end if;
         when prepare_send =>
--            data_uart <= RAM(temp_addr)((addr_counter_size - 8*slice) - 1 downto (addr_counter_size - 8*(slice+1)));
            data_uart <= tdc_result((addr_counter_size - 8*slice) - 1 downto (addr_counter_size - 8*(slice+1)));
            uart_state <= send_data;
         when send_data =>
            uart_send <= '1';
            uart_state <= busy;
         when busy =>
            uart_send <= '0';
            if slice = addr_bytes - 1 then 
                slice := 0;
                uart_state <= ready;
            else
                uart_state <= load_data;
                slice := slice + 1;
            end if;
        end case;
    end if;
end process;

------------------------------------------------------------
----------          End Send Uart               ------------
------------------------------------------------------------

end Behavioral;
