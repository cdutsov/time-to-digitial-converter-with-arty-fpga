library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
--Work Library--
library WORK;
use WORK.ALL;

entity binary_counter is
	generic(n : integer := 8);
	port(
            --INPUTS--
		clk     : in std_logic;
		cnt_en  : in std_logic;	--synchronous count enable
		rst_bar : in std_logic;	--synchronous reset (active low)
            --OUTPUTS--
		cnt     : out std_logic_vector(n - 1 downto 0); --holds count
		max_cnt : out std_logic --high when count has reached max count
	    );
end binary_counter;

architecture behavioral of binary_counter is
begin
    process(clk)
    variable count : unsigned (n - 1 downto 0);
    begin		
        if(rising_edge(clk))then
            if(rst_bar = '0')then
                max_cnt <= '0';
                count := to_unsigned(0,n);
            elsif(cnt_en = '1')then
                count := count + 1;
                max_cnt <= '0';
                if(count+1 = 0)then
                    max_cnt <= '1';
                end if;
            end if;
        end if;	
        cnt <= std_logic_vector(count);
    end process;
end behavioral;