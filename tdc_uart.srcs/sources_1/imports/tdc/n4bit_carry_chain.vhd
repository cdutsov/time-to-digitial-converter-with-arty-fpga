library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.math_real.all;
--Xilinx Primitives--
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
--Work Library--
library WORK;
use WORK.ALL;

entity n4bit_carry_chain is
    generic (n : integer := 4*80;
             xoff: integer := 8;
             yoff: integer := 0
            );	--n must be a multiple of 4
    port (
            --INPUT--
        clk	: in std_logic;	    --System clock
        c_in: in std_logic;	    --carry input
            --OUTPUT--
		carry_out : out std_logic_vector(n-1 downto 0)--carry out from the carry4 blocks
	);
	attribute io_buffer_type : string;
    attribute clock_buffer_type : string;
    attribute io_buffer_type of c_in : signal is "IBUF";
    attribute clock_buffer_type of c_in : signal is "NONE";
end n4bit_carry_chain;

architecture structural of n4bit_carry_chain is
--Signal Carriers--
signal data_chain : std_logic_vector(n-1 downto 0);
signal carry_chain: std_logic_vector(n-1 downto 0);

--Constants--
signal a:  std_logic_vector(n-1 downto 0) := (others => '0'); --dont care
signal sel:  std_logic_vector(n-1 downto 0) := (others => '1'); --has to be '1' always
--since sel is the select of the muxes and 1 will allow the carry to propogate

--Attributes--
ATTRIBUTE LOC : string;
attribute ASYNC_REG : string;
attribute ASYNC_REG of register_gen: label is "TRUE";
begin
	-- CARRY4: Fast Carry Logic Component
	-- Virtex-5
	-- bits arive in the order 3, 1, 0, 2
	first_carry: 
	for i in 0 to 0 generate
        ATTRIBUTE LOC OF CARRY4_inst : LABEL IS "SLICE_X"&INTEGER'image(xoff)&"Y"&INTEGER'image(yoff);
        begin
        CARRY4_inst : CARRY4
        port map (
            --INPUTS--
        CI => '0',              -- 1-bit carry cascade input; must come from another carry chain
        CYINIT => c_in,         -- 1-bit carry initialization
        DI  => a(3 downto 0),   -- 4-bit carry-MUX data in
        S 	=> sel(3 downto 0), -- 4-bit carry-MUX select input
            --OUTPUTS--
        CO  => carry_chain(3 downto 0),
        O	=> data_chain(3 downto 0)
        );
	-- End of CARRY4_inst instantiation
	end generate;
	
	tdl_gen:
	for i in 1 to n/4 -1 generate
	    ATTRIBUTE LOC OF carry_x : LABEL IS "SLICE_X"&INTEGER'image(xoff)&"Y"&INTEGER'image(yoff-i);
	    begin
		carry_x: 
		CARRY4 port map (
				--INPUTS--
			CI => carry_chain	((i*4)-1),
			CYINIT => '0',
			DI => a				(3+(i*4) downto 0+(i*4)),
			S 	=> sel			(3+(i*4) downto 0+(i*4)),
				--OUTPUTS--
			CO => carry_chain	(3+(i*4) downto 0+(i*4)),
			O 	=> data_chain 	(3+(i*4) downto 0+(i*4))
			);

	end generate;
	
	register_gen: 
	for i in 0 to (n/4 - 1) generate
	   ATTRIBUTE LOC OF dff : LABEL IS "SLICE_X"&INTEGER'image(Xoff)&"Y"&INTEGER'image(Yoff-i);
--	   ATTRIBUTE LOC OF dff : LABEL IS "SLICE_X"&INTEGER'image(Xoff)&"Y"&INTEGER'image(Yoff-integer(floor(real(i/4))));
	   begin
		dff: entity work.nbit_register 
		generic map (n => 4)
		port map (
			clk  => clk, 
			d 	 => carry_chain(3+(i*4) downto 0+(i*4)),
			q(0) => carry_out  (1+(i*4)),
            q(1) => carry_out  (2+(i*4)),
            q(2) => carry_out  (3+(i*4)),
            q(3) => carry_out  (0+(i*4))
			);	
	end generate register_gen;	
end structural;

