----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/25/2019 09:13:51 AM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_arith.conv_std_logic_vector;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port (
           BTN  : in STD_LOGIC_VECTOR(3 downto 0);
           JA   : in STD_LOGIC_VECTOR(7 downto 0);
           JD   : out STD_LOGIC_VECTOR(7 downto 0);
           LED  : out STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
           CLK  : in std_logic;
           UART_TXD : out STD_LOGIC);
--           START: in std_logic;
--           STOP : in std_logic;
end top;

architecture Behavioral of top is

component edge_detector
Port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		BUTTON_IN : in std_logic;          
		PULSE_OUT : out std_logic
		);
end component;

component phase_detector_block
Port(
    fast_clk : in std_logic;
    slow_clk : in std_logic;
    pd_out   : out std_logic
);
end component;

component UART_TX_CTRL
Port (     SEND : in  STD_LOGIC;
           DATA : in std_logic_vector(7 downto 0);
           CLK : in  STD_LOGIC;
           READY : out  STD_LOGIC;
           UART_TX : out  STD_LOGIC);
end component;

component ring_osc
Port (
    ring_ce : in std_logic;
    ring_out_fast : out std_logic
    );
end component;

--component fifo_buffer is
--  generic (
--    DATA_WIDTH : natural := 8;
--    FIFO_DEPTH : integer := 1024*8
--    );
--  port (
--    RST        : in std_logic;
--    CLK      : in std_logic;
 
--    -- FIFO Write Interface
--    WriteEn   : in  std_logic;
--    DataIn    : in  std_logic_vector(DATA_WIDTH-1 downto 0);
--    Full      : out std_logic;
 
--    -- FIFO Read Interface
--    ReadEn   : in  std_logic;
--    DataOut  : out std_logic_vector(DATA_WIDTH-1 downto 0);
--    Empty    : out std_logic
--    );
--end component;


component clk_wiz_1
port
(   
    clk_out1          : out    std_logic;
    reset             : in     std_logic;
    clk_in1           : in     std_logic;
    power_down        : in     std_logic
);
end component;

component clk_wiz_2
port
(   
    clk_out1          : out    std_logic;
    reset             : in     std_logic;
    clk_in1           : in     std_logic;
    power_down        : in     std_logic
);
end component;

component addf
port
(
    d : in std_logic;
    q : out std_logic;
    clr :  in std_logic;
    clk : in std_logic;
    ce : in std_logic;
    pre : in std_logic
);
end component;


-- NCO signals
signal rst_clk_fast : std_logic := '0';
signal clk_fast : std_logic := '0';
signal rst_clk_slow : std_logic := '0';
signal clk_slow : std_logic := '0';
signal clk_f_pd : std_logic := '0';
signal clk_s_pd : std_logic := '0';
signal start    : std_logic := '0';
signal stop     : std_logic := '0';
signal dff_sout : std_logic := '0';
signal dff_fout : std_logic := '0';
signal clear_clocks : std_logic := '0';
signal set_clocks : std_logic := '1';

type tdc_state_type is (tdc_load, tdc_clear, tdc_idle);
signal tdc_state :tdc_state_type := tdc_idle;

type histogram_state is (hist_ready, hist_writing);
signal hist_state : histogram_state := hist_ready;

-- TDC signals
signal zero_ns : std_logic := '0';
signal tdc_result : integer := 0;
constant t1 : integer := 100;
constant t2 : integer := 111;
signal pd_out : std_logic := '0';
signal tst : std_logic := '0';
signal clear : std_logic := '0';
signal n1 : integer := 0;
signal n2 : integer := 0;
signal n1_inc : integer := 0;
signal n2_inc : integer := 0;
signal start_fin : std_logic := '0';
signal stop_fin : std_logic := '0';
signal locked1 : std_logic := '0';
signal locked2 : std_logic := '0';


signal r_rst_sync  : std_logic := '0';
signal r_wr_data   : std_logic_vector (7 downto 0) := (others => '1');
signal r_rd_en     : std_logic := '0';
signal r_wr_en     : std_logic := '0';

signal w_rd_data  : std_logic_vector (7 downto 0) := (others => '1');
signal w_empty    : std_logic;
signal w_full       : std_logic;

--type fifo_state_type is (fifo_ready, load_data, write_enable, write_disable, fifo_o_full);
--signal fifo_state : fifo_state_type := fifo_ready;
--signal start_fifo : std_logic := '0';
--signal fifo_empty : std_logic := '0';


constant BTN_COUNTER_SIZE : integer := 64;
constant btn_bytes : integer := btn_counter_size / 8;
signal btn_count: std_logic_vector( (BTN_COUNTER_SIZE - 1) downto 0);
signal btn_de_bnc: std_logic_vector(3 downto 0);
signal btn_detect: std_logic;
signal btn_reg: std_logic_vector (3 downto 0);

constant CLK_period : time := 10ns;


-- UART stuff
signal uart_send : std_logic;
signal DATA_UART : std_logic_vector(7 downto 0) := (others => '0');
signal uart_ready: std_logic;
signal uart_tx   : std_logic;
signal start_uart : std_logic := '0';
type uart_state_type is (ready, load_data, prepare_send, send_data, busy);
signal uart_state : uart_state_type := ready;

-- Histogram stuff
constant RAM_SIZE: integer := 1024;
constant ADDR_COUNTER_SIZE : integer := 64;
constant addr_bytes : integer := ADDR_COUNTER_SIZE / 8;
signal addr: integer := 0;
type RAM_T is array (0 to RAM_SIZE - 1) of std_logic_vector(ADDR_COUNTER_SIZE downto 0);
signal RAM : RAM_T:=(others => (others => '0'));
signal load_hist : std_logic := '0';
signal hist_loaded : std_logic := '0';

begin

----------------------------------------------------------
------              Button Control                 -------
----------------------------------------------------------
--Buttons are debounced and their rising edges are detected
--to trigger UART messages
Inst_btn_debounce: edge_detector 
    port map(
		CLOCK => CLK,
		RESET => BTN(1),
		BUTTON_IN => BTN(0),
		PULSE_OUT => btn_detect
	);				  
------------------------------------------------------------
----------          End Button Control          ------------
------------------------------------------------------------

------------------------------------------------------------
----------          Button Counter              ------------
------------------------------------------------------------

count_process : process (CLK)
begin
    if (rising_edge(CLK)) then
        if btn_detect = '1' then
            start_uart <= '1';
        else
            start_uart <= '0';
        end if;
    end if;
end process;

------------------------------------------------------------
----------          End Button Counter          ------------
------------------------------------------------------------

----------------------------------------------------------
------              Simple TDC                     -------
----------------------------------------------------------
Inst_nco_f: clk_wiz_2
port map(
    clk_in1 => CLK,
    clk_out1 => clk_fast,
    reset => clk_f_pd,
    power_down => '0'
);

Inst_nco_s: clk_wiz_1
port map(
    clk_in1 => CLK,
    clk_out1 => clk_slow,
    reset => clk_s_pd,
    power_down => '0'
);

Inst_pd: phase_detector_block
port map (
    fast_clk => clk_fast,
    slow_clk => clk_slow,
    pd_out => pd_out
);

inst_dff1: addf
port map (
    d => '1',    
    ce => '1',
    pre => '0',
    clk => start,
    q => dff_sout,
--    clr => zero_ns
    clr => BTN(2)
);

inst_dff2: addf
port map (
    d => '1',
    ce => '1',
    pre => '0',
    clk => stop,
    q => dff_fout,
--    clr => zero_ns
    clr => BTN(2)

);

--inst_ring1: ring_osc
--port map (
--    ring_ce => '1',
--    ring_out_fast =>JD(3)
--);

--inst_dff_stop_clks: addf
--port map (
--    d => '1',
--    ce => '1',
--    pre => '0',
--    clk => pd_out,
--    q => set_clocks,
--    clr => '0'
--);

--clear_clocks <= not set_clocks;
clk_s_pd <= not dff_sout;
clk_f_pd <= not dff_fout;

JD(0) <= start;
JD(1) <= stop;
JD(2) <= clk_slow;
JD(3) <= clk_fast;
start <= JA(2);
stop  <= JA(3);

count_start: process(clk_fast)
begin
    if rising_edge(clk_fast) then
        n1_inc <= n1_inc + 1;
        if n1_inc = RAM_SIZE then 
--            start_fin <= '1';
        end if;
        if load_hist = '1' then
            n1_inc <= 0;
            start_fin <= '0';
        end if;
    end if;
 end process;

count_stop: process(clk_slow)
begin   
    if rising_edge(clk_slow) then
        n2_inc <= n2_inc + 1;
        if n2_inc = RAM_SIZE then 
--            stop_fin <= '1';
        end if;
        if load_hist = '1' then
            n2_inc <= 0;
            stop_fin <= '0';
        end if;
    end if;
 end process;

--pd: process(pd_out)
--begin
--    if (rising_edge(pd_out)) then
--        tdc_result <= (n1 - 1)*t1 - (n2 - 1)*t2;
--        load_hist <= '1';
--    end if;
--end process;

main: process(clk)
begin
    if (rising_edge(clk)) then
        if stop_fin = '1' or start_fin = '1' then
            zero_ns <= '1';
        elsif pd_out = '1' then
            tdc_result <= (n1_inc - 1)*t1 - (n2_inc - 1)*t2;
--            tdc_result <= n2_inc;
            load_hist <= '1';
        elsif hist_loaded = '1' then
            load_hist <= '0';
            zero_ns <= '1';
        else
            zero_ns <= '0';
        end if;
    end if;
end process;

--conv: process(pd_out)
--begin
--end process;

				  
------------------------------------------------------------
----------          End Simple TDC              ------------
------------------------------------------------------------

------------------------------------------------------------
----------         Histogram Logic              ------------
------------------------------------------------------------

histogram_process: process (CLK)
begin
    if (rising_edge(CLK) ) then
        case hist_state is
        when hist_ready =>
            hist_loaded <= '0';
            if load_hist = '1' then
                hist_state <= hist_writing;
            end if;
        when hist_writing =>
            hist_loaded <= '1';
            addr <= tdc_result;
            RAM(addr) <= std_logic_vector(unsigned(RAM(addr)) + 1);
            hist_state <= hist_ready;
    end case;
    end if;
end process;
------------------------------------------------------------
----------       End Histogram Logic            ------------
------------------------------------------------------------

------------------------------------------------------------
----------          Data Bufer                  ------------
------------------------------------------------------------

--fifo: fifo_buffer
--port map (
--    CLK => CLK,
--    RST => r_rst_sync,
--    DataIn => r_wr_data,
--    WriteEn => r_wr_en,
--    ReadEn => r_rd_en,
--    DataOut => w_rd_data,
--    Empty => w_empty,
--    Full => w_full
--);

--fifo_state_prc: process(clk)
--variable temp_dat : std_logic_vector (7 downto 0) := (others => '0');
--variable slice : integer range 0 to addr_bytes - 1 := 0;
--variable temp_addr: integer range 0 to 4095 := 0;
--begin
--    if (rising_edge(clk)) then
--        case fifo_state is
--        when fifo_ready =>
--            LED(3) <= '0';
--            if start_fifo = '1' then
--                fifo_state <= load_data;
--            end if;
--        when load_data =>
--            if w_full = '1' then
--                fifo_state <= fifo_o_full;
--            end if;
--            r_wr_data <= RAM(temp_addr)((addr_counter_size - 8*slice) - 1 downto (addr_counter_size - 8*(slice+1)));
--            fifo_state <= write_enable;
--        when write_enable =>
--            r_wr_en <= '1';
--            fifo_state <= write_disable;
--        when write_disable =>
--            r_wr_en <= '0';
--            if slice = addr_bytes - 1 then 
--                slice := 0;
--                temp_addr := temp_addr + 1;
--                fifo_state <= load_data;
--            else
--                fifo_state <= load_data;
--                slice := slice + 1;
--            end if;
--            if temp_addr = 1023 then
--                fifo_state <= fifo_ready;
--                temp_addr := 0;
--            end if;
--        when fifo_o_full =>
--            LED(3) <= '1';
--        end case;
--    end if;
--end process;

------------------------------------------------------------
----------          End Data Bufer              ------------
------------------------------------------------------------


------------------------------------------------------------
----------             Send Uart                ------------
------------------------------------------------------------

Inst_UART_TX_CTRL: UART_TX_CTRL port map(
		SEND => uart_send,
		DATA => DATA_UART,
		CLK => CLK,
		READY => uart_ready,
		UART_TX => uart_txd
	);
	
next_uartState_process : process (CLK)
variable temp_dat : std_logic_vector (7 downto 0) := (others => '0');
variable slice : integer range 0 to addr_bytes - 1 := 0;
variable temp_addr: integer range 0 to RAM_SIZE := 0;

begin
    if (rising_edge(CLK)) then
        case uart_state is
        when ready =>
            if start_uart = '1' then
                uart_state <= load_data;
            end if;
         when load_data => 
            if uart_ready = '1' then
                uart_state <= prepare_send;
            end if;
         when prepare_send =>
            data_uart <= RAM(temp_addr)((addr_counter_size - 8*slice) - 1 downto (addr_counter_size - 8*(slice+1)));
            uart_state <= send_data;
         when send_data =>
            uart_send <= '1';
            uart_state <= busy;
         when busy =>
            uart_send <= '0';
            if slice = addr_bytes - 1 then 
                slice := 0;
                temp_addr := temp_addr + 1;
                uart_state <= load_data;
            else
                uart_state <= load_data;
                slice := slice + 1;
            end if;
            if temp_addr = RAM_SIZE - 1 then
                temp_addr := 0;
                uart_state <= ready;
            end if;

        end case;
    end if;
end process;

------------------------------------------------------------
----------          End Send Uart               ------------
------------------------------------------------------------

LED(0) <= w_empty;

end Behavioral;
