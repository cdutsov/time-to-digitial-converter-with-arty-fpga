// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Thu Aug 29 18:36:10 2019
// Host        : rambo running 64-bit Arch Linux
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/chavdar/tdc_uart/tdc_uart.sim/sim_1/impl/timing/xsim/n4bit_carry_chain_tb_time_impl.v
// Design      : top
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module UART_TX_CTRL
   (UART_TXD_OBUF,
    \FSM_sequential_uart_state_reg[2] ,
    \FSM_sequential_txState_reg[0]_0 ,
    CLK,
    uart_state,
    \FSM_sequential_uart_state_reg[0] ,
    \FSM_sequential_uart_state_reg[0]_0 ,
    JA_OBUF,
    E,
    D);
  output UART_TXD_OBUF;
  output \FSM_sequential_uart_state_reg[2] ;
  output \FSM_sequential_txState_reg[0]_0 ;
  input CLK;
  input [2:0]uart_state;
  input \FSM_sequential_uart_state_reg[0] ;
  input \FSM_sequential_uart_state_reg[0]_0 ;
  input [0:0]JA_OBUF;
  input [0:0]E;
  input [5:0]D;

  wire CLK;
  wire [5:0]D;
  wire [0:0]E;
  wire \FSM_sequential_txState[0]_i_1_n_0 ;
  wire \FSM_sequential_txState[0]_i_2_n_0 ;
  wire \FSM_sequential_txState[0]_i_3_n_0 ;
  wire \FSM_sequential_txState[0]_i_4_n_0 ;
  wire \FSM_sequential_txState[0]_i_5_n_0 ;
  wire \FSM_sequential_txState[0]_i_6_n_0 ;
  wire \FSM_sequential_txState[0]_i_7_n_0 ;
  wire \FSM_sequential_txState[0]_i_8_n_0 ;
  wire \FSM_sequential_txState[0]_i_9_n_0 ;
  wire \FSM_sequential_txState[1]_i_1_n_0 ;
  wire \FSM_sequential_txState[1]_i_3_n_0 ;
  wire \FSM_sequential_txState[1]_i_4_n_0 ;
  wire \FSM_sequential_txState_reg[0]_0 ;
  wire \FSM_sequential_uart_state[0]_i_3_n_0 ;
  wire \FSM_sequential_uart_state_reg[0] ;
  wire \FSM_sequential_uart_state_reg[0]_0 ;
  wire \FSM_sequential_uart_state_reg[2] ;
  wire [0:0]JA_OBUF;
  wire READY;
  wire UART_TXD_OBUF;
  wire bitIndex;
  wire \bitIndex[0]_i_2_n_0 ;
  wire [30:0]bitIndex_reg;
  wire \bitIndex_reg[0]_i_1_n_0 ;
  wire \bitIndex_reg[0]_i_1_n_4 ;
  wire \bitIndex_reg[0]_i_1_n_5 ;
  wire \bitIndex_reg[0]_i_1_n_6 ;
  wire \bitIndex_reg[0]_i_1_n_7 ;
  wire \bitIndex_reg[12]_i_1_n_0 ;
  wire \bitIndex_reg[12]_i_1_n_4 ;
  wire \bitIndex_reg[12]_i_1_n_5 ;
  wire \bitIndex_reg[12]_i_1_n_6 ;
  wire \bitIndex_reg[12]_i_1_n_7 ;
  wire \bitIndex_reg[16]_i_1_n_0 ;
  wire \bitIndex_reg[16]_i_1_n_4 ;
  wire \bitIndex_reg[16]_i_1_n_5 ;
  wire \bitIndex_reg[16]_i_1_n_6 ;
  wire \bitIndex_reg[16]_i_1_n_7 ;
  wire \bitIndex_reg[20]_i_1_n_0 ;
  wire \bitIndex_reg[20]_i_1_n_4 ;
  wire \bitIndex_reg[20]_i_1_n_5 ;
  wire \bitIndex_reg[20]_i_1_n_6 ;
  wire \bitIndex_reg[20]_i_1_n_7 ;
  wire \bitIndex_reg[24]_i_1_n_0 ;
  wire \bitIndex_reg[24]_i_1_n_4 ;
  wire \bitIndex_reg[24]_i_1_n_5 ;
  wire \bitIndex_reg[24]_i_1_n_6 ;
  wire \bitIndex_reg[24]_i_1_n_7 ;
  wire \bitIndex_reg[28]_i_1_n_5 ;
  wire \bitIndex_reg[28]_i_1_n_6 ;
  wire \bitIndex_reg[28]_i_1_n_7 ;
  wire \bitIndex_reg[4]_i_1_n_0 ;
  wire \bitIndex_reg[4]_i_1_n_4 ;
  wire \bitIndex_reg[4]_i_1_n_5 ;
  wire \bitIndex_reg[4]_i_1_n_6 ;
  wire \bitIndex_reg[4]_i_1_n_7 ;
  wire \bitIndex_reg[8]_i_1_n_0 ;
  wire \bitIndex_reg[8]_i_1_n_4 ;
  wire \bitIndex_reg[8]_i_1_n_5 ;
  wire \bitIndex_reg[8]_i_1_n_6 ;
  wire \bitIndex_reg[8]_i_1_n_7 ;
  wire bitTmr;
  wire \bitTmr[0]_i_3_n_0 ;
  wire [13:0]bitTmr_reg;
  wire \bitTmr_reg[0]_i_2_n_0 ;
  wire \bitTmr_reg[0]_i_2_n_4 ;
  wire \bitTmr_reg[0]_i_2_n_5 ;
  wire \bitTmr_reg[0]_i_2_n_6 ;
  wire \bitTmr_reg[0]_i_2_n_7 ;
  wire \bitTmr_reg[12]_i_1_n_6 ;
  wire \bitTmr_reg[12]_i_1_n_7 ;
  wire \bitTmr_reg[4]_i_1_n_0 ;
  wire \bitTmr_reg[4]_i_1_n_4 ;
  wire \bitTmr_reg[4]_i_1_n_5 ;
  wire \bitTmr_reg[4]_i_1_n_6 ;
  wire \bitTmr_reg[4]_i_1_n_7 ;
  wire \bitTmr_reg[8]_i_1_n_0 ;
  wire \bitTmr_reg[8]_i_1_n_4 ;
  wire \bitTmr_reg[8]_i_1_n_5 ;
  wire \bitTmr_reg[8]_i_1_n_6 ;
  wire \bitTmr_reg[8]_i_1_n_7 ;
  wire eqOp__12;
  wire txBit_i_3_n_0;
  wire txBit_i_4_n_0;
  wire txBit_i_5_n_0;
  wire [6:1]txData;
  wire [1:0]txState;
  wire [2:0]uart_state;
  wire [2:0]\NLW_bitIndex_reg[0]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[12]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[16]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[20]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[24]_i_1_CO_UNCONNECTED ;
  wire [3:0]\NLW_bitIndex_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_bitIndex_reg[28]_i_1_O_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[4]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[8]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitTmr_reg[0]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_bitTmr_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_bitTmr_reg[12]_i_1_O_UNCONNECTED ;
  wire [2:0]\NLW_bitTmr_reg[4]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitTmr_reg[8]_i_1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hF000F0CA)) 
    \FSM_sequential_txState[0]_i_1 
       (.I0(E),
        .I1(eqOp__12),
        .I2(txState[1]),
        .I3(txState[0]),
        .I4(\FSM_sequential_txState[0]_i_2_n_0 ),
        .O(\FSM_sequential_txState[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \FSM_sequential_txState[0]_i_2 
       (.I0(\FSM_sequential_txState[0]_i_3_n_0 ),
        .I1(\FSM_sequential_txState[0]_i_4_n_0 ),
        .I2(bitIndex_reg[0]),
        .I3(bitIndex_reg[2]),
        .I4(bitIndex_reg[4]),
        .I5(\FSM_sequential_txState[0]_i_5_n_0 ),
        .O(\FSM_sequential_txState[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \FSM_sequential_txState[0]_i_3 
       (.I0(bitIndex_reg[13]),
        .I1(bitIndex_reg[14]),
        .I2(bitIndex_reg[15]),
        .I3(bitIndex_reg[28]),
        .I4(bitIndex_reg[30]),
        .I5(txState[1]),
        .O(\FSM_sequential_txState[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h00010000)) 
    \FSM_sequential_txState[0]_i_4 
       (.I0(bitIndex_reg[5]),
        .I1(bitIndex_reg[6]),
        .I2(bitIndex_reg[7]),
        .I3(bitIndex_reg[8]),
        .I4(\FSM_sequential_txState[0]_i_6_n_0 ),
        .O(\FSM_sequential_txState[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFEF)) 
    \FSM_sequential_txState[0]_i_5 
       (.I0(\FSM_sequential_txState[0]_i_7_n_0 ),
        .I1(\FSM_sequential_txState[0]_i_8_n_0 ),
        .I2(bitIndex_reg[1]),
        .I3(bitIndex_reg[29]),
        .I4(bitIndex_reg[17]),
        .I5(\FSM_sequential_txState[0]_i_9_n_0 ),
        .O(\FSM_sequential_txState[0]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_sequential_txState[0]_i_6 
       (.I0(bitIndex_reg[12]),
        .I1(bitIndex_reg[11]),
        .I2(bitIndex_reg[10]),
        .I3(bitIndex_reg[9]),
        .O(\FSM_sequential_txState[0]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_txState[0]_i_7 
       (.I0(bitIndex_reg[23]),
        .I1(bitIndex_reg[20]),
        .I2(bitIndex_reg[25]),
        .I3(bitIndex_reg[22]),
        .O(\FSM_sequential_txState[0]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_txState[0]_i_8 
       (.I0(bitIndex_reg[19]),
        .I1(bitIndex_reg[16]),
        .I2(bitIndex_reg[21]),
        .I3(bitIndex_reg[18]),
        .O(\FSM_sequential_txState[0]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \FSM_sequential_txState[0]_i_9 
       (.I0(bitIndex_reg[27]),
        .I1(bitIndex_reg[24]),
        .I2(bitIndex_reg[3]),
        .I3(bitIndex_reg[26]),
        .O(\FSM_sequential_txState[0]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \FSM_sequential_txState[1]_i_1 
       (.I0(eqOp__12),
        .I1(txState[1]),
        .I2(txState[0]),
        .O(\FSM_sequential_txState[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \FSM_sequential_txState[1]_i_2 
       (.I0(\FSM_sequential_txState[1]_i_3_n_0 ),
        .I1(bitTmr_reg[1]),
        .I2(bitTmr_reg[0]),
        .I3(bitTmr_reg[3]),
        .I4(bitTmr_reg[2]),
        .I5(\FSM_sequential_txState[1]_i_4_n_0 ),
        .O(eqOp__12));
  LUT4 #(
    .INIT(16'h0400)) 
    \FSM_sequential_txState[1]_i_3 
       (.I0(bitTmr_reg[7]),
        .I1(bitTmr_reg[6]),
        .I2(bitTmr_reg[4]),
        .I3(bitTmr_reg[5]),
        .O(\FSM_sequential_txState[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \FSM_sequential_txState[1]_i_4 
       (.I0(bitTmr_reg[8]),
        .I1(bitTmr_reg[9]),
        .I2(bitTmr_reg[10]),
        .I3(bitTmr_reg[11]),
        .I4(bitTmr_reg[13]),
        .I5(bitTmr_reg[12]),
        .O(\FSM_sequential_txState[1]_i_4_n_0 ));
  (* FSM_ENCODED_STATES = "send_bit:10,load_bit:01,rdy:00" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_txState_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_sequential_txState[0]_i_1_n_0 ),
        .Q(txState[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "send_bit:10,load_bit:01,rdy:00" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_txState_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_sequential_txState[1]_i_1_n_0 ),
        .Q(txState[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000FFFF75770000)) 
    \FSM_sequential_uart_state[0]_i_1 
       (.I0(uart_state[2]),
        .I1(uart_state[1]),
        .I2(\FSM_sequential_uart_state_reg[0] ),
        .I3(\FSM_sequential_uart_state_reg[0]_0 ),
        .I4(\FSM_sequential_uart_state[0]_i_3_n_0 ),
        .I5(uart_state[0]),
        .O(\FSM_sequential_uart_state_reg[2] ));
  LUT6 #(
    .INIT(64'h0000FFFF00FF03AA)) 
    \FSM_sequential_uart_state[0]_i_3 
       (.I0(JA_OBUF),
        .I1(txState[0]),
        .I2(txState[1]),
        .I3(uart_state[0]),
        .I4(uart_state[2]),
        .I5(uart_state[1]),
        .O(\FSM_sequential_uart_state[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFF0F0010)) 
    \FSM_sequential_uart_state[1]_i_1 
       (.I0(txState[0]),
        .I1(txState[1]),
        .I2(uart_state[0]),
        .I3(uart_state[2]),
        .I4(uart_state[1]),
        .O(\FSM_sequential_txState_reg[0]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \bitIndex[0]_i_2 
       (.I0(bitIndex_reg[0]),
        .O(\bitIndex[0]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[0] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[0]_i_1_n_7 ),
        .Q(bitIndex_reg[0]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\bitIndex_reg[0]_i_1_n_0 ,\NLW_bitIndex_reg[0]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\bitIndex_reg[0]_i_1_n_4 ,\bitIndex_reg[0]_i_1_n_5 ,\bitIndex_reg[0]_i_1_n_6 ,\bitIndex_reg[0]_i_1_n_7 }),
        .S({bitIndex_reg[3:1],\bitIndex[0]_i_2_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[10] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[8]_i_1_n_5 ),
        .Q(bitIndex_reg[10]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[11] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[8]_i_1_n_4 ),
        .Q(bitIndex_reg[11]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[12] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[12]_i_1_n_7 ),
        .Q(bitIndex_reg[12]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[12]_i_1 
       (.CI(\bitIndex_reg[8]_i_1_n_0 ),
        .CO({\bitIndex_reg[12]_i_1_n_0 ,\NLW_bitIndex_reg[12]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[12]_i_1_n_4 ,\bitIndex_reg[12]_i_1_n_5 ,\bitIndex_reg[12]_i_1_n_6 ,\bitIndex_reg[12]_i_1_n_7 }),
        .S(bitIndex_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[13] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[12]_i_1_n_6 ),
        .Q(bitIndex_reg[13]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[14] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[12]_i_1_n_5 ),
        .Q(bitIndex_reg[14]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[15] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[12]_i_1_n_4 ),
        .Q(bitIndex_reg[15]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[16] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[16]_i_1_n_7 ),
        .Q(bitIndex_reg[16]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[16]_i_1 
       (.CI(\bitIndex_reg[12]_i_1_n_0 ),
        .CO({\bitIndex_reg[16]_i_1_n_0 ,\NLW_bitIndex_reg[16]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[16]_i_1_n_4 ,\bitIndex_reg[16]_i_1_n_5 ,\bitIndex_reg[16]_i_1_n_6 ,\bitIndex_reg[16]_i_1_n_7 }),
        .S(bitIndex_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[17] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[16]_i_1_n_6 ),
        .Q(bitIndex_reg[17]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[18] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[16]_i_1_n_5 ),
        .Q(bitIndex_reg[18]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[19] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[16]_i_1_n_4 ),
        .Q(bitIndex_reg[19]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[1] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[0]_i_1_n_6 ),
        .Q(bitIndex_reg[1]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[20] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[20]_i_1_n_7 ),
        .Q(bitIndex_reg[20]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[20]_i_1 
       (.CI(\bitIndex_reg[16]_i_1_n_0 ),
        .CO({\bitIndex_reg[20]_i_1_n_0 ,\NLW_bitIndex_reg[20]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[20]_i_1_n_4 ,\bitIndex_reg[20]_i_1_n_5 ,\bitIndex_reg[20]_i_1_n_6 ,\bitIndex_reg[20]_i_1_n_7 }),
        .S(bitIndex_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[21] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[20]_i_1_n_6 ),
        .Q(bitIndex_reg[21]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[22] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[20]_i_1_n_5 ),
        .Q(bitIndex_reg[22]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[23] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[20]_i_1_n_4 ),
        .Q(bitIndex_reg[23]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[24] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[24]_i_1_n_7 ),
        .Q(bitIndex_reg[24]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[24]_i_1 
       (.CI(\bitIndex_reg[20]_i_1_n_0 ),
        .CO({\bitIndex_reg[24]_i_1_n_0 ,\NLW_bitIndex_reg[24]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[24]_i_1_n_4 ,\bitIndex_reg[24]_i_1_n_5 ,\bitIndex_reg[24]_i_1_n_6 ,\bitIndex_reg[24]_i_1_n_7 }),
        .S(bitIndex_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[25] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[24]_i_1_n_6 ),
        .Q(bitIndex_reg[25]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[26] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[24]_i_1_n_5 ),
        .Q(bitIndex_reg[26]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[27] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[24]_i_1_n_4 ),
        .Q(bitIndex_reg[27]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[28] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[28]_i_1_n_7 ),
        .Q(bitIndex_reg[28]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[28]_i_1 
       (.CI(\bitIndex_reg[24]_i_1_n_0 ),
        .CO(\NLW_bitIndex_reg[28]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_bitIndex_reg[28]_i_1_O_UNCONNECTED [3],\bitIndex_reg[28]_i_1_n_5 ,\bitIndex_reg[28]_i_1_n_6 ,\bitIndex_reg[28]_i_1_n_7 }),
        .S({1'b0,bitIndex_reg[30:28]}));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[29] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[28]_i_1_n_6 ),
        .Q(bitIndex_reg[29]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[2] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[0]_i_1_n_5 ),
        .Q(bitIndex_reg[2]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[30] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[28]_i_1_n_5 ),
        .Q(bitIndex_reg[30]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[3] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[0]_i_1_n_4 ),
        .Q(bitIndex_reg[3]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[4] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[4]_i_1_n_7 ),
        .Q(bitIndex_reg[4]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[4]_i_1 
       (.CI(\bitIndex_reg[0]_i_1_n_0 ),
        .CO({\bitIndex_reg[4]_i_1_n_0 ,\NLW_bitIndex_reg[4]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[4]_i_1_n_4 ,\bitIndex_reg[4]_i_1_n_5 ,\bitIndex_reg[4]_i_1_n_6 ,\bitIndex_reg[4]_i_1_n_7 }),
        .S(bitIndex_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[5] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[4]_i_1_n_6 ),
        .Q(bitIndex_reg[5]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[6] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[4]_i_1_n_5 ),
        .Q(bitIndex_reg[6]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[7] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[4]_i_1_n_4 ),
        .Q(bitIndex_reg[7]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[8] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[8]_i_1_n_7 ),
        .Q(bitIndex_reg[8]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[8]_i_1 
       (.CI(\bitIndex_reg[4]_i_1_n_0 ),
        .CO({\bitIndex_reg[8]_i_1_n_0 ,\NLW_bitIndex_reg[8]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[8]_i_1_n_4 ,\bitIndex_reg[8]_i_1_n_5 ,\bitIndex_reg[8]_i_1_n_6 ,\bitIndex_reg[8]_i_1_n_7 }),
        .S(bitIndex_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[9] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[8]_i_1_n_6 ),
        .Q(bitIndex_reg[9]),
        .R(READY));
  LUT3 #(
    .INIT(8'hAB)) 
    \bitTmr[0]_i_1 
       (.I0(eqOp__12),
        .I1(txState[1]),
        .I2(txState[0]),
        .O(bitTmr));
  LUT1 #(
    .INIT(2'h1)) 
    \bitTmr[0]_i_3 
       (.I0(bitTmr_reg[0]),
        .O(\bitTmr[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[0]_i_2_n_7 ),
        .Q(bitTmr_reg[0]),
        .R(bitTmr));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitTmr_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\bitTmr_reg[0]_i_2_n_0 ,\NLW_bitTmr_reg[0]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\bitTmr_reg[0]_i_2_n_4 ,\bitTmr_reg[0]_i_2_n_5 ,\bitTmr_reg[0]_i_2_n_6 ,\bitTmr_reg[0]_i_2_n_7 }),
        .S({bitTmr_reg[3:1],\bitTmr[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[8]_i_1_n_5 ),
        .Q(bitTmr_reg[10]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[8]_i_1_n_4 ),
        .Q(bitTmr_reg[11]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[12]_i_1_n_7 ),
        .Q(bitTmr_reg[12]),
        .R(bitTmr));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitTmr_reg[12]_i_1 
       (.CI(\bitTmr_reg[8]_i_1_n_0 ),
        .CO(\NLW_bitTmr_reg[12]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_bitTmr_reg[12]_i_1_O_UNCONNECTED [3:2],\bitTmr_reg[12]_i_1_n_6 ,\bitTmr_reg[12]_i_1_n_7 }),
        .S({1'b0,1'b0,bitTmr_reg[13:12]}));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[12]_i_1_n_6 ),
        .Q(bitTmr_reg[13]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[0]_i_2_n_6 ),
        .Q(bitTmr_reg[1]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[0]_i_2_n_5 ),
        .Q(bitTmr_reg[2]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[0]_i_2_n_4 ),
        .Q(bitTmr_reg[3]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[4]_i_1_n_7 ),
        .Q(bitTmr_reg[4]),
        .R(bitTmr));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitTmr_reg[4]_i_1 
       (.CI(\bitTmr_reg[0]_i_2_n_0 ),
        .CO({\bitTmr_reg[4]_i_1_n_0 ,\NLW_bitTmr_reg[4]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitTmr_reg[4]_i_1_n_4 ,\bitTmr_reg[4]_i_1_n_5 ,\bitTmr_reg[4]_i_1_n_6 ,\bitTmr_reg[4]_i_1_n_7 }),
        .S(bitTmr_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[4]_i_1_n_6 ),
        .Q(bitTmr_reg[5]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[4]_i_1_n_5 ),
        .Q(bitTmr_reg[6]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[4]_i_1_n_4 ),
        .Q(bitTmr_reg[7]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[8]_i_1_n_7 ),
        .Q(bitTmr_reg[8]),
        .R(bitTmr));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitTmr_reg[8]_i_1 
       (.CI(\bitTmr_reg[4]_i_1_n_0 ),
        .CO({\bitTmr_reg[8]_i_1_n_0 ,\NLW_bitTmr_reg[8]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitTmr_reg[8]_i_1_n_4 ,\bitTmr_reg[8]_i_1_n_5 ,\bitTmr_reg[8]_i_1_n_6 ,\bitTmr_reg[8]_i_1_n_7 }),
        .S(bitTmr_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[8]_i_1_n_6 ),
        .Q(bitTmr_reg[9]),
        .R(bitTmr));
  LUT2 #(
    .INIT(4'h1)) 
    txBit_i_1
       (.I0(txState[0]),
        .I1(txState[1]),
        .O(READY));
  LUT2 #(
    .INIT(4'h2)) 
    txBit_i_2
       (.I0(txState[0]),
        .I1(txState[1]),
        .O(bitIndex));
  LUT5 #(
    .INIT(32'hFFE400E4)) 
    txBit_i_3
       (.I0(bitIndex_reg[2]),
        .I1(txBit_i_4_n_0),
        .I2(txBit_i_5_n_0),
        .I3(bitIndex_reg[3]),
        .I4(bitIndex_reg[0]),
        .O(txBit_i_3_n_0));
  LUT5 #(
    .INIT(32'hFAC00AC0)) 
    txBit_i_4
       (.I0(txData[2]),
        .I1(txData[1]),
        .I2(bitIndex_reg[0]),
        .I3(bitIndex_reg[1]),
        .I4(txData[3]),
        .O(txBit_i_4_n_0));
  LUT5 #(
    .INIT(32'h0C0CFA0A)) 
    txBit_i_5
       (.I0(txData[4]),
        .I1(txData[6]),
        .I2(bitIndex_reg[0]),
        .I3(txData[5]),
        .I4(bitIndex_reg[1]),
        .O(txBit_i_5_n_0));
  FDSE #(
    .INIT(1'b1)) 
    txBit_reg
       (.C(CLK),
        .CE(bitIndex),
        .D(txBit_i_3_n_0),
        .Q(UART_TXD_OBUF),
        .S(READY));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(D[0]),
        .Q(txData[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(D[1]),
        .Q(txData[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(D[2]),
        .Q(txData[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(D[3]),
        .Q(txData[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(D[4]),
        .Q(txData[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(D[5]),
        .Q(txData[6]),
        .R(1'b0));
endmodule

module addf
   (JA_OBUF,
    p_0_in,
    SR,
    START_IBUF_BUFG,
    meas_end,
    \count_reg[2] );
  output [1:0]JA_OBUF;
  output p_0_in;
  output [0:0]SR;
  input START_IBUF_BUFG;
  input meas_end;
  input [0:0]\count_reg[2] ;

  wire [1:0]JA_OBUF;
  wire [0:0]SR;
  wire START_IBUF_BUFG;
  wire [0:0]\count_reg[2] ;
  wire meas_end;
  wire p_0_in;

  LUT2 #(
    .INIT(4'h8)) 
    \JA_OBUF[1]_inst_i_1 
       (.I0(\count_reg[2] ),
        .I1(JA_OBUF[1]),
        .O(JA_OBUF[0]));
  FDCE #(
    .INIT(1'b0)) 
    Q_reg
       (.C(START_IBUF_BUFG),
        .CE(1'b1),
        .CLR(meas_end),
        .D(1'b1),
        .Q(JA_OBUF[1]));
  LUT2 #(
    .INIT(4'h7)) 
    \count[2]_i_1 
       (.I0(JA_OBUF[1]),
        .I1(\count_reg[2] ),
        .O(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    max_cnt_i_1
       (.I0(JA_OBUF[1]),
        .O(SR));
endmodule

module binary_counter
   (JA_OBUF,
    meas_end,
    E,
    p_0_in,
    clk_out1,
    start_end,
    uart_state);
  output [0:0]JA_OBUF;
  output meas_end;
  output [0:0]E;
  input p_0_in;
  input clk_out1;
  input start_end;
  input [2:0]uart_state;

  wire [0:0]E;
  wire [0:0]JA_OBUF;
  wire clk_out1;
  wire \count[0]_i_1_n_0 ;
  wire \count[1]_i_1_n_0 ;
  wire \count[2]_i_2_n_0 ;
  wire \count_reg_n_0_[0] ;
  wire \count_reg_n_0_[1] ;
  wire \count_reg_n_0_[2] ;
  wire max_cnt_i_1__0_n_0;
  wire meas_end;
  wire p_0_in;
  wire start_end;
  wire [2:0]uart_state;

  LUT2 #(
    .INIT(4'hE)) 
    Q_i_1
       (.I0(JA_OBUF),
        .I1(start_end),
        .O(meas_end));
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1 
       (.I0(\count_reg_n_0_[0] ),
        .O(\count[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \count[1]_i_1 
       (.I0(\count_reg_n_0_[0] ),
        .I1(\count_reg_n_0_[1] ),
        .O(\count[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h78)) 
    \count[2]_i_2 
       (.I0(\count_reg_n_0_[0] ),
        .I1(\count_reg_n_0_[1] ),
        .I2(\count_reg_n_0_[2] ),
        .O(\count[2]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\count[0]_i_1_n_0 ),
        .Q(\count_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\count[1]_i_1_n_0 ),
        .Q(\count_reg_n_0_[1] ),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(clk_out1),
        .CE(1'b1),
        .D(\count[2]_i_2_n_0 ),
        .Q(\count_reg_n_0_[2] ),
        .R(p_0_in));
  LUT3 #(
    .INIT(8'h20)) 
    max_cnt_i_1__0
       (.I0(\count_reg_n_0_[2] ),
        .I1(\count_reg_n_0_[0] ),
        .I2(\count_reg_n_0_[1] ),
        .O(max_cnt_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    max_cnt_reg
       (.C(clk_out1),
        .CE(1'b1),
        .D(max_cnt_i_1__0_n_0),
        .Q(JA_OBUF),
        .R(p_0_in));
  LUT4 #(
    .INIT(16'h0100)) 
    \tdc_result[13]_i_1 
       (.I0(uart_state[1]),
        .I1(uart_state[0]),
        .I2(uart_state[2]),
        .I3(JA_OBUF),
        .O(E));
endmodule

(* ORIG_REF_NAME = "binary_counter" *) 
module binary_counter_9
   (max_cnt_reg_0,
    p_0_in,
    CLK);
  output max_cnt_reg_0;
  input p_0_in;
  input CLK;

  wire CLK;
  wire \count[0]_i_1_n_0 ;
  wire \count[1]_i_1_n_0 ;
  wire \count[2]_i_1_n_0 ;
  wire \count_reg_n_0_[0] ;
  wire \count_reg_n_0_[1] ;
  wire \count_reg_n_0_[2] ;
  wire max_cnt_i_1__1_n_0;
  wire max_cnt_reg_0;
  wire p_0_in;

  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1 
       (.I0(\count_reg_n_0_[0] ),
        .O(\count[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \count[1]_i_1 
       (.I0(\count_reg_n_0_[0] ),
        .I1(\count_reg_n_0_[1] ),
        .O(\count[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h78)) 
    \count[2]_i_1 
       (.I0(\count_reg_n_0_[0] ),
        .I1(\count_reg_n_0_[1] ),
        .I2(\count_reg_n_0_[2] ),
        .O(\count[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\count[0]_i_1_n_0 ),
        .Q(\count_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\count[1]_i_1_n_0 ),
        .Q(\count_reg_n_0_[1] ),
        .R(p_0_in));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\count[2]_i_1_n_0 ),
        .Q(\count_reg_n_0_[2] ),
        .R(p_0_in));
  LUT3 #(
    .INIT(8'h20)) 
    max_cnt_i_1__1
       (.I0(\count_reg_n_0_[2] ),
        .I1(\count_reg_n_0_[0] ),
        .I2(\count_reg_n_0_[1] ),
        .O(max_cnt_i_1__1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    max_cnt_reg
       (.C(CLK),
        .CE(1'b1),
        .D(max_cnt_i_1__1_n_0),
        .Q(max_cnt_reg_0),
        .R(p_0_in));
endmodule

(* ORIG_REF_NAME = "binary_counter" *) 
module binary_counter__parameterized0
   (start_end,
    SR,
    E,
    CLK);
  output start_end;
  input [0:0]SR;
  input [0:0]E;
  input CLK;

  wire CLK;
  wire [0:0]E;
  wire [0:0]SR;
  wire \count[6]_i_2_n_0 ;
  wire [6:0]count_reg;
  wire max_cnt_i_2_n_0;
  wire max_cnt_i_3_n_0;
  wire [6:0]plusOp;
  wire start_end;

  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1 
       (.I0(count_reg[0]),
        .O(plusOp[0]));
  LUT2 #(
    .INIT(4'h6)) 
    \count[1]_i_1 
       (.I0(count_reg[0]),
        .I1(count_reg[1]),
        .O(plusOp[1]));
  LUT3 #(
    .INIT(8'h78)) 
    \count[2]_i_1__0 
       (.I0(count_reg[0]),
        .I1(count_reg[1]),
        .I2(count_reg[2]),
        .O(plusOp[2]));
  LUT4 #(
    .INIT(16'h7F80)) 
    \count[3]_i_1 
       (.I0(count_reg[1]),
        .I1(count_reg[0]),
        .I2(count_reg[2]),
        .I3(count_reg[3]),
        .O(plusOp[3]));
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \count[4]_i_1 
       (.I0(count_reg[2]),
        .I1(count_reg[0]),
        .I2(count_reg[1]),
        .I3(count_reg[3]),
        .I4(count_reg[4]),
        .O(plusOp[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \count[5]_i_1 
       (.I0(count_reg[3]),
        .I1(count_reg[1]),
        .I2(count_reg[0]),
        .I3(count_reg[2]),
        .I4(count_reg[4]),
        .I5(count_reg[5]),
        .O(plusOp[5]));
  LUT3 #(
    .INIT(8'h78)) 
    \count[6]_i_1 
       (.I0(\count[6]_i_2_n_0 ),
        .I1(count_reg[5]),
        .I2(count_reg[6]),
        .O(plusOp[6]));
  LUT5 #(
    .INIT(32'h80000000)) 
    \count[6]_i_2 
       (.I0(count_reg[4]),
        .I1(count_reg[2]),
        .I2(count_reg[0]),
        .I3(count_reg[1]),
        .I4(count_reg[3]),
        .O(\count[6]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(plusOp[0]),
        .Q(count_reg[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(plusOp[1]),
        .Q(count_reg[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(plusOp[2]),
        .Q(count_reg[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(plusOp[3]),
        .Q(count_reg[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(plusOp[4]),
        .Q(count_reg[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(plusOp[5]),
        .Q(count_reg[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(plusOp[6]),
        .Q(count_reg[6]),
        .R(SR));
  LUT5 #(
    .INIT(32'h00008000)) 
    max_cnt_i_2
       (.I0(max_cnt_i_3_n_0),
        .I1(count_reg[3]),
        .I2(count_reg[2]),
        .I3(count_reg[1]),
        .I4(count_reg[0]),
        .O(max_cnt_i_2_n_0));
  LUT6 #(
    .INIT(64'h8001010101010101)) 
    max_cnt_i_3
       (.I0(count_reg[6]),
        .I1(count_reg[5]),
        .I2(count_reg[4]),
        .I3(count_reg[2]),
        .I4(count_reg[1]),
        .I5(count_reg[3]),
        .O(max_cnt_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    max_cnt_reg
       (.C(CLK),
        .CE(E),
        .D(max_cnt_i_2_n_0),
        .Q(start_end),
        .R(SR));
endmodule

module calc_unit
   (pst_trig,
    Q,
    JA_OBUF,
    CLK,
    p_0_in,
    D,
    \q_reg[0] ,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3);
  output pst_trig;
  output [5:0]Q;
  input [0:0]JA_OBUF;
  input CLK;
  input p_0_in;
  input [39:0]D;
  input \q_reg[0] ;
  output lopt;
  output lopt_1;
  output lopt_2;
  output lopt_3;

  wire CLK;
  wire [39:0]D;
  wire [0:0]JA_OBUF;
  wire [5:0]Q;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire out_reg_trig_n_0;
  wire p_0_in;
  wire pst_reg_n_0;
  wire pst_reg_n_1;
  wire pst_reg_n_2;
  wire pst_reg_n_3;
  wire pst_reg_n_4;
  wire pst_reg_n_5;
  wire pst_trig;
  wire \q_reg[0] ;

  binary_counter_9 out_reg_trig
       (.CLK(CLK),
        .max_cnt_reg_0(out_reg_trig_n_0),
        .p_0_in(p_0_in));
  nbit_register__parameterized1 output_reg
       (.D({pst_reg_n_0,pst_reg_n_1,pst_reg_n_2,pst_reg_n_3,pst_reg_n_4,pst_reg_n_5}),
        .Q(Q),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .\q_reg[5]_0 (out_reg_trig_n_0));
  nbit_register__parameterized0 pst_reg
       (.D({pst_reg_n_0,pst_reg_n_1,pst_reg_n_2,pst_reg_n_3,pst_reg_n_4,pst_reg_n_5}),
        .\q_reg[0]_0 (\q_reg[0] ),
        .\q_reg[39]_0 (D));
  FDRE #(
    .INIT(1'b0)) 
    pst_trig_reg
       (.C(CLK),
        .CE(1'b1),
        .D(JA_OBUF),
        .Q(pst_trig),
        .R(1'b0));
endmodule

module clk_wiz_0
   (clk_out1,
    reset,
    clk_in1);
  output clk_out1;
  input reset;
  input clk_in1;

  (* IBUF_LOW_PWR *) wire clk_in1;
  wire clk_out1;
  wire reset;

  clk_wiz_0__clk_wiz_0_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .reset(reset));
endmodule

(* ORIG_REF_NAME = "clk_wiz_0_clk_wiz" *) 
module clk_wiz_0__clk_wiz_0_clk_wiz
   (clk_out1,
    reset,
    clk_in1);
  output clk_out1;
  input reset;
  input clk_in1;

  wire clk_in1;
  wire clk_out1;
  wire clk_out1_clk_wiz_0;
  wire clk_out1_clk_wiz_0_en_clk;
  wire clkfbout_buf_clk_wiz_0;
  wire clkfbout_clk_wiz_0;
  wire locked_int;
  wire reset;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) wire [7:0]seq_reg1;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_0),
        .O(clkfbout_buf_clk_wiz_0));
  (* OPT_MODIFIED = "BUFG_OPT" *) 
  (* XILINX_LEGACY_PRIM = "BUFGCE" *) 
  (* XILINX_TRANSFORM_PINMAP = "CE:CE0 I:I0" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFGCTRL #(
    .CE_TYPE_CE0("SYNC"),
    .CE_TYPE_CE1("SYNC"),
    .INIT_OUT(0),
    .PRESELECT_I0("TRUE"),
    .PRESELECT_I1("FALSE"),
    .SIM_DEVICE("7SERIES"),
    .STARTUP_SYNC("FALSE")) 
    clkout1_buf
       (.CE0(seq_reg1[7]),
        .CE1(1'b0),
        .I0(clk_out1_clk_wiz_0),
        .I1(1'b1),
        .IGNORE0(1'b0),
        .IGNORE1(1'b1),
        .O(clk_out1),
        .S0(1'b1),
        .S1(1'b0));
  (* OPT_MODIFIED = "BUFG_OPT" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFH clkout1_buf_en
       (.I(clk_out1_clk_wiz_0),
        .O(clk_out1_clk_wiz_0_en_clk));
  (* OPT_MODIFIED = "MLO BUFG_OPT" *) 
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(2.500000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(1),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("BUF_IN"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_0),
        .CLKFBOUT(clkfbout_clk_wiz_0),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(clk_in1),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(NLW_mmcm_adv_inst_CLKOUT1_UNCONNECTED),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(locked_int),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(reset));
  initial assign \seq_reg1_reg[0] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[0] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(locked_int),
        .Q(seq_reg1[0]));
  initial assign \seq_reg1_reg[1] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[1] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[0]),
        .Q(seq_reg1[1]));
  initial assign \seq_reg1_reg[2] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[2] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[1]),
        .Q(seq_reg1[2]));
  initial assign \seq_reg1_reg[3] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[3] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[2]),
        .Q(seq_reg1[3]));
  initial assign \seq_reg1_reg[4] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[4] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[3]),
        .Q(seq_reg1[4]));
  initial assign \seq_reg1_reg[5] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[5] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[4]),
        .Q(seq_reg1[5]));
  initial assign \seq_reg1_reg[6] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[6] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[5]),
        .Q(seq_reg1[6]));
  initial assign \seq_reg1_reg[7] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[7] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[6]),
        .Q(seq_reg1[7]));
endmodule

module n4bit_carry_chain
   (D,
    JA_OBUF,
    CLK);
  output [39:0]D;
  input [0:0]JA_OBUF;
  input CLK;

  wire CLK;
  wire [39:0]D;
  wire [0:0]JA_OBUF;
  wire [3:0]carry_chain_0;
  wire [3:0]carry_chain_12;
  wire [3:0]carry_chain_16;
  wire [3:0]carry_chain_20;
  wire [3:0]carry_chain_24;
  wire [3:0]carry_chain_28;
  wire [3:0]carry_chain_32;
  wire [3:0]carry_chain_36;
  wire [3:0]carry_chain_4;
  wire [3:0]carry_chain_8;
  wire [3:0]NLW_CARRY4_inst_O_UNCONNECTED;
  wire [3:0]\NLW_tdl_gen[1].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[2].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[3].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[4].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[5].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[6].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[7].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[8].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[9].carry_x_O_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  CARRY4 CARRY4_inst
       (.CI(1'b0),
        .CO(carry_chain_0),
        .CYINIT(JA_OBUF),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_CARRY4_inst_O_UNCONNECTED[3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  nbit_register \register_gen[0].dff 
       (.CLK(CLK),
        .D(D[3:0]),
        .\q_reg[3]_0 (carry_chain_0));
  nbit_register_0 \register_gen[1].dff 
       (.CLK(CLK),
        .D(D[7:4]),
        .\q_reg[3]_0 (carry_chain_4));
  nbit_register_1 \register_gen[2].dff 
       (.CLK(CLK),
        .D(D[11:8]),
        .\q_reg[3]_0 (carry_chain_8));
  nbit_register_2 \register_gen[3].dff 
       (.CLK(CLK),
        .D(D[15:12]),
        .\q_reg[3]_0 (carry_chain_12));
  nbit_register_3 \register_gen[4].dff 
       (.CLK(CLK),
        .D(D[19:16]),
        .\q_reg[3]_0 (carry_chain_16));
  nbit_register_4 \register_gen[5].dff 
       (.CLK(CLK),
        .D(D[23:20]),
        .\q_reg[3]_0 (carry_chain_20));
  nbit_register_5 \register_gen[6].dff 
       (.CLK(CLK),
        .D(D[27:24]),
        .\q_reg[3]_0 (carry_chain_24));
  nbit_register_6 \register_gen[7].dff 
       (.CLK(CLK),
        .D(D[31:28]),
        .\q_reg[3]_0 (carry_chain_28));
  nbit_register_7 \register_gen[8].dff 
       (.CLK(CLK),
        .D(D[35:32]),
        .\q_reg[3]_0 (carry_chain_32));
  nbit_register_8 \register_gen[9].dff 
       (.CLK(CLK),
        .D(D[39:36]),
        .\q_reg[3]_0 (carry_chain_36));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[1].carry_x 
       (.CI(carry_chain_0[3]),
        .CO(carry_chain_4),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[1].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[2].carry_x 
       (.CI(carry_chain_4[3]),
        .CO(carry_chain_8),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[2].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[3].carry_x 
       (.CI(carry_chain_8[3]),
        .CO(carry_chain_12),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[3].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[4].carry_x 
       (.CI(carry_chain_12[3]),
        .CO(carry_chain_16),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[4].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[5].carry_x 
       (.CI(carry_chain_16[3]),
        .CO(carry_chain_20),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[5].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[6].carry_x 
       (.CI(carry_chain_20[3]),
        .CO(carry_chain_24),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[6].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[7].carry_x 
       (.CI(carry_chain_24[3]),
        .CO(carry_chain_28),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[7].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[8].carry_x 
       (.CI(carry_chain_28[3]),
        .CO(carry_chain_32),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[8].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[9].carry_x 
       (.CI(carry_chain_32[3]),
        .CO(carry_chain_36),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[9].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
endmodule

module nbit_register
   (D,
    \q_reg[3]_0 ,
    CLK);
  output [3:0]D;
  input [3:0]\q_reg[3]_0 ;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[3]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [0]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [2]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [3]),
        .Q(D[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_0
   (D,
    \q_reg[3]_0 ,
    CLK);
  output [3:0]D;
  input [3:0]\q_reg[3]_0 ;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[3]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [0]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [2]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [3]),
        .Q(D[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_1
   (D,
    \q_reg[3]_0 ,
    CLK);
  output [3:0]D;
  input [3:0]\q_reg[3]_0 ;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[3]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [0]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [2]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [3]),
        .Q(D[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_2
   (D,
    \q_reg[3]_0 ,
    CLK);
  output [3:0]D;
  input [3:0]\q_reg[3]_0 ;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[3]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [0]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [2]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [3]),
        .Q(D[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_3
   (D,
    \q_reg[3]_0 ,
    CLK);
  output [3:0]D;
  input [3:0]\q_reg[3]_0 ;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[3]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [0]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [2]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [3]),
        .Q(D[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_4
   (D,
    \q_reg[3]_0 ,
    CLK);
  output [3:0]D;
  input [3:0]\q_reg[3]_0 ;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[3]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [0]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [2]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [3]),
        .Q(D[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_5
   (D,
    \q_reg[3]_0 ,
    CLK);
  output [3:0]D;
  input [3:0]\q_reg[3]_0 ;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[3]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [0]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [2]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [3]),
        .Q(D[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_6
   (D,
    \q_reg[3]_0 ,
    CLK);
  output [3:0]D;
  input [3:0]\q_reg[3]_0 ;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[3]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [0]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [2]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [3]),
        .Q(D[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_7
   (D,
    \q_reg[3]_0 ,
    CLK);
  output [3:0]D;
  input [3:0]\q_reg[3]_0 ;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[3]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [0]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [2]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [3]),
        .Q(D[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_8
   (D,
    \q_reg[3]_0 ,
    CLK);
  output [3:0]D;
  input [3:0]\q_reg[3]_0 ;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[3]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [0]),
        .Q(D[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [1]),
        .Q(D[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [2]),
        .Q(D[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\q_reg[3]_0 [3]),
        .Q(D[2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register__parameterized0
   (D,
    \q_reg[39]_0 ,
    \q_reg[0]_0 );
  output [5:0]D;
  input [39:0]\q_reg[39]_0 ;
  input \q_reg[0]_0 ;

  wire [5:0]D;
  wire [39:0]pst_reg_cnt;
  wire \q[0]_i_10_n_0 ;
  wire \q[0]_i_11_n_0 ;
  wire \q[0]_i_2_n_0 ;
  wire \q[0]_i_3_n_0 ;
  wire \q[0]_i_4_n_0 ;
  wire \q[0]_i_5_n_0 ;
  wire \q[0]_i_6_n_0 ;
  wire \q[0]_i_7_n_0 ;
  wire \q[0]_i_8_n_0 ;
  wire \q[0]_i_9_n_0 ;
  wire \q[1]_i_10_n_0 ;
  wire \q[1]_i_11_n_0 ;
  wire \q[1]_i_12_n_0 ;
  wire \q[1]_i_2_n_0 ;
  wire \q[1]_i_3_n_0 ;
  wire \q[1]_i_4_n_0 ;
  wire \q[1]_i_5_n_0 ;
  wire \q[1]_i_6_n_0 ;
  wire \q[1]_i_7_n_0 ;
  wire \q[1]_i_8_n_0 ;
  wire \q[1]_i_9_n_0 ;
  wire \q[2]_i_2_n_0 ;
  wire \q[2]_i_3_n_0 ;
  wire \q[2]_i_4_n_0 ;
  wire \q[2]_i_5_n_0 ;
  wire \q[2]_i_6_n_0 ;
  wire \q[2]_i_7_n_0 ;
  wire \q[2]_i_8_n_0 ;
  wire \q[2]_i_9_n_0 ;
  wire \q[3]_i_2_n_0 ;
  wire \q[3]_i_3_n_0 ;
  wire \q[3]_i_4_n_0 ;
  wire \q[3]_i_5_n_0 ;
  wire \q[3]_i_6_n_0 ;
  wire \q[3]_i_7_n_0 ;
  wire \q[3]_i_8_n_0 ;
  wire \q[5]_i_2_n_0 ;
  wire \q[5]_i_3_n_0 ;
  wire \q[5]_i_4_n_0 ;
  wire \q[5]_i_5_n_0 ;
  wire \q[5]_i_6_n_0 ;
  wire \q[5]_i_7_n_0 ;
  wire \q[5]_i_8_n_0 ;
  wire \q_reg[0]_0 ;
  wire [39:0]\q_reg[39]_0 ;
  wire \q_reg_n_0_[10] ;
  wire \q_reg_n_0_[11] ;
  wire \q_reg_n_0_[12] ;
  wire \q_reg_n_0_[13] ;
  wire \q_reg_n_0_[14] ;
  wire \q_reg_n_0_[15] ;
  wire \q_reg_n_0_[16] ;
  wire \q_reg_n_0_[17] ;
  wire \q_reg_n_0_[18] ;
  wire \q_reg_n_0_[19] ;
  wire \q_reg_n_0_[1] ;
  wire \q_reg_n_0_[20] ;
  wire \q_reg_n_0_[21] ;
  wire \q_reg_n_0_[22] ;
  wire \q_reg_n_0_[23] ;
  wire \q_reg_n_0_[24] ;
  wire \q_reg_n_0_[25] ;
  wire \q_reg_n_0_[26] ;
  wire \q_reg_n_0_[27] ;
  wire \q_reg_n_0_[28] ;
  wire \q_reg_n_0_[29] ;
  wire \q_reg_n_0_[2] ;
  wire \q_reg_n_0_[30] ;
  wire \q_reg_n_0_[31] ;
  wire \q_reg_n_0_[32] ;
  wire \q_reg_n_0_[33] ;
  wire \q_reg_n_0_[34] ;
  wire \q_reg_n_0_[35] ;
  wire \q_reg_n_0_[36] ;
  wire \q_reg_n_0_[37] ;
  wire \q_reg_n_0_[3] ;
  wire \q_reg_n_0_[4] ;
  wire \q_reg_n_0_[5] ;
  wire \q_reg_n_0_[6] ;
  wire \q_reg_n_0_[7] ;
  wire \q_reg_n_0_[8] ;
  wire \q_reg_n_0_[9] ;

  LUT6 #(
    .INIT(64'hEEEEEEEEFEFEFFFE)) 
    \q[0]_i_1 
       (.I0(\q[0]_i_2_n_0 ),
        .I1(\q[0]_i_3_n_0 ),
        .I2(pst_reg_cnt[38]),
        .I3(\q_reg_n_0_[36] ),
        .I4(\q_reg_n_0_[37] ),
        .I5(pst_reg_cnt[39]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFF55551110)) 
    \q[0]_i_10 
       (.I0(\q_reg_n_0_[9] ),
        .I1(\q_reg_n_0_[7] ),
        .I2(\q[0]_i_11_n_0 ),
        .I3(\q_reg_n_0_[6] ),
        .I4(\q_reg_n_0_[8] ),
        .I5(\q_reg_n_0_[10] ),
        .O(\q[0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AAAAEEFE)) 
    \q[0]_i_11 
       (.I0(\q_reg_n_0_[4] ),
        .I1(\q_reg_n_0_[2] ),
        .I2(pst_reg_cnt[0]),
        .I3(\q_reg_n_0_[1] ),
        .I4(\q_reg_n_0_[3] ),
        .I5(\q_reg_n_0_[5] ),
        .O(\q[0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF000088A8)) 
    \q[0]_i_2 
       (.I0(\q[0]_i_4_n_0 ),
        .I1(\q_reg_n_0_[28] ),
        .I2(\q_reg_n_0_[26] ),
        .I3(\q_reg_n_0_[27] ),
        .I4(\q_reg_n_0_[33] ),
        .I5(\q[0]_i_5_n_0 ),
        .O(\q[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAA20AA20AA22AA20)) 
    \q[0]_i_3 
       (.I0(\q[0]_i_6_n_0 ),
        .I1(\q_reg_n_0_[23] ),
        .I2(\q_reg_n_0_[22] ),
        .I3(\q_reg_n_0_[24] ),
        .I4(\q[0]_i_7_n_0 ),
        .I5(\q_reg_n_0_[21] ),
        .O(\q[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    \q[0]_i_4 
       (.I0(\q_reg_n_0_[31] ),
        .I1(pst_reg_cnt[39]),
        .I2(\q_reg_n_0_[37] ),
        .I3(\q_reg_n_0_[35] ),
        .I4(\q_reg_n_0_[29] ),
        .O(\q[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hC0C0C0C0E0F0E0E0)) 
    \q[0]_i_5 
       (.I0(\q_reg_n_0_[32] ),
        .I1(\q_reg_n_0_[34] ),
        .I2(\q[0]_i_8_n_0 ),
        .I3(\q_reg_n_0_[31] ),
        .I4(\q_reg_n_0_[30] ),
        .I5(\q_reg_n_0_[33] ),
        .O(\q[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \q[0]_i_6 
       (.I0(\q_reg_n_0_[29] ),
        .I1(\q[0]_i_8_n_0 ),
        .I2(\q_reg_n_0_[31] ),
        .I3(\q_reg_n_0_[33] ),
        .I4(\q_reg_n_0_[25] ),
        .I5(\q_reg_n_0_[27] ),
        .O(\q[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF55551110)) 
    \q[0]_i_7 
       (.I0(\q_reg_n_0_[19] ),
        .I1(\q_reg_n_0_[17] ),
        .I2(\q[0]_i_9_n_0 ),
        .I3(\q_reg_n_0_[16] ),
        .I4(\q_reg_n_0_[18] ),
        .I5(\q_reg_n_0_[20] ),
        .O(\q[0]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \q[0]_i_8 
       (.I0(pst_reg_cnt[39]),
        .I1(\q_reg_n_0_[37] ),
        .I2(\q_reg_n_0_[35] ),
        .O(\q[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AAAAEEFE)) 
    \q[0]_i_9 
       (.I0(\q_reg_n_0_[14] ),
        .I1(\q_reg_n_0_[12] ),
        .I2(\q[0]_i_10_n_0 ),
        .I3(\q_reg_n_0_[11] ),
        .I4(\q_reg_n_0_[13] ),
        .I5(\q_reg_n_0_[15] ),
        .O(\q[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFEFFF0F0FEFEF0F0)) 
    \q[1]_i_1 
       (.I0(\q_reg_n_0_[36] ),
        .I1(\q_reg_n_0_[37] ),
        .I2(\q[1]_i_2_n_0 ),
        .I3(\q[1]_i_3_n_0 ),
        .I4(\q[1]_i_4_n_0 ),
        .I5(\q[1]_i_5_n_0 ),
        .O(D[1]));
  LUT2 #(
    .INIT(4'hE)) 
    \q[1]_i_10 
       (.I0(\q_reg_n_0_[16] ),
        .I1(\q_reg_n_0_[17] ),
        .O(\q[1]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF1110)) 
    \q[1]_i_11 
       (.I0(\q_reg_n_0_[2] ),
        .I1(\q_reg_n_0_[3] ),
        .I2(pst_reg_cnt[0]),
        .I3(\q_reg_n_0_[1] ),
        .I4(\q_reg_n_0_[5] ),
        .I5(\q_reg_n_0_[4] ),
        .O(\q[1]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \q[1]_i_12 
       (.I0(\q_reg_n_0_[10] ),
        .I1(\q_reg_n_0_[11] ),
        .O(\q[1]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0808080808080800)) 
    \q[1]_i_2 
       (.I0(\q[5]_i_2_n_0 ),
        .I1(\q[1]_i_4_n_0 ),
        .I2(\q_reg_n_0_[27] ),
        .I3(\q_reg_n_0_[24] ),
        .I4(\q_reg_n_0_[25] ),
        .I5(\q[1]_i_6_n_0 ),
        .O(\q[1]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \q[1]_i_3 
       (.I0(\q_reg_n_0_[34] ),
        .I1(\q_reg_n_0_[35] ),
        .O(\q[1]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \q[1]_i_4 
       (.I0(pst_reg_cnt[38]),
        .I1(pst_reg_cnt[39]),
        .O(\q[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF1110)) 
    \q[1]_i_5 
       (.I0(\q_reg_n_0_[30] ),
        .I1(\q_reg_n_0_[31] ),
        .I2(\q_reg_n_0_[28] ),
        .I3(\q_reg_n_0_[29] ),
        .I4(\q_reg_n_0_[33] ),
        .I5(\q_reg_n_0_[32] ),
        .O(\q[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h1110111011111110)) 
    \q[1]_i_6 
       (.I0(\q_reg_n_0_[23] ),
        .I1(\q_reg_n_0_[22] ),
        .I2(\q_reg_n_0_[20] ),
        .I3(\q_reg_n_0_[21] ),
        .I4(\q[1]_i_7_n_0 ),
        .I5(\q[1]_i_8_n_0 ),
        .O(\q[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF11111110)) 
    \q[1]_i_7 
       (.I0(\q_reg_n_0_[14] ),
        .I1(\q_reg_n_0_[15] ),
        .I2(\q[1]_i_9_n_0 ),
        .I3(\q_reg_n_0_[13] ),
        .I4(\q_reg_n_0_[12] ),
        .I5(\q[1]_i_10_n_0 ),
        .O(\q[1]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \q[1]_i_8 
       (.I0(\q_reg_n_0_[18] ),
        .I1(\q_reg_n_0_[19] ),
        .O(\q[1]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h00000000EEEFEEEE)) 
    \q[1]_i_9 
       (.I0(\q_reg_n_0_[8] ),
        .I1(\q_reg_n_0_[9] ),
        .I2(\q_reg_n_0_[7] ),
        .I3(\q_reg_n_0_[6] ),
        .I4(\q[1]_i_11_n_0 ),
        .I5(\q[1]_i_12_n_0 ),
        .O(\q[1]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF000000020000)) 
    \q[2]_i_1 
       (.I0(\q[2]_i_2_n_0 ),
        .I1(\q_reg_n_0_[28] ),
        .I2(\q_reg_n_0_[29] ),
        .I3(\q[2]_i_3_n_0 ),
        .I4(\q[2]_i_4_n_0 ),
        .I5(\q[2]_i_5_n_0 ),
        .O(D[2]));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \q[2]_i_2 
       (.I0(\q[2]_i_6_n_0 ),
        .I1(\q_reg_n_0_[26] ),
        .I2(\q_reg_n_0_[27] ),
        .I3(\q_reg_n_0_[25] ),
        .I4(\q_reg_n_0_[24] ),
        .O(\q[2]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \q[2]_i_3 
       (.I0(\q_reg_n_0_[30] ),
        .I1(\q_reg_n_0_[31] ),
        .O(\q[2]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \q[2]_i_4 
       (.I0(pst_reg_cnt[39]),
        .I1(pst_reg_cnt[38]),
        .I2(\q_reg_n_0_[37] ),
        .I3(\q_reg_n_0_[36] ),
        .O(\q[2]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \q[2]_i_5 
       (.I0(\q_reg_n_0_[33] ),
        .I1(\q_reg_n_0_[32] ),
        .I2(\q_reg_n_0_[35] ),
        .I3(\q_reg_n_0_[34] ),
        .O(\q[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AAAAFEEE)) 
    \q[2]_i_6 
       (.I0(\q[5]_i_6_n_0 ),
        .I1(\q[5]_i_8_n_0 ),
        .I2(\q[2]_i_7_n_0 ),
        .I3(\q[3]_i_8_n_0 ),
        .I4(\q[2]_i_8_n_0 ),
        .I5(\q[2]_i_9_n_0 ),
        .O(\q[2]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \q[2]_i_7 
       (.I0(\q_reg_n_0_[7] ),
        .I1(\q_reg_n_0_[6] ),
        .I2(\q_reg_n_0_[5] ),
        .I3(\q_reg_n_0_[4] ),
        .O(\q[2]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \q[2]_i_8 
       (.I0(\q_reg_n_0_[15] ),
        .I1(\q_reg_n_0_[14] ),
        .I2(\q_reg_n_0_[13] ),
        .I3(\q_reg_n_0_[12] ),
        .O(\q[2]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \q[2]_i_9 
       (.I0(\q_reg_n_0_[22] ),
        .I1(\q_reg_n_0_[20] ),
        .I2(\q_reg_n_0_[23] ),
        .I3(\q_reg_n_0_[21] ),
        .O(\q[2]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFF00FF00FF00BA00)) 
    \q[3]_i_1 
       (.I0(\q[3]_i_2_n_0 ),
        .I1(\q[3]_i_3_n_0 ),
        .I2(\q[3]_i_4_n_0 ),
        .I3(\q[3]_i_5_n_0 ),
        .I4(\q[3]_i_6_n_0 ),
        .I5(\q[3]_i_7_n_0 ),
        .O(D[3]));
  LUT3 #(
    .INIT(8'hFE)) 
    \q[3]_i_2 
       (.I0(\q_reg_n_0_[27] ),
        .I1(\q_reg_n_0_[25] ),
        .I2(\q_reg_n_0_[24] ),
        .O(\q[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \q[3]_i_3 
       (.I0(\q_reg_n_0_[21] ),
        .I1(\q_reg_n_0_[23] ),
        .I2(\q_reg_n_0_[20] ),
        .I3(\q_reg_n_0_[22] ),
        .I4(\q[5]_i_6_n_0 ),
        .O(\q[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000001)) 
    \q[3]_i_4 
       (.I0(\q[3]_i_8_n_0 ),
        .I1(\q_reg_n_0_[7] ),
        .I2(\q_reg_n_0_[6] ),
        .I3(\q_reg_n_0_[5] ),
        .I4(\q_reg_n_0_[4] ),
        .I5(\q[5]_i_7_n_0 ),
        .O(\q[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h00000002)) 
    \q[3]_i_5 
       (.I0(\q[2]_i_4_n_0 ),
        .I1(\q_reg_n_0_[34] ),
        .I2(\q_reg_n_0_[35] ),
        .I3(\q_reg_n_0_[32] ),
        .I4(\q_reg_n_0_[33] ),
        .O(\q[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \q[3]_i_6 
       (.I0(\q_reg_n_0_[28] ),
        .I1(\q_reg_n_0_[29] ),
        .O(\q[3]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \q[3]_i_7 
       (.I0(\q_reg_n_0_[31] ),
        .I1(\q_reg_n_0_[30] ),
        .I2(\q_reg_n_0_[26] ),
        .O(\q[3]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \q[3]_i_8 
       (.I0(\q_reg_n_0_[3] ),
        .I1(\q_reg_n_0_[2] ),
        .I2(\q_reg_n_0_[1] ),
        .I3(pst_reg_cnt[0]),
        .O(\q[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0200000000000000)) 
    \q[4]_i_1 
       (.I0(\q[5]_i_2_n_0 ),
        .I1(\q_reg_n_0_[32] ),
        .I2(\q_reg_n_0_[24] ),
        .I3(\q[5]_i_3_n_0 ),
        .I4(\q[5]_i_4_n_0 ),
        .I5(\q[5]_i_5_n_0 ),
        .O(D[4]));
  LUT6 #(
    .INIT(64'h0000000002000000)) 
    \q[5]_i_1 
       (.I0(\q[5]_i_2_n_0 ),
        .I1(\q_reg_n_0_[32] ),
        .I2(\q_reg_n_0_[24] ),
        .I3(\q[5]_i_3_n_0 ),
        .I4(\q[5]_i_4_n_0 ),
        .I5(\q[5]_i_5_n_0 ),
        .O(D[5]));
  LUT5 #(
    .INIT(32'h00000001)) 
    \q[5]_i_2 
       (.I0(\q_reg_n_0_[26] ),
        .I1(\q_reg_n_0_[30] ),
        .I2(\q_reg_n_0_[31] ),
        .I3(\q_reg_n_0_[35] ),
        .I4(\q_reg_n_0_[34] ),
        .O(\q[5]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \q[5]_i_3 
       (.I0(\q_reg_n_0_[27] ),
        .I1(\q_reg_n_0_[25] ),
        .I2(\q_reg_n_0_[33] ),
        .O(\q[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \q[5]_i_4 
       (.I0(\q_reg_n_0_[36] ),
        .I1(\q_reg_n_0_[37] ),
        .I2(pst_reg_cnt[38]),
        .I3(pst_reg_cnt[39]),
        .I4(\q_reg_n_0_[29] ),
        .I5(\q_reg_n_0_[28] ),
        .O(\q[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \q[5]_i_5 
       (.I0(\q[5]_i_6_n_0 ),
        .I1(\q_reg_n_0_[22] ),
        .I2(\q_reg_n_0_[20] ),
        .I3(\q_reg_n_0_[23] ),
        .I4(\q_reg_n_0_[21] ),
        .I5(\q[5]_i_7_n_0 ),
        .O(\q[5]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \q[5]_i_6 
       (.I0(\q_reg_n_0_[19] ),
        .I1(\q_reg_n_0_[18] ),
        .I2(\q_reg_n_0_[17] ),
        .I3(\q_reg_n_0_[16] ),
        .O(\q[5]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \q[5]_i_7 
       (.I0(\q_reg_n_0_[12] ),
        .I1(\q_reg_n_0_[13] ),
        .I2(\q_reg_n_0_[14] ),
        .I3(\q_reg_n_0_[15] ),
        .I4(\q[5]_i_8_n_0 ),
        .O(\q[5]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \q[5]_i_8 
       (.I0(\q_reg_n_0_[11] ),
        .I1(\q_reg_n_0_[10] ),
        .I2(\q_reg_n_0_[9] ),
        .I3(\q_reg_n_0_[8] ),
        .O(\q[5]_i_8_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [0]),
        .Q(pst_reg_cnt[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[10] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [10]),
        .Q(\q_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[11] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [11]),
        .Q(\q_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[12] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [12]),
        .Q(\q_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[13] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [13]),
        .Q(\q_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[14] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [14]),
        .Q(\q_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[15] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [15]),
        .Q(\q_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[16] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [16]),
        .Q(\q_reg_n_0_[16] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[17] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [17]),
        .Q(\q_reg_n_0_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[18] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [18]),
        .Q(\q_reg_n_0_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[19] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [19]),
        .Q(\q_reg_n_0_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [1]),
        .Q(\q_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[20] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [20]),
        .Q(\q_reg_n_0_[20] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[21] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [21]),
        .Q(\q_reg_n_0_[21] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[22] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [22]),
        .Q(\q_reg_n_0_[22] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[23] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [23]),
        .Q(\q_reg_n_0_[23] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[24] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [24]),
        .Q(\q_reg_n_0_[24] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[25] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [25]),
        .Q(\q_reg_n_0_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[26] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [26]),
        .Q(\q_reg_n_0_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[27] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [27]),
        .Q(\q_reg_n_0_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[28] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [28]),
        .Q(\q_reg_n_0_[28] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[29] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [29]),
        .Q(\q_reg_n_0_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [2]),
        .Q(\q_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[30] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [30]),
        .Q(\q_reg_n_0_[30] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[31] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [31]),
        .Q(\q_reg_n_0_[31] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[32] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [32]),
        .Q(\q_reg_n_0_[32] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[33] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [33]),
        .Q(\q_reg_n_0_[33] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[34] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [34]),
        .Q(\q_reg_n_0_[34] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[35] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [35]),
        .Q(\q_reg_n_0_[35] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[36] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [36]),
        .Q(\q_reg_n_0_[36] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[37] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [37]),
        .Q(\q_reg_n_0_[37] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[38] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [38]),
        .Q(pst_reg_cnt[38]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[39] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [39]),
        .Q(pst_reg_cnt[39]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [3]),
        .Q(\q_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[4] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [4]),
        .Q(\q_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[5] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [5]),
        .Q(\q_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[6] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [6]),
        .Q(\q_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[7] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [7]),
        .Q(\q_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[8] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [8]),
        .Q(\q_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[9] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[39]_0 [9]),
        .Q(\q_reg_n_0_[9] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register__parameterized1
   (Q,
    D,
    \q_reg[5]_0 ,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3);
  output [5:0]Q;
  input [5:0]D;
  input \q_reg[5]_0 ;
  output lopt;
  output lopt_1;
  output lopt_2;
  output lopt_3;

  wire [5:0]D;
  wire [5:0]Q;
  wire \q_reg[0]_lopt_replica_1 ;
  wire \q_reg[1]_lopt_replica_1 ;
  wire \q_reg[2]_lopt_replica_1 ;
  wire \q_reg[3]_lopt_replica_1 ;
  wire \q_reg[5]_0 ;

  assign lopt = \q_reg[0]_lopt_replica_1 ;
  assign lopt_1 = \q_reg[1]_lopt_replica_1 ;
  assign lopt_2 = \q_reg[2]_lopt_replica_1 ;
  assign lopt_3 = \q_reg[3]_lopt_replica_1 ;
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(\q_reg[5]_0 ),
        .CE(1'b1),
        .D(D[0]),
        .Q(Q[0]),
        .R(1'b0));
  (* OPT_INSERTED_REPDRIVER *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0]_lopt_replica 
       (.C(\q_reg[5]_0 ),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_lopt_replica_1 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(\q_reg[5]_0 ),
        .CE(1'b1),
        .D(D[1]),
        .Q(Q[1]),
        .R(1'b0));
  (* OPT_INSERTED_REPDRIVER *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1]_lopt_replica 
       (.C(\q_reg[5]_0 ),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[1]_lopt_replica_1 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(\q_reg[5]_0 ),
        .CE(1'b1),
        .D(D[2]),
        .Q(Q[2]),
        .R(1'b0));
  (* OPT_INSERTED_REPDRIVER *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2]_lopt_replica 
       (.C(\q_reg[5]_0 ),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[2]_lopt_replica_1 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(\q_reg[5]_0 ),
        .CE(1'b1),
        .D(D[3]),
        .Q(Q[3]),
        .R(1'b0));
  (* OPT_INSERTED_REPDRIVER *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3]_lopt_replica 
       (.C(\q_reg[5]_0 ),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[3]_lopt_replica_1 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[4] 
       (.C(\q_reg[5]_0 ),
        .CE(1'b1),
        .D(D[4]),
        .Q(Q[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[5] 
       (.C(\q_reg[5]_0 ),
        .CE(1'b1),
        .D(D[5]),
        .Q(Q[5]),
        .R(1'b0));
endmodule

module tdc
   (pst_trig,
    Q,
    JA_OBUF,
    CLK,
    p_0_in,
    \q_reg[0] ,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3);
  output pst_trig;
  output [5:0]Q;
  input [0:0]JA_OBUF;
  input CLK;
  input p_0_in;
  input \q_reg[0] ;
  output lopt;
  output lopt_1;
  output lopt_2;
  output lopt_3;

  wire CLK;
  wire [0:0]JA_OBUF;
  wire [5:0]Q;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire p_0_in;
  wire pst_tdl_n_0;
  wire pst_tdl_n_1;
  wire pst_tdl_n_10;
  wire pst_tdl_n_11;
  wire pst_tdl_n_12;
  wire pst_tdl_n_13;
  wire pst_tdl_n_14;
  wire pst_tdl_n_15;
  wire pst_tdl_n_16;
  wire pst_tdl_n_17;
  wire pst_tdl_n_18;
  wire pst_tdl_n_19;
  wire pst_tdl_n_2;
  wire pst_tdl_n_20;
  wire pst_tdl_n_21;
  wire pst_tdl_n_22;
  wire pst_tdl_n_23;
  wire pst_tdl_n_24;
  wire pst_tdl_n_25;
  wire pst_tdl_n_26;
  wire pst_tdl_n_27;
  wire pst_tdl_n_28;
  wire pst_tdl_n_29;
  wire pst_tdl_n_3;
  wire pst_tdl_n_30;
  wire pst_tdl_n_31;
  wire pst_tdl_n_32;
  wire pst_tdl_n_33;
  wire pst_tdl_n_34;
  wire pst_tdl_n_35;
  wire pst_tdl_n_36;
  wire pst_tdl_n_37;
  wire pst_tdl_n_38;
  wire pst_tdl_n_39;
  wire pst_tdl_n_4;
  wire pst_tdl_n_5;
  wire pst_tdl_n_6;
  wire pst_tdl_n_7;
  wire pst_tdl_n_8;
  wire pst_tdl_n_9;
  wire pst_trig;
  wire \q_reg[0] ;

  calc_unit calc_unit
       (.CLK(CLK),
        .D({pst_tdl_n_0,pst_tdl_n_1,pst_tdl_n_2,pst_tdl_n_3,pst_tdl_n_4,pst_tdl_n_5,pst_tdl_n_6,pst_tdl_n_7,pst_tdl_n_8,pst_tdl_n_9,pst_tdl_n_10,pst_tdl_n_11,pst_tdl_n_12,pst_tdl_n_13,pst_tdl_n_14,pst_tdl_n_15,pst_tdl_n_16,pst_tdl_n_17,pst_tdl_n_18,pst_tdl_n_19,pst_tdl_n_20,pst_tdl_n_21,pst_tdl_n_22,pst_tdl_n_23,pst_tdl_n_24,pst_tdl_n_25,pst_tdl_n_26,pst_tdl_n_27,pst_tdl_n_28,pst_tdl_n_29,pst_tdl_n_30,pst_tdl_n_31,pst_tdl_n_32,pst_tdl_n_33,pst_tdl_n_34,pst_tdl_n_35,pst_tdl_n_36,pst_tdl_n_37,pst_tdl_n_38,pst_tdl_n_39}),
        .JA_OBUF(JA_OBUF),
        .Q(Q),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .p_0_in(p_0_in),
        .pst_trig(pst_trig),
        .\q_reg[0] (\q_reg[0] ));
  n4bit_carry_chain pst_tdl
       (.CLK(CLK),
        .D({pst_tdl_n_0,pst_tdl_n_1,pst_tdl_n_2,pst_tdl_n_3,pst_tdl_n_4,pst_tdl_n_5,pst_tdl_n_6,pst_tdl_n_7,pst_tdl_n_8,pst_tdl_n_9,pst_tdl_n_10,pst_tdl_n_11,pst_tdl_n_12,pst_tdl_n_13,pst_tdl_n_14,pst_tdl_n_15,pst_tdl_n_16,pst_tdl_n_17,pst_tdl_n_18,pst_tdl_n_19,pst_tdl_n_20,pst_tdl_n_21,pst_tdl_n_22,pst_tdl_n_23,pst_tdl_n_24,pst_tdl_n_25,pst_tdl_n_26,pst_tdl_n_27,pst_tdl_n_28,pst_tdl_n_29,pst_tdl_n_30,pst_tdl_n_31,pst_tdl_n_32,pst_tdl_n_33,pst_tdl_n_34,pst_tdl_n_35,pst_tdl_n_36,pst_tdl_n_37,pst_tdl_n_38,pst_tdl_n_39}),
        .JA_OBUF(JA_OBUF));
endmodule

(* ECO_CHECKSUM = "c802f065" *) 
(* NotValidForBitStream *)
module top
   (BTN,
    JA,
    LED,
    CLK,
    UART_TXD,
    START,
    STOP);
  input [3:0]BTN;
  output [7:0]JA;
  output [3:0]LED;
  input CLK;
  output UART_TXD;
  input START;
  input STOP;

  wire CLK;
  wire CLK_IBUF;
  wire CLK_IBUF_BUFG;
  wire [5:0]DATA;
  wire DATA_UART;
  wire \DATA_UART[5]_i_1_n_0 ;
  wire \FSM_sequential_uart_state[0]_i_2_n_0 ;
  wire \FSM_sequential_uart_state[2]_i_1_n_0 ;
  wire Inst_UART_TX_CTRL_n_1;
  wire Inst_UART_TX_CTRL_n_2;
  wire [7:0]JA;
  wire [3:0]JA_OBUF;
  wire [3:0]LED;
  wire [3:0]LED_OBUF;
  wire START;
  wire START_IBUF;
  wire START_IBUF_BUFG;
  wire STOP;
  wire UART_TXD;
  wire UART_TXD_OBUF;
  wire clear;
  wire clk_out1;
  wire [5:0]data3;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire meas_end;
  wire p_0_in;
  wire pst_trig;
  wire pst_trig_BUFG;
  wire \slice[0]_i_1_n_0 ;
  wire \slice[1]_i_1_n_0 ;
  wire \slice[2]_i_1_n_0 ;
  wire \slice_reg_n_0_[0] ;
  wire \slice_reg_n_0_[1] ;
  wire \slice_reg_n_0_[2] ;
  wire start_end;
  wire [24:24]tdc_result;
  wire [13:12]tdc_result0_in;
  wire uart_send_i_1_n_0;
  wire uart_send_reg_n_0;
  wire [2:0]uart_state;

initial begin
 $sdf_annotate("n4bit_carry_chain_tb_time_impl.sdf",,,,"tool_control");
end
  (* LOPT_BUFG_CLOCK *) 
  (* OPT_MODIFIED = "BUFG_OPT" *) 
  BUFG CLK_IBUF_BUFG_inst
       (.I(CLK_IBUF),
        .O(CLK_IBUF_BUFG));
  (* OPT_INSERTED *) 
  (* OPT_MODIFIED = "MLO BUFG_OPT" *) 
  IBUF CLK_IBUF_inst
       (.I(CLK),
        .O(CLK_IBUF));
  LUT6 #(
    .INIT(64'h0400040404040404)) 
    \DATA_UART[5]_i_1 
       (.I0(uart_state[0]),
        .I1(uart_state[1]),
        .I2(uart_state[2]),
        .I3(\slice_reg_n_0_[2] ),
        .I4(\slice_reg_n_0_[1] ),
        .I5(\slice_reg_n_0_[0] ),
        .O(\DATA_UART[5]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \DATA_UART[5]_i_2 
       (.I0(uart_state[2]),
        .I1(uart_state[1]),
        .I2(uart_state[0]),
        .O(DATA_UART));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(data3[0]),
        .Q(DATA[0]),
        .R(\DATA_UART[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(data3[1]),
        .Q(DATA[1]),
        .R(\DATA_UART[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(data3[2]),
        .Q(DATA[2]),
        .R(\DATA_UART[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(data3[3]),
        .Q(DATA[3]),
        .R(\DATA_UART[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(data3[4]),
        .Q(DATA[4]),
        .R(\DATA_UART[5]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(data3[5]),
        .Q(DATA[5]),
        .R(\DATA_UART[5]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_uart_state[0]_i_2 
       (.I0(\slice_reg_n_0_[2] ),
        .I1(\slice_reg_n_0_[1] ),
        .O(\FSM_sequential_uart_state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \FSM_sequential_uart_state[2]_i_1 
       (.I0(uart_state[0]),
        .I1(uart_state[2]),
        .I2(uart_state[1]),
        .O(\FSM_sequential_uart_state[2]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "prepare_send:010,send_data:011,busy:100,load_data:001,ready:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_uart_state_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(Inst_UART_TX_CTRL_n_1),
        .Q(uart_state[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "prepare_send:010,send_data:011,busy:100,load_data:001,ready:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_uart_state_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(Inst_UART_TX_CTRL_n_2),
        .Q(uart_state[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "prepare_send:010,send_data:011,busy:100,load_data:001,ready:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_uart_state_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_sequential_uart_state[2]_i_1_n_0 ),
        .Q(uart_state[2]),
        .R(1'b0));
  UART_TX_CTRL Inst_UART_TX_CTRL
       (.CLK(CLK_IBUF_BUFG),
        .D(DATA),
        .E(uart_send_reg_n_0),
        .\FSM_sequential_txState_reg[0]_0 (Inst_UART_TX_CTRL_n_2),
        .\FSM_sequential_uart_state_reg[0] (\slice_reg_n_0_[0] ),
        .\FSM_sequential_uart_state_reg[0]_0 (\FSM_sequential_uart_state[0]_i_2_n_0 ),
        .\FSM_sequential_uart_state_reg[2] (Inst_UART_TX_CTRL_n_1),
        .JA_OBUF(JA_OBUF[3]),
        .UART_TXD_OBUF(UART_TXD_OBUF),
        .uart_state(uart_state));
  OBUF \JA_OBUF[0]_inst 
       (.I(JA_OBUF[0]),
        .O(JA[0]));
  OBUF \JA_OBUF[1]_inst 
       (.I(JA_OBUF[1]),
        .O(JA[1]));
  OBUF \JA_OBUF[2]_inst 
       (.I(JA_OBUF[2]),
        .O(JA[2]));
  OBUF \JA_OBUF[3]_inst 
       (.I(JA_OBUF[3]),
        .O(JA[3]));
  OBUFT \JA_OBUF[4]_inst 
       (.I(1'b0),
        .O(JA[4]),
        .T(1'b1));
  OBUFT \JA_OBUF[5]_inst 
       (.I(1'b0),
        .O(JA[5]),
        .T(1'b1));
  OBUFT \JA_OBUF[6]_inst 
       (.I(1'b0),
        .O(JA[6]),
        .T(1'b1));
  OBUFT \JA_OBUF[7]_inst 
       (.I(1'b0),
        .O(JA[7]),
        .T(1'b1));
  (* OPT_MODIFIED = "SWEEP" *) 
  OBUF \LED_OBUF[0]_inst 
       (.I(lopt),
        .O(LED[0]));
  (* OPT_MODIFIED = "SWEEP" *) 
  OBUF \LED_OBUF[1]_inst 
       (.I(lopt_1),
        .O(LED[1]));
  (* OPT_MODIFIED = "SWEEP" *) 
  OBUF \LED_OBUF[2]_inst 
       (.I(lopt_2),
        .O(LED[2]));
  (* OPT_MODIFIED = "SWEEP" *) 
  OBUF \LED_OBUF[3]_inst 
       (.I(lopt_3),
        .O(LED[3]));
  BUFG START_IBUF_BUFG_inst
       (.I(START_IBUF),
        .O(START_IBUF_BUFG));
  IBUF START_IBUF_inst
       (.I(START),
        .O(START_IBUF));
  IBUF STOP_IBUF_inst
       (.I(STOP),
        .O(JA_OBUF[0]));
  OBUF UART_TXD_OBUF_inst
       (.I(UART_TXD_OBUF),
        .O(UART_TXD));
  (* syn_black_box = "TRUE" *) 
  clk_wiz_0 inst_clk
       (.clk_in1(CLK_IBUF_BUFG),
        .clk_out1(clk_out1),
        .reset(1'b0));
  addf inst_dff1
       (.JA_OBUF(JA_OBUF[2:1]),
        .SR(clear),
        .START_IBUF_BUFG(START_IBUF_BUFG),
        .\count_reg[2] (JA_OBUF[0]),
        .meas_end(meas_end),
        .p_0_in(p_0_in));
  tdc inst_tdc
       (.CLK(clk_out1),
        .JA_OBUF(JA_OBUF[1]),
        .Q({tdc_result0_in,LED_OBUF}),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .p_0_in(p_0_in),
        .pst_trig(pst_trig),
        .\q_reg[0] (pst_trig_BUFG));
  BUFG pst_trig_BUFG_inst
       (.I(pst_trig),
        .O(pst_trig_BUFG));
  LUT6 #(
    .INIT(64'hFFFFF0FF00000D00)) 
    \slice[0]_i_1 
       (.I0(\slice_reg_n_0_[2] ),
        .I1(\slice_reg_n_0_[1] ),
        .I2(uart_state[1]),
        .I3(uart_state[2]),
        .I4(uart_state[0]),
        .I5(\slice_reg_n_0_[0] ),
        .O(\slice[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hFFDF0020)) 
    \slice[1]_i_1 
       (.I0(\slice_reg_n_0_[0] ),
        .I1(uart_state[1]),
        .I2(uart_state[2]),
        .I3(uart_state[0]),
        .I4(\slice_reg_n_0_[1] ),
        .O(\slice[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF6FF00000800)) 
    \slice[2]_i_1 
       (.I0(\slice_reg_n_0_[1] ),
        .I1(\slice_reg_n_0_[0] ),
        .I2(uart_state[1]),
        .I3(uart_state[2]),
        .I4(uart_state[0]),
        .I5(\slice_reg_n_0_[2] ),
        .O(\slice[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slice_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\slice[0]_i_1_n_0 ),
        .Q(\slice_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slice_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\slice[1]_i_1_n_0 ),
        .Q(\slice_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slice_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\slice[2]_i_1_n_0 ),
        .Q(\slice_reg_n_0_[2] ),
        .R(1'b0));
  binary_counter__parameterized0 start_counter
       (.CLK(clk_out1),
        .E(JA_OBUF[2]),
        .SR(clear),
        .start_end(start_end));
  binary_counter stop_counter
       (.E(tdc_result),
        .JA_OBUF(JA_OBUF[3]),
        .clk_out1(clk_out1),
        .meas_end(meas_end),
        .p_0_in(p_0_in),
        .start_end(start_end),
        .uart_state(uart_state));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(LED_OBUF[2]),
        .Q(data3[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(LED_OBUF[3]),
        .Q(data3[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[12]),
        .Q(data3[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[13]),
        .Q(data3[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(LED_OBUF[0]),
        .Q(data3[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(LED_OBUF[1]),
        .Q(data3[1]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hEF08)) 
    uart_send_i_1
       (.I0(uart_state[0]),
        .I1(uart_state[1]),
        .I2(uart_state[2]),
        .I3(uart_send_reg_n_0),
        .O(uart_send_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    uart_send_reg
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(uart_send_i_1_n_0),
        .Q(uart_send_reg_n_0),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
