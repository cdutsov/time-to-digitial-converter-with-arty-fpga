// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Tue Aug 27 16:36:11 2019
// Host        : rambo running 64-bit Arch Linux
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/chavdar/tdc_uart/tdc_uart.sim/sim_1/impl/timing/xsim/tdc_tb_time_impl.v
// Design      : top
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7a35ticsg324-1L
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module UART_TX_CTRL
   (UART_TXD_OBUF,
    \FSM_sequential_uart_state_reg[2] ,
    \FSM_sequential_txState_reg[0]_0 ,
    CLK,
    uart_state,
    \FSM_sequential_uart_state_reg[0] ,
    \FSM_sequential_uart_state_reg[0]_0 ,
    JD_OBUF,
    E,
    D);
  output UART_TXD_OBUF;
  output \FSM_sequential_uart_state_reg[2] ;
  output \FSM_sequential_txState_reg[0]_0 ;
  input CLK;
  input [2:0]uart_state;
  input \FSM_sequential_uart_state_reg[0] ;
  input \FSM_sequential_uart_state_reg[0]_0 ;
  input [0:0]JD_OBUF;
  input [0:0]E;
  input [7:0]D;

  wire CLK;
  wire [7:0]D;
  wire [0:0]E;
  wire \FSM_sequential_txState[0]_i_1_n_0 ;
  wire \FSM_sequential_txState[0]_i_2_n_0 ;
  wire \FSM_sequential_txState[0]_i_3_n_0 ;
  wire \FSM_sequential_txState[0]_i_4_n_0 ;
  wire \FSM_sequential_txState[0]_i_5_n_0 ;
  wire \FSM_sequential_txState[0]_i_6_n_0 ;
  wire \FSM_sequential_txState[0]_i_7_n_0 ;
  wire \FSM_sequential_txState[0]_i_8_n_0 ;
  wire \FSM_sequential_txState[0]_i_9_n_0 ;
  wire \FSM_sequential_txState[1]_i_1_n_0 ;
  wire \FSM_sequential_txState[1]_i_3_n_0 ;
  wire \FSM_sequential_txState[1]_i_4_n_0 ;
  wire \FSM_sequential_txState_reg[0]_0 ;
  wire \FSM_sequential_uart_state[0]_i_3_n_0 ;
  wire \FSM_sequential_uart_state_reg[0] ;
  wire \FSM_sequential_uart_state_reg[0]_0 ;
  wire \FSM_sequential_uart_state_reg[2] ;
  wire [0:0]JD_OBUF;
  wire READY;
  wire UART_TXD_OBUF;
  wire bitIndex;
  wire \bitIndex[0]_i_2_n_0 ;
  wire [30:0]bitIndex_reg;
  wire \bitIndex_reg[0]_i_1_n_0 ;
  wire \bitIndex_reg[0]_i_1_n_4 ;
  wire \bitIndex_reg[0]_i_1_n_5 ;
  wire \bitIndex_reg[0]_i_1_n_6 ;
  wire \bitIndex_reg[0]_i_1_n_7 ;
  wire \bitIndex_reg[12]_i_1_n_0 ;
  wire \bitIndex_reg[12]_i_1_n_4 ;
  wire \bitIndex_reg[12]_i_1_n_5 ;
  wire \bitIndex_reg[12]_i_1_n_6 ;
  wire \bitIndex_reg[12]_i_1_n_7 ;
  wire \bitIndex_reg[16]_i_1_n_0 ;
  wire \bitIndex_reg[16]_i_1_n_4 ;
  wire \bitIndex_reg[16]_i_1_n_5 ;
  wire \bitIndex_reg[16]_i_1_n_6 ;
  wire \bitIndex_reg[16]_i_1_n_7 ;
  wire \bitIndex_reg[20]_i_1_n_0 ;
  wire \bitIndex_reg[20]_i_1_n_4 ;
  wire \bitIndex_reg[20]_i_1_n_5 ;
  wire \bitIndex_reg[20]_i_1_n_6 ;
  wire \bitIndex_reg[20]_i_1_n_7 ;
  wire \bitIndex_reg[24]_i_1_n_0 ;
  wire \bitIndex_reg[24]_i_1_n_4 ;
  wire \bitIndex_reg[24]_i_1_n_5 ;
  wire \bitIndex_reg[24]_i_1_n_6 ;
  wire \bitIndex_reg[24]_i_1_n_7 ;
  wire \bitIndex_reg[28]_i_1_n_5 ;
  wire \bitIndex_reg[28]_i_1_n_6 ;
  wire \bitIndex_reg[28]_i_1_n_7 ;
  wire \bitIndex_reg[4]_i_1_n_0 ;
  wire \bitIndex_reg[4]_i_1_n_4 ;
  wire \bitIndex_reg[4]_i_1_n_5 ;
  wire \bitIndex_reg[4]_i_1_n_6 ;
  wire \bitIndex_reg[4]_i_1_n_7 ;
  wire \bitIndex_reg[8]_i_1_n_0 ;
  wire \bitIndex_reg[8]_i_1_n_4 ;
  wire \bitIndex_reg[8]_i_1_n_5 ;
  wire \bitIndex_reg[8]_i_1_n_6 ;
  wire \bitIndex_reg[8]_i_1_n_7 ;
  wire bitTmr;
  wire \bitTmr[0]_i_3_n_0 ;
  wire [13:0]bitTmr_reg;
  wire \bitTmr_reg[0]_i_2_n_0 ;
  wire \bitTmr_reg[0]_i_2_n_4 ;
  wire \bitTmr_reg[0]_i_2_n_5 ;
  wire \bitTmr_reg[0]_i_2_n_6 ;
  wire \bitTmr_reg[0]_i_2_n_7 ;
  wire \bitTmr_reg[12]_i_1_n_6 ;
  wire \bitTmr_reg[12]_i_1_n_7 ;
  wire \bitTmr_reg[4]_i_1_n_0 ;
  wire \bitTmr_reg[4]_i_1_n_4 ;
  wire \bitTmr_reg[4]_i_1_n_5 ;
  wire \bitTmr_reg[4]_i_1_n_6 ;
  wire \bitTmr_reg[4]_i_1_n_7 ;
  wire \bitTmr_reg[8]_i_1_n_0 ;
  wire \bitTmr_reg[8]_i_1_n_4 ;
  wire \bitTmr_reg[8]_i_1_n_5 ;
  wire \bitTmr_reg[8]_i_1_n_6 ;
  wire \bitTmr_reg[8]_i_1_n_7 ;
  wire eqOp__12;
  wire txBit_i_3_n_0;
  wire txBit_i_4_n_0;
  wire txBit_i_5_n_0;
  wire [8:1]txData;
  wire [1:0]txState;
  wire [2:0]uart_state;
  wire [2:0]\NLW_bitIndex_reg[0]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[12]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[16]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[20]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[24]_i_1_CO_UNCONNECTED ;
  wire [3:0]\NLW_bitIndex_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_bitIndex_reg[28]_i_1_O_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[4]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitIndex_reg[8]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitTmr_reg[0]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_bitTmr_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_bitTmr_reg[12]_i_1_O_UNCONNECTED ;
  wire [2:0]\NLW_bitTmr_reg[4]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_bitTmr_reg[8]_i_1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hF000F0CA)) 
    \FSM_sequential_txState[0]_i_1 
       (.I0(E),
        .I1(eqOp__12),
        .I2(txState[1]),
        .I3(txState[0]),
        .I4(\FSM_sequential_txState[0]_i_2_n_0 ),
        .O(\FSM_sequential_txState[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \FSM_sequential_txState[0]_i_2 
       (.I0(\FSM_sequential_txState[0]_i_3_n_0 ),
        .I1(\FSM_sequential_txState[0]_i_4_n_0 ),
        .I2(bitIndex_reg[0]),
        .I3(bitIndex_reg[2]),
        .I4(bitIndex_reg[4]),
        .I5(\FSM_sequential_txState[0]_i_5_n_0 ),
        .O(\FSM_sequential_txState[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \FSM_sequential_txState[0]_i_3 
       (.I0(bitIndex_reg[13]),
        .I1(bitIndex_reg[14]),
        .I2(bitIndex_reg[15]),
        .I3(bitIndex_reg[28]),
        .I4(bitIndex_reg[30]),
        .I5(txState[1]),
        .O(\FSM_sequential_txState[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h00010000)) 
    \FSM_sequential_txState[0]_i_4 
       (.I0(bitIndex_reg[5]),
        .I1(bitIndex_reg[6]),
        .I2(bitIndex_reg[7]),
        .I3(bitIndex_reg[8]),
        .I4(\FSM_sequential_txState[0]_i_6_n_0 ),
        .O(\FSM_sequential_txState[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFEF)) 
    \FSM_sequential_txState[0]_i_5 
       (.I0(\FSM_sequential_txState[0]_i_7_n_0 ),
        .I1(\FSM_sequential_txState[0]_i_8_n_0 ),
        .I2(bitIndex_reg[1]),
        .I3(bitIndex_reg[29]),
        .I4(bitIndex_reg[17]),
        .I5(\FSM_sequential_txState[0]_i_9_n_0 ),
        .O(\FSM_sequential_txState[0]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_sequential_txState[0]_i_6 
       (.I0(bitIndex_reg[12]),
        .I1(bitIndex_reg[11]),
        .I2(bitIndex_reg[10]),
        .I3(bitIndex_reg[9]),
        .O(\FSM_sequential_txState[0]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_txState[0]_i_7 
       (.I0(bitIndex_reg[23]),
        .I1(bitIndex_reg[20]),
        .I2(bitIndex_reg[25]),
        .I3(bitIndex_reg[22]),
        .O(\FSM_sequential_txState[0]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_txState[0]_i_8 
       (.I0(bitIndex_reg[19]),
        .I1(bitIndex_reg[16]),
        .I2(bitIndex_reg[21]),
        .I3(bitIndex_reg[18]),
        .O(\FSM_sequential_txState[0]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \FSM_sequential_txState[0]_i_9 
       (.I0(bitIndex_reg[27]),
        .I1(bitIndex_reg[24]),
        .I2(bitIndex_reg[3]),
        .I3(bitIndex_reg[26]),
        .O(\FSM_sequential_txState[0]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    \FSM_sequential_txState[1]_i_1 
       (.I0(eqOp__12),
        .I1(txState[1]),
        .I2(txState[0]),
        .O(\FSM_sequential_txState[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    \FSM_sequential_txState[1]_i_2 
       (.I0(\FSM_sequential_txState[1]_i_3_n_0 ),
        .I1(bitTmr_reg[1]),
        .I2(bitTmr_reg[0]),
        .I3(bitTmr_reg[3]),
        .I4(bitTmr_reg[2]),
        .I5(\FSM_sequential_txState[1]_i_4_n_0 ),
        .O(eqOp__12));
  LUT4 #(
    .INIT(16'h0400)) 
    \FSM_sequential_txState[1]_i_3 
       (.I0(bitTmr_reg[7]),
        .I1(bitTmr_reg[6]),
        .I2(bitTmr_reg[4]),
        .I3(bitTmr_reg[5]),
        .O(\FSM_sequential_txState[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \FSM_sequential_txState[1]_i_4 
       (.I0(bitTmr_reg[8]),
        .I1(bitTmr_reg[9]),
        .I2(bitTmr_reg[10]),
        .I3(bitTmr_reg[11]),
        .I4(bitTmr_reg[13]),
        .I5(bitTmr_reg[12]),
        .O(\FSM_sequential_txState[1]_i_4_n_0 ));
  (* FSM_ENCODED_STATES = "send_bit:10,load_bit:01,rdy:00" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_txState_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_sequential_txState[0]_i_1_n_0 ),
        .Q(txState[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "send_bit:10,load_bit:01,rdy:00" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_txState_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_sequential_txState[1]_i_1_n_0 ),
        .Q(txState[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000FFFF75770000)) 
    \FSM_sequential_uart_state[0]_i_1 
       (.I0(uart_state[2]),
        .I1(uart_state[1]),
        .I2(\FSM_sequential_uart_state_reg[0] ),
        .I3(\FSM_sequential_uart_state_reg[0]_0 ),
        .I4(\FSM_sequential_uart_state[0]_i_3_n_0 ),
        .I5(uart_state[0]),
        .O(\FSM_sequential_uart_state_reg[2] ));
  LUT6 #(
    .INIT(64'h0000FFFF00FF03AA)) 
    \FSM_sequential_uart_state[0]_i_3 
       (.I0(JD_OBUF),
        .I1(txState[0]),
        .I2(txState[1]),
        .I3(uart_state[0]),
        .I4(uart_state[2]),
        .I5(uart_state[1]),
        .O(\FSM_sequential_uart_state[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFF0F0010)) 
    \FSM_sequential_uart_state[1]_i_1 
       (.I0(txState[0]),
        .I1(txState[1]),
        .I2(uart_state[0]),
        .I3(uart_state[2]),
        .I4(uart_state[1]),
        .O(\FSM_sequential_txState_reg[0]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \bitIndex[0]_i_2 
       (.I0(bitIndex_reg[0]),
        .O(\bitIndex[0]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[0] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[0]_i_1_n_7 ),
        .Q(bitIndex_reg[0]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\bitIndex_reg[0]_i_1_n_0 ,\NLW_bitIndex_reg[0]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\bitIndex_reg[0]_i_1_n_4 ,\bitIndex_reg[0]_i_1_n_5 ,\bitIndex_reg[0]_i_1_n_6 ,\bitIndex_reg[0]_i_1_n_7 }),
        .S({bitIndex_reg[3:1],\bitIndex[0]_i_2_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[10] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[8]_i_1_n_5 ),
        .Q(bitIndex_reg[10]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[11] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[8]_i_1_n_4 ),
        .Q(bitIndex_reg[11]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[12] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[12]_i_1_n_7 ),
        .Q(bitIndex_reg[12]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[12]_i_1 
       (.CI(\bitIndex_reg[8]_i_1_n_0 ),
        .CO({\bitIndex_reg[12]_i_1_n_0 ,\NLW_bitIndex_reg[12]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[12]_i_1_n_4 ,\bitIndex_reg[12]_i_1_n_5 ,\bitIndex_reg[12]_i_1_n_6 ,\bitIndex_reg[12]_i_1_n_7 }),
        .S(bitIndex_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[13] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[12]_i_1_n_6 ),
        .Q(bitIndex_reg[13]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[14] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[12]_i_1_n_5 ),
        .Q(bitIndex_reg[14]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[15] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[12]_i_1_n_4 ),
        .Q(bitIndex_reg[15]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[16] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[16]_i_1_n_7 ),
        .Q(bitIndex_reg[16]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[16]_i_1 
       (.CI(\bitIndex_reg[12]_i_1_n_0 ),
        .CO({\bitIndex_reg[16]_i_1_n_0 ,\NLW_bitIndex_reg[16]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[16]_i_1_n_4 ,\bitIndex_reg[16]_i_1_n_5 ,\bitIndex_reg[16]_i_1_n_6 ,\bitIndex_reg[16]_i_1_n_7 }),
        .S(bitIndex_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[17] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[16]_i_1_n_6 ),
        .Q(bitIndex_reg[17]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[18] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[16]_i_1_n_5 ),
        .Q(bitIndex_reg[18]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[19] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[16]_i_1_n_4 ),
        .Q(bitIndex_reg[19]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[1] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[0]_i_1_n_6 ),
        .Q(bitIndex_reg[1]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[20] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[20]_i_1_n_7 ),
        .Q(bitIndex_reg[20]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[20]_i_1 
       (.CI(\bitIndex_reg[16]_i_1_n_0 ),
        .CO({\bitIndex_reg[20]_i_1_n_0 ,\NLW_bitIndex_reg[20]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[20]_i_1_n_4 ,\bitIndex_reg[20]_i_1_n_5 ,\bitIndex_reg[20]_i_1_n_6 ,\bitIndex_reg[20]_i_1_n_7 }),
        .S(bitIndex_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[21] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[20]_i_1_n_6 ),
        .Q(bitIndex_reg[21]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[22] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[20]_i_1_n_5 ),
        .Q(bitIndex_reg[22]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[23] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[20]_i_1_n_4 ),
        .Q(bitIndex_reg[23]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[24] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[24]_i_1_n_7 ),
        .Q(bitIndex_reg[24]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[24]_i_1 
       (.CI(\bitIndex_reg[20]_i_1_n_0 ),
        .CO({\bitIndex_reg[24]_i_1_n_0 ,\NLW_bitIndex_reg[24]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[24]_i_1_n_4 ,\bitIndex_reg[24]_i_1_n_5 ,\bitIndex_reg[24]_i_1_n_6 ,\bitIndex_reg[24]_i_1_n_7 }),
        .S(bitIndex_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[25] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[24]_i_1_n_6 ),
        .Q(bitIndex_reg[25]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[26] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[24]_i_1_n_5 ),
        .Q(bitIndex_reg[26]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[27] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[24]_i_1_n_4 ),
        .Q(bitIndex_reg[27]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[28] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[28]_i_1_n_7 ),
        .Q(bitIndex_reg[28]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[28]_i_1 
       (.CI(\bitIndex_reg[24]_i_1_n_0 ),
        .CO(\NLW_bitIndex_reg[28]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_bitIndex_reg[28]_i_1_O_UNCONNECTED [3],\bitIndex_reg[28]_i_1_n_5 ,\bitIndex_reg[28]_i_1_n_6 ,\bitIndex_reg[28]_i_1_n_7 }),
        .S({1'b0,bitIndex_reg[30:28]}));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[29] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[28]_i_1_n_6 ),
        .Q(bitIndex_reg[29]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[2] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[0]_i_1_n_5 ),
        .Q(bitIndex_reg[2]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[30] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[28]_i_1_n_5 ),
        .Q(bitIndex_reg[30]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[3] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[0]_i_1_n_4 ),
        .Q(bitIndex_reg[3]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[4] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[4]_i_1_n_7 ),
        .Q(bitIndex_reg[4]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[4]_i_1 
       (.CI(\bitIndex_reg[0]_i_1_n_0 ),
        .CO({\bitIndex_reg[4]_i_1_n_0 ,\NLW_bitIndex_reg[4]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[4]_i_1_n_4 ,\bitIndex_reg[4]_i_1_n_5 ,\bitIndex_reg[4]_i_1_n_6 ,\bitIndex_reg[4]_i_1_n_7 }),
        .S(bitIndex_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[5] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[4]_i_1_n_6 ),
        .Q(bitIndex_reg[5]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[6] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[4]_i_1_n_5 ),
        .Q(bitIndex_reg[6]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[7] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[4]_i_1_n_4 ),
        .Q(bitIndex_reg[7]),
        .R(READY));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[8] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[8]_i_1_n_7 ),
        .Q(bitIndex_reg[8]),
        .R(READY));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitIndex_reg[8]_i_1 
       (.CI(\bitIndex_reg[4]_i_1_n_0 ),
        .CO({\bitIndex_reg[8]_i_1_n_0 ,\NLW_bitIndex_reg[8]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitIndex_reg[8]_i_1_n_4 ,\bitIndex_reg[8]_i_1_n_5 ,\bitIndex_reg[8]_i_1_n_6 ,\bitIndex_reg[8]_i_1_n_7 }),
        .S(bitIndex_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \bitIndex_reg[9] 
       (.C(CLK),
        .CE(bitIndex),
        .D(\bitIndex_reg[8]_i_1_n_6 ),
        .Q(bitIndex_reg[9]),
        .R(READY));
  LUT3 #(
    .INIT(8'hAB)) 
    \bitTmr[0]_i_1 
       (.I0(eqOp__12),
        .I1(txState[1]),
        .I2(txState[0]),
        .O(bitTmr));
  LUT1 #(
    .INIT(2'h1)) 
    \bitTmr[0]_i_3 
       (.I0(bitTmr_reg[0]),
        .O(\bitTmr[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[0]_i_2_n_7 ),
        .Q(bitTmr_reg[0]),
        .R(bitTmr));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitTmr_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\bitTmr_reg[0]_i_2_n_0 ,\NLW_bitTmr_reg[0]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\bitTmr_reg[0]_i_2_n_4 ,\bitTmr_reg[0]_i_2_n_5 ,\bitTmr_reg[0]_i_2_n_6 ,\bitTmr_reg[0]_i_2_n_7 }),
        .S({bitTmr_reg[3:1],\bitTmr[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[8]_i_1_n_5 ),
        .Q(bitTmr_reg[10]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[8]_i_1_n_4 ),
        .Q(bitTmr_reg[11]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[12]_i_1_n_7 ),
        .Q(bitTmr_reg[12]),
        .R(bitTmr));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitTmr_reg[12]_i_1 
       (.CI(\bitTmr_reg[8]_i_1_n_0 ),
        .CO(\NLW_bitTmr_reg[12]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_bitTmr_reg[12]_i_1_O_UNCONNECTED [3:2],\bitTmr_reg[12]_i_1_n_6 ,\bitTmr_reg[12]_i_1_n_7 }),
        .S({1'b0,1'b0,bitTmr_reg[13:12]}));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[12]_i_1_n_6 ),
        .Q(bitTmr_reg[13]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[0]_i_2_n_6 ),
        .Q(bitTmr_reg[1]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[0]_i_2_n_5 ),
        .Q(bitTmr_reg[2]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[0]_i_2_n_4 ),
        .Q(bitTmr_reg[3]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[4] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[4]_i_1_n_7 ),
        .Q(bitTmr_reg[4]),
        .R(bitTmr));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitTmr_reg[4]_i_1 
       (.CI(\bitTmr_reg[0]_i_2_n_0 ),
        .CO({\bitTmr_reg[4]_i_1_n_0 ,\NLW_bitTmr_reg[4]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitTmr_reg[4]_i_1_n_4 ,\bitTmr_reg[4]_i_1_n_5 ,\bitTmr_reg[4]_i_1_n_6 ,\bitTmr_reg[4]_i_1_n_7 }),
        .S(bitTmr_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[5] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[4]_i_1_n_6 ),
        .Q(bitTmr_reg[5]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[4]_i_1_n_5 ),
        .Q(bitTmr_reg[6]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[7] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[4]_i_1_n_4 ),
        .Q(bitTmr_reg[7]),
        .R(bitTmr));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[8] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[8]_i_1_n_7 ),
        .Q(bitTmr_reg[8]),
        .R(bitTmr));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \bitTmr_reg[8]_i_1 
       (.CI(\bitTmr_reg[4]_i_1_n_0 ),
        .CO({\bitTmr_reg[8]_i_1_n_0 ,\NLW_bitTmr_reg[8]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\bitTmr_reg[8]_i_1_n_4 ,\bitTmr_reg[8]_i_1_n_5 ,\bitTmr_reg[8]_i_1_n_6 ,\bitTmr_reg[8]_i_1_n_7 }),
        .S(bitTmr_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \bitTmr_reg[9] 
       (.C(CLK),
        .CE(1'b1),
        .D(\bitTmr_reg[8]_i_1_n_6 ),
        .Q(bitTmr_reg[9]),
        .R(bitTmr));
  LUT2 #(
    .INIT(4'h1)) 
    txBit_i_1
       (.I0(txState[0]),
        .I1(txState[1]),
        .O(READY));
  LUT2 #(
    .INIT(4'h2)) 
    txBit_i_2
       (.I0(txState[0]),
        .I1(txState[1]),
        .O(bitIndex));
  LUT6 #(
    .INIT(64'hEEFFEEF0EE0FEE00)) 
    txBit_i_3
       (.I0(txData[8]),
        .I1(bitIndex_reg[0]),
        .I2(bitIndex_reg[2]),
        .I3(bitIndex_reg[3]),
        .I4(txBit_i_4_n_0),
        .I5(txBit_i_5_n_0),
        .O(txBit_i_3_n_0));
  LUT5 #(
    .INIT(32'hFAC00AC0)) 
    txBit_i_4
       (.I0(txData[2]),
        .I1(txData[1]),
        .I2(bitIndex_reg[0]),
        .I3(bitIndex_reg[1]),
        .I4(txData[3]),
        .O(txBit_i_4_n_0));
  LUT6 #(
    .INIT(64'hCACAFFF0CACA0F00)) 
    txBit_i_5
       (.I0(txData[5]),
        .I1(txData[7]),
        .I2(bitIndex_reg[1]),
        .I3(txData[4]),
        .I4(bitIndex_reg[0]),
        .I5(txData[6]),
        .O(txBit_i_5_n_0));
  FDSE #(
    .INIT(1'b1)) 
    txBit_reg
       (.C(CLK),
        .CE(bitIndex),
        .D(txBit_i_3_n_0),
        .Q(UART_TXD_OBUF),
        .S(READY));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(D[0]),
        .Q(txData[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(D[1]),
        .Q(txData[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(D[2]),
        .Q(txData[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(D[3]),
        .Q(txData[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(D[4]),
        .Q(txData[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(D[5]),
        .Q(txData[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[7] 
       (.C(CLK),
        .CE(E),
        .D(D[6]),
        .Q(txData[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \txData_reg[8] 
       (.C(CLK),
        .CE(E),
        .D(D[7]),
        .Q(txData[8]),
        .R(1'b0));
endmodule

module binary_counter
   (Q,
    A,
    JD_OBUF,
    CLK);
  output [3:0]Q;
  output [0:0]A;
  input [1:0]JD_OBUF;
  input CLK;

  wire [0:0]A;
  wire CLK;
  wire [1:0]JD_OBUF;
  wire [3:0]Q;
  wire clear;
  wire [3:0]plusOp__0;

  (* SOFT_HLUTNM = "soft_lutpair220" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1 
       (.I0(Q[0]),
        .O(plusOp__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair220" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(plusOp__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair219" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count[2]_i_1__0 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .O(plusOp__0[2]));
  LUT2 #(
    .INIT(4'h1)) 
    \count[3]_i_1 
       (.I0(JD_OBUF[0]),
        .I1(JD_OBUF[1]),
        .O(clear));
  (* SOFT_HLUTNM = "soft_lutpair219" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count[3]_i_2 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(plusOp__0[3]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(plusOp__0[0]),
        .Q(Q[0]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(plusOp__0[1]),
        .Q(Q[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(plusOp__0[2]),
        .Q(Q[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(plusOp__0[3]),
        .Q(Q[3]),
        .R(clear));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d3_i_1
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[3]),
        .I3(Q[2]),
        .O(A));
endmodule

(* ORIG_REF_NAME = "binary_counter" *) 
module binary_counter__parameterized0
   (max_cnt,
    JD_OBUF,
    CLK);
  output max_cnt;
  input [0:0]JD_OBUF;
  input CLK;

  wire CLK;
  wire [0:0]JD_OBUF;
  wire \count[2]_i_1_n_0 ;
  wire \count_reg_n_0_[0] ;
  wire \count_reg_n_0_[1] ;
  wire \count_reg_n_0_[2] ;
  wire max_cnt;
  wire max_cnt_i_1_n_0;
  wire [2:0]plusOp;

  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1 
       (.I0(\count_reg_n_0_[0] ),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count[1]_i_1 
       (.I0(\count_reg_n_0_[0] ),
        .I1(\count_reg_n_0_[1] ),
        .O(plusOp[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \count[2]_i_1 
       (.I0(JD_OBUF),
        .O(\count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count[2]_i_2 
       (.I0(\count_reg_n_0_[0] ),
        .I1(\count_reg_n_0_[1] ),
        .I2(\count_reg_n_0_[2] ),
        .O(plusOp[2]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(CLK),
        .CE(JD_OBUF),
        .D(plusOp[0]),
        .Q(\count_reg_n_0_[0] ),
        .R(\count[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(CLK),
        .CE(JD_OBUF),
        .D(plusOp[1]),
        .Q(\count_reg_n_0_[1] ),
        .R(\count[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(CLK),
        .CE(JD_OBUF),
        .D(plusOp[2]),
        .Q(\count_reg_n_0_[2] ),
        .R(\count[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h20)) 
    max_cnt_i_1
       (.I0(\count_reg_n_0_[2] ),
        .I1(\count_reg_n_0_[0] ),
        .I2(\count_reg_n_0_[1] ),
        .O(max_cnt_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    max_cnt_reg
       (.C(CLK),
        .CE(JD_OBUF),
        .D(max_cnt_i_1_n_0),
        .Q(max_cnt),
        .R(\count[2]_i_1_n_0 ));
endmodule

module calc_unit
   (pre_trig,
    pst_trig,
    D,
    JD_OBUF,
    CLK,
    \q_reg[0] ,
    A,
    \q_reg[319] ,
    \q_reg[319]_0 ,
    \q_reg[0]_0 ,
    Q);
  output pre_trig;
  output pst_trig;
  output [23:0]D;
  input [1:0]JD_OBUF;
  input CLK;
  input \q_reg[0] ;
  input [0:0]A;
  input [319:0]\q_reg[319] ;
  input [319:0]\q_reg[319]_0 ;
  input \q_reg[0]_0 ;
  input [3:0]Q;

  wire [0:0]A;
  wire CLK;
  wire [23:0]D;
  wire [1:0]JD_OBUF;
  wire [23:3]PCIN;
  wire [3:0]Q;
  wire crs_reg_n_0;
  wire crs_reg_n_1;
  wire crs_reg_n_2;
  wire crs_reg_n_3;
  wire d0_n_100;
  wire d0_n_101;
  wire d0_n_102;
  wire d0_n_103;
  wire d0_n_104;
  wire d0_n_105;
  wire d0_n_82;
  wire d0_n_83;
  wire d0_n_84;
  wire d0_n_85;
  wire d0_n_86;
  wire d0_n_87;
  wire d0_n_88;
  wire d0_n_89;
  wire d0_n_90;
  wire d0_n_91;
  wire d0_n_92;
  wire d0_n_93;
  wire d0_n_94;
  wire d0_n_95;
  wire d0_n_96;
  wire d0_n_97;
  wire d0_n_98;
  wire d0_n_99;
  wire d3_n_100;
  wire d3_n_101;
  wire d3_n_102;
  wire d3_n_103;
  wire d3_n_104;
  wire d3_n_105;
  wire d3_n_82;
  wire d3_n_83;
  wire d3_n_84;
  wire d3_n_85;
  wire d3_n_86;
  wire d3_n_87;
  wire d3_n_88;
  wire d3_n_89;
  wire d3_n_90;
  wire d3_n_91;
  wire d3_n_92;
  wire d3_n_93;
  wire d3_n_94;
  wire d3_n_95;
  wire d3_n_96;
  wire d3_n_97;
  wire d3_n_98;
  wire d3_n_99;
  wire max_cnt;
  wire [0:0]pre_bit_cnt;
  wire pre_reg_n_0;
  wire pre_reg_n_1;
  wire pre_reg_n_10;
  wire pre_reg_n_11;
  wire pre_reg_n_12;
  wire pre_reg_n_13;
  wire pre_reg_n_14;
  wire pre_reg_n_15;
  wire pre_reg_n_16;
  wire pre_reg_n_17;
  wire pre_reg_n_18;
  wire pre_reg_n_19;
  wire pre_reg_n_2;
  wire pre_reg_n_20;
  wire pre_reg_n_21;
  wire pre_reg_n_22;
  wire pre_reg_n_23;
  wire pre_reg_n_24;
  wire pre_reg_n_25;
  wire pre_reg_n_26;
  wire pre_reg_n_27;
  wire pre_reg_n_28;
  wire pre_reg_n_29;
  wire pre_reg_n_3;
  wire pre_reg_n_30;
  wire pre_reg_n_31;
  wire pre_reg_n_32;
  wire pre_reg_n_4;
  wire pre_reg_n_5;
  wire pre_reg_n_6;
  wire pre_reg_n_7;
  wire pre_reg_n_8;
  wire pre_reg_n_9;
  wire pre_tdl_bit_cnt_n_1;
  wire pre_tdl_bit_cnt_n_10;
  wire pre_tdl_bit_cnt_n_11;
  wire pre_tdl_bit_cnt_n_2;
  wire pre_tdl_bit_cnt_n_3;
  wire pre_tdl_bit_cnt_n_4;
  wire pre_tdl_bit_cnt_n_5;
  wire pre_tdl_bit_cnt_n_6;
  wire pre_tdl_bit_cnt_n_7;
  wire pre_tdl_bit_cnt_n_8;
  wire pre_tdl_bit_cnt_n_9;
  wire pre_trig;
  wire [0:0]pst_bit_cnt;
  wire pst_reg_n_0;
  wire pst_reg_n_1;
  wire pst_reg_n_10;
  wire pst_reg_n_11;
  wire pst_reg_n_12;
  wire pst_reg_n_13;
  wire pst_reg_n_14;
  wire pst_reg_n_15;
  wire pst_reg_n_16;
  wire pst_reg_n_17;
  wire pst_reg_n_18;
  wire pst_reg_n_19;
  wire pst_reg_n_2;
  wire pst_reg_n_20;
  wire pst_reg_n_21;
  wire pst_reg_n_22;
  wire pst_reg_n_23;
  wire pst_reg_n_24;
  wire pst_reg_n_25;
  wire pst_reg_n_26;
  wire pst_reg_n_27;
  wire pst_reg_n_28;
  wire pst_reg_n_29;
  wire pst_reg_n_3;
  wire pst_reg_n_30;
  wire pst_reg_n_31;
  wire pst_reg_n_32;
  wire pst_reg_n_4;
  wire pst_reg_n_5;
  wire pst_reg_n_6;
  wire pst_reg_n_7;
  wire pst_reg_n_8;
  wire pst_reg_n_9;
  wire pst_tdl_bit_cnt_n_0;
  wire pst_tdl_bit_cnt_n_1;
  wire pst_tdl_bit_cnt_n_10;
  wire pst_tdl_bit_cnt_n_11;
  wire pst_tdl_bit_cnt_n_2;
  wire pst_tdl_bit_cnt_n_3;
  wire pst_tdl_bit_cnt_n_4;
  wire pst_tdl_bit_cnt_n_5;
  wire pst_tdl_bit_cnt_n_6;
  wire pst_tdl_bit_cnt_n_7;
  wire pst_tdl_bit_cnt_n_8;
  wire pst_trig;
  wire \q_reg[0] ;
  wire \q_reg[0]_0 ;
  wire [319:0]\q_reg[319] ;
  wire [319:0]\q_reg[319]_0 ;
  wire q_reg_i_12_n_0;
  wire q_reg_i_13_n_0;
  wire q_reg_i_14_n_0;
  wire q_reg_i_15_n_0;
  wire q_reg_i_16_n_0;
  wire q_reg_i_28_n_0;
  wire q_reg_i_29_n_0;
  wire q_reg_i_30_n_0;
  wire q_reg_i_31_n_0;
  wire q_reg_i_32_n_0;
  wire q_reg_i_33_n_0;
  wire q_reg_i_34_n_0;
  wire q_reg_i_35_n_0;
  wire q_reg_i_36_n_0;
  wire q_reg_i_37_n_0;
  wire q_reg_i_38_n_0;
  wire q_reg_i_39_n_0;
  wire q_reg_i_40_n_0;
  wire q_reg_i_41_n_0;
  wire NLW_d0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_d0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_d0_OVERFLOW_UNCONNECTED;
  wire NLW_d0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_d0_PATTERNDETECT_UNCONNECTED;
  wire NLW_d0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_d0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_d0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_d0_CARRYOUT_UNCONNECTED;
  wire [47:24]NLW_d0_P_UNCONNECTED;
  wire [47:0]NLW_d0_PCOUT_UNCONNECTED;
  wire NLW_d3_CARRYCASCOUT_UNCONNECTED;
  wire NLW_d3_MULTSIGNOUT_UNCONNECTED;
  wire NLW_d3_OVERFLOW_UNCONNECTED;
  wire NLW_d3_PATTERNBDETECT_UNCONNECTED;
  wire NLW_d3_PATTERNDETECT_UNCONNECTED;
  wire NLW_d3_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_d3_ACOUT_UNCONNECTED;
  wire [17:0]NLW_d3_BCOUT_UNCONNECTED;
  wire [3:0]NLW_d3_CARRYOUT_UNCONNECTED;
  wire [47:24]NLW_d3_P_UNCONNECTED;
  wire [47:0]NLW_d3_PCOUT_UNCONNECTED;
  wire [3:0]NLW_q_reg_i_11_CO_UNCONNECTED;
  wire [3:1]NLW_q_reg_i_11_O_UNCONNECTED;
  wire [2:0]NLW_q_reg_i_12_CO_UNCONNECTED;
  wire [2:0]NLW_q_reg_i_13_CO_UNCONNECTED;
  wire [2:0]NLW_q_reg_i_14_CO_UNCONNECTED;
  wire [2:0]NLW_q_reg_i_15_CO_UNCONNECTED;
  wire [2:0]NLW_q_reg_i_16_CO_UNCONNECTED;

  nbit_register_160 crs_reg
       (.Q({crs_reg_n_0,crs_reg_n_1,crs_reg_n_2,crs_reg_n_3}),
        .\q_reg[0]_0 (\q_reg[0]_0 ),
        .\q_reg[3]_0 (Q));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(0),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    d0
       (.A({pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_4,pre_tdl_bit_cnt_n_5,pre_tdl_bit_cnt_n_6,pre_tdl_bit_cnt_n_7,pre_tdl_bit_cnt_n_8,pre_tdl_bit_cnt_n_9,pre_tdl_bit_cnt_n_10,pre_tdl_bit_cnt_n_11,pre_bit_cnt}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_d0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_d0_BCOUT_UNCONNECTED[17:0]),
        .C({d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_82,d3_n_83,d3_n_84,d3_n_85,d3_n_86,d3_n_87,d3_n_88,d3_n_89,d3_n_90,d3_n_91,d3_n_92,d3_n_93,d3_n_94,d3_n_95,d3_n_96,d3_n_97,d3_n_98,d3_n_99,d3_n_100,d3_n_101,d3_n_102,d3_n_103,d3_n_104,d3_n_105}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_d0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_d0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_d0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b1,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_d0_OVERFLOW_UNCONNECTED),
        .P({NLW_d0_P_UNCONNECTED[47:24],d0_n_82,d0_n_83,d0_n_84,d0_n_85,d0_n_86,d0_n_87,d0_n_88,d0_n_89,d0_n_90,d0_n_91,d0_n_92,d0_n_93,d0_n_94,d0_n_95,d0_n_96,d0_n_97,d0_n_98,d0_n_99,d0_n_100,d0_n_101,d0_n_102,d0_n_103,d0_n_104,d0_n_105}),
        .PATTERNBDETECT(NLW_d0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_d0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_d0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_d0_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-11 {cell *THIS*}}" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(0),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(0),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("TRUE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    d3
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_d3_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,1'b1,1'b1,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b0,1'b1,1'b0,1'b1,1'b0,1'b0,1'b0}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_d3_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_d3_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_d3_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b1),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(\q_reg[0] ),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,crs_reg_n_0,crs_reg_n_1,crs_reg_n_2,crs_reg_n_3}),
        .INMODE({1'b0,1'b1,1'b1,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_d3_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_d3_OVERFLOW_UNCONNECTED),
        .P({NLW_d3_P_UNCONNECTED[47:24],d3_n_82,d3_n_83,d3_n_84,d3_n_85,d3_n_86,d3_n_87,d3_n_88,d3_n_89,d3_n_90,d3_n_91,d3_n_92,d3_n_93,d3_n_94,d3_n_95,d3_n_96,d3_n_97,d3_n_98,d3_n_99,d3_n_100,d3_n_101,d3_n_102,d3_n_103,d3_n_104,d3_n_105}),
        .PATTERNBDETECT(NLW_d3_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_d3_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_d3_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_d3_UNDERFLOW_UNCONNECTED));
  binary_counter__parameterized0 out_reg_trig
       (.CLK(CLK),
        .JD_OBUF(JD_OBUF[1]),
        .max_cnt(max_cnt));
  nbit_register__parameterized1 output_reg
       (.A({pst_tdl_bit_cnt_n_0,pst_tdl_bit_cnt_n_1,pst_tdl_bit_cnt_n_2,pst_tdl_bit_cnt_n_3,pst_tdl_bit_cnt_n_4,pst_tdl_bit_cnt_n_5,pst_tdl_bit_cnt_n_6,pst_tdl_bit_cnt_n_7,pst_tdl_bit_cnt_n_8,pst_bit_cnt}),
        .C({PCIN,d0_n_103,d0_n_104,d0_n_105}),
        .D(D),
        .max_cnt(max_cnt));
  nbit_register__parameterized0 pre_reg
       (.DI({pre_reg_n_6,pre_reg_n_7}),
        .S({pre_reg_n_0,pre_reg_n_1,pre_reg_n_2}),
        .d0({pre_tdl_bit_cnt_n_1,pre_tdl_bit_cnt_n_2}),
        .d0_i_163_0(pre_reg_n_4),
        .d0_i_47_0({pre_reg_n_11,pre_reg_n_12}),
        .d0_i_64_0(pre_reg_n_3),
        .d0_i_75_0(pre_reg_n_21),
        .d0_i_78_0(pre_reg_n_10),
        .d0_i_93_0(pre_reg_n_15),
        .d0_i_96_0(pre_reg_n_14),
        .\q_reg[0]_0 (\q_reg[0] ),
        .\q_reg[109]_0 (pre_reg_n_9),
        .\q_reg[131]_0 (pre_reg_n_23),
        .\q_reg[132]_0 (pre_reg_n_24),
        .\q_reg[137]_0 (pre_reg_n_5),
        .\q_reg[184]_0 (pre_reg_n_29),
        .\q_reg[188]_0 (pre_reg_n_22),
        .\q_reg[19]_0 (pre_reg_n_26),
        .\q_reg[215]_0 (pre_reg_n_30),
        .\q_reg[239]_0 (pre_reg_n_32),
        .\q_reg[247]_0 (pre_reg_n_31),
        .\q_reg[271]_0 (pre_reg_n_8),
        .\q_reg[295]_0 (pre_reg_n_17),
        .\q_reg[295]_1 (pre_reg_n_25),
        .\q_reg[319]_0 (\q_reg[319] ),
        .\q_reg[31]_0 (pre_reg_n_19),
        .\q_reg[34]_0 (pre_reg_n_13),
        .\q_reg[43]_0 (pre_reg_n_18),
        .\q_reg[55]_0 (pre_reg_n_16),
        .\q_reg[57]_0 (pre_reg_n_27),
        .\q_reg[77]_0 (pre_reg_n_28),
        .\q_reg[91]_0 (pre_reg_n_20));
  hammin_weight_cnter pre_tdl_bit_cnt
       (.A({pre_tdl_bit_cnt_n_3,pre_tdl_bit_cnt_n_4,pre_tdl_bit_cnt_n_5,pre_tdl_bit_cnt_n_6,pre_tdl_bit_cnt_n_7,pre_tdl_bit_cnt_n_8,pre_tdl_bit_cnt_n_9,pre_tdl_bit_cnt_n_10,pre_tdl_bit_cnt_n_11}),
        .DI({pre_reg_n_6,pre_reg_n_7}),
        .O(pre_bit_cnt),
        .S({pre_reg_n_0,pre_reg_n_1,pre_reg_n_2}),
        .d0({pre_reg_n_11,pre_reg_n_12}),
        .d0_0(pre_reg_n_26),
        .d0_1(pre_reg_n_19),
        .d0_10(pre_reg_n_18),
        .d0_11(pre_reg_n_17),
        .d0_12(pre_reg_n_16),
        .d0_13(pre_reg_n_13),
        .d0_14(pre_reg_n_15),
        .d0_15(pre_reg_n_14),
        .d0_16(pre_reg_n_20),
        .d0_17(pre_reg_n_21),
        .d0_18(pre_reg_n_10),
        .d0_19(pre_reg_n_8),
        .d0_2(pre_reg_n_28),
        .d0_20(pre_reg_n_9),
        .d0_21(pre_reg_n_23),
        .d0_22(pre_reg_n_25),
        .d0_3(pre_reg_n_27),
        .d0_4(pre_reg_n_24),
        .d0_5(pre_reg_n_22),
        .d0_6(pre_reg_n_29),
        .d0_7(pre_reg_n_32),
        .d0_8(pre_reg_n_30),
        .d0_9(pre_reg_n_31),
        .d0_i_12_0(pre_reg_n_4),
        .d0_i_12_1(pre_reg_n_3),
        .d0_i_12_2(pre_reg_n_5),
        .d0_i_33({pre_tdl_bit_cnt_n_1,pre_tdl_bit_cnt_n_2}));
  FDRE #(
    .INIT(1'b0)) 
    pre_trig_reg
       (.C(CLK),
        .CE(1'b1),
        .D(JD_OBUF[0]),
        .Q(pre_trig),
        .R(1'b0));
  nbit_register__parameterized0_161 pst_reg
       (.DI({pst_reg_n_6,pst_reg_n_7}),
        .S({pst_reg_n_0,pst_reg_n_1,pst_reg_n_2}),
        .q_reg({pst_tdl_bit_cnt_n_10,pst_tdl_bit_cnt_n_11}),
        .\q_reg[0]_0 (\q_reg[0]_0 ),
        .\q_reg[109]_0 (pst_reg_n_9),
        .\q_reg[131]_0 (pst_reg_n_23),
        .\q_reg[132]_0 (pst_reg_n_24),
        .\q_reg[137]_0 (pst_reg_n_5),
        .\q_reg[184]_0 (pst_reg_n_29),
        .\q_reg[188]_0 (pst_reg_n_22),
        .\q_reg[19]_0 (pst_reg_n_26),
        .\q_reg[215]_0 (pst_reg_n_30),
        .\q_reg[239]_0 (pst_reg_n_32),
        .\q_reg[247]_0 (pst_reg_n_31),
        .\q_reg[271]_0 (pst_reg_n_8),
        .\q_reg[295]_0 (pst_reg_n_17),
        .\q_reg[295]_1 (pst_reg_n_25),
        .\q_reg[319]_0 (\q_reg[319]_0 ),
        .\q_reg[31]_0 (pst_reg_n_19),
        .\q_reg[34]_0 (pst_reg_n_13),
        .\q_reg[43]_0 (pst_reg_n_18),
        .\q_reg[55]_0 (pst_reg_n_16),
        .\q_reg[57]_0 (pst_reg_n_27),
        .\q_reg[77]_0 (pst_reg_n_28),
        .\q_reg[91]_0 (pst_reg_n_20),
        .q_reg_i_113_0(pst_reg_n_15),
        .q_reg_i_116_0(pst_reg_n_14),
        .q_reg_i_183_0(pst_reg_n_4),
        .q_reg_i_67_0({pst_reg_n_11,pst_reg_n_12}),
        .q_reg_i_84_0(pst_reg_n_3),
        .q_reg_i_95_0(pst_reg_n_21),
        .q_reg_i_98_0(pst_reg_n_10));
  hammin_weight_cnter_162 pst_tdl_bit_cnt
       (.A({pst_tdl_bit_cnt_n_0,pst_tdl_bit_cnt_n_1,pst_tdl_bit_cnt_n_2,pst_tdl_bit_cnt_n_3,pst_tdl_bit_cnt_n_4,pst_tdl_bit_cnt_n_5,pst_tdl_bit_cnt_n_6,pst_tdl_bit_cnt_n_7,pst_tdl_bit_cnt_n_8,pst_bit_cnt}),
        .DI({pst_reg_n_6,pst_reg_n_7}),
        .S({pst_reg_n_0,pst_reg_n_1,pst_reg_n_2}),
        .q_reg({pst_reg_n_11,pst_reg_n_12}),
        .q_reg_0(pst_reg_n_26),
        .q_reg_1(pst_reg_n_19),
        .q_reg_10(pst_reg_n_18),
        .q_reg_11(pst_reg_n_17),
        .q_reg_12(pst_reg_n_16),
        .q_reg_13(pst_reg_n_13),
        .q_reg_14(pst_reg_n_15),
        .q_reg_15(pst_reg_n_14),
        .q_reg_16(pst_reg_n_20),
        .q_reg_17(pst_reg_n_21),
        .q_reg_18(pst_reg_n_10),
        .q_reg_19(pst_reg_n_8),
        .q_reg_2(pst_reg_n_28),
        .q_reg_20(pst_reg_n_9),
        .q_reg_21(pst_reg_n_23),
        .q_reg_22(pst_reg_n_25),
        .q_reg_3(pst_reg_n_27),
        .q_reg_4(pst_reg_n_24),
        .q_reg_5(pst_reg_n_22),
        .q_reg_6(pst_reg_n_29),
        .q_reg_7(pst_reg_n_32),
        .q_reg_8(pst_reg_n_30),
        .q_reg_9(pst_reg_n_31),
        .q_reg_i_18_0(pst_reg_n_4),
        .q_reg_i_18_1(pst_reg_n_3),
        .q_reg_i_18_2(pst_reg_n_5),
        .q_reg_i_53({pst_tdl_bit_cnt_n_10,pst_tdl_bit_cnt_n_11}));
  FDRE #(
    .INIT(1'b0)) 
    pst_trig_reg
       (.C(CLK),
        .CE(1'b1),
        .D(JD_OBUF[1]),
        .Q(pst_trig),
        .R(1'b0));
  CARRY4 q_reg_i_11
       (.CI(q_reg_i_12_n_0),
        .CO(NLW_q_reg_i_11_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_q_reg_i_11_O_UNCONNECTED[3:1],PCIN[23]}),
        .S({1'b0,1'b0,1'b0,q_reg_i_28_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 q_reg_i_12
       (.CI(q_reg_i_13_n_0),
        .CO({q_reg_i_12_n_0,NLW_q_reg_i_12_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({d0_n_83,d0_n_84,d0_n_85,d0_n_86}),
        .O(PCIN[22:19]),
        .S({q_reg_i_29_n_0,q_reg_i_30_n_0,q_reg_i_31_n_0,q_reg_i_32_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 q_reg_i_13
       (.CI(q_reg_i_14_n_0),
        .CO({q_reg_i_13_n_0,NLW_q_reg_i_13_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({d0_n_87,d0_n_88,d0_n_89,d0_n_90}),
        .O(PCIN[18:15]),
        .S({q_reg_i_33_n_0,q_reg_i_34_n_0,q_reg_i_35_n_0,q_reg_i_36_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 q_reg_i_14
       (.CI(q_reg_i_15_n_0),
        .CO({q_reg_i_14_n_0,NLW_q_reg_i_14_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,d0_n_94}),
        .O(PCIN[14:11]),
        .S({d0_n_91,d0_n_92,d0_n_93,q_reg_i_37_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 q_reg_i_15
       (.CI(q_reg_i_16_n_0),
        .CO({q_reg_i_15_n_0,NLW_q_reg_i_15_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,d0_n_96,1'b0,d0_n_98}),
        .O(PCIN[10:7]),
        .S({d0_n_95,q_reg_i_38_n_0,d0_n_97,q_reg_i_39_n_0}));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 q_reg_i_16
       (.CI(1'b0),
        .CO({q_reg_i_16_n_0,NLW_q_reg_i_16_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({d0_n_99,1'b0,d0_n_101,1'b0}),
        .O(PCIN[6:3]),
        .S({q_reg_i_40_n_0,d0_n_100,q_reg_i_41_n_0,d0_n_102}));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_28
       (.I0(d0_n_82),
        .O(q_reg_i_28_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_29
       (.I0(d0_n_83),
        .O(q_reg_i_29_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_30
       (.I0(d0_n_84),
        .O(q_reg_i_30_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_31
       (.I0(d0_n_85),
        .O(q_reg_i_31_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_32
       (.I0(d0_n_86),
        .O(q_reg_i_32_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_33
       (.I0(d0_n_87),
        .O(q_reg_i_33_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_34
       (.I0(d0_n_88),
        .O(q_reg_i_34_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_35
       (.I0(d0_n_89),
        .O(q_reg_i_35_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_36
       (.I0(d0_n_90),
        .O(q_reg_i_36_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_37
       (.I0(d0_n_94),
        .O(q_reg_i_37_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_38
       (.I0(d0_n_96),
        .O(q_reg_i_38_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_39
       (.I0(d0_n_98),
        .O(q_reg_i_39_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_40
       (.I0(d0_n_99),
        .O(q_reg_i_40_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    q_reg_i_41
       (.I0(d0_n_101),
        .O(q_reg_i_41_n_0));
endmodule

module clk_wiz_0
   (clk_out1,
    reset,
    clk_in1);
  output clk_out1;
  input reset;
  input clk_in1;

  (* IBUF_LOW_PWR *) wire clk_in1;
  wire clk_out1;
  wire reset;

  clk_wiz_0_clk_wiz_0_clk_wiz inst
       (.clk_in1(clk_in1),
        .clk_out1(clk_out1),
        .reset(reset));
endmodule

(* ORIG_REF_NAME = "clk_wiz_0_clk_wiz" *) 
module clk_wiz_0_clk_wiz_0_clk_wiz
   (clk_out1,
    reset,
    clk_in1);
  output clk_out1;
  input reset;
  input clk_in1;

  wire clk_in1;
  wire clk_out1;
  wire clk_out1_clk_wiz_0;
  wire clk_out1_clk_wiz_0_en_clk;
  wire clkfbout_buf_clk_wiz_0;
  wire clkfbout_clk_wiz_0;
  wire locked_int;
  wire reset;
  (* RTL_KEEP = "true" *) (* async_reg = "true" *) wire [7:0]seq_reg1;
  wire NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED;
  wire NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED;
  wire NLW_mmcm_adv_inst_DRDY_UNCONNECTED;
  wire NLW_mmcm_adv_inst_PSDONE_UNCONNECTED;
  wire [15:0]NLW_mmcm_adv_inst_DO_UNCONNECTED;

  (* box_type = "PRIMITIVE" *) 
  BUFG clkf_buf
       (.I(clkfbout_clk_wiz_0),
        .O(clkfbout_buf_clk_wiz_0));
  (* OPT_MODIFIED = "BUFG_OPT" *) 
  (* XILINX_LEGACY_PRIM = "BUFGCE" *) 
  (* XILINX_TRANSFORM_PINMAP = "CE:CE0 I:I0" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFGCTRL #(
    .CE_TYPE_CE0("SYNC"),
    .CE_TYPE_CE1("SYNC"),
    .INIT_OUT(0),
    .PRESELECT_I0("TRUE"),
    .PRESELECT_I1("FALSE"),
    .SIM_DEVICE("7SERIES"),
    .STARTUP_SYNC("FALSE")) 
    clkout1_buf
       (.CE0(seq_reg1[7]),
        .CE1(1'b0),
        .I0(clk_out1_clk_wiz_0),
        .I1(1'b1),
        .IGNORE0(1'b0),
        .IGNORE1(1'b1),
        .O(clk_out1),
        .S0(1'b1),
        .S1(1'b0));
  (* OPT_MODIFIED = "BUFG_OPT" *) 
  (* box_type = "PRIMITIVE" *) 
  BUFH clkout1_buf_en
       (.I(clk_out1_clk_wiz_0),
        .O(clk_out1_clk_wiz_0_en_clk));
  (* OPT_MODIFIED = "MLO BUFG_OPT" *) 
  (* box_type = "PRIMITIVE" *) 
  MMCME2_ADV #(
    .BANDWIDTH("OPTIMIZED"),
    .CLKFBOUT_MULT_F(10.000000),
    .CLKFBOUT_PHASE(0.000000),
    .CLKFBOUT_USE_FINE_PS("FALSE"),
    .CLKIN1_PERIOD(10.000000),
    .CLKIN2_PERIOD(0.000000),
    .CLKOUT0_DIVIDE_F(2.500000),
    .CLKOUT0_DUTY_CYCLE(0.500000),
    .CLKOUT0_PHASE(0.000000),
    .CLKOUT0_USE_FINE_PS("FALSE"),
    .CLKOUT1_DIVIDE(1),
    .CLKOUT1_DUTY_CYCLE(0.500000),
    .CLKOUT1_PHASE(0.000000),
    .CLKOUT1_USE_FINE_PS("FALSE"),
    .CLKOUT2_DIVIDE(1),
    .CLKOUT2_DUTY_CYCLE(0.500000),
    .CLKOUT2_PHASE(0.000000),
    .CLKOUT2_USE_FINE_PS("FALSE"),
    .CLKOUT3_DIVIDE(1),
    .CLKOUT3_DUTY_CYCLE(0.500000),
    .CLKOUT3_PHASE(0.000000),
    .CLKOUT3_USE_FINE_PS("FALSE"),
    .CLKOUT4_CASCADE("FALSE"),
    .CLKOUT4_DIVIDE(1),
    .CLKOUT4_DUTY_CYCLE(0.500000),
    .CLKOUT4_PHASE(0.000000),
    .CLKOUT4_USE_FINE_PS("FALSE"),
    .CLKOUT5_DIVIDE(1),
    .CLKOUT5_DUTY_CYCLE(0.500000),
    .CLKOUT5_PHASE(0.000000),
    .CLKOUT5_USE_FINE_PS("FALSE"),
    .CLKOUT6_DIVIDE(1),
    .CLKOUT6_DUTY_CYCLE(0.500000),
    .CLKOUT6_PHASE(0.000000),
    .CLKOUT6_USE_FINE_PS("FALSE"),
    .COMPENSATION("BUF_IN"),
    .DIVCLK_DIVIDE(1),
    .IS_CLKINSEL_INVERTED(1'b0),
    .IS_PSEN_INVERTED(1'b0),
    .IS_PSINCDEC_INVERTED(1'b0),
    .IS_PWRDWN_INVERTED(1'b0),
    .IS_RST_INVERTED(1'b0),
    .REF_JITTER1(0.010000),
    .REF_JITTER2(0.010000),
    .SS_EN("FALSE"),
    .SS_MODE("CENTER_HIGH"),
    .SS_MOD_PERIOD(10000),
    .STARTUP_WAIT("FALSE")) 
    mmcm_adv_inst
       (.CLKFBIN(clkfbout_buf_clk_wiz_0),
        .CLKFBOUT(clkfbout_clk_wiz_0),
        .CLKFBOUTB(NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED),
        .CLKFBSTOPPED(NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED),
        .CLKIN1(clk_in1),
        .CLKIN2(1'b0),
        .CLKINSEL(1'b1),
        .CLKINSTOPPED(NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED),
        .CLKOUT0(clk_out1_clk_wiz_0),
        .CLKOUT0B(NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED),
        .CLKOUT1(NLW_mmcm_adv_inst_CLKOUT1_UNCONNECTED),
        .CLKOUT1B(NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED),
        .CLKOUT2(NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED),
        .CLKOUT2B(NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED),
        .CLKOUT3(NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED),
        .CLKOUT3B(NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED),
        .CLKOUT4(NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED),
        .CLKOUT5(NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED),
        .CLKOUT6(NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED),
        .DADDR({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DCLK(1'b0),
        .DEN(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DO(NLW_mmcm_adv_inst_DO_UNCONNECTED[15:0]),
        .DRDY(NLW_mmcm_adv_inst_DRDY_UNCONNECTED),
        .DWE(1'b0),
        .LOCKED(locked_int),
        .PSCLK(1'b0),
        .PSDONE(NLW_mmcm_adv_inst_PSDONE_UNCONNECTED),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .PWRDWN(1'b0),
        .RST(reset));
  initial assign \seq_reg1_reg[0] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[0] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(locked_int),
        .Q(seq_reg1[0]));
  initial assign \seq_reg1_reg[1] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[1] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[0]),
        .Q(seq_reg1[1]));
  initial assign \seq_reg1_reg[2] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[2] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[1]),
        .Q(seq_reg1[2]));
  initial assign \seq_reg1_reg[3] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[3] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[2]),
        .Q(seq_reg1[3]));
  initial assign \seq_reg1_reg[4] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[4] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[3]),
        .Q(seq_reg1[4]));
  initial assign \seq_reg1_reg[5] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[5] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[4]),
        .Q(seq_reg1[5]));
  initial assign \seq_reg1_reg[6] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[6] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[5]),
        .Q(seq_reg1[6]));
  initial assign \seq_reg1_reg[7] .notifier = 1'bx;
(* ASYNC_REG *) 
  (* KEEP = "yes" *) 
  FDCE #(
    .INIT(1'b0),
    .XON("FALSE")) 
    \seq_reg1_reg[7] 
       (.C(clk_out1_clk_wiz_0_en_clk),
        .CE(1'b1),
        .CLR(reset),
        .D(seq_reg1[6]),
        .Q(seq_reg1[7]));
endmodule

module hammin_weight_cnter
   (O,
    d0_i_33,
    A,
    d0,
    DI,
    S,
    d0_0,
    d0_1,
    d0_2,
    d0_3,
    d0_4,
    d0_5,
    d0_6,
    d0_7,
    d0_8,
    d0_9,
    d0_10,
    d0_11,
    d0_12,
    d0_13,
    d0_14,
    d0_15,
    d0_16,
    d0_17,
    d0_18,
    d0_19,
    d0_20,
    d0_i_12_0,
    d0_i_12_1,
    d0_i_12_2,
    d0_21,
    d0_22);
  output [0:0]O;
  output [1:0]d0_i_33;
  output [8:0]A;
  input [1:0]d0;
  input [1:0]DI;
  input [2:0]S;
  input d0_0;
  input d0_1;
  input d0_2;
  input d0_3;
  input d0_4;
  input d0_5;
  input d0_6;
  input d0_7;
  input d0_8;
  input d0_9;
  input d0_10;
  input d0_11;
  input d0_12;
  input d0_13;
  input d0_14;
  input d0_15;
  input d0_16;
  input d0_17;
  input d0_18;
  input d0_19;
  input d0_20;
  input d0_i_12_0;
  input d0_i_12_1;
  input d0_i_12_2;
  input d0_21;
  input d0_22;

  wire [8:0]A;
  wire [1:0]DI;
  wire [0:0]O;
  wire [2:0]S;
  wire [1:0]d0;
  wire d0_0;
  wire d0_1;
  wire d0_10;
  wire d0_11;
  wire d0_12;
  wire d0_13;
  wire d0_14;
  wire d0_15;
  wire d0_16;
  wire d0_17;
  wire d0_18;
  wire d0_19;
  wire d0_2;
  wire d0_20;
  wire d0_21;
  wire d0_22;
  wire d0_3;
  wire d0_4;
  wire d0_5;
  wire d0_6;
  wire d0_7;
  wire d0_8;
  wire d0_9;
  wire d0_i_10_n_0;
  wire d0_i_12_0;
  wire d0_i_12_1;
  wire d0_i_12_2;
  wire d0_i_12_n_0;
  wire d0_i_13_n_0;
  wire d0_i_16_n_0;
  wire d0_i_17_n_0;
  wire d0_i_20_n_0;
  wire d0_i_21_n_0;
  wire d0_i_22_n_0;
  wire d0_i_27_n_0;
  wire [1:0]d0_i_33;
  wire [8:1]pre_bit_cnt;
  wire [2:0]NLW_d0_i_10_CO_UNCONNECTED;
  wire [3:1]NLW_d0_i_11_CO_UNCONNECTED;
  wire [3:0]NLW_d0_i_11_O_UNCONNECTED;
  wire [2:0]NLW_d0_i_12_CO_UNCONNECTED;

  LUT4 #(
    .INIT(16'h8A88)) 
    d0_i_1
       (.I0(pre_bit_cnt[8]),
        .I1(pre_bit_cnt[7]),
        .I2(d0_i_13_n_0),
        .I3(pre_bit_cnt[6]),
        .O(A[8]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 d0_i_10
       (.CI(1'b0),
        .CO({d0_i_10_n_0,NLW_d0_i_10_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({d0_i_33,d0_i_16_n_0,d0_i_17_n_0}),
        .O({pre_bit_cnt[3:1],O}),
        .S({d0,d0_i_20_n_0,d0_i_21_n_0}));
  CARRY4 d0_i_11
       (.CI(d0_i_12_n_0),
        .CO({NLW_d0_i_11_CO_UNCONNECTED[3:1],pre_bit_cnt[8]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_d0_i_11_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 d0_i_12
       (.CI(d0_i_10_n_0),
        .CO({d0_i_12_n_0,NLW_d0_i_12_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,d0_i_22_n_0,DI}),
        .O(pre_bit_cnt[7:4]),
        .S({S[2:1],d0_i_27_n_0,S[0]}));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_13
       (.I0(pre_bit_cnt[4]),
        .I1(pre_bit_cnt[2]),
        .I2(pre_bit_cnt[1]),
        .I3(O),
        .I4(pre_bit_cnt[3]),
        .I5(pre_bit_cnt[5]),
        .O(d0_i_13_n_0));
  LUT5 #(
    .INIT(32'h96696996)) 
    d0_i_14
       (.I0(d0_16),
        .I1(d0_17),
        .I2(d0_18),
        .I3(d0_19),
        .I4(d0_20),
        .O(d0_i_33[1]));
  LUT3 #(
    .INIT(8'h96)) 
    d0_i_15
       (.I0(d0_13),
        .I1(d0_14),
        .I2(d0_15),
        .O(d0_i_33[0]));
  LUT3 #(
    .INIT(8'h96)) 
    d0_i_16
       (.I0(d0_10),
        .I1(d0_11),
        .I2(d0_12),
        .O(d0_i_16_n_0));
  LUT6 #(
    .INIT(64'h5555555556AA5656)) 
    d0_i_17
       (.I0(d0_4),
        .I1(d0_5),
        .I2(d0_6),
        .I3(d0_7),
        .I4(d0_8),
        .I5(d0_9),
        .O(d0_i_17_n_0));
  LUT4 #(
    .INIT(16'hBA45)) 
    d0_i_2
       (.I0(pre_bit_cnt[7]),
        .I1(d0_i_13_n_0),
        .I2(pre_bit_cnt[6]),
        .I3(pre_bit_cnt[8]),
        .O(A[7]));
  LUT4 #(
    .INIT(16'h9669)) 
    d0_i_20
       (.I0(d0_i_16_n_0),
        .I1(d0_4),
        .I2(d0_21),
        .I3(d0_22),
        .O(d0_i_20_n_0));
  LUT5 #(
    .INIT(32'h99995999)) 
    d0_i_21
       (.I0(d0_i_17_n_0),
        .I1(d0_0),
        .I2(d0_1),
        .I3(d0_2),
        .I4(d0_3),
        .O(d0_i_21_n_0));
  LUT3 #(
    .INIT(8'h78)) 
    d0_i_22
       (.I0(d0_i_12_0),
        .I1(d0_i_12_1),
        .I2(d0_i_12_2),
        .O(d0_i_22_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    d0_i_27
       (.I0(DI[1]),
        .I1(d0_i_12_0),
        .I2(d0_i_12_1),
        .O(d0_i_27_n_0));
  LUT3 #(
    .INIT(8'hD2)) 
    d0_i_3
       (.I0(pre_bit_cnt[6]),
        .I1(d0_i_13_n_0),
        .I2(pre_bit_cnt[7]),
        .O(A[6]));
  LUT2 #(
    .INIT(4'h6)) 
    d0_i_4
       (.I0(d0_i_13_n_0),
        .I1(pre_bit_cnt[6]),
        .O(A[5]));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    d0_i_5
       (.I0(pre_bit_cnt[4]),
        .I1(pre_bit_cnt[2]),
        .I2(pre_bit_cnt[1]),
        .I3(O),
        .I4(pre_bit_cnt[3]),
        .I5(pre_bit_cnt[5]),
        .O(A[4]));
  LUT5 #(
    .INIT(32'h0001FFFE)) 
    d0_i_6
       (.I0(pre_bit_cnt[3]),
        .I1(O),
        .I2(pre_bit_cnt[1]),
        .I3(pre_bit_cnt[2]),
        .I4(pre_bit_cnt[4]),
        .O(A[3]));
  LUT4 #(
    .INIT(16'h01FE)) 
    d0_i_7
       (.I0(pre_bit_cnt[2]),
        .I1(pre_bit_cnt[1]),
        .I2(O),
        .I3(pre_bit_cnt[3]),
        .O(A[2]));
  LUT3 #(
    .INIT(8'h1E)) 
    d0_i_8
       (.I0(O),
        .I1(pre_bit_cnt[1]),
        .I2(pre_bit_cnt[2]),
        .O(A[1]));
  LUT2 #(
    .INIT(4'h6)) 
    d0_i_9
       (.I0(O),
        .I1(pre_bit_cnt[1]),
        .O(A[0]));
endmodule

(* ORIG_REF_NAME = "hammin_weight_cnter" *) 
module hammin_weight_cnter_162
   (A,
    q_reg_i_53,
    q_reg,
    DI,
    S,
    q_reg_0,
    q_reg_1,
    q_reg_2,
    q_reg_3,
    q_reg_4,
    q_reg_5,
    q_reg_6,
    q_reg_7,
    q_reg_8,
    q_reg_9,
    q_reg_10,
    q_reg_11,
    q_reg_12,
    q_reg_13,
    q_reg_14,
    q_reg_15,
    q_reg_16,
    q_reg_17,
    q_reg_18,
    q_reg_19,
    q_reg_20,
    q_reg_i_18_0,
    q_reg_i_18_1,
    q_reg_i_18_2,
    q_reg_21,
    q_reg_22);
  output [9:0]A;
  output [1:0]q_reg_i_53;
  input [1:0]q_reg;
  input [1:0]DI;
  input [2:0]S;
  input q_reg_0;
  input q_reg_1;
  input q_reg_2;
  input q_reg_3;
  input q_reg_4;
  input q_reg_5;
  input q_reg_6;
  input q_reg_7;
  input q_reg_8;
  input q_reg_9;
  input q_reg_10;
  input q_reg_11;
  input q_reg_12;
  input q_reg_13;
  input q_reg_14;
  input q_reg_15;
  input q_reg_16;
  input q_reg_17;
  input q_reg_18;
  input q_reg_19;
  input q_reg_20;
  input q_reg_i_18_0;
  input q_reg_i_18_1;
  input q_reg_i_18_2;
  input q_reg_21;
  input q_reg_22;

  wire [9:0]A;
  wire [1:0]DI;
  wire [2:0]S;
  wire [8:1]pst_bit_cnt;
  wire [1:0]q_reg;
  wire q_reg_0;
  wire q_reg_1;
  wire q_reg_10;
  wire q_reg_11;
  wire q_reg_12;
  wire q_reg_13;
  wire q_reg_14;
  wire q_reg_15;
  wire q_reg_16;
  wire q_reg_17;
  wire q_reg_18;
  wire q_reg_19;
  wire q_reg_2;
  wire q_reg_20;
  wire q_reg_21;
  wire q_reg_22;
  wire q_reg_3;
  wire q_reg_4;
  wire q_reg_5;
  wire q_reg_6;
  wire q_reg_7;
  wire q_reg_8;
  wire q_reg_9;
  wire q_reg_i_10_n_0;
  wire q_reg_i_18_0;
  wire q_reg_i_18_1;
  wire q_reg_i_18_2;
  wire q_reg_i_18_n_0;
  wire q_reg_i_19_n_0;
  wire q_reg_i_22_n_0;
  wire q_reg_i_23_n_0;
  wire q_reg_i_26_n_0;
  wire q_reg_i_27_n_0;
  wire q_reg_i_42_n_0;
  wire q_reg_i_47_n_0;
  wire [1:0]q_reg_i_53;
  wire [2:0]NLW_q_reg_i_10_CO_UNCONNECTED;
  wire [3:1]NLW_q_reg_i_17_CO_UNCONNECTED;
  wire [3:0]NLW_q_reg_i_17_O_UNCONNECTED;
  wire [2:0]NLW_q_reg_i_18_CO_UNCONNECTED;

  LUT4 #(
    .INIT(16'h8A88)) 
    q_reg_i_1
       (.I0(pst_bit_cnt[8]),
        .I1(pst_bit_cnt[7]),
        .I2(q_reg_i_19_n_0),
        .I3(pst_bit_cnt[6]),
        .O(A[9]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 q_reg_i_10
       (.CI(1'b0),
        .CO({q_reg_i_10_n_0,NLW_q_reg_i_10_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({q_reg_i_53,q_reg_i_22_n_0,q_reg_i_23_n_0}),
        .O({pst_bit_cnt[3:1],A[0]}),
        .S({q_reg,q_reg_i_26_n_0,q_reg_i_27_n_0}));
  CARRY4 q_reg_i_17
       (.CI(q_reg_i_18_n_0),
        .CO({NLW_q_reg_i_17_CO_UNCONNECTED[3:1],pst_bit_cnt[8]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_q_reg_i_17_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 q_reg_i_18
       (.CI(q_reg_i_10_n_0),
        .CO({q_reg_i_18_n_0,NLW_q_reg_i_18_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,q_reg_i_42_n_0,DI}),
        .O(pst_bit_cnt[7:4]),
        .S({S[2:1],q_reg_i_47_n_0,S[0]}));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_19
       (.I0(pst_bit_cnt[4]),
        .I1(pst_bit_cnt[2]),
        .I2(pst_bit_cnt[1]),
        .I3(A[0]),
        .I4(pst_bit_cnt[3]),
        .I5(pst_bit_cnt[5]),
        .O(q_reg_i_19_n_0));
  LUT4 #(
    .INIT(16'hBA45)) 
    q_reg_i_2
       (.I0(pst_bit_cnt[7]),
        .I1(q_reg_i_19_n_0),
        .I2(pst_bit_cnt[6]),
        .I3(pst_bit_cnt[8]),
        .O(A[8]));
  LUT5 #(
    .INIT(32'h96696996)) 
    q_reg_i_20
       (.I0(q_reg_16),
        .I1(q_reg_17),
        .I2(q_reg_18),
        .I3(q_reg_19),
        .I4(q_reg_20),
        .O(q_reg_i_53[1]));
  LUT3 #(
    .INIT(8'h96)) 
    q_reg_i_21
       (.I0(q_reg_13),
        .I1(q_reg_14),
        .I2(q_reg_15),
        .O(q_reg_i_53[0]));
  LUT3 #(
    .INIT(8'h96)) 
    q_reg_i_22
       (.I0(q_reg_10),
        .I1(q_reg_11),
        .I2(q_reg_12),
        .O(q_reg_i_22_n_0));
  LUT6 #(
    .INIT(64'h5555555556AA5656)) 
    q_reg_i_23
       (.I0(q_reg_4),
        .I1(q_reg_5),
        .I2(q_reg_6),
        .I3(q_reg_7),
        .I4(q_reg_8),
        .I5(q_reg_9),
        .O(q_reg_i_23_n_0));
  LUT4 #(
    .INIT(16'h9669)) 
    q_reg_i_26
       (.I0(q_reg_i_22_n_0),
        .I1(q_reg_4),
        .I2(q_reg_21),
        .I3(q_reg_22),
        .O(q_reg_i_26_n_0));
  LUT5 #(
    .INIT(32'h99995999)) 
    q_reg_i_27
       (.I0(q_reg_i_23_n_0),
        .I1(q_reg_0),
        .I2(q_reg_1),
        .I3(q_reg_2),
        .I4(q_reg_3),
        .O(q_reg_i_27_n_0));
  LUT3 #(
    .INIT(8'hD2)) 
    q_reg_i_3
       (.I0(pst_bit_cnt[6]),
        .I1(q_reg_i_19_n_0),
        .I2(pst_bit_cnt[7]),
        .O(A[7]));
  LUT2 #(
    .INIT(4'h6)) 
    q_reg_i_4
       (.I0(q_reg_i_19_n_0),
        .I1(pst_bit_cnt[6]),
        .O(A[6]));
  LUT3 #(
    .INIT(8'h78)) 
    q_reg_i_42
       (.I0(q_reg_i_18_0),
        .I1(q_reg_i_18_1),
        .I2(q_reg_i_18_2),
        .O(q_reg_i_42_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    q_reg_i_47
       (.I0(DI[1]),
        .I1(q_reg_i_18_0),
        .I2(q_reg_i_18_1),
        .O(q_reg_i_47_n_0));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    q_reg_i_5
       (.I0(pst_bit_cnt[4]),
        .I1(pst_bit_cnt[2]),
        .I2(pst_bit_cnt[1]),
        .I3(A[0]),
        .I4(pst_bit_cnt[3]),
        .I5(pst_bit_cnt[5]),
        .O(A[5]));
  LUT5 #(
    .INIT(32'h0001FFFE)) 
    q_reg_i_6
       (.I0(pst_bit_cnt[3]),
        .I1(A[0]),
        .I2(pst_bit_cnt[1]),
        .I3(pst_bit_cnt[2]),
        .I4(pst_bit_cnt[4]),
        .O(A[4]));
  LUT4 #(
    .INIT(16'h01FE)) 
    q_reg_i_7
       (.I0(pst_bit_cnt[2]),
        .I1(pst_bit_cnt[1]),
        .I2(A[0]),
        .I3(pst_bit_cnt[3]),
        .O(A[3]));
  LUT3 #(
    .INIT(8'h1E)) 
    q_reg_i_8
       (.I0(A[0]),
        .I1(pst_bit_cnt[1]),
        .I2(pst_bit_cnt[2]),
        .O(A[2]));
  LUT2 #(
    .INIT(4'h6)) 
    q_reg_i_9
       (.I0(A[0]),
        .I1(pst_bit_cnt[1]),
        .O(A[1]));
endmodule

module n4bit_carry_chain
   (\q_reg[0] ,
    JD_OBUF,
    CLK);
  output [319:0]\q_reg[0] ;
  input [0:0]JD_OBUF;
  input CLK;

  wire CLK;
  wire [0:0]JD_OBUF;
  wire [3:0]carry_chain_0;
  wire [3:0]carry_chain_100;
  wire [3:0]carry_chain_104;
  wire [3:0]carry_chain_108;
  wire [3:0]carry_chain_112;
  wire [3:0]carry_chain_116;
  wire [3:0]carry_chain_12;
  wire [3:0]carry_chain_120;
  wire [3:0]carry_chain_124;
  wire [3:0]carry_chain_128;
  wire [3:0]carry_chain_132;
  wire [3:0]carry_chain_136;
  wire [3:0]carry_chain_140;
  wire [3:0]carry_chain_144;
  wire [3:0]carry_chain_148;
  wire [3:0]carry_chain_152;
  wire [3:0]carry_chain_156;
  wire [3:0]carry_chain_16;
  wire [3:0]carry_chain_160;
  wire [3:0]carry_chain_164;
  wire [3:0]carry_chain_168;
  wire [3:0]carry_chain_172;
  wire [3:0]carry_chain_176;
  wire [3:0]carry_chain_180;
  wire [3:0]carry_chain_184;
  wire [3:0]carry_chain_188;
  wire [3:0]carry_chain_192;
  wire [3:0]carry_chain_196;
  wire [3:0]carry_chain_20;
  wire [3:0]carry_chain_200;
  wire [3:0]carry_chain_204;
  wire [3:0]carry_chain_208;
  wire [3:0]carry_chain_212;
  wire [3:0]carry_chain_216;
  wire [3:0]carry_chain_220;
  wire [3:0]carry_chain_224;
  wire [3:0]carry_chain_228;
  wire [3:0]carry_chain_232;
  wire [3:0]carry_chain_236;
  wire [3:0]carry_chain_24;
  wire [3:0]carry_chain_240;
  wire [3:0]carry_chain_244;
  wire [3:0]carry_chain_248;
  wire [3:0]carry_chain_252;
  wire [3:0]carry_chain_256;
  wire [3:0]carry_chain_260;
  wire [3:0]carry_chain_264;
  wire [3:0]carry_chain_268;
  wire [3:0]carry_chain_272;
  wire [3:0]carry_chain_276;
  wire [3:0]carry_chain_28;
  wire [3:0]carry_chain_280;
  wire [3:0]carry_chain_284;
  wire [3:0]carry_chain_288;
  wire [3:0]carry_chain_292;
  wire [3:0]carry_chain_296;
  wire [3:0]carry_chain_300;
  wire [3:0]carry_chain_304;
  wire [3:0]carry_chain_308;
  wire [3:0]carry_chain_312;
  wire [3:0]carry_chain_316;
  wire [3:0]carry_chain_32;
  wire [3:0]carry_chain_36;
  wire [3:0]carry_chain_4;
  wire [3:0]carry_chain_40;
  wire [3:0]carry_chain_44;
  wire [3:0]carry_chain_48;
  wire [3:0]carry_chain_52;
  wire [3:0]carry_chain_56;
  wire [3:0]carry_chain_60;
  wire [3:0]carry_chain_64;
  wire [3:0]carry_chain_68;
  wire [3:0]carry_chain_72;
  wire [3:0]carry_chain_76;
  wire [3:0]carry_chain_8;
  wire [3:0]carry_chain_80;
  wire [3:0]carry_chain_84;
  wire [3:0]carry_chain_88;
  wire [3:0]carry_chain_92;
  wire [3:0]carry_chain_96;
  wire [319:0]\q_reg[0] ;
  wire [3:0]NLW_CARRY4_inst_O_UNCONNECTED;
  wire [3:0]\NLW_tdl_gen[10].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[11].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[12].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[13].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[14].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[15].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[16].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[17].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[18].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[19].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[1].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[20].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[21].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[22].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[23].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[24].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[25].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[26].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[27].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[28].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[29].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[2].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[30].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[31].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[32].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[33].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[34].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[35].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[36].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[37].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[38].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[39].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[3].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[40].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[41].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[42].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[43].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[44].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[45].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[46].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[47].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[48].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[49].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[4].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[50].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[51].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[52].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[53].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[54].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[55].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[56].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[57].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[58].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[59].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[5].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[60].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[61].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[62].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[63].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[64].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[65].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[66].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[67].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[68].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[69].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[6].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[70].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[71].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[72].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[73].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[74].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[75].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[76].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[77].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[78].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[79].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[7].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[8].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[9].carry_x_O_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  CARRY4 CARRY4_inst
       (.CI(1'b0),
        .CO(carry_chain_0),
        .CYINIT(JD_OBUF),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_CARRY4_inst_O_UNCONNECTED[3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  nbit_register_80 \register_gen[0].dff 
       (.CLK(CLK),
        .D(carry_chain_0),
        .\q_reg[0]_0 (\q_reg[0] [3:0]));
  nbit_register_81 \register_gen[10].dff 
       (.CLK(CLK),
        .D(carry_chain_40),
        .\q_reg[0]_0 (\q_reg[0] [43:40]));
  nbit_register_82 \register_gen[11].dff 
       (.CLK(CLK),
        .D(carry_chain_44),
        .\q_reg[0]_0 (\q_reg[0] [47:44]));
  nbit_register_83 \register_gen[12].dff 
       (.CLK(CLK),
        .D(carry_chain_48),
        .\q_reg[0]_0 (\q_reg[0] [51:48]));
  nbit_register_84 \register_gen[13].dff 
       (.CLK(CLK),
        .D(carry_chain_52),
        .\q_reg[0]_0 (\q_reg[0] [55:52]));
  nbit_register_85 \register_gen[14].dff 
       (.CLK(CLK),
        .D(carry_chain_56),
        .\q_reg[0]_0 (\q_reg[0] [59:56]));
  nbit_register_86 \register_gen[15].dff 
       (.CLK(CLK),
        .D(carry_chain_60),
        .\q_reg[0]_0 (\q_reg[0] [63:60]));
  nbit_register_87 \register_gen[16].dff 
       (.CLK(CLK),
        .D(carry_chain_64),
        .\q_reg[0]_0 (\q_reg[0] [67:64]));
  nbit_register_88 \register_gen[17].dff 
       (.CLK(CLK),
        .D(carry_chain_68),
        .\q_reg[0]_0 (\q_reg[0] [71:68]));
  nbit_register_89 \register_gen[18].dff 
       (.CLK(CLK),
        .D(carry_chain_72),
        .\q_reg[0]_0 (\q_reg[0] [75:72]));
  nbit_register_90 \register_gen[19].dff 
       (.CLK(CLK),
        .D(carry_chain_76),
        .\q_reg[0]_0 (\q_reg[0] [79:76]));
  nbit_register_91 \register_gen[1].dff 
       (.CLK(CLK),
        .D(carry_chain_4),
        .\q_reg[0]_0 (\q_reg[0] [7:4]));
  nbit_register_92 \register_gen[20].dff 
       (.CLK(CLK),
        .D(carry_chain_80),
        .\q_reg[0]_0 (\q_reg[0] [83:80]));
  nbit_register_93 \register_gen[21].dff 
       (.CLK(CLK),
        .D(carry_chain_84),
        .\q_reg[0]_0 (\q_reg[0] [87:84]));
  nbit_register_94 \register_gen[22].dff 
       (.CLK(CLK),
        .D(carry_chain_88),
        .\q_reg[0]_0 (\q_reg[0] [91:88]));
  nbit_register_95 \register_gen[23].dff 
       (.CLK(CLK),
        .D(carry_chain_92),
        .\q_reg[0]_0 (\q_reg[0] [95:92]));
  nbit_register_96 \register_gen[24].dff 
       (.CLK(CLK),
        .D(carry_chain_96),
        .\q_reg[0]_0 (\q_reg[0] [99:96]));
  nbit_register_97 \register_gen[25].dff 
       (.CLK(CLK),
        .D(carry_chain_100),
        .\q_reg[0]_0 (\q_reg[0] [103:100]));
  nbit_register_98 \register_gen[26].dff 
       (.CLK(CLK),
        .D(carry_chain_104),
        .\q_reg[0]_0 (\q_reg[0] [107:104]));
  nbit_register_99 \register_gen[27].dff 
       (.CLK(CLK),
        .D(carry_chain_108),
        .\q_reg[0]_0 (\q_reg[0] [111:108]));
  nbit_register_100 \register_gen[28].dff 
       (.CLK(CLK),
        .D(carry_chain_112),
        .\q_reg[0]_0 (\q_reg[0] [115:112]));
  nbit_register_101 \register_gen[29].dff 
       (.CLK(CLK),
        .D(carry_chain_116),
        .\q_reg[0]_0 (\q_reg[0] [119:116]));
  nbit_register_102 \register_gen[2].dff 
       (.CLK(CLK),
        .D(carry_chain_8),
        .\q_reg[0]_0 (\q_reg[0] [11:8]));
  nbit_register_103 \register_gen[30].dff 
       (.CLK(CLK),
        .D(carry_chain_120),
        .\q_reg[0]_0 (\q_reg[0] [123:120]));
  nbit_register_104 \register_gen[31].dff 
       (.CLK(CLK),
        .D(carry_chain_124),
        .\q_reg[0]_0 (\q_reg[0] [127:124]));
  nbit_register_105 \register_gen[32].dff 
       (.CLK(CLK),
        .D(carry_chain_128),
        .\q_reg[0]_0 (\q_reg[0] [131:128]));
  nbit_register_106 \register_gen[33].dff 
       (.CLK(CLK),
        .D(carry_chain_132),
        .\q_reg[0]_0 (\q_reg[0] [135:132]));
  nbit_register_107 \register_gen[34].dff 
       (.CLK(CLK),
        .D(carry_chain_136),
        .\q_reg[0]_0 (\q_reg[0] [139:136]));
  nbit_register_108 \register_gen[35].dff 
       (.CLK(CLK),
        .D(carry_chain_140),
        .\q_reg[0]_0 (\q_reg[0] [143:140]));
  nbit_register_109 \register_gen[36].dff 
       (.CLK(CLK),
        .D(carry_chain_144),
        .\q_reg[0]_0 (\q_reg[0] [147:144]));
  nbit_register_110 \register_gen[37].dff 
       (.CLK(CLK),
        .D(carry_chain_148),
        .\q_reg[0]_0 (\q_reg[0] [151:148]));
  nbit_register_111 \register_gen[38].dff 
       (.CLK(CLK),
        .D(carry_chain_152),
        .\q_reg[0]_0 (\q_reg[0] [155:152]));
  nbit_register_112 \register_gen[39].dff 
       (.CLK(CLK),
        .D(carry_chain_156),
        .\q_reg[0]_0 (\q_reg[0] [159:156]));
  nbit_register_113 \register_gen[3].dff 
       (.CLK(CLK),
        .D(carry_chain_12),
        .\q_reg[0]_0 (\q_reg[0] [15:12]));
  nbit_register_114 \register_gen[40].dff 
       (.CLK(CLK),
        .D(carry_chain_160),
        .\q_reg[0]_0 (\q_reg[0] [163:160]));
  nbit_register_115 \register_gen[41].dff 
       (.CLK(CLK),
        .D(carry_chain_164),
        .\q_reg[0]_0 (\q_reg[0] [167:164]));
  nbit_register_116 \register_gen[42].dff 
       (.CLK(CLK),
        .D(carry_chain_168),
        .\q_reg[0]_0 (\q_reg[0] [171:168]));
  nbit_register_117 \register_gen[43].dff 
       (.CLK(CLK),
        .D(carry_chain_172),
        .\q_reg[0]_0 (\q_reg[0] [175:172]));
  nbit_register_118 \register_gen[44].dff 
       (.CLK(CLK),
        .D(carry_chain_176),
        .\q_reg[0]_0 (\q_reg[0] [179:176]));
  nbit_register_119 \register_gen[45].dff 
       (.CLK(CLK),
        .D(carry_chain_180),
        .\q_reg[0]_0 (\q_reg[0] [183:180]));
  nbit_register_120 \register_gen[46].dff 
       (.CLK(CLK),
        .D(carry_chain_184),
        .\q_reg[0]_0 (\q_reg[0] [187:184]));
  nbit_register_121 \register_gen[47].dff 
       (.CLK(CLK),
        .D(carry_chain_188),
        .\q_reg[0]_0 (\q_reg[0] [191:188]));
  nbit_register_122 \register_gen[48].dff 
       (.CLK(CLK),
        .D(carry_chain_192),
        .\q_reg[0]_0 (\q_reg[0] [195:192]));
  nbit_register_123 \register_gen[49].dff 
       (.CLK(CLK),
        .D(carry_chain_196),
        .\q_reg[0]_0 (\q_reg[0] [199:196]));
  nbit_register_124 \register_gen[4].dff 
       (.CLK(CLK),
        .D(carry_chain_16),
        .\q_reg[0]_0 (\q_reg[0] [19:16]));
  nbit_register_125 \register_gen[50].dff 
       (.CLK(CLK),
        .D(carry_chain_200),
        .\q_reg[0]_0 (\q_reg[0] [203:200]));
  nbit_register_126 \register_gen[51].dff 
       (.CLK(CLK),
        .D(carry_chain_204),
        .\q_reg[0]_0 (\q_reg[0] [207:204]));
  nbit_register_127 \register_gen[52].dff 
       (.CLK(CLK),
        .D(carry_chain_208),
        .\q_reg[0]_0 (\q_reg[0] [211:208]));
  nbit_register_128 \register_gen[53].dff 
       (.CLK(CLK),
        .D(carry_chain_212),
        .\q_reg[0]_0 (\q_reg[0] [215:212]));
  nbit_register_129 \register_gen[54].dff 
       (.CLK(CLK),
        .D(carry_chain_216),
        .\q_reg[0]_0 (\q_reg[0] [219:216]));
  nbit_register_130 \register_gen[55].dff 
       (.CLK(CLK),
        .D(carry_chain_220),
        .\q_reg[0]_0 (\q_reg[0] [223:220]));
  nbit_register_131 \register_gen[56].dff 
       (.CLK(CLK),
        .D(carry_chain_224),
        .\q_reg[0]_0 (\q_reg[0] [227:224]));
  nbit_register_132 \register_gen[57].dff 
       (.CLK(CLK),
        .D(carry_chain_228),
        .\q_reg[0]_0 (\q_reg[0] [231:228]));
  nbit_register_133 \register_gen[58].dff 
       (.CLK(CLK),
        .D(carry_chain_232),
        .\q_reg[0]_0 (\q_reg[0] [235:232]));
  nbit_register_134 \register_gen[59].dff 
       (.CLK(CLK),
        .D(carry_chain_236),
        .\q_reg[0]_0 (\q_reg[0] [239:236]));
  nbit_register_135 \register_gen[5].dff 
       (.CLK(CLK),
        .D(carry_chain_20),
        .\q_reg[0]_0 (\q_reg[0] [23:20]));
  nbit_register_136 \register_gen[60].dff 
       (.CLK(CLK),
        .D(carry_chain_240),
        .\q_reg[0]_0 (\q_reg[0] [243:240]));
  nbit_register_137 \register_gen[61].dff 
       (.CLK(CLK),
        .D(carry_chain_244),
        .\q_reg[0]_0 (\q_reg[0] [247:244]));
  nbit_register_138 \register_gen[62].dff 
       (.CLK(CLK),
        .D(carry_chain_248),
        .\q_reg[0]_0 (\q_reg[0] [251:248]));
  nbit_register_139 \register_gen[63].dff 
       (.CLK(CLK),
        .D(carry_chain_252),
        .\q_reg[0]_0 (\q_reg[0] [255:252]));
  nbit_register_140 \register_gen[64].dff 
       (.CLK(CLK),
        .D(carry_chain_256),
        .\q_reg[0]_0 (\q_reg[0] [259:256]));
  nbit_register_141 \register_gen[65].dff 
       (.CLK(CLK),
        .D(carry_chain_260),
        .\q_reg[0]_0 (\q_reg[0] [263:260]));
  nbit_register_142 \register_gen[66].dff 
       (.CLK(CLK),
        .D(carry_chain_264),
        .\q_reg[0]_0 (\q_reg[0] [267:264]));
  nbit_register_143 \register_gen[67].dff 
       (.CLK(CLK),
        .D(carry_chain_268),
        .\q_reg[0]_0 (\q_reg[0] [271:268]));
  nbit_register_144 \register_gen[68].dff 
       (.CLK(CLK),
        .D(carry_chain_272),
        .\q_reg[0]_0 (\q_reg[0] [275:272]));
  nbit_register_145 \register_gen[69].dff 
       (.CLK(CLK),
        .D(carry_chain_276),
        .\q_reg[0]_0 (\q_reg[0] [279:276]));
  nbit_register_146 \register_gen[6].dff 
       (.CLK(CLK),
        .D(carry_chain_24),
        .\q_reg[0]_0 (\q_reg[0] [27:24]));
  nbit_register_147 \register_gen[70].dff 
       (.CLK(CLK),
        .D(carry_chain_280),
        .\q_reg[0]_0 (\q_reg[0] [283:280]));
  nbit_register_148 \register_gen[71].dff 
       (.CLK(CLK),
        .D(carry_chain_284),
        .\q_reg[0]_0 (\q_reg[0] [287:284]));
  nbit_register_149 \register_gen[72].dff 
       (.CLK(CLK),
        .D(carry_chain_288),
        .\q_reg[0]_0 (\q_reg[0] [291:288]));
  nbit_register_150 \register_gen[73].dff 
       (.CLK(CLK),
        .D(carry_chain_292),
        .\q_reg[0]_0 (\q_reg[0] [295:292]));
  nbit_register_151 \register_gen[74].dff 
       (.CLK(CLK),
        .D(carry_chain_296),
        .\q_reg[0]_0 (\q_reg[0] [299:296]));
  nbit_register_152 \register_gen[75].dff 
       (.CLK(CLK),
        .D(carry_chain_300),
        .\q_reg[0]_0 (\q_reg[0] [303:300]));
  nbit_register_153 \register_gen[76].dff 
       (.CLK(CLK),
        .D(carry_chain_304),
        .\q_reg[0]_0 (\q_reg[0] [307:304]));
  nbit_register_154 \register_gen[77].dff 
       (.CLK(CLK),
        .D(carry_chain_308),
        .\q_reg[0]_0 (\q_reg[0] [311:308]));
  nbit_register_155 \register_gen[78].dff 
       (.CLK(CLK),
        .D(carry_chain_312),
        .\q_reg[0]_0 (\q_reg[0] [315:312]));
  nbit_register_156 \register_gen[79].dff 
       (.CLK(CLK),
        .D(carry_chain_316),
        .\q_reg[0]_0 (\q_reg[0] [319:316]));
  nbit_register_157 \register_gen[7].dff 
       (.CLK(CLK),
        .D(carry_chain_28),
        .\q_reg[0]_0 (\q_reg[0] [31:28]));
  nbit_register_158 \register_gen[8].dff 
       (.CLK(CLK),
        .D(carry_chain_32),
        .\q_reg[0]_0 (\q_reg[0] [35:32]));
  nbit_register_159 \register_gen[9].dff 
       (.CLK(CLK),
        .D(carry_chain_36),
        .\q_reg[0]_0 (\q_reg[0] [39:36]));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[10].carry_x 
       (.CI(carry_chain_36[3]),
        .CO(carry_chain_40),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[10].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[11].carry_x 
       (.CI(carry_chain_40[3]),
        .CO(carry_chain_44),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[11].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[12].carry_x 
       (.CI(carry_chain_44[3]),
        .CO(carry_chain_48),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[12].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[13].carry_x 
       (.CI(carry_chain_48[3]),
        .CO(carry_chain_52),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[13].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[14].carry_x 
       (.CI(carry_chain_52[3]),
        .CO(carry_chain_56),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[14].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[15].carry_x 
       (.CI(carry_chain_56[3]),
        .CO(carry_chain_60),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[15].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[16].carry_x 
       (.CI(carry_chain_60[3]),
        .CO(carry_chain_64),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[16].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[17].carry_x 
       (.CI(carry_chain_64[3]),
        .CO(carry_chain_68),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[17].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[18].carry_x 
       (.CI(carry_chain_68[3]),
        .CO(carry_chain_72),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[18].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[19].carry_x 
       (.CI(carry_chain_72[3]),
        .CO(carry_chain_76),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[19].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[1].carry_x 
       (.CI(carry_chain_0[3]),
        .CO(carry_chain_4),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[1].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[20].carry_x 
       (.CI(carry_chain_76[3]),
        .CO(carry_chain_80),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[20].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[21].carry_x 
       (.CI(carry_chain_80[3]),
        .CO(carry_chain_84),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[21].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[22].carry_x 
       (.CI(carry_chain_84[3]),
        .CO(carry_chain_88),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[22].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[23].carry_x 
       (.CI(carry_chain_88[3]),
        .CO(carry_chain_92),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[23].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[24].carry_x 
       (.CI(carry_chain_92[3]),
        .CO(carry_chain_96),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[24].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[25].carry_x 
       (.CI(carry_chain_96[3]),
        .CO(carry_chain_100),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[25].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[26].carry_x 
       (.CI(carry_chain_100[3]),
        .CO(carry_chain_104),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[26].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[27].carry_x 
       (.CI(carry_chain_104[3]),
        .CO(carry_chain_108),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[27].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[28].carry_x 
       (.CI(carry_chain_108[3]),
        .CO(carry_chain_112),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[28].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[29].carry_x 
       (.CI(carry_chain_112[3]),
        .CO(carry_chain_116),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[29].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[2].carry_x 
       (.CI(carry_chain_4[3]),
        .CO(carry_chain_8),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[2].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[30].carry_x 
       (.CI(carry_chain_116[3]),
        .CO(carry_chain_120),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[30].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[31].carry_x 
       (.CI(carry_chain_120[3]),
        .CO(carry_chain_124),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[31].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[32].carry_x 
       (.CI(carry_chain_124[3]),
        .CO(carry_chain_128),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[32].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[33].carry_x 
       (.CI(carry_chain_128[3]),
        .CO(carry_chain_132),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[33].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[34].carry_x 
       (.CI(carry_chain_132[3]),
        .CO(carry_chain_136),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[34].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[35].carry_x 
       (.CI(carry_chain_136[3]),
        .CO(carry_chain_140),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[35].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[36].carry_x 
       (.CI(carry_chain_140[3]),
        .CO(carry_chain_144),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[36].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[37].carry_x 
       (.CI(carry_chain_144[3]),
        .CO(carry_chain_148),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[37].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[38].carry_x 
       (.CI(carry_chain_148[3]),
        .CO(carry_chain_152),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[38].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[39].carry_x 
       (.CI(carry_chain_152[3]),
        .CO(carry_chain_156),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[39].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[3].carry_x 
       (.CI(carry_chain_8[3]),
        .CO(carry_chain_12),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[3].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[40].carry_x 
       (.CI(carry_chain_156[3]),
        .CO(carry_chain_160),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[40].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[41].carry_x 
       (.CI(carry_chain_160[3]),
        .CO(carry_chain_164),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[41].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[42].carry_x 
       (.CI(carry_chain_164[3]),
        .CO(carry_chain_168),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[42].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[43].carry_x 
       (.CI(carry_chain_168[3]),
        .CO(carry_chain_172),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[43].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[44].carry_x 
       (.CI(carry_chain_172[3]),
        .CO(carry_chain_176),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[44].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[45].carry_x 
       (.CI(carry_chain_176[3]),
        .CO(carry_chain_180),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[45].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[46].carry_x 
       (.CI(carry_chain_180[3]),
        .CO(carry_chain_184),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[46].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[47].carry_x 
       (.CI(carry_chain_184[3]),
        .CO(carry_chain_188),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[47].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[48].carry_x 
       (.CI(carry_chain_188[3]),
        .CO(carry_chain_192),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[48].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[49].carry_x 
       (.CI(carry_chain_192[3]),
        .CO(carry_chain_196),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[49].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[4].carry_x 
       (.CI(carry_chain_12[3]),
        .CO(carry_chain_16),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[4].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[50].carry_x 
       (.CI(carry_chain_196[3]),
        .CO(carry_chain_200),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[50].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[51].carry_x 
       (.CI(carry_chain_200[3]),
        .CO(carry_chain_204),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[51].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[52].carry_x 
       (.CI(carry_chain_204[3]),
        .CO(carry_chain_208),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[52].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[53].carry_x 
       (.CI(carry_chain_208[3]),
        .CO(carry_chain_212),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[53].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[54].carry_x 
       (.CI(carry_chain_212[3]),
        .CO(carry_chain_216),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[54].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[55].carry_x 
       (.CI(carry_chain_216[3]),
        .CO(carry_chain_220),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[55].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[56].carry_x 
       (.CI(carry_chain_220[3]),
        .CO(carry_chain_224),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[56].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[57].carry_x 
       (.CI(carry_chain_224[3]),
        .CO(carry_chain_228),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[57].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[58].carry_x 
       (.CI(carry_chain_228[3]),
        .CO(carry_chain_232),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[58].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[59].carry_x 
       (.CI(carry_chain_232[3]),
        .CO(carry_chain_236),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[59].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[5].carry_x 
       (.CI(carry_chain_16[3]),
        .CO(carry_chain_20),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[5].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[60].carry_x 
       (.CI(carry_chain_236[3]),
        .CO(carry_chain_240),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[60].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[61].carry_x 
       (.CI(carry_chain_240[3]),
        .CO(carry_chain_244),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[61].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[62].carry_x 
       (.CI(carry_chain_244[3]),
        .CO(carry_chain_248),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[62].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[63].carry_x 
       (.CI(carry_chain_248[3]),
        .CO(carry_chain_252),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[63].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[64].carry_x 
       (.CI(carry_chain_252[3]),
        .CO(carry_chain_256),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[64].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[65].carry_x 
       (.CI(carry_chain_256[3]),
        .CO(carry_chain_260),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[65].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[66].carry_x 
       (.CI(carry_chain_260[3]),
        .CO(carry_chain_264),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[66].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[67].carry_x 
       (.CI(carry_chain_264[3]),
        .CO(carry_chain_268),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[67].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[68].carry_x 
       (.CI(carry_chain_268[3]),
        .CO(carry_chain_272),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[68].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[69].carry_x 
       (.CI(carry_chain_272[3]),
        .CO(carry_chain_276),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[69].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[6].carry_x 
       (.CI(carry_chain_20[3]),
        .CO(carry_chain_24),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[6].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[70].carry_x 
       (.CI(carry_chain_276[3]),
        .CO(carry_chain_280),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[70].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[71].carry_x 
       (.CI(carry_chain_280[3]),
        .CO(carry_chain_284),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[71].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[72].carry_x 
       (.CI(carry_chain_284[3]),
        .CO(carry_chain_288),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[72].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[73].carry_x 
       (.CI(carry_chain_288[3]),
        .CO(carry_chain_292),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[73].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[74].carry_x 
       (.CI(carry_chain_292[3]),
        .CO(carry_chain_296),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[74].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[75].carry_x 
       (.CI(carry_chain_296[3]),
        .CO(carry_chain_300),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[75].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[76].carry_x 
       (.CI(carry_chain_300[3]),
        .CO(carry_chain_304),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[76].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[77].carry_x 
       (.CI(carry_chain_304[3]),
        .CO(carry_chain_308),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[77].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[78].carry_x 
       (.CI(carry_chain_308[3]),
        .CO(carry_chain_312),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[78].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[79].carry_x 
       (.CI(carry_chain_312[3]),
        .CO(carry_chain_316),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[79].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[7].carry_x 
       (.CI(carry_chain_24[3]),
        .CO(carry_chain_28),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[7].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[8].carry_x 
       (.CI(carry_chain_28[3]),
        .CO(carry_chain_32),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[8].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[9].carry_x 
       (.CI(carry_chain_32[3]),
        .CO(carry_chain_36),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[9].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
endmodule

(* ORIG_REF_NAME = "n4bit_carry_chain" *) 
module n4bit_carry_chain_0
   (\q_reg[0] ,
    JD_OBUF,
    CLK);
  output [319:0]\q_reg[0] ;
  input [0:0]JD_OBUF;
  input CLK;

  wire CLK;
  wire [0:0]JD_OBUF;
  wire [3:0]carry_chain_0;
  wire [3:0]carry_chain_100;
  wire [3:0]carry_chain_104;
  wire [3:0]carry_chain_108;
  wire [3:0]carry_chain_112;
  wire [3:0]carry_chain_116;
  wire [3:0]carry_chain_12;
  wire [3:0]carry_chain_120;
  wire [3:0]carry_chain_124;
  wire [3:0]carry_chain_128;
  wire [3:0]carry_chain_132;
  wire [3:0]carry_chain_136;
  wire [3:0]carry_chain_140;
  wire [3:0]carry_chain_144;
  wire [3:0]carry_chain_148;
  wire [3:0]carry_chain_152;
  wire [3:0]carry_chain_156;
  wire [3:0]carry_chain_16;
  wire [3:0]carry_chain_160;
  wire [3:0]carry_chain_164;
  wire [3:0]carry_chain_168;
  wire [3:0]carry_chain_172;
  wire [3:0]carry_chain_176;
  wire [3:0]carry_chain_180;
  wire [3:0]carry_chain_184;
  wire [3:0]carry_chain_188;
  wire [3:0]carry_chain_192;
  wire [3:0]carry_chain_196;
  wire [3:0]carry_chain_20;
  wire [3:0]carry_chain_200;
  wire [3:0]carry_chain_204;
  wire [3:0]carry_chain_208;
  wire [3:0]carry_chain_212;
  wire [3:0]carry_chain_216;
  wire [3:0]carry_chain_220;
  wire [3:0]carry_chain_224;
  wire [3:0]carry_chain_228;
  wire [3:0]carry_chain_232;
  wire [3:0]carry_chain_236;
  wire [3:0]carry_chain_24;
  wire [3:0]carry_chain_240;
  wire [3:0]carry_chain_244;
  wire [3:0]carry_chain_248;
  wire [3:0]carry_chain_252;
  wire [3:0]carry_chain_256;
  wire [3:0]carry_chain_260;
  wire [3:0]carry_chain_264;
  wire [3:0]carry_chain_268;
  wire [3:0]carry_chain_272;
  wire [3:0]carry_chain_276;
  wire [3:0]carry_chain_28;
  wire [3:0]carry_chain_280;
  wire [3:0]carry_chain_284;
  wire [3:0]carry_chain_288;
  wire [3:0]carry_chain_292;
  wire [3:0]carry_chain_296;
  wire [3:0]carry_chain_300;
  wire [3:0]carry_chain_304;
  wire [3:0]carry_chain_308;
  wire [3:0]carry_chain_312;
  wire [3:0]carry_chain_316;
  wire [3:0]carry_chain_32;
  wire [3:0]carry_chain_36;
  wire [3:0]carry_chain_4;
  wire [3:0]carry_chain_40;
  wire [3:0]carry_chain_44;
  wire [3:0]carry_chain_48;
  wire [3:0]carry_chain_52;
  wire [3:0]carry_chain_56;
  wire [3:0]carry_chain_60;
  wire [3:0]carry_chain_64;
  wire [3:0]carry_chain_68;
  wire [3:0]carry_chain_72;
  wire [3:0]carry_chain_76;
  wire [3:0]carry_chain_8;
  wire [3:0]carry_chain_80;
  wire [3:0]carry_chain_84;
  wire [3:0]carry_chain_88;
  wire [3:0]carry_chain_92;
  wire [3:0]carry_chain_96;
  wire [319:0]\q_reg[0] ;
  wire [3:0]NLW_CARRY4_inst_O_UNCONNECTED;
  wire [3:0]\NLW_tdl_gen[10].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[11].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[12].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[13].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[14].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[15].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[16].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[17].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[18].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[19].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[1].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[20].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[21].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[22].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[23].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[24].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[25].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[26].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[27].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[28].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[29].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[2].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[30].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[31].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[32].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[33].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[34].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[35].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[36].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[37].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[38].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[39].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[3].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[40].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[41].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[42].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[43].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[44].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[45].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[46].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[47].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[48].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[49].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[4].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[50].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[51].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[52].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[53].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[54].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[55].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[56].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[57].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[58].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[59].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[5].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[60].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[61].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[62].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[63].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[64].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[65].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[66].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[67].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[68].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[69].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[6].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[70].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[71].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[72].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[73].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[74].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[75].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[76].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[77].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[78].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[79].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[7].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[8].carry_x_O_UNCONNECTED ;
  wire [3:0]\NLW_tdl_gen[9].carry_x_O_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  CARRY4 CARRY4_inst
       (.CI(1'b0),
        .CO(carry_chain_0),
        .CYINIT(JD_OBUF),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_CARRY4_inst_O_UNCONNECTED[3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  nbit_register \register_gen[0].dff 
       (.CLK(CLK),
        .D(carry_chain_0),
        .\q_reg[0]_0 (\q_reg[0] [3:0]));
  nbit_register_1 \register_gen[10].dff 
       (.CLK(CLK),
        .D(carry_chain_40),
        .\q_reg[0]_0 (\q_reg[0] [43:40]));
  nbit_register_2 \register_gen[11].dff 
       (.CLK(CLK),
        .D(carry_chain_44),
        .\q_reg[0]_0 (\q_reg[0] [47:44]));
  nbit_register_3 \register_gen[12].dff 
       (.CLK(CLK),
        .D(carry_chain_48),
        .\q_reg[0]_0 (\q_reg[0] [51:48]));
  nbit_register_4 \register_gen[13].dff 
       (.CLK(CLK),
        .D(carry_chain_52),
        .\q_reg[0]_0 (\q_reg[0] [55:52]));
  nbit_register_5 \register_gen[14].dff 
       (.CLK(CLK),
        .D(carry_chain_56),
        .\q_reg[0]_0 (\q_reg[0] [59:56]));
  nbit_register_6 \register_gen[15].dff 
       (.CLK(CLK),
        .D(carry_chain_60),
        .\q_reg[0]_0 (\q_reg[0] [63:60]));
  nbit_register_7 \register_gen[16].dff 
       (.CLK(CLK),
        .D(carry_chain_64),
        .\q_reg[0]_0 (\q_reg[0] [67:64]));
  nbit_register_8 \register_gen[17].dff 
       (.CLK(CLK),
        .D(carry_chain_68),
        .\q_reg[0]_0 (\q_reg[0] [71:68]));
  nbit_register_9 \register_gen[18].dff 
       (.CLK(CLK),
        .D(carry_chain_72),
        .\q_reg[0]_0 (\q_reg[0] [75:72]));
  nbit_register_10 \register_gen[19].dff 
       (.CLK(CLK),
        .D(carry_chain_76),
        .\q_reg[0]_0 (\q_reg[0] [79:76]));
  nbit_register_11 \register_gen[1].dff 
       (.CLK(CLK),
        .D(carry_chain_4),
        .\q_reg[0]_0 (\q_reg[0] [7:4]));
  nbit_register_12 \register_gen[20].dff 
       (.CLK(CLK),
        .D(carry_chain_80),
        .\q_reg[0]_0 (\q_reg[0] [83:80]));
  nbit_register_13 \register_gen[21].dff 
       (.CLK(CLK),
        .D(carry_chain_84),
        .\q_reg[0]_0 (\q_reg[0] [87:84]));
  nbit_register_14 \register_gen[22].dff 
       (.CLK(CLK),
        .D(carry_chain_88),
        .\q_reg[0]_0 (\q_reg[0] [91:88]));
  nbit_register_15 \register_gen[23].dff 
       (.CLK(CLK),
        .D(carry_chain_92),
        .\q_reg[0]_0 (\q_reg[0] [95:92]));
  nbit_register_16 \register_gen[24].dff 
       (.CLK(CLK),
        .D(carry_chain_96),
        .\q_reg[0]_0 (\q_reg[0] [99:96]));
  nbit_register_17 \register_gen[25].dff 
       (.CLK(CLK),
        .D(carry_chain_100),
        .\q_reg[0]_0 (\q_reg[0] [103:100]));
  nbit_register_18 \register_gen[26].dff 
       (.CLK(CLK),
        .D(carry_chain_104),
        .\q_reg[0]_0 (\q_reg[0] [107:104]));
  nbit_register_19 \register_gen[27].dff 
       (.CLK(CLK),
        .D(carry_chain_108),
        .\q_reg[0]_0 (\q_reg[0] [111:108]));
  nbit_register_20 \register_gen[28].dff 
       (.CLK(CLK),
        .D(carry_chain_112),
        .\q_reg[0]_0 (\q_reg[0] [115:112]));
  nbit_register_21 \register_gen[29].dff 
       (.CLK(CLK),
        .D(carry_chain_116),
        .\q_reg[0]_0 (\q_reg[0] [119:116]));
  nbit_register_22 \register_gen[2].dff 
       (.CLK(CLK),
        .D(carry_chain_8),
        .\q_reg[0]_0 (\q_reg[0] [11:8]));
  nbit_register_23 \register_gen[30].dff 
       (.CLK(CLK),
        .D(carry_chain_120),
        .\q_reg[0]_0 (\q_reg[0] [123:120]));
  nbit_register_24 \register_gen[31].dff 
       (.CLK(CLK),
        .D(carry_chain_124),
        .\q_reg[0]_0 (\q_reg[0] [127:124]));
  nbit_register_25 \register_gen[32].dff 
       (.CLK(CLK),
        .D(carry_chain_128),
        .\q_reg[0]_0 (\q_reg[0] [131:128]));
  nbit_register_26 \register_gen[33].dff 
       (.CLK(CLK),
        .D(carry_chain_132),
        .\q_reg[0]_0 (\q_reg[0] [135:132]));
  nbit_register_27 \register_gen[34].dff 
       (.CLK(CLK),
        .D(carry_chain_136),
        .\q_reg[0]_0 (\q_reg[0] [139:136]));
  nbit_register_28 \register_gen[35].dff 
       (.CLK(CLK),
        .D(carry_chain_140),
        .\q_reg[0]_0 (\q_reg[0] [143:140]));
  nbit_register_29 \register_gen[36].dff 
       (.CLK(CLK),
        .D(carry_chain_144),
        .\q_reg[0]_0 (\q_reg[0] [147:144]));
  nbit_register_30 \register_gen[37].dff 
       (.CLK(CLK),
        .D(carry_chain_148),
        .\q_reg[0]_0 (\q_reg[0] [151:148]));
  nbit_register_31 \register_gen[38].dff 
       (.CLK(CLK),
        .D(carry_chain_152),
        .\q_reg[0]_0 (\q_reg[0] [155:152]));
  nbit_register_32 \register_gen[39].dff 
       (.CLK(CLK),
        .D(carry_chain_156),
        .\q_reg[0]_0 (\q_reg[0] [159:156]));
  nbit_register_33 \register_gen[3].dff 
       (.CLK(CLK),
        .D(carry_chain_12),
        .\q_reg[0]_0 (\q_reg[0] [15:12]));
  nbit_register_34 \register_gen[40].dff 
       (.CLK(CLK),
        .D(carry_chain_160),
        .\q_reg[0]_0 (\q_reg[0] [163:160]));
  nbit_register_35 \register_gen[41].dff 
       (.CLK(CLK),
        .D(carry_chain_164),
        .\q_reg[0]_0 (\q_reg[0] [167:164]));
  nbit_register_36 \register_gen[42].dff 
       (.CLK(CLK),
        .D(carry_chain_168),
        .\q_reg[0]_0 (\q_reg[0] [171:168]));
  nbit_register_37 \register_gen[43].dff 
       (.CLK(CLK),
        .D(carry_chain_172),
        .\q_reg[0]_0 (\q_reg[0] [175:172]));
  nbit_register_38 \register_gen[44].dff 
       (.CLK(CLK),
        .D(carry_chain_176),
        .\q_reg[0]_0 (\q_reg[0] [179:176]));
  nbit_register_39 \register_gen[45].dff 
       (.CLK(CLK),
        .D(carry_chain_180),
        .\q_reg[0]_0 (\q_reg[0] [183:180]));
  nbit_register_40 \register_gen[46].dff 
       (.CLK(CLK),
        .D(carry_chain_184),
        .\q_reg[0]_0 (\q_reg[0] [187:184]));
  nbit_register_41 \register_gen[47].dff 
       (.CLK(CLK),
        .D(carry_chain_188),
        .\q_reg[0]_0 (\q_reg[0] [191:188]));
  nbit_register_42 \register_gen[48].dff 
       (.CLK(CLK),
        .D(carry_chain_192),
        .\q_reg[0]_0 (\q_reg[0] [195:192]));
  nbit_register_43 \register_gen[49].dff 
       (.CLK(CLK),
        .D(carry_chain_196),
        .\q_reg[0]_0 (\q_reg[0] [199:196]));
  nbit_register_44 \register_gen[4].dff 
       (.CLK(CLK),
        .D(carry_chain_16),
        .\q_reg[0]_0 (\q_reg[0] [19:16]));
  nbit_register_45 \register_gen[50].dff 
       (.CLK(CLK),
        .D(carry_chain_200),
        .\q_reg[0]_0 (\q_reg[0] [203:200]));
  nbit_register_46 \register_gen[51].dff 
       (.CLK(CLK),
        .D(carry_chain_204),
        .\q_reg[0]_0 (\q_reg[0] [207:204]));
  nbit_register_47 \register_gen[52].dff 
       (.CLK(CLK),
        .D(carry_chain_208),
        .\q_reg[0]_0 (\q_reg[0] [211:208]));
  nbit_register_48 \register_gen[53].dff 
       (.CLK(CLK),
        .D(carry_chain_212),
        .\q_reg[0]_0 (\q_reg[0] [215:212]));
  nbit_register_49 \register_gen[54].dff 
       (.CLK(CLK),
        .D(carry_chain_216),
        .\q_reg[0]_0 (\q_reg[0] [219:216]));
  nbit_register_50 \register_gen[55].dff 
       (.CLK(CLK),
        .D(carry_chain_220),
        .\q_reg[0]_0 (\q_reg[0] [223:220]));
  nbit_register_51 \register_gen[56].dff 
       (.CLK(CLK),
        .D(carry_chain_224),
        .\q_reg[0]_0 (\q_reg[0] [227:224]));
  nbit_register_52 \register_gen[57].dff 
       (.CLK(CLK),
        .D(carry_chain_228),
        .\q_reg[0]_0 (\q_reg[0] [231:228]));
  nbit_register_53 \register_gen[58].dff 
       (.CLK(CLK),
        .D(carry_chain_232),
        .\q_reg[0]_0 (\q_reg[0] [235:232]));
  nbit_register_54 \register_gen[59].dff 
       (.CLK(CLK),
        .D(carry_chain_236),
        .\q_reg[0]_0 (\q_reg[0] [239:236]));
  nbit_register_55 \register_gen[5].dff 
       (.CLK(CLK),
        .D(carry_chain_20),
        .\q_reg[0]_0 (\q_reg[0] [23:20]));
  nbit_register_56 \register_gen[60].dff 
       (.CLK(CLK),
        .D(carry_chain_240),
        .\q_reg[0]_0 (\q_reg[0] [243:240]));
  nbit_register_57 \register_gen[61].dff 
       (.CLK(CLK),
        .D(carry_chain_244),
        .\q_reg[0]_0 (\q_reg[0] [247:244]));
  nbit_register_58 \register_gen[62].dff 
       (.CLK(CLK),
        .D(carry_chain_248),
        .\q_reg[0]_0 (\q_reg[0] [251:248]));
  nbit_register_59 \register_gen[63].dff 
       (.CLK(CLK),
        .D(carry_chain_252),
        .\q_reg[0]_0 (\q_reg[0] [255:252]));
  nbit_register_60 \register_gen[64].dff 
       (.CLK(CLK),
        .D(carry_chain_256),
        .\q_reg[0]_0 (\q_reg[0] [259:256]));
  nbit_register_61 \register_gen[65].dff 
       (.CLK(CLK),
        .D(carry_chain_260),
        .\q_reg[0]_0 (\q_reg[0] [263:260]));
  nbit_register_62 \register_gen[66].dff 
       (.CLK(CLK),
        .D(carry_chain_264),
        .\q_reg[0]_0 (\q_reg[0] [267:264]));
  nbit_register_63 \register_gen[67].dff 
       (.CLK(CLK),
        .D(carry_chain_268),
        .\q_reg[0]_0 (\q_reg[0] [271:268]));
  nbit_register_64 \register_gen[68].dff 
       (.CLK(CLK),
        .D(carry_chain_272),
        .\q_reg[0]_0 (\q_reg[0] [275:272]));
  nbit_register_65 \register_gen[69].dff 
       (.CLK(CLK),
        .D(carry_chain_276),
        .\q_reg[0]_0 (\q_reg[0] [279:276]));
  nbit_register_66 \register_gen[6].dff 
       (.CLK(CLK),
        .D(carry_chain_24),
        .\q_reg[0]_0 (\q_reg[0] [27:24]));
  nbit_register_67 \register_gen[70].dff 
       (.CLK(CLK),
        .D(carry_chain_280),
        .\q_reg[0]_0 (\q_reg[0] [283:280]));
  nbit_register_68 \register_gen[71].dff 
       (.CLK(CLK),
        .D(carry_chain_284),
        .\q_reg[0]_0 (\q_reg[0] [287:284]));
  nbit_register_69 \register_gen[72].dff 
       (.CLK(CLK),
        .D(carry_chain_288),
        .\q_reg[0]_0 (\q_reg[0] [291:288]));
  nbit_register_70 \register_gen[73].dff 
       (.CLK(CLK),
        .D(carry_chain_292),
        .\q_reg[0]_0 (\q_reg[0] [295:292]));
  nbit_register_71 \register_gen[74].dff 
       (.CLK(CLK),
        .D(carry_chain_296),
        .\q_reg[0]_0 (\q_reg[0] [299:296]));
  nbit_register_72 \register_gen[75].dff 
       (.CLK(CLK),
        .D(carry_chain_300),
        .\q_reg[0]_0 (\q_reg[0] [303:300]));
  nbit_register_73 \register_gen[76].dff 
       (.CLK(CLK),
        .D(carry_chain_304),
        .\q_reg[0]_0 (\q_reg[0] [307:304]));
  nbit_register_74 \register_gen[77].dff 
       (.CLK(CLK),
        .D(carry_chain_308),
        .\q_reg[0]_0 (\q_reg[0] [311:308]));
  nbit_register_75 \register_gen[78].dff 
       (.CLK(CLK),
        .D(carry_chain_312),
        .\q_reg[0]_0 (\q_reg[0] [315:312]));
  nbit_register_76 \register_gen[79].dff 
       (.CLK(CLK),
        .D(carry_chain_316),
        .\q_reg[0]_0 (\q_reg[0] [319:316]));
  nbit_register_77 \register_gen[7].dff 
       (.CLK(CLK),
        .D(carry_chain_28),
        .\q_reg[0]_0 (\q_reg[0] [31:28]));
  nbit_register_78 \register_gen[8].dff 
       (.CLK(CLK),
        .D(carry_chain_32),
        .\q_reg[0]_0 (\q_reg[0] [35:32]));
  nbit_register_79 \register_gen[9].dff 
       (.CLK(CLK),
        .D(carry_chain_36),
        .\q_reg[0]_0 (\q_reg[0] [39:36]));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[10].carry_x 
       (.CI(carry_chain_36[3]),
        .CO(carry_chain_40),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[10].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[11].carry_x 
       (.CI(carry_chain_40[3]),
        .CO(carry_chain_44),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[11].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[12].carry_x 
       (.CI(carry_chain_44[3]),
        .CO(carry_chain_48),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[12].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[13].carry_x 
       (.CI(carry_chain_48[3]),
        .CO(carry_chain_52),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[13].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[14].carry_x 
       (.CI(carry_chain_52[3]),
        .CO(carry_chain_56),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[14].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[15].carry_x 
       (.CI(carry_chain_56[3]),
        .CO(carry_chain_60),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[15].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[16].carry_x 
       (.CI(carry_chain_60[3]),
        .CO(carry_chain_64),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[16].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[17].carry_x 
       (.CI(carry_chain_64[3]),
        .CO(carry_chain_68),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[17].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[18].carry_x 
       (.CI(carry_chain_68[3]),
        .CO(carry_chain_72),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[18].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[19].carry_x 
       (.CI(carry_chain_72[3]),
        .CO(carry_chain_76),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[19].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[1].carry_x 
       (.CI(carry_chain_0[3]),
        .CO(carry_chain_4),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[1].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[20].carry_x 
       (.CI(carry_chain_76[3]),
        .CO(carry_chain_80),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[20].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[21].carry_x 
       (.CI(carry_chain_80[3]),
        .CO(carry_chain_84),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[21].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[22].carry_x 
       (.CI(carry_chain_84[3]),
        .CO(carry_chain_88),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[22].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[23].carry_x 
       (.CI(carry_chain_88[3]),
        .CO(carry_chain_92),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[23].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[24].carry_x 
       (.CI(carry_chain_92[3]),
        .CO(carry_chain_96),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[24].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[25].carry_x 
       (.CI(carry_chain_96[3]),
        .CO(carry_chain_100),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[25].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[26].carry_x 
       (.CI(carry_chain_100[3]),
        .CO(carry_chain_104),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[26].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[27].carry_x 
       (.CI(carry_chain_104[3]),
        .CO(carry_chain_108),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[27].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[28].carry_x 
       (.CI(carry_chain_108[3]),
        .CO(carry_chain_112),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[28].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[29].carry_x 
       (.CI(carry_chain_112[3]),
        .CO(carry_chain_116),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[29].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[2].carry_x 
       (.CI(carry_chain_4[3]),
        .CO(carry_chain_8),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[2].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[30].carry_x 
       (.CI(carry_chain_116[3]),
        .CO(carry_chain_120),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[30].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[31].carry_x 
       (.CI(carry_chain_120[3]),
        .CO(carry_chain_124),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[31].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[32].carry_x 
       (.CI(carry_chain_124[3]),
        .CO(carry_chain_128),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[32].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[33].carry_x 
       (.CI(carry_chain_128[3]),
        .CO(carry_chain_132),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[33].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[34].carry_x 
       (.CI(carry_chain_132[3]),
        .CO(carry_chain_136),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[34].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[35].carry_x 
       (.CI(carry_chain_136[3]),
        .CO(carry_chain_140),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[35].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[36].carry_x 
       (.CI(carry_chain_140[3]),
        .CO(carry_chain_144),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[36].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[37].carry_x 
       (.CI(carry_chain_144[3]),
        .CO(carry_chain_148),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[37].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[38].carry_x 
       (.CI(carry_chain_148[3]),
        .CO(carry_chain_152),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[38].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[39].carry_x 
       (.CI(carry_chain_152[3]),
        .CO(carry_chain_156),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[39].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[3].carry_x 
       (.CI(carry_chain_8[3]),
        .CO(carry_chain_12),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[3].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[40].carry_x 
       (.CI(carry_chain_156[3]),
        .CO(carry_chain_160),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[40].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[41].carry_x 
       (.CI(carry_chain_160[3]),
        .CO(carry_chain_164),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[41].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[42].carry_x 
       (.CI(carry_chain_164[3]),
        .CO(carry_chain_168),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[42].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[43].carry_x 
       (.CI(carry_chain_168[3]),
        .CO(carry_chain_172),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[43].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[44].carry_x 
       (.CI(carry_chain_172[3]),
        .CO(carry_chain_176),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[44].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[45].carry_x 
       (.CI(carry_chain_176[3]),
        .CO(carry_chain_180),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[45].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[46].carry_x 
       (.CI(carry_chain_180[3]),
        .CO(carry_chain_184),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[46].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[47].carry_x 
       (.CI(carry_chain_184[3]),
        .CO(carry_chain_188),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[47].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[48].carry_x 
       (.CI(carry_chain_188[3]),
        .CO(carry_chain_192),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[48].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[49].carry_x 
       (.CI(carry_chain_192[3]),
        .CO(carry_chain_196),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[49].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[4].carry_x 
       (.CI(carry_chain_12[3]),
        .CO(carry_chain_16),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[4].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[50].carry_x 
       (.CI(carry_chain_196[3]),
        .CO(carry_chain_200),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[50].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[51].carry_x 
       (.CI(carry_chain_200[3]),
        .CO(carry_chain_204),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[51].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[52].carry_x 
       (.CI(carry_chain_204[3]),
        .CO(carry_chain_208),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[52].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[53].carry_x 
       (.CI(carry_chain_208[3]),
        .CO(carry_chain_212),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[53].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[54].carry_x 
       (.CI(carry_chain_212[3]),
        .CO(carry_chain_216),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[54].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[55].carry_x 
       (.CI(carry_chain_216[3]),
        .CO(carry_chain_220),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[55].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[56].carry_x 
       (.CI(carry_chain_220[3]),
        .CO(carry_chain_224),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[56].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[57].carry_x 
       (.CI(carry_chain_224[3]),
        .CO(carry_chain_228),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[57].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[58].carry_x 
       (.CI(carry_chain_228[3]),
        .CO(carry_chain_232),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[58].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[59].carry_x 
       (.CI(carry_chain_232[3]),
        .CO(carry_chain_236),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[59].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[5].carry_x 
       (.CI(carry_chain_16[3]),
        .CO(carry_chain_20),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[5].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[60].carry_x 
       (.CI(carry_chain_236[3]),
        .CO(carry_chain_240),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[60].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[61].carry_x 
       (.CI(carry_chain_240[3]),
        .CO(carry_chain_244),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[61].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[62].carry_x 
       (.CI(carry_chain_244[3]),
        .CO(carry_chain_248),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[62].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[63].carry_x 
       (.CI(carry_chain_248[3]),
        .CO(carry_chain_252),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[63].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[64].carry_x 
       (.CI(carry_chain_252[3]),
        .CO(carry_chain_256),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[64].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[65].carry_x 
       (.CI(carry_chain_256[3]),
        .CO(carry_chain_260),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[65].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[66].carry_x 
       (.CI(carry_chain_260[3]),
        .CO(carry_chain_264),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[66].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[67].carry_x 
       (.CI(carry_chain_264[3]),
        .CO(carry_chain_268),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[67].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[68].carry_x 
       (.CI(carry_chain_268[3]),
        .CO(carry_chain_272),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[68].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[69].carry_x 
       (.CI(carry_chain_272[3]),
        .CO(carry_chain_276),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[69].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[6].carry_x 
       (.CI(carry_chain_20[3]),
        .CO(carry_chain_24),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[6].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[70].carry_x 
       (.CI(carry_chain_276[3]),
        .CO(carry_chain_280),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[70].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[71].carry_x 
       (.CI(carry_chain_280[3]),
        .CO(carry_chain_284),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[71].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[72].carry_x 
       (.CI(carry_chain_284[3]),
        .CO(carry_chain_288),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[72].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[73].carry_x 
       (.CI(carry_chain_288[3]),
        .CO(carry_chain_292),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[73].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[74].carry_x 
       (.CI(carry_chain_292[3]),
        .CO(carry_chain_296),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[74].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[75].carry_x 
       (.CI(carry_chain_296[3]),
        .CO(carry_chain_300),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[75].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[76].carry_x 
       (.CI(carry_chain_300[3]),
        .CO(carry_chain_304),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[76].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[77].carry_x 
       (.CI(carry_chain_304[3]),
        .CO(carry_chain_308),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[77].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[78].carry_x 
       (.CI(carry_chain_308[3]),
        .CO(carry_chain_312),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[78].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[79].carry_x 
       (.CI(carry_chain_312[3]),
        .CO(carry_chain_316),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[79].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[7].carry_x 
       (.CI(carry_chain_24[3]),
        .CO(carry_chain_28),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[7].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[8].carry_x 
       (.CI(carry_chain_28[3]),
        .CO(carry_chain_32),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[8].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* box_type = "PRIMITIVE" *) 
  CARRY4 \tdl_gen[9].carry_x 
       (.CI(carry_chain_32[3]),
        .CO(carry_chain_36),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_tdl_gen[9].carry_x_O_UNCONNECTED [3:0]),
        .S({1'b1,1'b1,1'b1,1'b1}));
endmodule

module nbit_register
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_1
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_10
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_100
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_101
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_102
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_103
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_104
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_105
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_106
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_107
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_108
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_109
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_11
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_110
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_111
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_112
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_113
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_114
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_115
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_116
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_117
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_118
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_119
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_12
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_120
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_121
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_122
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_123
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_124
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_125
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_126
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_127
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_128
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_129
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_13
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_130
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_131
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_132
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_133
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_134
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_135
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_136
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_137
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_138
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_139
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_14
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_140
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_141
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_142
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_143
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_144
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_145
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_146
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_147
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_148
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_149
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_15
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_150
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_151
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_152
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_153
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_154
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_155
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_156
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_157
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_158
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_159
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_16
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_160
   (Q,
    \q_reg[3]_0 ,
    \q_reg[0]_0 );
  output [3:0]Q;
  input [3:0]\q_reg[3]_0 ;
  input \q_reg[0]_0 ;

  wire [3:0]Q;
  wire \q_reg[0]_0 ;
  wire [3:0]\q_reg[3]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[3]_0 [0]),
        .Q(Q[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[3]_0 [1]),
        .Q(Q[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[3]_0 [2]),
        .Q(Q[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[3]_0 [3]),
        .Q(Q[3]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_17
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_18
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_19
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_2
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_20
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_21
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_22
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_23
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_24
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_25
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_26
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_27
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_28
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_29
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_3
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_30
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_31
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_32
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_33
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_34
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_35
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_36
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_37
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_38
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_39
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_4
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_40
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_41
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_42
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_43
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_44
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_45
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_46
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_47
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_48
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_49
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_5
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_50
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_51
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_52
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_53
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_54
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_55
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_56
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_57
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_58
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_59
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_6
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_60
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_61
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_62
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_63
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_64
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_65
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_66
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_67
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_68
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_69
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_7
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_70
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_71
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_72
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_73
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_74
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_75
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_76
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_77
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_78
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_79
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_8
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_80
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_81
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_82
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_83
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_84
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_85
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_86
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_87
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_88
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_89
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_9
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_90
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_91
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_92
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_93
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_94
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_95
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_96
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_97
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_98
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register_99
   (\q_reg[0]_0 ,
    D,
    CLK);
  output [3:0]\q_reg[0]_0 ;
  input [3:0]D;
  input CLK;

  wire CLK;
  wire [3:0]D;
  wire [3:0]\q_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[0]),
        .Q(\q_reg[0]_0 [3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[1]),
        .Q(\q_reg[0]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[2]),
        .Q(\q_reg[0]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(1'b1),
        .D(D[3]),
        .Q(\q_reg[0]_0 [2]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register__parameterized0
   (S,
    d0_i_64_0,
    d0_i_163_0,
    \q_reg[137]_0 ,
    DI,
    \q_reg[271]_0 ,
    \q_reg[109]_0 ,
    d0_i_78_0,
    d0_i_47_0,
    \q_reg[34]_0 ,
    d0_i_96_0,
    d0_i_93_0,
    \q_reg[55]_0 ,
    \q_reg[295]_0 ,
    \q_reg[43]_0 ,
    \q_reg[31]_0 ,
    \q_reg[91]_0 ,
    d0_i_75_0,
    \q_reg[188]_0 ,
    \q_reg[131]_0 ,
    \q_reg[132]_0 ,
    \q_reg[295]_1 ,
    \q_reg[19]_0 ,
    \q_reg[57]_0 ,
    \q_reg[77]_0 ,
    \q_reg[184]_0 ,
    \q_reg[215]_0 ,
    \q_reg[247]_0 ,
    \q_reg[239]_0 ,
    d0,
    \q_reg[319]_0 ,
    \q_reg[0]_0 );
  output [2:0]S;
  output d0_i_64_0;
  output d0_i_163_0;
  output \q_reg[137]_0 ;
  output [1:0]DI;
  output \q_reg[271]_0 ;
  output \q_reg[109]_0 ;
  output d0_i_78_0;
  output [1:0]d0_i_47_0;
  output \q_reg[34]_0 ;
  output d0_i_96_0;
  output d0_i_93_0;
  output \q_reg[55]_0 ;
  output \q_reg[295]_0 ;
  output \q_reg[43]_0 ;
  output \q_reg[31]_0 ;
  output \q_reg[91]_0 ;
  output d0_i_75_0;
  output \q_reg[188]_0 ;
  output \q_reg[131]_0 ;
  output \q_reg[132]_0 ;
  output \q_reg[295]_1 ;
  output \q_reg[19]_0 ;
  output \q_reg[57]_0 ;
  output \q_reg[77]_0 ;
  output \q_reg[184]_0 ;
  output \q_reg[215]_0 ;
  output \q_reg[247]_0 ;
  output \q_reg[239]_0 ;
  input [1:0]d0;
  input [319:0]\q_reg[319]_0 ;
  input \q_reg[0]_0 ;

  wire [1:0]DI;
  wire [2:0]S;
  wire [1:0]d0;
  wire d0_i_100_n_0;
  wire d0_i_101_n_0;
  wire d0_i_102_n_0;
  wire d0_i_103_n_0;
  wire d0_i_104_n_0;
  wire d0_i_105_n_0;
  wire d0_i_106_n_0;
  wire d0_i_107_n_0;
  wire d0_i_108_n_0;
  wire d0_i_109_n_0;
  wire d0_i_110_n_0;
  wire d0_i_111_n_0;
  wire d0_i_112_n_0;
  wire d0_i_113_n_0;
  wire d0_i_114_n_0;
  wire d0_i_115_n_0;
  wire d0_i_116_n_0;
  wire d0_i_117_n_0;
  wire d0_i_118_n_0;
  wire d0_i_119_n_0;
  wire d0_i_120_n_0;
  wire d0_i_121_n_0;
  wire d0_i_122_n_0;
  wire d0_i_123_n_0;
  wire d0_i_124_n_0;
  wire d0_i_125_n_0;
  wire d0_i_126_n_0;
  wire d0_i_127_n_0;
  wire d0_i_128_n_0;
  wire d0_i_129_n_0;
  wire d0_i_130_n_0;
  wire d0_i_131_n_0;
  wire d0_i_132_n_0;
  wire d0_i_133_n_0;
  wire d0_i_134_n_0;
  wire d0_i_135_n_0;
  wire d0_i_136_n_0;
  wire d0_i_137_n_0;
  wire d0_i_138_n_0;
  wire d0_i_139_n_0;
  wire d0_i_140_n_0;
  wire d0_i_141_n_0;
  wire d0_i_142_n_0;
  wire d0_i_143_n_0;
  wire d0_i_144_n_0;
  wire d0_i_145_n_0;
  wire d0_i_146_n_0;
  wire d0_i_147_n_0;
  wire d0_i_148_n_0;
  wire d0_i_149_n_0;
  wire d0_i_150_n_0;
  wire d0_i_151_n_0;
  wire d0_i_152_n_0;
  wire d0_i_153_n_0;
  wire d0_i_154_n_0;
  wire d0_i_155_n_0;
  wire d0_i_156_n_0;
  wire d0_i_157_n_0;
  wire d0_i_158_n_0;
  wire d0_i_159_n_0;
  wire d0_i_160_n_0;
  wire d0_i_161_n_0;
  wire d0_i_162_n_0;
  wire d0_i_163_0;
  wire d0_i_163_n_0;
  wire d0_i_164_n_0;
  wire d0_i_165_n_0;
  wire d0_i_166_n_0;
  wire d0_i_167_n_0;
  wire d0_i_168_n_0;
  wire d0_i_169_n_0;
  wire d0_i_170_n_0;
  wire d0_i_171_n_0;
  wire d0_i_172_n_0;
  wire d0_i_173_n_0;
  wire d0_i_174_n_0;
  wire d0_i_175_n_0;
  wire d0_i_176_n_0;
  wire d0_i_177_n_0;
  wire d0_i_178_n_0;
  wire d0_i_179_n_0;
  wire d0_i_180_n_0;
  wire d0_i_181_n_0;
  wire d0_i_182_n_0;
  wire d0_i_183_n_0;
  wire d0_i_184_n_0;
  wire d0_i_185_n_0;
  wire d0_i_186_n_0;
  wire d0_i_187_n_0;
  wire d0_i_188_n_0;
  wire d0_i_189_n_0;
  wire d0_i_190_n_0;
  wire d0_i_191_n_0;
  wire d0_i_192_n_0;
  wire d0_i_193_n_0;
  wire d0_i_194_n_0;
  wire d0_i_195_n_0;
  wire d0_i_196_n_0;
  wire d0_i_197_n_0;
  wire d0_i_198_n_0;
  wire d0_i_199_n_0;
  wire d0_i_200_n_0;
  wire d0_i_201_n_0;
  wire d0_i_202_n_0;
  wire d0_i_203_n_0;
  wire d0_i_204_n_0;
  wire d0_i_205_n_0;
  wire d0_i_206_n_0;
  wire d0_i_207_n_0;
  wire d0_i_208_n_0;
  wire d0_i_209_n_0;
  wire d0_i_210_n_0;
  wire d0_i_211_n_0;
  wire d0_i_212_n_0;
  wire d0_i_213_n_0;
  wire d0_i_214_n_0;
  wire d0_i_215_n_0;
  wire d0_i_216_n_0;
  wire d0_i_217_n_0;
  wire d0_i_218_n_0;
  wire d0_i_219_n_0;
  wire d0_i_220_n_0;
  wire d0_i_221_n_0;
  wire d0_i_222_n_0;
  wire d0_i_223_n_0;
  wire d0_i_224_n_0;
  wire d0_i_225_n_0;
  wire d0_i_226_n_0;
  wire d0_i_227_n_0;
  wire d0_i_228_n_0;
  wire d0_i_229_n_0;
  wire d0_i_230_n_0;
  wire d0_i_231_n_0;
  wire d0_i_232_n_0;
  wire d0_i_233_n_0;
  wire d0_i_234_n_0;
  wire d0_i_235_n_0;
  wire d0_i_236_n_0;
  wire d0_i_237_n_0;
  wire d0_i_238_n_0;
  wire d0_i_239_n_0;
  wire d0_i_240_n_0;
  wire d0_i_241_n_0;
  wire d0_i_242_n_0;
  wire d0_i_243_n_0;
  wire d0_i_244_n_0;
  wire d0_i_245_n_0;
  wire d0_i_246_n_0;
  wire d0_i_247_n_0;
  wire d0_i_248_n_0;
  wire d0_i_249_n_0;
  wire d0_i_250_n_0;
  wire d0_i_251_n_0;
  wire d0_i_252_n_0;
  wire d0_i_253_n_0;
  wire d0_i_254_n_0;
  wire d0_i_255_n_0;
  wire d0_i_256_n_0;
  wire d0_i_257_n_0;
  wire d0_i_258_n_0;
  wire d0_i_259_n_0;
  wire d0_i_260_n_0;
  wire d0_i_261_n_0;
  wire d0_i_262_n_0;
  wire d0_i_263_n_0;
  wire d0_i_264_n_0;
  wire d0_i_265_n_0;
  wire d0_i_266_n_0;
  wire d0_i_267_n_0;
  wire d0_i_268_n_0;
  wire d0_i_269_n_0;
  wire d0_i_270_n_0;
  wire d0_i_271_n_0;
  wire d0_i_272_n_0;
  wire d0_i_273_n_0;
  wire d0_i_274_n_0;
  wire d0_i_275_n_0;
  wire d0_i_276_n_0;
  wire d0_i_277_n_0;
  wire d0_i_278_n_0;
  wire d0_i_279_n_0;
  wire d0_i_280_n_0;
  wire d0_i_281_n_0;
  wire d0_i_282_n_0;
  wire d0_i_283_n_0;
  wire d0_i_284_n_0;
  wire d0_i_285_n_0;
  wire d0_i_286_n_0;
  wire d0_i_287_n_0;
  wire d0_i_288_n_0;
  wire d0_i_289_n_0;
  wire d0_i_290_n_0;
  wire d0_i_291_n_0;
  wire d0_i_292_n_0;
  wire d0_i_293_n_0;
  wire d0_i_294_n_0;
  wire d0_i_295_n_0;
  wire d0_i_296_n_0;
  wire d0_i_297_n_0;
  wire d0_i_298_n_0;
  wire d0_i_299_n_0;
  wire d0_i_300_n_0;
  wire d0_i_301_n_0;
  wire d0_i_302_n_0;
  wire d0_i_303_n_0;
  wire d0_i_304_n_0;
  wire d0_i_305_n_0;
  wire d0_i_306_n_0;
  wire d0_i_307_n_0;
  wire d0_i_308_n_0;
  wire d0_i_309_n_0;
  wire d0_i_310_n_0;
  wire d0_i_311_n_0;
  wire d0_i_312_n_0;
  wire d0_i_313_n_0;
  wire d0_i_314_n_0;
  wire d0_i_315_n_0;
  wire d0_i_316_n_0;
  wire d0_i_317_n_0;
  wire d0_i_318_n_0;
  wire d0_i_319_n_0;
  wire d0_i_320_n_0;
  wire d0_i_321_n_0;
  wire d0_i_322_n_0;
  wire d0_i_323_n_0;
  wire d0_i_324_n_0;
  wire d0_i_325_n_0;
  wire d0_i_326_n_0;
  wire d0_i_327_n_0;
  wire d0_i_328_n_0;
  wire d0_i_329_n_0;
  wire d0_i_330_n_0;
  wire d0_i_331_n_0;
  wire d0_i_332_n_0;
  wire d0_i_333_n_0;
  wire d0_i_334_n_0;
  wire d0_i_335_n_0;
  wire d0_i_336_n_0;
  wire d0_i_337_n_0;
  wire d0_i_338_n_0;
  wire d0_i_339_n_0;
  wire d0_i_340_n_0;
  wire d0_i_341_n_0;
  wire d0_i_342_n_0;
  wire d0_i_343_n_0;
  wire d0_i_344_n_0;
  wire d0_i_345_n_0;
  wire d0_i_346_n_0;
  wire d0_i_347_n_0;
  wire d0_i_348_n_0;
  wire d0_i_349_n_0;
  wire d0_i_350_n_0;
  wire d0_i_351_n_0;
  wire d0_i_352_n_0;
  wire d0_i_353_n_0;
  wire d0_i_354_n_0;
  wire d0_i_355_n_0;
  wire d0_i_356_n_0;
  wire d0_i_357_n_0;
  wire d0_i_358_n_0;
  wire d0_i_359_n_0;
  wire d0_i_360_n_0;
  wire d0_i_361_n_0;
  wire d0_i_362_n_0;
  wire d0_i_363_n_0;
  wire d0_i_364_n_0;
  wire d0_i_365_n_0;
  wire d0_i_366_n_0;
  wire d0_i_367_n_0;
  wire d0_i_368_n_0;
  wire d0_i_369_n_0;
  wire d0_i_370_n_0;
  wire d0_i_371_n_0;
  wire d0_i_372_n_0;
  wire d0_i_373_n_0;
  wire d0_i_374_n_0;
  wire d0_i_375_n_0;
  wire d0_i_376_n_0;
  wire d0_i_377_n_0;
  wire d0_i_378_n_0;
  wire d0_i_379_n_0;
  wire d0_i_380_n_0;
  wire d0_i_381_n_0;
  wire d0_i_382_n_0;
  wire d0_i_383_n_0;
  wire d0_i_384_n_0;
  wire d0_i_385_n_0;
  wire d0_i_386_n_0;
  wire d0_i_387_n_0;
  wire d0_i_388_n_0;
  wire d0_i_389_n_0;
  wire d0_i_390_n_0;
  wire d0_i_391_n_0;
  wire d0_i_392_n_0;
  wire d0_i_393_n_0;
  wire d0_i_394_n_0;
  wire d0_i_395_n_0;
  wire d0_i_396_n_0;
  wire d0_i_397_n_0;
  wire d0_i_398_n_0;
  wire d0_i_399_n_0;
  wire d0_i_400_n_0;
  wire d0_i_401_n_0;
  wire d0_i_402_n_0;
  wire d0_i_403_n_0;
  wire d0_i_404_n_0;
  wire d0_i_405_n_0;
  wire d0_i_406_n_0;
  wire d0_i_407_n_0;
  wire d0_i_408_n_0;
  wire d0_i_409_n_0;
  wire d0_i_410_n_0;
  wire d0_i_411_n_0;
  wire d0_i_412_n_0;
  wire d0_i_413_n_0;
  wire d0_i_414_n_0;
  wire d0_i_415_n_0;
  wire d0_i_416_n_0;
  wire d0_i_417_n_0;
  wire d0_i_418_n_0;
  wire d0_i_419_n_0;
  wire d0_i_420_n_0;
  wire d0_i_421_n_0;
  wire d0_i_422_n_0;
  wire d0_i_423_n_0;
  wire d0_i_424_n_0;
  wire d0_i_425_n_0;
  wire d0_i_426_n_0;
  wire d0_i_427_n_0;
  wire d0_i_428_n_0;
  wire d0_i_429_n_0;
  wire d0_i_430_n_0;
  wire d0_i_431_n_0;
  wire d0_i_432_n_0;
  wire d0_i_433_n_0;
  wire d0_i_434_n_0;
  wire d0_i_435_n_0;
  wire d0_i_436_n_0;
  wire d0_i_437_n_0;
  wire d0_i_438_n_0;
  wire d0_i_439_n_0;
  wire d0_i_440_n_0;
  wire d0_i_441_n_0;
  wire d0_i_442_n_0;
  wire d0_i_443_n_0;
  wire d0_i_444_n_0;
  wire d0_i_445_n_0;
  wire d0_i_446_n_0;
  wire d0_i_447_n_0;
  wire d0_i_448_n_0;
  wire d0_i_449_n_0;
  wire d0_i_450_n_0;
  wire d0_i_451_n_0;
  wire d0_i_452_n_0;
  wire d0_i_453_n_0;
  wire d0_i_454_n_0;
  wire d0_i_455_n_0;
  wire d0_i_456_n_0;
  wire d0_i_457_n_0;
  wire d0_i_458_n_0;
  wire d0_i_459_n_0;
  wire d0_i_460_n_0;
  wire d0_i_461_n_0;
  wire d0_i_462_n_0;
  wire d0_i_463_n_0;
  wire d0_i_464_n_0;
  wire d0_i_465_n_0;
  wire d0_i_466_n_0;
  wire d0_i_467_n_0;
  wire d0_i_468_n_0;
  wire d0_i_469_n_0;
  wire d0_i_46_n_0;
  wire d0_i_470_n_0;
  wire d0_i_471_n_0;
  wire d0_i_472_n_0;
  wire d0_i_473_n_0;
  wire d0_i_474_n_0;
  wire d0_i_475_n_0;
  wire d0_i_476_n_0;
  wire d0_i_477_n_0;
  wire d0_i_478_n_0;
  wire d0_i_479_n_0;
  wire [1:0]d0_i_47_0;
  wire d0_i_47_n_0;
  wire d0_i_480_n_0;
  wire d0_i_481_n_0;
  wire d0_i_482_n_0;
  wire d0_i_483_n_0;
  wire d0_i_484_n_0;
  wire d0_i_485_n_0;
  wire d0_i_486_n_0;
  wire d0_i_487_n_0;
  wire d0_i_488_n_0;
  wire d0_i_489_n_0;
  wire d0_i_48_n_0;
  wire d0_i_490_n_0;
  wire d0_i_491_n_0;
  wire d0_i_492_n_0;
  wire d0_i_493_n_0;
  wire d0_i_494_n_0;
  wire d0_i_495_n_0;
  wire d0_i_496_n_0;
  wire d0_i_497_n_0;
  wire d0_i_498_n_0;
  wire d0_i_499_n_0;
  wire d0_i_49_n_0;
  wire d0_i_500_n_0;
  wire d0_i_501_n_0;
  wire d0_i_502_n_0;
  wire d0_i_503_n_0;
  wire d0_i_504_n_0;
  wire d0_i_505_n_0;
  wire d0_i_506_n_0;
  wire d0_i_507_n_0;
  wire d0_i_508_n_0;
  wire d0_i_509_n_0;
  wire d0_i_510_n_0;
  wire d0_i_511_n_0;
  wire d0_i_512_n_0;
  wire d0_i_513_n_0;
  wire d0_i_514_n_0;
  wire d0_i_515_n_0;
  wire d0_i_516_n_0;
  wire d0_i_517_n_0;
  wire d0_i_518_n_0;
  wire d0_i_519_n_0;
  wire d0_i_520_n_0;
  wire d0_i_521_n_0;
  wire d0_i_522_n_0;
  wire d0_i_523_n_0;
  wire d0_i_524_n_0;
  wire d0_i_525_n_0;
  wire d0_i_526_n_0;
  wire d0_i_527_n_0;
  wire d0_i_528_n_0;
  wire d0_i_529_n_0;
  wire d0_i_530_n_0;
  wire d0_i_531_n_0;
  wire d0_i_532_n_0;
  wire d0_i_533_n_0;
  wire d0_i_534_n_0;
  wire d0_i_535_n_0;
  wire d0_i_536_n_0;
  wire d0_i_537_n_0;
  wire d0_i_538_n_0;
  wire d0_i_539_n_0;
  wire d0_i_540_n_0;
  wire d0_i_541_n_0;
  wire d0_i_542_n_0;
  wire d0_i_543_n_0;
  wire d0_i_544_n_0;
  wire d0_i_545_n_0;
  wire d0_i_546_n_0;
  wire d0_i_547_n_0;
  wire d0_i_548_n_0;
  wire d0_i_549_n_0;
  wire d0_i_550_n_0;
  wire d0_i_551_n_0;
  wire d0_i_552_n_0;
  wire d0_i_553_n_0;
  wire d0_i_554_n_0;
  wire d0_i_555_n_0;
  wire d0_i_556_n_0;
  wire d0_i_557_n_0;
  wire d0_i_558_n_0;
  wire d0_i_559_n_0;
  wire d0_i_560_n_0;
  wire d0_i_561_n_0;
  wire d0_i_562_n_0;
  wire d0_i_563_n_0;
  wire d0_i_564_n_0;
  wire d0_i_565_n_0;
  wire d0_i_566_n_0;
  wire d0_i_567_n_0;
  wire d0_i_568_n_0;
  wire d0_i_569_n_0;
  wire d0_i_570_n_0;
  wire d0_i_571_n_0;
  wire d0_i_59_n_0;
  wire d0_i_60_n_0;
  wire d0_i_61_n_0;
  wire d0_i_62_n_0;
  wire d0_i_63_n_0;
  wire d0_i_64_0;
  wire d0_i_64_n_0;
  wire d0_i_65_n_0;
  wire d0_i_66_n_0;
  wire d0_i_67_n_0;
  wire d0_i_68_n_0;
  wire d0_i_69_n_0;
  wire d0_i_70_n_0;
  wire d0_i_71_n_0;
  wire d0_i_72_n_0;
  wire d0_i_73_n_0;
  wire d0_i_74_n_0;
  wire d0_i_75_0;
  wire d0_i_75_n_0;
  wire d0_i_76_n_0;
  wire d0_i_77_n_0;
  wire d0_i_78_0;
  wire d0_i_78_n_0;
  wire d0_i_79_n_0;
  wire d0_i_80_n_0;
  wire d0_i_81_n_0;
  wire d0_i_82_n_0;
  wire d0_i_83_n_0;
  wire d0_i_84_n_0;
  wire d0_i_85_n_0;
  wire d0_i_86_n_0;
  wire d0_i_87_n_0;
  wire d0_i_88_n_0;
  wire d0_i_89_n_0;
  wire d0_i_90_n_0;
  wire d0_i_91_n_0;
  wire d0_i_92_n_0;
  wire d0_i_93_0;
  wire d0_i_93_n_0;
  wire d0_i_94_n_0;
  wire d0_i_95_n_0;
  wire d0_i_96_0;
  wire d0_i_96_n_0;
  wire d0_i_97_n_0;
  wire d0_i_98_n_0;
  wire d0_i_99_n_0;
  wire [319:0]pre_reg_cnt;
  wire \q_reg[0]_0 ;
  wire \q_reg[109]_0 ;
  wire \q_reg[131]_0 ;
  wire \q_reg[132]_0 ;
  wire \q_reg[137]_0 ;
  wire \q_reg[184]_0 ;
  wire \q_reg[188]_0 ;
  wire \q_reg[19]_0 ;
  wire \q_reg[215]_0 ;
  wire \q_reg[239]_0 ;
  wire \q_reg[247]_0 ;
  wire \q_reg[271]_0 ;
  wire \q_reg[295]_0 ;
  wire \q_reg[295]_1 ;
  wire [319:0]\q_reg[319]_0 ;
  wire \q_reg[31]_0 ;
  wire \q_reg[34]_0 ;
  wire \q_reg[43]_0 ;
  wire \q_reg[55]_0 ;
  wire \q_reg[57]_0 ;
  wire \q_reg[77]_0 ;
  wire \q_reg[91]_0 ;

  LUT5 #(
    .INIT(32'h8A88AAAA)) 
    d0_i_100
       (.I0(d0_i_274_n_0),
        .I1(d0_i_275_n_0),
        .I2(pre_reg_cnt[307]),
        .I3(pre_reg_cnt[306]),
        .I4(d0_i_276_n_0),
        .O(d0_i_100_n_0));
  LUT5 #(
    .INIT(32'h8AAA8A8A)) 
    d0_i_101
       (.I0(d0_i_277_n_0),
        .I1(d0_i_71_n_0),
        .I2(d0_i_117_n_0),
        .I3(d0_i_278_n_0),
        .I4(d0_i_128_n_0),
        .O(d0_i_101_n_0));
  LUT4 #(
    .INIT(16'hE0EE)) 
    d0_i_102
       (.I0(d0_i_279_n_0),
        .I1(d0_i_280_n_0),
        .I2(d0_i_144_n_0),
        .I3(d0_i_281_n_0),
        .O(d0_i_102_n_0));
  LUT5 #(
    .INIT(32'hBFBBAAAA)) 
    d0_i_103
       (.I0(d0_i_282_n_0),
        .I1(d0_i_283_n_0),
        .I2(d0_i_242_n_0),
        .I3(d0_i_261_n_0),
        .I4(d0_i_248_n_0),
        .O(d0_i_103_n_0));
  LUT5 #(
    .INIT(32'h0000BABB)) 
    d0_i_104
       (.I0(d0_i_276_n_0),
        .I1(d0_i_284_n_0),
        .I2(pre_reg_cnt[295]),
        .I3(pre_reg_cnt[294]),
        .I4(d0_i_285_n_0),
        .O(d0_i_104_n_0));
  LUT5 #(
    .INIT(32'h00A8AAAA)) 
    d0_i_105
       (.I0(d0_i_134_n_0),
        .I1(d0_i_278_n_0),
        .I2(d0_i_286_n_0),
        .I3(d0_i_287_n_0),
        .I4(d0_i_258_n_0),
        .O(d0_i_105_n_0));
  LUT6 #(
    .INIT(64'h888A8888888A888A)) 
    d0_i_106
       (.I0(d0_i_288_n_0),
        .I1(d0_i_289_n_0),
        .I2(d0_i_237_n_0),
        .I3(d0_i_216_n_0),
        .I4(pre_reg_cnt[55]),
        .I5(pre_reg_cnt[54]),
        .O(d0_i_106_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFF2FFFFFFFF)) 
    d0_i_107
       (.I0(pre_reg_cnt[117]),
        .I1(pre_reg_cnt[118]),
        .I2(pre_reg_cnt[119]),
        .I3(d0_i_200_n_0),
        .I4(d0_i_290_n_0),
        .I5(d0_i_291_n_0),
        .O(d0_i_107_n_0));
  LUT6 #(
    .INIT(64'h0001000000010001)) 
    d0_i_108
       (.I0(d0_i_292_n_0),
        .I1(pre_reg_cnt[101]),
        .I2(pre_reg_cnt[102]),
        .I3(pre_reg_cnt[103]),
        .I4(pre_reg_cnt[100]),
        .I5(pre_reg_cnt[99]),
        .O(d0_i_108_n_0));
  LUT6 #(
    .INIT(64'h0045FFFF00450045)) 
    d0_i_109
       (.I0(d0_i_293_n_0),
        .I1(pre_reg_cnt[247]),
        .I2(pre_reg_cnt[246]),
        .I3(d0_i_294_n_0),
        .I4(d0_i_258_n_0),
        .I5(d0_i_295_n_0),
        .O(d0_i_109_n_0));
  LUT5 #(
    .INIT(32'h0000AAFB)) 
    d0_i_110
       (.I0(d0_i_202_n_0),
        .I1(pre_reg_cnt[132]),
        .I2(pre_reg_cnt[133]),
        .I3(d0_i_296_n_0),
        .I4(d0_i_297_n_0),
        .O(d0_i_110_n_0));
  LUT6 #(
    .INIT(64'h000000A2AAAA00A2)) 
    d0_i_111
       (.I0(d0_i_209_n_0),
        .I1(pre_reg_cnt[141]),
        .I2(pre_reg_cnt[142]),
        .I3(pre_reg_cnt[143]),
        .I4(d0_i_165_n_0),
        .I5(d0_i_211_n_0),
        .O(d0_i_111_n_0));
  LUT6 #(
    .INIT(64'h0100010000000100)) 
    d0_i_112
       (.I0(pre_reg_cnt[158]),
        .I1(pre_reg_cnt[159]),
        .I2(d0_i_298_n_0),
        .I3(d0_i_299_n_0),
        .I4(pre_reg_cnt[156]),
        .I5(pre_reg_cnt[157]),
        .O(d0_i_112_n_0));
  LUT5 #(
    .INIT(32'h00007707)) 
    d0_i_113
       (.I0(d0_i_184_n_0),
        .I1(d0_i_300_n_0),
        .I2(pre_reg_cnt[84]),
        .I3(pre_reg_cnt[85]),
        .I4(d0_i_301_n_0),
        .O(d0_i_113_n_0));
  LUT6 #(
    .INIT(64'h0000000011155555)) 
    d0_i_114
       (.I0(d0_i_302_n_0),
        .I1(d0_i_108_n_0),
        .I2(d0_i_303_n_0),
        .I3(d0_i_304_n_0),
        .I4(d0_i_305_n_0),
        .I5(d0_i_257_n_0),
        .O(d0_i_114_n_0));
  LUT6 #(
    .INIT(64'h000000000000FF0E)) 
    d0_i_115
       (.I0(d0_i_306_n_0),
        .I1(pre_reg_cnt[188]),
        .I2(pre_reg_cnt[189]),
        .I3(pre_reg_cnt[190]),
        .I4(pre_reg_cnt[191]),
        .I5(d0_i_194_n_0),
        .O(d0_i_115_n_0));
  LUT6 #(
    .INIT(64'h00010000FFFFFFFF)) 
    d0_i_116
       (.I0(pre_reg_cnt[194]),
        .I1(pre_reg_cnt[195]),
        .I2(d0_i_307_n_0),
        .I3(d0_i_308_n_0),
        .I4(pre_reg_cnt[193]),
        .I5(d0_i_194_n_0),
        .O(d0_i_116_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    d0_i_117
       (.I0(pre_reg_cnt[209]),
        .I1(d0_i_309_n_0),
        .I2(d0_i_310_n_0),
        .I3(d0_i_311_n_0),
        .I4(pre_reg_cnt[207]),
        .O(d0_i_117_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    d0_i_118
       (.I0(d0_i_311_n_0),
        .I1(pre_reg_cnt[207]),
        .I2(pre_reg_cnt[206]),
        .I3(pre_reg_cnt[205]),
        .I4(pre_reg_cnt[204]),
        .O(d0_i_118_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    d0_i_119
       (.I0(d0_i_310_n_0),
        .I1(pre_reg_cnt[213]),
        .I2(pre_reg_cnt[212]),
        .I3(pre_reg_cnt[211]),
        .I4(pre_reg_cnt[210]),
        .O(d0_i_119_n_0));
  LUT5 #(
    .INIT(32'hFFFF4544)) 
    d0_i_120
       (.I0(d0_i_307_n_0),
        .I1(pre_reg_cnt[199]),
        .I2(pre_reg_cnt[198]),
        .I3(d0_i_312_n_0),
        .I4(d0_i_313_n_0),
        .O(d0_i_120_n_0));
  LUT6 #(
    .INIT(64'h000000000000BBBF)) 
    d0_i_121
       (.I0(d0_i_314_n_0),
        .I1(d0_i_315_n_0),
        .I2(d0_i_316_n_0),
        .I3(d0_i_317_n_0),
        .I4(d0_i_318_n_0),
        .I5(d0_i_319_n_0),
        .O(d0_i_121_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_122
       (.I0(d0_i_194_n_0),
        .I1(d0_i_320_n_0),
        .I2(pre_reg_cnt[187]),
        .I3(pre_reg_cnt[185]),
        .I4(pre_reg_cnt[186]),
        .O(d0_i_122_n_0));
  LUT6 #(
    .INIT(64'h00000000000000F1)) 
    d0_i_123
       (.I0(d0_i_321_n_0),
        .I1(pre_reg_cnt[180]),
        .I2(pre_reg_cnt[181]),
        .I3(d0_i_233_n_0),
        .I4(d0_i_193_n_0),
        .I5(d0_i_194_n_0),
        .O(d0_i_123_n_0));
  LUT5 #(
    .INIT(32'h00002022)) 
    d0_i_124
       (.I0(d0_i_322_n_0),
        .I1(pre_reg_cnt[167]),
        .I2(pre_reg_cnt[166]),
        .I3(pre_reg_cnt[165]),
        .I4(d0_i_323_n_0),
        .O(d0_i_124_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_125
       (.I0(d0_i_324_n_0),
        .I1(d0_i_325_n_0),
        .I2(d0_i_326_n_0),
        .I3(d0_i_327_n_0),
        .I4(d0_i_328_n_0),
        .I5(d0_i_329_n_0),
        .O(d0_i_125_n_0));
  LUT6 #(
    .INIT(64'h00000000FFFF00F2)) 
    d0_i_126
       (.I0(pre_reg_cnt[231]),
        .I1(pre_reg_cnt[232]),
        .I2(pre_reg_cnt[233]),
        .I3(pre_reg_cnt[234]),
        .I4(pre_reg_cnt[235]),
        .I5(d0_i_187_n_0),
        .O(d0_i_126_n_0));
  LUT5 #(
    .INIT(32'h0000BABB)) 
    d0_i_127
       (.I0(d0_i_278_n_0),
        .I1(d0_i_132_n_0),
        .I2(pre_reg_cnt[223]),
        .I3(pre_reg_cnt[222]),
        .I4(d0_i_287_n_0),
        .O(d0_i_127_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    d0_i_128
       (.I0(d0_i_330_n_0),
        .I1(d0_i_132_n_0),
        .I2(pre_reg_cnt[219]),
        .I3(pre_reg_cnt[218]),
        .I4(pre_reg_cnt[217]),
        .I5(pre_reg_cnt[216]),
        .O(d0_i_128_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_129
       (.I0(d0_i_330_n_0),
        .I1(pre_reg_cnt[217]),
        .I2(pre_reg_cnt[216]),
        .I3(pre_reg_cnt[219]),
        .I4(pre_reg_cnt[218]),
        .O(d0_i_129_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_130
       (.I0(pre_reg_cnt[213]),
        .I1(pre_reg_cnt[214]),
        .O(d0_i_130_n_0));
  LUT5 #(
    .INIT(32'hFFFFEEFE)) 
    d0_i_131
       (.I0(pre_reg_cnt[222]),
        .I1(pre_reg_cnt[223]),
        .I2(pre_reg_cnt[219]),
        .I3(pre_reg_cnt[220]),
        .I4(pre_reg_cnt[221]),
        .O(d0_i_131_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_132
       (.I0(d0_i_324_n_0),
        .I1(d0_i_325_n_0),
        .I2(d0_i_326_n_0),
        .I3(d0_i_331_n_0),
        .I4(d0_i_329_n_0),
        .I5(d0_i_332_n_0),
        .O(d0_i_132_n_0));
  LUT5 #(
    .INIT(32'h000000A2)) 
    d0_i_133
       (.I0(d0_i_333_n_0),
        .I1(pre_reg_cnt[273]),
        .I2(pre_reg_cnt[274]),
        .I3(pre_reg_cnt[275]),
        .I4(d0_i_334_n_0),
        .O(d0_i_133_n_0));
  LUT6 #(
    .INIT(64'h00000000AAAAEEAE)) 
    d0_i_134
       (.I0(d0_i_335_n_0),
        .I1(d0_i_336_n_0),
        .I2(pre_reg_cnt[276]),
        .I3(pre_reg_cnt[277]),
        .I4(pre_reg_cnt[278]),
        .I5(d0_i_337_n_0),
        .O(d0_i_134_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF54555454)) 
    d0_i_135
       (.I0(d0_i_274_n_0),
        .I1(d0_i_338_n_0),
        .I2(pre_reg_cnt[287]),
        .I3(pre_reg_cnt[286]),
        .I4(pre_reg_cnt[285]),
        .I5(d0_i_339_n_0),
        .O(d0_i_135_n_0));
  LUT6 #(
    .INIT(64'hABAAABABAAAAAAAA)) 
    d0_i_136
       (.I0(d0_i_340_n_0),
        .I1(d0_i_341_n_0),
        .I2(pre_reg_cnt[311]),
        .I3(pre_reg_cnt[310]),
        .I4(pre_reg_cnt[309]),
        .I5(d0_i_342_n_0),
        .O(d0_i_136_n_0));
  LUT6 #(
    .INIT(64'h00000000FFFFFFFD)) 
    d0_i_137
       (.I0(d0_i_179_n_0),
        .I1(d0_i_343_n_0),
        .I2(pre_reg_cnt[131]),
        .I3(pre_reg_cnt[130]),
        .I4(d0_i_344_n_0),
        .I5(d0_i_345_n_0),
        .O(d0_i_137_n_0));
  LUT6 #(
    .INIT(64'h4444444FFFFFFFFF)) 
    d0_i_138
       (.I0(d0_i_176_n_0),
        .I1(d0_i_251_n_0),
        .I2(d0_i_222_n_0),
        .I3(d0_i_301_n_0),
        .I4(pre_reg_cnt[85]),
        .I5(d0_i_155_n_0),
        .O(d0_i_138_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    d0_i_139
       (.I0(d0_i_346_n_0),
        .I1(d0_i_347_n_0),
        .I2(d0_i_80_n_0),
        .O(d0_i_139_n_0));
  LUT6 #(
    .INIT(64'h00000000AAAAFFFE)) 
    d0_i_140
       (.I0(d0_i_217_n_0),
        .I1(pre_reg_cnt[287]),
        .I2(pre_reg_cnt[286]),
        .I3(d0_i_338_n_0),
        .I4(d0_i_348_n_0),
        .I5(d0_i_349_n_0),
        .O(d0_i_140_n_0));
  LUT6 #(
    .INIT(64'h00000000D0D0D000)) 
    d0_i_141
       (.I0(d0_i_350_n_0),
        .I1(d0_i_221_n_0),
        .I2(d0_i_310_n_0),
        .I3(d0_i_175_n_0),
        .I4(d0_i_351_n_0),
        .I5(d0_i_89_n_0),
        .O(d0_i_141_n_0));
  LUT6 #(
    .INIT(64'h00000057FFFFFFFF)) 
    d0_i_142
       (.I0(d0_i_218_n_0),
        .I1(d0_i_284_n_0),
        .I2(pre_reg_cnt[295]),
        .I3(pre_reg_cnt[301]),
        .I4(d0_i_352_n_0),
        .I5(d0_i_353_n_0),
        .O(d0_i_142_n_0));
  LUT6 #(
    .INIT(64'h000000AE00000000)) 
    d0_i_143
       (.I0(d0_i_248_n_0),
        .I1(d0_i_354_n_0),
        .I2(d0_i_355_n_0),
        .I3(d0_i_356_n_0),
        .I4(d0_i_357_n_0),
        .I5(d0_i_358_n_0),
        .O(d0_i_143_n_0));
  LUT6 #(
    .INIT(64'h00000000000000AB)) 
    d0_i_144
       (.I0(d0_i_359_n_0),
        .I1(d0_i_360_n_0),
        .I2(d0_i_361_n_0),
        .I3(d0_i_362_n_0),
        .I4(d0_i_363_n_0),
        .I5(d0_i_200_n_0),
        .O(d0_i_144_n_0));
  LUT6 #(
    .INIT(64'h11110001FFFFFFFF)) 
    d0_i_145
       (.I0(d0_i_364_n_0),
        .I1(d0_i_365_n_0),
        .I2(d0_i_366_n_0),
        .I3(d0_i_367_n_0),
        .I4(d0_i_289_n_0),
        .I5(d0_i_257_n_0),
        .O(d0_i_145_n_0));
  LUT6 #(
    .INIT(64'h0E0E0E0E0E0F0E0E)) 
    d0_i_146
       (.I0(d0_i_270_n_0),
        .I1(d0_i_368_n_0),
        .I2(d0_i_369_n_0),
        .I3(d0_i_370_n_0),
        .I4(d0_i_371_n_0),
        .I5(d0_i_372_n_0),
        .O(d0_i_146_n_0));
  LUT6 #(
    .INIT(64'h000000000000FFBA)) 
    d0_i_147
       (.I0(d0_i_373_n_0),
        .I1(pre_reg_cnt[19]),
        .I2(pre_reg_cnt[18]),
        .I3(d0_i_374_n_0),
        .I4(d0_i_375_n_0),
        .I5(d0_i_376_n_0),
        .O(d0_i_147_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    d0_i_148
       (.I0(d0_i_269_n_0),
        .I1(pre_reg_cnt[31]),
        .I2(pre_reg_cnt[30]),
        .O(d0_i_148_n_0));
  LUT6 #(
    .INIT(64'h000000000000000E)) 
    d0_i_149
       (.I0(d0_i_377_n_0),
        .I1(d0_i_378_n_0),
        .I2(d0_i_379_n_0),
        .I3(d0_i_380_n_0),
        .I4(d0_i_269_n_0),
        .I5(pre_reg_cnt[31]),
        .O(d0_i_149_n_0));
  LUT6 #(
    .INIT(64'h000000000000FF0E)) 
    d0_i_150
       (.I0(d0_i_381_n_0),
        .I1(pre_reg_cnt[35]),
        .I2(pre_reg_cnt[36]),
        .I3(pre_reg_cnt[37]),
        .I4(d0_i_382_n_0),
        .I5(d0_i_383_n_0),
        .O(d0_i_150_n_0));
  LUT6 #(
    .INIT(64'hFFFBFFFFFFFAFFFF)) 
    d0_i_151
       (.I0(d0_i_384_n_0),
        .I1(pre_reg_cnt[40]),
        .I2(pre_reg_cnt[41]),
        .I3(d0_i_385_n_0),
        .I4(d0_i_386_n_0),
        .I5(pre_reg_cnt[39]),
        .O(d0_i_151_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEF0)) 
    d0_i_152
       (.I0(d0_i_387_n_0),
        .I1(pre_reg_cnt[51]),
        .I2(d0_i_237_n_0),
        .I3(d0_i_388_n_0),
        .I4(d0_i_389_n_0),
        .I5(d0_i_216_n_0),
        .O(d0_i_152_n_0));
  LUT6 #(
    .INIT(64'h0000FF0E00000000)) 
    d0_i_153
       (.I0(d0_i_390_n_0),
        .I1(pre_reg_cnt[44]),
        .I2(pre_reg_cnt[45]),
        .I3(pre_reg_cnt[46]),
        .I4(pre_reg_cnt[47]),
        .I5(d0_i_386_n_0),
        .O(d0_i_153_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF0FE)) 
    d0_i_154
       (.I0(d0_i_391_n_0),
        .I1(pre_reg_cnt[72]),
        .I2(d0_i_392_n_0),
        .I3(pre_reg_cnt[73]),
        .I4(d0_i_393_n_0),
        .I5(d0_i_394_n_0),
        .O(d0_i_154_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_155
       (.I0(pre_reg_cnt[79]),
        .I1(d0_i_395_n_0),
        .I2(d0_i_305_n_0),
        .O(d0_i_155_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_156
       (.I0(pre_reg_cnt[61]),
        .I1(pre_reg_cnt[60]),
        .I2(pre_reg_cnt[63]),
        .I3(pre_reg_cnt[62]),
        .I4(d0_i_216_n_0),
        .O(d0_i_156_n_0));
  LUT6 #(
    .INIT(64'h000000000000CC08)) 
    d0_i_157
       (.I0(d0_i_396_n_0),
        .I1(d0_i_266_n_0),
        .I2(pre_reg_cnt[54]),
        .I3(pre_reg_cnt[55]),
        .I4(d0_i_216_n_0),
        .I5(d0_i_237_n_0),
        .O(d0_i_157_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF4544)) 
    d0_i_158
       (.I0(d0_i_289_n_0),
        .I1(d0_i_367_n_0),
        .I2(pre_reg_cnt[61]),
        .I3(pre_reg_cnt[60]),
        .I4(d0_i_365_n_0),
        .I5(d0_i_364_n_0),
        .O(d0_i_158_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    d0_i_159
       (.I0(d0_i_72_n_0),
        .I1(d0_i_170_n_0),
        .I2(d0_i_171_n_0),
        .O(d0_i_159_n_0));
  LUT5 #(
    .INIT(32'h757575FF)) 
    d0_i_160
       (.I0(d0_i_79_n_0),
        .I1(d0_i_63_n_0),
        .I2(d0_i_80_n_0),
        .I3(d0_i_81_n_0),
        .I4(d0_i_82_n_0),
        .O(d0_i_160_n_0));
  LUT5 #(
    .INIT(32'hFFD0D0D0)) 
    d0_i_161
       (.I0(d0_i_85_n_0),
        .I1(d0_i_84_n_0),
        .I2(d0_i_83_n_0),
        .I3(d0_i_87_n_0),
        .I4(d0_i_86_n_0),
        .O(d0_i_161_n_0));
  LUT3 #(
    .INIT(8'h32)) 
    d0_i_162
       (.I0(d0_i_172_n_0),
        .I1(d0_i_67_n_0),
        .I2(d0_i_219_n_0),
        .O(d0_i_162_n_0));
  LUT3 #(
    .INIT(8'h8E)) 
    d0_i_163
       (.I0(d0_i_73_n_0),
        .I1(d0_i_74_n_0),
        .I2(d0_i_75_n_0),
        .O(d0_i_163_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_164
       (.I0(pre_reg_cnt[142]),
        .I1(pre_reg_cnt[143]),
        .I2(pre_reg_cnt[140]),
        .I3(pre_reg_cnt[141]),
        .O(d0_i_164_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_165
       (.I0(d0_i_397_n_0),
        .I1(d0_i_398_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_400_n_0),
        .I4(d0_i_401_n_0),
        .I5(d0_i_402_n_0),
        .O(d0_i_165_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_166
       (.I0(pre_reg_cnt[139]),
        .I1(pre_reg_cnt[138]),
        .O(d0_i_166_n_0));
  LUT6 #(
    .INIT(64'hCD32CD32CD3232CD)) 
    d0_i_167
       (.I0(d0_i_219_n_0),
        .I1(d0_i_67_n_0),
        .I2(d0_i_172_n_0),
        .I3(d0_i_161_n_0),
        .I4(d0_i_403_n_0),
        .I5(d0_i_404_n_0),
        .O(d0_i_167_n_0));
  LUT6 #(
    .INIT(64'h9696966996696969)) 
    d0_i_168
       (.I0(\q_reg[109]_0 ),
        .I1(d0_i_81_n_0),
        .I2(d0_i_405_n_0),
        .I3(d0_i_78_n_0),
        .I4(d0_i_77_n_0),
        .I5(d0_i_76_n_0),
        .O(d0_i_168_n_0));
  LUT4 #(
    .INIT(16'h888A)) 
    d0_i_169
       (.I0(d0_i_175_n_0),
        .I1(d0_i_63_n_0),
        .I2(pre_reg_cnt[135]),
        .I3(d0_i_241_n_0),
        .O(d0_i_169_n_0));
  LUT6 #(
    .INIT(64'h5555100055555555)) 
    d0_i_170
       (.I0(d0_i_180_n_0),
        .I1(d0_i_85_n_0),
        .I2(d0_i_181_n_0),
        .I3(d0_i_182_n_0),
        .I4(d0_i_183_n_0),
        .I5(d0_i_121_n_0),
        .O(d0_i_170_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    d0_i_171
       (.I0(d0_i_85_n_0),
        .I1(d0_i_406_n_0),
        .O(d0_i_171_n_0));
  LUT5 #(
    .INIT(32'hD02FD0D0)) 
    d0_i_172
       (.I0(d0_i_407_n_0),
        .I1(d0_i_241_n_0),
        .I2(d0_i_408_n_0),
        .I3(d0_i_63_n_0),
        .I4(d0_i_202_n_0),
        .O(d0_i_172_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_173
       (.I0(d0_i_409_n_0),
        .I1(d0_i_402_n_0),
        .I2(d0_i_410_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_398_n_0),
        .I5(d0_i_411_n_0),
        .O(d0_i_173_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_174
       (.I0(pre_reg_cnt[147]),
        .I1(pre_reg_cnt[146]),
        .O(d0_i_174_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    d0_i_175
       (.I0(pre_reg_cnt[217]),
        .I1(d0_i_330_n_0),
        .I2(d0_i_132_n_0),
        .I3(pre_reg_cnt[219]),
        .I4(pre_reg_cnt[218]),
        .O(d0_i_175_n_0));
  LUT6 #(
    .INIT(64'h0011001000110011)) 
    d0_i_176
       (.I0(pre_reg_cnt[95]),
        .I1(pre_reg_cnt[94]),
        .I2(pre_reg_cnt[91]),
        .I3(d0_i_305_n_0),
        .I4(d0_i_412_n_0),
        .I5(d0_i_413_n_0),
        .O(d0_i_176_n_0));
  LUT6 #(
    .INIT(64'h3020302030203030)) 
    d0_i_177
       (.I0(pre_reg_cnt[37]),
        .I1(d0_i_414_n_0),
        .I2(d0_i_386_n_0),
        .I3(d0_i_382_n_0),
        .I4(pre_reg_cnt[34]),
        .I5(d0_i_415_n_0),
        .O(d0_i_177_n_0));
  LUT6 #(
    .INIT(64'h0055005000550054)) 
    d0_i_178
       (.I0(pre_reg_cnt[175]),
        .I1(d0_i_416_n_0),
        .I2(pre_reg_cnt[172]),
        .I3(d0_i_319_n_0),
        .I4(d0_i_417_n_0),
        .I5(pre_reg_cnt[171]),
        .O(d0_i_178_n_0));
  LUT6 #(
    .INIT(64'hFFFF1110FFFFFFFF)) 
    d0_i_179
       (.I0(d0_i_418_n_0),
        .I1(d0_i_419_n_0),
        .I2(pre_reg_cnt[115]),
        .I3(d0_i_420_n_0),
        .I4(d0_i_421_n_0),
        .I5(d0_i_422_n_0),
        .O(d0_i_179_n_0));
  LUT6 #(
    .INIT(64'h2BD4D42BD42B2BD4)) 
    d0_i_18
       (.I0(\q_reg[34]_0 ),
        .I1(d0_i_96_0),
        .I2(d0_i_93_0),
        .I3(d0[1]),
        .I4(d0_i_46_n_0),
        .I5(d0_i_47_n_0),
        .O(d0_i_47_0[1]));
  LUT6 #(
    .INIT(64'hD0DDD000DDDDDDDD)) 
    d0_i_180
       (.I0(d0_i_423_n_0),
        .I1(d0_i_424_n_0),
        .I2(d0_i_425_n_0),
        .I3(d0_i_241_n_0),
        .I4(pre_reg_cnt[135]),
        .I5(d0_i_291_n_0),
        .O(d0_i_180_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_181
       (.I0(pre_reg_cnt[154]),
        .I1(pre_reg_cnt[155]),
        .I2(d0_i_298_n_0),
        .I3(d0_i_426_n_0),
        .O(d0_i_181_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF01)) 
    d0_i_182
       (.I0(d0_i_427_n_0),
        .I1(d0_i_315_n_0),
        .I2(pre_reg_cnt[163]),
        .I3(d0_i_417_n_0),
        .I4(d0_i_319_n_0),
        .I5(pre_reg_cnt[172]),
        .O(d0_i_182_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    d0_i_183
       (.I0(d0_i_311_n_0),
        .I1(pre_reg_cnt[226]),
        .I2(pre_reg_cnt[227]),
        .I3(d0_i_428_n_0),
        .I4(d0_i_429_n_0),
        .I5(d0_i_175_n_0),
        .O(d0_i_183_n_0));
  LUT6 #(
    .INIT(64'h000000000000000B)) 
    d0_i_184
       (.I0(pre_reg_cnt[82]),
        .I1(pre_reg_cnt[81]),
        .I2(d0_i_430_n_0),
        .I3(d0_i_413_n_0),
        .I4(d0_i_305_n_0),
        .I5(pre_reg_cnt[83]),
        .O(d0_i_184_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_185
       (.I0(pre_reg_cnt[93]),
        .I1(pre_reg_cnt[92]),
        .I2(pre_reg_cnt[95]),
        .I3(pre_reg_cnt[94]),
        .I4(d0_i_305_n_0),
        .O(d0_i_185_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFEE)) 
    d0_i_186
       (.I0(d0_i_394_n_0),
        .I1(d0_i_393_n_0),
        .I2(pre_reg_cnt[73]),
        .I3(pre_reg_cnt[74]),
        .I4(pre_reg_cnt[75]),
        .I5(pre_reg_cnt[72]),
        .O(d0_i_186_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_187
       (.I0(pre_reg_cnt[239]),
        .I1(pre_reg_cnt[238]),
        .I2(pre_reg_cnt[237]),
        .I3(pre_reg_cnt[236]),
        .I4(d0_i_125_n_0),
        .O(d0_i_187_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    d0_i_188
       (.I0(pre_reg_cnt[255]),
        .I1(pre_reg_cnt[254]),
        .I2(d0_i_399_n_0),
        .I3(pre_reg_cnt[253]),
        .I4(pre_reg_cnt[252]),
        .O(d0_i_188_n_0));
  LUT6 #(
    .INIT(64'h0000000001010001)) 
    d0_i_189
       (.I0(d0_i_293_n_0),
        .I1(pre_reg_cnt[247]),
        .I2(pre_reg_cnt[246]),
        .I3(pre_reg_cnt[243]),
        .I4(pre_reg_cnt[244]),
        .I5(pre_reg_cnt[245]),
        .O(d0_i_189_n_0));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    d0_i_19
       (.I0(\q_reg[55]_0 ),
        .I1(\q_reg[295]_0 ),
        .I2(\q_reg[43]_0 ),
        .I3(d0[0]),
        .I4(d0_i_48_n_0),
        .I5(d0_i_49_n_0),
        .O(d0_i_47_0[0]));
  LUT6 #(
    .INIT(64'h0011001000110011)) 
    d0_i_190
       (.I0(pre_reg_cnt[167]),
        .I1(pre_reg_cnt[166]),
        .I2(pre_reg_cnt[163]),
        .I3(d0_i_323_n_0),
        .I4(d0_i_427_n_0),
        .I5(d0_i_298_n_0),
        .O(d0_i_190_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_191
       (.I0(pre_reg_cnt[180]),
        .I1(pre_reg_cnt[178]),
        .I2(pre_reg_cnt[179]),
        .O(d0_i_191_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_192
       (.I0(pre_reg_cnt[181]),
        .I1(d0_i_402_n_0),
        .I2(d0_i_410_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_193_n_0),
        .I5(d0_i_233_n_0),
        .O(d0_i_192_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_193
       (.I0(d0_i_320_n_0),
        .I1(pre_reg_cnt[185]),
        .I2(pre_reg_cnt[184]),
        .I3(pre_reg_cnt[187]),
        .I4(pre_reg_cnt[186]),
        .O(d0_i_193_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_194
       (.I0(d0_i_399_n_0),
        .I1(d0_i_400_n_0),
        .I2(d0_i_401_n_0),
        .I3(d0_i_402_n_0),
        .O(d0_i_194_n_0));
  LUT5 #(
    .INIT(32'h0000BBFB)) 
    d0_i_195
       (.I0(d0_i_85_n_0),
        .I1(d0_i_181_n_0),
        .I2(d0_i_431_n_0),
        .I3(d0_i_408_n_0),
        .I4(d0_i_183_n_0),
        .O(d0_i_195_n_0));
  LUT4 #(
    .INIT(16'hDDD0)) 
    d0_i_196
       (.I0(d0_i_241_n_0),
        .I1(d0_i_432_n_0),
        .I2(d0_i_200_n_0),
        .I3(pre_reg_cnt[127]),
        .O(d0_i_196_n_0));
  LUT4 #(
    .INIT(16'hFFF1)) 
    d0_i_197
       (.I0(d0_i_433_n_0),
        .I1(d0_i_222_n_0),
        .I2(d0_i_434_n_0),
        .I3(d0_i_83_n_0),
        .O(d0_i_197_n_0));
  LUT6 #(
    .INIT(64'h10101055FFFFFFFF)) 
    d0_i_198
       (.I0(d0_i_86_n_0),
        .I1(d0_i_220_n_0),
        .I2(d0_i_435_n_0),
        .I3(d0_i_436_n_0),
        .I4(d0_i_437_n_0),
        .I5(d0_i_259_n_0),
        .O(d0_i_198_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_199
       (.I0(pre_reg_cnt[126]),
        .I1(pre_reg_cnt[127]),
        .O(d0_i_199_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_200
       (.I0(d0_i_402_n_0),
        .I1(d0_i_401_n_0),
        .I2(d0_i_400_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_438_n_0),
        .O(d0_i_200_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFCFFFFFFFE)) 
    d0_i_201
       (.I0(pre_reg_cnt[144]),
        .I1(d0_i_174_n_0),
        .I2(d0_i_411_n_0),
        .I3(d0_i_298_n_0),
        .I4(d0_i_409_n_0),
        .I5(pre_reg_cnt[145]),
        .O(d0_i_201_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFEE)) 
    d0_i_202
       (.I0(d0_i_164_n_0),
        .I1(d0_i_166_n_0),
        .I2(pre_reg_cnt[136]),
        .I3(pre_reg_cnt[137]),
        .I4(d0_i_165_n_0),
        .I5(pre_reg_cnt[135]),
        .O(d0_i_202_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAA02000202)) 
    d0_i_203
       (.I0(d0_i_439_n_0),
        .I1(d0_i_427_n_0),
        .I2(d0_i_323_n_0),
        .I3(pre_reg_cnt[163]),
        .I4(pre_reg_cnt[162]),
        .I5(d0_i_314_n_0),
        .O(d0_i_203_n_0));
  LUT5 #(
    .INIT(32'hA0A8AAAA)) 
    d0_i_204
       (.I0(d0_i_440_n_0),
        .I1(pre_reg_cnt[198]),
        .I2(d0_i_307_n_0),
        .I3(pre_reg_cnt[199]),
        .I4(d0_i_441_n_0),
        .O(d0_i_204_n_0));
  LUT6 #(
    .INIT(64'hFFBF0000FFBFFFBF)) 
    d0_i_205
       (.I0(d0_i_442_n_0),
        .I1(d0_i_443_n_0),
        .I2(d0_i_444_n_0),
        .I3(d0_i_445_n_0),
        .I4(d0_i_446_n_0),
        .I5(d0_i_447_n_0),
        .O(d0_i_205_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAA80A0)) 
    d0_i_206
       (.I0(d0_i_448_n_0),
        .I1(pre_reg_cnt[171]),
        .I2(d0_i_431_n_0),
        .I3(d0_i_416_n_0),
        .I4(pre_reg_cnt[175]),
        .I5(d0_i_319_n_0),
        .O(d0_i_206_n_0));
  LUT6 #(
    .INIT(64'h00000000000F000E)) 
    d0_i_207
       (.I0(d0_i_449_n_0),
        .I1(d0_i_450_n_0),
        .I2(d0_i_298_n_0),
        .I3(d0_i_451_n_0),
        .I4(pre_reg_cnt[156]),
        .I5(pre_reg_cnt[157]),
        .O(d0_i_207_n_0));
  LUT6 #(
    .INIT(64'h0010001000100011)) 
    d0_i_208
       (.I0(d0_i_164_n_0),
        .I1(pre_reg_cnt[139]),
        .I2(d0_i_452_n_0),
        .I3(d0_i_165_n_0),
        .I4(pre_reg_cnt[133]),
        .I5(d0_i_453_n_0),
        .O(d0_i_208_n_0));
  LUT6 #(
    .INIT(64'h0000000000000301)) 
    d0_i_209
       (.I0(pre_reg_cnt[147]),
        .I1(d0_i_411_n_0),
        .I2(d0_i_298_n_0),
        .I3(pre_reg_cnt[148]),
        .I4(pre_reg_cnt[151]),
        .I5(d0_i_454_n_0),
        .O(d0_i_209_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    d0_i_210
       (.I0(pre_reg_cnt[143]),
        .I1(pre_reg_cnt[142]),
        .I2(pre_reg_cnt[141]),
        .O(d0_i_210_n_0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    d0_i_211
       (.I0(pre_reg_cnt[145]),
        .I1(d0_i_409_n_0),
        .I2(d0_i_298_n_0),
        .I3(d0_i_411_n_0),
        .I4(pre_reg_cnt[147]),
        .I5(pre_reg_cnt[146]),
        .O(d0_i_211_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_212
       (.I0(d0_i_328_n_0),
        .I1(d0_i_327_n_0),
        .I2(d0_i_326_n_0),
        .I3(d0_i_324_n_0),
        .O(d0_i_212_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_213
       (.I0(d0_i_395_n_0),
        .I1(d0_i_455_n_0),
        .I2(d0_i_438_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_456_n_0),
        .I5(d0_i_457_n_0),
        .O(d0_i_213_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_214
       (.I0(pre_reg_cnt[70]),
        .I1(pre_reg_cnt[71]),
        .I2(pre_reg_cnt[68]),
        .I3(pre_reg_cnt[69]),
        .O(d0_i_214_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_215
       (.I0(pre_reg_cnt[63]),
        .I1(pre_reg_cnt[62]),
        .O(d0_i_215_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_216
       (.I0(d0_i_402_n_0),
        .I1(d0_i_410_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_455_n_0),
        .I4(d0_i_458_n_0),
        .I5(d0_i_438_n_0),
        .O(d0_i_216_n_0));
  LUT6 #(
    .INIT(64'h1010101010101011)) 
    d0_i_217
       (.I0(d0_i_459_n_0),
        .I1(pre_reg_cnt[283]),
        .I2(d0_i_231_n_0),
        .I3(pre_reg_cnt[279]),
        .I4(pre_reg_cnt[277]),
        .I5(pre_reg_cnt[278]),
        .O(d0_i_217_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    d0_i_218
       (.I0(pre_reg_cnt[298]),
        .I1(pre_reg_cnt[299]),
        .I2(d0_i_460_n_0),
        .O(d0_i_218_n_0));
  LUT5 #(
    .INIT(32'hDDDDDDD5)) 
    d0_i_219
       (.I0(d0_i_461_n_0),
        .I1(d0_i_175_n_0),
        .I2(d0_i_194_n_0),
        .I3(pre_reg_cnt[190]),
        .I4(pre_reg_cnt[191]),
        .O(d0_i_219_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_220
       (.I0(pre_reg_cnt[10]),
        .I1(pre_reg_cnt[11]),
        .I2(d0_i_462_n_0),
        .I3(d0_i_463_n_0),
        .I4(pre_reg_cnt[16]),
        .I5(d0_i_376_n_0),
        .O(d0_i_220_n_0));
  LUT6 #(
    .INIT(64'h0000003200000033)) 
    d0_i_221
       (.I0(d0_i_464_n_0),
        .I1(d0_i_428_n_0),
        .I2(pre_reg_cnt[228]),
        .I3(pre_reg_cnt[229]),
        .I4(d0_i_465_n_0),
        .I5(d0_i_466_n_0),
        .O(d0_i_221_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    d0_i_222
       (.I0(pre_reg_cnt[82]),
        .I1(pre_reg_cnt[83]),
        .I2(d0_i_305_n_0),
        .I3(d0_i_413_n_0),
        .I4(d0_i_430_n_0),
        .O(d0_i_222_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    d0_i_223
       (.I0(d0_i_269_n_0),
        .I1(pre_reg_cnt[31]),
        .I2(pre_reg_cnt[28]),
        .I3(pre_reg_cnt[30]),
        .I4(pre_reg_cnt[29]),
        .O(d0_i_223_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_224
       (.I0(pre_reg_cnt[55]),
        .I1(d0_i_237_n_0),
        .I2(d0_i_456_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_467_n_0),
        .I5(d0_i_438_n_0),
        .O(d0_i_224_n_0));
  LUT6 #(
    .INIT(64'h1010101010101011)) 
    d0_i_225
       (.I0(d0_i_468_n_0),
        .I1(pre_reg_cnt[163]),
        .I2(d0_i_241_n_0),
        .I3(pre_reg_cnt[109]),
        .I4(d0_i_229_n_0),
        .I5(d0_i_230_n_0),
        .O(d0_i_225_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_226
       (.I0(d0_i_469_n_0),
        .I1(pre_reg_cnt[107]),
        .I2(pre_reg_cnt[106]),
        .I3(pre_reg_cnt[105]),
        .I4(pre_reg_cnt[104]),
        .O(d0_i_226_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_227
       (.I0(pre_reg_cnt[102]),
        .I1(pre_reg_cnt[103]),
        .I2(pre_reg_cnt[100]),
        .I3(pre_reg_cnt[101]),
        .O(d0_i_227_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_228
       (.I0(d0_i_418_n_0),
        .I1(d0_i_402_n_0),
        .I2(d0_i_410_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_438_n_0),
        .I5(d0_i_290_n_0),
        .O(d0_i_228_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_229
       (.I0(d0_i_470_n_0),
        .I1(d0_i_438_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_400_n_0),
        .I4(d0_i_401_n_0),
        .I5(d0_i_402_n_0),
        .O(d0_i_229_n_0));
  LUT6 #(
    .INIT(64'h65A6FFFF000065A6)) 
    d0_i_23
       (.I0(d0_i_59_n_0),
        .I1(\q_reg[271]_0 ),
        .I2(\q_reg[109]_0 ),
        .I3(d0_i_78_0),
        .I4(d0_i_60_n_0),
        .I5(d0_i_61_n_0),
        .O(DI[1]));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_230
       (.I0(pre_reg_cnt[111]),
        .I1(pre_reg_cnt[110]),
        .O(d0_i_230_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_231
       (.I0(d0_i_328_n_0),
        .I1(d0_i_327_n_0),
        .I2(d0_i_326_n_0),
        .I3(d0_i_471_n_0),
        .O(d0_i_231_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_232
       (.I0(pre_reg_cnt[265]),
        .I1(pre_reg_cnt[264]),
        .I2(pre_reg_cnt[267]),
        .I3(pre_reg_cnt[266]),
        .I4(d0_i_472_n_0),
        .O(d0_i_232_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_233
       (.I0(pre_reg_cnt[183]),
        .I1(pre_reg_cnt[182]),
        .O(d0_i_233_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_234
       (.I0(pre_reg_cnt[199]),
        .I1(d0_i_400_n_0),
        .I2(d0_i_132_n_0),
        .I3(d0_i_473_n_0),
        .O(d0_i_234_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_235
       (.I0(pre_reg_cnt[191]),
        .I1(pre_reg_cnt[190]),
        .O(d0_i_235_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_236
       (.I0(d0_i_389_n_0),
        .I1(pre_reg_cnt[51]),
        .I2(pre_reg_cnt[48]),
        .I3(pre_reg_cnt[50]),
        .I4(pre_reg_cnt[49]),
        .O(d0_i_236_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_237
       (.I0(pre_reg_cnt[57]),
        .I1(pre_reg_cnt[56]),
        .I2(pre_reg_cnt[59]),
        .I3(pre_reg_cnt[58]),
        .I4(d0_i_474_n_0),
        .O(d0_i_237_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_238
       (.I0(d0_i_237_n_0),
        .I1(d0_i_456_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_467_n_0),
        .I4(d0_i_438_n_0),
        .I5(d0_i_475_n_0),
        .O(d0_i_238_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_239
       (.I0(pre_reg_cnt[137]),
        .I1(pre_reg_cnt[138]),
        .I2(pre_reg_cnt[139]),
        .O(d0_i_239_n_0));
  LUT6 #(
    .INIT(64'h9A5965A665A69A59)) 
    d0_i_24
       (.I0(d0_i_59_n_0),
        .I1(\q_reg[271]_0 ),
        .I2(\q_reg[109]_0 ),
        .I3(d0_i_78_0),
        .I4(d0_i_61_n_0),
        .I5(d0_i_60_n_0),
        .O(DI[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_240
       (.I0(d0_i_164_n_0),
        .I1(d0_i_402_n_0),
        .I2(d0_i_410_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_398_n_0),
        .I5(d0_i_397_n_0),
        .O(d0_i_240_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_241
       (.I0(d0_i_452_n_0),
        .I1(d0_i_402_n_0),
        .I2(d0_i_410_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_398_n_0),
        .I5(d0_i_397_n_0),
        .O(d0_i_241_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    d0_i_242
       (.I0(d0_i_414_n_0),
        .I1(d0_i_476_n_0),
        .I2(d0_i_216_n_0),
        .I3(d0_i_382_n_0),
        .I4(pre_reg_cnt[37]),
        .I5(pre_reg_cnt[36]),
        .O(d0_i_242_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_243
       (.I0(d0_i_216_n_0),
        .I1(d0_i_476_n_0),
        .I2(d0_i_414_n_0),
        .I3(d0_i_477_n_0),
        .I4(pre_reg_cnt[35]),
        .O(d0_i_243_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFAAFE)) 
    d0_i_244
       (.I0(pre_reg_cnt[247]),
        .I1(pre_reg_cnt[241]),
        .I2(d0_i_478_n_0),
        .I3(d0_i_479_n_0),
        .I4(d0_i_480_n_0),
        .I5(d0_i_399_n_0),
        .O(d0_i_244_n_0));
  LUT6 #(
    .INIT(64'h0E0E0E0E0E0E0E0F)) 
    d0_i_245
       (.I0(pre_reg_cnt[253]),
        .I1(d0_i_481_n_0),
        .I2(d0_i_399_n_0),
        .I3(pre_reg_cnt[250]),
        .I4(pre_reg_cnt[251]),
        .I5(pre_reg_cnt[252]),
        .O(d0_i_245_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_246
       (.I0(pre_reg_cnt[266]),
        .I1(pre_reg_cnt[267]),
        .I2(d0_i_212_n_0),
        .I3(d0_i_472_n_0),
        .O(d0_i_246_n_0));
  LUT6 #(
    .INIT(64'h00000000000000FE)) 
    d0_i_247
       (.I0(pre_reg_cnt[259]),
        .I1(pre_reg_cnt[261]),
        .I2(pre_reg_cnt[260]),
        .I3(pre_reg_cnt[263]),
        .I4(pre_reg_cnt[262]),
        .I5(d0_i_482_n_0),
        .O(d0_i_247_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF2)) 
    d0_i_248
       (.I0(pre_reg_cnt[9]),
        .I1(pre_reg_cnt[10]),
        .I2(pre_reg_cnt[11]),
        .I3(d0_i_462_n_0),
        .I4(d0_i_483_n_0),
        .I5(d0_i_376_n_0),
        .O(d0_i_248_n_0));
  LUT4 #(
    .INIT(16'h1011)) 
    d0_i_249
       (.I0(d0_i_305_n_0),
        .I1(d0_i_412_n_0),
        .I2(pre_reg_cnt[91]),
        .I3(pre_reg_cnt[90]),
        .O(d0_i_249_n_0));
  LUT4 #(
    .INIT(16'h40FF)) 
    d0_i_25
       (.I0(d0_i_62_n_0),
        .I1(d0_i_64_0),
        .I2(d0_i_163_0),
        .I3(d0_i_63_n_0),
        .O(S[2]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF1)) 
    d0_i_250
       (.I0(pre_reg_cnt[84]),
        .I1(d0_i_484_n_0),
        .I2(d0_i_485_n_0),
        .I3(d0_i_413_n_0),
        .I4(d0_i_305_n_0),
        .I5(pre_reg_cnt[85]),
        .O(d0_i_250_n_0));
  LUT6 #(
    .INIT(64'h0404040404040405)) 
    d0_i_251
       (.I0(pre_reg_cnt[103]),
        .I1(d0_i_227_n_0),
        .I2(d0_i_292_n_0),
        .I3(pre_reg_cnt[97]),
        .I4(pre_reg_cnt[98]),
        .I5(pre_reg_cnt[99]),
        .O(d0_i_251_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    d0_i_252
       (.I0(d0_i_79_n_0),
        .I1(d0_i_276_n_0),
        .O(d0_i_252_n_0));
  LUT6 #(
    .INIT(64'hFF55FF55FF55FFFD)) 
    d0_i_253
       (.I0(d0_i_486_n_0),
        .I1(d0_i_463_n_0),
        .I2(pre_reg_cnt[16]),
        .I3(d0_i_376_n_0),
        .I4(pre_reg_cnt[19]),
        .I5(d0_i_373_n_0),
        .O(d0_i_253_n_0));
  LUT6 #(
    .INIT(64'h888A8888888A888A)) 
    d0_i_254
       (.I0(d0_i_189_n_0),
        .I1(d0_i_128_n_0),
        .I2(d0_i_194_n_0),
        .I3(pre_reg_cnt[191]),
        .I4(pre_reg_cnt[190]),
        .I5(pre_reg_cnt[189]),
        .O(d0_i_254_n_0));
  LUT6 #(
    .INIT(64'h00000000000D0000)) 
    d0_i_255
       (.I0(d0_i_466_n_0),
        .I1(d0_i_487_n_0),
        .I2(pre_reg_cnt[229]),
        .I3(d0_i_488_n_0),
        .I4(d0_i_443_n_0),
        .I5(d0_i_442_n_0),
        .O(d0_i_255_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    d0_i_256
       (.I0(d0_i_217_n_0),
        .I1(d0_i_489_n_0),
        .O(d0_i_256_n_0));
  LUT6 #(
    .INIT(64'h000000000000BABB)) 
    d0_i_257
       (.I0(d0_i_412_n_0),
        .I1(pre_reg_cnt[91]),
        .I2(pre_reg_cnt[90]),
        .I3(d0_i_490_n_0),
        .I4(d0_i_491_n_0),
        .I5(d0_i_305_n_0),
        .O(d0_i_257_n_0));
  LUT6 #(
    .INIT(64'h00AE000000000000)) 
    d0_i_258
       (.I0(d0_i_188_n_0),
        .I1(d0_i_492_n_0),
        .I2(d0_i_493_n_0),
        .I3(d0_i_494_n_0),
        .I4(d0_i_495_n_0),
        .I5(d0_i_496_n_0),
        .O(d0_i_258_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_259
       (.I0(pre_reg_cnt[317]),
        .I1(pre_reg_cnt[316]),
        .I2(pre_reg_cnt[318]),
        .I3(pre_reg_cnt[319]),
        .I4(pre_reg_cnt[314]),
        .I5(pre_reg_cnt[315]),
        .O(d0_i_259_n_0));
  LUT6 #(
    .INIT(64'h870F0F7887F078F0)) 
    d0_i_26
       (.I0(d0_i_59_n_0),
        .I1(d0_i_64_n_0),
        .I2(\q_reg[137]_0 ),
        .I3(d0_i_163_0),
        .I4(d0_i_65_n_0),
        .I5(d0_i_66_n_0),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h40F040F040F04040)) 
    d0_i_260
       (.I0(d0_i_220_n_0),
        .I1(d0_i_435_n_0),
        .I2(d0_i_259_n_0),
        .I3(d0_i_436_n_0),
        .I4(d0_i_497_n_0),
        .I5(d0_i_223_n_0),
        .O(d0_i_260_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    d0_i_261
       (.I0(pre_reg_cnt[31]),
        .I1(d0_i_269_n_0),
        .I2(pre_reg_cnt[30]),
        .I3(pre_reg_cnt[29]),
        .I4(pre_reg_cnt[28]),
        .I5(pre_reg_cnt[27]),
        .O(d0_i_261_n_0));
  LUT5 #(
    .INIT(32'hFFFF01FF)) 
    d0_i_262
       (.I0(d0_i_461_n_0),
        .I1(d0_i_498_n_0),
        .I2(pre_reg_cnt[253]),
        .I3(d0_i_499_n_0),
        .I4(d0_i_84_n_0),
        .O(d0_i_262_n_0));
  LUT6 #(
    .INIT(64'hDDDDDDDDDDD0DDDD)) 
    d0_i_263
       (.I0(d0_i_500_n_0),
        .I1(d0_i_218_n_0),
        .I2(pre_reg_cnt[290]),
        .I3(pre_reg_cnt[291]),
        .I4(d0_i_501_n_0),
        .I5(pre_reg_cnt[289]),
        .O(d0_i_263_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_264
       (.I0(pre_reg_cnt[45]),
        .I1(pre_reg_cnt[44]),
        .I2(pre_reg_cnt[47]),
        .I3(pre_reg_cnt[46]),
        .I4(d0_i_476_n_0),
        .I5(d0_i_216_n_0),
        .O(d0_i_264_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_265
       (.I0(pre_reg_cnt[46]),
        .I1(pre_reg_cnt[47]),
        .I2(d0_i_476_n_0),
        .I3(d0_i_216_n_0),
        .O(d0_i_265_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_266
       (.I0(pre_reg_cnt[51]),
        .I1(d0_i_216_n_0),
        .I2(d0_i_237_n_0),
        .I3(d0_i_389_n_0),
        .O(d0_i_266_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_267
       (.I0(pre_reg_cnt[50]),
        .I1(pre_reg_cnt[49]),
        .O(d0_i_267_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_268
       (.I0(pre_reg_cnt[29]),
        .I1(pre_reg_cnt[30]),
        .I2(pre_reg_cnt[28]),
        .I3(pre_reg_cnt[31]),
        .O(d0_i_268_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_269
       (.I0(d0_i_438_n_0),
        .I1(d0_i_467_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_410_n_0),
        .I4(d0_i_402_n_0),
        .I5(d0_i_502_n_0),
        .O(d0_i_269_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_270
       (.I0(d0_i_355_n_0),
        .I1(d0_i_376_n_0),
        .I2(pre_reg_cnt[16]),
        .I3(d0_i_463_n_0),
        .O(d0_i_270_n_0));
  LUT6 #(
    .INIT(64'h000000000000000E)) 
    d0_i_271
       (.I0(pre_reg_cnt[12]),
        .I1(d0_i_503_n_0),
        .I2(d0_i_357_n_0),
        .I3(d0_i_376_n_0),
        .I4(d0_i_483_n_0),
        .I5(pre_reg_cnt[13]),
        .O(d0_i_271_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF4)) 
    d0_i_272
       (.I0(pre_reg_cnt[1]),
        .I1(d0_i_371_n_0),
        .I2(d0_i_372_n_0),
        .I3(d0_i_355_n_0),
        .I4(d0_i_376_n_0),
        .I5(d0_i_483_n_0),
        .O(d0_i_272_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    d0_i_273
       (.I0(pre_reg_cnt[318]),
        .I1(pre_reg_cnt[319]),
        .O(d0_i_273_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAA8AAAAAAAA)) 
    d0_i_274
       (.I0(d0_i_338_n_0),
        .I1(pre_reg_cnt[290]),
        .I2(pre_reg_cnt[291]),
        .I3(d0_i_284_n_0),
        .I4(d0_i_504_n_0),
        .I5(pre_reg_cnt[289]),
        .O(d0_i_274_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_275
       (.I0(pre_reg_cnt[308]),
        .I1(pre_reg_cnt[309]),
        .I2(pre_reg_cnt[311]),
        .I3(pre_reg_cnt[310]),
        .I4(d0_i_341_n_0),
        .O(d0_i_275_n_0));
  LUT4 #(
    .INIT(16'hFFF2)) 
    d0_i_276
       (.I0(pre_reg_cnt[297]),
        .I1(pre_reg_cnt[298]),
        .I2(pre_reg_cnt[299]),
        .I3(d0_i_460_n_0),
        .O(d0_i_276_n_0));
  LUT3 #(
    .INIT(8'hAB)) 
    d0_i_277
       (.I0(d0_i_505_n_0),
        .I1(d0_i_335_n_0),
        .I2(d0_i_79_n_0),
        .O(d0_i_277_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    d0_i_278
       (.I0(d0_i_429_n_0),
        .I1(d0_i_428_n_0),
        .I2(pre_reg_cnt[227]),
        .I3(pre_reg_cnt[226]),
        .I4(pre_reg_cnt[225]),
        .O(d0_i_278_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_279
       (.I0(d0_i_290_n_0),
        .I1(d0_i_200_n_0),
        .I2(d0_i_420_n_0),
        .I3(d0_i_506_n_0),
        .O(d0_i_279_n_0));
  LUT6 #(
    .INIT(64'hD42B00FFFF00D42B)) 
    d0_i_28
       (.I0(\q_reg[34]_0 ),
        .I1(d0_i_96_0),
        .I2(d0_i_93_0),
        .I3(DI[0]),
        .I4(d0_i_47_n_0),
        .I5(d0_i_46_n_0),
        .O(S[0]));
  LUT6 #(
    .INIT(64'h00000000000000F2)) 
    d0_i_280
       (.I0(d0_i_507_n_0),
        .I1(pre_reg_cnt[108]),
        .I2(pre_reg_cnt[109]),
        .I3(pre_reg_cnt[110]),
        .I4(pre_reg_cnt[111]),
        .I5(d0_i_229_n_0),
        .O(d0_i_280_n_0));
  LUT6 #(
    .INIT(64'h00000000000000A2)) 
    d0_i_281
       (.I0(d0_i_508_n_0),
        .I1(pre_reg_cnt[129]),
        .I2(pre_reg_cnt[130]),
        .I3(pre_reg_cnt[131]),
        .I4(d0_i_241_n_0),
        .I5(d0_i_509_n_0),
        .O(d0_i_281_n_0));
  LUT6 #(
    .INIT(64'h0000000000010000)) 
    d0_i_282
       (.I0(d0_i_483_n_0),
        .I1(d0_i_376_n_0),
        .I2(d0_i_355_n_0),
        .I3(d0_i_372_n_0),
        .I4(d0_i_371_n_0),
        .I5(d0_i_370_n_0),
        .O(d0_i_282_n_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    d0_i_283
       (.I0(d0_i_376_n_0),
        .I1(d0_i_373_n_0),
        .I2(pre_reg_cnt[19]),
        .I3(pre_reg_cnt[18]),
        .O(d0_i_283_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_284
       (.I0(d0_i_326_n_0),
        .I1(pre_reg_cnt[298]),
        .I2(pre_reg_cnt[299]),
        .I3(pre_reg_cnt[296]),
        .I4(pre_reg_cnt[297]),
        .I5(d0_i_510_n_0),
        .O(d0_i_284_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    d0_i_285
       (.I0(pre_reg_cnt[303]),
        .I1(pre_reg_cnt[302]),
        .I2(d0_i_326_n_0),
        .I3(pre_reg_cnt[301]),
        .I4(pre_reg_cnt[300]),
        .O(d0_i_285_n_0));
  LUT3 #(
    .INIT(8'h45)) 
    d0_i_286
       (.I0(d0_i_132_n_0),
        .I1(pre_reg_cnt[223]),
        .I2(pre_reg_cnt[222]),
        .O(d0_i_286_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    d0_i_287
       (.I0(d0_i_428_n_0),
        .I1(pre_reg_cnt[231]),
        .I2(pre_reg_cnt[230]),
        .I3(pre_reg_cnt[229]),
        .I4(pre_reg_cnt[228]),
        .O(d0_i_287_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    d0_i_288
       (.I0(d0_i_216_n_0),
        .I1(d0_i_476_n_0),
        .I2(pre_reg_cnt[47]),
        .I3(pre_reg_cnt[46]),
        .I4(pre_reg_cnt[45]),
        .O(d0_i_288_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    d0_i_289
       (.I0(d0_i_511_n_0),
        .I1(d0_i_214_n_0),
        .I2(d0_i_213_n_0),
        .I3(pre_reg_cnt[65]),
        .I4(d0_i_216_n_0),
        .I5(pre_reg_cnt[63]),
        .O(d0_i_289_n_0));
  LUT6 #(
    .INIT(64'hB4B44B4BB4B44BB4)) 
    d0_i_29
       (.I0(d0_i_67_n_0),
        .I1(d0_i_68_n_0),
        .I2(d0_i_69_n_0),
        .I3(d0_i_70_n_0),
        .I4(d0_i_71_n_0),
        .I5(d0_i_72_n_0),
        .O(\q_reg[91]_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_290
       (.I0(pre_reg_cnt[121]),
        .I1(pre_reg_cnt[120]),
        .I2(pre_reg_cnt[123]),
        .I3(pre_reg_cnt[122]),
        .I4(d0_i_363_n_0),
        .O(d0_i_290_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_291
       (.I0(d0_i_470_n_0),
        .I1(d0_i_438_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_456_n_0),
        .I4(d0_i_230_n_0),
        .I5(d0_i_512_n_0),
        .O(d0_i_291_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_292
       (.I0(d0_i_226_n_0),
        .I1(d0_i_402_n_0),
        .I2(d0_i_410_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_438_n_0),
        .I5(d0_i_470_n_0),
        .O(d0_i_292_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_293
       (.I0(d0_i_324_n_0),
        .I1(d0_i_325_n_0),
        .I2(d0_i_326_n_0),
        .I3(d0_i_327_n_0),
        .I4(d0_i_328_n_0),
        .I5(d0_i_480_n_0),
        .O(d0_i_293_n_0));
  LUT6 #(
    .INIT(64'hAAA2AAAA00000000)) 
    d0_i_294
       (.I0(d0_i_189_n_0),
        .I1(d0_i_461_n_0),
        .I2(pre_reg_cnt[243]),
        .I3(pre_reg_cnt[242]),
        .I4(pre_reg_cnt[241]),
        .I5(d0_i_125_n_0),
        .O(d0_i_294_n_0));
  LUT5 #(
    .INIT(32'h0000DF55)) 
    d0_i_295
       (.I0(d0_i_505_n_0),
        .I1(pre_reg_cnt[259]),
        .I2(pre_reg_cnt[258]),
        .I3(d0_i_495_n_0),
        .I4(d0_i_513_n_0),
        .O(d0_i_295_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_296
       (.I0(pre_reg_cnt[134]),
        .I1(pre_reg_cnt[135]),
        .I2(d0_i_241_n_0),
        .O(d0_i_296_n_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    d0_i_297
       (.I0(d0_i_165_n_0),
        .I1(d0_i_164_n_0),
        .I2(pre_reg_cnt[139]),
        .I3(pre_reg_cnt[138]),
        .O(d0_i_297_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_298
       (.I0(d0_i_402_n_0),
        .I1(d0_i_401_n_0),
        .I2(d0_i_400_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_398_n_0),
        .O(d0_i_298_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF45)) 
    d0_i_299
       (.I0(d0_i_411_n_0),
        .I1(pre_reg_cnt[151]),
        .I2(pre_reg_cnt[150]),
        .I3(d0_i_514_n_0),
        .I4(d0_i_426_n_0),
        .I5(d0_i_298_n_0),
        .O(d0_i_299_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    d0_i_30
       (.I0(d0_i_73_n_0),
        .I1(d0_i_74_n_0),
        .I2(d0_i_75_n_0),
        .O(d0_i_75_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    d0_i_300
       (.I0(d0_i_395_n_0),
        .I1(d0_i_305_n_0),
        .I2(pre_reg_cnt[79]),
        .I3(pre_reg_cnt[78]),
        .O(d0_i_300_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_301
       (.I0(pre_reg_cnt[86]),
        .I1(pre_reg_cnt[87]),
        .I2(d0_i_413_n_0),
        .I3(d0_i_305_n_0),
        .O(d0_i_301_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    d0_i_302
       (.I0(d0_i_292_n_0),
        .I1(pre_reg_cnt[103]),
        .I2(pre_reg_cnt[102]),
        .O(d0_i_302_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_303
       (.I0(pre_reg_cnt[101]),
        .I1(pre_reg_cnt[100]),
        .I2(pre_reg_cnt[103]),
        .I3(pre_reg_cnt[102]),
        .I4(d0_i_292_n_0),
        .O(d0_i_303_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    d0_i_304
       (.I0(pre_reg_cnt[98]),
        .I1(pre_reg_cnt[99]),
        .I2(pre_reg_cnt[97]),
        .O(d0_i_304_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_305
       (.I0(d0_i_455_n_0),
        .I1(d0_i_438_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_400_n_0),
        .I4(d0_i_401_n_0),
        .I5(d0_i_402_n_0),
        .O(d0_i_305_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_306
       (.I0(pre_reg_cnt[186]),
        .I1(pre_reg_cnt[187]),
        .O(d0_i_306_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_307
       (.I0(d0_i_473_n_0),
        .I1(d0_i_132_n_0),
        .I2(d0_i_400_n_0),
        .O(d0_i_307_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_308
       (.I0(pre_reg_cnt[196]),
        .I1(pre_reg_cnt[197]),
        .I2(pre_reg_cnt[198]),
        .I3(pre_reg_cnt[199]),
        .O(d0_i_308_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_309
       (.I0(pre_reg_cnt[212]),
        .I1(pre_reg_cnt[213]),
        .I2(pre_reg_cnt[210]),
        .I3(pre_reg_cnt[211]),
        .O(d0_i_309_n_0));
  LUT3 #(
    .INIT(8'hE8)) 
    d0_i_31
       (.I0(d0_i_76_n_0),
        .I1(d0_i_77_n_0),
        .I2(d0_i_78_n_0),
        .O(d0_i_78_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_310
       (.I0(pre_reg_cnt[214]),
        .I1(pre_reg_cnt[215]),
        .I2(d0_i_132_n_0),
        .I3(d0_i_129_n_0),
        .O(d0_i_310_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_311
       (.I0(d0_i_400_n_0),
        .I1(d0_i_132_n_0),
        .O(d0_i_311_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    d0_i_312
       (.I0(pre_reg_cnt[197]),
        .I1(pre_reg_cnt[196]),
        .I2(pre_reg_cnt[195]),
        .O(d0_i_312_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFDFFFFFFFC)) 
    d0_i_313
       (.I0(pre_reg_cnt[202]),
        .I1(pre_reg_cnt[203]),
        .I2(d0_i_400_n_0),
        .I3(d0_i_132_n_0),
        .I4(d0_i_515_n_0),
        .I5(pre_reg_cnt[201]),
        .O(d0_i_313_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_314
       (.I0(d0_i_402_n_0),
        .I1(d0_i_410_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_516_n_0),
        .I4(d0_i_417_n_0),
        .I5(d0_i_517_n_0),
        .O(d0_i_314_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_315
       (.I0(pre_reg_cnt[170]),
        .I1(pre_reg_cnt[169]),
        .I2(pre_reg_cnt[171]),
        .I3(pre_reg_cnt[168]),
        .I4(d0_i_518_n_0),
        .O(d0_i_315_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    d0_i_316
       (.I0(pre_reg_cnt[170]),
        .I1(pre_reg_cnt[169]),
        .O(d0_i_316_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_317
       (.I0(pre_reg_cnt[171]),
        .I1(d0_i_417_n_0),
        .I2(d0_i_516_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_456_n_0),
        .I5(pre_reg_cnt[172]),
        .O(d0_i_317_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_318
       (.I0(pre_reg_cnt[174]),
        .I1(pre_reg_cnt[175]),
        .O(d0_i_318_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_319
       (.I0(d0_i_402_n_0),
        .I1(d0_i_401_n_0),
        .I2(d0_i_400_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_516_n_0),
        .O(d0_i_319_n_0));
  LUT5 #(
    .INIT(32'h8A75758A)) 
    d0_i_32
       (.I0(d0_i_79_n_0),
        .I1(d0_i_63_n_0),
        .I2(d0_i_80_n_0),
        .I3(d0_i_81_n_0),
        .I4(d0_i_82_n_0),
        .O(\q_reg[271]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_320
       (.I0(pre_reg_cnt[188]),
        .I1(pre_reg_cnt[189]),
        .I2(pre_reg_cnt[190]),
        .I3(pre_reg_cnt[191]),
        .O(d0_i_320_n_0));
  LUT4 #(
    .INIT(16'h000B)) 
    d0_i_321
       (.I0(pre_reg_cnt[178]),
        .I1(pre_reg_cnt[177]),
        .I2(pre_reg_cnt[179]),
        .I3(pre_reg_cnt[180]),
        .O(d0_i_321_n_0));
  LUT6 #(
    .INIT(64'hCFCDCFCDCFCCCFCD)) 
    d0_i_322
       (.I0(pre_reg_cnt[161]),
        .I1(d0_i_468_n_0),
        .I2(pre_reg_cnt[163]),
        .I3(pre_reg_cnt[162]),
        .I4(pre_reg_cnt[159]),
        .I5(d0_i_298_n_0),
        .O(d0_i_322_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_323
       (.I0(d0_i_315_n_0),
        .I1(d0_i_516_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_400_n_0),
        .I4(d0_i_401_n_0),
        .I5(d0_i_402_n_0),
        .O(d0_i_323_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_324
       (.I0(pre_reg_cnt[272]),
        .I1(pre_reg_cnt[273]),
        .I2(d0_i_519_n_0),
        .I3(d0_i_520_n_0),
        .I4(d0_i_521_n_0),
        .I5(d0_i_522_n_0),
        .O(d0_i_324_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_325
       (.I0(d0_i_472_n_0),
        .I1(d0_i_523_n_0),
        .I2(pre_reg_cnt[264]),
        .I3(pre_reg_cnt[265]),
        .I4(d0_i_524_n_0),
        .I5(d0_i_525_n_0),
        .O(d0_i_325_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    d0_i_326
       (.I0(d0_i_526_n_0),
        .I1(d0_i_527_n_0),
        .I2(pre_reg_cnt[312]),
        .I3(pre_reg_cnt[313]),
        .I4(d0_i_528_n_0),
        .I5(d0_i_529_n_0),
        .O(d0_i_326_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_327
       (.I0(d0_i_504_n_0),
        .I1(pre_reg_cnt[291]),
        .I2(pre_reg_cnt[290]),
        .I3(pre_reg_cnt[289]),
        .I4(pre_reg_cnt[288]),
        .O(d0_i_327_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_328
       (.I0(d0_i_510_n_0),
        .I1(pre_reg_cnt[297]),
        .I2(pre_reg_cnt[296]),
        .I3(pre_reg_cnt[299]),
        .I4(pre_reg_cnt[298]),
        .O(d0_i_328_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_329
       (.I0(d0_i_530_n_0),
        .I1(d0_i_531_n_0),
        .I2(d0_i_479_n_0),
        .I3(d0_i_478_n_0),
        .I4(pre_reg_cnt[240]),
        .I5(pre_reg_cnt[241]),
        .O(d0_i_329_n_0));
  LUT5 #(
    .INIT(32'h8AFF7500)) 
    d0_i_33
       (.I0(d0_i_83_n_0),
        .I1(d0_i_84_n_0),
        .I2(d0_i_85_n_0),
        .I3(d0_i_86_n_0),
        .I4(d0_i_87_n_0),
        .O(\q_reg[109]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_330
       (.I0(pre_reg_cnt[222]),
        .I1(pre_reg_cnt[223]),
        .I2(pre_reg_cnt[220]),
        .I3(pre_reg_cnt[221]),
        .O(d0_i_330_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_331
       (.I0(d0_i_532_n_0),
        .I1(pre_reg_cnt[296]),
        .I2(pre_reg_cnt[297]),
        .I3(d0_i_510_n_0),
        .I4(d0_i_533_n_0),
        .I5(d0_i_504_n_0),
        .O(d0_i_331_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_332
       (.I0(d0_i_534_n_0),
        .I1(d0_i_535_n_0),
        .I2(pre_reg_cnt[224]),
        .I3(pre_reg_cnt[225]),
        .I4(d0_i_464_n_0),
        .I5(d0_i_429_n_0),
        .O(d0_i_332_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF0000FF0D)) 
    d0_i_333
       (.I0(pre_reg_cnt[267]),
        .I1(pre_reg_cnt[268]),
        .I2(pre_reg_cnt[269]),
        .I3(pre_reg_cnt[270]),
        .I4(pre_reg_cnt[271]),
        .I5(d0_i_212_n_0),
        .O(d0_i_333_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_334
       (.I0(pre_reg_cnt[277]),
        .I1(pre_reg_cnt[276]),
        .I2(pre_reg_cnt[279]),
        .I3(pre_reg_cnt[278]),
        .I4(d0_i_231_n_0),
        .O(d0_i_334_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    d0_i_335
       (.I0(pre_reg_cnt[282]),
        .I1(pre_reg_cnt[283]),
        .I2(d0_i_459_n_0),
        .I3(pre_reg_cnt[281]),
        .I4(pre_reg_cnt[280]),
        .I5(pre_reg_cnt[279]),
        .O(d0_i_335_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    d0_i_336
       (.I0(pre_reg_cnt[279]),
        .I1(d0_i_231_n_0),
        .O(d0_i_336_n_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    d0_i_337
       (.I0(d0_i_522_n_0),
        .I1(d0_i_338_n_0),
        .I2(pre_reg_cnt[283]),
        .I3(pre_reg_cnt[282]),
        .O(d0_i_337_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_338
       (.I0(d0_i_326_n_0),
        .I1(d0_i_504_n_0),
        .I2(d0_i_536_n_0),
        .I3(pre_reg_cnt[289]),
        .I4(pre_reg_cnt[288]),
        .I5(d0_i_328_n_0),
        .O(d0_i_338_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    d0_i_339
       (.I0(pre_reg_cnt[293]),
        .I1(pre_reg_cnt[294]),
        .I2(pre_reg_cnt[295]),
        .I3(d0_i_284_n_0),
        .I4(pre_reg_cnt[292]),
        .I5(pre_reg_cnt[291]),
        .O(d0_i_339_n_0));
  LUT5 #(
    .INIT(32'hBA4545BA)) 
    d0_i_34
       (.I0(d0_i_88_n_0),
        .I1(d0_i_89_n_0),
        .I2(d0_i_72_n_0),
        .I3(d0_i_90_n_0),
        .I4(d0_i_91_n_0),
        .O(\q_reg[34]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000045)) 
    d0_i_340
       (.I0(pre_reg_cnt[317]),
        .I1(pre_reg_cnt[316]),
        .I2(pre_reg_cnt[315]),
        .I3(pre_reg_cnt[318]),
        .I4(pre_reg_cnt[319]),
        .I5(d0_i_537_n_0),
        .O(d0_i_340_n_0));
  LUT5 #(
    .INIT(32'hFFFEFFFF)) 
    d0_i_341
       (.I0(pre_reg_cnt[313]),
        .I1(pre_reg_cnt[312]),
        .I2(pre_reg_cnt[315]),
        .I3(pre_reg_cnt[314]),
        .I4(d0_i_526_n_0),
        .O(d0_i_341_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000FFFFFF0D)) 
    d0_i_342
       (.I0(pre_reg_cnt[303]),
        .I1(d0_i_326_n_0),
        .I2(pre_reg_cnt[305]),
        .I3(pre_reg_cnt[306]),
        .I4(d0_i_275_n_0),
        .I5(pre_reg_cnt[307]),
        .O(d0_i_342_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_343
       (.I0(pre_reg_cnt[133]),
        .I1(pre_reg_cnt[132]),
        .I2(pre_reg_cnt[135]),
        .I3(pre_reg_cnt[134]),
        .I4(d0_i_241_n_0),
        .O(d0_i_343_n_0));
  LUT5 #(
    .INIT(32'h05050504)) 
    d0_i_344
       (.I0(d0_i_200_n_0),
        .I1(pre_reg_cnt[126]),
        .I2(pre_reg_cnt[127]),
        .I3(pre_reg_cnt[124]),
        .I4(pre_reg_cnt[125]),
        .O(d0_i_344_n_0));
  LUT6 #(
    .INIT(64'h00FF00FF00FF0001)) 
    d0_i_345
       (.I0(pre_reg_cnt[108]),
        .I1(pre_reg_cnt[107]),
        .I2(pre_reg_cnt[106]),
        .I3(d0_i_229_n_0),
        .I4(pre_reg_cnt[109]),
        .I5(d0_i_230_n_0),
        .O(d0_i_345_n_0));
  LUT6 #(
    .INIT(64'h0000111000001111)) 
    d0_i_346
       (.I0(d0_i_474_n_0),
        .I1(d0_i_538_n_0),
        .I2(pre_reg_cnt[55]),
        .I3(d0_i_237_n_0),
        .I4(d0_i_216_n_0),
        .I5(d0_i_389_n_0),
        .O(d0_i_346_n_0));
  LUT6 #(
    .INIT(64'hFFFFFF00FFFFFF0E)) 
    d0_i_347
       (.I0(d0_i_539_n_0),
        .I1(pre_reg_cnt[72]),
        .I2(pre_reg_cnt[73]),
        .I3(d0_i_394_n_0),
        .I4(d0_i_393_n_0),
        .I5(d0_i_392_n_0),
        .O(d0_i_347_n_0));
  LUT4 #(
    .INIT(16'hF0E0)) 
    d0_i_348
       (.I0(pre_reg_cnt[290]),
        .I1(pre_reg_cnt[291]),
        .I2(d0_i_501_n_0),
        .I3(pre_reg_cnt[289]),
        .O(d0_i_348_n_0));
  LUT6 #(
    .INIT(64'h0000000F0000000E)) 
    d0_i_349
       (.I0(pre_reg_cnt[271]),
        .I1(d0_i_212_n_0),
        .I2(d0_i_334_n_0),
        .I3(pre_reg_cnt[275]),
        .I4(pre_reg_cnt[274]),
        .I5(d0_i_540_n_0),
        .O(d0_i_349_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    d0_i_35
       (.I0(\q_reg[188]_0 ),
        .I1(d0_i_92_n_0),
        .I2(d0_i_93_n_0),
        .O(d0_i_93_0));
  LUT6 #(
    .INIT(64'h0011001000110011)) 
    d0_i_350
       (.I0(pre_reg_cnt[239]),
        .I1(pre_reg_cnt[238]),
        .I2(pre_reg_cnt[235]),
        .I3(d0_i_125_n_0),
        .I4(d0_i_535_n_0),
        .I5(d0_i_541_n_0),
        .O(d0_i_350_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_351
       (.I0(pre_reg_cnt[221]),
        .I1(pre_reg_cnt[220]),
        .I2(pre_reg_cnt[223]),
        .I3(pre_reg_cnt[222]),
        .I4(d0_i_132_n_0),
        .O(d0_i_351_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_352
       (.I0(d0_i_326_n_0),
        .I1(pre_reg_cnt[302]),
        .I2(pre_reg_cnt[303]),
        .O(d0_i_352_n_0));
  LUT6 #(
    .INIT(64'hAAFFAAFFAAFFFEFF)) 
    d0_i_353
       (.I0(d0_i_489_n_0),
        .I1(pre_reg_cnt[312]),
        .I2(pre_reg_cnt[313]),
        .I3(d0_i_526_n_0),
        .I4(pre_reg_cnt[314]),
        .I5(pre_reg_cnt[315]),
        .O(d0_i_353_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    d0_i_354
       (.I0(pre_reg_cnt[7]),
        .I1(pre_reg_cnt[6]),
        .O(d0_i_354_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_355
       (.I0(d0_i_462_n_0),
        .I1(pre_reg_cnt[9]),
        .I2(pre_reg_cnt[8]),
        .I3(pre_reg_cnt[11]),
        .I4(pre_reg_cnt[10]),
        .O(d0_i_355_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_356
       (.I0(pre_reg_cnt[12]),
        .I1(pre_reg_cnt[13]),
        .O(d0_i_356_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_357
       (.I0(pre_reg_cnt[15]),
        .I1(pre_reg_cnt[14]),
        .O(d0_i_357_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_358
       (.I0(d0_i_463_n_0),
        .I1(pre_reg_cnt[16]),
        .I2(pre_reg_cnt[24]),
        .I3(pre_reg_cnt[25]),
        .I4(d0_i_378_n_0),
        .I5(d0_i_269_n_0),
        .O(d0_i_358_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_359
       (.I0(d0_i_290_n_0),
        .I1(d0_i_438_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_456_n_0),
        .I4(pre_reg_cnt[119]),
        .I5(d0_i_542_n_0),
        .O(d0_i_359_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    d0_i_36
       (.I0(d0_i_94_n_0),
        .I1(d0_i_95_n_0),
        .I2(d0_i_96_n_0),
        .O(d0_i_96_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_360
       (.I0(d0_i_420_n_0),
        .I1(d0_i_402_n_0),
        .I2(d0_i_410_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_438_n_0),
        .I5(d0_i_290_n_0),
        .O(d0_i_360_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_361
       (.I0(pre_reg_cnt[114]),
        .I1(pre_reg_cnt[115]),
        .O(d0_i_361_n_0));
  LUT4 #(
    .INIT(16'hFCFE)) 
    d0_i_362
       (.I0(pre_reg_cnt[120]),
        .I1(pre_reg_cnt[123]),
        .I2(pre_reg_cnt[122]),
        .I3(pre_reg_cnt[121]),
        .O(d0_i_362_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_363
       (.I0(pre_reg_cnt[126]),
        .I1(pre_reg_cnt[127]),
        .I2(pre_reg_cnt[124]),
        .I3(pre_reg_cnt[125]),
        .O(d0_i_363_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_364
       (.I0(d0_i_457_n_0),
        .I1(d0_i_393_n_0),
        .I2(pre_reg_cnt[69]),
        .I3(pre_reg_cnt[68]),
        .I4(pre_reg_cnt[71]),
        .I5(pre_reg_cnt[70]),
        .O(d0_i_364_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_365
       (.I0(pre_reg_cnt[66]),
        .I1(pre_reg_cnt[67]),
        .O(d0_i_365_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_366
       (.I0(pre_reg_cnt[60]),
        .I1(pre_reg_cnt[61]),
        .O(d0_i_366_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_367
       (.I0(d0_i_216_n_0),
        .I1(pre_reg_cnt[62]),
        .I2(pre_reg_cnt[63]),
        .O(d0_i_367_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    d0_i_368
       (.I0(pre_reg_cnt[7]),
        .I1(pre_reg_cnt[6]),
        .I2(pre_reg_cnt[5]),
        .I3(pre_reg_cnt[4]),
        .I4(pre_reg_cnt[3]),
        .O(d0_i_368_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_369
       (.I0(pre_reg_cnt[318]),
        .I1(pre_reg_cnt[319]),
        .O(d0_i_369_n_0));
  LUT6 #(
    .INIT(64'h55FF55FD00FF0000)) 
    d0_i_37
       (.I0(d0_i_76_n_0),
        .I1(d0_i_97_n_0),
        .I2(d0_i_98_n_0),
        .I3(d0_i_99_n_0),
        .I4(d0_i_100_n_0),
        .I5(d0_i_101_n_0),
        .O(\q_reg[43]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_370
       (.I0(pre_reg_cnt[0]),
        .I1(pre_reg_cnt[1]),
        .O(d0_i_370_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    d0_i_371
       (.I0(pre_reg_cnt[3]),
        .I1(pre_reg_cnt[2]),
        .O(d0_i_371_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_372
       (.I0(pre_reg_cnt[4]),
        .I1(pre_reg_cnt[7]),
        .I2(pre_reg_cnt[6]),
        .I3(pre_reg_cnt[5]),
        .O(d0_i_372_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_373
       (.I0(pre_reg_cnt[22]),
        .I1(pre_reg_cnt[23]),
        .I2(pre_reg_cnt[20]),
        .I3(pre_reg_cnt[21]),
        .O(d0_i_373_n_0));
  LUT6 #(
    .INIT(64'h0001000000010001)) 
    d0_i_374
       (.I0(pre_reg_cnt[18]),
        .I1(pre_reg_cnt[17]),
        .I2(pre_reg_cnt[19]),
        .I3(d0_i_373_n_0),
        .I4(pre_reg_cnt[16]),
        .I5(pre_reg_cnt[15]),
        .O(d0_i_374_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    d0_i_375
       (.I0(pre_reg_cnt[23]),
        .I1(pre_reg_cnt[22]),
        .I2(pre_reg_cnt[21]),
        .O(d0_i_375_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_376
       (.I0(d0_i_502_n_0),
        .I1(d0_i_456_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_467_n_0),
        .I4(d0_i_438_n_0),
        .I5(d0_i_543_n_0),
        .O(d0_i_376_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_377
       (.I0(pre_reg_cnt[24]),
        .I1(pre_reg_cnt[25]),
        .O(d0_i_377_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_378
       (.I0(pre_reg_cnt[31]),
        .I1(pre_reg_cnt[28]),
        .I2(pre_reg_cnt[30]),
        .I3(pre_reg_cnt[29]),
        .I4(pre_reg_cnt[26]),
        .I5(pre_reg_cnt[27]),
        .O(d0_i_378_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_379
       (.I0(pre_reg_cnt[27]),
        .I1(pre_reg_cnt[28]),
        .O(d0_i_379_n_0));
  LUT5 #(
    .INIT(32'h2D2D222D)) 
    d0_i_38
       (.I0(\q_reg[31]_0 ),
        .I1(d0_i_102_n_0),
        .I2(d0_i_103_n_0),
        .I3(d0_i_104_n_0),
        .I4(d0_i_105_n_0),
        .O(\q_reg[295]_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_380
       (.I0(pre_reg_cnt[30]),
        .I1(pre_reg_cnt[29]),
        .O(d0_i_380_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_381
       (.I0(pre_reg_cnt[33]),
        .I1(pre_reg_cnt[34]),
        .O(d0_i_381_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_382
       (.I0(pre_reg_cnt[39]),
        .I1(pre_reg_cnt[38]),
        .O(d0_i_382_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_383
       (.I0(d0_i_414_n_0),
        .I1(d0_i_476_n_0),
        .I2(d0_i_456_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_467_n_0),
        .I5(d0_i_438_n_0),
        .O(d0_i_383_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_384
       (.I0(pre_reg_cnt[43]),
        .I1(pre_reg_cnt[42]),
        .O(d0_i_384_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_385
       (.I0(pre_reg_cnt[46]),
        .I1(pre_reg_cnt[47]),
        .I2(pre_reg_cnt[44]),
        .I3(pre_reg_cnt[45]),
        .O(d0_i_385_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_386
       (.I0(d0_i_438_n_0),
        .I1(d0_i_467_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_410_n_0),
        .I4(d0_i_402_n_0),
        .I5(d0_i_476_n_0),
        .O(d0_i_386_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    d0_i_387
       (.I0(pre_reg_cnt[50]),
        .I1(pre_reg_cnt[49]),
        .O(d0_i_387_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_388
       (.I0(pre_reg_cnt[49]),
        .I1(pre_reg_cnt[50]),
        .I2(pre_reg_cnt[48]),
        .I3(pre_reg_cnt[51]),
        .O(d0_i_388_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_389
       (.I0(pre_reg_cnt[53]),
        .I1(pre_reg_cnt[54]),
        .I2(pre_reg_cnt[52]),
        .I3(pre_reg_cnt[55]),
        .O(d0_i_389_n_0));
  LUT6 #(
    .INIT(64'h000088F888F888F8)) 
    d0_i_39
       (.I0(d0_i_106_n_0),
        .I1(d0_i_70_n_0),
        .I2(d0_i_107_n_0),
        .I3(d0_i_108_n_0),
        .I4(\q_reg[188]_0 ),
        .I5(d0_i_109_n_0),
        .O(\q_reg[55]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_390
       (.I0(pre_reg_cnt[42]),
        .I1(pre_reg_cnt[43]),
        .O(d0_i_390_n_0));
  LUT3 #(
    .INIT(8'h45)) 
    d0_i_391
       (.I0(pre_reg_cnt[71]),
        .I1(pre_reg_cnt[70]),
        .I2(pre_reg_cnt[69]),
        .O(d0_i_391_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_392
       (.I0(pre_reg_cnt[75]),
        .I1(pre_reg_cnt[74]),
        .O(d0_i_392_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_393
       (.I0(d0_i_402_n_0),
        .I1(d0_i_410_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_438_n_0),
        .I4(d0_i_455_n_0),
        .I5(d0_i_395_n_0),
        .O(d0_i_393_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_394
       (.I0(pre_reg_cnt[77]),
        .I1(pre_reg_cnt[78]),
        .I2(pre_reg_cnt[76]),
        .I3(pre_reg_cnt[79]),
        .O(d0_i_394_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_395
       (.I0(d0_i_544_n_0),
        .I1(d0_i_412_n_0),
        .I2(pre_reg_cnt[80]),
        .I3(pre_reg_cnt[81]),
        .I4(d0_i_484_n_0),
        .I5(d0_i_430_n_0),
        .O(d0_i_395_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    d0_i_396
       (.I0(pre_reg_cnt[53]),
        .I1(pre_reg_cnt[54]),
        .I2(pre_reg_cnt[52]),
        .O(d0_i_396_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_397
       (.I0(d0_i_426_n_0),
        .I1(pre_reg_cnt[152]),
        .I2(pre_reg_cnt[153]),
        .I3(d0_i_450_n_0),
        .I4(d0_i_545_n_0),
        .I5(d0_i_409_n_0),
        .O(d0_i_397_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_398
       (.I0(d0_i_427_n_0),
        .I1(pre_reg_cnt[161]),
        .I2(pre_reg_cnt[160]),
        .I3(d0_i_546_n_0),
        .I4(d0_i_315_n_0),
        .I5(d0_i_516_n_0),
        .O(d0_i_398_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_399
       (.I0(d0_i_328_n_0),
        .I1(d0_i_327_n_0),
        .I2(d0_i_326_n_0),
        .I3(d0_i_232_n_0),
        .I4(d0_i_547_n_0),
        .I5(d0_i_324_n_0),
        .O(d0_i_399_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFBA00)) 
    d0_i_40
       (.I0(d0_i_110_n_0),
        .I1(d0_i_111_n_0),
        .I2(d0_i_112_n_0),
        .I3(d0_i_102_n_0),
        .I4(d0_i_113_n_0),
        .I5(d0_i_114_n_0),
        .O(\q_reg[132]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_400
       (.I0(d0_i_548_n_0),
        .I1(pre_reg_cnt[216]),
        .I2(pre_reg_cnt[217]),
        .I3(d0_i_330_n_0),
        .I4(d0_i_549_n_0),
        .I5(d0_i_309_n_0),
        .O(d0_i_400_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_401
       (.I0(d0_i_308_n_0),
        .I1(pre_reg_cnt[194]),
        .I2(pre_reg_cnt[195]),
        .I3(pre_reg_cnt[192]),
        .I4(pre_reg_cnt[193]),
        .I5(d0_i_473_n_0),
        .O(d0_i_401_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_402
       (.I0(d0_i_429_n_0),
        .I1(d0_i_464_n_0),
        .I2(pre_reg_cnt[225]),
        .I3(pre_reg_cnt[224]),
        .I4(d0_i_541_n_0),
        .I5(d0_i_329_n_0),
        .O(d0_i_402_n_0));
  LUT6 #(
    .INIT(64'h00000000D0D000D0)) 
    d0_i_403
       (.I0(d0_i_80_n_0),
        .I1(d0_i_63_n_0),
        .I2(d0_i_79_n_0),
        .I3(d0_i_220_n_0),
        .I4(d0_i_219_n_0),
        .I5(d0_i_550_n_0),
        .O(d0_i_403_n_0));
  LUT6 #(
    .INIT(64'hD0D000D000000000)) 
    d0_i_404
       (.I0(d0_i_80_n_0),
        .I1(d0_i_63_n_0),
        .I2(d0_i_79_n_0),
        .I3(d0_i_225_n_0),
        .I4(d0_i_551_n_0),
        .I5(d0_i_221_n_0),
        .O(d0_i_404_n_0));
  LUT6 #(
    .INIT(64'h2F2FD02FD0D0D0D0)) 
    d0_i_405
       (.I0(d0_i_80_n_0),
        .I1(d0_i_63_n_0),
        .I2(d0_i_79_n_0),
        .I3(d0_i_225_n_0),
        .I4(d0_i_551_n_0),
        .I5(d0_i_221_n_0),
        .O(d0_i_405_n_0));
  LUT6 #(
    .INIT(64'h2A2A2A2A2A2A2A00)) 
    d0_i_406
       (.I0(d0_i_217_n_0),
        .I1(d0_i_241_n_0),
        .I2(d0_i_175_n_0),
        .I3(pre_reg_cnt[55]),
        .I4(d0_i_237_n_0),
        .I5(d0_i_216_n_0),
        .O(d0_i_406_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_407
       (.I0(pre_reg_cnt[109]),
        .I1(d0_i_229_n_0),
        .I2(pre_reg_cnt[111]),
        .I3(pre_reg_cnt[110]),
        .O(d0_i_407_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_408
       (.I0(pre_reg_cnt[163]),
        .I1(d0_i_323_n_0),
        .I2(pre_reg_cnt[166]),
        .I3(pre_reg_cnt[167]),
        .I4(pre_reg_cnt[164]),
        .I5(pre_reg_cnt[165]),
        .O(d0_i_408_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_409
       (.I0(pre_reg_cnt[149]),
        .I1(pre_reg_cnt[150]),
        .I2(pre_reg_cnt[148]),
        .I3(pre_reg_cnt[151]),
        .O(d0_i_409_n_0));
  LUT6 #(
    .INIT(64'hBBBB0B00BBBBBBBB)) 
    d0_i_41
       (.I0(d0_i_115_n_0),
        .I1(d0_i_116_n_0),
        .I2(d0_i_117_n_0),
        .I3(d0_i_118_n_0),
        .I4(d0_i_119_n_0),
        .I5(d0_i_120_n_0),
        .O(\q_reg[188]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_410
       (.I0(d0_i_400_n_0),
        .I1(d0_i_473_n_0),
        .I2(pre_reg_cnt[193]),
        .I3(pre_reg_cnt[192]),
        .I4(d0_i_552_n_0),
        .I5(d0_i_308_n_0),
        .O(d0_i_410_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_411
       (.I0(pre_reg_cnt[155]),
        .I1(pre_reg_cnt[154]),
        .I2(pre_reg_cnt[153]),
        .I3(pre_reg_cnt[152]),
        .I4(d0_i_426_n_0),
        .O(d0_i_411_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_412
       (.I0(pre_reg_cnt[94]),
        .I1(pre_reg_cnt[95]),
        .I2(pre_reg_cnt[92]),
        .I3(pre_reg_cnt[93]),
        .O(d0_i_412_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_413
       (.I0(d0_i_412_n_0),
        .I1(pre_reg_cnt[89]),
        .I2(pre_reg_cnt[88]),
        .I3(pre_reg_cnt[91]),
        .I4(pre_reg_cnt[90]),
        .O(d0_i_413_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_414
       (.I0(d0_i_385_n_0),
        .I1(pre_reg_cnt[41]),
        .I2(pre_reg_cnt[40]),
        .I3(pre_reg_cnt[43]),
        .I4(pre_reg_cnt[42]),
        .O(d0_i_414_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_415
       (.I0(pre_reg_cnt[35]),
        .I1(pre_reg_cnt[37]),
        .I2(pre_reg_cnt[36]),
        .I3(pre_reg_cnt[39]),
        .I4(pre_reg_cnt[38]),
        .O(d0_i_415_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    d0_i_416
       (.I0(pre_reg_cnt[170]),
        .I1(pre_reg_cnt[169]),
        .O(d0_i_416_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_417
       (.I0(pre_reg_cnt[173]),
        .I1(pre_reg_cnt[174]),
        .I2(pre_reg_cnt[175]),
        .O(d0_i_417_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_418
       (.I0(pre_reg_cnt[119]),
        .I1(pre_reg_cnt[118]),
        .O(d0_i_418_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_419
       (.I0(d0_i_290_n_0),
        .I1(d0_i_438_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_400_n_0),
        .I4(d0_i_401_n_0),
        .I5(d0_i_402_n_0),
        .O(d0_i_419_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00001011)) 
    d0_i_42
       (.I0(d0_i_121_n_0),
        .I1(d0_i_122_n_0),
        .I2(pre_reg_cnt[184]),
        .I3(pre_reg_cnt[183]),
        .I4(d0_i_123_n_0),
        .I5(d0_i_124_n_0),
        .O(\q_reg[184]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_420
       (.I0(pre_reg_cnt[116]),
        .I1(pre_reg_cnt[117]),
        .I2(pre_reg_cnt[118]),
        .I3(pre_reg_cnt[119]),
        .O(d0_i_420_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_421
       (.I0(d0_i_363_n_0),
        .I1(d0_i_438_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_400_n_0),
        .I4(d0_i_401_n_0),
        .I5(d0_i_402_n_0),
        .O(d0_i_421_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    d0_i_422
       (.I0(pre_reg_cnt[121]),
        .I1(pre_reg_cnt[122]),
        .I2(pre_reg_cnt[123]),
        .O(d0_i_422_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_423
       (.I0(d0_i_546_n_0),
        .I1(d0_i_315_n_0),
        .I2(d0_i_516_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_456_n_0),
        .I5(d0_i_427_n_0),
        .O(d0_i_423_n_0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    d0_i_424
       (.I0(pre_reg_cnt[163]),
        .I1(d0_i_315_n_0),
        .I2(d0_i_516_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_456_n_0),
        .I5(d0_i_427_n_0),
        .O(d0_i_424_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_425
       (.I0(d0_i_239_n_0),
        .I1(d0_i_397_n_0),
        .I2(d0_i_398_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_456_n_0),
        .I5(d0_i_164_n_0),
        .O(d0_i_425_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_426
       (.I0(pre_reg_cnt[158]),
        .I1(pre_reg_cnt[159]),
        .I2(pre_reg_cnt[156]),
        .I3(pre_reg_cnt[157]),
        .O(d0_i_426_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_427
       (.I0(pre_reg_cnt[166]),
        .I1(pre_reg_cnt[167]),
        .I2(pre_reg_cnt[164]),
        .I3(pre_reg_cnt[165]),
        .O(d0_i_427_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_428
       (.I0(pre_reg_cnt[234]),
        .I1(pre_reg_cnt[235]),
        .I2(pre_reg_cnt[232]),
        .I3(pre_reg_cnt[233]),
        .I4(d0_i_535_n_0),
        .I5(d0_i_125_n_0),
        .O(d0_i_428_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_429
       (.I0(pre_reg_cnt[228]),
        .I1(pre_reg_cnt[229]),
        .I2(pre_reg_cnt[230]),
        .I3(pre_reg_cnt[231]),
        .O(d0_i_429_n_0));
  LUT6 #(
    .INIT(64'h0000000000001011)) 
    d0_i_43
       (.I0(d0_i_125_n_0),
        .I1(pre_reg_cnt[239]),
        .I2(pre_reg_cnt[238]),
        .I3(pre_reg_cnt[237]),
        .I4(d0_i_126_n_0),
        .I5(d0_i_127_n_0),
        .O(\q_reg[239]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_430
       (.I0(pre_reg_cnt[86]),
        .I1(pre_reg_cnt[87]),
        .I2(pre_reg_cnt[84]),
        .I3(pre_reg_cnt[85]),
        .O(d0_i_430_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_431
       (.I0(pre_reg_cnt[172]),
        .I1(d0_i_402_n_0),
        .I2(d0_i_410_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_516_n_0),
        .I5(d0_i_417_n_0),
        .O(d0_i_431_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_432
       (.I0(pre_reg_cnt[145]),
        .I1(d0_i_409_n_0),
        .I2(d0_i_298_n_0),
        .I3(d0_i_411_n_0),
        .I4(pre_reg_cnt[147]),
        .I5(pre_reg_cnt[146]),
        .O(d0_i_432_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_433
       (.I0(pre_reg_cnt[91]),
        .I1(d0_i_305_n_0),
        .I2(pre_reg_cnt[94]),
        .I3(pre_reg_cnt[95]),
        .I4(pre_reg_cnt[92]),
        .I5(pre_reg_cnt[93]),
        .O(d0_i_433_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_434
       (.I0(pre_reg_cnt[73]),
        .I1(d0_i_394_n_0),
        .I2(d0_i_395_n_0),
        .I3(d0_i_305_n_0),
        .I4(pre_reg_cnt[75]),
        .I5(pre_reg_cnt[74]),
        .O(d0_i_434_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_435
       (.I0(d0_i_463_n_0),
        .I1(pre_reg_cnt[16]),
        .I2(d0_i_376_n_0),
        .I3(d0_i_355_n_0),
        .I4(d0_i_553_n_0),
        .I5(pre_reg_cnt[4]),
        .O(d0_i_435_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_436
       (.I0(pre_reg_cnt[19]),
        .I1(d0_i_376_n_0),
        .I2(pre_reg_cnt[22]),
        .I3(pre_reg_cnt[23]),
        .I4(pre_reg_cnt[20]),
        .I5(pre_reg_cnt[21]),
        .O(d0_i_436_n_0));
  LUT6 #(
    .INIT(64'h0000000000000E00)) 
    d0_i_437
       (.I0(d0_i_268_n_0),
        .I1(d0_i_269_n_0),
        .I2(d0_i_382_n_0),
        .I3(d0_i_386_n_0),
        .I4(d0_i_414_n_0),
        .I5(pre_reg_cnt[37]),
        .O(d0_i_437_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_438
       (.I0(d0_i_516_n_0),
        .I1(d0_i_315_n_0),
        .I2(d0_i_554_n_0),
        .I3(d0_i_397_n_0),
        .I4(d0_i_452_n_0),
        .I5(d0_i_555_n_0),
        .O(d0_i_438_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_439
       (.I0(d0_i_402_n_0),
        .I1(d0_i_410_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_398_n_0),
        .I4(d0_i_426_n_0),
        .I5(d0_i_514_n_0),
        .O(d0_i_439_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF5554)) 
    d0_i_44
       (.I0(d0_i_128_n_0),
        .I1(d0_i_129_n_0),
        .I2(pre_reg_cnt[215]),
        .I3(d0_i_130_n_0),
        .I4(d0_i_131_n_0),
        .I5(d0_i_132_n_0),
        .O(\q_reg[215]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_440
       (.I0(d0_i_402_n_0),
        .I1(d0_i_410_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_193_n_0),
        .I4(d0_i_233_n_0),
        .I5(d0_i_556_n_0),
        .O(d0_i_440_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    d0_i_441
       (.I0(d0_i_399_n_0),
        .I1(d0_i_410_n_0),
        .I2(d0_i_402_n_0),
        .I3(pre_reg_cnt[191]),
        .I4(pre_reg_cnt[190]),
        .I5(pre_reg_cnt[189]),
        .O(d0_i_441_n_0));
  LUT6 #(
    .INIT(64'h000000000000000E)) 
    d0_i_442
       (.I0(d0_i_473_n_0),
        .I1(pre_reg_cnt[199]),
        .I2(d0_i_515_n_0),
        .I3(d0_i_132_n_0),
        .I4(d0_i_400_n_0),
        .I5(d0_i_557_n_0),
        .O(d0_i_442_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_443
       (.I0(d0_i_308_n_0),
        .I1(d0_i_400_n_0),
        .I2(d0_i_132_n_0),
        .I3(d0_i_473_n_0),
        .O(d0_i_443_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_444
       (.I0(pre_reg_cnt[211]),
        .I1(d0_i_558_n_0),
        .I2(d0_i_132_n_0),
        .I3(d0_i_129_n_0),
        .I4(pre_reg_cnt[213]),
        .I5(pre_reg_cnt[212]),
        .O(d0_i_444_n_0));
  LUT5 #(
    .INIT(32'h000000FE)) 
    d0_i_445
       (.I0(pre_reg_cnt[206]),
        .I1(pre_reg_cnt[207]),
        .I2(pre_reg_cnt[205]),
        .I3(d0_i_132_n_0),
        .I4(d0_i_400_n_0),
        .O(d0_i_445_n_0));
  LUT6 #(
    .INIT(64'h000000000000000E)) 
    d0_i_446
       (.I0(d0_i_320_n_0),
        .I1(pre_reg_cnt[187]),
        .I2(d0_i_235_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_410_n_0),
        .I5(d0_i_402_n_0),
        .O(d0_i_446_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_447
       (.I0(pre_reg_cnt[193]),
        .I1(d0_i_308_n_0),
        .I2(d0_i_400_n_0),
        .I3(d0_i_132_n_0),
        .I4(d0_i_473_n_0),
        .I5(d0_i_552_n_0),
        .O(d0_i_447_n_0));
  LUT6 #(
    .INIT(64'h00000000000000FD)) 
    d0_i_448
       (.I0(d0_i_191_n_0),
        .I1(pre_reg_cnt[181]),
        .I2(d0_i_233_n_0),
        .I3(d0_i_193_n_0),
        .I4(d0_i_399_n_0),
        .I5(d0_i_456_n_0),
        .O(d0_i_448_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_449
       (.I0(pre_reg_cnt[151]),
        .I1(d0_i_402_n_0),
        .I2(d0_i_410_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_398_n_0),
        .I5(d0_i_411_n_0),
        .O(d0_i_449_n_0));
  LUT6 #(
    .INIT(64'h4445FFFF44454445)) 
    d0_i_45
       (.I0(d0_i_109_n_0),
        .I1(d0_i_133_n_0),
        .I2(d0_i_134_n_0),
        .I3(d0_i_135_n_0),
        .I4(d0_i_104_n_0),
        .I5(d0_i_136_n_0),
        .O(\q_reg[247]_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_450
       (.I0(pre_reg_cnt[155]),
        .I1(pre_reg_cnt[154]),
        .O(d0_i_450_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_451
       (.I0(pre_reg_cnt[159]),
        .I1(pre_reg_cnt[158]),
        .O(d0_i_451_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_452
       (.I0(pre_reg_cnt[137]),
        .I1(pre_reg_cnt[136]),
        .I2(pre_reg_cnt[139]),
        .I3(pre_reg_cnt[138]),
        .I4(d0_i_164_n_0),
        .O(d0_i_452_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_453
       (.I0(pre_reg_cnt[135]),
        .I1(pre_reg_cnt[134]),
        .O(d0_i_453_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_454
       (.I0(pre_reg_cnt[150]),
        .I1(pre_reg_cnt[149]),
        .O(d0_i_454_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_455
       (.I0(d0_i_470_n_0),
        .I1(d0_i_226_n_0),
        .I2(d0_i_227_n_0),
        .I3(pre_reg_cnt[97]),
        .I4(pre_reg_cnt[96]),
        .I5(d0_i_559_n_0),
        .O(d0_i_455_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_456
       (.I0(d0_i_329_n_0),
        .I1(d0_i_541_n_0),
        .I2(d0_i_560_n_0),
        .I3(d0_i_561_n_0),
        .I4(d0_i_473_n_0),
        .I5(d0_i_400_n_0),
        .O(d0_i_456_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_457
       (.I0(pre_reg_cnt[73]),
        .I1(pre_reg_cnt[72]),
        .I2(pre_reg_cnt[75]),
        .I3(pre_reg_cnt[74]),
        .I4(d0_i_394_n_0),
        .O(d0_i_457_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_458
       (.I0(d0_i_395_n_0),
        .I1(d0_i_214_n_0),
        .I2(pre_reg_cnt[65]),
        .I3(pre_reg_cnt[64]),
        .I4(d0_i_511_n_0),
        .I5(d0_i_457_n_0),
        .O(d0_i_458_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_459
       (.I0(d0_i_328_n_0),
        .I1(d0_i_327_n_0),
        .I2(d0_i_326_n_0),
        .I3(d0_i_522_n_0),
        .O(d0_i_459_n_0));
  LUT5 #(
    .INIT(32'h007171FF)) 
    d0_i_46
       (.I0(\q_reg[55]_0 ),
        .I1(\q_reg[295]_0 ),
        .I2(\q_reg[43]_0 ),
        .I3(d0_i_49_n_0),
        .I4(d0_i_48_n_0),
        .O(d0_i_46_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_460
       (.I0(d0_i_326_n_0),
        .I1(pre_reg_cnt[303]),
        .I2(pre_reg_cnt[302]),
        .I3(pre_reg_cnt[301]),
        .I4(pre_reg_cnt[300]),
        .O(d0_i_460_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_461
       (.I0(pre_reg_cnt[247]),
        .I1(pre_reg_cnt[246]),
        .I2(pre_reg_cnt[245]),
        .I3(pre_reg_cnt[244]),
        .I4(d0_i_480_n_0),
        .I5(d0_i_399_n_0),
        .O(d0_i_461_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_462
       (.I0(pre_reg_cnt[14]),
        .I1(pre_reg_cnt[15]),
        .I2(pre_reg_cnt[12]),
        .I3(pre_reg_cnt[13]),
        .O(d0_i_462_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_463
       (.I0(d0_i_373_n_0),
        .I1(pre_reg_cnt[19]),
        .I2(pre_reg_cnt[17]),
        .I3(pre_reg_cnt[18]),
        .O(d0_i_463_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_464
       (.I0(pre_reg_cnt[227]),
        .I1(pre_reg_cnt[226]),
        .O(d0_i_464_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_465
       (.I0(pre_reg_cnt[231]),
        .I1(pre_reg_cnt[230]),
        .O(d0_i_465_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_466
       (.I0(pre_reg_cnt[223]),
        .I1(d0_i_132_n_0),
        .O(d0_i_466_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_467
       (.I0(d0_i_562_n_0),
        .I1(d0_i_226_n_0),
        .I2(d0_i_470_n_0),
        .I3(d0_i_457_n_0),
        .I4(d0_i_563_n_0),
        .I5(d0_i_395_n_0),
        .O(d0_i_467_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_468
       (.I0(d0_i_427_n_0),
        .I1(d0_i_402_n_0),
        .I2(d0_i_410_n_0),
        .I3(d0_i_399_n_0),
        .I4(d0_i_516_n_0),
        .I5(d0_i_315_n_0),
        .O(d0_i_468_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_469
       (.I0(pre_reg_cnt[108]),
        .I1(pre_reg_cnt[109]),
        .I2(pre_reg_cnt[110]),
        .I3(pre_reg_cnt[111]),
        .O(d0_i_469_n_0));
  LUT3 #(
    .INIT(8'h69)) 
    d0_i_47
       (.I0(d0_i_78_n_0),
        .I1(d0_i_77_n_0),
        .I2(d0_i_76_n_0),
        .O(d0_i_47_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_470
       (.I0(d0_i_363_n_0),
        .I1(d0_i_564_n_0),
        .I2(d0_i_420_n_0),
        .I3(d0_i_565_n_0),
        .I4(pre_reg_cnt[112]),
        .I5(pre_reg_cnt[113]),
        .O(d0_i_470_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_471
       (.I0(d0_i_522_n_0),
        .I1(pre_reg_cnt[281]),
        .I2(pre_reg_cnt[280]),
        .I3(pre_reg_cnt[283]),
        .I4(pre_reg_cnt[282]),
        .O(d0_i_471_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_472
       (.I0(pre_reg_cnt[268]),
        .I1(pre_reg_cnt[269]),
        .I2(pre_reg_cnt[270]),
        .I3(pre_reg_cnt[271]),
        .O(d0_i_472_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_473
       (.I0(d0_i_515_n_0),
        .I1(pre_reg_cnt[201]),
        .I2(pre_reg_cnt[200]),
        .I3(pre_reg_cnt[203]),
        .I4(pre_reg_cnt[202]),
        .O(d0_i_473_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_474
       (.I0(pre_reg_cnt[62]),
        .I1(pre_reg_cnt[63]),
        .I2(pre_reg_cnt[60]),
        .I3(pre_reg_cnt[61]),
        .O(d0_i_474_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_475
       (.I0(pre_reg_cnt[54]),
        .I1(pre_reg_cnt[55]),
        .O(d0_i_475_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_476
       (.I0(d0_i_474_n_0),
        .I1(d0_i_538_n_0),
        .I2(pre_reg_cnt[56]),
        .I3(pre_reg_cnt[57]),
        .I4(d0_i_388_n_0),
        .I5(d0_i_389_n_0),
        .O(d0_i_476_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_477
       (.I0(pre_reg_cnt[38]),
        .I1(pre_reg_cnt[39]),
        .I2(pre_reg_cnt[36]),
        .I3(pre_reg_cnt[37]),
        .O(d0_i_477_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_478
       (.I0(pre_reg_cnt[243]),
        .I1(pre_reg_cnt[242]),
        .O(d0_i_478_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_479
       (.I0(pre_reg_cnt[244]),
        .I1(pre_reg_cnt[245]),
        .I2(pre_reg_cnt[246]),
        .I3(pre_reg_cnt[247]),
        .O(d0_i_479_n_0));
  LUT3 #(
    .INIT(8'h2B)) 
    d0_i_48
       (.I0(\q_reg[131]_0 ),
        .I1(\q_reg[132]_0 ),
        .I2(\q_reg[295]_1 ),
        .O(d0_i_48_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_480
       (.I0(pre_reg_cnt[249]),
        .I1(pre_reg_cnt[248]),
        .I2(pre_reg_cnt[251]),
        .I3(pre_reg_cnt[250]),
        .I4(d0_i_530_n_0),
        .O(d0_i_480_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_481
       (.I0(pre_reg_cnt[255]),
        .I1(pre_reg_cnt[254]),
        .O(d0_i_481_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_482
       (.I0(d0_i_232_n_0),
        .I1(d0_i_324_n_0),
        .I2(d0_i_326_n_0),
        .I3(d0_i_327_n_0),
        .I4(d0_i_328_n_0),
        .O(d0_i_482_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_483
       (.I0(pre_reg_cnt[16]),
        .I1(pre_reg_cnt[18]),
        .I2(pre_reg_cnt[17]),
        .I3(pre_reg_cnt[19]),
        .I4(d0_i_373_n_0),
        .O(d0_i_483_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_484
       (.I0(pre_reg_cnt[83]),
        .I1(pre_reg_cnt[82]),
        .O(d0_i_484_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_485
       (.I0(pre_reg_cnt[87]),
        .I1(pre_reg_cnt[86]),
        .O(d0_i_485_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    d0_i_486
       (.I0(pre_reg_cnt[23]),
        .I1(pre_reg_cnt[22]),
        .O(d0_i_486_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_487
       (.I0(pre_reg_cnt[226]),
        .I1(pre_reg_cnt[227]),
        .I2(d0_i_541_n_0),
        .I3(d0_i_125_n_0),
        .I4(d0_i_429_n_0),
        .O(d0_i_487_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_488
       (.I0(pre_reg_cnt[230]),
        .I1(pre_reg_cnt[231]),
        .I2(d0_i_125_n_0),
        .I3(d0_i_541_n_0),
        .O(d0_i_488_n_0));
  LUT5 #(
    .INIT(32'hCCC8FFFF)) 
    d0_i_489
       (.I0(pre_reg_cnt[307]),
        .I1(d0_i_566_n_0),
        .I2(pre_reg_cnt[309]),
        .I3(pre_reg_cnt[308]),
        .I4(d0_i_326_n_0),
        .O(d0_i_489_n_0));
  LUT4 #(
    .INIT(16'h0444)) 
    d0_i_49
       (.I0(d0_i_103_n_0),
        .I1(\q_reg[31]_0 ),
        .I2(d0_i_47_n_0),
        .I3(d0_i_105_n_0),
        .O(d0_i_49_n_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    d0_i_490
       (.I0(pre_reg_cnt[89]),
        .I1(pre_reg_cnt[90]),
        .I2(pre_reg_cnt[88]),
        .I3(pre_reg_cnt[87]),
        .O(d0_i_490_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    d0_i_491
       (.I0(pre_reg_cnt[95]),
        .I1(pre_reg_cnt[94]),
        .I2(pre_reg_cnt[93]),
        .O(d0_i_491_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_492
       (.I0(pre_reg_cnt[255]),
        .I1(pre_reg_cnt[254]),
        .I2(d0_i_399_n_0),
        .I3(pre_reg_cnt[253]),
        .I4(pre_reg_cnt[252]),
        .I5(pre_reg_cnt[251]),
        .O(d0_i_492_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_493
       (.I0(pre_reg_cnt[249]),
        .I1(pre_reg_cnt[250]),
        .O(d0_i_493_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_494
       (.I0(pre_reg_cnt[255]),
        .I1(d0_i_399_n_0),
        .O(d0_i_494_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    d0_i_495
       (.I0(pre_reg_cnt[261]),
        .I1(pre_reg_cnt[260]),
        .I2(pre_reg_cnt[263]),
        .I3(pre_reg_cnt[262]),
        .I4(d0_i_482_n_0),
        .O(d0_i_495_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    d0_i_496
       (.I0(pre_reg_cnt[257]),
        .I1(pre_reg_cnt[258]),
        .I2(pre_reg_cnt[259]),
        .O(d0_i_496_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_497
       (.I0(pre_reg_cnt[37]),
        .I1(d0_i_414_n_0),
        .I2(d0_i_476_n_0),
        .I3(d0_i_216_n_0),
        .I4(pre_reg_cnt[39]),
        .I5(pre_reg_cnt[38]),
        .O(d0_i_497_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_498
       (.I0(d0_i_399_n_0),
        .I1(pre_reg_cnt[254]),
        .I2(pre_reg_cnt[255]),
        .O(d0_i_498_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_499
       (.I0(pre_reg_cnt[235]),
        .I1(d0_i_125_n_0),
        .I2(pre_reg_cnt[236]),
        .I3(pre_reg_cnt[237]),
        .I4(pre_reg_cnt[238]),
        .I5(pre_reg_cnt[239]),
        .O(d0_i_499_n_0));
  LUT6 #(
    .INIT(64'hAEAEAEAEAEAEAE00)) 
    d0_i_50
       (.I0(d0_i_137_n_0),
        .I1(d0_i_138_n_0),
        .I2(d0_i_139_n_0),
        .I3(d0_i_77_n_0),
        .I4(d0_i_140_n_0),
        .I5(d0_i_141_n_0),
        .O(\q_reg[131]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    d0_i_500
       (.I0(pre_reg_cnt[307]),
        .I1(d0_i_341_n_0),
        .I2(pre_reg_cnt[310]),
        .I3(pre_reg_cnt[311]),
        .I4(pre_reg_cnt[309]),
        .I5(pre_reg_cnt[308]),
        .O(d0_i_500_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    d0_i_501
       (.I0(pre_reg_cnt[295]),
        .I1(pre_reg_cnt[294]),
        .I2(pre_reg_cnt[293]),
        .I3(pre_reg_cnt[292]),
        .I4(d0_i_284_n_0),
        .O(d0_i_501_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_502
       (.I0(d0_i_476_n_0),
        .I1(d0_i_567_n_0),
        .I2(d0_i_414_n_0),
        .O(d0_i_502_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_503
       (.I0(pre_reg_cnt[11]),
        .I1(pre_reg_cnt[10]),
        .O(d0_i_503_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_504
       (.I0(pre_reg_cnt[292]),
        .I1(pre_reg_cnt[293]),
        .I2(pre_reg_cnt[294]),
        .I3(pre_reg_cnt[295]),
        .O(d0_i_504_n_0));
  LUT4 #(
    .INIT(16'h0051)) 
    d0_i_505
       (.I0(d0_i_482_n_0),
        .I1(pre_reg_cnt[261]),
        .I2(pre_reg_cnt[262]),
        .I3(pre_reg_cnt[263]),
        .O(d0_i_505_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    d0_i_506
       (.I0(pre_reg_cnt[115]),
        .I1(pre_reg_cnt[114]),
        .I2(pre_reg_cnt[113]),
        .I3(pre_reg_cnt[112]),
        .I4(pre_reg_cnt[111]),
        .O(d0_i_506_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    d0_i_507
       (.I0(pre_reg_cnt[107]),
        .I1(pre_reg_cnt[106]),
        .I2(pre_reg_cnt[105]),
        .O(d0_i_507_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF0D)) 
    d0_i_508
       (.I0(d0_i_568_n_0),
        .I1(pre_reg_cnt[126]),
        .I2(pre_reg_cnt[127]),
        .I3(d0_i_456_n_0),
        .I4(d0_i_399_n_0),
        .I5(d0_i_438_n_0),
        .O(d0_i_508_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_509
       (.I0(pre_reg_cnt[134]),
        .I1(pre_reg_cnt[135]),
        .I2(pre_reg_cnt[132]),
        .I3(pre_reg_cnt[133]),
        .O(d0_i_509_n_0));
  LUT6 #(
    .INIT(64'hF2F200F200F200F2)) 
    d0_i_51
       (.I0(d0_i_142_n_0),
        .I1(d0_i_88_n_0),
        .I2(d0_i_143_n_0),
        .I3(d0_i_78_n_0),
        .I4(d0_i_144_n_0),
        .I5(d0_i_145_n_0),
        .O(\q_reg[295]_1 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_510
       (.I0(pre_reg_cnt[300]),
        .I1(pre_reg_cnt[301]),
        .I2(pre_reg_cnt[302]),
        .I3(pre_reg_cnt[303]),
        .O(d0_i_510_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_511
       (.I0(pre_reg_cnt[67]),
        .I1(pre_reg_cnt[66]),
        .O(d0_i_511_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_512
       (.I0(pre_reg_cnt[108]),
        .I1(pre_reg_cnt[109]),
        .O(d0_i_512_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFCFFFFFFFE)) 
    d0_i_513
       (.I0(pre_reg_cnt[264]),
        .I1(pre_reg_cnt[266]),
        .I2(pre_reg_cnt[267]),
        .I3(d0_i_212_n_0),
        .I4(d0_i_472_n_0),
        .I5(pre_reg_cnt[265]),
        .O(d0_i_513_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    d0_i_514
       (.I0(pre_reg_cnt[155]),
        .I1(pre_reg_cnt[154]),
        .I2(pre_reg_cnt[153]),
        .O(d0_i_514_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_515
       (.I0(pre_reg_cnt[204]),
        .I1(pre_reg_cnt[205]),
        .I2(pre_reg_cnt[206]),
        .I3(pre_reg_cnt[207]),
        .O(d0_i_515_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_516
       (.I0(d0_i_569_n_0),
        .I1(d0_i_320_n_0),
        .I2(d0_i_570_n_0),
        .I3(d0_i_233_n_0),
        .I4(pre_reg_cnt[180]),
        .I5(pre_reg_cnt[181]),
        .O(d0_i_516_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_517
       (.I0(pre_reg_cnt[171]),
        .I1(pre_reg_cnt[172]),
        .O(d0_i_517_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_518
       (.I0(pre_reg_cnt[174]),
        .I1(pre_reg_cnt[175]),
        .I2(pre_reg_cnt[172]),
        .I3(pre_reg_cnt[173]),
        .O(d0_i_518_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_519
       (.I0(pre_reg_cnt[275]),
        .I1(pre_reg_cnt[274]),
        .O(d0_i_519_n_0));
  LUT3 #(
    .INIT(8'h8A)) 
    d0_i_52
       (.I0(d0_i_146_n_0),
        .I1(d0_i_143_n_0),
        .I2(d0_i_147_n_0),
        .O(\q_reg[19]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_520
       (.I0(pre_reg_cnt[278]),
        .I1(pre_reg_cnt[279]),
        .I2(pre_reg_cnt[276]),
        .I3(pre_reg_cnt[277]),
        .O(d0_i_520_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_521
       (.I0(pre_reg_cnt[282]),
        .I1(pre_reg_cnt[283]),
        .I2(pre_reg_cnt[280]),
        .I3(pre_reg_cnt[281]),
        .O(d0_i_521_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_522
       (.I0(pre_reg_cnt[286]),
        .I1(pre_reg_cnt[287]),
        .I2(pre_reg_cnt[284]),
        .I3(pre_reg_cnt[285]),
        .O(d0_i_522_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_523
       (.I0(pre_reg_cnt[267]),
        .I1(pre_reg_cnt[266]),
        .O(d0_i_523_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_524
       (.I0(pre_reg_cnt[258]),
        .I1(pre_reg_cnt[259]),
        .I2(pre_reg_cnt[256]),
        .I3(pre_reg_cnt[257]),
        .O(d0_i_524_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_525
       (.I0(pre_reg_cnt[262]),
        .I1(pre_reg_cnt[263]),
        .I2(pre_reg_cnt[260]),
        .I3(pre_reg_cnt[261]),
        .O(d0_i_525_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    d0_i_526
       (.I0(pre_reg_cnt[319]),
        .I1(pre_reg_cnt[318]),
        .I2(pre_reg_cnt[316]),
        .I3(pre_reg_cnt[317]),
        .O(d0_i_526_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_527
       (.I0(pre_reg_cnt[315]),
        .I1(pre_reg_cnt[314]),
        .O(d0_i_527_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_528
       (.I0(pre_reg_cnt[308]),
        .I1(pre_reg_cnt[309]),
        .I2(pre_reg_cnt[305]),
        .I3(pre_reg_cnt[306]),
        .O(d0_i_528_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_529
       (.I0(pre_reg_cnt[310]),
        .I1(pre_reg_cnt[311]),
        .I2(pre_reg_cnt[304]),
        .I3(pre_reg_cnt[307]),
        .O(d0_i_529_n_0));
  LUT6 #(
    .INIT(64'hEEEEEEEEEEEE000E)) 
    d0_i_53
       (.I0(d0_i_148_n_0),
        .I1(d0_i_149_n_0),
        .I2(d0_i_150_n_0),
        .I3(d0_i_151_n_0),
        .I4(d0_i_152_n_0),
        .I5(d0_i_153_n_0),
        .O(\q_reg[31]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_530
       (.I0(pre_reg_cnt[254]),
        .I1(pre_reg_cnt[255]),
        .I2(pre_reg_cnt[252]),
        .I3(pre_reg_cnt[253]),
        .O(d0_i_530_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_531
       (.I0(pre_reg_cnt[250]),
        .I1(pre_reg_cnt[251]),
        .I2(pre_reg_cnt[248]),
        .I3(pre_reg_cnt[249]),
        .O(d0_i_531_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_532
       (.I0(pre_reg_cnt[299]),
        .I1(pre_reg_cnt[298]),
        .O(d0_i_532_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_533
       (.I0(pre_reg_cnt[288]),
        .I1(pre_reg_cnt[289]),
        .I2(pre_reg_cnt[290]),
        .I3(pre_reg_cnt[291]),
        .O(d0_i_533_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_534
       (.I0(pre_reg_cnt[234]),
        .I1(pre_reg_cnt[235]),
        .I2(pre_reg_cnt[232]),
        .I3(pre_reg_cnt[233]),
        .O(d0_i_534_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_535
       (.I0(pre_reg_cnt[236]),
        .I1(pre_reg_cnt[237]),
        .I2(pre_reg_cnt[238]),
        .I3(pre_reg_cnt[239]),
        .O(d0_i_535_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_536
       (.I0(pre_reg_cnt[291]),
        .I1(pre_reg_cnt[290]),
        .O(d0_i_536_n_0));
  LUT3 #(
    .INIT(8'h8A)) 
    d0_i_537
       (.I0(d0_i_259_n_0),
        .I1(pre_reg_cnt[313]),
        .I2(pre_reg_cnt[312]),
        .O(d0_i_537_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_538
       (.I0(pre_reg_cnt[59]),
        .I1(pre_reg_cnt[58]),
        .O(d0_i_538_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_539
       (.I0(pre_reg_cnt[71]),
        .I1(pre_reg_cnt[70]),
        .O(d0_i_539_n_0));
  LUT6 #(
    .INIT(64'h0000000002000202)) 
    d0_i_54
       (.I0(d0_i_154_n_0),
        .I1(pre_reg_cnt[77]),
        .I2(pre_reg_cnt[78]),
        .I3(pre_reg_cnt[76]),
        .I4(pre_reg_cnt[75]),
        .I5(d0_i_155_n_0),
        .O(\q_reg[77]_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    d0_i_540
       (.I0(pre_reg_cnt[271]),
        .I1(pre_reg_cnt[270]),
        .I2(pre_reg_cnt[269]),
        .I3(pre_reg_cnt[268]),
        .I4(d0_i_212_n_0),
        .O(d0_i_540_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_541
       (.I0(d0_i_535_n_0),
        .I1(pre_reg_cnt[233]),
        .I2(pre_reg_cnt[232]),
        .I3(pre_reg_cnt[235]),
        .I4(pre_reg_cnt[234]),
        .O(d0_i_541_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_542
       (.I0(pre_reg_cnt[117]),
        .I1(pre_reg_cnt[118]),
        .O(d0_i_542_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_543
       (.I0(pre_reg_cnt[24]),
        .I1(pre_reg_cnt[25]),
        .I2(d0_i_378_n_0),
        .O(d0_i_543_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_544
       (.I0(pre_reg_cnt[90]),
        .I1(pre_reg_cnt[91]),
        .I2(pre_reg_cnt[88]),
        .I3(pre_reg_cnt[89]),
        .O(d0_i_544_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_545
       (.I0(pre_reg_cnt[144]),
        .I1(pre_reg_cnt[145]),
        .I2(pre_reg_cnt[146]),
        .I3(pre_reg_cnt[147]),
        .O(d0_i_545_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_546
       (.I0(pre_reg_cnt[163]),
        .I1(pre_reg_cnt[162]),
        .O(d0_i_546_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_547
       (.I0(d0_i_525_n_0),
        .I1(pre_reg_cnt[257]),
        .I2(pre_reg_cnt[256]),
        .I3(pre_reg_cnt[259]),
        .I4(pre_reg_cnt[258]),
        .O(d0_i_547_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_548
       (.I0(pre_reg_cnt[219]),
        .I1(pre_reg_cnt[218]),
        .O(d0_i_548_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_549
       (.I0(pre_reg_cnt[214]),
        .I1(pre_reg_cnt[215]),
        .I2(pre_reg_cnt[208]),
        .I3(pre_reg_cnt[209]),
        .O(d0_i_549_n_0));
  LUT6 #(
    .INIT(64'h00000000FFFFFFAE)) 
    d0_i_55
       (.I0(d0_i_156_n_0),
        .I1(pre_reg_cnt[57]),
        .I2(pre_reg_cnt[58]),
        .I3(pre_reg_cnt[59]),
        .I4(d0_i_157_n_0),
        .I5(d0_i_158_n_0),
        .O(\q_reg[57]_0 ));
  LUT4 #(
    .INIT(16'hEE0E)) 
    d0_i_550
       (.I0(pre_reg_cnt[271]),
        .I1(d0_i_212_n_0),
        .I2(d0_i_218_n_0),
        .I3(d0_i_217_n_0),
        .O(d0_i_550_n_0));
  LUT6 #(
    .INIT(64'h0000000101010101)) 
    d0_i_551
       (.I0(d0_i_430_n_0),
        .I1(d0_i_571_n_0),
        .I2(d0_i_484_n_0),
        .I3(d0_i_269_n_0),
        .I4(d0_i_268_n_0),
        .I5(d0_i_224_n_0),
        .O(d0_i_551_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_552
       (.I0(pre_reg_cnt[195]),
        .I1(pre_reg_cnt[194]),
        .O(d0_i_552_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    d0_i_553
       (.I0(pre_reg_cnt[5]),
        .I1(pre_reg_cnt[6]),
        .I2(pre_reg_cnt[7]),
        .O(d0_i_553_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_554
       (.I0(d0_i_427_n_0),
        .I1(pre_reg_cnt[161]),
        .I2(pre_reg_cnt[160]),
        .I3(pre_reg_cnt[163]),
        .I4(pre_reg_cnt[162]),
        .O(d0_i_554_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_555
       (.I0(d0_i_509_n_0),
        .I1(pre_reg_cnt[131]),
        .I2(pre_reg_cnt[130]),
        .I3(pre_reg_cnt[129]),
        .I4(pre_reg_cnt[128]),
        .O(d0_i_555_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    d0_i_556
       (.I0(pre_reg_cnt[180]),
        .I1(pre_reg_cnt[181]),
        .O(d0_i_556_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_557
       (.I0(pre_reg_cnt[203]),
        .I1(pre_reg_cnt[202]),
        .O(d0_i_557_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_558
       (.I0(pre_reg_cnt[215]),
        .I1(pre_reg_cnt[214]),
        .O(d0_i_558_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_559
       (.I0(pre_reg_cnt[99]),
        .I1(pre_reg_cnt[98]),
        .O(d0_i_559_n_0));
  LUT5 #(
    .INIT(32'h7DD71441)) 
    d0_i_56
       (.I0(d0_i_159_n_0),
        .I1(d0_i_160_n_0),
        .I2(d0_i_161_n_0),
        .I3(d0_i_162_n_0),
        .I4(d0_i_163_n_0),
        .O(d0_i_163_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_560
       (.I0(d0_i_429_n_0),
        .I1(pre_reg_cnt[227]),
        .I2(pre_reg_cnt[226]),
        .I3(pre_reg_cnt[225]),
        .I4(pre_reg_cnt[224]),
        .O(d0_i_560_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_561
       (.I0(pre_reg_cnt[193]),
        .I1(pre_reg_cnt[192]),
        .I2(pre_reg_cnt[195]),
        .I3(pre_reg_cnt[194]),
        .I4(d0_i_308_n_0),
        .O(d0_i_561_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_562
       (.I0(d0_i_227_n_0),
        .I1(pre_reg_cnt[97]),
        .I2(pre_reg_cnt[96]),
        .I3(pre_reg_cnt[99]),
        .I4(pre_reg_cnt[98]),
        .O(d0_i_562_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_563
       (.I0(d0_i_214_n_0),
        .I1(pre_reg_cnt[65]),
        .I2(pre_reg_cnt[64]),
        .I3(pre_reg_cnt[67]),
        .I4(pre_reg_cnt[66]),
        .O(d0_i_563_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_564
       (.I0(pre_reg_cnt[122]),
        .I1(pre_reg_cnt[123]),
        .I2(pre_reg_cnt[120]),
        .I3(pre_reg_cnt[121]),
        .O(d0_i_564_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    d0_i_565
       (.I0(pre_reg_cnt[115]),
        .I1(pre_reg_cnt[114]),
        .O(d0_i_565_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    d0_i_566
       (.I0(d0_i_341_n_0),
        .I1(pre_reg_cnt[310]),
        .I2(pre_reg_cnt[311]),
        .O(d0_i_566_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    d0_i_567
       (.I0(pre_reg_cnt[33]),
        .I1(pre_reg_cnt[32]),
        .I2(pre_reg_cnt[35]),
        .I3(pre_reg_cnt[34]),
        .I4(d0_i_477_n_0),
        .O(d0_i_567_n_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    d0_i_568
       (.I0(pre_reg_cnt[125]),
        .I1(pre_reg_cnt[126]),
        .I2(pre_reg_cnt[124]),
        .I3(pre_reg_cnt[123]),
        .O(d0_i_568_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_569
       (.I0(pre_reg_cnt[186]),
        .I1(pre_reg_cnt[187]),
        .I2(pre_reg_cnt[184]),
        .I3(pre_reg_cnt[185]),
        .O(d0_i_569_n_0));
  LUT4 #(
    .INIT(16'h9666)) 
    d0_i_57
       (.I0(d0_i_66_n_0),
        .I1(d0_i_65_n_0),
        .I2(d0_i_59_n_0),
        .I3(d0_i_64_n_0),
        .O(d0_i_64_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    d0_i_570
       (.I0(pre_reg_cnt[178]),
        .I1(pre_reg_cnt[179]),
        .I2(pre_reg_cnt[176]),
        .I3(pre_reg_cnt[177]),
        .O(d0_i_570_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    d0_i_571
       (.I0(d0_i_402_n_0),
        .I1(d0_i_410_n_0),
        .I2(d0_i_399_n_0),
        .I3(d0_i_438_n_0),
        .I4(d0_i_455_n_0),
        .I5(d0_i_413_n_0),
        .O(d0_i_571_n_0));
  LUT6 #(
    .INIT(64'h0000FFFEFFFEFFFE)) 
    d0_i_58
       (.I0(d0_i_164_n_0),
        .I1(d0_i_165_n_0),
        .I2(pre_reg_cnt[137]),
        .I3(d0_i_166_n_0),
        .I4(d0_i_63_n_0),
        .I5(d0_i_62_n_0),
        .O(\q_reg[137]_0 ));
  LUT6 #(
    .INIT(64'h32FF323200320000)) 
    d0_i_59
       (.I0(d0_i_72_n_0),
        .I1(d0_i_71_n_0),
        .I2(d0_i_70_n_0),
        .I3(d0_i_67_n_0),
        .I4(d0_i_68_n_0),
        .I5(d0_i_69_n_0),
        .O(d0_i_59_n_0));
  LUT5 #(
    .INIT(32'h2BD4D42B)) 
    d0_i_60
       (.I0(d0_i_75_n_0),
        .I1(d0_i_74_n_0),
        .I2(d0_i_73_n_0),
        .I3(d0_i_159_n_0),
        .I4(d0_i_167_n_0),
        .O(d0_i_60_n_0));
  LUT5 #(
    .INIT(32'hFF696900)) 
    d0_i_61
       (.I0(d0_i_75_n_0),
        .I1(d0_i_74_n_0),
        .I2(d0_i_73_n_0),
        .I3(d0_i_168_n_0),
        .I4(\q_reg[91]_0 ),
        .O(d0_i_61_n_0));
  LUT5 #(
    .INIT(32'h0000FDD5)) 
    d0_i_62
       (.I0(d0_i_169_n_0),
        .I1(d0_i_72_n_0),
        .I2(d0_i_170_n_0),
        .I3(d0_i_171_n_0),
        .I4(d0_i_172_n_0),
        .O(d0_i_62_n_0));
  LUT6 #(
    .INIT(64'hFE00FE00FE00FEFE)) 
    d0_i_63
       (.I0(d0_i_165_n_0),
        .I1(pre_reg_cnt[143]),
        .I2(pre_reg_cnt[142]),
        .I3(d0_i_173_n_0),
        .I4(pre_reg_cnt[145]),
        .I5(d0_i_174_n_0),
        .O(d0_i_63_n_0));
  LUT3 #(
    .INIT(8'hB2)) 
    d0_i_64
       (.I0(d0_i_78_0),
        .I1(\q_reg[109]_0 ),
        .I2(\q_reg[271]_0 ),
        .O(d0_i_64_n_0));
  LUT3 #(
    .INIT(8'h17)) 
    d0_i_65
       (.I0(d0_i_160_n_0),
        .I1(d0_i_161_n_0),
        .I2(d0_i_162_n_0),
        .O(d0_i_65_n_0));
  LUT6 #(
    .INIT(64'h022AFDD5FFFF0000)) 
    d0_i_66
       (.I0(d0_i_169_n_0),
        .I1(d0_i_72_n_0),
        .I2(d0_i_170_n_0),
        .I3(d0_i_171_n_0),
        .I4(d0_i_172_n_0),
        .I5(d0_i_175_n_0),
        .O(d0_i_66_n_0));
  LUT3 #(
    .INIT(8'hA8)) 
    d0_i_67
       (.I0(d0_i_176_n_0),
        .I1(d0_i_80_n_0),
        .I2(d0_i_177_n_0),
        .O(d0_i_67_n_0));
  LUT3 #(
    .INIT(8'h8A)) 
    d0_i_68
       (.I0(d0_i_178_n_0),
        .I1(d0_i_63_n_0),
        .I2(d0_i_179_n_0),
        .O(d0_i_68_n_0));
  LUT6 #(
    .INIT(64'h55559AAAAAAA6555)) 
    d0_i_69
       (.I0(d0_i_180_n_0),
        .I1(d0_i_85_n_0),
        .I2(d0_i_181_n_0),
        .I3(d0_i_182_n_0),
        .I4(d0_i_183_n_0),
        .I5(d0_i_121_n_0),
        .O(d0_i_69_n_0));
  LUT5 #(
    .INIT(32'h1011FFFF)) 
    d0_i_70
       (.I0(d0_i_184_n_0),
        .I1(d0_i_185_n_0),
        .I2(pre_reg_cnt[91]),
        .I3(pre_reg_cnt[90]),
        .I4(d0_i_186_n_0),
        .O(d0_i_70_n_0));
  LUT5 #(
    .INIT(32'hF2F2F200)) 
    d0_i_71
       (.I0(pre_reg_cnt[234]),
        .I1(pre_reg_cnt[235]),
        .I2(d0_i_187_n_0),
        .I3(d0_i_188_n_0),
        .I4(d0_i_189_n_0),
        .O(d0_i_71_n_0));
  LUT6 #(
    .INIT(64'h5555555555555444)) 
    d0_i_72
       (.I0(d0_i_190_n_0),
        .I1(d0_i_178_n_0),
        .I2(d0_i_191_n_0),
        .I3(d0_i_192_n_0),
        .I4(d0_i_193_n_0),
        .I5(d0_i_194_n_0),
        .O(d0_i_72_n_0));
  LUT5 #(
    .INIT(32'h71777171)) 
    d0_i_73
       (.I0(d0_i_91_n_0),
        .I1(d0_i_90_n_0),
        .I2(d0_i_88_n_0),
        .I3(d0_i_89_n_0),
        .I4(d0_i_72_n_0),
        .O(d0_i_73_n_0));
  LUT6 #(
    .INIT(64'h000000750075FFFF)) 
    d0_i_74
       (.I0(d0_i_195_n_0),
        .I1(d0_i_196_n_0),
        .I2(d0_i_197_n_0),
        .I3(d0_i_198_n_0),
        .I4(d0_i_94_n_0),
        .I5(d0_i_96_n_0),
        .O(d0_i_74_n_0));
  LUT3 #(
    .INIT(8'hE8)) 
    d0_i_75
       (.I0(d0_i_93_n_0),
        .I1(d0_i_92_n_0),
        .I2(\q_reg[188]_0 ),
        .O(d0_i_75_n_0));
  LUT6 #(
    .INIT(64'h00000000FFFF1F11)) 
    d0_i_76
       (.I0(d0_i_199_n_0),
        .I1(d0_i_200_n_0),
        .I2(d0_i_201_n_0),
        .I3(d0_i_202_n_0),
        .I4(d0_i_203_n_0),
        .I5(d0_i_204_n_0),
        .O(d0_i_76_n_0));
  LUT6 #(
    .INIT(64'h5555555555010101)) 
    d0_i_77
       (.I0(d0_i_205_n_0),
        .I1(d0_i_190_n_0),
        .I2(d0_i_206_n_0),
        .I3(d0_i_207_n_0),
        .I4(d0_i_63_n_0),
        .I5(d0_i_208_n_0),
        .O(d0_i_77_n_0));
  LUT6 #(
    .INIT(64'h0004440455555555)) 
    d0_i_78
       (.I0(d0_i_120_n_0),
        .I1(d0_i_209_n_0),
        .I2(d0_i_210_n_0),
        .I3(d0_i_165_n_0),
        .I4(d0_i_211_n_0),
        .I5(d0_i_121_n_0),
        .O(d0_i_78_n_0));
  LUT3 #(
    .INIT(8'h45)) 
    d0_i_79
       (.I0(d0_i_212_n_0),
        .I1(pre_reg_cnt[271]),
        .I2(pre_reg_cnt[270]),
        .O(d0_i_79_n_0));
  LUT6 #(
    .INIT(64'hFEFEFEFEFFFFFFFE)) 
    d0_i_80
       (.I0(d0_i_213_n_0),
        .I1(d0_i_214_n_0),
        .I2(pre_reg_cnt[67]),
        .I3(pre_reg_cnt[61]),
        .I4(d0_i_215_n_0),
        .I5(d0_i_216_n_0),
        .O(d0_i_80_n_0));
  LUT6 #(
    .INIT(64'h1F1100001F111F11)) 
    d0_i_81
       (.I0(pre_reg_cnt[271]),
        .I1(d0_i_212_n_0),
        .I2(d0_i_217_n_0),
        .I3(d0_i_218_n_0),
        .I4(d0_i_219_n_0),
        .I5(d0_i_220_n_0),
        .O(d0_i_81_n_0));
  LUT5 #(
    .INIT(32'h8088AAAA)) 
    d0_i_82
       (.I0(d0_i_221_n_0),
        .I1(d0_i_222_n_0),
        .I2(d0_i_223_n_0),
        .I3(d0_i_224_n_0),
        .I4(d0_i_225_n_0),
        .O(d0_i_82_n_0));
  LUT6 #(
    .INIT(64'hF0F0E0E0F0F0E0EE)) 
    d0_i_83
       (.I0(d0_i_226_n_0),
        .I1(d0_i_227_n_0),
        .I2(d0_i_228_n_0),
        .I3(pre_reg_cnt[109]),
        .I4(d0_i_229_n_0),
        .I5(d0_i_230_n_0),
        .O(d0_i_83_n_0));
  LUT6 #(
    .INIT(64'hCCDDCCDDCCDDCCD0)) 
    d0_i_84
       (.I0(pre_reg_cnt[271]),
        .I1(d0_i_231_n_0),
        .I2(d0_i_232_n_0),
        .I3(d0_i_212_n_0),
        .I4(pre_reg_cnt[263]),
        .I5(pre_reg_cnt[262]),
        .O(d0_i_84_n_0));
  LUT6 #(
    .INIT(64'hFF00FE00FF00FEFE)) 
    d0_i_85
       (.I0(d0_i_233_n_0),
        .I1(d0_i_193_n_0),
        .I2(pre_reg_cnt[181]),
        .I3(d0_i_234_n_0),
        .I4(d0_i_194_n_0),
        .I5(d0_i_235_n_0),
        .O(d0_i_85_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF000000FE)) 
    d0_i_86
       (.I0(d0_i_236_n_0),
        .I1(pre_reg_cnt[47]),
        .I2(pre_reg_cnt[46]),
        .I3(pre_reg_cnt[55]),
        .I4(d0_i_237_n_0),
        .I5(d0_i_216_n_0),
        .O(d0_i_86_n_0));
  LUT6 #(
    .INIT(64'h888A8888888AAAAA)) 
    d0_i_87
       (.I0(d0_i_238_n_0),
        .I1(d0_i_128_n_0),
        .I2(d0_i_239_n_0),
        .I3(d0_i_240_n_0),
        .I4(d0_i_241_n_0),
        .I5(pre_reg_cnt[135]),
        .O(d0_i_87_n_0));
  LUT5 #(
    .INIT(32'h0000BABB)) 
    d0_i_88
       (.I0(d0_i_242_n_0),
        .I1(d0_i_243_n_0),
        .I2(pre_reg_cnt[34]),
        .I3(pre_reg_cnt[33]),
        .I4(d0_i_151_n_0),
        .O(d0_i_88_n_0));
  LUT5 #(
    .INIT(32'hAAAAAAA8)) 
    d0_i_89
       (.I0(d0_i_244_n_0),
        .I1(d0_i_245_n_0),
        .I2(d0_i_246_n_0),
        .I3(pre_reg_cnt[265]),
        .I4(d0_i_247_n_0),
        .O(d0_i_89_n_0));
  LUT5 #(
    .INIT(32'hAAAA20AA)) 
    d0_i_90
       (.I0(d0_i_248_n_0),
        .I1(d0_i_184_n_0),
        .I2(d0_i_249_n_0),
        .I3(d0_i_186_n_0),
        .I4(d0_i_203_n_0),
        .O(d0_i_90_n_0));
  LUT6 #(
    .INIT(64'hDCCCDCCCCCCCDCCC)) 
    d0_i_91
       (.I0(d0_i_71_n_0),
        .I1(d0_i_100_n_0),
        .I2(d0_i_155_n_0),
        .I3(d0_i_250_n_0),
        .I4(d0_i_251_n_0),
        .I5(d0_i_176_n_0),
        .O(d0_i_91_n_0));
  LUT6 #(
    .INIT(64'h4040400044444444)) 
    d0_i_92
       (.I0(d0_i_252_n_0),
        .I1(d0_i_253_n_0),
        .I2(d0_i_176_n_0),
        .I3(d0_i_80_n_0),
        .I4(d0_i_177_n_0),
        .I5(d0_i_254_n_0),
        .O(d0_i_92_n_0));
  LUT6 #(
    .INIT(64'hFFFFD0FFD0D0D0D0)) 
    d0_i_93
       (.I0(d0_i_245_n_0),
        .I1(d0_i_255_n_0),
        .I2(d0_i_256_n_0),
        .I3(d0_i_179_n_0),
        .I4(d0_i_63_n_0),
        .I5(d0_i_178_n_0),
        .O(d0_i_93_n_0));
  LUT3 #(
    .INIT(8'h45)) 
    d0_i_94
       (.I0(d0_i_257_n_0),
        .I1(d0_i_121_n_0),
        .I2(d0_i_258_n_0),
        .O(d0_i_94_n_0));
  LUT6 #(
    .INIT(64'hFFFF5DFF5D5D5D5D)) 
    d0_i_95
       (.I0(d0_i_259_n_0),
        .I1(d0_i_260_n_0),
        .I2(d0_i_86_n_0),
        .I3(d0_i_197_n_0),
        .I4(d0_i_196_n_0),
        .I5(d0_i_195_n_0),
        .O(d0_i_95_n_0));
  LUT6 #(
    .INIT(64'h2F2F2F2F2F002F2F)) 
    d0_i_96
       (.I0(d0_i_261_n_0),
        .I1(d0_i_238_n_0),
        .I2(d0_i_184_n_0),
        .I3(d0_i_180_n_0),
        .I4(d0_i_262_n_0),
        .I5(d0_i_263_n_0),
        .O(d0_i_96_n_0));
  LUT6 #(
    .INIT(64'h00000000000000F1)) 
    d0_i_97
       (.I0(d0_i_264_n_0),
        .I1(pre_reg_cnt[43]),
        .I2(d0_i_265_n_0),
        .I3(d0_i_266_n_0),
        .I4(d0_i_267_n_0),
        .I5(d0_i_177_n_0),
        .O(d0_i_97_n_0));
  LUT6 #(
    .INIT(64'h0000000050505051)) 
    d0_i_98
       (.I0(pre_reg_cnt[31]),
        .I1(pre_reg_cnt[25]),
        .I2(d0_i_268_n_0),
        .I3(pre_reg_cnt[26]),
        .I4(pre_reg_cnt[27]),
        .I5(d0_i_269_n_0),
        .O(d0_i_98_n_0));
  LUT6 #(
    .INIT(64'h0054FFFFFFFFFFFF)) 
    d0_i_99
       (.I0(d0_i_253_n_0),
        .I1(d0_i_270_n_0),
        .I2(pre_reg_cnt[7]),
        .I3(d0_i_271_n_0),
        .I4(d0_i_272_n_0),
        .I5(d0_i_273_n_0),
        .O(d0_i_99_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [0]),
        .Q(pre_reg_cnt[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[100] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [100]),
        .Q(pre_reg_cnt[100]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[101] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [101]),
        .Q(pre_reg_cnt[101]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[102] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [102]),
        .Q(pre_reg_cnt[102]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[103] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [103]),
        .Q(pre_reg_cnt[103]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[104] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [104]),
        .Q(pre_reg_cnt[104]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[105] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [105]),
        .Q(pre_reg_cnt[105]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[106] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [106]),
        .Q(pre_reg_cnt[106]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[107] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [107]),
        .Q(pre_reg_cnt[107]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[108] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [108]),
        .Q(pre_reg_cnt[108]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[109] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [109]),
        .Q(pre_reg_cnt[109]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[10] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [10]),
        .Q(pre_reg_cnt[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[110] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [110]),
        .Q(pre_reg_cnt[110]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[111] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [111]),
        .Q(pre_reg_cnt[111]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[112] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [112]),
        .Q(pre_reg_cnt[112]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[113] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [113]),
        .Q(pre_reg_cnt[113]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[114] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [114]),
        .Q(pre_reg_cnt[114]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[115] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [115]),
        .Q(pre_reg_cnt[115]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[116] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [116]),
        .Q(pre_reg_cnt[116]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[117] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [117]),
        .Q(pre_reg_cnt[117]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[118] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [118]),
        .Q(pre_reg_cnt[118]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[119] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [119]),
        .Q(pre_reg_cnt[119]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[11] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [11]),
        .Q(pre_reg_cnt[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[120] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [120]),
        .Q(pre_reg_cnt[120]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[121] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [121]),
        .Q(pre_reg_cnt[121]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[122] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [122]),
        .Q(pre_reg_cnt[122]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[123] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [123]),
        .Q(pre_reg_cnt[123]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[124] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [124]),
        .Q(pre_reg_cnt[124]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[125] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [125]),
        .Q(pre_reg_cnt[125]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[126] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [126]),
        .Q(pre_reg_cnt[126]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[127] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [127]),
        .Q(pre_reg_cnt[127]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[128] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [128]),
        .Q(pre_reg_cnt[128]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[129] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [129]),
        .Q(pre_reg_cnt[129]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[12] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [12]),
        .Q(pre_reg_cnt[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[130] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [130]),
        .Q(pre_reg_cnt[130]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[131] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [131]),
        .Q(pre_reg_cnt[131]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[132] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [132]),
        .Q(pre_reg_cnt[132]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[133] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [133]),
        .Q(pre_reg_cnt[133]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[134] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [134]),
        .Q(pre_reg_cnt[134]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[135] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [135]),
        .Q(pre_reg_cnt[135]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[136] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [136]),
        .Q(pre_reg_cnt[136]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[137] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [137]),
        .Q(pre_reg_cnt[137]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[138] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [138]),
        .Q(pre_reg_cnt[138]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[139] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [139]),
        .Q(pre_reg_cnt[139]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[13] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [13]),
        .Q(pre_reg_cnt[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[140] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [140]),
        .Q(pre_reg_cnt[140]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[141] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [141]),
        .Q(pre_reg_cnt[141]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[142] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [142]),
        .Q(pre_reg_cnt[142]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[143] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [143]),
        .Q(pre_reg_cnt[143]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[144] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [144]),
        .Q(pre_reg_cnt[144]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[145] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [145]),
        .Q(pre_reg_cnt[145]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[146] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [146]),
        .Q(pre_reg_cnt[146]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[147] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [147]),
        .Q(pre_reg_cnt[147]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[148] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [148]),
        .Q(pre_reg_cnt[148]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[149] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [149]),
        .Q(pre_reg_cnt[149]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[14] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [14]),
        .Q(pre_reg_cnt[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[150] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [150]),
        .Q(pre_reg_cnt[150]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[151] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [151]),
        .Q(pre_reg_cnt[151]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[152] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [152]),
        .Q(pre_reg_cnt[152]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[153] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [153]),
        .Q(pre_reg_cnt[153]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[154] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [154]),
        .Q(pre_reg_cnt[154]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[155] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [155]),
        .Q(pre_reg_cnt[155]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[156] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [156]),
        .Q(pre_reg_cnt[156]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[157] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [157]),
        .Q(pre_reg_cnt[157]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[158] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [158]),
        .Q(pre_reg_cnt[158]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[159] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [159]),
        .Q(pre_reg_cnt[159]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[15] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [15]),
        .Q(pre_reg_cnt[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[160] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [160]),
        .Q(pre_reg_cnt[160]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[161] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [161]),
        .Q(pre_reg_cnt[161]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[162] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [162]),
        .Q(pre_reg_cnt[162]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[163] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [163]),
        .Q(pre_reg_cnt[163]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[164] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [164]),
        .Q(pre_reg_cnt[164]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[165] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [165]),
        .Q(pre_reg_cnt[165]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[166] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [166]),
        .Q(pre_reg_cnt[166]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[167] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [167]),
        .Q(pre_reg_cnt[167]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[168] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [168]),
        .Q(pre_reg_cnt[168]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[169] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [169]),
        .Q(pre_reg_cnt[169]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[16] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [16]),
        .Q(pre_reg_cnt[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[170] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [170]),
        .Q(pre_reg_cnt[170]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[171] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [171]),
        .Q(pre_reg_cnt[171]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[172] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [172]),
        .Q(pre_reg_cnt[172]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[173] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [173]),
        .Q(pre_reg_cnt[173]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[174] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [174]),
        .Q(pre_reg_cnt[174]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[175] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [175]),
        .Q(pre_reg_cnt[175]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[176] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [176]),
        .Q(pre_reg_cnt[176]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[177] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [177]),
        .Q(pre_reg_cnt[177]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[178] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [178]),
        .Q(pre_reg_cnt[178]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[179] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [179]),
        .Q(pre_reg_cnt[179]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[17] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [17]),
        .Q(pre_reg_cnt[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[180] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [180]),
        .Q(pre_reg_cnt[180]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[181] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [181]),
        .Q(pre_reg_cnt[181]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[182] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [182]),
        .Q(pre_reg_cnt[182]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[183] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [183]),
        .Q(pre_reg_cnt[183]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[184] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [184]),
        .Q(pre_reg_cnt[184]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[185] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [185]),
        .Q(pre_reg_cnt[185]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[186] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [186]),
        .Q(pre_reg_cnt[186]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[187] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [187]),
        .Q(pre_reg_cnt[187]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[188] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [188]),
        .Q(pre_reg_cnt[188]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[189] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [189]),
        .Q(pre_reg_cnt[189]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[18] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [18]),
        .Q(pre_reg_cnt[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[190] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [190]),
        .Q(pre_reg_cnt[190]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[191] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [191]),
        .Q(pre_reg_cnt[191]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[192] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [192]),
        .Q(pre_reg_cnt[192]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[193] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [193]),
        .Q(pre_reg_cnt[193]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[194] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [194]),
        .Q(pre_reg_cnt[194]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[195] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [195]),
        .Q(pre_reg_cnt[195]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[196] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [196]),
        .Q(pre_reg_cnt[196]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[197] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [197]),
        .Q(pre_reg_cnt[197]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[198] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [198]),
        .Q(pre_reg_cnt[198]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[199] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [199]),
        .Q(pre_reg_cnt[199]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[19] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [19]),
        .Q(pre_reg_cnt[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [1]),
        .Q(pre_reg_cnt[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[200] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [200]),
        .Q(pre_reg_cnt[200]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[201] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [201]),
        .Q(pre_reg_cnt[201]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[202] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [202]),
        .Q(pre_reg_cnt[202]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[203] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [203]),
        .Q(pre_reg_cnt[203]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[204] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [204]),
        .Q(pre_reg_cnt[204]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[205] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [205]),
        .Q(pre_reg_cnt[205]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[206] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [206]),
        .Q(pre_reg_cnt[206]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[207] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [207]),
        .Q(pre_reg_cnt[207]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[208] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [208]),
        .Q(pre_reg_cnt[208]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[209] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [209]),
        .Q(pre_reg_cnt[209]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[20] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [20]),
        .Q(pre_reg_cnt[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[210] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [210]),
        .Q(pre_reg_cnt[210]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[211] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [211]),
        .Q(pre_reg_cnt[211]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[212] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [212]),
        .Q(pre_reg_cnt[212]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[213] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [213]),
        .Q(pre_reg_cnt[213]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[214] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [214]),
        .Q(pre_reg_cnt[214]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[215] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [215]),
        .Q(pre_reg_cnt[215]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[216] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [216]),
        .Q(pre_reg_cnt[216]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[217] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [217]),
        .Q(pre_reg_cnt[217]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[218] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [218]),
        .Q(pre_reg_cnt[218]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[219] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [219]),
        .Q(pre_reg_cnt[219]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[21] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [21]),
        .Q(pre_reg_cnt[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[220] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [220]),
        .Q(pre_reg_cnt[220]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[221] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [221]),
        .Q(pre_reg_cnt[221]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[222] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [222]),
        .Q(pre_reg_cnt[222]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[223] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [223]),
        .Q(pre_reg_cnt[223]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[224] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [224]),
        .Q(pre_reg_cnt[224]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[225] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [225]),
        .Q(pre_reg_cnt[225]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[226] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [226]),
        .Q(pre_reg_cnt[226]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[227] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [227]),
        .Q(pre_reg_cnt[227]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[228] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [228]),
        .Q(pre_reg_cnt[228]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[229] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [229]),
        .Q(pre_reg_cnt[229]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[22] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [22]),
        .Q(pre_reg_cnt[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[230] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [230]),
        .Q(pre_reg_cnt[230]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[231] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [231]),
        .Q(pre_reg_cnt[231]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[232] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [232]),
        .Q(pre_reg_cnt[232]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[233] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [233]),
        .Q(pre_reg_cnt[233]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[234] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [234]),
        .Q(pre_reg_cnt[234]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[235] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [235]),
        .Q(pre_reg_cnt[235]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[236] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [236]),
        .Q(pre_reg_cnt[236]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[237] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [237]),
        .Q(pre_reg_cnt[237]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[238] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [238]),
        .Q(pre_reg_cnt[238]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[239] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [239]),
        .Q(pre_reg_cnt[239]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[23] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [23]),
        .Q(pre_reg_cnt[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[240] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [240]),
        .Q(pre_reg_cnt[240]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[241] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [241]),
        .Q(pre_reg_cnt[241]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[242] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [242]),
        .Q(pre_reg_cnt[242]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[243] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [243]),
        .Q(pre_reg_cnt[243]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[244] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [244]),
        .Q(pre_reg_cnt[244]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[245] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [245]),
        .Q(pre_reg_cnt[245]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[246] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [246]),
        .Q(pre_reg_cnt[246]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[247] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [247]),
        .Q(pre_reg_cnt[247]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[248] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [248]),
        .Q(pre_reg_cnt[248]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[249] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [249]),
        .Q(pre_reg_cnt[249]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[24] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [24]),
        .Q(pre_reg_cnt[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[250] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [250]),
        .Q(pre_reg_cnt[250]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[251] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [251]),
        .Q(pre_reg_cnt[251]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[252] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [252]),
        .Q(pre_reg_cnt[252]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[253] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [253]),
        .Q(pre_reg_cnt[253]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[254] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [254]),
        .Q(pre_reg_cnt[254]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[255] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [255]),
        .Q(pre_reg_cnt[255]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[256] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [256]),
        .Q(pre_reg_cnt[256]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[257] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [257]),
        .Q(pre_reg_cnt[257]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[258] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [258]),
        .Q(pre_reg_cnt[258]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[259] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [259]),
        .Q(pre_reg_cnt[259]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[25] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [25]),
        .Q(pre_reg_cnt[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[260] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [260]),
        .Q(pre_reg_cnt[260]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[261] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [261]),
        .Q(pre_reg_cnt[261]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[262] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [262]),
        .Q(pre_reg_cnt[262]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[263] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [263]),
        .Q(pre_reg_cnt[263]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[264] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [264]),
        .Q(pre_reg_cnt[264]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[265] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [265]),
        .Q(pre_reg_cnt[265]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[266] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [266]),
        .Q(pre_reg_cnt[266]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[267] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [267]),
        .Q(pre_reg_cnt[267]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[268] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [268]),
        .Q(pre_reg_cnt[268]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[269] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [269]),
        .Q(pre_reg_cnt[269]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[26] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [26]),
        .Q(pre_reg_cnt[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[270] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [270]),
        .Q(pre_reg_cnt[270]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[271] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [271]),
        .Q(pre_reg_cnt[271]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[272] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [272]),
        .Q(pre_reg_cnt[272]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[273] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [273]),
        .Q(pre_reg_cnt[273]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[274] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [274]),
        .Q(pre_reg_cnt[274]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[275] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [275]),
        .Q(pre_reg_cnt[275]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[276] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [276]),
        .Q(pre_reg_cnt[276]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[277] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [277]),
        .Q(pre_reg_cnt[277]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[278] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [278]),
        .Q(pre_reg_cnt[278]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[279] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [279]),
        .Q(pre_reg_cnt[279]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[27] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [27]),
        .Q(pre_reg_cnt[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[280] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [280]),
        .Q(pre_reg_cnt[280]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[281] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [281]),
        .Q(pre_reg_cnt[281]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[282] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [282]),
        .Q(pre_reg_cnt[282]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[283] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [283]),
        .Q(pre_reg_cnt[283]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[284] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [284]),
        .Q(pre_reg_cnt[284]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[285] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [285]),
        .Q(pre_reg_cnt[285]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[286] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [286]),
        .Q(pre_reg_cnt[286]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[287] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [287]),
        .Q(pre_reg_cnt[287]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[288] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [288]),
        .Q(pre_reg_cnt[288]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[289] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [289]),
        .Q(pre_reg_cnt[289]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[28] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [28]),
        .Q(pre_reg_cnt[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[290] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [290]),
        .Q(pre_reg_cnt[290]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[291] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [291]),
        .Q(pre_reg_cnt[291]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[292] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [292]),
        .Q(pre_reg_cnt[292]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[293] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [293]),
        .Q(pre_reg_cnt[293]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[294] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [294]),
        .Q(pre_reg_cnt[294]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[295] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [295]),
        .Q(pre_reg_cnt[295]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[296] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [296]),
        .Q(pre_reg_cnt[296]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[297] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [297]),
        .Q(pre_reg_cnt[297]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[298] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [298]),
        .Q(pre_reg_cnt[298]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[299] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [299]),
        .Q(pre_reg_cnt[299]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[29] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [29]),
        .Q(pre_reg_cnt[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [2]),
        .Q(pre_reg_cnt[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[300] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [300]),
        .Q(pre_reg_cnt[300]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[301] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [301]),
        .Q(pre_reg_cnt[301]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[302] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [302]),
        .Q(pre_reg_cnt[302]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[303] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [303]),
        .Q(pre_reg_cnt[303]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[304] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [304]),
        .Q(pre_reg_cnt[304]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[305] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [305]),
        .Q(pre_reg_cnt[305]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[306] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [306]),
        .Q(pre_reg_cnt[306]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[307] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [307]),
        .Q(pre_reg_cnt[307]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[308] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [308]),
        .Q(pre_reg_cnt[308]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[309] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [309]),
        .Q(pre_reg_cnt[309]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[30] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [30]),
        .Q(pre_reg_cnt[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[310] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [310]),
        .Q(pre_reg_cnt[310]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[311] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [311]),
        .Q(pre_reg_cnt[311]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[312] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [312]),
        .Q(pre_reg_cnt[312]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[313] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [313]),
        .Q(pre_reg_cnt[313]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[314] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [314]),
        .Q(pre_reg_cnt[314]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[315] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [315]),
        .Q(pre_reg_cnt[315]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[316] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [316]),
        .Q(pre_reg_cnt[316]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[317] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [317]),
        .Q(pre_reg_cnt[317]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[318] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [318]),
        .Q(pre_reg_cnt[318]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[319] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [319]),
        .Q(pre_reg_cnt[319]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[31] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [31]),
        .Q(pre_reg_cnt[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[32] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [32]),
        .Q(pre_reg_cnt[32]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[33] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [33]),
        .Q(pre_reg_cnt[33]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[34] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [34]),
        .Q(pre_reg_cnt[34]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[35] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [35]),
        .Q(pre_reg_cnt[35]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[36] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [36]),
        .Q(pre_reg_cnt[36]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[37] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [37]),
        .Q(pre_reg_cnt[37]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[38] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [38]),
        .Q(pre_reg_cnt[38]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[39] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [39]),
        .Q(pre_reg_cnt[39]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [3]),
        .Q(pre_reg_cnt[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[40] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [40]),
        .Q(pre_reg_cnt[40]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[41] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [41]),
        .Q(pre_reg_cnt[41]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[42] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [42]),
        .Q(pre_reg_cnt[42]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[43] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [43]),
        .Q(pre_reg_cnt[43]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[44] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [44]),
        .Q(pre_reg_cnt[44]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[45] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [45]),
        .Q(pre_reg_cnt[45]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[46] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [46]),
        .Q(pre_reg_cnt[46]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[47] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [47]),
        .Q(pre_reg_cnt[47]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[48] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [48]),
        .Q(pre_reg_cnt[48]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[49] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [49]),
        .Q(pre_reg_cnt[49]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[4] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [4]),
        .Q(pre_reg_cnt[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[50] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [50]),
        .Q(pre_reg_cnt[50]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[51] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [51]),
        .Q(pre_reg_cnt[51]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[52] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [52]),
        .Q(pre_reg_cnt[52]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[53] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [53]),
        .Q(pre_reg_cnt[53]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[54] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [54]),
        .Q(pre_reg_cnt[54]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[55] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [55]),
        .Q(pre_reg_cnt[55]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[56] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [56]),
        .Q(pre_reg_cnt[56]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[57] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [57]),
        .Q(pre_reg_cnt[57]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[58] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [58]),
        .Q(pre_reg_cnt[58]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[59] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [59]),
        .Q(pre_reg_cnt[59]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[5] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [5]),
        .Q(pre_reg_cnt[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[60] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [60]),
        .Q(pre_reg_cnt[60]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[61] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [61]),
        .Q(pre_reg_cnt[61]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[62] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [62]),
        .Q(pre_reg_cnt[62]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[63] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [63]),
        .Q(pre_reg_cnt[63]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[64] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [64]),
        .Q(pre_reg_cnt[64]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[65] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [65]),
        .Q(pre_reg_cnt[65]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[66] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [66]),
        .Q(pre_reg_cnt[66]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[67] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [67]),
        .Q(pre_reg_cnt[67]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[68] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [68]),
        .Q(pre_reg_cnt[68]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[69] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [69]),
        .Q(pre_reg_cnt[69]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[6] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [6]),
        .Q(pre_reg_cnt[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[70] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [70]),
        .Q(pre_reg_cnt[70]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[71] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [71]),
        .Q(pre_reg_cnt[71]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[72] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [72]),
        .Q(pre_reg_cnt[72]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[73] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [73]),
        .Q(pre_reg_cnt[73]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[74] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [74]),
        .Q(pre_reg_cnt[74]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[75] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [75]),
        .Q(pre_reg_cnt[75]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[76] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [76]),
        .Q(pre_reg_cnt[76]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[77] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [77]),
        .Q(pre_reg_cnt[77]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[78] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [78]),
        .Q(pre_reg_cnt[78]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[79] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [79]),
        .Q(pre_reg_cnt[79]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[7] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [7]),
        .Q(pre_reg_cnt[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[80] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [80]),
        .Q(pre_reg_cnt[80]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[81] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [81]),
        .Q(pre_reg_cnt[81]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[82] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [82]),
        .Q(pre_reg_cnt[82]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[83] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [83]),
        .Q(pre_reg_cnt[83]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[84] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [84]),
        .Q(pre_reg_cnt[84]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[85] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [85]),
        .Q(pre_reg_cnt[85]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[86] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [86]),
        .Q(pre_reg_cnt[86]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[87] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [87]),
        .Q(pre_reg_cnt[87]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[88] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [88]),
        .Q(pre_reg_cnt[88]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[89] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [89]),
        .Q(pre_reg_cnt[89]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[8] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [8]),
        .Q(pre_reg_cnt[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[90] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [90]),
        .Q(pre_reg_cnt[90]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[91] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [91]),
        .Q(pre_reg_cnt[91]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[92] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [92]),
        .Q(pre_reg_cnt[92]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[93] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [93]),
        .Q(pre_reg_cnt[93]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[94] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [94]),
        .Q(pre_reg_cnt[94]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[95] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [95]),
        .Q(pre_reg_cnt[95]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[96] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [96]),
        .Q(pre_reg_cnt[96]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[97] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [97]),
        .Q(pre_reg_cnt[97]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[98] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [98]),
        .Q(pre_reg_cnt[98]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[99] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [99]),
        .Q(pre_reg_cnt[99]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[9] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [9]),
        .Q(pre_reg_cnt[9]),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register__parameterized0_161
   (S,
    q_reg_i_84_0,
    q_reg_i_183_0,
    \q_reg[137]_0 ,
    DI,
    \q_reg[271]_0 ,
    \q_reg[109]_0 ,
    q_reg_i_98_0,
    q_reg_i_67_0,
    \q_reg[34]_0 ,
    q_reg_i_116_0,
    q_reg_i_113_0,
    \q_reg[55]_0 ,
    \q_reg[295]_0 ,
    \q_reg[43]_0 ,
    \q_reg[31]_0 ,
    \q_reg[91]_0 ,
    q_reg_i_95_0,
    \q_reg[188]_0 ,
    \q_reg[131]_0 ,
    \q_reg[132]_0 ,
    \q_reg[295]_1 ,
    \q_reg[19]_0 ,
    \q_reg[57]_0 ,
    \q_reg[77]_0 ,
    \q_reg[184]_0 ,
    \q_reg[215]_0 ,
    \q_reg[247]_0 ,
    \q_reg[239]_0 ,
    q_reg,
    \q_reg[319]_0 ,
    \q_reg[0]_0 );
  output [2:0]S;
  output q_reg_i_84_0;
  output q_reg_i_183_0;
  output \q_reg[137]_0 ;
  output [1:0]DI;
  output \q_reg[271]_0 ;
  output \q_reg[109]_0 ;
  output q_reg_i_98_0;
  output [1:0]q_reg_i_67_0;
  output \q_reg[34]_0 ;
  output q_reg_i_116_0;
  output q_reg_i_113_0;
  output \q_reg[55]_0 ;
  output \q_reg[295]_0 ;
  output \q_reg[43]_0 ;
  output \q_reg[31]_0 ;
  output \q_reg[91]_0 ;
  output q_reg_i_95_0;
  output \q_reg[188]_0 ;
  output \q_reg[131]_0 ;
  output \q_reg[132]_0 ;
  output \q_reg[295]_1 ;
  output \q_reg[19]_0 ;
  output \q_reg[57]_0 ;
  output \q_reg[77]_0 ;
  output \q_reg[184]_0 ;
  output \q_reg[215]_0 ;
  output \q_reg[247]_0 ;
  output \q_reg[239]_0 ;
  input [1:0]q_reg;
  input [319:0]\q_reg[319]_0 ;
  input \q_reg[0]_0 ;

  wire [1:0]DI;
  wire [2:0]S;
  wire [319:0]pst_reg_cnt;
  wire [1:0]q_reg;
  wire \q_reg[0]_0 ;
  wire \q_reg[109]_0 ;
  wire \q_reg[131]_0 ;
  wire \q_reg[132]_0 ;
  wire \q_reg[137]_0 ;
  wire \q_reg[184]_0 ;
  wire \q_reg[188]_0 ;
  wire \q_reg[19]_0 ;
  wire \q_reg[215]_0 ;
  wire \q_reg[239]_0 ;
  wire \q_reg[247]_0 ;
  wire \q_reg[271]_0 ;
  wire \q_reg[295]_0 ;
  wire \q_reg[295]_1 ;
  wire [319:0]\q_reg[319]_0 ;
  wire \q_reg[31]_0 ;
  wire \q_reg[34]_0 ;
  wire \q_reg[43]_0 ;
  wire \q_reg[55]_0 ;
  wire \q_reg[57]_0 ;
  wire \q_reg[77]_0 ;
  wire \q_reg[91]_0 ;
  wire q_reg_i_100_n_0;
  wire q_reg_i_101_n_0;
  wire q_reg_i_102_n_0;
  wire q_reg_i_103_n_0;
  wire q_reg_i_104_n_0;
  wire q_reg_i_105_n_0;
  wire q_reg_i_106_n_0;
  wire q_reg_i_107_n_0;
  wire q_reg_i_108_n_0;
  wire q_reg_i_109_n_0;
  wire q_reg_i_110_n_0;
  wire q_reg_i_111_n_0;
  wire q_reg_i_112_n_0;
  wire q_reg_i_113_0;
  wire q_reg_i_113_n_0;
  wire q_reg_i_114_n_0;
  wire q_reg_i_115_n_0;
  wire q_reg_i_116_0;
  wire q_reg_i_116_n_0;
  wire q_reg_i_117_n_0;
  wire q_reg_i_118_n_0;
  wire q_reg_i_119_n_0;
  wire q_reg_i_120_n_0;
  wire q_reg_i_121_n_0;
  wire q_reg_i_122_n_0;
  wire q_reg_i_123_n_0;
  wire q_reg_i_124_n_0;
  wire q_reg_i_125_n_0;
  wire q_reg_i_126_n_0;
  wire q_reg_i_127_n_0;
  wire q_reg_i_128_n_0;
  wire q_reg_i_129_n_0;
  wire q_reg_i_130_n_0;
  wire q_reg_i_131_n_0;
  wire q_reg_i_132_n_0;
  wire q_reg_i_133_n_0;
  wire q_reg_i_134_n_0;
  wire q_reg_i_135_n_0;
  wire q_reg_i_136_n_0;
  wire q_reg_i_137_n_0;
  wire q_reg_i_138_n_0;
  wire q_reg_i_139_n_0;
  wire q_reg_i_140_n_0;
  wire q_reg_i_141_n_0;
  wire q_reg_i_142_n_0;
  wire q_reg_i_143_n_0;
  wire q_reg_i_144_n_0;
  wire q_reg_i_145_n_0;
  wire q_reg_i_146_n_0;
  wire q_reg_i_147_n_0;
  wire q_reg_i_148_n_0;
  wire q_reg_i_149_n_0;
  wire q_reg_i_150_n_0;
  wire q_reg_i_151_n_0;
  wire q_reg_i_152_n_0;
  wire q_reg_i_153_n_0;
  wire q_reg_i_154_n_0;
  wire q_reg_i_155_n_0;
  wire q_reg_i_156_n_0;
  wire q_reg_i_157_n_0;
  wire q_reg_i_158_n_0;
  wire q_reg_i_159_n_0;
  wire q_reg_i_160_n_0;
  wire q_reg_i_161_n_0;
  wire q_reg_i_162_n_0;
  wire q_reg_i_163_n_0;
  wire q_reg_i_164_n_0;
  wire q_reg_i_165_n_0;
  wire q_reg_i_166_n_0;
  wire q_reg_i_167_n_0;
  wire q_reg_i_168_n_0;
  wire q_reg_i_169_n_0;
  wire q_reg_i_170_n_0;
  wire q_reg_i_171_n_0;
  wire q_reg_i_172_n_0;
  wire q_reg_i_173_n_0;
  wire q_reg_i_174_n_0;
  wire q_reg_i_175_n_0;
  wire q_reg_i_176_n_0;
  wire q_reg_i_177_n_0;
  wire q_reg_i_178_n_0;
  wire q_reg_i_179_n_0;
  wire q_reg_i_180_n_0;
  wire q_reg_i_181_n_0;
  wire q_reg_i_182_n_0;
  wire q_reg_i_183_0;
  wire q_reg_i_183_n_0;
  wire q_reg_i_184_n_0;
  wire q_reg_i_185_n_0;
  wire q_reg_i_186_n_0;
  wire q_reg_i_187_n_0;
  wire q_reg_i_188_n_0;
  wire q_reg_i_189_n_0;
  wire q_reg_i_190_n_0;
  wire q_reg_i_191_n_0;
  wire q_reg_i_192_n_0;
  wire q_reg_i_193_n_0;
  wire q_reg_i_194_n_0;
  wire q_reg_i_195_n_0;
  wire q_reg_i_196_n_0;
  wire q_reg_i_197_n_0;
  wire q_reg_i_198_n_0;
  wire q_reg_i_199_n_0;
  wire q_reg_i_200_n_0;
  wire q_reg_i_201_n_0;
  wire q_reg_i_202_n_0;
  wire q_reg_i_203_n_0;
  wire q_reg_i_204_n_0;
  wire q_reg_i_205_n_0;
  wire q_reg_i_206_n_0;
  wire q_reg_i_207_n_0;
  wire q_reg_i_208_n_0;
  wire q_reg_i_209_n_0;
  wire q_reg_i_210_n_0;
  wire q_reg_i_211_n_0;
  wire q_reg_i_212_n_0;
  wire q_reg_i_213_n_0;
  wire q_reg_i_214_n_0;
  wire q_reg_i_215_n_0;
  wire q_reg_i_216_n_0;
  wire q_reg_i_217_n_0;
  wire q_reg_i_218_n_0;
  wire q_reg_i_219_n_0;
  wire q_reg_i_220_n_0;
  wire q_reg_i_221_n_0;
  wire q_reg_i_222_n_0;
  wire q_reg_i_223_n_0;
  wire q_reg_i_224_n_0;
  wire q_reg_i_225_n_0;
  wire q_reg_i_226_n_0;
  wire q_reg_i_227_n_0;
  wire q_reg_i_228_n_0;
  wire q_reg_i_229_n_0;
  wire q_reg_i_230_n_0;
  wire q_reg_i_231_n_0;
  wire q_reg_i_232_n_0;
  wire q_reg_i_233_n_0;
  wire q_reg_i_234_n_0;
  wire q_reg_i_235_n_0;
  wire q_reg_i_236_n_0;
  wire q_reg_i_237_n_0;
  wire q_reg_i_238_n_0;
  wire q_reg_i_239_n_0;
  wire q_reg_i_240_n_0;
  wire q_reg_i_241_n_0;
  wire q_reg_i_242_n_0;
  wire q_reg_i_243_n_0;
  wire q_reg_i_244_n_0;
  wire q_reg_i_245_n_0;
  wire q_reg_i_246_n_0;
  wire q_reg_i_247_n_0;
  wire q_reg_i_248_n_0;
  wire q_reg_i_249_n_0;
  wire q_reg_i_250_n_0;
  wire q_reg_i_251_n_0;
  wire q_reg_i_252_n_0;
  wire q_reg_i_253_n_0;
  wire q_reg_i_254_n_0;
  wire q_reg_i_255_n_0;
  wire q_reg_i_256_n_0;
  wire q_reg_i_257_n_0;
  wire q_reg_i_258_n_0;
  wire q_reg_i_259_n_0;
  wire q_reg_i_260_n_0;
  wire q_reg_i_261_n_0;
  wire q_reg_i_262_n_0;
  wire q_reg_i_263_n_0;
  wire q_reg_i_264_n_0;
  wire q_reg_i_265_n_0;
  wire q_reg_i_266_n_0;
  wire q_reg_i_267_n_0;
  wire q_reg_i_268_n_0;
  wire q_reg_i_269_n_0;
  wire q_reg_i_270_n_0;
  wire q_reg_i_271_n_0;
  wire q_reg_i_272_n_0;
  wire q_reg_i_273_n_0;
  wire q_reg_i_274_n_0;
  wire q_reg_i_275_n_0;
  wire q_reg_i_276_n_0;
  wire q_reg_i_277_n_0;
  wire q_reg_i_278_n_0;
  wire q_reg_i_279_n_0;
  wire q_reg_i_280_n_0;
  wire q_reg_i_281_n_0;
  wire q_reg_i_282_n_0;
  wire q_reg_i_283_n_0;
  wire q_reg_i_284_n_0;
  wire q_reg_i_285_n_0;
  wire q_reg_i_286_n_0;
  wire q_reg_i_287_n_0;
  wire q_reg_i_288_n_0;
  wire q_reg_i_289_n_0;
  wire q_reg_i_290_n_0;
  wire q_reg_i_291_n_0;
  wire q_reg_i_292_n_0;
  wire q_reg_i_293_n_0;
  wire q_reg_i_294_n_0;
  wire q_reg_i_295_n_0;
  wire q_reg_i_296_n_0;
  wire q_reg_i_297_n_0;
  wire q_reg_i_298_n_0;
  wire q_reg_i_299_n_0;
  wire q_reg_i_300_n_0;
  wire q_reg_i_301_n_0;
  wire q_reg_i_302_n_0;
  wire q_reg_i_303_n_0;
  wire q_reg_i_304_n_0;
  wire q_reg_i_305_n_0;
  wire q_reg_i_306_n_0;
  wire q_reg_i_307_n_0;
  wire q_reg_i_308_n_0;
  wire q_reg_i_309_n_0;
  wire q_reg_i_310_n_0;
  wire q_reg_i_311_n_0;
  wire q_reg_i_312_n_0;
  wire q_reg_i_313_n_0;
  wire q_reg_i_314_n_0;
  wire q_reg_i_315_n_0;
  wire q_reg_i_316_n_0;
  wire q_reg_i_317_n_0;
  wire q_reg_i_318_n_0;
  wire q_reg_i_319_n_0;
  wire q_reg_i_320_n_0;
  wire q_reg_i_321_n_0;
  wire q_reg_i_322_n_0;
  wire q_reg_i_323_n_0;
  wire q_reg_i_324_n_0;
  wire q_reg_i_325_n_0;
  wire q_reg_i_326_n_0;
  wire q_reg_i_327_n_0;
  wire q_reg_i_328_n_0;
  wire q_reg_i_329_n_0;
  wire q_reg_i_330_n_0;
  wire q_reg_i_331_n_0;
  wire q_reg_i_332_n_0;
  wire q_reg_i_333_n_0;
  wire q_reg_i_334_n_0;
  wire q_reg_i_335_n_0;
  wire q_reg_i_336_n_0;
  wire q_reg_i_337_n_0;
  wire q_reg_i_338_n_0;
  wire q_reg_i_339_n_0;
  wire q_reg_i_340_n_0;
  wire q_reg_i_341_n_0;
  wire q_reg_i_342_n_0;
  wire q_reg_i_343_n_0;
  wire q_reg_i_344_n_0;
  wire q_reg_i_345_n_0;
  wire q_reg_i_346_n_0;
  wire q_reg_i_347_n_0;
  wire q_reg_i_348_n_0;
  wire q_reg_i_349_n_0;
  wire q_reg_i_350_n_0;
  wire q_reg_i_351_n_0;
  wire q_reg_i_352_n_0;
  wire q_reg_i_353_n_0;
  wire q_reg_i_354_n_0;
  wire q_reg_i_355_n_0;
  wire q_reg_i_356_n_0;
  wire q_reg_i_357_n_0;
  wire q_reg_i_358_n_0;
  wire q_reg_i_359_n_0;
  wire q_reg_i_360_n_0;
  wire q_reg_i_361_n_0;
  wire q_reg_i_362_n_0;
  wire q_reg_i_363_n_0;
  wire q_reg_i_364_n_0;
  wire q_reg_i_365_n_0;
  wire q_reg_i_366_n_0;
  wire q_reg_i_367_n_0;
  wire q_reg_i_368_n_0;
  wire q_reg_i_369_n_0;
  wire q_reg_i_370_n_0;
  wire q_reg_i_371_n_0;
  wire q_reg_i_372_n_0;
  wire q_reg_i_373_n_0;
  wire q_reg_i_374_n_0;
  wire q_reg_i_375_n_0;
  wire q_reg_i_376_n_0;
  wire q_reg_i_377_n_0;
  wire q_reg_i_378_n_0;
  wire q_reg_i_379_n_0;
  wire q_reg_i_380_n_0;
  wire q_reg_i_381_n_0;
  wire q_reg_i_382_n_0;
  wire q_reg_i_383_n_0;
  wire q_reg_i_384_n_0;
  wire q_reg_i_385_n_0;
  wire q_reg_i_386_n_0;
  wire q_reg_i_387_n_0;
  wire q_reg_i_388_n_0;
  wire q_reg_i_389_n_0;
  wire q_reg_i_390_n_0;
  wire q_reg_i_391_n_0;
  wire q_reg_i_392_n_0;
  wire q_reg_i_393_n_0;
  wire q_reg_i_394_n_0;
  wire q_reg_i_395_n_0;
  wire q_reg_i_396_n_0;
  wire q_reg_i_397_n_0;
  wire q_reg_i_398_n_0;
  wire q_reg_i_399_n_0;
  wire q_reg_i_400_n_0;
  wire q_reg_i_401_n_0;
  wire q_reg_i_402_n_0;
  wire q_reg_i_403_n_0;
  wire q_reg_i_404_n_0;
  wire q_reg_i_405_n_0;
  wire q_reg_i_406_n_0;
  wire q_reg_i_407_n_0;
  wire q_reg_i_408_n_0;
  wire q_reg_i_409_n_0;
  wire q_reg_i_410_n_0;
  wire q_reg_i_411_n_0;
  wire q_reg_i_412_n_0;
  wire q_reg_i_413_n_0;
  wire q_reg_i_414_n_0;
  wire q_reg_i_415_n_0;
  wire q_reg_i_416_n_0;
  wire q_reg_i_417_n_0;
  wire q_reg_i_418_n_0;
  wire q_reg_i_419_n_0;
  wire q_reg_i_420_n_0;
  wire q_reg_i_421_n_0;
  wire q_reg_i_422_n_0;
  wire q_reg_i_423_n_0;
  wire q_reg_i_424_n_0;
  wire q_reg_i_425_n_0;
  wire q_reg_i_426_n_0;
  wire q_reg_i_427_n_0;
  wire q_reg_i_428_n_0;
  wire q_reg_i_429_n_0;
  wire q_reg_i_430_n_0;
  wire q_reg_i_431_n_0;
  wire q_reg_i_432_n_0;
  wire q_reg_i_433_n_0;
  wire q_reg_i_434_n_0;
  wire q_reg_i_435_n_0;
  wire q_reg_i_436_n_0;
  wire q_reg_i_437_n_0;
  wire q_reg_i_438_n_0;
  wire q_reg_i_439_n_0;
  wire q_reg_i_440_n_0;
  wire q_reg_i_441_n_0;
  wire q_reg_i_442_n_0;
  wire q_reg_i_443_n_0;
  wire q_reg_i_444_n_0;
  wire q_reg_i_445_n_0;
  wire q_reg_i_446_n_0;
  wire q_reg_i_447_n_0;
  wire q_reg_i_448_n_0;
  wire q_reg_i_449_n_0;
  wire q_reg_i_450_n_0;
  wire q_reg_i_451_n_0;
  wire q_reg_i_452_n_0;
  wire q_reg_i_453_n_0;
  wire q_reg_i_454_n_0;
  wire q_reg_i_455_n_0;
  wire q_reg_i_456_n_0;
  wire q_reg_i_457_n_0;
  wire q_reg_i_458_n_0;
  wire q_reg_i_459_n_0;
  wire q_reg_i_460_n_0;
  wire q_reg_i_461_n_0;
  wire q_reg_i_462_n_0;
  wire q_reg_i_463_n_0;
  wire q_reg_i_464_n_0;
  wire q_reg_i_465_n_0;
  wire q_reg_i_466_n_0;
  wire q_reg_i_467_n_0;
  wire q_reg_i_468_n_0;
  wire q_reg_i_469_n_0;
  wire q_reg_i_470_n_0;
  wire q_reg_i_471_n_0;
  wire q_reg_i_472_n_0;
  wire q_reg_i_473_n_0;
  wire q_reg_i_474_n_0;
  wire q_reg_i_475_n_0;
  wire q_reg_i_476_n_0;
  wire q_reg_i_477_n_0;
  wire q_reg_i_478_n_0;
  wire q_reg_i_479_n_0;
  wire q_reg_i_480_n_0;
  wire q_reg_i_481_n_0;
  wire q_reg_i_482_n_0;
  wire q_reg_i_483_n_0;
  wire q_reg_i_484_n_0;
  wire q_reg_i_485_n_0;
  wire q_reg_i_486_n_0;
  wire q_reg_i_487_n_0;
  wire q_reg_i_488_n_0;
  wire q_reg_i_489_n_0;
  wire q_reg_i_490_n_0;
  wire q_reg_i_491_n_0;
  wire q_reg_i_492_n_0;
  wire q_reg_i_493_n_0;
  wire q_reg_i_494_n_0;
  wire q_reg_i_495_n_0;
  wire q_reg_i_496_n_0;
  wire q_reg_i_497_n_0;
  wire q_reg_i_498_n_0;
  wire q_reg_i_499_n_0;
  wire q_reg_i_500_n_0;
  wire q_reg_i_501_n_0;
  wire q_reg_i_502_n_0;
  wire q_reg_i_503_n_0;
  wire q_reg_i_504_n_0;
  wire q_reg_i_505_n_0;
  wire q_reg_i_506_n_0;
  wire q_reg_i_507_n_0;
  wire q_reg_i_508_n_0;
  wire q_reg_i_509_n_0;
  wire q_reg_i_510_n_0;
  wire q_reg_i_511_n_0;
  wire q_reg_i_512_n_0;
  wire q_reg_i_513_n_0;
  wire q_reg_i_514_n_0;
  wire q_reg_i_515_n_0;
  wire q_reg_i_516_n_0;
  wire q_reg_i_517_n_0;
  wire q_reg_i_518_n_0;
  wire q_reg_i_519_n_0;
  wire q_reg_i_520_n_0;
  wire q_reg_i_521_n_0;
  wire q_reg_i_522_n_0;
  wire q_reg_i_523_n_0;
  wire q_reg_i_524_n_0;
  wire q_reg_i_525_n_0;
  wire q_reg_i_526_n_0;
  wire q_reg_i_527_n_0;
  wire q_reg_i_528_n_0;
  wire q_reg_i_529_n_0;
  wire q_reg_i_530_n_0;
  wire q_reg_i_531_n_0;
  wire q_reg_i_532_n_0;
  wire q_reg_i_533_n_0;
  wire q_reg_i_534_n_0;
  wire q_reg_i_535_n_0;
  wire q_reg_i_536_n_0;
  wire q_reg_i_537_n_0;
  wire q_reg_i_538_n_0;
  wire q_reg_i_539_n_0;
  wire q_reg_i_540_n_0;
  wire q_reg_i_541_n_0;
  wire q_reg_i_542_n_0;
  wire q_reg_i_543_n_0;
  wire q_reg_i_544_n_0;
  wire q_reg_i_545_n_0;
  wire q_reg_i_546_n_0;
  wire q_reg_i_547_n_0;
  wire q_reg_i_548_n_0;
  wire q_reg_i_549_n_0;
  wire q_reg_i_550_n_0;
  wire q_reg_i_551_n_0;
  wire q_reg_i_552_n_0;
  wire q_reg_i_553_n_0;
  wire q_reg_i_554_n_0;
  wire q_reg_i_555_n_0;
  wire q_reg_i_556_n_0;
  wire q_reg_i_557_n_0;
  wire q_reg_i_558_n_0;
  wire q_reg_i_559_n_0;
  wire q_reg_i_560_n_0;
  wire q_reg_i_561_n_0;
  wire q_reg_i_562_n_0;
  wire q_reg_i_563_n_0;
  wire q_reg_i_564_n_0;
  wire q_reg_i_565_n_0;
  wire q_reg_i_566_n_0;
  wire q_reg_i_567_n_0;
  wire q_reg_i_568_n_0;
  wire q_reg_i_569_n_0;
  wire q_reg_i_570_n_0;
  wire q_reg_i_571_n_0;
  wire q_reg_i_572_n_0;
  wire q_reg_i_573_n_0;
  wire q_reg_i_574_n_0;
  wire q_reg_i_575_n_0;
  wire q_reg_i_576_n_0;
  wire q_reg_i_577_n_0;
  wire q_reg_i_578_n_0;
  wire q_reg_i_579_n_0;
  wire q_reg_i_580_n_0;
  wire q_reg_i_581_n_0;
  wire q_reg_i_582_n_0;
  wire q_reg_i_583_n_0;
  wire q_reg_i_584_n_0;
  wire q_reg_i_585_n_0;
  wire q_reg_i_586_n_0;
  wire q_reg_i_587_n_0;
  wire q_reg_i_588_n_0;
  wire q_reg_i_589_n_0;
  wire q_reg_i_590_n_0;
  wire q_reg_i_591_n_0;
  wire q_reg_i_66_n_0;
  wire [1:0]q_reg_i_67_0;
  wire q_reg_i_67_n_0;
  wire q_reg_i_68_n_0;
  wire q_reg_i_69_n_0;
  wire q_reg_i_79_n_0;
  wire q_reg_i_80_n_0;
  wire q_reg_i_81_n_0;
  wire q_reg_i_82_n_0;
  wire q_reg_i_83_n_0;
  wire q_reg_i_84_0;
  wire q_reg_i_84_n_0;
  wire q_reg_i_85_n_0;
  wire q_reg_i_86_n_0;
  wire q_reg_i_87_n_0;
  wire q_reg_i_88_n_0;
  wire q_reg_i_89_n_0;
  wire q_reg_i_90_n_0;
  wire q_reg_i_91_n_0;
  wire q_reg_i_92_n_0;
  wire q_reg_i_93_n_0;
  wire q_reg_i_94_n_0;
  wire q_reg_i_95_0;
  wire q_reg_i_95_n_0;
  wire q_reg_i_96_n_0;
  wire q_reg_i_97_n_0;
  wire q_reg_i_98_0;
  wire q_reg_i_98_n_0;
  wire q_reg_i_99_n_0;

  FDRE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [0]),
        .Q(pst_reg_cnt[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[100] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [100]),
        .Q(pst_reg_cnt[100]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[101] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [101]),
        .Q(pst_reg_cnt[101]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[102] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [102]),
        .Q(pst_reg_cnt[102]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[103] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [103]),
        .Q(pst_reg_cnt[103]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[104] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [104]),
        .Q(pst_reg_cnt[104]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[105] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [105]),
        .Q(pst_reg_cnt[105]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[106] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [106]),
        .Q(pst_reg_cnt[106]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[107] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [107]),
        .Q(pst_reg_cnt[107]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[108] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [108]),
        .Q(pst_reg_cnt[108]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[109] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [109]),
        .Q(pst_reg_cnt[109]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[10] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [10]),
        .Q(pst_reg_cnt[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[110] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [110]),
        .Q(pst_reg_cnt[110]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[111] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [111]),
        .Q(pst_reg_cnt[111]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[112] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [112]),
        .Q(pst_reg_cnt[112]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[113] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [113]),
        .Q(pst_reg_cnt[113]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[114] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [114]),
        .Q(pst_reg_cnt[114]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[115] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [115]),
        .Q(pst_reg_cnt[115]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[116] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [116]),
        .Q(pst_reg_cnt[116]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[117] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [117]),
        .Q(pst_reg_cnt[117]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[118] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [118]),
        .Q(pst_reg_cnt[118]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[119] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [119]),
        .Q(pst_reg_cnt[119]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[11] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [11]),
        .Q(pst_reg_cnt[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[120] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [120]),
        .Q(pst_reg_cnt[120]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[121] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [121]),
        .Q(pst_reg_cnt[121]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[122] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [122]),
        .Q(pst_reg_cnt[122]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[123] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [123]),
        .Q(pst_reg_cnt[123]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[124] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [124]),
        .Q(pst_reg_cnt[124]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[125] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [125]),
        .Q(pst_reg_cnt[125]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[126] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [126]),
        .Q(pst_reg_cnt[126]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[127] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [127]),
        .Q(pst_reg_cnt[127]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[128] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [128]),
        .Q(pst_reg_cnt[128]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[129] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [129]),
        .Q(pst_reg_cnt[129]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[12] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [12]),
        .Q(pst_reg_cnt[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[130] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [130]),
        .Q(pst_reg_cnt[130]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[131] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [131]),
        .Q(pst_reg_cnt[131]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[132] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [132]),
        .Q(pst_reg_cnt[132]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[133] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [133]),
        .Q(pst_reg_cnt[133]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[134] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [134]),
        .Q(pst_reg_cnt[134]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[135] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [135]),
        .Q(pst_reg_cnt[135]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[136] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [136]),
        .Q(pst_reg_cnt[136]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[137] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [137]),
        .Q(pst_reg_cnt[137]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[138] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [138]),
        .Q(pst_reg_cnt[138]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[139] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [139]),
        .Q(pst_reg_cnt[139]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[13] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [13]),
        .Q(pst_reg_cnt[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[140] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [140]),
        .Q(pst_reg_cnt[140]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[141] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [141]),
        .Q(pst_reg_cnt[141]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[142] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [142]),
        .Q(pst_reg_cnt[142]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[143] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [143]),
        .Q(pst_reg_cnt[143]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[144] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [144]),
        .Q(pst_reg_cnt[144]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[145] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [145]),
        .Q(pst_reg_cnt[145]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[146] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [146]),
        .Q(pst_reg_cnt[146]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[147] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [147]),
        .Q(pst_reg_cnt[147]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[148] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [148]),
        .Q(pst_reg_cnt[148]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[149] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [149]),
        .Q(pst_reg_cnt[149]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[14] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [14]),
        .Q(pst_reg_cnt[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[150] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [150]),
        .Q(pst_reg_cnt[150]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[151] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [151]),
        .Q(pst_reg_cnt[151]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[152] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [152]),
        .Q(pst_reg_cnt[152]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[153] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [153]),
        .Q(pst_reg_cnt[153]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[154] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [154]),
        .Q(pst_reg_cnt[154]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[155] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [155]),
        .Q(pst_reg_cnt[155]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[156] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [156]),
        .Q(pst_reg_cnt[156]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[157] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [157]),
        .Q(pst_reg_cnt[157]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[158] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [158]),
        .Q(pst_reg_cnt[158]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[159] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [159]),
        .Q(pst_reg_cnt[159]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[15] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [15]),
        .Q(pst_reg_cnt[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[160] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [160]),
        .Q(pst_reg_cnt[160]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[161] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [161]),
        .Q(pst_reg_cnt[161]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[162] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [162]),
        .Q(pst_reg_cnt[162]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[163] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [163]),
        .Q(pst_reg_cnt[163]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[164] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [164]),
        .Q(pst_reg_cnt[164]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[165] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [165]),
        .Q(pst_reg_cnt[165]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[166] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [166]),
        .Q(pst_reg_cnt[166]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[167] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [167]),
        .Q(pst_reg_cnt[167]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[168] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [168]),
        .Q(pst_reg_cnt[168]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[169] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [169]),
        .Q(pst_reg_cnt[169]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[16] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [16]),
        .Q(pst_reg_cnt[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[170] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [170]),
        .Q(pst_reg_cnt[170]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[171] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [171]),
        .Q(pst_reg_cnt[171]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[172] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [172]),
        .Q(pst_reg_cnt[172]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[173] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [173]),
        .Q(pst_reg_cnt[173]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[174] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [174]),
        .Q(pst_reg_cnt[174]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[175] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [175]),
        .Q(pst_reg_cnt[175]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[176] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [176]),
        .Q(pst_reg_cnt[176]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[177] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [177]),
        .Q(pst_reg_cnt[177]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[178] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [178]),
        .Q(pst_reg_cnt[178]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[179] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [179]),
        .Q(pst_reg_cnt[179]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[17] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [17]),
        .Q(pst_reg_cnt[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[180] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [180]),
        .Q(pst_reg_cnt[180]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[181] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [181]),
        .Q(pst_reg_cnt[181]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[182] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [182]),
        .Q(pst_reg_cnt[182]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[183] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [183]),
        .Q(pst_reg_cnt[183]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[184] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [184]),
        .Q(pst_reg_cnt[184]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[185] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [185]),
        .Q(pst_reg_cnt[185]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[186] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [186]),
        .Q(pst_reg_cnt[186]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[187] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [187]),
        .Q(pst_reg_cnt[187]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[188] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [188]),
        .Q(pst_reg_cnt[188]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[189] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [189]),
        .Q(pst_reg_cnt[189]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[18] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [18]),
        .Q(pst_reg_cnt[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[190] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [190]),
        .Q(pst_reg_cnt[190]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[191] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [191]),
        .Q(pst_reg_cnt[191]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[192] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [192]),
        .Q(pst_reg_cnt[192]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[193] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [193]),
        .Q(pst_reg_cnt[193]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[194] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [194]),
        .Q(pst_reg_cnt[194]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[195] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [195]),
        .Q(pst_reg_cnt[195]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[196] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [196]),
        .Q(pst_reg_cnt[196]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[197] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [197]),
        .Q(pst_reg_cnt[197]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[198] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [198]),
        .Q(pst_reg_cnt[198]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[199] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [199]),
        .Q(pst_reg_cnt[199]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[19] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [19]),
        .Q(pst_reg_cnt[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [1]),
        .Q(pst_reg_cnt[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[200] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [200]),
        .Q(pst_reg_cnt[200]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[201] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [201]),
        .Q(pst_reg_cnt[201]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[202] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [202]),
        .Q(pst_reg_cnt[202]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[203] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [203]),
        .Q(pst_reg_cnt[203]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[204] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [204]),
        .Q(pst_reg_cnt[204]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[205] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [205]),
        .Q(pst_reg_cnt[205]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[206] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [206]),
        .Q(pst_reg_cnt[206]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[207] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [207]),
        .Q(pst_reg_cnt[207]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[208] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [208]),
        .Q(pst_reg_cnt[208]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[209] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [209]),
        .Q(pst_reg_cnt[209]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[20] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [20]),
        .Q(pst_reg_cnt[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[210] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [210]),
        .Q(pst_reg_cnt[210]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[211] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [211]),
        .Q(pst_reg_cnt[211]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[212] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [212]),
        .Q(pst_reg_cnt[212]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[213] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [213]),
        .Q(pst_reg_cnt[213]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[214] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [214]),
        .Q(pst_reg_cnt[214]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[215] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [215]),
        .Q(pst_reg_cnt[215]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[216] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [216]),
        .Q(pst_reg_cnt[216]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[217] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [217]),
        .Q(pst_reg_cnt[217]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[218] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [218]),
        .Q(pst_reg_cnt[218]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[219] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [219]),
        .Q(pst_reg_cnt[219]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[21] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [21]),
        .Q(pst_reg_cnt[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[220] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [220]),
        .Q(pst_reg_cnt[220]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[221] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [221]),
        .Q(pst_reg_cnt[221]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[222] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [222]),
        .Q(pst_reg_cnt[222]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[223] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [223]),
        .Q(pst_reg_cnt[223]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[224] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [224]),
        .Q(pst_reg_cnt[224]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[225] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [225]),
        .Q(pst_reg_cnt[225]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[226] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [226]),
        .Q(pst_reg_cnt[226]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[227] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [227]),
        .Q(pst_reg_cnt[227]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[228] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [228]),
        .Q(pst_reg_cnt[228]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[229] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [229]),
        .Q(pst_reg_cnt[229]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[22] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [22]),
        .Q(pst_reg_cnt[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[230] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [230]),
        .Q(pst_reg_cnt[230]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[231] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [231]),
        .Q(pst_reg_cnt[231]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[232] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [232]),
        .Q(pst_reg_cnt[232]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[233] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [233]),
        .Q(pst_reg_cnt[233]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[234] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [234]),
        .Q(pst_reg_cnt[234]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[235] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [235]),
        .Q(pst_reg_cnt[235]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[236] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [236]),
        .Q(pst_reg_cnt[236]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[237] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [237]),
        .Q(pst_reg_cnt[237]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[238] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [238]),
        .Q(pst_reg_cnt[238]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[239] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [239]),
        .Q(pst_reg_cnt[239]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[23] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [23]),
        .Q(pst_reg_cnt[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[240] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [240]),
        .Q(pst_reg_cnt[240]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[241] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [241]),
        .Q(pst_reg_cnt[241]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[242] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [242]),
        .Q(pst_reg_cnt[242]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[243] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [243]),
        .Q(pst_reg_cnt[243]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[244] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [244]),
        .Q(pst_reg_cnt[244]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[245] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [245]),
        .Q(pst_reg_cnt[245]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[246] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [246]),
        .Q(pst_reg_cnt[246]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[247] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [247]),
        .Q(pst_reg_cnt[247]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[248] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [248]),
        .Q(pst_reg_cnt[248]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[249] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [249]),
        .Q(pst_reg_cnt[249]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[24] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [24]),
        .Q(pst_reg_cnt[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[250] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [250]),
        .Q(pst_reg_cnt[250]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[251] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [251]),
        .Q(pst_reg_cnt[251]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[252] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [252]),
        .Q(pst_reg_cnt[252]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[253] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [253]),
        .Q(pst_reg_cnt[253]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[254] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [254]),
        .Q(pst_reg_cnt[254]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[255] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [255]),
        .Q(pst_reg_cnt[255]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[256] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [256]),
        .Q(pst_reg_cnt[256]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[257] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [257]),
        .Q(pst_reg_cnt[257]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[258] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [258]),
        .Q(pst_reg_cnt[258]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[259] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [259]),
        .Q(pst_reg_cnt[259]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[25] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [25]),
        .Q(pst_reg_cnt[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[260] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [260]),
        .Q(pst_reg_cnt[260]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[261] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [261]),
        .Q(pst_reg_cnt[261]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[262] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [262]),
        .Q(pst_reg_cnt[262]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[263] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [263]),
        .Q(pst_reg_cnt[263]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[264] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [264]),
        .Q(pst_reg_cnt[264]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[265] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [265]),
        .Q(pst_reg_cnt[265]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[266] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [266]),
        .Q(pst_reg_cnt[266]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[267] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [267]),
        .Q(pst_reg_cnt[267]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[268] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [268]),
        .Q(pst_reg_cnt[268]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[269] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [269]),
        .Q(pst_reg_cnt[269]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[26] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [26]),
        .Q(pst_reg_cnt[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[270] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [270]),
        .Q(pst_reg_cnt[270]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[271] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [271]),
        .Q(pst_reg_cnt[271]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[272] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [272]),
        .Q(pst_reg_cnt[272]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[273] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [273]),
        .Q(pst_reg_cnt[273]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[274] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [274]),
        .Q(pst_reg_cnt[274]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[275] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [275]),
        .Q(pst_reg_cnt[275]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[276] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [276]),
        .Q(pst_reg_cnt[276]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[277] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [277]),
        .Q(pst_reg_cnt[277]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[278] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [278]),
        .Q(pst_reg_cnt[278]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[279] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [279]),
        .Q(pst_reg_cnt[279]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[27] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [27]),
        .Q(pst_reg_cnt[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[280] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [280]),
        .Q(pst_reg_cnt[280]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[281] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [281]),
        .Q(pst_reg_cnt[281]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[282] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [282]),
        .Q(pst_reg_cnt[282]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[283] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [283]),
        .Q(pst_reg_cnt[283]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[284] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [284]),
        .Q(pst_reg_cnt[284]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[285] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [285]),
        .Q(pst_reg_cnt[285]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[286] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [286]),
        .Q(pst_reg_cnt[286]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[287] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [287]),
        .Q(pst_reg_cnt[287]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[288] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [288]),
        .Q(pst_reg_cnt[288]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[289] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [289]),
        .Q(pst_reg_cnt[289]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[28] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [28]),
        .Q(pst_reg_cnt[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[290] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [290]),
        .Q(pst_reg_cnt[290]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[291] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [291]),
        .Q(pst_reg_cnt[291]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[292] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [292]),
        .Q(pst_reg_cnt[292]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[293] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [293]),
        .Q(pst_reg_cnt[293]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[294] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [294]),
        .Q(pst_reg_cnt[294]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[295] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [295]),
        .Q(pst_reg_cnt[295]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[296] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [296]),
        .Q(pst_reg_cnt[296]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[297] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [297]),
        .Q(pst_reg_cnt[297]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[298] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [298]),
        .Q(pst_reg_cnt[298]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[299] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [299]),
        .Q(pst_reg_cnt[299]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[29] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [29]),
        .Q(pst_reg_cnt[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [2]),
        .Q(pst_reg_cnt[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[300] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [300]),
        .Q(pst_reg_cnt[300]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[301] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [301]),
        .Q(pst_reg_cnt[301]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[302] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [302]),
        .Q(pst_reg_cnt[302]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[303] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [303]),
        .Q(pst_reg_cnt[303]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[304] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [304]),
        .Q(pst_reg_cnt[304]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[305] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [305]),
        .Q(pst_reg_cnt[305]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[306] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [306]),
        .Q(pst_reg_cnt[306]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[307] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [307]),
        .Q(pst_reg_cnt[307]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[308] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [308]),
        .Q(pst_reg_cnt[308]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[309] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [309]),
        .Q(pst_reg_cnt[309]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[30] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [30]),
        .Q(pst_reg_cnt[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[310] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [310]),
        .Q(pst_reg_cnt[310]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[311] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [311]),
        .Q(pst_reg_cnt[311]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[312] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [312]),
        .Q(pst_reg_cnt[312]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[313] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [313]),
        .Q(pst_reg_cnt[313]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[314] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [314]),
        .Q(pst_reg_cnt[314]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[315] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [315]),
        .Q(pst_reg_cnt[315]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[316] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [316]),
        .Q(pst_reg_cnt[316]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[317] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [317]),
        .Q(pst_reg_cnt[317]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[318] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [318]),
        .Q(pst_reg_cnt[318]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[319] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [319]),
        .Q(pst_reg_cnt[319]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[31] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [31]),
        .Q(pst_reg_cnt[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[32] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [32]),
        .Q(pst_reg_cnt[32]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[33] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [33]),
        .Q(pst_reg_cnt[33]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[34] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [34]),
        .Q(pst_reg_cnt[34]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[35] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [35]),
        .Q(pst_reg_cnt[35]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[36] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [36]),
        .Q(pst_reg_cnt[36]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[37] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [37]),
        .Q(pst_reg_cnt[37]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[38] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [38]),
        .Q(pst_reg_cnt[38]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[39] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [39]),
        .Q(pst_reg_cnt[39]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [3]),
        .Q(pst_reg_cnt[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[40] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [40]),
        .Q(pst_reg_cnt[40]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[41] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [41]),
        .Q(pst_reg_cnt[41]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[42] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [42]),
        .Q(pst_reg_cnt[42]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[43] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [43]),
        .Q(pst_reg_cnt[43]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[44] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [44]),
        .Q(pst_reg_cnt[44]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[45] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [45]),
        .Q(pst_reg_cnt[45]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[46] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [46]),
        .Q(pst_reg_cnt[46]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[47] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [47]),
        .Q(pst_reg_cnt[47]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[48] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [48]),
        .Q(pst_reg_cnt[48]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[49] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [49]),
        .Q(pst_reg_cnt[49]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[4] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [4]),
        .Q(pst_reg_cnt[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[50] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [50]),
        .Q(pst_reg_cnt[50]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[51] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [51]),
        .Q(pst_reg_cnt[51]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[52] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [52]),
        .Q(pst_reg_cnt[52]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[53] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [53]),
        .Q(pst_reg_cnt[53]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[54] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [54]),
        .Q(pst_reg_cnt[54]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[55] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [55]),
        .Q(pst_reg_cnt[55]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[56] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [56]),
        .Q(pst_reg_cnt[56]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[57] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [57]),
        .Q(pst_reg_cnt[57]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[58] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [58]),
        .Q(pst_reg_cnt[58]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[59] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [59]),
        .Q(pst_reg_cnt[59]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[5] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [5]),
        .Q(pst_reg_cnt[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[60] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [60]),
        .Q(pst_reg_cnt[60]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[61] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [61]),
        .Q(pst_reg_cnt[61]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[62] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [62]),
        .Q(pst_reg_cnt[62]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[63] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [63]),
        .Q(pst_reg_cnt[63]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[64] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [64]),
        .Q(pst_reg_cnt[64]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[65] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [65]),
        .Q(pst_reg_cnt[65]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[66] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [66]),
        .Q(pst_reg_cnt[66]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[67] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [67]),
        .Q(pst_reg_cnt[67]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[68] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [68]),
        .Q(pst_reg_cnt[68]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[69] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [69]),
        .Q(pst_reg_cnt[69]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[6] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [6]),
        .Q(pst_reg_cnt[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[70] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [70]),
        .Q(pst_reg_cnt[70]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[71] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [71]),
        .Q(pst_reg_cnt[71]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[72] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [72]),
        .Q(pst_reg_cnt[72]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[73] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [73]),
        .Q(pst_reg_cnt[73]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[74] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [74]),
        .Q(pst_reg_cnt[74]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[75] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [75]),
        .Q(pst_reg_cnt[75]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[76] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [76]),
        .Q(pst_reg_cnt[76]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[77] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [77]),
        .Q(pst_reg_cnt[77]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[78] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [78]),
        .Q(pst_reg_cnt[78]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[79] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [79]),
        .Q(pst_reg_cnt[79]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[7] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [7]),
        .Q(pst_reg_cnt[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[80] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [80]),
        .Q(pst_reg_cnt[80]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[81] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [81]),
        .Q(pst_reg_cnt[81]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[82] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [82]),
        .Q(pst_reg_cnt[82]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[83] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [83]),
        .Q(pst_reg_cnt[83]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[84] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [84]),
        .Q(pst_reg_cnt[84]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[85] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [85]),
        .Q(pst_reg_cnt[85]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[86] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [86]),
        .Q(pst_reg_cnt[86]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[87] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [87]),
        .Q(pst_reg_cnt[87]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[88] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [88]),
        .Q(pst_reg_cnt[88]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[89] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [89]),
        .Q(pst_reg_cnt[89]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[8] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [8]),
        .Q(pst_reg_cnt[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[90] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [90]),
        .Q(pst_reg_cnt[90]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[91] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [91]),
        .Q(pst_reg_cnt[91]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[92] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [92]),
        .Q(pst_reg_cnt[92]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[93] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [93]),
        .Q(pst_reg_cnt[93]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[94] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [94]),
        .Q(pst_reg_cnt[94]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[95] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [95]),
        .Q(pst_reg_cnt[95]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[96] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [96]),
        .Q(pst_reg_cnt[96]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[97] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [97]),
        .Q(pst_reg_cnt[97]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[98] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [98]),
        .Q(pst_reg_cnt[98]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[99] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [99]),
        .Q(pst_reg_cnt[99]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \q_reg[9] 
       (.C(\q_reg[0]_0 ),
        .CE(1'b1),
        .D(\q_reg[319]_0 [9]),
        .Q(pst_reg_cnt[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFEFEFEFEFFFFFFFE)) 
    q_reg_i_100
       (.I0(q_reg_i_233_n_0),
        .I1(q_reg_i_234_n_0),
        .I2(pst_reg_cnt[67]),
        .I3(pst_reg_cnt[61]),
        .I4(q_reg_i_235_n_0),
        .I5(q_reg_i_236_n_0),
        .O(q_reg_i_100_n_0));
  LUT6 #(
    .INIT(64'h1F1100001F111F11)) 
    q_reg_i_101
       (.I0(pst_reg_cnt[271]),
        .I1(q_reg_i_232_n_0),
        .I2(q_reg_i_237_n_0),
        .I3(q_reg_i_238_n_0),
        .I4(q_reg_i_239_n_0),
        .I5(q_reg_i_240_n_0),
        .O(q_reg_i_101_n_0));
  LUT5 #(
    .INIT(32'h8088AAAA)) 
    q_reg_i_102
       (.I0(q_reg_i_241_n_0),
        .I1(q_reg_i_242_n_0),
        .I2(q_reg_i_243_n_0),
        .I3(q_reg_i_244_n_0),
        .I4(q_reg_i_245_n_0),
        .O(q_reg_i_102_n_0));
  LUT6 #(
    .INIT(64'hF0F0E0E0F0F0E0EE)) 
    q_reg_i_103
       (.I0(q_reg_i_246_n_0),
        .I1(q_reg_i_247_n_0),
        .I2(q_reg_i_248_n_0),
        .I3(pst_reg_cnt[109]),
        .I4(q_reg_i_249_n_0),
        .I5(q_reg_i_250_n_0),
        .O(q_reg_i_103_n_0));
  LUT6 #(
    .INIT(64'hCCDDCCDDCCDDCCD0)) 
    q_reg_i_104
       (.I0(pst_reg_cnt[271]),
        .I1(q_reg_i_251_n_0),
        .I2(q_reg_i_252_n_0),
        .I3(q_reg_i_232_n_0),
        .I4(pst_reg_cnt[263]),
        .I5(pst_reg_cnt[262]),
        .O(q_reg_i_104_n_0));
  LUT6 #(
    .INIT(64'hFF00FE00FF00FEFE)) 
    q_reg_i_105
       (.I0(q_reg_i_253_n_0),
        .I1(q_reg_i_213_n_0),
        .I2(pst_reg_cnt[181]),
        .I3(q_reg_i_254_n_0),
        .I4(q_reg_i_214_n_0),
        .I5(q_reg_i_255_n_0),
        .O(q_reg_i_105_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF000000FE)) 
    q_reg_i_106
       (.I0(q_reg_i_256_n_0),
        .I1(pst_reg_cnt[47]),
        .I2(pst_reg_cnt[46]),
        .I3(pst_reg_cnt[55]),
        .I4(q_reg_i_257_n_0),
        .I5(q_reg_i_236_n_0),
        .O(q_reg_i_106_n_0));
  LUT6 #(
    .INIT(64'h888A8888888AAAAA)) 
    q_reg_i_107
       (.I0(q_reg_i_258_n_0),
        .I1(q_reg_i_148_n_0),
        .I2(q_reg_i_259_n_0),
        .I3(q_reg_i_260_n_0),
        .I4(q_reg_i_261_n_0),
        .I5(pst_reg_cnt[135]),
        .O(q_reg_i_107_n_0));
  LUT5 #(
    .INIT(32'h0000BABB)) 
    q_reg_i_108
       (.I0(q_reg_i_262_n_0),
        .I1(q_reg_i_263_n_0),
        .I2(pst_reg_cnt[34]),
        .I3(pst_reg_cnt[33]),
        .I4(q_reg_i_171_n_0),
        .O(q_reg_i_108_n_0));
  LUT5 #(
    .INIT(32'hAAAAAAA8)) 
    q_reg_i_109
       (.I0(q_reg_i_264_n_0),
        .I1(q_reg_i_265_n_0),
        .I2(q_reg_i_266_n_0),
        .I3(pst_reg_cnt[265]),
        .I4(q_reg_i_267_n_0),
        .O(q_reg_i_109_n_0));
  LUT5 #(
    .INIT(32'hAAAA20AA)) 
    q_reg_i_110
       (.I0(q_reg_i_268_n_0),
        .I1(q_reg_i_204_n_0),
        .I2(q_reg_i_269_n_0),
        .I3(q_reg_i_206_n_0),
        .I4(q_reg_i_223_n_0),
        .O(q_reg_i_110_n_0));
  LUT6 #(
    .INIT(64'hDCCCDCCCCCCCDCCC)) 
    q_reg_i_111
       (.I0(q_reg_i_91_n_0),
        .I1(q_reg_i_120_n_0),
        .I2(q_reg_i_175_n_0),
        .I3(q_reg_i_270_n_0),
        .I4(q_reg_i_271_n_0),
        .I5(q_reg_i_196_n_0),
        .O(q_reg_i_111_n_0));
  LUT6 #(
    .INIT(64'h4040400044444444)) 
    q_reg_i_112
       (.I0(q_reg_i_272_n_0),
        .I1(q_reg_i_273_n_0),
        .I2(q_reg_i_196_n_0),
        .I3(q_reg_i_100_n_0),
        .I4(q_reg_i_197_n_0),
        .I5(q_reg_i_274_n_0),
        .O(q_reg_i_112_n_0));
  LUT6 #(
    .INIT(64'hFFFFD0FFD0D0D0D0)) 
    q_reg_i_113
       (.I0(q_reg_i_265_n_0),
        .I1(q_reg_i_275_n_0),
        .I2(q_reg_i_276_n_0),
        .I3(q_reg_i_199_n_0),
        .I4(q_reg_i_83_n_0),
        .I5(q_reg_i_198_n_0),
        .O(q_reg_i_113_n_0));
  LUT3 #(
    .INIT(8'h45)) 
    q_reg_i_114
       (.I0(q_reg_i_277_n_0),
        .I1(q_reg_i_141_n_0),
        .I2(q_reg_i_278_n_0),
        .O(q_reg_i_114_n_0));
  LUT6 #(
    .INIT(64'hFFFF5DFF5D5D5D5D)) 
    q_reg_i_115
       (.I0(q_reg_i_279_n_0),
        .I1(q_reg_i_280_n_0),
        .I2(q_reg_i_106_n_0),
        .I3(q_reg_i_217_n_0),
        .I4(q_reg_i_216_n_0),
        .I5(q_reg_i_215_n_0),
        .O(q_reg_i_115_n_0));
  LUT6 #(
    .INIT(64'h2F2F2F2F2F002F2F)) 
    q_reg_i_116
       (.I0(q_reg_i_281_n_0),
        .I1(q_reg_i_258_n_0),
        .I2(q_reg_i_204_n_0),
        .I3(q_reg_i_200_n_0),
        .I4(q_reg_i_282_n_0),
        .I5(q_reg_i_283_n_0),
        .O(q_reg_i_116_n_0));
  LUT6 #(
    .INIT(64'h00000000000000F1)) 
    q_reg_i_117
       (.I0(q_reg_i_284_n_0),
        .I1(pst_reg_cnt[43]),
        .I2(q_reg_i_285_n_0),
        .I3(q_reg_i_286_n_0),
        .I4(q_reg_i_287_n_0),
        .I5(q_reg_i_197_n_0),
        .O(q_reg_i_117_n_0));
  LUT6 #(
    .INIT(64'h0000000050505051)) 
    q_reg_i_118
       (.I0(pst_reg_cnt[31]),
        .I1(pst_reg_cnt[25]),
        .I2(q_reg_i_288_n_0),
        .I3(pst_reg_cnt[26]),
        .I4(pst_reg_cnt[27]),
        .I5(q_reg_i_289_n_0),
        .O(q_reg_i_118_n_0));
  LUT6 #(
    .INIT(64'h0054FFFFFFFFFFFF)) 
    q_reg_i_119
       (.I0(q_reg_i_273_n_0),
        .I1(q_reg_i_290_n_0),
        .I2(pst_reg_cnt[7]),
        .I3(q_reg_i_291_n_0),
        .I4(q_reg_i_292_n_0),
        .I5(q_reg_i_293_n_0),
        .O(q_reg_i_119_n_0));
  LUT5 #(
    .INIT(32'h8A88AAAA)) 
    q_reg_i_120
       (.I0(q_reg_i_294_n_0),
        .I1(q_reg_i_295_n_0),
        .I2(pst_reg_cnt[307]),
        .I3(pst_reg_cnt[306]),
        .I4(q_reg_i_296_n_0),
        .O(q_reg_i_120_n_0));
  LUT5 #(
    .INIT(32'h8AAA8A8A)) 
    q_reg_i_121
       (.I0(q_reg_i_297_n_0),
        .I1(q_reg_i_91_n_0),
        .I2(q_reg_i_137_n_0),
        .I3(q_reg_i_298_n_0),
        .I4(q_reg_i_148_n_0),
        .O(q_reg_i_121_n_0));
  LUT4 #(
    .INIT(16'hE0EE)) 
    q_reg_i_122
       (.I0(q_reg_i_299_n_0),
        .I1(q_reg_i_300_n_0),
        .I2(q_reg_i_164_n_0),
        .I3(q_reg_i_301_n_0),
        .O(q_reg_i_122_n_0));
  LUT5 #(
    .INIT(32'hBFBBAAAA)) 
    q_reg_i_123
       (.I0(q_reg_i_302_n_0),
        .I1(q_reg_i_303_n_0),
        .I2(q_reg_i_262_n_0),
        .I3(q_reg_i_281_n_0),
        .I4(q_reg_i_268_n_0),
        .O(q_reg_i_123_n_0));
  LUT5 #(
    .INIT(32'h0000BABB)) 
    q_reg_i_124
       (.I0(q_reg_i_296_n_0),
        .I1(q_reg_i_304_n_0),
        .I2(pst_reg_cnt[295]),
        .I3(pst_reg_cnt[294]),
        .I4(q_reg_i_305_n_0),
        .O(q_reg_i_124_n_0));
  LUT5 #(
    .INIT(32'h00A8AAAA)) 
    q_reg_i_125
       (.I0(q_reg_i_154_n_0),
        .I1(q_reg_i_298_n_0),
        .I2(q_reg_i_306_n_0),
        .I3(q_reg_i_307_n_0),
        .I4(q_reg_i_278_n_0),
        .O(q_reg_i_125_n_0));
  LUT6 #(
    .INIT(64'h888A8888888A888A)) 
    q_reg_i_126
       (.I0(q_reg_i_308_n_0),
        .I1(q_reg_i_309_n_0),
        .I2(q_reg_i_257_n_0),
        .I3(q_reg_i_236_n_0),
        .I4(pst_reg_cnt[55]),
        .I5(pst_reg_cnt[54]),
        .O(q_reg_i_126_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFF2FFFFFFFF)) 
    q_reg_i_127
       (.I0(pst_reg_cnt[117]),
        .I1(pst_reg_cnt[118]),
        .I2(pst_reg_cnt[119]),
        .I3(q_reg_i_220_n_0),
        .I4(q_reg_i_310_n_0),
        .I5(q_reg_i_311_n_0),
        .O(q_reg_i_127_n_0));
  LUT6 #(
    .INIT(64'h0001000000010001)) 
    q_reg_i_128
       (.I0(q_reg_i_312_n_0),
        .I1(pst_reg_cnt[101]),
        .I2(pst_reg_cnt[102]),
        .I3(pst_reg_cnt[103]),
        .I4(pst_reg_cnt[100]),
        .I5(pst_reg_cnt[99]),
        .O(q_reg_i_128_n_0));
  LUT6 #(
    .INIT(64'h0045FFFF00450045)) 
    q_reg_i_129
       (.I0(q_reg_i_313_n_0),
        .I1(pst_reg_cnt[247]),
        .I2(pst_reg_cnt[246]),
        .I3(q_reg_i_314_n_0),
        .I4(q_reg_i_278_n_0),
        .I5(q_reg_i_315_n_0),
        .O(q_reg_i_129_n_0));
  LUT5 #(
    .INIT(32'h0000AAFB)) 
    q_reg_i_130
       (.I0(q_reg_i_222_n_0),
        .I1(pst_reg_cnt[132]),
        .I2(pst_reg_cnt[133]),
        .I3(q_reg_i_316_n_0),
        .I4(q_reg_i_317_n_0),
        .O(q_reg_i_130_n_0));
  LUT6 #(
    .INIT(64'h000000A2AAAA00A2)) 
    q_reg_i_131
       (.I0(q_reg_i_229_n_0),
        .I1(pst_reg_cnt[141]),
        .I2(pst_reg_cnt[142]),
        .I3(pst_reg_cnt[143]),
        .I4(q_reg_i_185_n_0),
        .I5(q_reg_i_231_n_0),
        .O(q_reg_i_131_n_0));
  LUT6 #(
    .INIT(64'h0100010000000100)) 
    q_reg_i_132
       (.I0(pst_reg_cnt[158]),
        .I1(pst_reg_cnt[159]),
        .I2(q_reg_i_318_n_0),
        .I3(q_reg_i_319_n_0),
        .I4(pst_reg_cnt[156]),
        .I5(pst_reg_cnt[157]),
        .O(q_reg_i_132_n_0));
  LUT5 #(
    .INIT(32'h00007707)) 
    q_reg_i_133
       (.I0(q_reg_i_204_n_0),
        .I1(q_reg_i_320_n_0),
        .I2(pst_reg_cnt[84]),
        .I3(pst_reg_cnt[85]),
        .I4(q_reg_i_321_n_0),
        .O(q_reg_i_133_n_0));
  LUT6 #(
    .INIT(64'h0000000011155555)) 
    q_reg_i_134
       (.I0(q_reg_i_322_n_0),
        .I1(q_reg_i_128_n_0),
        .I2(q_reg_i_323_n_0),
        .I3(q_reg_i_324_n_0),
        .I4(q_reg_i_325_n_0),
        .I5(q_reg_i_277_n_0),
        .O(q_reg_i_134_n_0));
  LUT6 #(
    .INIT(64'h000000000000FF0E)) 
    q_reg_i_135
       (.I0(q_reg_i_326_n_0),
        .I1(pst_reg_cnt[188]),
        .I2(pst_reg_cnt[189]),
        .I3(pst_reg_cnt[190]),
        .I4(pst_reg_cnt[191]),
        .I5(q_reg_i_214_n_0),
        .O(q_reg_i_135_n_0));
  LUT6 #(
    .INIT(64'h00010000FFFFFFFF)) 
    q_reg_i_136
       (.I0(pst_reg_cnt[194]),
        .I1(pst_reg_cnt[195]),
        .I2(q_reg_i_327_n_0),
        .I3(q_reg_i_328_n_0),
        .I4(pst_reg_cnt[193]),
        .I5(q_reg_i_214_n_0),
        .O(q_reg_i_136_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    q_reg_i_137
       (.I0(pst_reg_cnt[209]),
        .I1(q_reg_i_329_n_0),
        .I2(q_reg_i_330_n_0),
        .I3(q_reg_i_331_n_0),
        .I4(pst_reg_cnt[207]),
        .O(q_reg_i_137_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    q_reg_i_138
       (.I0(q_reg_i_331_n_0),
        .I1(pst_reg_cnt[207]),
        .I2(pst_reg_cnt[206]),
        .I3(pst_reg_cnt[205]),
        .I4(pst_reg_cnt[204]),
        .O(q_reg_i_138_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    q_reg_i_139
       (.I0(q_reg_i_330_n_0),
        .I1(pst_reg_cnt[213]),
        .I2(pst_reg_cnt[212]),
        .I3(pst_reg_cnt[211]),
        .I4(pst_reg_cnt[210]),
        .O(q_reg_i_139_n_0));
  LUT5 #(
    .INIT(32'hFFFF4544)) 
    q_reg_i_140
       (.I0(q_reg_i_327_n_0),
        .I1(pst_reg_cnt[199]),
        .I2(pst_reg_cnt[198]),
        .I3(q_reg_i_332_n_0),
        .I4(q_reg_i_333_n_0),
        .O(q_reg_i_140_n_0));
  LUT6 #(
    .INIT(64'h000000000000BBBF)) 
    q_reg_i_141
       (.I0(q_reg_i_334_n_0),
        .I1(q_reg_i_335_n_0),
        .I2(q_reg_i_336_n_0),
        .I3(q_reg_i_337_n_0),
        .I4(q_reg_i_338_n_0),
        .I5(q_reg_i_339_n_0),
        .O(q_reg_i_141_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_142
       (.I0(q_reg_i_214_n_0),
        .I1(q_reg_i_340_n_0),
        .I2(pst_reg_cnt[187]),
        .I3(pst_reg_cnt[185]),
        .I4(pst_reg_cnt[186]),
        .O(q_reg_i_142_n_0));
  LUT6 #(
    .INIT(64'h00000000000000F1)) 
    q_reg_i_143
       (.I0(q_reg_i_341_n_0),
        .I1(pst_reg_cnt[180]),
        .I2(pst_reg_cnt[181]),
        .I3(q_reg_i_253_n_0),
        .I4(q_reg_i_213_n_0),
        .I5(q_reg_i_214_n_0),
        .O(q_reg_i_143_n_0));
  LUT5 #(
    .INIT(32'h00002022)) 
    q_reg_i_144
       (.I0(q_reg_i_342_n_0),
        .I1(pst_reg_cnt[167]),
        .I2(pst_reg_cnt[166]),
        .I3(pst_reg_cnt[165]),
        .I4(q_reg_i_343_n_0),
        .O(q_reg_i_144_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_145
       (.I0(q_reg_i_344_n_0),
        .I1(q_reg_i_345_n_0),
        .I2(q_reg_i_346_n_0),
        .I3(q_reg_i_347_n_0),
        .I4(q_reg_i_348_n_0),
        .I5(q_reg_i_349_n_0),
        .O(q_reg_i_145_n_0));
  LUT6 #(
    .INIT(64'h00000000FFFF00F2)) 
    q_reg_i_146
       (.I0(pst_reg_cnt[231]),
        .I1(pst_reg_cnt[232]),
        .I2(pst_reg_cnt[233]),
        .I3(pst_reg_cnt[234]),
        .I4(pst_reg_cnt[235]),
        .I5(q_reg_i_207_n_0),
        .O(q_reg_i_146_n_0));
  LUT5 #(
    .INIT(32'h0000BABB)) 
    q_reg_i_147
       (.I0(q_reg_i_298_n_0),
        .I1(q_reg_i_152_n_0),
        .I2(pst_reg_cnt[223]),
        .I3(pst_reg_cnt[222]),
        .I4(q_reg_i_307_n_0),
        .O(q_reg_i_147_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    q_reg_i_148
       (.I0(q_reg_i_350_n_0),
        .I1(q_reg_i_152_n_0),
        .I2(pst_reg_cnt[219]),
        .I3(pst_reg_cnt[218]),
        .I4(pst_reg_cnt[217]),
        .I5(pst_reg_cnt[216]),
        .O(q_reg_i_148_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_149
       (.I0(q_reg_i_350_n_0),
        .I1(pst_reg_cnt[217]),
        .I2(pst_reg_cnt[216]),
        .I3(pst_reg_cnt[219]),
        .I4(pst_reg_cnt[218]),
        .O(q_reg_i_149_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_150
       (.I0(pst_reg_cnt[213]),
        .I1(pst_reg_cnt[214]),
        .O(q_reg_i_150_n_0));
  LUT5 #(
    .INIT(32'hFFFFEEFE)) 
    q_reg_i_151
       (.I0(pst_reg_cnt[222]),
        .I1(pst_reg_cnt[223]),
        .I2(pst_reg_cnt[219]),
        .I3(pst_reg_cnt[220]),
        .I4(pst_reg_cnt[221]),
        .O(q_reg_i_151_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_152
       (.I0(q_reg_i_344_n_0),
        .I1(q_reg_i_345_n_0),
        .I2(q_reg_i_346_n_0),
        .I3(q_reg_i_351_n_0),
        .I4(q_reg_i_349_n_0),
        .I5(q_reg_i_352_n_0),
        .O(q_reg_i_152_n_0));
  LUT5 #(
    .INIT(32'h000000A2)) 
    q_reg_i_153
       (.I0(q_reg_i_353_n_0),
        .I1(pst_reg_cnt[273]),
        .I2(pst_reg_cnt[274]),
        .I3(pst_reg_cnt[275]),
        .I4(q_reg_i_354_n_0),
        .O(q_reg_i_153_n_0));
  LUT6 #(
    .INIT(64'h00000000AAAAEEAE)) 
    q_reg_i_154
       (.I0(q_reg_i_355_n_0),
        .I1(q_reg_i_356_n_0),
        .I2(pst_reg_cnt[276]),
        .I3(pst_reg_cnt[277]),
        .I4(pst_reg_cnt[278]),
        .I5(q_reg_i_357_n_0),
        .O(q_reg_i_154_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF54555454)) 
    q_reg_i_155
       (.I0(q_reg_i_294_n_0),
        .I1(q_reg_i_358_n_0),
        .I2(pst_reg_cnt[287]),
        .I3(pst_reg_cnt[286]),
        .I4(pst_reg_cnt[285]),
        .I5(q_reg_i_359_n_0),
        .O(q_reg_i_155_n_0));
  LUT6 #(
    .INIT(64'hABAAABABAAAAAAAA)) 
    q_reg_i_156
       (.I0(q_reg_i_360_n_0),
        .I1(q_reg_i_361_n_0),
        .I2(pst_reg_cnt[311]),
        .I3(pst_reg_cnt[310]),
        .I4(pst_reg_cnt[309]),
        .I5(q_reg_i_362_n_0),
        .O(q_reg_i_156_n_0));
  LUT6 #(
    .INIT(64'h00000000FFFFFFFD)) 
    q_reg_i_157
       (.I0(q_reg_i_199_n_0),
        .I1(q_reg_i_363_n_0),
        .I2(pst_reg_cnt[131]),
        .I3(pst_reg_cnt[130]),
        .I4(q_reg_i_364_n_0),
        .I5(q_reg_i_365_n_0),
        .O(q_reg_i_157_n_0));
  LUT6 #(
    .INIT(64'h4444444FFFFFFFFF)) 
    q_reg_i_158
       (.I0(q_reg_i_196_n_0),
        .I1(q_reg_i_271_n_0),
        .I2(q_reg_i_242_n_0),
        .I3(q_reg_i_321_n_0),
        .I4(pst_reg_cnt[85]),
        .I5(q_reg_i_175_n_0),
        .O(q_reg_i_158_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    q_reg_i_159
       (.I0(q_reg_i_366_n_0),
        .I1(q_reg_i_367_n_0),
        .I2(q_reg_i_100_n_0),
        .O(q_reg_i_159_n_0));
  LUT6 #(
    .INIT(64'h00000000AAAAFFFE)) 
    q_reg_i_160
       (.I0(q_reg_i_237_n_0),
        .I1(pst_reg_cnt[287]),
        .I2(pst_reg_cnt[286]),
        .I3(q_reg_i_358_n_0),
        .I4(q_reg_i_368_n_0),
        .I5(q_reg_i_369_n_0),
        .O(q_reg_i_160_n_0));
  LUT6 #(
    .INIT(64'h00000000D0D0D000)) 
    q_reg_i_161
       (.I0(q_reg_i_370_n_0),
        .I1(q_reg_i_241_n_0),
        .I2(q_reg_i_330_n_0),
        .I3(q_reg_i_195_n_0),
        .I4(q_reg_i_371_n_0),
        .I5(q_reg_i_109_n_0),
        .O(q_reg_i_161_n_0));
  LUT6 #(
    .INIT(64'h00000057FFFFFFFF)) 
    q_reg_i_162
       (.I0(q_reg_i_238_n_0),
        .I1(q_reg_i_304_n_0),
        .I2(pst_reg_cnt[295]),
        .I3(pst_reg_cnt[301]),
        .I4(q_reg_i_372_n_0),
        .I5(q_reg_i_373_n_0),
        .O(q_reg_i_162_n_0));
  LUT6 #(
    .INIT(64'h000000AE00000000)) 
    q_reg_i_163
       (.I0(q_reg_i_268_n_0),
        .I1(q_reg_i_374_n_0),
        .I2(q_reg_i_375_n_0),
        .I3(q_reg_i_376_n_0),
        .I4(q_reg_i_377_n_0),
        .I5(q_reg_i_378_n_0),
        .O(q_reg_i_163_n_0));
  LUT6 #(
    .INIT(64'h00000000000000AB)) 
    q_reg_i_164
       (.I0(q_reg_i_379_n_0),
        .I1(q_reg_i_380_n_0),
        .I2(q_reg_i_381_n_0),
        .I3(q_reg_i_382_n_0),
        .I4(q_reg_i_383_n_0),
        .I5(q_reg_i_220_n_0),
        .O(q_reg_i_164_n_0));
  LUT6 #(
    .INIT(64'h11110001FFFFFFFF)) 
    q_reg_i_165
       (.I0(q_reg_i_384_n_0),
        .I1(q_reg_i_385_n_0),
        .I2(q_reg_i_386_n_0),
        .I3(q_reg_i_387_n_0),
        .I4(q_reg_i_309_n_0),
        .I5(q_reg_i_277_n_0),
        .O(q_reg_i_165_n_0));
  LUT6 #(
    .INIT(64'h0E0E0E0E0E0F0E0E)) 
    q_reg_i_166
       (.I0(q_reg_i_290_n_0),
        .I1(q_reg_i_388_n_0),
        .I2(q_reg_i_389_n_0),
        .I3(q_reg_i_390_n_0),
        .I4(q_reg_i_391_n_0),
        .I5(q_reg_i_392_n_0),
        .O(q_reg_i_166_n_0));
  LUT6 #(
    .INIT(64'h000000000000FFBA)) 
    q_reg_i_167
       (.I0(q_reg_i_393_n_0),
        .I1(pst_reg_cnt[19]),
        .I2(pst_reg_cnt[18]),
        .I3(q_reg_i_394_n_0),
        .I4(q_reg_i_395_n_0),
        .I5(q_reg_i_396_n_0),
        .O(q_reg_i_167_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    q_reg_i_168
       (.I0(q_reg_i_289_n_0),
        .I1(pst_reg_cnt[31]),
        .I2(pst_reg_cnt[30]),
        .O(q_reg_i_168_n_0));
  LUT6 #(
    .INIT(64'h000000000000000E)) 
    q_reg_i_169
       (.I0(q_reg_i_397_n_0),
        .I1(q_reg_i_398_n_0),
        .I2(q_reg_i_399_n_0),
        .I3(q_reg_i_400_n_0),
        .I4(q_reg_i_289_n_0),
        .I5(pst_reg_cnt[31]),
        .O(q_reg_i_169_n_0));
  LUT6 #(
    .INIT(64'h000000000000FF0E)) 
    q_reg_i_170
       (.I0(q_reg_i_401_n_0),
        .I1(pst_reg_cnt[35]),
        .I2(pst_reg_cnt[36]),
        .I3(pst_reg_cnt[37]),
        .I4(q_reg_i_402_n_0),
        .I5(q_reg_i_403_n_0),
        .O(q_reg_i_170_n_0));
  LUT6 #(
    .INIT(64'hFFFBFFFFFFFAFFFF)) 
    q_reg_i_171
       (.I0(q_reg_i_404_n_0),
        .I1(pst_reg_cnt[40]),
        .I2(pst_reg_cnt[41]),
        .I3(q_reg_i_405_n_0),
        .I4(q_reg_i_406_n_0),
        .I5(pst_reg_cnt[39]),
        .O(q_reg_i_171_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEF0)) 
    q_reg_i_172
       (.I0(q_reg_i_407_n_0),
        .I1(pst_reg_cnt[51]),
        .I2(q_reg_i_257_n_0),
        .I3(q_reg_i_408_n_0),
        .I4(q_reg_i_409_n_0),
        .I5(q_reg_i_236_n_0),
        .O(q_reg_i_172_n_0));
  LUT6 #(
    .INIT(64'h0000FF0E00000000)) 
    q_reg_i_173
       (.I0(q_reg_i_410_n_0),
        .I1(pst_reg_cnt[44]),
        .I2(pst_reg_cnt[45]),
        .I3(pst_reg_cnt[46]),
        .I4(pst_reg_cnt[47]),
        .I5(q_reg_i_406_n_0),
        .O(q_reg_i_173_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF0FE)) 
    q_reg_i_174
       (.I0(q_reg_i_411_n_0),
        .I1(pst_reg_cnt[72]),
        .I2(q_reg_i_412_n_0),
        .I3(pst_reg_cnt[73]),
        .I4(q_reg_i_413_n_0),
        .I5(q_reg_i_414_n_0),
        .O(q_reg_i_174_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_175
       (.I0(pst_reg_cnt[79]),
        .I1(q_reg_i_415_n_0),
        .I2(q_reg_i_325_n_0),
        .O(q_reg_i_175_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_176
       (.I0(pst_reg_cnt[61]),
        .I1(pst_reg_cnt[60]),
        .I2(pst_reg_cnt[63]),
        .I3(pst_reg_cnt[62]),
        .I4(q_reg_i_236_n_0),
        .O(q_reg_i_176_n_0));
  LUT6 #(
    .INIT(64'h000000000000CC08)) 
    q_reg_i_177
       (.I0(q_reg_i_416_n_0),
        .I1(q_reg_i_286_n_0),
        .I2(pst_reg_cnt[54]),
        .I3(pst_reg_cnt[55]),
        .I4(q_reg_i_236_n_0),
        .I5(q_reg_i_257_n_0),
        .O(q_reg_i_177_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF4544)) 
    q_reg_i_178
       (.I0(q_reg_i_309_n_0),
        .I1(q_reg_i_387_n_0),
        .I2(pst_reg_cnt[61]),
        .I3(pst_reg_cnt[60]),
        .I4(q_reg_i_385_n_0),
        .I5(q_reg_i_384_n_0),
        .O(q_reg_i_178_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    q_reg_i_179
       (.I0(q_reg_i_92_n_0),
        .I1(q_reg_i_190_n_0),
        .I2(q_reg_i_191_n_0),
        .O(q_reg_i_179_n_0));
  LUT5 #(
    .INIT(32'h757575FF)) 
    q_reg_i_180
       (.I0(q_reg_i_99_n_0),
        .I1(q_reg_i_83_n_0),
        .I2(q_reg_i_100_n_0),
        .I3(q_reg_i_101_n_0),
        .I4(q_reg_i_102_n_0),
        .O(q_reg_i_180_n_0));
  LUT5 #(
    .INIT(32'hFFD0D0D0)) 
    q_reg_i_181
       (.I0(q_reg_i_105_n_0),
        .I1(q_reg_i_104_n_0),
        .I2(q_reg_i_103_n_0),
        .I3(q_reg_i_107_n_0),
        .I4(q_reg_i_106_n_0),
        .O(q_reg_i_181_n_0));
  LUT3 #(
    .INIT(8'h32)) 
    q_reg_i_182
       (.I0(q_reg_i_192_n_0),
        .I1(q_reg_i_87_n_0),
        .I2(q_reg_i_239_n_0),
        .O(q_reg_i_182_n_0));
  LUT3 #(
    .INIT(8'h8E)) 
    q_reg_i_183
       (.I0(q_reg_i_93_n_0),
        .I1(q_reg_i_94_n_0),
        .I2(q_reg_i_95_n_0),
        .O(q_reg_i_183_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_184
       (.I0(pst_reg_cnt[142]),
        .I1(pst_reg_cnt[143]),
        .I2(pst_reg_cnt[140]),
        .I3(pst_reg_cnt[141]),
        .O(q_reg_i_184_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_185
       (.I0(q_reg_i_417_n_0),
        .I1(q_reg_i_418_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_420_n_0),
        .I4(q_reg_i_421_n_0),
        .I5(q_reg_i_422_n_0),
        .O(q_reg_i_185_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_186
       (.I0(pst_reg_cnt[139]),
        .I1(pst_reg_cnt[138]),
        .O(q_reg_i_186_n_0));
  LUT6 #(
    .INIT(64'hCD32CD32CD3232CD)) 
    q_reg_i_187
       (.I0(q_reg_i_239_n_0),
        .I1(q_reg_i_87_n_0),
        .I2(q_reg_i_192_n_0),
        .I3(q_reg_i_181_n_0),
        .I4(q_reg_i_423_n_0),
        .I5(q_reg_i_424_n_0),
        .O(q_reg_i_187_n_0));
  LUT6 #(
    .INIT(64'h9696966996696969)) 
    q_reg_i_188
       (.I0(\q_reg[109]_0 ),
        .I1(q_reg_i_101_n_0),
        .I2(q_reg_i_425_n_0),
        .I3(q_reg_i_98_n_0),
        .I4(q_reg_i_97_n_0),
        .I5(q_reg_i_96_n_0),
        .O(q_reg_i_188_n_0));
  LUT4 #(
    .INIT(16'h888A)) 
    q_reg_i_189
       (.I0(q_reg_i_195_n_0),
        .I1(q_reg_i_83_n_0),
        .I2(pst_reg_cnt[135]),
        .I3(q_reg_i_261_n_0),
        .O(q_reg_i_189_n_0));
  LUT6 #(
    .INIT(64'h5555100055555555)) 
    q_reg_i_190
       (.I0(q_reg_i_200_n_0),
        .I1(q_reg_i_105_n_0),
        .I2(q_reg_i_201_n_0),
        .I3(q_reg_i_202_n_0),
        .I4(q_reg_i_203_n_0),
        .I5(q_reg_i_141_n_0),
        .O(q_reg_i_190_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    q_reg_i_191
       (.I0(q_reg_i_105_n_0),
        .I1(q_reg_i_426_n_0),
        .O(q_reg_i_191_n_0));
  LUT5 #(
    .INIT(32'hD02FD0D0)) 
    q_reg_i_192
       (.I0(q_reg_i_427_n_0),
        .I1(q_reg_i_261_n_0),
        .I2(q_reg_i_428_n_0),
        .I3(q_reg_i_83_n_0),
        .I4(q_reg_i_222_n_0),
        .O(q_reg_i_192_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_193
       (.I0(q_reg_i_429_n_0),
        .I1(q_reg_i_422_n_0),
        .I2(q_reg_i_430_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_418_n_0),
        .I5(q_reg_i_431_n_0),
        .O(q_reg_i_193_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_194
       (.I0(pst_reg_cnt[147]),
        .I1(pst_reg_cnt[146]),
        .O(q_reg_i_194_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    q_reg_i_195
       (.I0(pst_reg_cnt[217]),
        .I1(q_reg_i_350_n_0),
        .I2(q_reg_i_152_n_0),
        .I3(pst_reg_cnt[219]),
        .I4(pst_reg_cnt[218]),
        .O(q_reg_i_195_n_0));
  LUT6 #(
    .INIT(64'h0011001000110011)) 
    q_reg_i_196
       (.I0(pst_reg_cnt[95]),
        .I1(pst_reg_cnt[94]),
        .I2(pst_reg_cnt[91]),
        .I3(q_reg_i_325_n_0),
        .I4(q_reg_i_432_n_0),
        .I5(q_reg_i_433_n_0),
        .O(q_reg_i_196_n_0));
  LUT6 #(
    .INIT(64'h3020302030203030)) 
    q_reg_i_197
       (.I0(pst_reg_cnt[37]),
        .I1(q_reg_i_434_n_0),
        .I2(q_reg_i_406_n_0),
        .I3(q_reg_i_402_n_0),
        .I4(pst_reg_cnt[34]),
        .I5(q_reg_i_435_n_0),
        .O(q_reg_i_197_n_0));
  LUT6 #(
    .INIT(64'h0055005000550054)) 
    q_reg_i_198
       (.I0(pst_reg_cnt[175]),
        .I1(q_reg_i_436_n_0),
        .I2(pst_reg_cnt[172]),
        .I3(q_reg_i_339_n_0),
        .I4(q_reg_i_437_n_0),
        .I5(pst_reg_cnt[171]),
        .O(q_reg_i_198_n_0));
  LUT6 #(
    .INIT(64'hFFFF1110FFFFFFFF)) 
    q_reg_i_199
       (.I0(q_reg_i_438_n_0),
        .I1(q_reg_i_439_n_0),
        .I2(pst_reg_cnt[115]),
        .I3(q_reg_i_440_n_0),
        .I4(q_reg_i_441_n_0),
        .I5(q_reg_i_442_n_0),
        .O(q_reg_i_199_n_0));
  LUT6 #(
    .INIT(64'hD0DDD000DDDDDDDD)) 
    q_reg_i_200
       (.I0(q_reg_i_443_n_0),
        .I1(q_reg_i_444_n_0),
        .I2(q_reg_i_445_n_0),
        .I3(q_reg_i_261_n_0),
        .I4(pst_reg_cnt[135]),
        .I5(q_reg_i_311_n_0),
        .O(q_reg_i_200_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_201
       (.I0(pst_reg_cnt[154]),
        .I1(pst_reg_cnt[155]),
        .I2(q_reg_i_318_n_0),
        .I3(q_reg_i_446_n_0),
        .O(q_reg_i_201_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF01)) 
    q_reg_i_202
       (.I0(q_reg_i_447_n_0),
        .I1(q_reg_i_335_n_0),
        .I2(pst_reg_cnt[163]),
        .I3(q_reg_i_437_n_0),
        .I4(q_reg_i_339_n_0),
        .I5(pst_reg_cnt[172]),
        .O(q_reg_i_202_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    q_reg_i_203
       (.I0(q_reg_i_331_n_0),
        .I1(pst_reg_cnt[226]),
        .I2(pst_reg_cnt[227]),
        .I3(q_reg_i_448_n_0),
        .I4(q_reg_i_449_n_0),
        .I5(q_reg_i_195_n_0),
        .O(q_reg_i_203_n_0));
  LUT6 #(
    .INIT(64'h000000000000000B)) 
    q_reg_i_204
       (.I0(pst_reg_cnt[82]),
        .I1(pst_reg_cnt[81]),
        .I2(q_reg_i_450_n_0),
        .I3(q_reg_i_433_n_0),
        .I4(q_reg_i_325_n_0),
        .I5(pst_reg_cnt[83]),
        .O(q_reg_i_204_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_205
       (.I0(pst_reg_cnt[93]),
        .I1(pst_reg_cnt[92]),
        .I2(pst_reg_cnt[95]),
        .I3(pst_reg_cnt[94]),
        .I4(q_reg_i_325_n_0),
        .O(q_reg_i_205_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFEE)) 
    q_reg_i_206
       (.I0(q_reg_i_414_n_0),
        .I1(q_reg_i_413_n_0),
        .I2(pst_reg_cnt[73]),
        .I3(pst_reg_cnt[74]),
        .I4(pst_reg_cnt[75]),
        .I5(pst_reg_cnt[72]),
        .O(q_reg_i_206_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_207
       (.I0(pst_reg_cnt[239]),
        .I1(pst_reg_cnt[238]),
        .I2(pst_reg_cnt[237]),
        .I3(pst_reg_cnt[236]),
        .I4(q_reg_i_145_n_0),
        .O(q_reg_i_207_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    q_reg_i_208
       (.I0(pst_reg_cnt[255]),
        .I1(pst_reg_cnt[254]),
        .I2(q_reg_i_419_n_0),
        .I3(pst_reg_cnt[253]),
        .I4(pst_reg_cnt[252]),
        .O(q_reg_i_208_n_0));
  LUT6 #(
    .INIT(64'h0000000001010001)) 
    q_reg_i_209
       (.I0(q_reg_i_313_n_0),
        .I1(pst_reg_cnt[247]),
        .I2(pst_reg_cnt[246]),
        .I3(pst_reg_cnt[243]),
        .I4(pst_reg_cnt[244]),
        .I5(pst_reg_cnt[245]),
        .O(q_reg_i_209_n_0));
  LUT6 #(
    .INIT(64'h0011001000110011)) 
    q_reg_i_210
       (.I0(pst_reg_cnt[167]),
        .I1(pst_reg_cnt[166]),
        .I2(pst_reg_cnt[163]),
        .I3(q_reg_i_343_n_0),
        .I4(q_reg_i_447_n_0),
        .I5(q_reg_i_318_n_0),
        .O(q_reg_i_210_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_211
       (.I0(pst_reg_cnt[180]),
        .I1(pst_reg_cnt[178]),
        .I2(pst_reg_cnt[179]),
        .O(q_reg_i_211_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_212
       (.I0(pst_reg_cnt[181]),
        .I1(q_reg_i_422_n_0),
        .I2(q_reg_i_430_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_213_n_0),
        .I5(q_reg_i_253_n_0),
        .O(q_reg_i_212_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_213
       (.I0(q_reg_i_340_n_0),
        .I1(pst_reg_cnt[185]),
        .I2(pst_reg_cnt[184]),
        .I3(pst_reg_cnt[187]),
        .I4(pst_reg_cnt[186]),
        .O(q_reg_i_213_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_214
       (.I0(q_reg_i_419_n_0),
        .I1(q_reg_i_420_n_0),
        .I2(q_reg_i_421_n_0),
        .I3(q_reg_i_422_n_0),
        .O(q_reg_i_214_n_0));
  LUT5 #(
    .INIT(32'h0000BBFB)) 
    q_reg_i_215
       (.I0(q_reg_i_105_n_0),
        .I1(q_reg_i_201_n_0),
        .I2(q_reg_i_451_n_0),
        .I3(q_reg_i_428_n_0),
        .I4(q_reg_i_203_n_0),
        .O(q_reg_i_215_n_0));
  LUT4 #(
    .INIT(16'hDDD0)) 
    q_reg_i_216
       (.I0(q_reg_i_261_n_0),
        .I1(q_reg_i_452_n_0),
        .I2(q_reg_i_220_n_0),
        .I3(pst_reg_cnt[127]),
        .O(q_reg_i_216_n_0));
  LUT4 #(
    .INIT(16'hFFF1)) 
    q_reg_i_217
       (.I0(q_reg_i_453_n_0),
        .I1(q_reg_i_242_n_0),
        .I2(q_reg_i_454_n_0),
        .I3(q_reg_i_103_n_0),
        .O(q_reg_i_217_n_0));
  LUT6 #(
    .INIT(64'h10101055FFFFFFFF)) 
    q_reg_i_218
       (.I0(q_reg_i_106_n_0),
        .I1(q_reg_i_240_n_0),
        .I2(q_reg_i_455_n_0),
        .I3(q_reg_i_456_n_0),
        .I4(q_reg_i_457_n_0),
        .I5(q_reg_i_279_n_0),
        .O(q_reg_i_218_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_219
       (.I0(pst_reg_cnt[126]),
        .I1(pst_reg_cnt[127]),
        .O(q_reg_i_219_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_220
       (.I0(q_reg_i_422_n_0),
        .I1(q_reg_i_421_n_0),
        .I2(q_reg_i_420_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_458_n_0),
        .O(q_reg_i_220_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFCFFFFFFFE)) 
    q_reg_i_221
       (.I0(pst_reg_cnt[144]),
        .I1(q_reg_i_194_n_0),
        .I2(q_reg_i_431_n_0),
        .I3(q_reg_i_318_n_0),
        .I4(q_reg_i_429_n_0),
        .I5(pst_reg_cnt[145]),
        .O(q_reg_i_221_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEFFFFFFFEE)) 
    q_reg_i_222
       (.I0(q_reg_i_184_n_0),
        .I1(q_reg_i_186_n_0),
        .I2(pst_reg_cnt[136]),
        .I3(pst_reg_cnt[137]),
        .I4(q_reg_i_185_n_0),
        .I5(pst_reg_cnt[135]),
        .O(q_reg_i_222_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAA02000202)) 
    q_reg_i_223
       (.I0(q_reg_i_459_n_0),
        .I1(q_reg_i_447_n_0),
        .I2(q_reg_i_343_n_0),
        .I3(pst_reg_cnt[163]),
        .I4(pst_reg_cnt[162]),
        .I5(q_reg_i_334_n_0),
        .O(q_reg_i_223_n_0));
  LUT5 #(
    .INIT(32'hA0A8AAAA)) 
    q_reg_i_224
       (.I0(q_reg_i_460_n_0),
        .I1(pst_reg_cnt[198]),
        .I2(q_reg_i_327_n_0),
        .I3(pst_reg_cnt[199]),
        .I4(q_reg_i_461_n_0),
        .O(q_reg_i_224_n_0));
  LUT6 #(
    .INIT(64'hFFBF0000FFBFFFBF)) 
    q_reg_i_225
       (.I0(q_reg_i_462_n_0),
        .I1(q_reg_i_463_n_0),
        .I2(q_reg_i_464_n_0),
        .I3(q_reg_i_465_n_0),
        .I4(q_reg_i_466_n_0),
        .I5(q_reg_i_467_n_0),
        .O(q_reg_i_225_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAA80A0)) 
    q_reg_i_226
       (.I0(q_reg_i_468_n_0),
        .I1(pst_reg_cnt[171]),
        .I2(q_reg_i_451_n_0),
        .I3(q_reg_i_436_n_0),
        .I4(pst_reg_cnt[175]),
        .I5(q_reg_i_339_n_0),
        .O(q_reg_i_226_n_0));
  LUT6 #(
    .INIT(64'h00000000000F000E)) 
    q_reg_i_227
       (.I0(q_reg_i_469_n_0),
        .I1(q_reg_i_470_n_0),
        .I2(q_reg_i_318_n_0),
        .I3(q_reg_i_471_n_0),
        .I4(pst_reg_cnt[156]),
        .I5(pst_reg_cnt[157]),
        .O(q_reg_i_227_n_0));
  LUT6 #(
    .INIT(64'h0010001000100011)) 
    q_reg_i_228
       (.I0(q_reg_i_184_n_0),
        .I1(pst_reg_cnt[139]),
        .I2(q_reg_i_472_n_0),
        .I3(q_reg_i_185_n_0),
        .I4(pst_reg_cnt[133]),
        .I5(q_reg_i_473_n_0),
        .O(q_reg_i_228_n_0));
  LUT6 #(
    .INIT(64'h0000000000000301)) 
    q_reg_i_229
       (.I0(pst_reg_cnt[147]),
        .I1(q_reg_i_431_n_0),
        .I2(q_reg_i_318_n_0),
        .I3(pst_reg_cnt[148]),
        .I4(pst_reg_cnt[151]),
        .I5(q_reg_i_474_n_0),
        .O(q_reg_i_229_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    q_reg_i_230
       (.I0(pst_reg_cnt[143]),
        .I1(pst_reg_cnt[142]),
        .I2(pst_reg_cnt[141]),
        .O(q_reg_i_230_n_0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    q_reg_i_231
       (.I0(pst_reg_cnt[145]),
        .I1(q_reg_i_429_n_0),
        .I2(q_reg_i_318_n_0),
        .I3(q_reg_i_431_n_0),
        .I4(pst_reg_cnt[147]),
        .I5(pst_reg_cnt[146]),
        .O(q_reg_i_231_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_232
       (.I0(q_reg_i_348_n_0),
        .I1(q_reg_i_347_n_0),
        .I2(q_reg_i_346_n_0),
        .I3(q_reg_i_344_n_0),
        .O(q_reg_i_232_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_233
       (.I0(q_reg_i_415_n_0),
        .I1(q_reg_i_475_n_0),
        .I2(q_reg_i_458_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_476_n_0),
        .I5(q_reg_i_477_n_0),
        .O(q_reg_i_233_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_234
       (.I0(pst_reg_cnt[70]),
        .I1(pst_reg_cnt[71]),
        .I2(pst_reg_cnt[68]),
        .I3(pst_reg_cnt[69]),
        .O(q_reg_i_234_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_235
       (.I0(pst_reg_cnt[63]),
        .I1(pst_reg_cnt[62]),
        .O(q_reg_i_235_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_236
       (.I0(q_reg_i_422_n_0),
        .I1(q_reg_i_430_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_475_n_0),
        .I4(q_reg_i_478_n_0),
        .I5(q_reg_i_458_n_0),
        .O(q_reg_i_236_n_0));
  LUT6 #(
    .INIT(64'h1010101010101011)) 
    q_reg_i_237
       (.I0(q_reg_i_479_n_0),
        .I1(pst_reg_cnt[283]),
        .I2(q_reg_i_251_n_0),
        .I3(pst_reg_cnt[279]),
        .I4(pst_reg_cnt[277]),
        .I5(pst_reg_cnt[278]),
        .O(q_reg_i_237_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    q_reg_i_238
       (.I0(pst_reg_cnt[298]),
        .I1(pst_reg_cnt[299]),
        .I2(q_reg_i_480_n_0),
        .O(q_reg_i_238_n_0));
  LUT5 #(
    .INIT(32'hDDDDDDD5)) 
    q_reg_i_239
       (.I0(q_reg_i_481_n_0),
        .I1(q_reg_i_195_n_0),
        .I2(q_reg_i_214_n_0),
        .I3(pst_reg_cnt[190]),
        .I4(pst_reg_cnt[191]),
        .O(q_reg_i_239_n_0));
  LUT6 #(
    .INIT(64'h2BD4D42BD42B2BD4)) 
    q_reg_i_24
       (.I0(\q_reg[34]_0 ),
        .I1(q_reg_i_116_0),
        .I2(q_reg_i_113_0),
        .I3(q_reg[1]),
        .I4(q_reg_i_66_n_0),
        .I5(q_reg_i_67_n_0),
        .O(q_reg_i_67_0[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_240
       (.I0(pst_reg_cnt[10]),
        .I1(pst_reg_cnt[11]),
        .I2(q_reg_i_482_n_0),
        .I3(q_reg_i_483_n_0),
        .I4(pst_reg_cnt[16]),
        .I5(q_reg_i_396_n_0),
        .O(q_reg_i_240_n_0));
  LUT6 #(
    .INIT(64'h0000003200000033)) 
    q_reg_i_241
       (.I0(q_reg_i_484_n_0),
        .I1(q_reg_i_448_n_0),
        .I2(pst_reg_cnt[228]),
        .I3(pst_reg_cnt[229]),
        .I4(q_reg_i_485_n_0),
        .I5(q_reg_i_486_n_0),
        .O(q_reg_i_241_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    q_reg_i_242
       (.I0(pst_reg_cnt[82]),
        .I1(pst_reg_cnt[83]),
        .I2(q_reg_i_325_n_0),
        .I3(q_reg_i_433_n_0),
        .I4(q_reg_i_450_n_0),
        .O(q_reg_i_242_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    q_reg_i_243
       (.I0(q_reg_i_289_n_0),
        .I1(pst_reg_cnt[31]),
        .I2(pst_reg_cnt[28]),
        .I3(pst_reg_cnt[30]),
        .I4(pst_reg_cnt[29]),
        .O(q_reg_i_243_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_244
       (.I0(pst_reg_cnt[55]),
        .I1(q_reg_i_257_n_0),
        .I2(q_reg_i_476_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_487_n_0),
        .I5(q_reg_i_458_n_0),
        .O(q_reg_i_244_n_0));
  LUT6 #(
    .INIT(64'h1010101010101011)) 
    q_reg_i_245
       (.I0(q_reg_i_488_n_0),
        .I1(pst_reg_cnt[163]),
        .I2(q_reg_i_261_n_0),
        .I3(pst_reg_cnt[109]),
        .I4(q_reg_i_249_n_0),
        .I5(q_reg_i_250_n_0),
        .O(q_reg_i_245_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_246
       (.I0(q_reg_i_489_n_0),
        .I1(pst_reg_cnt[107]),
        .I2(pst_reg_cnt[106]),
        .I3(pst_reg_cnt[105]),
        .I4(pst_reg_cnt[104]),
        .O(q_reg_i_246_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_247
       (.I0(pst_reg_cnt[102]),
        .I1(pst_reg_cnt[103]),
        .I2(pst_reg_cnt[100]),
        .I3(pst_reg_cnt[101]),
        .O(q_reg_i_247_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_248
       (.I0(q_reg_i_438_n_0),
        .I1(q_reg_i_422_n_0),
        .I2(q_reg_i_430_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_458_n_0),
        .I5(q_reg_i_310_n_0),
        .O(q_reg_i_248_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_249
       (.I0(q_reg_i_490_n_0),
        .I1(q_reg_i_458_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_420_n_0),
        .I4(q_reg_i_421_n_0),
        .I5(q_reg_i_422_n_0),
        .O(q_reg_i_249_n_0));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    q_reg_i_25
       (.I0(\q_reg[55]_0 ),
        .I1(\q_reg[295]_0 ),
        .I2(\q_reg[43]_0 ),
        .I3(q_reg[0]),
        .I4(q_reg_i_68_n_0),
        .I5(q_reg_i_69_n_0),
        .O(q_reg_i_67_0[0]));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_250
       (.I0(pst_reg_cnt[111]),
        .I1(pst_reg_cnt[110]),
        .O(q_reg_i_250_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_251
       (.I0(q_reg_i_348_n_0),
        .I1(q_reg_i_347_n_0),
        .I2(q_reg_i_346_n_0),
        .I3(q_reg_i_491_n_0),
        .O(q_reg_i_251_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_252
       (.I0(pst_reg_cnt[265]),
        .I1(pst_reg_cnt[264]),
        .I2(pst_reg_cnt[267]),
        .I3(pst_reg_cnt[266]),
        .I4(q_reg_i_492_n_0),
        .O(q_reg_i_252_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_253
       (.I0(pst_reg_cnt[183]),
        .I1(pst_reg_cnt[182]),
        .O(q_reg_i_253_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_254
       (.I0(pst_reg_cnt[199]),
        .I1(q_reg_i_420_n_0),
        .I2(q_reg_i_152_n_0),
        .I3(q_reg_i_493_n_0),
        .O(q_reg_i_254_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_255
       (.I0(pst_reg_cnt[191]),
        .I1(pst_reg_cnt[190]),
        .O(q_reg_i_255_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_256
       (.I0(q_reg_i_409_n_0),
        .I1(pst_reg_cnt[51]),
        .I2(pst_reg_cnt[48]),
        .I3(pst_reg_cnt[50]),
        .I4(pst_reg_cnt[49]),
        .O(q_reg_i_256_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_257
       (.I0(pst_reg_cnt[57]),
        .I1(pst_reg_cnt[56]),
        .I2(pst_reg_cnt[59]),
        .I3(pst_reg_cnt[58]),
        .I4(q_reg_i_494_n_0),
        .O(q_reg_i_257_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_258
       (.I0(q_reg_i_257_n_0),
        .I1(q_reg_i_476_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_487_n_0),
        .I4(q_reg_i_458_n_0),
        .I5(q_reg_i_495_n_0),
        .O(q_reg_i_258_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_259
       (.I0(pst_reg_cnt[137]),
        .I1(pst_reg_cnt[138]),
        .I2(pst_reg_cnt[139]),
        .O(q_reg_i_259_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_260
       (.I0(q_reg_i_184_n_0),
        .I1(q_reg_i_422_n_0),
        .I2(q_reg_i_430_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_418_n_0),
        .I5(q_reg_i_417_n_0),
        .O(q_reg_i_260_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_261
       (.I0(q_reg_i_472_n_0),
        .I1(q_reg_i_422_n_0),
        .I2(q_reg_i_430_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_418_n_0),
        .I5(q_reg_i_417_n_0),
        .O(q_reg_i_261_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    q_reg_i_262
       (.I0(q_reg_i_434_n_0),
        .I1(q_reg_i_496_n_0),
        .I2(q_reg_i_236_n_0),
        .I3(q_reg_i_402_n_0),
        .I4(pst_reg_cnt[37]),
        .I5(pst_reg_cnt[36]),
        .O(q_reg_i_262_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_263
       (.I0(q_reg_i_236_n_0),
        .I1(q_reg_i_496_n_0),
        .I2(q_reg_i_434_n_0),
        .I3(q_reg_i_497_n_0),
        .I4(pst_reg_cnt[35]),
        .O(q_reg_i_263_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFAAFE)) 
    q_reg_i_264
       (.I0(pst_reg_cnt[247]),
        .I1(pst_reg_cnt[241]),
        .I2(q_reg_i_498_n_0),
        .I3(q_reg_i_499_n_0),
        .I4(q_reg_i_500_n_0),
        .I5(q_reg_i_419_n_0),
        .O(q_reg_i_264_n_0));
  LUT6 #(
    .INIT(64'h0E0E0E0E0E0E0E0F)) 
    q_reg_i_265
       (.I0(pst_reg_cnt[253]),
        .I1(q_reg_i_501_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(pst_reg_cnt[250]),
        .I4(pst_reg_cnt[251]),
        .I5(pst_reg_cnt[252]),
        .O(q_reg_i_265_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_266
       (.I0(pst_reg_cnt[266]),
        .I1(pst_reg_cnt[267]),
        .I2(q_reg_i_232_n_0),
        .I3(q_reg_i_492_n_0),
        .O(q_reg_i_266_n_0));
  LUT6 #(
    .INIT(64'h00000000000000FE)) 
    q_reg_i_267
       (.I0(pst_reg_cnt[259]),
        .I1(pst_reg_cnt[261]),
        .I2(pst_reg_cnt[260]),
        .I3(pst_reg_cnt[263]),
        .I4(pst_reg_cnt[262]),
        .I5(q_reg_i_502_n_0),
        .O(q_reg_i_267_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF2)) 
    q_reg_i_268
       (.I0(pst_reg_cnt[9]),
        .I1(pst_reg_cnt[10]),
        .I2(pst_reg_cnt[11]),
        .I3(q_reg_i_482_n_0),
        .I4(q_reg_i_503_n_0),
        .I5(q_reg_i_396_n_0),
        .O(q_reg_i_268_n_0));
  LUT4 #(
    .INIT(16'h1011)) 
    q_reg_i_269
       (.I0(q_reg_i_325_n_0),
        .I1(q_reg_i_432_n_0),
        .I2(pst_reg_cnt[91]),
        .I3(pst_reg_cnt[90]),
        .O(q_reg_i_269_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF1)) 
    q_reg_i_270
       (.I0(pst_reg_cnt[84]),
        .I1(q_reg_i_504_n_0),
        .I2(q_reg_i_505_n_0),
        .I3(q_reg_i_433_n_0),
        .I4(q_reg_i_325_n_0),
        .I5(pst_reg_cnt[85]),
        .O(q_reg_i_270_n_0));
  LUT6 #(
    .INIT(64'h0404040404040405)) 
    q_reg_i_271
       (.I0(pst_reg_cnt[103]),
        .I1(q_reg_i_247_n_0),
        .I2(q_reg_i_312_n_0),
        .I3(pst_reg_cnt[97]),
        .I4(pst_reg_cnt[98]),
        .I5(pst_reg_cnt[99]),
        .O(q_reg_i_271_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    q_reg_i_272
       (.I0(q_reg_i_99_n_0),
        .I1(q_reg_i_296_n_0),
        .O(q_reg_i_272_n_0));
  LUT6 #(
    .INIT(64'hFF55FF55FF55FFFD)) 
    q_reg_i_273
       (.I0(q_reg_i_506_n_0),
        .I1(q_reg_i_483_n_0),
        .I2(pst_reg_cnt[16]),
        .I3(q_reg_i_396_n_0),
        .I4(pst_reg_cnt[19]),
        .I5(q_reg_i_393_n_0),
        .O(q_reg_i_273_n_0));
  LUT6 #(
    .INIT(64'h888A8888888A888A)) 
    q_reg_i_274
       (.I0(q_reg_i_209_n_0),
        .I1(q_reg_i_148_n_0),
        .I2(q_reg_i_214_n_0),
        .I3(pst_reg_cnt[191]),
        .I4(pst_reg_cnt[190]),
        .I5(pst_reg_cnt[189]),
        .O(q_reg_i_274_n_0));
  LUT6 #(
    .INIT(64'h00000000000D0000)) 
    q_reg_i_275
       (.I0(q_reg_i_486_n_0),
        .I1(q_reg_i_507_n_0),
        .I2(pst_reg_cnt[229]),
        .I3(q_reg_i_508_n_0),
        .I4(q_reg_i_463_n_0),
        .I5(q_reg_i_462_n_0),
        .O(q_reg_i_275_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    q_reg_i_276
       (.I0(q_reg_i_237_n_0),
        .I1(q_reg_i_509_n_0),
        .O(q_reg_i_276_n_0));
  LUT6 #(
    .INIT(64'h000000000000BABB)) 
    q_reg_i_277
       (.I0(q_reg_i_432_n_0),
        .I1(pst_reg_cnt[91]),
        .I2(pst_reg_cnt[90]),
        .I3(q_reg_i_510_n_0),
        .I4(q_reg_i_511_n_0),
        .I5(q_reg_i_325_n_0),
        .O(q_reg_i_277_n_0));
  LUT6 #(
    .INIT(64'h00AE000000000000)) 
    q_reg_i_278
       (.I0(q_reg_i_208_n_0),
        .I1(q_reg_i_512_n_0),
        .I2(q_reg_i_513_n_0),
        .I3(q_reg_i_514_n_0),
        .I4(q_reg_i_515_n_0),
        .I5(q_reg_i_516_n_0),
        .O(q_reg_i_278_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_279
       (.I0(pst_reg_cnt[317]),
        .I1(pst_reg_cnt[316]),
        .I2(pst_reg_cnt[318]),
        .I3(pst_reg_cnt[319]),
        .I4(pst_reg_cnt[314]),
        .I5(pst_reg_cnt[315]),
        .O(q_reg_i_279_n_0));
  LUT6 #(
    .INIT(64'h40F040F040F04040)) 
    q_reg_i_280
       (.I0(q_reg_i_240_n_0),
        .I1(q_reg_i_455_n_0),
        .I2(q_reg_i_279_n_0),
        .I3(q_reg_i_456_n_0),
        .I4(q_reg_i_517_n_0),
        .I5(q_reg_i_243_n_0),
        .O(q_reg_i_280_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    q_reg_i_281
       (.I0(pst_reg_cnt[31]),
        .I1(q_reg_i_289_n_0),
        .I2(pst_reg_cnt[30]),
        .I3(pst_reg_cnt[29]),
        .I4(pst_reg_cnt[28]),
        .I5(pst_reg_cnt[27]),
        .O(q_reg_i_281_n_0));
  LUT5 #(
    .INIT(32'hFFFF01FF)) 
    q_reg_i_282
       (.I0(q_reg_i_481_n_0),
        .I1(q_reg_i_518_n_0),
        .I2(pst_reg_cnt[253]),
        .I3(q_reg_i_519_n_0),
        .I4(q_reg_i_104_n_0),
        .O(q_reg_i_282_n_0));
  LUT6 #(
    .INIT(64'hDDDDDDDDDDD0DDDD)) 
    q_reg_i_283
       (.I0(q_reg_i_520_n_0),
        .I1(q_reg_i_238_n_0),
        .I2(pst_reg_cnt[290]),
        .I3(pst_reg_cnt[291]),
        .I4(q_reg_i_521_n_0),
        .I5(pst_reg_cnt[289]),
        .O(q_reg_i_283_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_284
       (.I0(pst_reg_cnt[45]),
        .I1(pst_reg_cnt[44]),
        .I2(pst_reg_cnt[47]),
        .I3(pst_reg_cnt[46]),
        .I4(q_reg_i_496_n_0),
        .I5(q_reg_i_236_n_0),
        .O(q_reg_i_284_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_285
       (.I0(pst_reg_cnt[46]),
        .I1(pst_reg_cnt[47]),
        .I2(q_reg_i_496_n_0),
        .I3(q_reg_i_236_n_0),
        .O(q_reg_i_285_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_286
       (.I0(pst_reg_cnt[51]),
        .I1(q_reg_i_236_n_0),
        .I2(q_reg_i_257_n_0),
        .I3(q_reg_i_409_n_0),
        .O(q_reg_i_286_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_287
       (.I0(pst_reg_cnt[50]),
        .I1(pst_reg_cnt[49]),
        .O(q_reg_i_287_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_288
       (.I0(pst_reg_cnt[29]),
        .I1(pst_reg_cnt[30]),
        .I2(pst_reg_cnt[28]),
        .I3(pst_reg_cnt[31]),
        .O(q_reg_i_288_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_289
       (.I0(q_reg_i_458_n_0),
        .I1(q_reg_i_487_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_430_n_0),
        .I4(q_reg_i_422_n_0),
        .I5(q_reg_i_522_n_0),
        .O(q_reg_i_289_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_290
       (.I0(q_reg_i_375_n_0),
        .I1(q_reg_i_396_n_0),
        .I2(pst_reg_cnt[16]),
        .I3(q_reg_i_483_n_0),
        .O(q_reg_i_290_n_0));
  LUT6 #(
    .INIT(64'h000000000000000E)) 
    q_reg_i_291
       (.I0(pst_reg_cnt[12]),
        .I1(q_reg_i_523_n_0),
        .I2(q_reg_i_377_n_0),
        .I3(q_reg_i_396_n_0),
        .I4(q_reg_i_503_n_0),
        .I5(pst_reg_cnt[13]),
        .O(q_reg_i_291_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF4)) 
    q_reg_i_292
       (.I0(pst_reg_cnt[1]),
        .I1(q_reg_i_391_n_0),
        .I2(q_reg_i_392_n_0),
        .I3(q_reg_i_375_n_0),
        .I4(q_reg_i_396_n_0),
        .I5(q_reg_i_503_n_0),
        .O(q_reg_i_292_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    q_reg_i_293
       (.I0(pst_reg_cnt[318]),
        .I1(pst_reg_cnt[319]),
        .O(q_reg_i_293_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAA8AAAAAAAA)) 
    q_reg_i_294
       (.I0(q_reg_i_358_n_0),
        .I1(pst_reg_cnt[290]),
        .I2(pst_reg_cnt[291]),
        .I3(q_reg_i_304_n_0),
        .I4(q_reg_i_524_n_0),
        .I5(pst_reg_cnt[289]),
        .O(q_reg_i_294_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_295
       (.I0(pst_reg_cnt[308]),
        .I1(pst_reg_cnt[309]),
        .I2(pst_reg_cnt[311]),
        .I3(pst_reg_cnt[310]),
        .I4(q_reg_i_361_n_0),
        .O(q_reg_i_295_n_0));
  LUT4 #(
    .INIT(16'hFFF2)) 
    q_reg_i_296
       (.I0(pst_reg_cnt[297]),
        .I1(pst_reg_cnt[298]),
        .I2(pst_reg_cnt[299]),
        .I3(q_reg_i_480_n_0),
        .O(q_reg_i_296_n_0));
  LUT3 #(
    .INIT(8'hAB)) 
    q_reg_i_297
       (.I0(q_reg_i_525_n_0),
        .I1(q_reg_i_355_n_0),
        .I2(q_reg_i_99_n_0),
        .O(q_reg_i_297_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    q_reg_i_298
       (.I0(q_reg_i_449_n_0),
        .I1(q_reg_i_448_n_0),
        .I2(pst_reg_cnt[227]),
        .I3(pst_reg_cnt[226]),
        .I4(pst_reg_cnt[225]),
        .O(q_reg_i_298_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_299
       (.I0(q_reg_i_310_n_0),
        .I1(q_reg_i_220_n_0),
        .I2(q_reg_i_440_n_0),
        .I3(q_reg_i_526_n_0),
        .O(q_reg_i_299_n_0));
  LUT6 #(
    .INIT(64'h00000000000000F2)) 
    q_reg_i_300
       (.I0(q_reg_i_527_n_0),
        .I1(pst_reg_cnt[108]),
        .I2(pst_reg_cnt[109]),
        .I3(pst_reg_cnt[110]),
        .I4(pst_reg_cnt[111]),
        .I5(q_reg_i_249_n_0),
        .O(q_reg_i_300_n_0));
  LUT6 #(
    .INIT(64'h00000000000000A2)) 
    q_reg_i_301
       (.I0(q_reg_i_528_n_0),
        .I1(pst_reg_cnt[129]),
        .I2(pst_reg_cnt[130]),
        .I3(pst_reg_cnt[131]),
        .I4(q_reg_i_261_n_0),
        .I5(q_reg_i_529_n_0),
        .O(q_reg_i_301_n_0));
  LUT6 #(
    .INIT(64'h0000000000010000)) 
    q_reg_i_302
       (.I0(q_reg_i_503_n_0),
        .I1(q_reg_i_396_n_0),
        .I2(q_reg_i_375_n_0),
        .I3(q_reg_i_392_n_0),
        .I4(q_reg_i_391_n_0),
        .I5(q_reg_i_390_n_0),
        .O(q_reg_i_302_n_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    q_reg_i_303
       (.I0(q_reg_i_396_n_0),
        .I1(q_reg_i_393_n_0),
        .I2(pst_reg_cnt[19]),
        .I3(pst_reg_cnt[18]),
        .O(q_reg_i_303_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_304
       (.I0(q_reg_i_346_n_0),
        .I1(pst_reg_cnt[298]),
        .I2(pst_reg_cnt[299]),
        .I3(pst_reg_cnt[296]),
        .I4(pst_reg_cnt[297]),
        .I5(q_reg_i_530_n_0),
        .O(q_reg_i_304_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    q_reg_i_305
       (.I0(pst_reg_cnt[303]),
        .I1(pst_reg_cnt[302]),
        .I2(q_reg_i_346_n_0),
        .I3(pst_reg_cnt[301]),
        .I4(pst_reg_cnt[300]),
        .O(q_reg_i_305_n_0));
  LUT3 #(
    .INIT(8'h45)) 
    q_reg_i_306
       (.I0(q_reg_i_152_n_0),
        .I1(pst_reg_cnt[223]),
        .I2(pst_reg_cnt[222]),
        .O(q_reg_i_306_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    q_reg_i_307
       (.I0(q_reg_i_448_n_0),
        .I1(pst_reg_cnt[231]),
        .I2(pst_reg_cnt[230]),
        .I3(pst_reg_cnt[229]),
        .I4(pst_reg_cnt[228]),
        .O(q_reg_i_307_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    q_reg_i_308
       (.I0(q_reg_i_236_n_0),
        .I1(q_reg_i_496_n_0),
        .I2(pst_reg_cnt[47]),
        .I3(pst_reg_cnt[46]),
        .I4(pst_reg_cnt[45]),
        .O(q_reg_i_308_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    q_reg_i_309
       (.I0(q_reg_i_531_n_0),
        .I1(q_reg_i_234_n_0),
        .I2(q_reg_i_233_n_0),
        .I3(pst_reg_cnt[65]),
        .I4(q_reg_i_236_n_0),
        .I5(pst_reg_cnt[63]),
        .O(q_reg_i_309_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_310
       (.I0(pst_reg_cnt[121]),
        .I1(pst_reg_cnt[120]),
        .I2(pst_reg_cnt[123]),
        .I3(pst_reg_cnt[122]),
        .I4(q_reg_i_383_n_0),
        .O(q_reg_i_310_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_311
       (.I0(q_reg_i_490_n_0),
        .I1(q_reg_i_458_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_476_n_0),
        .I4(q_reg_i_250_n_0),
        .I5(q_reg_i_532_n_0),
        .O(q_reg_i_311_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_312
       (.I0(q_reg_i_246_n_0),
        .I1(q_reg_i_422_n_0),
        .I2(q_reg_i_430_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_458_n_0),
        .I5(q_reg_i_490_n_0),
        .O(q_reg_i_312_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_313
       (.I0(q_reg_i_344_n_0),
        .I1(q_reg_i_345_n_0),
        .I2(q_reg_i_346_n_0),
        .I3(q_reg_i_347_n_0),
        .I4(q_reg_i_348_n_0),
        .I5(q_reg_i_500_n_0),
        .O(q_reg_i_313_n_0));
  LUT6 #(
    .INIT(64'hAAA2AAAA00000000)) 
    q_reg_i_314
       (.I0(q_reg_i_209_n_0),
        .I1(q_reg_i_481_n_0),
        .I2(pst_reg_cnt[243]),
        .I3(pst_reg_cnt[242]),
        .I4(pst_reg_cnt[241]),
        .I5(q_reg_i_145_n_0),
        .O(q_reg_i_314_n_0));
  LUT5 #(
    .INIT(32'h0000DF55)) 
    q_reg_i_315
       (.I0(q_reg_i_525_n_0),
        .I1(pst_reg_cnt[259]),
        .I2(pst_reg_cnt[258]),
        .I3(q_reg_i_515_n_0),
        .I4(q_reg_i_533_n_0),
        .O(q_reg_i_315_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_316
       (.I0(pst_reg_cnt[134]),
        .I1(pst_reg_cnt[135]),
        .I2(q_reg_i_261_n_0),
        .O(q_reg_i_316_n_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    q_reg_i_317
       (.I0(q_reg_i_185_n_0),
        .I1(q_reg_i_184_n_0),
        .I2(pst_reg_cnt[139]),
        .I3(pst_reg_cnt[138]),
        .O(q_reg_i_317_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_318
       (.I0(q_reg_i_422_n_0),
        .I1(q_reg_i_421_n_0),
        .I2(q_reg_i_420_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_418_n_0),
        .O(q_reg_i_318_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF45)) 
    q_reg_i_319
       (.I0(q_reg_i_431_n_0),
        .I1(pst_reg_cnt[151]),
        .I2(pst_reg_cnt[150]),
        .I3(q_reg_i_534_n_0),
        .I4(q_reg_i_446_n_0),
        .I5(q_reg_i_318_n_0),
        .O(q_reg_i_319_n_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    q_reg_i_320
       (.I0(q_reg_i_415_n_0),
        .I1(q_reg_i_325_n_0),
        .I2(pst_reg_cnt[79]),
        .I3(pst_reg_cnt[78]),
        .O(q_reg_i_320_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_321
       (.I0(pst_reg_cnt[86]),
        .I1(pst_reg_cnt[87]),
        .I2(q_reg_i_433_n_0),
        .I3(q_reg_i_325_n_0),
        .O(q_reg_i_321_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    q_reg_i_322
       (.I0(q_reg_i_312_n_0),
        .I1(pst_reg_cnt[103]),
        .I2(pst_reg_cnt[102]),
        .O(q_reg_i_322_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_323
       (.I0(pst_reg_cnt[101]),
        .I1(pst_reg_cnt[100]),
        .I2(pst_reg_cnt[103]),
        .I3(pst_reg_cnt[102]),
        .I4(q_reg_i_312_n_0),
        .O(q_reg_i_323_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    q_reg_i_324
       (.I0(pst_reg_cnt[98]),
        .I1(pst_reg_cnt[99]),
        .I2(pst_reg_cnt[97]),
        .O(q_reg_i_324_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_325
       (.I0(q_reg_i_475_n_0),
        .I1(q_reg_i_458_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_420_n_0),
        .I4(q_reg_i_421_n_0),
        .I5(q_reg_i_422_n_0),
        .O(q_reg_i_325_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_326
       (.I0(pst_reg_cnt[186]),
        .I1(pst_reg_cnt[187]),
        .O(q_reg_i_326_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_327
       (.I0(q_reg_i_493_n_0),
        .I1(q_reg_i_152_n_0),
        .I2(q_reg_i_420_n_0),
        .O(q_reg_i_327_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_328
       (.I0(pst_reg_cnt[196]),
        .I1(pst_reg_cnt[197]),
        .I2(pst_reg_cnt[198]),
        .I3(pst_reg_cnt[199]),
        .O(q_reg_i_328_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_329
       (.I0(pst_reg_cnt[212]),
        .I1(pst_reg_cnt[213]),
        .I2(pst_reg_cnt[210]),
        .I3(pst_reg_cnt[211]),
        .O(q_reg_i_329_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_330
       (.I0(pst_reg_cnt[214]),
        .I1(pst_reg_cnt[215]),
        .I2(q_reg_i_152_n_0),
        .I3(q_reg_i_149_n_0),
        .O(q_reg_i_330_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_331
       (.I0(q_reg_i_420_n_0),
        .I1(q_reg_i_152_n_0),
        .O(q_reg_i_331_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    q_reg_i_332
       (.I0(pst_reg_cnt[197]),
        .I1(pst_reg_cnt[196]),
        .I2(pst_reg_cnt[195]),
        .O(q_reg_i_332_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFDFFFFFFFC)) 
    q_reg_i_333
       (.I0(pst_reg_cnt[202]),
        .I1(pst_reg_cnt[203]),
        .I2(q_reg_i_420_n_0),
        .I3(q_reg_i_152_n_0),
        .I4(q_reg_i_535_n_0),
        .I5(pst_reg_cnt[201]),
        .O(q_reg_i_333_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_334
       (.I0(q_reg_i_422_n_0),
        .I1(q_reg_i_430_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_536_n_0),
        .I4(q_reg_i_437_n_0),
        .I5(q_reg_i_537_n_0),
        .O(q_reg_i_334_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_335
       (.I0(pst_reg_cnt[170]),
        .I1(pst_reg_cnt[169]),
        .I2(pst_reg_cnt[171]),
        .I3(pst_reg_cnt[168]),
        .I4(q_reg_i_538_n_0),
        .O(q_reg_i_335_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    q_reg_i_336
       (.I0(pst_reg_cnt[170]),
        .I1(pst_reg_cnt[169]),
        .O(q_reg_i_336_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_337
       (.I0(pst_reg_cnt[171]),
        .I1(q_reg_i_437_n_0),
        .I2(q_reg_i_536_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_476_n_0),
        .I5(pst_reg_cnt[172]),
        .O(q_reg_i_337_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_338
       (.I0(pst_reg_cnt[174]),
        .I1(pst_reg_cnt[175]),
        .O(q_reg_i_338_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_339
       (.I0(q_reg_i_422_n_0),
        .I1(q_reg_i_421_n_0),
        .I2(q_reg_i_420_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_536_n_0),
        .O(q_reg_i_339_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_340
       (.I0(pst_reg_cnt[188]),
        .I1(pst_reg_cnt[189]),
        .I2(pst_reg_cnt[190]),
        .I3(pst_reg_cnt[191]),
        .O(q_reg_i_340_n_0));
  LUT4 #(
    .INIT(16'h000B)) 
    q_reg_i_341
       (.I0(pst_reg_cnt[178]),
        .I1(pst_reg_cnt[177]),
        .I2(pst_reg_cnt[179]),
        .I3(pst_reg_cnt[180]),
        .O(q_reg_i_341_n_0));
  LUT6 #(
    .INIT(64'hCFCDCFCDCFCCCFCD)) 
    q_reg_i_342
       (.I0(pst_reg_cnt[161]),
        .I1(q_reg_i_488_n_0),
        .I2(pst_reg_cnt[163]),
        .I3(pst_reg_cnt[162]),
        .I4(pst_reg_cnt[159]),
        .I5(q_reg_i_318_n_0),
        .O(q_reg_i_342_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_343
       (.I0(q_reg_i_335_n_0),
        .I1(q_reg_i_536_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_420_n_0),
        .I4(q_reg_i_421_n_0),
        .I5(q_reg_i_422_n_0),
        .O(q_reg_i_343_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_344
       (.I0(pst_reg_cnt[272]),
        .I1(pst_reg_cnt[273]),
        .I2(q_reg_i_539_n_0),
        .I3(q_reg_i_540_n_0),
        .I4(q_reg_i_541_n_0),
        .I5(q_reg_i_542_n_0),
        .O(q_reg_i_344_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_345
       (.I0(q_reg_i_492_n_0),
        .I1(q_reg_i_543_n_0),
        .I2(pst_reg_cnt[264]),
        .I3(pst_reg_cnt[265]),
        .I4(q_reg_i_544_n_0),
        .I5(q_reg_i_545_n_0),
        .O(q_reg_i_345_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    q_reg_i_346
       (.I0(q_reg_i_546_n_0),
        .I1(q_reg_i_547_n_0),
        .I2(pst_reg_cnt[312]),
        .I3(pst_reg_cnt[313]),
        .I4(q_reg_i_548_n_0),
        .I5(q_reg_i_549_n_0),
        .O(q_reg_i_346_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_347
       (.I0(q_reg_i_524_n_0),
        .I1(pst_reg_cnt[291]),
        .I2(pst_reg_cnt[290]),
        .I3(pst_reg_cnt[289]),
        .I4(pst_reg_cnt[288]),
        .O(q_reg_i_347_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_348
       (.I0(q_reg_i_530_n_0),
        .I1(pst_reg_cnt[297]),
        .I2(pst_reg_cnt[296]),
        .I3(pst_reg_cnt[299]),
        .I4(pst_reg_cnt[298]),
        .O(q_reg_i_348_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_349
       (.I0(q_reg_i_550_n_0),
        .I1(q_reg_i_551_n_0),
        .I2(q_reg_i_499_n_0),
        .I3(q_reg_i_498_n_0),
        .I4(pst_reg_cnt[240]),
        .I5(pst_reg_cnt[241]),
        .O(q_reg_i_349_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_350
       (.I0(pst_reg_cnt[222]),
        .I1(pst_reg_cnt[223]),
        .I2(pst_reg_cnt[220]),
        .I3(pst_reg_cnt[221]),
        .O(q_reg_i_350_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_351
       (.I0(q_reg_i_552_n_0),
        .I1(pst_reg_cnt[296]),
        .I2(pst_reg_cnt[297]),
        .I3(q_reg_i_530_n_0),
        .I4(q_reg_i_553_n_0),
        .I5(q_reg_i_524_n_0),
        .O(q_reg_i_351_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_352
       (.I0(q_reg_i_554_n_0),
        .I1(q_reg_i_555_n_0),
        .I2(pst_reg_cnt[224]),
        .I3(pst_reg_cnt[225]),
        .I4(q_reg_i_484_n_0),
        .I5(q_reg_i_449_n_0),
        .O(q_reg_i_352_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF0000FF0D)) 
    q_reg_i_353
       (.I0(pst_reg_cnt[267]),
        .I1(pst_reg_cnt[268]),
        .I2(pst_reg_cnt[269]),
        .I3(pst_reg_cnt[270]),
        .I4(pst_reg_cnt[271]),
        .I5(q_reg_i_232_n_0),
        .O(q_reg_i_353_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_354
       (.I0(pst_reg_cnt[277]),
        .I1(pst_reg_cnt[276]),
        .I2(pst_reg_cnt[279]),
        .I3(pst_reg_cnt[278]),
        .I4(q_reg_i_251_n_0),
        .O(q_reg_i_354_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    q_reg_i_355
       (.I0(pst_reg_cnt[282]),
        .I1(pst_reg_cnt[283]),
        .I2(q_reg_i_479_n_0),
        .I3(pst_reg_cnt[281]),
        .I4(pst_reg_cnt[280]),
        .I5(pst_reg_cnt[279]),
        .O(q_reg_i_355_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    q_reg_i_356
       (.I0(pst_reg_cnt[279]),
        .I1(q_reg_i_251_n_0),
        .O(q_reg_i_356_n_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    q_reg_i_357
       (.I0(q_reg_i_542_n_0),
        .I1(q_reg_i_358_n_0),
        .I2(pst_reg_cnt[283]),
        .I3(pst_reg_cnt[282]),
        .O(q_reg_i_357_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_358
       (.I0(q_reg_i_346_n_0),
        .I1(q_reg_i_524_n_0),
        .I2(q_reg_i_556_n_0),
        .I3(pst_reg_cnt[289]),
        .I4(pst_reg_cnt[288]),
        .I5(q_reg_i_348_n_0),
        .O(q_reg_i_358_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    q_reg_i_359
       (.I0(pst_reg_cnt[293]),
        .I1(pst_reg_cnt[294]),
        .I2(pst_reg_cnt[295]),
        .I3(q_reg_i_304_n_0),
        .I4(pst_reg_cnt[292]),
        .I5(pst_reg_cnt[291]),
        .O(q_reg_i_359_n_0));
  LUT6 #(
    .INIT(64'h0000000000000045)) 
    q_reg_i_360
       (.I0(pst_reg_cnt[317]),
        .I1(pst_reg_cnt[316]),
        .I2(pst_reg_cnt[315]),
        .I3(pst_reg_cnt[318]),
        .I4(pst_reg_cnt[319]),
        .I5(q_reg_i_557_n_0),
        .O(q_reg_i_360_n_0));
  LUT5 #(
    .INIT(32'hFFFEFFFF)) 
    q_reg_i_361
       (.I0(pst_reg_cnt[313]),
        .I1(pst_reg_cnt[312]),
        .I2(pst_reg_cnt[315]),
        .I3(pst_reg_cnt[314]),
        .I4(q_reg_i_546_n_0),
        .O(q_reg_i_361_n_0));
  LUT6 #(
    .INIT(64'hFFFF0000FFFFFF0D)) 
    q_reg_i_362
       (.I0(pst_reg_cnt[303]),
        .I1(q_reg_i_346_n_0),
        .I2(pst_reg_cnt[305]),
        .I3(pst_reg_cnt[306]),
        .I4(q_reg_i_295_n_0),
        .I5(pst_reg_cnt[307]),
        .O(q_reg_i_362_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_363
       (.I0(pst_reg_cnt[133]),
        .I1(pst_reg_cnt[132]),
        .I2(pst_reg_cnt[135]),
        .I3(pst_reg_cnt[134]),
        .I4(q_reg_i_261_n_0),
        .O(q_reg_i_363_n_0));
  LUT5 #(
    .INIT(32'h05050504)) 
    q_reg_i_364
       (.I0(q_reg_i_220_n_0),
        .I1(pst_reg_cnt[126]),
        .I2(pst_reg_cnt[127]),
        .I3(pst_reg_cnt[124]),
        .I4(pst_reg_cnt[125]),
        .O(q_reg_i_364_n_0));
  LUT6 #(
    .INIT(64'h00FF00FF00FF0001)) 
    q_reg_i_365
       (.I0(pst_reg_cnt[108]),
        .I1(pst_reg_cnt[107]),
        .I2(pst_reg_cnt[106]),
        .I3(q_reg_i_249_n_0),
        .I4(pst_reg_cnt[109]),
        .I5(q_reg_i_250_n_0),
        .O(q_reg_i_365_n_0));
  LUT6 #(
    .INIT(64'h0000111000001111)) 
    q_reg_i_366
       (.I0(q_reg_i_494_n_0),
        .I1(q_reg_i_558_n_0),
        .I2(pst_reg_cnt[55]),
        .I3(q_reg_i_257_n_0),
        .I4(q_reg_i_236_n_0),
        .I5(q_reg_i_409_n_0),
        .O(q_reg_i_366_n_0));
  LUT6 #(
    .INIT(64'hFFFFFF00FFFFFF0E)) 
    q_reg_i_367
       (.I0(q_reg_i_559_n_0),
        .I1(pst_reg_cnt[72]),
        .I2(pst_reg_cnt[73]),
        .I3(q_reg_i_414_n_0),
        .I4(q_reg_i_413_n_0),
        .I5(q_reg_i_412_n_0),
        .O(q_reg_i_367_n_0));
  LUT4 #(
    .INIT(16'hF0E0)) 
    q_reg_i_368
       (.I0(pst_reg_cnt[290]),
        .I1(pst_reg_cnt[291]),
        .I2(q_reg_i_521_n_0),
        .I3(pst_reg_cnt[289]),
        .O(q_reg_i_368_n_0));
  LUT6 #(
    .INIT(64'h0000000F0000000E)) 
    q_reg_i_369
       (.I0(pst_reg_cnt[271]),
        .I1(q_reg_i_232_n_0),
        .I2(q_reg_i_354_n_0),
        .I3(pst_reg_cnt[275]),
        .I4(pst_reg_cnt[274]),
        .I5(q_reg_i_560_n_0),
        .O(q_reg_i_369_n_0));
  LUT6 #(
    .INIT(64'h0011001000110011)) 
    q_reg_i_370
       (.I0(pst_reg_cnt[239]),
        .I1(pst_reg_cnt[238]),
        .I2(pst_reg_cnt[235]),
        .I3(q_reg_i_145_n_0),
        .I4(q_reg_i_555_n_0),
        .I5(q_reg_i_561_n_0),
        .O(q_reg_i_370_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_371
       (.I0(pst_reg_cnt[221]),
        .I1(pst_reg_cnt[220]),
        .I2(pst_reg_cnt[223]),
        .I3(pst_reg_cnt[222]),
        .I4(q_reg_i_152_n_0),
        .O(q_reg_i_371_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_372
       (.I0(q_reg_i_346_n_0),
        .I1(pst_reg_cnt[302]),
        .I2(pst_reg_cnt[303]),
        .O(q_reg_i_372_n_0));
  LUT6 #(
    .INIT(64'hAAFFAAFFAAFFFEFF)) 
    q_reg_i_373
       (.I0(q_reg_i_509_n_0),
        .I1(pst_reg_cnt[312]),
        .I2(pst_reg_cnt[313]),
        .I3(q_reg_i_546_n_0),
        .I4(pst_reg_cnt[314]),
        .I5(pst_reg_cnt[315]),
        .O(q_reg_i_373_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    q_reg_i_374
       (.I0(pst_reg_cnt[7]),
        .I1(pst_reg_cnt[6]),
        .O(q_reg_i_374_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_375
       (.I0(q_reg_i_482_n_0),
        .I1(pst_reg_cnt[9]),
        .I2(pst_reg_cnt[8]),
        .I3(pst_reg_cnt[11]),
        .I4(pst_reg_cnt[10]),
        .O(q_reg_i_375_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_376
       (.I0(pst_reg_cnt[12]),
        .I1(pst_reg_cnt[13]),
        .O(q_reg_i_376_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_377
       (.I0(pst_reg_cnt[15]),
        .I1(pst_reg_cnt[14]),
        .O(q_reg_i_377_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_378
       (.I0(q_reg_i_483_n_0),
        .I1(pst_reg_cnt[16]),
        .I2(pst_reg_cnt[24]),
        .I3(pst_reg_cnt[25]),
        .I4(q_reg_i_398_n_0),
        .I5(q_reg_i_289_n_0),
        .O(q_reg_i_378_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_379
       (.I0(q_reg_i_310_n_0),
        .I1(q_reg_i_458_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_476_n_0),
        .I4(pst_reg_cnt[119]),
        .I5(q_reg_i_562_n_0),
        .O(q_reg_i_379_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_380
       (.I0(q_reg_i_440_n_0),
        .I1(q_reg_i_422_n_0),
        .I2(q_reg_i_430_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_458_n_0),
        .I5(q_reg_i_310_n_0),
        .O(q_reg_i_380_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_381
       (.I0(pst_reg_cnt[114]),
        .I1(pst_reg_cnt[115]),
        .O(q_reg_i_381_n_0));
  LUT4 #(
    .INIT(16'hFCFE)) 
    q_reg_i_382
       (.I0(pst_reg_cnt[120]),
        .I1(pst_reg_cnt[123]),
        .I2(pst_reg_cnt[122]),
        .I3(pst_reg_cnt[121]),
        .O(q_reg_i_382_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_383
       (.I0(pst_reg_cnt[126]),
        .I1(pst_reg_cnt[127]),
        .I2(pst_reg_cnt[124]),
        .I3(pst_reg_cnt[125]),
        .O(q_reg_i_383_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_384
       (.I0(q_reg_i_477_n_0),
        .I1(q_reg_i_413_n_0),
        .I2(pst_reg_cnt[69]),
        .I3(pst_reg_cnt[68]),
        .I4(pst_reg_cnt[71]),
        .I5(pst_reg_cnt[70]),
        .O(q_reg_i_384_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_385
       (.I0(pst_reg_cnt[66]),
        .I1(pst_reg_cnt[67]),
        .O(q_reg_i_385_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_386
       (.I0(pst_reg_cnt[60]),
        .I1(pst_reg_cnt[61]),
        .O(q_reg_i_386_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_387
       (.I0(q_reg_i_236_n_0),
        .I1(pst_reg_cnt[62]),
        .I2(pst_reg_cnt[63]),
        .O(q_reg_i_387_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    q_reg_i_388
       (.I0(pst_reg_cnt[7]),
        .I1(pst_reg_cnt[6]),
        .I2(pst_reg_cnt[5]),
        .I3(pst_reg_cnt[4]),
        .I4(pst_reg_cnt[3]),
        .O(q_reg_i_388_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_389
       (.I0(pst_reg_cnt[318]),
        .I1(pst_reg_cnt[319]),
        .O(q_reg_i_389_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_390
       (.I0(pst_reg_cnt[0]),
        .I1(pst_reg_cnt[1]),
        .O(q_reg_i_390_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    q_reg_i_391
       (.I0(pst_reg_cnt[3]),
        .I1(pst_reg_cnt[2]),
        .O(q_reg_i_391_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_392
       (.I0(pst_reg_cnt[4]),
        .I1(pst_reg_cnt[7]),
        .I2(pst_reg_cnt[6]),
        .I3(pst_reg_cnt[5]),
        .O(q_reg_i_392_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_393
       (.I0(pst_reg_cnt[22]),
        .I1(pst_reg_cnt[23]),
        .I2(pst_reg_cnt[20]),
        .I3(pst_reg_cnt[21]),
        .O(q_reg_i_393_n_0));
  LUT6 #(
    .INIT(64'h0001000000010001)) 
    q_reg_i_394
       (.I0(pst_reg_cnt[18]),
        .I1(pst_reg_cnt[17]),
        .I2(pst_reg_cnt[19]),
        .I3(q_reg_i_393_n_0),
        .I4(pst_reg_cnt[16]),
        .I5(pst_reg_cnt[15]),
        .O(q_reg_i_394_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    q_reg_i_395
       (.I0(pst_reg_cnt[23]),
        .I1(pst_reg_cnt[22]),
        .I2(pst_reg_cnt[21]),
        .O(q_reg_i_395_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_396
       (.I0(q_reg_i_522_n_0),
        .I1(q_reg_i_476_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_487_n_0),
        .I4(q_reg_i_458_n_0),
        .I5(q_reg_i_563_n_0),
        .O(q_reg_i_396_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_397
       (.I0(pst_reg_cnt[24]),
        .I1(pst_reg_cnt[25]),
        .O(q_reg_i_397_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_398
       (.I0(pst_reg_cnt[31]),
        .I1(pst_reg_cnt[28]),
        .I2(pst_reg_cnt[30]),
        .I3(pst_reg_cnt[29]),
        .I4(pst_reg_cnt[26]),
        .I5(pst_reg_cnt[27]),
        .O(q_reg_i_398_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_399
       (.I0(pst_reg_cnt[27]),
        .I1(pst_reg_cnt[28]),
        .O(q_reg_i_399_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_400
       (.I0(pst_reg_cnt[30]),
        .I1(pst_reg_cnt[29]),
        .O(q_reg_i_400_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_401
       (.I0(pst_reg_cnt[33]),
        .I1(pst_reg_cnt[34]),
        .O(q_reg_i_401_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_402
       (.I0(pst_reg_cnt[39]),
        .I1(pst_reg_cnt[38]),
        .O(q_reg_i_402_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_403
       (.I0(q_reg_i_434_n_0),
        .I1(q_reg_i_496_n_0),
        .I2(q_reg_i_476_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_487_n_0),
        .I5(q_reg_i_458_n_0),
        .O(q_reg_i_403_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_404
       (.I0(pst_reg_cnt[43]),
        .I1(pst_reg_cnt[42]),
        .O(q_reg_i_404_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_405
       (.I0(pst_reg_cnt[46]),
        .I1(pst_reg_cnt[47]),
        .I2(pst_reg_cnt[44]),
        .I3(pst_reg_cnt[45]),
        .O(q_reg_i_405_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_406
       (.I0(q_reg_i_458_n_0),
        .I1(q_reg_i_487_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_430_n_0),
        .I4(q_reg_i_422_n_0),
        .I5(q_reg_i_496_n_0),
        .O(q_reg_i_406_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    q_reg_i_407
       (.I0(pst_reg_cnt[50]),
        .I1(pst_reg_cnt[49]),
        .O(q_reg_i_407_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_408
       (.I0(pst_reg_cnt[49]),
        .I1(pst_reg_cnt[50]),
        .I2(pst_reg_cnt[48]),
        .I3(pst_reg_cnt[51]),
        .O(q_reg_i_408_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_409
       (.I0(pst_reg_cnt[53]),
        .I1(pst_reg_cnt[54]),
        .I2(pst_reg_cnt[52]),
        .I3(pst_reg_cnt[55]),
        .O(q_reg_i_409_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_410
       (.I0(pst_reg_cnt[42]),
        .I1(pst_reg_cnt[43]),
        .O(q_reg_i_410_n_0));
  LUT3 #(
    .INIT(8'h45)) 
    q_reg_i_411
       (.I0(pst_reg_cnt[71]),
        .I1(pst_reg_cnt[70]),
        .I2(pst_reg_cnt[69]),
        .O(q_reg_i_411_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_412
       (.I0(pst_reg_cnt[75]),
        .I1(pst_reg_cnt[74]),
        .O(q_reg_i_412_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_413
       (.I0(q_reg_i_422_n_0),
        .I1(q_reg_i_430_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_458_n_0),
        .I4(q_reg_i_475_n_0),
        .I5(q_reg_i_415_n_0),
        .O(q_reg_i_413_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_414
       (.I0(pst_reg_cnt[77]),
        .I1(pst_reg_cnt[78]),
        .I2(pst_reg_cnt[76]),
        .I3(pst_reg_cnt[79]),
        .O(q_reg_i_414_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_415
       (.I0(q_reg_i_564_n_0),
        .I1(q_reg_i_432_n_0),
        .I2(pst_reg_cnt[80]),
        .I3(pst_reg_cnt[81]),
        .I4(q_reg_i_504_n_0),
        .I5(q_reg_i_450_n_0),
        .O(q_reg_i_415_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    q_reg_i_416
       (.I0(pst_reg_cnt[53]),
        .I1(pst_reg_cnt[54]),
        .I2(pst_reg_cnt[52]),
        .O(q_reg_i_416_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_417
       (.I0(q_reg_i_446_n_0),
        .I1(pst_reg_cnt[152]),
        .I2(pst_reg_cnt[153]),
        .I3(q_reg_i_470_n_0),
        .I4(q_reg_i_565_n_0),
        .I5(q_reg_i_429_n_0),
        .O(q_reg_i_417_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_418
       (.I0(q_reg_i_447_n_0),
        .I1(pst_reg_cnt[161]),
        .I2(pst_reg_cnt[160]),
        .I3(q_reg_i_566_n_0),
        .I4(q_reg_i_335_n_0),
        .I5(q_reg_i_536_n_0),
        .O(q_reg_i_418_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_419
       (.I0(q_reg_i_348_n_0),
        .I1(q_reg_i_347_n_0),
        .I2(q_reg_i_346_n_0),
        .I3(q_reg_i_252_n_0),
        .I4(q_reg_i_567_n_0),
        .I5(q_reg_i_344_n_0),
        .O(q_reg_i_419_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_420
       (.I0(q_reg_i_568_n_0),
        .I1(pst_reg_cnt[216]),
        .I2(pst_reg_cnt[217]),
        .I3(q_reg_i_350_n_0),
        .I4(q_reg_i_569_n_0),
        .I5(q_reg_i_329_n_0),
        .O(q_reg_i_420_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_421
       (.I0(q_reg_i_328_n_0),
        .I1(pst_reg_cnt[194]),
        .I2(pst_reg_cnt[195]),
        .I3(pst_reg_cnt[192]),
        .I4(pst_reg_cnt[193]),
        .I5(q_reg_i_493_n_0),
        .O(q_reg_i_421_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_422
       (.I0(q_reg_i_449_n_0),
        .I1(q_reg_i_484_n_0),
        .I2(pst_reg_cnt[225]),
        .I3(pst_reg_cnt[224]),
        .I4(q_reg_i_561_n_0),
        .I5(q_reg_i_349_n_0),
        .O(q_reg_i_422_n_0));
  LUT6 #(
    .INIT(64'h00000000D0D000D0)) 
    q_reg_i_423
       (.I0(q_reg_i_100_n_0),
        .I1(q_reg_i_83_n_0),
        .I2(q_reg_i_99_n_0),
        .I3(q_reg_i_240_n_0),
        .I4(q_reg_i_239_n_0),
        .I5(q_reg_i_570_n_0),
        .O(q_reg_i_423_n_0));
  LUT6 #(
    .INIT(64'hD0D000D000000000)) 
    q_reg_i_424
       (.I0(q_reg_i_100_n_0),
        .I1(q_reg_i_83_n_0),
        .I2(q_reg_i_99_n_0),
        .I3(q_reg_i_245_n_0),
        .I4(q_reg_i_571_n_0),
        .I5(q_reg_i_241_n_0),
        .O(q_reg_i_424_n_0));
  LUT6 #(
    .INIT(64'h2F2FD02FD0D0D0D0)) 
    q_reg_i_425
       (.I0(q_reg_i_100_n_0),
        .I1(q_reg_i_83_n_0),
        .I2(q_reg_i_99_n_0),
        .I3(q_reg_i_245_n_0),
        .I4(q_reg_i_571_n_0),
        .I5(q_reg_i_241_n_0),
        .O(q_reg_i_425_n_0));
  LUT6 #(
    .INIT(64'h2A2A2A2A2A2A2A00)) 
    q_reg_i_426
       (.I0(q_reg_i_237_n_0),
        .I1(q_reg_i_261_n_0),
        .I2(q_reg_i_195_n_0),
        .I3(pst_reg_cnt[55]),
        .I4(q_reg_i_257_n_0),
        .I5(q_reg_i_236_n_0),
        .O(q_reg_i_426_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_427
       (.I0(pst_reg_cnt[109]),
        .I1(q_reg_i_249_n_0),
        .I2(pst_reg_cnt[111]),
        .I3(pst_reg_cnt[110]),
        .O(q_reg_i_427_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_428
       (.I0(pst_reg_cnt[163]),
        .I1(q_reg_i_343_n_0),
        .I2(pst_reg_cnt[166]),
        .I3(pst_reg_cnt[167]),
        .I4(pst_reg_cnt[164]),
        .I5(pst_reg_cnt[165]),
        .O(q_reg_i_428_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_429
       (.I0(pst_reg_cnt[149]),
        .I1(pst_reg_cnt[150]),
        .I2(pst_reg_cnt[148]),
        .I3(pst_reg_cnt[151]),
        .O(q_reg_i_429_n_0));
  LUT6 #(
    .INIT(64'h65A6FFFF000065A6)) 
    q_reg_i_43
       (.I0(q_reg_i_79_n_0),
        .I1(\q_reg[271]_0 ),
        .I2(\q_reg[109]_0 ),
        .I3(q_reg_i_98_0),
        .I4(q_reg_i_80_n_0),
        .I5(q_reg_i_81_n_0),
        .O(DI[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_430
       (.I0(q_reg_i_420_n_0),
        .I1(q_reg_i_493_n_0),
        .I2(pst_reg_cnt[193]),
        .I3(pst_reg_cnt[192]),
        .I4(q_reg_i_572_n_0),
        .I5(q_reg_i_328_n_0),
        .O(q_reg_i_430_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_431
       (.I0(pst_reg_cnt[155]),
        .I1(pst_reg_cnt[154]),
        .I2(pst_reg_cnt[153]),
        .I3(pst_reg_cnt[152]),
        .I4(q_reg_i_446_n_0),
        .O(q_reg_i_431_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_432
       (.I0(pst_reg_cnt[94]),
        .I1(pst_reg_cnt[95]),
        .I2(pst_reg_cnt[92]),
        .I3(pst_reg_cnt[93]),
        .O(q_reg_i_432_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_433
       (.I0(q_reg_i_432_n_0),
        .I1(pst_reg_cnt[89]),
        .I2(pst_reg_cnt[88]),
        .I3(pst_reg_cnt[91]),
        .I4(pst_reg_cnt[90]),
        .O(q_reg_i_433_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_434
       (.I0(q_reg_i_405_n_0),
        .I1(pst_reg_cnt[41]),
        .I2(pst_reg_cnt[40]),
        .I3(pst_reg_cnt[43]),
        .I4(pst_reg_cnt[42]),
        .O(q_reg_i_434_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_435
       (.I0(pst_reg_cnt[35]),
        .I1(pst_reg_cnt[37]),
        .I2(pst_reg_cnt[36]),
        .I3(pst_reg_cnt[39]),
        .I4(pst_reg_cnt[38]),
        .O(q_reg_i_435_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    q_reg_i_436
       (.I0(pst_reg_cnt[170]),
        .I1(pst_reg_cnt[169]),
        .O(q_reg_i_436_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_437
       (.I0(pst_reg_cnt[173]),
        .I1(pst_reg_cnt[174]),
        .I2(pst_reg_cnt[175]),
        .O(q_reg_i_437_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_438
       (.I0(pst_reg_cnt[119]),
        .I1(pst_reg_cnt[118]),
        .O(q_reg_i_438_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_439
       (.I0(q_reg_i_310_n_0),
        .I1(q_reg_i_458_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_420_n_0),
        .I4(q_reg_i_421_n_0),
        .I5(q_reg_i_422_n_0),
        .O(q_reg_i_439_n_0));
  LUT6 #(
    .INIT(64'h9A5965A665A69A59)) 
    q_reg_i_44
       (.I0(q_reg_i_79_n_0),
        .I1(\q_reg[271]_0 ),
        .I2(\q_reg[109]_0 ),
        .I3(q_reg_i_98_0),
        .I4(q_reg_i_81_n_0),
        .I5(q_reg_i_80_n_0),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_440
       (.I0(pst_reg_cnt[116]),
        .I1(pst_reg_cnt[117]),
        .I2(pst_reg_cnt[118]),
        .I3(pst_reg_cnt[119]),
        .O(q_reg_i_440_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_441
       (.I0(q_reg_i_383_n_0),
        .I1(q_reg_i_458_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_420_n_0),
        .I4(q_reg_i_421_n_0),
        .I5(q_reg_i_422_n_0),
        .O(q_reg_i_441_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    q_reg_i_442
       (.I0(pst_reg_cnt[121]),
        .I1(pst_reg_cnt[122]),
        .I2(pst_reg_cnt[123]),
        .O(q_reg_i_442_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_443
       (.I0(q_reg_i_566_n_0),
        .I1(q_reg_i_335_n_0),
        .I2(q_reg_i_536_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_476_n_0),
        .I5(q_reg_i_447_n_0),
        .O(q_reg_i_443_n_0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    q_reg_i_444
       (.I0(pst_reg_cnt[163]),
        .I1(q_reg_i_335_n_0),
        .I2(q_reg_i_536_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_476_n_0),
        .I5(q_reg_i_447_n_0),
        .O(q_reg_i_444_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_445
       (.I0(q_reg_i_259_n_0),
        .I1(q_reg_i_417_n_0),
        .I2(q_reg_i_418_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_476_n_0),
        .I5(q_reg_i_184_n_0),
        .O(q_reg_i_445_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_446
       (.I0(pst_reg_cnt[158]),
        .I1(pst_reg_cnt[159]),
        .I2(pst_reg_cnt[156]),
        .I3(pst_reg_cnt[157]),
        .O(q_reg_i_446_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_447
       (.I0(pst_reg_cnt[166]),
        .I1(pst_reg_cnt[167]),
        .I2(pst_reg_cnt[164]),
        .I3(pst_reg_cnt[165]),
        .O(q_reg_i_447_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_448
       (.I0(pst_reg_cnt[234]),
        .I1(pst_reg_cnt[235]),
        .I2(pst_reg_cnt[232]),
        .I3(pst_reg_cnt[233]),
        .I4(q_reg_i_555_n_0),
        .I5(q_reg_i_145_n_0),
        .O(q_reg_i_448_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_449
       (.I0(pst_reg_cnt[228]),
        .I1(pst_reg_cnt[229]),
        .I2(pst_reg_cnt[230]),
        .I3(pst_reg_cnt[231]),
        .O(q_reg_i_449_n_0));
  LUT4 #(
    .INIT(16'h40FF)) 
    q_reg_i_45
       (.I0(q_reg_i_82_n_0),
        .I1(q_reg_i_84_0),
        .I2(q_reg_i_183_0),
        .I3(q_reg_i_83_n_0),
        .O(S[2]));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_450
       (.I0(pst_reg_cnt[86]),
        .I1(pst_reg_cnt[87]),
        .I2(pst_reg_cnt[84]),
        .I3(pst_reg_cnt[85]),
        .O(q_reg_i_450_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_451
       (.I0(pst_reg_cnt[172]),
        .I1(q_reg_i_422_n_0),
        .I2(q_reg_i_430_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_536_n_0),
        .I5(q_reg_i_437_n_0),
        .O(q_reg_i_451_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_452
       (.I0(pst_reg_cnt[145]),
        .I1(q_reg_i_429_n_0),
        .I2(q_reg_i_318_n_0),
        .I3(q_reg_i_431_n_0),
        .I4(pst_reg_cnt[147]),
        .I5(pst_reg_cnt[146]),
        .O(q_reg_i_452_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_453
       (.I0(pst_reg_cnt[91]),
        .I1(q_reg_i_325_n_0),
        .I2(pst_reg_cnt[94]),
        .I3(pst_reg_cnt[95]),
        .I4(pst_reg_cnt[92]),
        .I5(pst_reg_cnt[93]),
        .O(q_reg_i_453_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_454
       (.I0(pst_reg_cnt[73]),
        .I1(q_reg_i_414_n_0),
        .I2(q_reg_i_415_n_0),
        .I3(q_reg_i_325_n_0),
        .I4(pst_reg_cnt[75]),
        .I5(pst_reg_cnt[74]),
        .O(q_reg_i_454_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_455
       (.I0(q_reg_i_483_n_0),
        .I1(pst_reg_cnt[16]),
        .I2(q_reg_i_396_n_0),
        .I3(q_reg_i_375_n_0),
        .I4(q_reg_i_573_n_0),
        .I5(pst_reg_cnt[4]),
        .O(q_reg_i_455_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_456
       (.I0(pst_reg_cnt[19]),
        .I1(q_reg_i_396_n_0),
        .I2(pst_reg_cnt[22]),
        .I3(pst_reg_cnt[23]),
        .I4(pst_reg_cnt[20]),
        .I5(pst_reg_cnt[21]),
        .O(q_reg_i_456_n_0));
  LUT6 #(
    .INIT(64'h0000000000000E00)) 
    q_reg_i_457
       (.I0(q_reg_i_288_n_0),
        .I1(q_reg_i_289_n_0),
        .I2(q_reg_i_402_n_0),
        .I3(q_reg_i_406_n_0),
        .I4(q_reg_i_434_n_0),
        .I5(pst_reg_cnt[37]),
        .O(q_reg_i_457_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_458
       (.I0(q_reg_i_536_n_0),
        .I1(q_reg_i_335_n_0),
        .I2(q_reg_i_574_n_0),
        .I3(q_reg_i_417_n_0),
        .I4(q_reg_i_472_n_0),
        .I5(q_reg_i_575_n_0),
        .O(q_reg_i_458_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_459
       (.I0(q_reg_i_422_n_0),
        .I1(q_reg_i_430_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_418_n_0),
        .I4(q_reg_i_446_n_0),
        .I5(q_reg_i_534_n_0),
        .O(q_reg_i_459_n_0));
  LUT6 #(
    .INIT(64'h870F0F7887F078F0)) 
    q_reg_i_46
       (.I0(q_reg_i_79_n_0),
        .I1(q_reg_i_84_n_0),
        .I2(\q_reg[137]_0 ),
        .I3(q_reg_i_183_0),
        .I4(q_reg_i_85_n_0),
        .I5(q_reg_i_86_n_0),
        .O(S[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_460
       (.I0(q_reg_i_422_n_0),
        .I1(q_reg_i_430_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_213_n_0),
        .I4(q_reg_i_253_n_0),
        .I5(q_reg_i_576_n_0),
        .O(q_reg_i_460_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    q_reg_i_461
       (.I0(q_reg_i_419_n_0),
        .I1(q_reg_i_430_n_0),
        .I2(q_reg_i_422_n_0),
        .I3(pst_reg_cnt[191]),
        .I4(pst_reg_cnt[190]),
        .I5(pst_reg_cnt[189]),
        .O(q_reg_i_461_n_0));
  LUT6 #(
    .INIT(64'h000000000000000E)) 
    q_reg_i_462
       (.I0(q_reg_i_493_n_0),
        .I1(pst_reg_cnt[199]),
        .I2(q_reg_i_535_n_0),
        .I3(q_reg_i_152_n_0),
        .I4(q_reg_i_420_n_0),
        .I5(q_reg_i_577_n_0),
        .O(q_reg_i_462_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_463
       (.I0(q_reg_i_328_n_0),
        .I1(q_reg_i_420_n_0),
        .I2(q_reg_i_152_n_0),
        .I3(q_reg_i_493_n_0),
        .O(q_reg_i_463_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_464
       (.I0(pst_reg_cnt[211]),
        .I1(q_reg_i_578_n_0),
        .I2(q_reg_i_152_n_0),
        .I3(q_reg_i_149_n_0),
        .I4(pst_reg_cnt[213]),
        .I5(pst_reg_cnt[212]),
        .O(q_reg_i_464_n_0));
  LUT5 #(
    .INIT(32'h000000FE)) 
    q_reg_i_465
       (.I0(pst_reg_cnt[206]),
        .I1(pst_reg_cnt[207]),
        .I2(pst_reg_cnt[205]),
        .I3(q_reg_i_152_n_0),
        .I4(q_reg_i_420_n_0),
        .O(q_reg_i_465_n_0));
  LUT6 #(
    .INIT(64'h000000000000000E)) 
    q_reg_i_466
       (.I0(q_reg_i_340_n_0),
        .I1(pst_reg_cnt[187]),
        .I2(q_reg_i_255_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_430_n_0),
        .I5(q_reg_i_422_n_0),
        .O(q_reg_i_466_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_467
       (.I0(pst_reg_cnt[193]),
        .I1(q_reg_i_328_n_0),
        .I2(q_reg_i_420_n_0),
        .I3(q_reg_i_152_n_0),
        .I4(q_reg_i_493_n_0),
        .I5(q_reg_i_572_n_0),
        .O(q_reg_i_467_n_0));
  LUT6 #(
    .INIT(64'h00000000000000FD)) 
    q_reg_i_468
       (.I0(q_reg_i_211_n_0),
        .I1(pst_reg_cnt[181]),
        .I2(q_reg_i_253_n_0),
        .I3(q_reg_i_213_n_0),
        .I4(q_reg_i_419_n_0),
        .I5(q_reg_i_476_n_0),
        .O(q_reg_i_468_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_469
       (.I0(pst_reg_cnt[151]),
        .I1(q_reg_i_422_n_0),
        .I2(q_reg_i_430_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_418_n_0),
        .I5(q_reg_i_431_n_0),
        .O(q_reg_i_469_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_470
       (.I0(pst_reg_cnt[155]),
        .I1(pst_reg_cnt[154]),
        .O(q_reg_i_470_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_471
       (.I0(pst_reg_cnt[159]),
        .I1(pst_reg_cnt[158]),
        .O(q_reg_i_471_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_472
       (.I0(pst_reg_cnt[137]),
        .I1(pst_reg_cnt[136]),
        .I2(pst_reg_cnt[139]),
        .I3(pst_reg_cnt[138]),
        .I4(q_reg_i_184_n_0),
        .O(q_reg_i_472_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_473
       (.I0(pst_reg_cnt[135]),
        .I1(pst_reg_cnt[134]),
        .O(q_reg_i_473_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_474
       (.I0(pst_reg_cnt[150]),
        .I1(pst_reg_cnt[149]),
        .O(q_reg_i_474_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_475
       (.I0(q_reg_i_490_n_0),
        .I1(q_reg_i_246_n_0),
        .I2(q_reg_i_247_n_0),
        .I3(pst_reg_cnt[97]),
        .I4(pst_reg_cnt[96]),
        .I5(q_reg_i_579_n_0),
        .O(q_reg_i_475_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_476
       (.I0(q_reg_i_349_n_0),
        .I1(q_reg_i_561_n_0),
        .I2(q_reg_i_580_n_0),
        .I3(q_reg_i_581_n_0),
        .I4(q_reg_i_493_n_0),
        .I5(q_reg_i_420_n_0),
        .O(q_reg_i_476_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_477
       (.I0(pst_reg_cnt[73]),
        .I1(pst_reg_cnt[72]),
        .I2(pst_reg_cnt[75]),
        .I3(pst_reg_cnt[74]),
        .I4(q_reg_i_414_n_0),
        .O(q_reg_i_477_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_478
       (.I0(q_reg_i_415_n_0),
        .I1(q_reg_i_234_n_0),
        .I2(pst_reg_cnt[65]),
        .I3(pst_reg_cnt[64]),
        .I4(q_reg_i_531_n_0),
        .I5(q_reg_i_477_n_0),
        .O(q_reg_i_478_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_479
       (.I0(q_reg_i_348_n_0),
        .I1(q_reg_i_347_n_0),
        .I2(q_reg_i_346_n_0),
        .I3(q_reg_i_542_n_0),
        .O(q_reg_i_479_n_0));
  LUT6 #(
    .INIT(64'hD42B00FFFF00D42B)) 
    q_reg_i_48
       (.I0(\q_reg[34]_0 ),
        .I1(q_reg_i_116_0),
        .I2(q_reg_i_113_0),
        .I3(DI[0]),
        .I4(q_reg_i_67_n_0),
        .I5(q_reg_i_66_n_0),
        .O(S[0]));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_480
       (.I0(q_reg_i_346_n_0),
        .I1(pst_reg_cnt[303]),
        .I2(pst_reg_cnt[302]),
        .I3(pst_reg_cnt[301]),
        .I4(pst_reg_cnt[300]),
        .O(q_reg_i_480_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_481
       (.I0(pst_reg_cnt[247]),
        .I1(pst_reg_cnt[246]),
        .I2(pst_reg_cnt[245]),
        .I3(pst_reg_cnt[244]),
        .I4(q_reg_i_500_n_0),
        .I5(q_reg_i_419_n_0),
        .O(q_reg_i_481_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_482
       (.I0(pst_reg_cnt[14]),
        .I1(pst_reg_cnt[15]),
        .I2(pst_reg_cnt[12]),
        .I3(pst_reg_cnt[13]),
        .O(q_reg_i_482_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_483
       (.I0(q_reg_i_393_n_0),
        .I1(pst_reg_cnt[19]),
        .I2(pst_reg_cnt[17]),
        .I3(pst_reg_cnt[18]),
        .O(q_reg_i_483_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_484
       (.I0(pst_reg_cnt[227]),
        .I1(pst_reg_cnt[226]),
        .O(q_reg_i_484_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_485
       (.I0(pst_reg_cnt[231]),
        .I1(pst_reg_cnt[230]),
        .O(q_reg_i_485_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_486
       (.I0(pst_reg_cnt[223]),
        .I1(q_reg_i_152_n_0),
        .O(q_reg_i_486_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_487
       (.I0(q_reg_i_582_n_0),
        .I1(q_reg_i_246_n_0),
        .I2(q_reg_i_490_n_0),
        .I3(q_reg_i_477_n_0),
        .I4(q_reg_i_583_n_0),
        .I5(q_reg_i_415_n_0),
        .O(q_reg_i_487_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_488
       (.I0(q_reg_i_447_n_0),
        .I1(q_reg_i_422_n_0),
        .I2(q_reg_i_430_n_0),
        .I3(q_reg_i_419_n_0),
        .I4(q_reg_i_536_n_0),
        .I5(q_reg_i_335_n_0),
        .O(q_reg_i_488_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_489
       (.I0(pst_reg_cnt[108]),
        .I1(pst_reg_cnt[109]),
        .I2(pst_reg_cnt[110]),
        .I3(pst_reg_cnt[111]),
        .O(q_reg_i_489_n_0));
  LUT6 #(
    .INIT(64'hB4B44B4BB4B44BB4)) 
    q_reg_i_49
       (.I0(q_reg_i_87_n_0),
        .I1(q_reg_i_88_n_0),
        .I2(q_reg_i_89_n_0),
        .I3(q_reg_i_90_n_0),
        .I4(q_reg_i_91_n_0),
        .I5(q_reg_i_92_n_0),
        .O(\q_reg[91]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_490
       (.I0(q_reg_i_383_n_0),
        .I1(q_reg_i_584_n_0),
        .I2(q_reg_i_440_n_0),
        .I3(q_reg_i_585_n_0),
        .I4(pst_reg_cnt[112]),
        .I5(pst_reg_cnt[113]),
        .O(q_reg_i_490_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_491
       (.I0(q_reg_i_542_n_0),
        .I1(pst_reg_cnt[281]),
        .I2(pst_reg_cnt[280]),
        .I3(pst_reg_cnt[283]),
        .I4(pst_reg_cnt[282]),
        .O(q_reg_i_491_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_492
       (.I0(pst_reg_cnt[268]),
        .I1(pst_reg_cnt[269]),
        .I2(pst_reg_cnt[270]),
        .I3(pst_reg_cnt[271]),
        .O(q_reg_i_492_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_493
       (.I0(q_reg_i_535_n_0),
        .I1(pst_reg_cnt[201]),
        .I2(pst_reg_cnt[200]),
        .I3(pst_reg_cnt[203]),
        .I4(pst_reg_cnt[202]),
        .O(q_reg_i_493_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_494
       (.I0(pst_reg_cnt[62]),
        .I1(pst_reg_cnt[63]),
        .I2(pst_reg_cnt[60]),
        .I3(pst_reg_cnt[61]),
        .O(q_reg_i_494_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_495
       (.I0(pst_reg_cnt[54]),
        .I1(pst_reg_cnt[55]),
        .O(q_reg_i_495_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_496
       (.I0(q_reg_i_494_n_0),
        .I1(q_reg_i_558_n_0),
        .I2(pst_reg_cnt[56]),
        .I3(pst_reg_cnt[57]),
        .I4(q_reg_i_408_n_0),
        .I5(q_reg_i_409_n_0),
        .O(q_reg_i_496_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_497
       (.I0(pst_reg_cnt[38]),
        .I1(pst_reg_cnt[39]),
        .I2(pst_reg_cnt[36]),
        .I3(pst_reg_cnt[37]),
        .O(q_reg_i_497_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_498
       (.I0(pst_reg_cnt[243]),
        .I1(pst_reg_cnt[242]),
        .O(q_reg_i_498_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_499
       (.I0(pst_reg_cnt[244]),
        .I1(pst_reg_cnt[245]),
        .I2(pst_reg_cnt[246]),
        .I3(pst_reg_cnt[247]),
        .O(q_reg_i_499_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    q_reg_i_50
       (.I0(q_reg_i_93_n_0),
        .I1(q_reg_i_94_n_0),
        .I2(q_reg_i_95_n_0),
        .O(q_reg_i_95_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_500
       (.I0(pst_reg_cnt[249]),
        .I1(pst_reg_cnt[248]),
        .I2(pst_reg_cnt[251]),
        .I3(pst_reg_cnt[250]),
        .I4(q_reg_i_550_n_0),
        .O(q_reg_i_500_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_501
       (.I0(pst_reg_cnt[255]),
        .I1(pst_reg_cnt[254]),
        .O(q_reg_i_501_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_502
       (.I0(q_reg_i_252_n_0),
        .I1(q_reg_i_344_n_0),
        .I2(q_reg_i_346_n_0),
        .I3(q_reg_i_347_n_0),
        .I4(q_reg_i_348_n_0),
        .O(q_reg_i_502_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_503
       (.I0(pst_reg_cnt[16]),
        .I1(pst_reg_cnt[18]),
        .I2(pst_reg_cnt[17]),
        .I3(pst_reg_cnt[19]),
        .I4(q_reg_i_393_n_0),
        .O(q_reg_i_503_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_504
       (.I0(pst_reg_cnt[83]),
        .I1(pst_reg_cnt[82]),
        .O(q_reg_i_504_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_505
       (.I0(pst_reg_cnt[87]),
        .I1(pst_reg_cnt[86]),
        .O(q_reg_i_505_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    q_reg_i_506
       (.I0(pst_reg_cnt[23]),
        .I1(pst_reg_cnt[22]),
        .O(q_reg_i_506_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_507
       (.I0(pst_reg_cnt[226]),
        .I1(pst_reg_cnt[227]),
        .I2(q_reg_i_561_n_0),
        .I3(q_reg_i_145_n_0),
        .I4(q_reg_i_449_n_0),
        .O(q_reg_i_507_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_508
       (.I0(pst_reg_cnt[230]),
        .I1(pst_reg_cnt[231]),
        .I2(q_reg_i_145_n_0),
        .I3(q_reg_i_561_n_0),
        .O(q_reg_i_508_n_0));
  LUT5 #(
    .INIT(32'hCCC8FFFF)) 
    q_reg_i_509
       (.I0(pst_reg_cnt[307]),
        .I1(q_reg_i_586_n_0),
        .I2(pst_reg_cnt[309]),
        .I3(pst_reg_cnt[308]),
        .I4(q_reg_i_346_n_0),
        .O(q_reg_i_509_n_0));
  LUT3 #(
    .INIT(8'hE8)) 
    q_reg_i_51
       (.I0(q_reg_i_96_n_0),
        .I1(q_reg_i_97_n_0),
        .I2(q_reg_i_98_n_0),
        .O(q_reg_i_98_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    q_reg_i_510
       (.I0(pst_reg_cnt[89]),
        .I1(pst_reg_cnt[90]),
        .I2(pst_reg_cnt[88]),
        .I3(pst_reg_cnt[87]),
        .O(q_reg_i_510_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    q_reg_i_511
       (.I0(pst_reg_cnt[95]),
        .I1(pst_reg_cnt[94]),
        .I2(pst_reg_cnt[93]),
        .O(q_reg_i_511_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_512
       (.I0(pst_reg_cnt[255]),
        .I1(pst_reg_cnt[254]),
        .I2(q_reg_i_419_n_0),
        .I3(pst_reg_cnt[253]),
        .I4(pst_reg_cnt[252]),
        .I5(pst_reg_cnt[251]),
        .O(q_reg_i_512_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_513
       (.I0(pst_reg_cnt[249]),
        .I1(pst_reg_cnt[250]),
        .O(q_reg_i_513_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_514
       (.I0(pst_reg_cnt[255]),
        .I1(q_reg_i_419_n_0),
        .O(q_reg_i_514_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    q_reg_i_515
       (.I0(pst_reg_cnt[261]),
        .I1(pst_reg_cnt[260]),
        .I2(pst_reg_cnt[263]),
        .I3(pst_reg_cnt[262]),
        .I4(q_reg_i_502_n_0),
        .O(q_reg_i_515_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    q_reg_i_516
       (.I0(pst_reg_cnt[257]),
        .I1(pst_reg_cnt[258]),
        .I2(pst_reg_cnt[259]),
        .O(q_reg_i_516_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_517
       (.I0(pst_reg_cnt[37]),
        .I1(q_reg_i_434_n_0),
        .I2(q_reg_i_496_n_0),
        .I3(q_reg_i_236_n_0),
        .I4(pst_reg_cnt[39]),
        .I5(pst_reg_cnt[38]),
        .O(q_reg_i_517_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_518
       (.I0(q_reg_i_419_n_0),
        .I1(pst_reg_cnt[254]),
        .I2(pst_reg_cnt[255]),
        .O(q_reg_i_518_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_519
       (.I0(pst_reg_cnt[235]),
        .I1(q_reg_i_145_n_0),
        .I2(pst_reg_cnt[236]),
        .I3(pst_reg_cnt[237]),
        .I4(pst_reg_cnt[238]),
        .I5(pst_reg_cnt[239]),
        .O(q_reg_i_519_n_0));
  LUT5 #(
    .INIT(32'h8A75758A)) 
    q_reg_i_52
       (.I0(q_reg_i_99_n_0),
        .I1(q_reg_i_83_n_0),
        .I2(q_reg_i_100_n_0),
        .I3(q_reg_i_101_n_0),
        .I4(q_reg_i_102_n_0),
        .O(\q_reg[271]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    q_reg_i_520
       (.I0(pst_reg_cnt[307]),
        .I1(q_reg_i_361_n_0),
        .I2(pst_reg_cnt[310]),
        .I3(pst_reg_cnt[311]),
        .I4(pst_reg_cnt[309]),
        .I5(pst_reg_cnt[308]),
        .O(q_reg_i_520_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    q_reg_i_521
       (.I0(pst_reg_cnt[295]),
        .I1(pst_reg_cnt[294]),
        .I2(pst_reg_cnt[293]),
        .I3(pst_reg_cnt[292]),
        .I4(q_reg_i_304_n_0),
        .O(q_reg_i_521_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_522
       (.I0(q_reg_i_496_n_0),
        .I1(q_reg_i_587_n_0),
        .I2(q_reg_i_434_n_0),
        .O(q_reg_i_522_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_523
       (.I0(pst_reg_cnt[11]),
        .I1(pst_reg_cnt[10]),
        .O(q_reg_i_523_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_524
       (.I0(pst_reg_cnt[292]),
        .I1(pst_reg_cnt[293]),
        .I2(pst_reg_cnt[294]),
        .I3(pst_reg_cnt[295]),
        .O(q_reg_i_524_n_0));
  LUT4 #(
    .INIT(16'h0051)) 
    q_reg_i_525
       (.I0(q_reg_i_502_n_0),
        .I1(pst_reg_cnt[261]),
        .I2(pst_reg_cnt[262]),
        .I3(pst_reg_cnt[263]),
        .O(q_reg_i_525_n_0));
  LUT5 #(
    .INIT(32'hFEFFFEFE)) 
    q_reg_i_526
       (.I0(pst_reg_cnt[115]),
        .I1(pst_reg_cnt[114]),
        .I2(pst_reg_cnt[113]),
        .I3(pst_reg_cnt[112]),
        .I4(pst_reg_cnt[111]),
        .O(q_reg_i_526_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    q_reg_i_527
       (.I0(pst_reg_cnt[107]),
        .I1(pst_reg_cnt[106]),
        .I2(pst_reg_cnt[105]),
        .O(q_reg_i_527_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF0D)) 
    q_reg_i_528
       (.I0(q_reg_i_588_n_0),
        .I1(pst_reg_cnt[126]),
        .I2(pst_reg_cnt[127]),
        .I3(q_reg_i_476_n_0),
        .I4(q_reg_i_419_n_0),
        .I5(q_reg_i_458_n_0),
        .O(q_reg_i_528_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_529
       (.I0(pst_reg_cnt[134]),
        .I1(pst_reg_cnt[135]),
        .I2(pst_reg_cnt[132]),
        .I3(pst_reg_cnt[133]),
        .O(q_reg_i_529_n_0));
  LUT5 #(
    .INIT(32'h8AFF7500)) 
    q_reg_i_53
       (.I0(q_reg_i_103_n_0),
        .I1(q_reg_i_104_n_0),
        .I2(q_reg_i_105_n_0),
        .I3(q_reg_i_106_n_0),
        .I4(q_reg_i_107_n_0),
        .O(\q_reg[109]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_530
       (.I0(pst_reg_cnt[300]),
        .I1(pst_reg_cnt[301]),
        .I2(pst_reg_cnt[302]),
        .I3(pst_reg_cnt[303]),
        .O(q_reg_i_530_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_531
       (.I0(pst_reg_cnt[67]),
        .I1(pst_reg_cnt[66]),
        .O(q_reg_i_531_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_532
       (.I0(pst_reg_cnt[108]),
        .I1(pst_reg_cnt[109]),
        .O(q_reg_i_532_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFCFFFFFFFE)) 
    q_reg_i_533
       (.I0(pst_reg_cnt[264]),
        .I1(pst_reg_cnt[266]),
        .I2(pst_reg_cnt[267]),
        .I3(q_reg_i_232_n_0),
        .I4(q_reg_i_492_n_0),
        .I5(pst_reg_cnt[265]),
        .O(q_reg_i_533_n_0));
  LUT3 #(
    .INIT(8'hBA)) 
    q_reg_i_534
       (.I0(pst_reg_cnt[155]),
        .I1(pst_reg_cnt[154]),
        .I2(pst_reg_cnt[153]),
        .O(q_reg_i_534_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_535
       (.I0(pst_reg_cnt[204]),
        .I1(pst_reg_cnt[205]),
        .I2(pst_reg_cnt[206]),
        .I3(pst_reg_cnt[207]),
        .O(q_reg_i_535_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_536
       (.I0(q_reg_i_589_n_0),
        .I1(q_reg_i_340_n_0),
        .I2(q_reg_i_590_n_0),
        .I3(q_reg_i_253_n_0),
        .I4(pst_reg_cnt[180]),
        .I5(pst_reg_cnt[181]),
        .O(q_reg_i_536_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_537
       (.I0(pst_reg_cnt[171]),
        .I1(pst_reg_cnt[172]),
        .O(q_reg_i_537_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_538
       (.I0(pst_reg_cnt[174]),
        .I1(pst_reg_cnt[175]),
        .I2(pst_reg_cnt[172]),
        .I3(pst_reg_cnt[173]),
        .O(q_reg_i_538_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_539
       (.I0(pst_reg_cnt[275]),
        .I1(pst_reg_cnt[274]),
        .O(q_reg_i_539_n_0));
  LUT5 #(
    .INIT(32'hBA4545BA)) 
    q_reg_i_54
       (.I0(q_reg_i_108_n_0),
        .I1(q_reg_i_109_n_0),
        .I2(q_reg_i_92_n_0),
        .I3(q_reg_i_110_n_0),
        .I4(q_reg_i_111_n_0),
        .O(\q_reg[34]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_540
       (.I0(pst_reg_cnt[278]),
        .I1(pst_reg_cnt[279]),
        .I2(pst_reg_cnt[276]),
        .I3(pst_reg_cnt[277]),
        .O(q_reg_i_540_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_541
       (.I0(pst_reg_cnt[282]),
        .I1(pst_reg_cnt[283]),
        .I2(pst_reg_cnt[280]),
        .I3(pst_reg_cnt[281]),
        .O(q_reg_i_541_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_542
       (.I0(pst_reg_cnt[286]),
        .I1(pst_reg_cnt[287]),
        .I2(pst_reg_cnt[284]),
        .I3(pst_reg_cnt[285]),
        .O(q_reg_i_542_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_543
       (.I0(pst_reg_cnt[267]),
        .I1(pst_reg_cnt[266]),
        .O(q_reg_i_543_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_544
       (.I0(pst_reg_cnt[258]),
        .I1(pst_reg_cnt[259]),
        .I2(pst_reg_cnt[256]),
        .I3(pst_reg_cnt[257]),
        .O(q_reg_i_544_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_545
       (.I0(pst_reg_cnt[262]),
        .I1(pst_reg_cnt[263]),
        .I2(pst_reg_cnt[260]),
        .I3(pst_reg_cnt[261]),
        .O(q_reg_i_545_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    q_reg_i_546
       (.I0(pst_reg_cnt[319]),
        .I1(pst_reg_cnt[318]),
        .I2(pst_reg_cnt[316]),
        .I3(pst_reg_cnt[317]),
        .O(q_reg_i_546_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_547
       (.I0(pst_reg_cnt[315]),
        .I1(pst_reg_cnt[314]),
        .O(q_reg_i_547_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_548
       (.I0(pst_reg_cnt[308]),
        .I1(pst_reg_cnt[309]),
        .I2(pst_reg_cnt[305]),
        .I3(pst_reg_cnt[306]),
        .O(q_reg_i_548_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_549
       (.I0(pst_reg_cnt[310]),
        .I1(pst_reg_cnt[311]),
        .I2(pst_reg_cnt[304]),
        .I3(pst_reg_cnt[307]),
        .O(q_reg_i_549_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    q_reg_i_55
       (.I0(\q_reg[188]_0 ),
        .I1(q_reg_i_112_n_0),
        .I2(q_reg_i_113_n_0),
        .O(q_reg_i_113_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_550
       (.I0(pst_reg_cnt[254]),
        .I1(pst_reg_cnt[255]),
        .I2(pst_reg_cnt[252]),
        .I3(pst_reg_cnt[253]),
        .O(q_reg_i_550_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_551
       (.I0(pst_reg_cnt[250]),
        .I1(pst_reg_cnt[251]),
        .I2(pst_reg_cnt[248]),
        .I3(pst_reg_cnt[249]),
        .O(q_reg_i_551_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_552
       (.I0(pst_reg_cnt[299]),
        .I1(pst_reg_cnt[298]),
        .O(q_reg_i_552_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_553
       (.I0(pst_reg_cnt[288]),
        .I1(pst_reg_cnt[289]),
        .I2(pst_reg_cnt[290]),
        .I3(pst_reg_cnt[291]),
        .O(q_reg_i_553_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_554
       (.I0(pst_reg_cnt[234]),
        .I1(pst_reg_cnt[235]),
        .I2(pst_reg_cnt[232]),
        .I3(pst_reg_cnt[233]),
        .O(q_reg_i_554_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_555
       (.I0(pst_reg_cnt[236]),
        .I1(pst_reg_cnt[237]),
        .I2(pst_reg_cnt[238]),
        .I3(pst_reg_cnt[239]),
        .O(q_reg_i_555_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_556
       (.I0(pst_reg_cnt[291]),
        .I1(pst_reg_cnt[290]),
        .O(q_reg_i_556_n_0));
  LUT3 #(
    .INIT(8'h8A)) 
    q_reg_i_557
       (.I0(q_reg_i_279_n_0),
        .I1(pst_reg_cnt[313]),
        .I2(pst_reg_cnt[312]),
        .O(q_reg_i_557_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_558
       (.I0(pst_reg_cnt[59]),
        .I1(pst_reg_cnt[58]),
        .O(q_reg_i_558_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_559
       (.I0(pst_reg_cnt[71]),
        .I1(pst_reg_cnt[70]),
        .O(q_reg_i_559_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    q_reg_i_56
       (.I0(q_reg_i_114_n_0),
        .I1(q_reg_i_115_n_0),
        .I2(q_reg_i_116_n_0),
        .O(q_reg_i_116_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    q_reg_i_560
       (.I0(pst_reg_cnt[271]),
        .I1(pst_reg_cnt[270]),
        .I2(pst_reg_cnt[269]),
        .I3(pst_reg_cnt[268]),
        .I4(q_reg_i_232_n_0),
        .O(q_reg_i_560_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_561
       (.I0(q_reg_i_555_n_0),
        .I1(pst_reg_cnt[233]),
        .I2(pst_reg_cnt[232]),
        .I3(pst_reg_cnt[235]),
        .I4(pst_reg_cnt[234]),
        .O(q_reg_i_561_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_562
       (.I0(pst_reg_cnt[117]),
        .I1(pst_reg_cnt[118]),
        .O(q_reg_i_562_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_563
       (.I0(pst_reg_cnt[24]),
        .I1(pst_reg_cnt[25]),
        .I2(q_reg_i_398_n_0),
        .O(q_reg_i_563_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_564
       (.I0(pst_reg_cnt[90]),
        .I1(pst_reg_cnt[91]),
        .I2(pst_reg_cnt[88]),
        .I3(pst_reg_cnt[89]),
        .O(q_reg_i_564_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_565
       (.I0(pst_reg_cnt[144]),
        .I1(pst_reg_cnt[145]),
        .I2(pst_reg_cnt[146]),
        .I3(pst_reg_cnt[147]),
        .O(q_reg_i_565_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_566
       (.I0(pst_reg_cnt[163]),
        .I1(pst_reg_cnt[162]),
        .O(q_reg_i_566_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_567
       (.I0(q_reg_i_545_n_0),
        .I1(pst_reg_cnt[257]),
        .I2(pst_reg_cnt[256]),
        .I3(pst_reg_cnt[259]),
        .I4(pst_reg_cnt[258]),
        .O(q_reg_i_567_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_568
       (.I0(pst_reg_cnt[219]),
        .I1(pst_reg_cnt[218]),
        .O(q_reg_i_568_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_569
       (.I0(pst_reg_cnt[214]),
        .I1(pst_reg_cnt[215]),
        .I2(pst_reg_cnt[208]),
        .I3(pst_reg_cnt[209]),
        .O(q_reg_i_569_n_0));
  LUT6 #(
    .INIT(64'h55FF55FD00FF0000)) 
    q_reg_i_57
       (.I0(q_reg_i_96_n_0),
        .I1(q_reg_i_117_n_0),
        .I2(q_reg_i_118_n_0),
        .I3(q_reg_i_119_n_0),
        .I4(q_reg_i_120_n_0),
        .I5(q_reg_i_121_n_0),
        .O(\q_reg[43]_0 ));
  LUT4 #(
    .INIT(16'hEE0E)) 
    q_reg_i_570
       (.I0(pst_reg_cnt[271]),
        .I1(q_reg_i_232_n_0),
        .I2(q_reg_i_238_n_0),
        .I3(q_reg_i_237_n_0),
        .O(q_reg_i_570_n_0));
  LUT6 #(
    .INIT(64'h0000000101010101)) 
    q_reg_i_571
       (.I0(q_reg_i_450_n_0),
        .I1(q_reg_i_591_n_0),
        .I2(q_reg_i_504_n_0),
        .I3(q_reg_i_289_n_0),
        .I4(q_reg_i_288_n_0),
        .I5(q_reg_i_244_n_0),
        .O(q_reg_i_571_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_572
       (.I0(pst_reg_cnt[195]),
        .I1(pst_reg_cnt[194]),
        .O(q_reg_i_572_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    q_reg_i_573
       (.I0(pst_reg_cnt[5]),
        .I1(pst_reg_cnt[6]),
        .I2(pst_reg_cnt[7]),
        .O(q_reg_i_573_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_574
       (.I0(q_reg_i_447_n_0),
        .I1(pst_reg_cnt[161]),
        .I2(pst_reg_cnt[160]),
        .I3(pst_reg_cnt[163]),
        .I4(pst_reg_cnt[162]),
        .O(q_reg_i_574_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_575
       (.I0(q_reg_i_529_n_0),
        .I1(pst_reg_cnt[131]),
        .I2(pst_reg_cnt[130]),
        .I3(pst_reg_cnt[129]),
        .I4(pst_reg_cnt[128]),
        .O(q_reg_i_575_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    q_reg_i_576
       (.I0(pst_reg_cnt[180]),
        .I1(pst_reg_cnt[181]),
        .O(q_reg_i_576_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_577
       (.I0(pst_reg_cnt[203]),
        .I1(pst_reg_cnt[202]),
        .O(q_reg_i_577_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_578
       (.I0(pst_reg_cnt[215]),
        .I1(pst_reg_cnt[214]),
        .O(q_reg_i_578_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_579
       (.I0(pst_reg_cnt[99]),
        .I1(pst_reg_cnt[98]),
        .O(q_reg_i_579_n_0));
  LUT5 #(
    .INIT(32'h2D2D222D)) 
    q_reg_i_58
       (.I0(\q_reg[31]_0 ),
        .I1(q_reg_i_122_n_0),
        .I2(q_reg_i_123_n_0),
        .I3(q_reg_i_124_n_0),
        .I4(q_reg_i_125_n_0),
        .O(\q_reg[295]_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_580
       (.I0(q_reg_i_449_n_0),
        .I1(pst_reg_cnt[227]),
        .I2(pst_reg_cnt[226]),
        .I3(pst_reg_cnt[225]),
        .I4(pst_reg_cnt[224]),
        .O(q_reg_i_580_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_581
       (.I0(pst_reg_cnt[193]),
        .I1(pst_reg_cnt[192]),
        .I2(pst_reg_cnt[195]),
        .I3(pst_reg_cnt[194]),
        .I4(q_reg_i_328_n_0),
        .O(q_reg_i_581_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_582
       (.I0(q_reg_i_247_n_0),
        .I1(pst_reg_cnt[97]),
        .I2(pst_reg_cnt[96]),
        .I3(pst_reg_cnt[99]),
        .I4(pst_reg_cnt[98]),
        .O(q_reg_i_582_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_583
       (.I0(q_reg_i_234_n_0),
        .I1(pst_reg_cnt[65]),
        .I2(pst_reg_cnt[64]),
        .I3(pst_reg_cnt[67]),
        .I4(pst_reg_cnt[66]),
        .O(q_reg_i_583_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_584
       (.I0(pst_reg_cnt[122]),
        .I1(pst_reg_cnt[123]),
        .I2(pst_reg_cnt[120]),
        .I3(pst_reg_cnt[121]),
        .O(q_reg_i_584_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    q_reg_i_585
       (.I0(pst_reg_cnt[115]),
        .I1(pst_reg_cnt[114]),
        .O(q_reg_i_585_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    q_reg_i_586
       (.I0(q_reg_i_361_n_0),
        .I1(pst_reg_cnt[310]),
        .I2(pst_reg_cnt[311]),
        .O(q_reg_i_586_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    q_reg_i_587
       (.I0(pst_reg_cnt[33]),
        .I1(pst_reg_cnt[32]),
        .I2(pst_reg_cnt[35]),
        .I3(pst_reg_cnt[34]),
        .I4(q_reg_i_497_n_0),
        .O(q_reg_i_587_n_0));
  LUT4 #(
    .INIT(16'hEFEE)) 
    q_reg_i_588
       (.I0(pst_reg_cnt[125]),
        .I1(pst_reg_cnt[126]),
        .I2(pst_reg_cnt[124]),
        .I3(pst_reg_cnt[123]),
        .O(q_reg_i_588_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_589
       (.I0(pst_reg_cnt[186]),
        .I1(pst_reg_cnt[187]),
        .I2(pst_reg_cnt[184]),
        .I3(pst_reg_cnt[185]),
        .O(q_reg_i_589_n_0));
  LUT6 #(
    .INIT(64'h000088F888F888F8)) 
    q_reg_i_59
       (.I0(q_reg_i_126_n_0),
        .I1(q_reg_i_90_n_0),
        .I2(q_reg_i_127_n_0),
        .I3(q_reg_i_128_n_0),
        .I4(\q_reg[188]_0 ),
        .I5(q_reg_i_129_n_0),
        .O(\q_reg[55]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    q_reg_i_590
       (.I0(pst_reg_cnt[178]),
        .I1(pst_reg_cnt[179]),
        .I2(pst_reg_cnt[176]),
        .I3(pst_reg_cnt[177]),
        .O(q_reg_i_590_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    q_reg_i_591
       (.I0(q_reg_i_422_n_0),
        .I1(q_reg_i_430_n_0),
        .I2(q_reg_i_419_n_0),
        .I3(q_reg_i_458_n_0),
        .I4(q_reg_i_475_n_0),
        .I5(q_reg_i_433_n_0),
        .O(q_reg_i_591_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFBA00)) 
    q_reg_i_60
       (.I0(q_reg_i_130_n_0),
        .I1(q_reg_i_131_n_0),
        .I2(q_reg_i_132_n_0),
        .I3(q_reg_i_122_n_0),
        .I4(q_reg_i_133_n_0),
        .I5(q_reg_i_134_n_0),
        .O(\q_reg[132]_0 ));
  LUT6 #(
    .INIT(64'hBBBB0B00BBBBBBBB)) 
    q_reg_i_61
       (.I0(q_reg_i_135_n_0),
        .I1(q_reg_i_136_n_0),
        .I2(q_reg_i_137_n_0),
        .I3(q_reg_i_138_n_0),
        .I4(q_reg_i_139_n_0),
        .I5(q_reg_i_140_n_0),
        .O(\q_reg[188]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00001011)) 
    q_reg_i_62
       (.I0(q_reg_i_141_n_0),
        .I1(q_reg_i_142_n_0),
        .I2(pst_reg_cnt[184]),
        .I3(pst_reg_cnt[183]),
        .I4(q_reg_i_143_n_0),
        .I5(q_reg_i_144_n_0),
        .O(\q_reg[184]_0 ));
  LUT6 #(
    .INIT(64'h0000000000001011)) 
    q_reg_i_63
       (.I0(q_reg_i_145_n_0),
        .I1(pst_reg_cnt[239]),
        .I2(pst_reg_cnt[238]),
        .I3(pst_reg_cnt[237]),
        .I4(q_reg_i_146_n_0),
        .I5(q_reg_i_147_n_0),
        .O(\q_reg[239]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF5554)) 
    q_reg_i_64
       (.I0(q_reg_i_148_n_0),
        .I1(q_reg_i_149_n_0),
        .I2(pst_reg_cnt[215]),
        .I3(q_reg_i_150_n_0),
        .I4(q_reg_i_151_n_0),
        .I5(q_reg_i_152_n_0),
        .O(\q_reg[215]_0 ));
  LUT6 #(
    .INIT(64'h4445FFFF44454445)) 
    q_reg_i_65
       (.I0(q_reg_i_129_n_0),
        .I1(q_reg_i_153_n_0),
        .I2(q_reg_i_154_n_0),
        .I3(q_reg_i_155_n_0),
        .I4(q_reg_i_124_n_0),
        .I5(q_reg_i_156_n_0),
        .O(\q_reg[247]_0 ));
  LUT5 #(
    .INIT(32'h007171FF)) 
    q_reg_i_66
       (.I0(\q_reg[55]_0 ),
        .I1(\q_reg[295]_0 ),
        .I2(\q_reg[43]_0 ),
        .I3(q_reg_i_69_n_0),
        .I4(q_reg_i_68_n_0),
        .O(q_reg_i_66_n_0));
  LUT3 #(
    .INIT(8'h69)) 
    q_reg_i_67
       (.I0(q_reg_i_98_n_0),
        .I1(q_reg_i_97_n_0),
        .I2(q_reg_i_96_n_0),
        .O(q_reg_i_67_n_0));
  LUT3 #(
    .INIT(8'h2B)) 
    q_reg_i_68
       (.I0(\q_reg[131]_0 ),
        .I1(\q_reg[132]_0 ),
        .I2(\q_reg[295]_1 ),
        .O(q_reg_i_68_n_0));
  LUT4 #(
    .INIT(16'h0444)) 
    q_reg_i_69
       (.I0(q_reg_i_123_n_0),
        .I1(\q_reg[31]_0 ),
        .I2(q_reg_i_67_n_0),
        .I3(q_reg_i_125_n_0),
        .O(q_reg_i_69_n_0));
  LUT6 #(
    .INIT(64'hAEAEAEAEAEAEAE00)) 
    q_reg_i_70
       (.I0(q_reg_i_157_n_0),
        .I1(q_reg_i_158_n_0),
        .I2(q_reg_i_159_n_0),
        .I3(q_reg_i_97_n_0),
        .I4(q_reg_i_160_n_0),
        .I5(q_reg_i_161_n_0),
        .O(\q_reg[131]_0 ));
  LUT6 #(
    .INIT(64'hF2F200F200F200F2)) 
    q_reg_i_71
       (.I0(q_reg_i_162_n_0),
        .I1(q_reg_i_108_n_0),
        .I2(q_reg_i_163_n_0),
        .I3(q_reg_i_98_n_0),
        .I4(q_reg_i_164_n_0),
        .I5(q_reg_i_165_n_0),
        .O(\q_reg[295]_1 ));
  LUT3 #(
    .INIT(8'h8A)) 
    q_reg_i_72
       (.I0(q_reg_i_166_n_0),
        .I1(q_reg_i_163_n_0),
        .I2(q_reg_i_167_n_0),
        .O(\q_reg[19]_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEEEEEEE000E)) 
    q_reg_i_73
       (.I0(q_reg_i_168_n_0),
        .I1(q_reg_i_169_n_0),
        .I2(q_reg_i_170_n_0),
        .I3(q_reg_i_171_n_0),
        .I4(q_reg_i_172_n_0),
        .I5(q_reg_i_173_n_0),
        .O(\q_reg[31]_0 ));
  LUT6 #(
    .INIT(64'h0000000002000202)) 
    q_reg_i_74
       (.I0(q_reg_i_174_n_0),
        .I1(pst_reg_cnt[77]),
        .I2(pst_reg_cnt[78]),
        .I3(pst_reg_cnt[76]),
        .I4(pst_reg_cnt[75]),
        .I5(q_reg_i_175_n_0),
        .O(\q_reg[77]_0 ));
  LUT6 #(
    .INIT(64'h00000000FFFFFFAE)) 
    q_reg_i_75
       (.I0(q_reg_i_176_n_0),
        .I1(pst_reg_cnt[57]),
        .I2(pst_reg_cnt[58]),
        .I3(pst_reg_cnt[59]),
        .I4(q_reg_i_177_n_0),
        .I5(q_reg_i_178_n_0),
        .O(\q_reg[57]_0 ));
  LUT5 #(
    .INIT(32'h7DD71441)) 
    q_reg_i_76
       (.I0(q_reg_i_179_n_0),
        .I1(q_reg_i_180_n_0),
        .I2(q_reg_i_181_n_0),
        .I3(q_reg_i_182_n_0),
        .I4(q_reg_i_183_n_0),
        .O(q_reg_i_183_0));
  LUT4 #(
    .INIT(16'h9666)) 
    q_reg_i_77
       (.I0(q_reg_i_86_n_0),
        .I1(q_reg_i_85_n_0),
        .I2(q_reg_i_79_n_0),
        .I3(q_reg_i_84_n_0),
        .O(q_reg_i_84_0));
  LUT6 #(
    .INIT(64'h0000FFFEFFFEFFFE)) 
    q_reg_i_78
       (.I0(q_reg_i_184_n_0),
        .I1(q_reg_i_185_n_0),
        .I2(pst_reg_cnt[137]),
        .I3(q_reg_i_186_n_0),
        .I4(q_reg_i_83_n_0),
        .I5(q_reg_i_82_n_0),
        .O(\q_reg[137]_0 ));
  LUT6 #(
    .INIT(64'h32FF323200320000)) 
    q_reg_i_79
       (.I0(q_reg_i_92_n_0),
        .I1(q_reg_i_91_n_0),
        .I2(q_reg_i_90_n_0),
        .I3(q_reg_i_87_n_0),
        .I4(q_reg_i_88_n_0),
        .I5(q_reg_i_89_n_0),
        .O(q_reg_i_79_n_0));
  LUT5 #(
    .INIT(32'h2BD4D42B)) 
    q_reg_i_80
       (.I0(q_reg_i_95_n_0),
        .I1(q_reg_i_94_n_0),
        .I2(q_reg_i_93_n_0),
        .I3(q_reg_i_179_n_0),
        .I4(q_reg_i_187_n_0),
        .O(q_reg_i_80_n_0));
  LUT5 #(
    .INIT(32'hFF696900)) 
    q_reg_i_81
       (.I0(q_reg_i_95_n_0),
        .I1(q_reg_i_94_n_0),
        .I2(q_reg_i_93_n_0),
        .I3(q_reg_i_188_n_0),
        .I4(\q_reg[91]_0 ),
        .O(q_reg_i_81_n_0));
  LUT5 #(
    .INIT(32'h0000FDD5)) 
    q_reg_i_82
       (.I0(q_reg_i_189_n_0),
        .I1(q_reg_i_92_n_0),
        .I2(q_reg_i_190_n_0),
        .I3(q_reg_i_191_n_0),
        .I4(q_reg_i_192_n_0),
        .O(q_reg_i_82_n_0));
  LUT6 #(
    .INIT(64'hFE00FE00FE00FEFE)) 
    q_reg_i_83
       (.I0(q_reg_i_185_n_0),
        .I1(pst_reg_cnt[143]),
        .I2(pst_reg_cnt[142]),
        .I3(q_reg_i_193_n_0),
        .I4(pst_reg_cnt[145]),
        .I5(q_reg_i_194_n_0),
        .O(q_reg_i_83_n_0));
  LUT3 #(
    .INIT(8'hB2)) 
    q_reg_i_84
       (.I0(q_reg_i_98_0),
        .I1(\q_reg[109]_0 ),
        .I2(\q_reg[271]_0 ),
        .O(q_reg_i_84_n_0));
  LUT3 #(
    .INIT(8'h17)) 
    q_reg_i_85
       (.I0(q_reg_i_180_n_0),
        .I1(q_reg_i_181_n_0),
        .I2(q_reg_i_182_n_0),
        .O(q_reg_i_85_n_0));
  LUT6 #(
    .INIT(64'h022AFDD5FFFF0000)) 
    q_reg_i_86
       (.I0(q_reg_i_189_n_0),
        .I1(q_reg_i_92_n_0),
        .I2(q_reg_i_190_n_0),
        .I3(q_reg_i_191_n_0),
        .I4(q_reg_i_192_n_0),
        .I5(q_reg_i_195_n_0),
        .O(q_reg_i_86_n_0));
  LUT3 #(
    .INIT(8'hA8)) 
    q_reg_i_87
       (.I0(q_reg_i_196_n_0),
        .I1(q_reg_i_100_n_0),
        .I2(q_reg_i_197_n_0),
        .O(q_reg_i_87_n_0));
  LUT3 #(
    .INIT(8'h8A)) 
    q_reg_i_88
       (.I0(q_reg_i_198_n_0),
        .I1(q_reg_i_83_n_0),
        .I2(q_reg_i_199_n_0),
        .O(q_reg_i_88_n_0));
  LUT6 #(
    .INIT(64'h55559AAAAAAA6555)) 
    q_reg_i_89
       (.I0(q_reg_i_200_n_0),
        .I1(q_reg_i_105_n_0),
        .I2(q_reg_i_201_n_0),
        .I3(q_reg_i_202_n_0),
        .I4(q_reg_i_203_n_0),
        .I5(q_reg_i_141_n_0),
        .O(q_reg_i_89_n_0));
  LUT5 #(
    .INIT(32'h1011FFFF)) 
    q_reg_i_90
       (.I0(q_reg_i_204_n_0),
        .I1(q_reg_i_205_n_0),
        .I2(pst_reg_cnt[91]),
        .I3(pst_reg_cnt[90]),
        .I4(q_reg_i_206_n_0),
        .O(q_reg_i_90_n_0));
  LUT5 #(
    .INIT(32'hF2F2F200)) 
    q_reg_i_91
       (.I0(pst_reg_cnt[234]),
        .I1(pst_reg_cnt[235]),
        .I2(q_reg_i_207_n_0),
        .I3(q_reg_i_208_n_0),
        .I4(q_reg_i_209_n_0),
        .O(q_reg_i_91_n_0));
  LUT6 #(
    .INIT(64'h5555555555555444)) 
    q_reg_i_92
       (.I0(q_reg_i_210_n_0),
        .I1(q_reg_i_198_n_0),
        .I2(q_reg_i_211_n_0),
        .I3(q_reg_i_212_n_0),
        .I4(q_reg_i_213_n_0),
        .I5(q_reg_i_214_n_0),
        .O(q_reg_i_92_n_0));
  LUT5 #(
    .INIT(32'h71777171)) 
    q_reg_i_93
       (.I0(q_reg_i_111_n_0),
        .I1(q_reg_i_110_n_0),
        .I2(q_reg_i_108_n_0),
        .I3(q_reg_i_109_n_0),
        .I4(q_reg_i_92_n_0),
        .O(q_reg_i_93_n_0));
  LUT6 #(
    .INIT(64'h000000750075FFFF)) 
    q_reg_i_94
       (.I0(q_reg_i_215_n_0),
        .I1(q_reg_i_216_n_0),
        .I2(q_reg_i_217_n_0),
        .I3(q_reg_i_218_n_0),
        .I4(q_reg_i_114_n_0),
        .I5(q_reg_i_116_n_0),
        .O(q_reg_i_94_n_0));
  LUT3 #(
    .INIT(8'hE8)) 
    q_reg_i_95
       (.I0(q_reg_i_113_n_0),
        .I1(q_reg_i_112_n_0),
        .I2(\q_reg[188]_0 ),
        .O(q_reg_i_95_n_0));
  LUT6 #(
    .INIT(64'h00000000FFFF1F11)) 
    q_reg_i_96
       (.I0(q_reg_i_219_n_0),
        .I1(q_reg_i_220_n_0),
        .I2(q_reg_i_221_n_0),
        .I3(q_reg_i_222_n_0),
        .I4(q_reg_i_223_n_0),
        .I5(q_reg_i_224_n_0),
        .O(q_reg_i_96_n_0));
  LUT6 #(
    .INIT(64'h5555555555010101)) 
    q_reg_i_97
       (.I0(q_reg_i_225_n_0),
        .I1(q_reg_i_210_n_0),
        .I2(q_reg_i_226_n_0),
        .I3(q_reg_i_227_n_0),
        .I4(q_reg_i_83_n_0),
        .I5(q_reg_i_228_n_0),
        .O(q_reg_i_97_n_0));
  LUT6 #(
    .INIT(64'h0004440455555555)) 
    q_reg_i_98
       (.I0(q_reg_i_140_n_0),
        .I1(q_reg_i_229_n_0),
        .I2(q_reg_i_230_n_0),
        .I3(q_reg_i_185_n_0),
        .I4(q_reg_i_231_n_0),
        .I5(q_reg_i_141_n_0),
        .O(q_reg_i_98_n_0));
  LUT3 #(
    .INIT(8'h45)) 
    q_reg_i_99
       (.I0(q_reg_i_232_n_0),
        .I1(pst_reg_cnt[271]),
        .I2(pst_reg_cnt[270]),
        .O(q_reg_i_99_n_0));
endmodule

(* ORIG_REF_NAME = "nbit_register" *) 
module nbit_register__parameterized1
   (D,
    max_cnt,
    A,
    C);
  output [23:0]D;
  input max_cnt;
  input [9:0]A;
  input [23:0]C;

  wire [9:0]A;
  wire [23:0]C;
  wire [23:0]D;
  wire max_cnt;
  wire NLW_q_reg_CARRYCASCOUT_UNCONNECTED;
  wire NLW_q_reg_MULTSIGNOUT_UNCONNECTED;
  wire NLW_q_reg_OVERFLOW_UNCONNECTED;
  wire NLW_q_reg_PATTERNBDETECT_UNCONNECTED;
  wire NLW_q_reg_PATTERNDETECT_UNCONNECTED;
  wire NLW_q_reg_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_q_reg_ACOUT_UNCONNECTED;
  wire [17:0]NLW_q_reg_BCOUT_UNCONNECTED;
  wire [3:0]NLW_q_reg_CARRYOUT_UNCONNECTED;
  wire [47:24]NLW_q_reg_P_UNCONNECTED;
  wire [47:0]NLW_q_reg_PCOUT_UNCONNECTED;

  (* METHODOLOGY_DRC_VIOS = "{SYNTH-12 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(0),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(1),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    q_reg
       (.A({A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A[9],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_q_reg_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b1,1'b1}),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b0,1'b0}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_q_reg_BCOUT_UNCONNECTED[17:0]),
        .C({C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C[23],C}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_q_reg_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_q_reg_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b1),
        .CLK(max_cnt),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_q_reg_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b1,1'b1,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_q_reg_OVERFLOW_UNCONNECTED),
        .P({NLW_q_reg_P_UNCONNECTED[47:24],D}),
        .PATTERNBDETECT(NLW_q_reg_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_q_reg_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_q_reg_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_q_reg_UNDERFLOW_UNCONNECTED));
endmodule

module tdc
   (pre_trig,
    pst_trig,
    D,
    JD_OBUF,
    CLK,
    \q_reg[0] ,
    \q_reg[0]_0 );
  output pre_trig;
  output pst_trig;
  output [23:0]D;
  input [1:0]JD_OBUF;
  input CLK;
  input \q_reg[0] ;
  input \q_reg[0]_0 ;

  wire [0:0]A;
  wire CLK;
  wire [23:0]D;
  wire [1:0]JD_OBUF;
  wire [3:0]count_reg;
  wire pre_tdl_n_0;
  wire pre_tdl_n_1;
  wire pre_tdl_n_10;
  wire pre_tdl_n_100;
  wire pre_tdl_n_101;
  wire pre_tdl_n_102;
  wire pre_tdl_n_103;
  wire pre_tdl_n_104;
  wire pre_tdl_n_105;
  wire pre_tdl_n_106;
  wire pre_tdl_n_107;
  wire pre_tdl_n_108;
  wire pre_tdl_n_109;
  wire pre_tdl_n_11;
  wire pre_tdl_n_110;
  wire pre_tdl_n_111;
  wire pre_tdl_n_112;
  wire pre_tdl_n_113;
  wire pre_tdl_n_114;
  wire pre_tdl_n_115;
  wire pre_tdl_n_116;
  wire pre_tdl_n_117;
  wire pre_tdl_n_118;
  wire pre_tdl_n_119;
  wire pre_tdl_n_12;
  wire pre_tdl_n_120;
  wire pre_tdl_n_121;
  wire pre_tdl_n_122;
  wire pre_tdl_n_123;
  wire pre_tdl_n_124;
  wire pre_tdl_n_125;
  wire pre_tdl_n_126;
  wire pre_tdl_n_127;
  wire pre_tdl_n_128;
  wire pre_tdl_n_129;
  wire pre_tdl_n_13;
  wire pre_tdl_n_130;
  wire pre_tdl_n_131;
  wire pre_tdl_n_132;
  wire pre_tdl_n_133;
  wire pre_tdl_n_134;
  wire pre_tdl_n_135;
  wire pre_tdl_n_136;
  wire pre_tdl_n_137;
  wire pre_tdl_n_138;
  wire pre_tdl_n_139;
  wire pre_tdl_n_14;
  wire pre_tdl_n_140;
  wire pre_tdl_n_141;
  wire pre_tdl_n_142;
  wire pre_tdl_n_143;
  wire pre_tdl_n_144;
  wire pre_tdl_n_145;
  wire pre_tdl_n_146;
  wire pre_tdl_n_147;
  wire pre_tdl_n_148;
  wire pre_tdl_n_149;
  wire pre_tdl_n_15;
  wire pre_tdl_n_150;
  wire pre_tdl_n_151;
  wire pre_tdl_n_152;
  wire pre_tdl_n_153;
  wire pre_tdl_n_154;
  wire pre_tdl_n_155;
  wire pre_tdl_n_156;
  wire pre_tdl_n_157;
  wire pre_tdl_n_158;
  wire pre_tdl_n_159;
  wire pre_tdl_n_16;
  wire pre_tdl_n_160;
  wire pre_tdl_n_161;
  wire pre_tdl_n_162;
  wire pre_tdl_n_163;
  wire pre_tdl_n_164;
  wire pre_tdl_n_165;
  wire pre_tdl_n_166;
  wire pre_tdl_n_167;
  wire pre_tdl_n_168;
  wire pre_tdl_n_169;
  wire pre_tdl_n_17;
  wire pre_tdl_n_170;
  wire pre_tdl_n_171;
  wire pre_tdl_n_172;
  wire pre_tdl_n_173;
  wire pre_tdl_n_174;
  wire pre_tdl_n_175;
  wire pre_tdl_n_176;
  wire pre_tdl_n_177;
  wire pre_tdl_n_178;
  wire pre_tdl_n_179;
  wire pre_tdl_n_18;
  wire pre_tdl_n_180;
  wire pre_tdl_n_181;
  wire pre_tdl_n_182;
  wire pre_tdl_n_183;
  wire pre_tdl_n_184;
  wire pre_tdl_n_185;
  wire pre_tdl_n_186;
  wire pre_tdl_n_187;
  wire pre_tdl_n_188;
  wire pre_tdl_n_189;
  wire pre_tdl_n_19;
  wire pre_tdl_n_190;
  wire pre_tdl_n_191;
  wire pre_tdl_n_192;
  wire pre_tdl_n_193;
  wire pre_tdl_n_194;
  wire pre_tdl_n_195;
  wire pre_tdl_n_196;
  wire pre_tdl_n_197;
  wire pre_tdl_n_198;
  wire pre_tdl_n_199;
  wire pre_tdl_n_2;
  wire pre_tdl_n_20;
  wire pre_tdl_n_200;
  wire pre_tdl_n_201;
  wire pre_tdl_n_202;
  wire pre_tdl_n_203;
  wire pre_tdl_n_204;
  wire pre_tdl_n_205;
  wire pre_tdl_n_206;
  wire pre_tdl_n_207;
  wire pre_tdl_n_208;
  wire pre_tdl_n_209;
  wire pre_tdl_n_21;
  wire pre_tdl_n_210;
  wire pre_tdl_n_211;
  wire pre_tdl_n_212;
  wire pre_tdl_n_213;
  wire pre_tdl_n_214;
  wire pre_tdl_n_215;
  wire pre_tdl_n_216;
  wire pre_tdl_n_217;
  wire pre_tdl_n_218;
  wire pre_tdl_n_219;
  wire pre_tdl_n_22;
  wire pre_tdl_n_220;
  wire pre_tdl_n_221;
  wire pre_tdl_n_222;
  wire pre_tdl_n_223;
  wire pre_tdl_n_224;
  wire pre_tdl_n_225;
  wire pre_tdl_n_226;
  wire pre_tdl_n_227;
  wire pre_tdl_n_228;
  wire pre_tdl_n_229;
  wire pre_tdl_n_23;
  wire pre_tdl_n_230;
  wire pre_tdl_n_231;
  wire pre_tdl_n_232;
  wire pre_tdl_n_233;
  wire pre_tdl_n_234;
  wire pre_tdl_n_235;
  wire pre_tdl_n_236;
  wire pre_tdl_n_237;
  wire pre_tdl_n_238;
  wire pre_tdl_n_239;
  wire pre_tdl_n_24;
  wire pre_tdl_n_240;
  wire pre_tdl_n_241;
  wire pre_tdl_n_242;
  wire pre_tdl_n_243;
  wire pre_tdl_n_244;
  wire pre_tdl_n_245;
  wire pre_tdl_n_246;
  wire pre_tdl_n_247;
  wire pre_tdl_n_248;
  wire pre_tdl_n_249;
  wire pre_tdl_n_25;
  wire pre_tdl_n_250;
  wire pre_tdl_n_251;
  wire pre_tdl_n_252;
  wire pre_tdl_n_253;
  wire pre_tdl_n_254;
  wire pre_tdl_n_255;
  wire pre_tdl_n_256;
  wire pre_tdl_n_257;
  wire pre_tdl_n_258;
  wire pre_tdl_n_259;
  wire pre_tdl_n_26;
  wire pre_tdl_n_260;
  wire pre_tdl_n_261;
  wire pre_tdl_n_262;
  wire pre_tdl_n_263;
  wire pre_tdl_n_264;
  wire pre_tdl_n_265;
  wire pre_tdl_n_266;
  wire pre_tdl_n_267;
  wire pre_tdl_n_268;
  wire pre_tdl_n_269;
  wire pre_tdl_n_27;
  wire pre_tdl_n_270;
  wire pre_tdl_n_271;
  wire pre_tdl_n_272;
  wire pre_tdl_n_273;
  wire pre_tdl_n_274;
  wire pre_tdl_n_275;
  wire pre_tdl_n_276;
  wire pre_tdl_n_277;
  wire pre_tdl_n_278;
  wire pre_tdl_n_279;
  wire pre_tdl_n_28;
  wire pre_tdl_n_280;
  wire pre_tdl_n_281;
  wire pre_tdl_n_282;
  wire pre_tdl_n_283;
  wire pre_tdl_n_284;
  wire pre_tdl_n_285;
  wire pre_tdl_n_286;
  wire pre_tdl_n_287;
  wire pre_tdl_n_288;
  wire pre_tdl_n_289;
  wire pre_tdl_n_29;
  wire pre_tdl_n_290;
  wire pre_tdl_n_291;
  wire pre_tdl_n_292;
  wire pre_tdl_n_293;
  wire pre_tdl_n_294;
  wire pre_tdl_n_295;
  wire pre_tdl_n_296;
  wire pre_tdl_n_297;
  wire pre_tdl_n_298;
  wire pre_tdl_n_299;
  wire pre_tdl_n_3;
  wire pre_tdl_n_30;
  wire pre_tdl_n_300;
  wire pre_tdl_n_301;
  wire pre_tdl_n_302;
  wire pre_tdl_n_303;
  wire pre_tdl_n_304;
  wire pre_tdl_n_305;
  wire pre_tdl_n_306;
  wire pre_tdl_n_307;
  wire pre_tdl_n_308;
  wire pre_tdl_n_309;
  wire pre_tdl_n_31;
  wire pre_tdl_n_310;
  wire pre_tdl_n_311;
  wire pre_tdl_n_312;
  wire pre_tdl_n_313;
  wire pre_tdl_n_314;
  wire pre_tdl_n_315;
  wire pre_tdl_n_316;
  wire pre_tdl_n_317;
  wire pre_tdl_n_318;
  wire pre_tdl_n_319;
  wire pre_tdl_n_32;
  wire pre_tdl_n_33;
  wire pre_tdl_n_34;
  wire pre_tdl_n_35;
  wire pre_tdl_n_36;
  wire pre_tdl_n_37;
  wire pre_tdl_n_38;
  wire pre_tdl_n_39;
  wire pre_tdl_n_4;
  wire pre_tdl_n_40;
  wire pre_tdl_n_41;
  wire pre_tdl_n_42;
  wire pre_tdl_n_43;
  wire pre_tdl_n_44;
  wire pre_tdl_n_45;
  wire pre_tdl_n_46;
  wire pre_tdl_n_47;
  wire pre_tdl_n_48;
  wire pre_tdl_n_49;
  wire pre_tdl_n_5;
  wire pre_tdl_n_50;
  wire pre_tdl_n_51;
  wire pre_tdl_n_52;
  wire pre_tdl_n_53;
  wire pre_tdl_n_54;
  wire pre_tdl_n_55;
  wire pre_tdl_n_56;
  wire pre_tdl_n_57;
  wire pre_tdl_n_58;
  wire pre_tdl_n_59;
  wire pre_tdl_n_6;
  wire pre_tdl_n_60;
  wire pre_tdl_n_61;
  wire pre_tdl_n_62;
  wire pre_tdl_n_63;
  wire pre_tdl_n_64;
  wire pre_tdl_n_65;
  wire pre_tdl_n_66;
  wire pre_tdl_n_67;
  wire pre_tdl_n_68;
  wire pre_tdl_n_69;
  wire pre_tdl_n_7;
  wire pre_tdl_n_70;
  wire pre_tdl_n_71;
  wire pre_tdl_n_72;
  wire pre_tdl_n_73;
  wire pre_tdl_n_74;
  wire pre_tdl_n_75;
  wire pre_tdl_n_76;
  wire pre_tdl_n_77;
  wire pre_tdl_n_78;
  wire pre_tdl_n_79;
  wire pre_tdl_n_8;
  wire pre_tdl_n_80;
  wire pre_tdl_n_81;
  wire pre_tdl_n_82;
  wire pre_tdl_n_83;
  wire pre_tdl_n_84;
  wire pre_tdl_n_85;
  wire pre_tdl_n_86;
  wire pre_tdl_n_87;
  wire pre_tdl_n_88;
  wire pre_tdl_n_89;
  wire pre_tdl_n_9;
  wire pre_tdl_n_90;
  wire pre_tdl_n_91;
  wire pre_tdl_n_92;
  wire pre_tdl_n_93;
  wire pre_tdl_n_94;
  wire pre_tdl_n_95;
  wire pre_tdl_n_96;
  wire pre_tdl_n_97;
  wire pre_tdl_n_98;
  wire pre_tdl_n_99;
  wire pre_trig;
  wire pst_tdl_n_0;
  wire pst_tdl_n_1;
  wire pst_tdl_n_10;
  wire pst_tdl_n_100;
  wire pst_tdl_n_101;
  wire pst_tdl_n_102;
  wire pst_tdl_n_103;
  wire pst_tdl_n_104;
  wire pst_tdl_n_105;
  wire pst_tdl_n_106;
  wire pst_tdl_n_107;
  wire pst_tdl_n_108;
  wire pst_tdl_n_109;
  wire pst_tdl_n_11;
  wire pst_tdl_n_110;
  wire pst_tdl_n_111;
  wire pst_tdl_n_112;
  wire pst_tdl_n_113;
  wire pst_tdl_n_114;
  wire pst_tdl_n_115;
  wire pst_tdl_n_116;
  wire pst_tdl_n_117;
  wire pst_tdl_n_118;
  wire pst_tdl_n_119;
  wire pst_tdl_n_12;
  wire pst_tdl_n_120;
  wire pst_tdl_n_121;
  wire pst_tdl_n_122;
  wire pst_tdl_n_123;
  wire pst_tdl_n_124;
  wire pst_tdl_n_125;
  wire pst_tdl_n_126;
  wire pst_tdl_n_127;
  wire pst_tdl_n_128;
  wire pst_tdl_n_129;
  wire pst_tdl_n_13;
  wire pst_tdl_n_130;
  wire pst_tdl_n_131;
  wire pst_tdl_n_132;
  wire pst_tdl_n_133;
  wire pst_tdl_n_134;
  wire pst_tdl_n_135;
  wire pst_tdl_n_136;
  wire pst_tdl_n_137;
  wire pst_tdl_n_138;
  wire pst_tdl_n_139;
  wire pst_tdl_n_14;
  wire pst_tdl_n_140;
  wire pst_tdl_n_141;
  wire pst_tdl_n_142;
  wire pst_tdl_n_143;
  wire pst_tdl_n_144;
  wire pst_tdl_n_145;
  wire pst_tdl_n_146;
  wire pst_tdl_n_147;
  wire pst_tdl_n_148;
  wire pst_tdl_n_149;
  wire pst_tdl_n_15;
  wire pst_tdl_n_150;
  wire pst_tdl_n_151;
  wire pst_tdl_n_152;
  wire pst_tdl_n_153;
  wire pst_tdl_n_154;
  wire pst_tdl_n_155;
  wire pst_tdl_n_156;
  wire pst_tdl_n_157;
  wire pst_tdl_n_158;
  wire pst_tdl_n_159;
  wire pst_tdl_n_16;
  wire pst_tdl_n_160;
  wire pst_tdl_n_161;
  wire pst_tdl_n_162;
  wire pst_tdl_n_163;
  wire pst_tdl_n_164;
  wire pst_tdl_n_165;
  wire pst_tdl_n_166;
  wire pst_tdl_n_167;
  wire pst_tdl_n_168;
  wire pst_tdl_n_169;
  wire pst_tdl_n_17;
  wire pst_tdl_n_170;
  wire pst_tdl_n_171;
  wire pst_tdl_n_172;
  wire pst_tdl_n_173;
  wire pst_tdl_n_174;
  wire pst_tdl_n_175;
  wire pst_tdl_n_176;
  wire pst_tdl_n_177;
  wire pst_tdl_n_178;
  wire pst_tdl_n_179;
  wire pst_tdl_n_18;
  wire pst_tdl_n_180;
  wire pst_tdl_n_181;
  wire pst_tdl_n_182;
  wire pst_tdl_n_183;
  wire pst_tdl_n_184;
  wire pst_tdl_n_185;
  wire pst_tdl_n_186;
  wire pst_tdl_n_187;
  wire pst_tdl_n_188;
  wire pst_tdl_n_189;
  wire pst_tdl_n_19;
  wire pst_tdl_n_190;
  wire pst_tdl_n_191;
  wire pst_tdl_n_192;
  wire pst_tdl_n_193;
  wire pst_tdl_n_194;
  wire pst_tdl_n_195;
  wire pst_tdl_n_196;
  wire pst_tdl_n_197;
  wire pst_tdl_n_198;
  wire pst_tdl_n_199;
  wire pst_tdl_n_2;
  wire pst_tdl_n_20;
  wire pst_tdl_n_200;
  wire pst_tdl_n_201;
  wire pst_tdl_n_202;
  wire pst_tdl_n_203;
  wire pst_tdl_n_204;
  wire pst_tdl_n_205;
  wire pst_tdl_n_206;
  wire pst_tdl_n_207;
  wire pst_tdl_n_208;
  wire pst_tdl_n_209;
  wire pst_tdl_n_21;
  wire pst_tdl_n_210;
  wire pst_tdl_n_211;
  wire pst_tdl_n_212;
  wire pst_tdl_n_213;
  wire pst_tdl_n_214;
  wire pst_tdl_n_215;
  wire pst_tdl_n_216;
  wire pst_tdl_n_217;
  wire pst_tdl_n_218;
  wire pst_tdl_n_219;
  wire pst_tdl_n_22;
  wire pst_tdl_n_220;
  wire pst_tdl_n_221;
  wire pst_tdl_n_222;
  wire pst_tdl_n_223;
  wire pst_tdl_n_224;
  wire pst_tdl_n_225;
  wire pst_tdl_n_226;
  wire pst_tdl_n_227;
  wire pst_tdl_n_228;
  wire pst_tdl_n_229;
  wire pst_tdl_n_23;
  wire pst_tdl_n_230;
  wire pst_tdl_n_231;
  wire pst_tdl_n_232;
  wire pst_tdl_n_233;
  wire pst_tdl_n_234;
  wire pst_tdl_n_235;
  wire pst_tdl_n_236;
  wire pst_tdl_n_237;
  wire pst_tdl_n_238;
  wire pst_tdl_n_239;
  wire pst_tdl_n_24;
  wire pst_tdl_n_240;
  wire pst_tdl_n_241;
  wire pst_tdl_n_242;
  wire pst_tdl_n_243;
  wire pst_tdl_n_244;
  wire pst_tdl_n_245;
  wire pst_tdl_n_246;
  wire pst_tdl_n_247;
  wire pst_tdl_n_248;
  wire pst_tdl_n_249;
  wire pst_tdl_n_25;
  wire pst_tdl_n_250;
  wire pst_tdl_n_251;
  wire pst_tdl_n_252;
  wire pst_tdl_n_253;
  wire pst_tdl_n_254;
  wire pst_tdl_n_255;
  wire pst_tdl_n_256;
  wire pst_tdl_n_257;
  wire pst_tdl_n_258;
  wire pst_tdl_n_259;
  wire pst_tdl_n_26;
  wire pst_tdl_n_260;
  wire pst_tdl_n_261;
  wire pst_tdl_n_262;
  wire pst_tdl_n_263;
  wire pst_tdl_n_264;
  wire pst_tdl_n_265;
  wire pst_tdl_n_266;
  wire pst_tdl_n_267;
  wire pst_tdl_n_268;
  wire pst_tdl_n_269;
  wire pst_tdl_n_27;
  wire pst_tdl_n_270;
  wire pst_tdl_n_271;
  wire pst_tdl_n_272;
  wire pst_tdl_n_273;
  wire pst_tdl_n_274;
  wire pst_tdl_n_275;
  wire pst_tdl_n_276;
  wire pst_tdl_n_277;
  wire pst_tdl_n_278;
  wire pst_tdl_n_279;
  wire pst_tdl_n_28;
  wire pst_tdl_n_280;
  wire pst_tdl_n_281;
  wire pst_tdl_n_282;
  wire pst_tdl_n_283;
  wire pst_tdl_n_284;
  wire pst_tdl_n_285;
  wire pst_tdl_n_286;
  wire pst_tdl_n_287;
  wire pst_tdl_n_288;
  wire pst_tdl_n_289;
  wire pst_tdl_n_29;
  wire pst_tdl_n_290;
  wire pst_tdl_n_291;
  wire pst_tdl_n_292;
  wire pst_tdl_n_293;
  wire pst_tdl_n_294;
  wire pst_tdl_n_295;
  wire pst_tdl_n_296;
  wire pst_tdl_n_297;
  wire pst_tdl_n_298;
  wire pst_tdl_n_299;
  wire pst_tdl_n_3;
  wire pst_tdl_n_30;
  wire pst_tdl_n_300;
  wire pst_tdl_n_301;
  wire pst_tdl_n_302;
  wire pst_tdl_n_303;
  wire pst_tdl_n_304;
  wire pst_tdl_n_305;
  wire pst_tdl_n_306;
  wire pst_tdl_n_307;
  wire pst_tdl_n_308;
  wire pst_tdl_n_309;
  wire pst_tdl_n_31;
  wire pst_tdl_n_310;
  wire pst_tdl_n_311;
  wire pst_tdl_n_312;
  wire pst_tdl_n_313;
  wire pst_tdl_n_314;
  wire pst_tdl_n_315;
  wire pst_tdl_n_316;
  wire pst_tdl_n_317;
  wire pst_tdl_n_318;
  wire pst_tdl_n_319;
  wire pst_tdl_n_32;
  wire pst_tdl_n_33;
  wire pst_tdl_n_34;
  wire pst_tdl_n_35;
  wire pst_tdl_n_36;
  wire pst_tdl_n_37;
  wire pst_tdl_n_38;
  wire pst_tdl_n_39;
  wire pst_tdl_n_4;
  wire pst_tdl_n_40;
  wire pst_tdl_n_41;
  wire pst_tdl_n_42;
  wire pst_tdl_n_43;
  wire pst_tdl_n_44;
  wire pst_tdl_n_45;
  wire pst_tdl_n_46;
  wire pst_tdl_n_47;
  wire pst_tdl_n_48;
  wire pst_tdl_n_49;
  wire pst_tdl_n_5;
  wire pst_tdl_n_50;
  wire pst_tdl_n_51;
  wire pst_tdl_n_52;
  wire pst_tdl_n_53;
  wire pst_tdl_n_54;
  wire pst_tdl_n_55;
  wire pst_tdl_n_56;
  wire pst_tdl_n_57;
  wire pst_tdl_n_58;
  wire pst_tdl_n_59;
  wire pst_tdl_n_6;
  wire pst_tdl_n_60;
  wire pst_tdl_n_61;
  wire pst_tdl_n_62;
  wire pst_tdl_n_63;
  wire pst_tdl_n_64;
  wire pst_tdl_n_65;
  wire pst_tdl_n_66;
  wire pst_tdl_n_67;
  wire pst_tdl_n_68;
  wire pst_tdl_n_69;
  wire pst_tdl_n_7;
  wire pst_tdl_n_70;
  wire pst_tdl_n_71;
  wire pst_tdl_n_72;
  wire pst_tdl_n_73;
  wire pst_tdl_n_74;
  wire pst_tdl_n_75;
  wire pst_tdl_n_76;
  wire pst_tdl_n_77;
  wire pst_tdl_n_78;
  wire pst_tdl_n_79;
  wire pst_tdl_n_8;
  wire pst_tdl_n_80;
  wire pst_tdl_n_81;
  wire pst_tdl_n_82;
  wire pst_tdl_n_83;
  wire pst_tdl_n_84;
  wire pst_tdl_n_85;
  wire pst_tdl_n_86;
  wire pst_tdl_n_87;
  wire pst_tdl_n_88;
  wire pst_tdl_n_89;
  wire pst_tdl_n_9;
  wire pst_tdl_n_90;
  wire pst_tdl_n_91;
  wire pst_tdl_n_92;
  wire pst_tdl_n_93;
  wire pst_tdl_n_94;
  wire pst_tdl_n_95;
  wire pst_tdl_n_96;
  wire pst_tdl_n_97;
  wire pst_tdl_n_98;
  wire pst_tdl_n_99;
  wire pst_trig;
  wire \q_reg[0] ;
  wire \q_reg[0]_0 ;

  calc_unit calc_unit
       (.A(A),
        .CLK(CLK),
        .D(D),
        .JD_OBUF(JD_OBUF),
        .Q(count_reg),
        .pre_trig(pre_trig),
        .pst_trig(pst_trig),
        .\q_reg[0] (\q_reg[0] ),
        .\q_reg[0]_0 (\q_reg[0]_0 ),
        .\q_reg[319] ({pre_tdl_n_0,pre_tdl_n_1,pre_tdl_n_2,pre_tdl_n_3,pre_tdl_n_4,pre_tdl_n_5,pre_tdl_n_6,pre_tdl_n_7,pre_tdl_n_8,pre_tdl_n_9,pre_tdl_n_10,pre_tdl_n_11,pre_tdl_n_12,pre_tdl_n_13,pre_tdl_n_14,pre_tdl_n_15,pre_tdl_n_16,pre_tdl_n_17,pre_tdl_n_18,pre_tdl_n_19,pre_tdl_n_20,pre_tdl_n_21,pre_tdl_n_22,pre_tdl_n_23,pre_tdl_n_24,pre_tdl_n_25,pre_tdl_n_26,pre_tdl_n_27,pre_tdl_n_28,pre_tdl_n_29,pre_tdl_n_30,pre_tdl_n_31,pre_tdl_n_32,pre_tdl_n_33,pre_tdl_n_34,pre_tdl_n_35,pre_tdl_n_36,pre_tdl_n_37,pre_tdl_n_38,pre_tdl_n_39,pre_tdl_n_40,pre_tdl_n_41,pre_tdl_n_42,pre_tdl_n_43,pre_tdl_n_44,pre_tdl_n_45,pre_tdl_n_46,pre_tdl_n_47,pre_tdl_n_48,pre_tdl_n_49,pre_tdl_n_50,pre_tdl_n_51,pre_tdl_n_52,pre_tdl_n_53,pre_tdl_n_54,pre_tdl_n_55,pre_tdl_n_56,pre_tdl_n_57,pre_tdl_n_58,pre_tdl_n_59,pre_tdl_n_60,pre_tdl_n_61,pre_tdl_n_62,pre_tdl_n_63,pre_tdl_n_64,pre_tdl_n_65,pre_tdl_n_66,pre_tdl_n_67,pre_tdl_n_68,pre_tdl_n_69,pre_tdl_n_70,pre_tdl_n_71,pre_tdl_n_72,pre_tdl_n_73,pre_tdl_n_74,pre_tdl_n_75,pre_tdl_n_76,pre_tdl_n_77,pre_tdl_n_78,pre_tdl_n_79,pre_tdl_n_80,pre_tdl_n_81,pre_tdl_n_82,pre_tdl_n_83,pre_tdl_n_84,pre_tdl_n_85,pre_tdl_n_86,pre_tdl_n_87,pre_tdl_n_88,pre_tdl_n_89,pre_tdl_n_90,pre_tdl_n_91,pre_tdl_n_92,pre_tdl_n_93,pre_tdl_n_94,pre_tdl_n_95,pre_tdl_n_96,pre_tdl_n_97,pre_tdl_n_98,pre_tdl_n_99,pre_tdl_n_100,pre_tdl_n_101,pre_tdl_n_102,pre_tdl_n_103,pre_tdl_n_104,pre_tdl_n_105,pre_tdl_n_106,pre_tdl_n_107,pre_tdl_n_108,pre_tdl_n_109,pre_tdl_n_110,pre_tdl_n_111,pre_tdl_n_112,pre_tdl_n_113,pre_tdl_n_114,pre_tdl_n_115,pre_tdl_n_116,pre_tdl_n_117,pre_tdl_n_118,pre_tdl_n_119,pre_tdl_n_120,pre_tdl_n_121,pre_tdl_n_122,pre_tdl_n_123,pre_tdl_n_124,pre_tdl_n_125,pre_tdl_n_126,pre_tdl_n_127,pre_tdl_n_128,pre_tdl_n_129,pre_tdl_n_130,pre_tdl_n_131,pre_tdl_n_132,pre_tdl_n_133,pre_tdl_n_134,pre_tdl_n_135,pre_tdl_n_136,pre_tdl_n_137,pre_tdl_n_138,pre_tdl_n_139,pre_tdl_n_140,pre_tdl_n_141,pre_tdl_n_142,pre_tdl_n_143,pre_tdl_n_144,pre_tdl_n_145,pre_tdl_n_146,pre_tdl_n_147,pre_tdl_n_148,pre_tdl_n_149,pre_tdl_n_150,pre_tdl_n_151,pre_tdl_n_152,pre_tdl_n_153,pre_tdl_n_154,pre_tdl_n_155,pre_tdl_n_156,pre_tdl_n_157,pre_tdl_n_158,pre_tdl_n_159,pre_tdl_n_160,pre_tdl_n_161,pre_tdl_n_162,pre_tdl_n_163,pre_tdl_n_164,pre_tdl_n_165,pre_tdl_n_166,pre_tdl_n_167,pre_tdl_n_168,pre_tdl_n_169,pre_tdl_n_170,pre_tdl_n_171,pre_tdl_n_172,pre_tdl_n_173,pre_tdl_n_174,pre_tdl_n_175,pre_tdl_n_176,pre_tdl_n_177,pre_tdl_n_178,pre_tdl_n_179,pre_tdl_n_180,pre_tdl_n_181,pre_tdl_n_182,pre_tdl_n_183,pre_tdl_n_184,pre_tdl_n_185,pre_tdl_n_186,pre_tdl_n_187,pre_tdl_n_188,pre_tdl_n_189,pre_tdl_n_190,pre_tdl_n_191,pre_tdl_n_192,pre_tdl_n_193,pre_tdl_n_194,pre_tdl_n_195,pre_tdl_n_196,pre_tdl_n_197,pre_tdl_n_198,pre_tdl_n_199,pre_tdl_n_200,pre_tdl_n_201,pre_tdl_n_202,pre_tdl_n_203,pre_tdl_n_204,pre_tdl_n_205,pre_tdl_n_206,pre_tdl_n_207,pre_tdl_n_208,pre_tdl_n_209,pre_tdl_n_210,pre_tdl_n_211,pre_tdl_n_212,pre_tdl_n_213,pre_tdl_n_214,pre_tdl_n_215,pre_tdl_n_216,pre_tdl_n_217,pre_tdl_n_218,pre_tdl_n_219,pre_tdl_n_220,pre_tdl_n_221,pre_tdl_n_222,pre_tdl_n_223,pre_tdl_n_224,pre_tdl_n_225,pre_tdl_n_226,pre_tdl_n_227,pre_tdl_n_228,pre_tdl_n_229,pre_tdl_n_230,pre_tdl_n_231,pre_tdl_n_232,pre_tdl_n_233,pre_tdl_n_234,pre_tdl_n_235,pre_tdl_n_236,pre_tdl_n_237,pre_tdl_n_238,pre_tdl_n_239,pre_tdl_n_240,pre_tdl_n_241,pre_tdl_n_242,pre_tdl_n_243,pre_tdl_n_244,pre_tdl_n_245,pre_tdl_n_246,pre_tdl_n_247,pre_tdl_n_248,pre_tdl_n_249,pre_tdl_n_250,pre_tdl_n_251,pre_tdl_n_252,pre_tdl_n_253,pre_tdl_n_254,pre_tdl_n_255,pre_tdl_n_256,pre_tdl_n_257,pre_tdl_n_258,pre_tdl_n_259,pre_tdl_n_260,pre_tdl_n_261,pre_tdl_n_262,pre_tdl_n_263,pre_tdl_n_264,pre_tdl_n_265,pre_tdl_n_266,pre_tdl_n_267,pre_tdl_n_268,pre_tdl_n_269,pre_tdl_n_270,pre_tdl_n_271,pre_tdl_n_272,pre_tdl_n_273,pre_tdl_n_274,pre_tdl_n_275,pre_tdl_n_276,pre_tdl_n_277,pre_tdl_n_278,pre_tdl_n_279,pre_tdl_n_280,pre_tdl_n_281,pre_tdl_n_282,pre_tdl_n_283,pre_tdl_n_284,pre_tdl_n_285,pre_tdl_n_286,pre_tdl_n_287,pre_tdl_n_288,pre_tdl_n_289,pre_tdl_n_290,pre_tdl_n_291,pre_tdl_n_292,pre_tdl_n_293,pre_tdl_n_294,pre_tdl_n_295,pre_tdl_n_296,pre_tdl_n_297,pre_tdl_n_298,pre_tdl_n_299,pre_tdl_n_300,pre_tdl_n_301,pre_tdl_n_302,pre_tdl_n_303,pre_tdl_n_304,pre_tdl_n_305,pre_tdl_n_306,pre_tdl_n_307,pre_tdl_n_308,pre_tdl_n_309,pre_tdl_n_310,pre_tdl_n_311,pre_tdl_n_312,pre_tdl_n_313,pre_tdl_n_314,pre_tdl_n_315,pre_tdl_n_316,pre_tdl_n_317,pre_tdl_n_318,pre_tdl_n_319}),
        .\q_reg[319]_0 ({pst_tdl_n_0,pst_tdl_n_1,pst_tdl_n_2,pst_tdl_n_3,pst_tdl_n_4,pst_tdl_n_5,pst_tdl_n_6,pst_tdl_n_7,pst_tdl_n_8,pst_tdl_n_9,pst_tdl_n_10,pst_tdl_n_11,pst_tdl_n_12,pst_tdl_n_13,pst_tdl_n_14,pst_tdl_n_15,pst_tdl_n_16,pst_tdl_n_17,pst_tdl_n_18,pst_tdl_n_19,pst_tdl_n_20,pst_tdl_n_21,pst_tdl_n_22,pst_tdl_n_23,pst_tdl_n_24,pst_tdl_n_25,pst_tdl_n_26,pst_tdl_n_27,pst_tdl_n_28,pst_tdl_n_29,pst_tdl_n_30,pst_tdl_n_31,pst_tdl_n_32,pst_tdl_n_33,pst_tdl_n_34,pst_tdl_n_35,pst_tdl_n_36,pst_tdl_n_37,pst_tdl_n_38,pst_tdl_n_39,pst_tdl_n_40,pst_tdl_n_41,pst_tdl_n_42,pst_tdl_n_43,pst_tdl_n_44,pst_tdl_n_45,pst_tdl_n_46,pst_tdl_n_47,pst_tdl_n_48,pst_tdl_n_49,pst_tdl_n_50,pst_tdl_n_51,pst_tdl_n_52,pst_tdl_n_53,pst_tdl_n_54,pst_tdl_n_55,pst_tdl_n_56,pst_tdl_n_57,pst_tdl_n_58,pst_tdl_n_59,pst_tdl_n_60,pst_tdl_n_61,pst_tdl_n_62,pst_tdl_n_63,pst_tdl_n_64,pst_tdl_n_65,pst_tdl_n_66,pst_tdl_n_67,pst_tdl_n_68,pst_tdl_n_69,pst_tdl_n_70,pst_tdl_n_71,pst_tdl_n_72,pst_tdl_n_73,pst_tdl_n_74,pst_tdl_n_75,pst_tdl_n_76,pst_tdl_n_77,pst_tdl_n_78,pst_tdl_n_79,pst_tdl_n_80,pst_tdl_n_81,pst_tdl_n_82,pst_tdl_n_83,pst_tdl_n_84,pst_tdl_n_85,pst_tdl_n_86,pst_tdl_n_87,pst_tdl_n_88,pst_tdl_n_89,pst_tdl_n_90,pst_tdl_n_91,pst_tdl_n_92,pst_tdl_n_93,pst_tdl_n_94,pst_tdl_n_95,pst_tdl_n_96,pst_tdl_n_97,pst_tdl_n_98,pst_tdl_n_99,pst_tdl_n_100,pst_tdl_n_101,pst_tdl_n_102,pst_tdl_n_103,pst_tdl_n_104,pst_tdl_n_105,pst_tdl_n_106,pst_tdl_n_107,pst_tdl_n_108,pst_tdl_n_109,pst_tdl_n_110,pst_tdl_n_111,pst_tdl_n_112,pst_tdl_n_113,pst_tdl_n_114,pst_tdl_n_115,pst_tdl_n_116,pst_tdl_n_117,pst_tdl_n_118,pst_tdl_n_119,pst_tdl_n_120,pst_tdl_n_121,pst_tdl_n_122,pst_tdl_n_123,pst_tdl_n_124,pst_tdl_n_125,pst_tdl_n_126,pst_tdl_n_127,pst_tdl_n_128,pst_tdl_n_129,pst_tdl_n_130,pst_tdl_n_131,pst_tdl_n_132,pst_tdl_n_133,pst_tdl_n_134,pst_tdl_n_135,pst_tdl_n_136,pst_tdl_n_137,pst_tdl_n_138,pst_tdl_n_139,pst_tdl_n_140,pst_tdl_n_141,pst_tdl_n_142,pst_tdl_n_143,pst_tdl_n_144,pst_tdl_n_145,pst_tdl_n_146,pst_tdl_n_147,pst_tdl_n_148,pst_tdl_n_149,pst_tdl_n_150,pst_tdl_n_151,pst_tdl_n_152,pst_tdl_n_153,pst_tdl_n_154,pst_tdl_n_155,pst_tdl_n_156,pst_tdl_n_157,pst_tdl_n_158,pst_tdl_n_159,pst_tdl_n_160,pst_tdl_n_161,pst_tdl_n_162,pst_tdl_n_163,pst_tdl_n_164,pst_tdl_n_165,pst_tdl_n_166,pst_tdl_n_167,pst_tdl_n_168,pst_tdl_n_169,pst_tdl_n_170,pst_tdl_n_171,pst_tdl_n_172,pst_tdl_n_173,pst_tdl_n_174,pst_tdl_n_175,pst_tdl_n_176,pst_tdl_n_177,pst_tdl_n_178,pst_tdl_n_179,pst_tdl_n_180,pst_tdl_n_181,pst_tdl_n_182,pst_tdl_n_183,pst_tdl_n_184,pst_tdl_n_185,pst_tdl_n_186,pst_tdl_n_187,pst_tdl_n_188,pst_tdl_n_189,pst_tdl_n_190,pst_tdl_n_191,pst_tdl_n_192,pst_tdl_n_193,pst_tdl_n_194,pst_tdl_n_195,pst_tdl_n_196,pst_tdl_n_197,pst_tdl_n_198,pst_tdl_n_199,pst_tdl_n_200,pst_tdl_n_201,pst_tdl_n_202,pst_tdl_n_203,pst_tdl_n_204,pst_tdl_n_205,pst_tdl_n_206,pst_tdl_n_207,pst_tdl_n_208,pst_tdl_n_209,pst_tdl_n_210,pst_tdl_n_211,pst_tdl_n_212,pst_tdl_n_213,pst_tdl_n_214,pst_tdl_n_215,pst_tdl_n_216,pst_tdl_n_217,pst_tdl_n_218,pst_tdl_n_219,pst_tdl_n_220,pst_tdl_n_221,pst_tdl_n_222,pst_tdl_n_223,pst_tdl_n_224,pst_tdl_n_225,pst_tdl_n_226,pst_tdl_n_227,pst_tdl_n_228,pst_tdl_n_229,pst_tdl_n_230,pst_tdl_n_231,pst_tdl_n_232,pst_tdl_n_233,pst_tdl_n_234,pst_tdl_n_235,pst_tdl_n_236,pst_tdl_n_237,pst_tdl_n_238,pst_tdl_n_239,pst_tdl_n_240,pst_tdl_n_241,pst_tdl_n_242,pst_tdl_n_243,pst_tdl_n_244,pst_tdl_n_245,pst_tdl_n_246,pst_tdl_n_247,pst_tdl_n_248,pst_tdl_n_249,pst_tdl_n_250,pst_tdl_n_251,pst_tdl_n_252,pst_tdl_n_253,pst_tdl_n_254,pst_tdl_n_255,pst_tdl_n_256,pst_tdl_n_257,pst_tdl_n_258,pst_tdl_n_259,pst_tdl_n_260,pst_tdl_n_261,pst_tdl_n_262,pst_tdl_n_263,pst_tdl_n_264,pst_tdl_n_265,pst_tdl_n_266,pst_tdl_n_267,pst_tdl_n_268,pst_tdl_n_269,pst_tdl_n_270,pst_tdl_n_271,pst_tdl_n_272,pst_tdl_n_273,pst_tdl_n_274,pst_tdl_n_275,pst_tdl_n_276,pst_tdl_n_277,pst_tdl_n_278,pst_tdl_n_279,pst_tdl_n_280,pst_tdl_n_281,pst_tdl_n_282,pst_tdl_n_283,pst_tdl_n_284,pst_tdl_n_285,pst_tdl_n_286,pst_tdl_n_287,pst_tdl_n_288,pst_tdl_n_289,pst_tdl_n_290,pst_tdl_n_291,pst_tdl_n_292,pst_tdl_n_293,pst_tdl_n_294,pst_tdl_n_295,pst_tdl_n_296,pst_tdl_n_297,pst_tdl_n_298,pst_tdl_n_299,pst_tdl_n_300,pst_tdl_n_301,pst_tdl_n_302,pst_tdl_n_303,pst_tdl_n_304,pst_tdl_n_305,pst_tdl_n_306,pst_tdl_n_307,pst_tdl_n_308,pst_tdl_n_309,pst_tdl_n_310,pst_tdl_n_311,pst_tdl_n_312,pst_tdl_n_313,pst_tdl_n_314,pst_tdl_n_315,pst_tdl_n_316,pst_tdl_n_317,pst_tdl_n_318,pst_tdl_n_319}));
  binary_counter crs_cnt
       (.A(A),
        .CLK(CLK),
        .JD_OBUF(JD_OBUF),
        .Q(count_reg));
  n4bit_carry_chain pre_tdl
       (.CLK(CLK),
        .JD_OBUF(JD_OBUF[0]),
        .\q_reg[0] ({pre_tdl_n_0,pre_tdl_n_1,pre_tdl_n_2,pre_tdl_n_3,pre_tdl_n_4,pre_tdl_n_5,pre_tdl_n_6,pre_tdl_n_7,pre_tdl_n_8,pre_tdl_n_9,pre_tdl_n_10,pre_tdl_n_11,pre_tdl_n_12,pre_tdl_n_13,pre_tdl_n_14,pre_tdl_n_15,pre_tdl_n_16,pre_tdl_n_17,pre_tdl_n_18,pre_tdl_n_19,pre_tdl_n_20,pre_tdl_n_21,pre_tdl_n_22,pre_tdl_n_23,pre_tdl_n_24,pre_tdl_n_25,pre_tdl_n_26,pre_tdl_n_27,pre_tdl_n_28,pre_tdl_n_29,pre_tdl_n_30,pre_tdl_n_31,pre_tdl_n_32,pre_tdl_n_33,pre_tdl_n_34,pre_tdl_n_35,pre_tdl_n_36,pre_tdl_n_37,pre_tdl_n_38,pre_tdl_n_39,pre_tdl_n_40,pre_tdl_n_41,pre_tdl_n_42,pre_tdl_n_43,pre_tdl_n_44,pre_tdl_n_45,pre_tdl_n_46,pre_tdl_n_47,pre_tdl_n_48,pre_tdl_n_49,pre_tdl_n_50,pre_tdl_n_51,pre_tdl_n_52,pre_tdl_n_53,pre_tdl_n_54,pre_tdl_n_55,pre_tdl_n_56,pre_tdl_n_57,pre_tdl_n_58,pre_tdl_n_59,pre_tdl_n_60,pre_tdl_n_61,pre_tdl_n_62,pre_tdl_n_63,pre_tdl_n_64,pre_tdl_n_65,pre_tdl_n_66,pre_tdl_n_67,pre_tdl_n_68,pre_tdl_n_69,pre_tdl_n_70,pre_tdl_n_71,pre_tdl_n_72,pre_tdl_n_73,pre_tdl_n_74,pre_tdl_n_75,pre_tdl_n_76,pre_tdl_n_77,pre_tdl_n_78,pre_tdl_n_79,pre_tdl_n_80,pre_tdl_n_81,pre_tdl_n_82,pre_tdl_n_83,pre_tdl_n_84,pre_tdl_n_85,pre_tdl_n_86,pre_tdl_n_87,pre_tdl_n_88,pre_tdl_n_89,pre_tdl_n_90,pre_tdl_n_91,pre_tdl_n_92,pre_tdl_n_93,pre_tdl_n_94,pre_tdl_n_95,pre_tdl_n_96,pre_tdl_n_97,pre_tdl_n_98,pre_tdl_n_99,pre_tdl_n_100,pre_tdl_n_101,pre_tdl_n_102,pre_tdl_n_103,pre_tdl_n_104,pre_tdl_n_105,pre_tdl_n_106,pre_tdl_n_107,pre_tdl_n_108,pre_tdl_n_109,pre_tdl_n_110,pre_tdl_n_111,pre_tdl_n_112,pre_tdl_n_113,pre_tdl_n_114,pre_tdl_n_115,pre_tdl_n_116,pre_tdl_n_117,pre_tdl_n_118,pre_tdl_n_119,pre_tdl_n_120,pre_tdl_n_121,pre_tdl_n_122,pre_tdl_n_123,pre_tdl_n_124,pre_tdl_n_125,pre_tdl_n_126,pre_tdl_n_127,pre_tdl_n_128,pre_tdl_n_129,pre_tdl_n_130,pre_tdl_n_131,pre_tdl_n_132,pre_tdl_n_133,pre_tdl_n_134,pre_tdl_n_135,pre_tdl_n_136,pre_tdl_n_137,pre_tdl_n_138,pre_tdl_n_139,pre_tdl_n_140,pre_tdl_n_141,pre_tdl_n_142,pre_tdl_n_143,pre_tdl_n_144,pre_tdl_n_145,pre_tdl_n_146,pre_tdl_n_147,pre_tdl_n_148,pre_tdl_n_149,pre_tdl_n_150,pre_tdl_n_151,pre_tdl_n_152,pre_tdl_n_153,pre_tdl_n_154,pre_tdl_n_155,pre_tdl_n_156,pre_tdl_n_157,pre_tdl_n_158,pre_tdl_n_159,pre_tdl_n_160,pre_tdl_n_161,pre_tdl_n_162,pre_tdl_n_163,pre_tdl_n_164,pre_tdl_n_165,pre_tdl_n_166,pre_tdl_n_167,pre_tdl_n_168,pre_tdl_n_169,pre_tdl_n_170,pre_tdl_n_171,pre_tdl_n_172,pre_tdl_n_173,pre_tdl_n_174,pre_tdl_n_175,pre_tdl_n_176,pre_tdl_n_177,pre_tdl_n_178,pre_tdl_n_179,pre_tdl_n_180,pre_tdl_n_181,pre_tdl_n_182,pre_tdl_n_183,pre_tdl_n_184,pre_tdl_n_185,pre_tdl_n_186,pre_tdl_n_187,pre_tdl_n_188,pre_tdl_n_189,pre_tdl_n_190,pre_tdl_n_191,pre_tdl_n_192,pre_tdl_n_193,pre_tdl_n_194,pre_tdl_n_195,pre_tdl_n_196,pre_tdl_n_197,pre_tdl_n_198,pre_tdl_n_199,pre_tdl_n_200,pre_tdl_n_201,pre_tdl_n_202,pre_tdl_n_203,pre_tdl_n_204,pre_tdl_n_205,pre_tdl_n_206,pre_tdl_n_207,pre_tdl_n_208,pre_tdl_n_209,pre_tdl_n_210,pre_tdl_n_211,pre_tdl_n_212,pre_tdl_n_213,pre_tdl_n_214,pre_tdl_n_215,pre_tdl_n_216,pre_tdl_n_217,pre_tdl_n_218,pre_tdl_n_219,pre_tdl_n_220,pre_tdl_n_221,pre_tdl_n_222,pre_tdl_n_223,pre_tdl_n_224,pre_tdl_n_225,pre_tdl_n_226,pre_tdl_n_227,pre_tdl_n_228,pre_tdl_n_229,pre_tdl_n_230,pre_tdl_n_231,pre_tdl_n_232,pre_tdl_n_233,pre_tdl_n_234,pre_tdl_n_235,pre_tdl_n_236,pre_tdl_n_237,pre_tdl_n_238,pre_tdl_n_239,pre_tdl_n_240,pre_tdl_n_241,pre_tdl_n_242,pre_tdl_n_243,pre_tdl_n_244,pre_tdl_n_245,pre_tdl_n_246,pre_tdl_n_247,pre_tdl_n_248,pre_tdl_n_249,pre_tdl_n_250,pre_tdl_n_251,pre_tdl_n_252,pre_tdl_n_253,pre_tdl_n_254,pre_tdl_n_255,pre_tdl_n_256,pre_tdl_n_257,pre_tdl_n_258,pre_tdl_n_259,pre_tdl_n_260,pre_tdl_n_261,pre_tdl_n_262,pre_tdl_n_263,pre_tdl_n_264,pre_tdl_n_265,pre_tdl_n_266,pre_tdl_n_267,pre_tdl_n_268,pre_tdl_n_269,pre_tdl_n_270,pre_tdl_n_271,pre_tdl_n_272,pre_tdl_n_273,pre_tdl_n_274,pre_tdl_n_275,pre_tdl_n_276,pre_tdl_n_277,pre_tdl_n_278,pre_tdl_n_279,pre_tdl_n_280,pre_tdl_n_281,pre_tdl_n_282,pre_tdl_n_283,pre_tdl_n_284,pre_tdl_n_285,pre_tdl_n_286,pre_tdl_n_287,pre_tdl_n_288,pre_tdl_n_289,pre_tdl_n_290,pre_tdl_n_291,pre_tdl_n_292,pre_tdl_n_293,pre_tdl_n_294,pre_tdl_n_295,pre_tdl_n_296,pre_tdl_n_297,pre_tdl_n_298,pre_tdl_n_299,pre_tdl_n_300,pre_tdl_n_301,pre_tdl_n_302,pre_tdl_n_303,pre_tdl_n_304,pre_tdl_n_305,pre_tdl_n_306,pre_tdl_n_307,pre_tdl_n_308,pre_tdl_n_309,pre_tdl_n_310,pre_tdl_n_311,pre_tdl_n_312,pre_tdl_n_313,pre_tdl_n_314,pre_tdl_n_315,pre_tdl_n_316,pre_tdl_n_317,pre_tdl_n_318,pre_tdl_n_319}));
  n4bit_carry_chain_0 pst_tdl
       (.CLK(CLK),
        .JD_OBUF(JD_OBUF[1]),
        .\q_reg[0] ({pst_tdl_n_0,pst_tdl_n_1,pst_tdl_n_2,pst_tdl_n_3,pst_tdl_n_4,pst_tdl_n_5,pst_tdl_n_6,pst_tdl_n_7,pst_tdl_n_8,pst_tdl_n_9,pst_tdl_n_10,pst_tdl_n_11,pst_tdl_n_12,pst_tdl_n_13,pst_tdl_n_14,pst_tdl_n_15,pst_tdl_n_16,pst_tdl_n_17,pst_tdl_n_18,pst_tdl_n_19,pst_tdl_n_20,pst_tdl_n_21,pst_tdl_n_22,pst_tdl_n_23,pst_tdl_n_24,pst_tdl_n_25,pst_tdl_n_26,pst_tdl_n_27,pst_tdl_n_28,pst_tdl_n_29,pst_tdl_n_30,pst_tdl_n_31,pst_tdl_n_32,pst_tdl_n_33,pst_tdl_n_34,pst_tdl_n_35,pst_tdl_n_36,pst_tdl_n_37,pst_tdl_n_38,pst_tdl_n_39,pst_tdl_n_40,pst_tdl_n_41,pst_tdl_n_42,pst_tdl_n_43,pst_tdl_n_44,pst_tdl_n_45,pst_tdl_n_46,pst_tdl_n_47,pst_tdl_n_48,pst_tdl_n_49,pst_tdl_n_50,pst_tdl_n_51,pst_tdl_n_52,pst_tdl_n_53,pst_tdl_n_54,pst_tdl_n_55,pst_tdl_n_56,pst_tdl_n_57,pst_tdl_n_58,pst_tdl_n_59,pst_tdl_n_60,pst_tdl_n_61,pst_tdl_n_62,pst_tdl_n_63,pst_tdl_n_64,pst_tdl_n_65,pst_tdl_n_66,pst_tdl_n_67,pst_tdl_n_68,pst_tdl_n_69,pst_tdl_n_70,pst_tdl_n_71,pst_tdl_n_72,pst_tdl_n_73,pst_tdl_n_74,pst_tdl_n_75,pst_tdl_n_76,pst_tdl_n_77,pst_tdl_n_78,pst_tdl_n_79,pst_tdl_n_80,pst_tdl_n_81,pst_tdl_n_82,pst_tdl_n_83,pst_tdl_n_84,pst_tdl_n_85,pst_tdl_n_86,pst_tdl_n_87,pst_tdl_n_88,pst_tdl_n_89,pst_tdl_n_90,pst_tdl_n_91,pst_tdl_n_92,pst_tdl_n_93,pst_tdl_n_94,pst_tdl_n_95,pst_tdl_n_96,pst_tdl_n_97,pst_tdl_n_98,pst_tdl_n_99,pst_tdl_n_100,pst_tdl_n_101,pst_tdl_n_102,pst_tdl_n_103,pst_tdl_n_104,pst_tdl_n_105,pst_tdl_n_106,pst_tdl_n_107,pst_tdl_n_108,pst_tdl_n_109,pst_tdl_n_110,pst_tdl_n_111,pst_tdl_n_112,pst_tdl_n_113,pst_tdl_n_114,pst_tdl_n_115,pst_tdl_n_116,pst_tdl_n_117,pst_tdl_n_118,pst_tdl_n_119,pst_tdl_n_120,pst_tdl_n_121,pst_tdl_n_122,pst_tdl_n_123,pst_tdl_n_124,pst_tdl_n_125,pst_tdl_n_126,pst_tdl_n_127,pst_tdl_n_128,pst_tdl_n_129,pst_tdl_n_130,pst_tdl_n_131,pst_tdl_n_132,pst_tdl_n_133,pst_tdl_n_134,pst_tdl_n_135,pst_tdl_n_136,pst_tdl_n_137,pst_tdl_n_138,pst_tdl_n_139,pst_tdl_n_140,pst_tdl_n_141,pst_tdl_n_142,pst_tdl_n_143,pst_tdl_n_144,pst_tdl_n_145,pst_tdl_n_146,pst_tdl_n_147,pst_tdl_n_148,pst_tdl_n_149,pst_tdl_n_150,pst_tdl_n_151,pst_tdl_n_152,pst_tdl_n_153,pst_tdl_n_154,pst_tdl_n_155,pst_tdl_n_156,pst_tdl_n_157,pst_tdl_n_158,pst_tdl_n_159,pst_tdl_n_160,pst_tdl_n_161,pst_tdl_n_162,pst_tdl_n_163,pst_tdl_n_164,pst_tdl_n_165,pst_tdl_n_166,pst_tdl_n_167,pst_tdl_n_168,pst_tdl_n_169,pst_tdl_n_170,pst_tdl_n_171,pst_tdl_n_172,pst_tdl_n_173,pst_tdl_n_174,pst_tdl_n_175,pst_tdl_n_176,pst_tdl_n_177,pst_tdl_n_178,pst_tdl_n_179,pst_tdl_n_180,pst_tdl_n_181,pst_tdl_n_182,pst_tdl_n_183,pst_tdl_n_184,pst_tdl_n_185,pst_tdl_n_186,pst_tdl_n_187,pst_tdl_n_188,pst_tdl_n_189,pst_tdl_n_190,pst_tdl_n_191,pst_tdl_n_192,pst_tdl_n_193,pst_tdl_n_194,pst_tdl_n_195,pst_tdl_n_196,pst_tdl_n_197,pst_tdl_n_198,pst_tdl_n_199,pst_tdl_n_200,pst_tdl_n_201,pst_tdl_n_202,pst_tdl_n_203,pst_tdl_n_204,pst_tdl_n_205,pst_tdl_n_206,pst_tdl_n_207,pst_tdl_n_208,pst_tdl_n_209,pst_tdl_n_210,pst_tdl_n_211,pst_tdl_n_212,pst_tdl_n_213,pst_tdl_n_214,pst_tdl_n_215,pst_tdl_n_216,pst_tdl_n_217,pst_tdl_n_218,pst_tdl_n_219,pst_tdl_n_220,pst_tdl_n_221,pst_tdl_n_222,pst_tdl_n_223,pst_tdl_n_224,pst_tdl_n_225,pst_tdl_n_226,pst_tdl_n_227,pst_tdl_n_228,pst_tdl_n_229,pst_tdl_n_230,pst_tdl_n_231,pst_tdl_n_232,pst_tdl_n_233,pst_tdl_n_234,pst_tdl_n_235,pst_tdl_n_236,pst_tdl_n_237,pst_tdl_n_238,pst_tdl_n_239,pst_tdl_n_240,pst_tdl_n_241,pst_tdl_n_242,pst_tdl_n_243,pst_tdl_n_244,pst_tdl_n_245,pst_tdl_n_246,pst_tdl_n_247,pst_tdl_n_248,pst_tdl_n_249,pst_tdl_n_250,pst_tdl_n_251,pst_tdl_n_252,pst_tdl_n_253,pst_tdl_n_254,pst_tdl_n_255,pst_tdl_n_256,pst_tdl_n_257,pst_tdl_n_258,pst_tdl_n_259,pst_tdl_n_260,pst_tdl_n_261,pst_tdl_n_262,pst_tdl_n_263,pst_tdl_n_264,pst_tdl_n_265,pst_tdl_n_266,pst_tdl_n_267,pst_tdl_n_268,pst_tdl_n_269,pst_tdl_n_270,pst_tdl_n_271,pst_tdl_n_272,pst_tdl_n_273,pst_tdl_n_274,pst_tdl_n_275,pst_tdl_n_276,pst_tdl_n_277,pst_tdl_n_278,pst_tdl_n_279,pst_tdl_n_280,pst_tdl_n_281,pst_tdl_n_282,pst_tdl_n_283,pst_tdl_n_284,pst_tdl_n_285,pst_tdl_n_286,pst_tdl_n_287,pst_tdl_n_288,pst_tdl_n_289,pst_tdl_n_290,pst_tdl_n_291,pst_tdl_n_292,pst_tdl_n_293,pst_tdl_n_294,pst_tdl_n_295,pst_tdl_n_296,pst_tdl_n_297,pst_tdl_n_298,pst_tdl_n_299,pst_tdl_n_300,pst_tdl_n_301,pst_tdl_n_302,pst_tdl_n_303,pst_tdl_n_304,pst_tdl_n_305,pst_tdl_n_306,pst_tdl_n_307,pst_tdl_n_308,pst_tdl_n_309,pst_tdl_n_310,pst_tdl_n_311,pst_tdl_n_312,pst_tdl_n_313,pst_tdl_n_314,pst_tdl_n_315,pst_tdl_n_316,pst_tdl_n_317,pst_tdl_n_318,pst_tdl_n_319}));
endmodule

(* ECO_CHECKSUM = "a603db3a" *) 
(* NotValidForBitStream *)
module top
   (BTN,
    JD,
    LED,
    CLK,
    UART_TXD,
    START,
    STOP);
  input [3:0]BTN;
  output [7:0]JD;
  output [3:0]LED;
  input CLK;
  output UART_TXD;
  input START;
  input STOP;

  wire CLK;
  wire CLK_IBUF;
  wire CLK_IBUF_BUFG;
  wire [7:0]DATA;
  wire DATA_UART;
  wire \DATA_UART[0]_i_1_n_0 ;
  wire \DATA_UART[1]_i_1_n_0 ;
  wire \DATA_UART[2]_i_1_n_0 ;
  wire \DATA_UART[3]_i_1_n_0 ;
  wire \DATA_UART[4]_i_1_n_0 ;
  wire \DATA_UART[5]_i_1_n_0 ;
  wire \DATA_UART[6]_i_1_n_0 ;
  wire \DATA_UART[7]_i_1_n_0 ;
  wire \DATA_UART[7]_i_3_n_0 ;
  wire \FSM_sequential_uart_state[0]_i_2_n_0 ;
  wire \FSM_sequential_uart_state[2]_i_1_n_0 ;
  wire Inst_UART_TX_CTRL_n_1;
  wire Inst_UART_TX_CTRL_n_2;
  wire [7:0]JD;
  wire [3:2]JD_OBUF;
  wire [3:0]LED;
  wire [3:0]LED_OBUF;
  wire START;
  wire STOP;
  wire UART_TXD;
  wire UART_TXD_OBUF;
  wire clk_out1;
  wire [7:0]data1;
  wire [7:0]data2;
  wire [7:0]data3;
  wire pre_trig;
  wire pre_trig_BUFG;
  wire pst_trig;
  wire pst_trig_BUFG;
  wire \slice[0]_i_1_n_0 ;
  wire \slice[1]_i_1_n_0 ;
  wire \slice[2]_i_1_n_0 ;
  wire \slice_reg_n_0_[0] ;
  wire \slice_reg_n_0_[1] ;
  wire \slice_reg_n_0_[2] ;
  wire [24:24]tdc_result;
  wire [31:12]tdc_result0_in;
  wire uart_send_i_1_n_0;
  wire uart_send_reg_n_0;
  wire [2:0]uart_state;

initial begin
 $sdf_annotate("tdc_tb_time_impl.sdf",,,,"tool_control");
end
  (* LOPT_BUFG_CLOCK *) 
  (* OPT_MODIFIED = "BUFG_OPT" *) 
  BUFG CLK_IBUF_BUFG_inst
       (.I(CLK_IBUF),
        .O(CLK_IBUF_BUFG));
  (* OPT_INSERTED *) 
  (* OPT_MODIFIED = "MLO BUFG_OPT" *) 
  IBUF CLK_IBUF_inst
       (.I(CLK),
        .O(CLK_IBUF));
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \DATA_UART[0]_i_1 
       (.I0(data2[0]),
        .I1(data1[0]),
        .I2(data3[0]),
        .I3(\slice_reg_n_0_[1] ),
        .I4(\slice_reg_n_0_[0] ),
        .O(\DATA_UART[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \DATA_UART[1]_i_1 
       (.I0(data2[1]),
        .I1(data1[1]),
        .I2(data3[1]),
        .I3(\slice_reg_n_0_[1] ),
        .I4(\slice_reg_n_0_[0] ),
        .O(\DATA_UART[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \DATA_UART[2]_i_1 
       (.I0(data2[2]),
        .I1(data1[2]),
        .I2(data3[2]),
        .I3(\slice_reg_n_0_[1] ),
        .I4(\slice_reg_n_0_[0] ),
        .O(\DATA_UART[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \DATA_UART[3]_i_1 
       (.I0(data2[3]),
        .I1(data1[3]),
        .I2(data3[3]),
        .I3(\slice_reg_n_0_[1] ),
        .I4(\slice_reg_n_0_[0] ),
        .O(\DATA_UART[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \DATA_UART[4]_i_1 
       (.I0(data2[4]),
        .I1(data1[4]),
        .I2(data3[4]),
        .I3(\slice_reg_n_0_[1] ),
        .I4(\slice_reg_n_0_[0] ),
        .O(\DATA_UART[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \DATA_UART[5]_i_1 
       (.I0(data2[5]),
        .I1(data1[5]),
        .I2(data3[5]),
        .I3(\slice_reg_n_0_[1] ),
        .I4(\slice_reg_n_0_[0] ),
        .O(\DATA_UART[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \DATA_UART[6]_i_1 
       (.I0(data2[6]),
        .I1(data1[6]),
        .I2(data3[6]),
        .I3(\slice_reg_n_0_[1] ),
        .I4(\slice_reg_n_0_[0] ),
        .O(\DATA_UART[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0020)) 
    \DATA_UART[7]_i_1 
       (.I0(\slice_reg_n_0_[2] ),
        .I1(uart_state[0]),
        .I2(uart_state[1]),
        .I3(uart_state[2]),
        .O(\DATA_UART[7]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \DATA_UART[7]_i_2 
       (.I0(uart_state[2]),
        .I1(uart_state[1]),
        .I2(uart_state[0]),
        .O(DATA_UART));
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \DATA_UART[7]_i_3 
       (.I0(data2[7]),
        .I1(data1[7]),
        .I2(data3[7]),
        .I3(\slice_reg_n_0_[1] ),
        .I4(\slice_reg_n_0_[0] ),
        .O(\DATA_UART[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(\DATA_UART[0]_i_1_n_0 ),
        .Q(DATA[0]),
        .R(\DATA_UART[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(\DATA_UART[1]_i_1_n_0 ),
        .Q(DATA[1]),
        .R(\DATA_UART[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(\DATA_UART[2]_i_1_n_0 ),
        .Q(DATA[2]),
        .R(\DATA_UART[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(\DATA_UART[3]_i_1_n_0 ),
        .Q(DATA[3]),
        .R(\DATA_UART[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(\DATA_UART[4]_i_1_n_0 ),
        .Q(DATA[4]),
        .R(\DATA_UART[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(\DATA_UART[5]_i_1_n_0 ),
        .Q(DATA[5]),
        .R(\DATA_UART[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(\DATA_UART[6]_i_1_n_0 ),
        .Q(DATA[6]),
        .R(\DATA_UART[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \DATA_UART_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(DATA_UART),
        .D(\DATA_UART[7]_i_3_n_0 ),
        .Q(DATA[7]),
        .R(\DATA_UART[7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_uart_state[0]_i_2 
       (.I0(\slice_reg_n_0_[2] ),
        .I1(\slice_reg_n_0_[1] ),
        .O(\FSM_sequential_uart_state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair221" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    \FSM_sequential_uart_state[2]_i_1 
       (.I0(uart_state[0]),
        .I1(uart_state[2]),
        .I2(uart_state[1]),
        .O(\FSM_sequential_uart_state[2]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "prepare_send:010,send_data:011,busy:100,load_data:001,ready:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_uart_state_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(Inst_UART_TX_CTRL_n_1),
        .Q(uart_state[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "prepare_send:010,send_data:011,busy:100,load_data:001,ready:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_uart_state_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(Inst_UART_TX_CTRL_n_2),
        .Q(uart_state[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "prepare_send:010,send_data:011,busy:100,load_data:001,ready:000" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_uart_state_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_sequential_uart_state[2]_i_1_n_0 ),
        .Q(uart_state[2]),
        .R(1'b0));
  UART_TX_CTRL Inst_UART_TX_CTRL
       (.CLK(CLK_IBUF_BUFG),
        .D(DATA),
        .E(uart_send_reg_n_0),
        .\FSM_sequential_txState_reg[0]_0 (Inst_UART_TX_CTRL_n_2),
        .\FSM_sequential_uart_state_reg[0] (\slice_reg_n_0_[0] ),
        .\FSM_sequential_uart_state_reg[0]_0 (\FSM_sequential_uart_state[0]_i_2_n_0 ),
        .\FSM_sequential_uart_state_reg[2] (Inst_UART_TX_CTRL_n_1),
        .JD_OBUF(JD_OBUF[2]),
        .UART_TXD_OBUF(UART_TXD_OBUF),
        .uart_state(uart_state));
  OBUFT \JD_OBUF[0]_inst 
       (.I(1'b0),
        .O(JD[0]),
        .T(1'b1));
  OBUF \JD_OBUF[1]_inst 
       (.I(1'b0),
        .O(JD[1]));
  OBUF \JD_OBUF[2]_inst 
       (.I(JD_OBUF[2]),
        .O(JD[2]));
  OBUF \JD_OBUF[3]_inst 
       (.I(JD_OBUF[3]),
        .O(JD[3]));
  OBUFT \JD_OBUF[4]_inst 
       (.I(1'b0),
        .O(JD[4]),
        .T(1'b1));
  OBUFT \JD_OBUF[5]_inst 
       (.I(1'b0),
        .O(JD[5]),
        .T(1'b1));
  OBUFT \JD_OBUF[6]_inst 
       (.I(1'b0),
        .O(JD[6]),
        .T(1'b1));
  OBUFT \JD_OBUF[7]_inst 
       (.I(1'b0),
        .O(JD[7]),
        .T(1'b1));
  OBUF \LED_OBUF[0]_inst 
       (.I(LED_OBUF[0]),
        .O(LED[0]));
  OBUF \LED_OBUF[1]_inst 
       (.I(LED_OBUF[1]),
        .O(LED[1]));
  OBUF \LED_OBUF[2]_inst 
       (.I(LED_OBUF[2]),
        .O(LED[2]));
  OBUF \LED_OBUF[3]_inst 
       (.I(LED_OBUF[3]),
        .O(LED[3]));
  IBUF START_IBUF_inst
       (.I(START),
        .O(JD_OBUF[2]));
  IBUF STOP_IBUF_inst
       (.I(STOP),
        .O(JD_OBUF[3]));
  OBUF UART_TXD_OBUF_inst
       (.I(UART_TXD_OBUF),
        .O(UART_TXD));
  (* syn_black_box = "TRUE" *) 
  clk_wiz_0 inst_clk
       (.clk_in1(CLK_IBUF_BUFG),
        .clk_out1(clk_out1),
        .reset(1'b0));
  tdc inst_tdc
       (.CLK(clk_out1),
        .D({tdc_result0_in,LED_OBUF}),
        .JD_OBUF(JD_OBUF),
        .pre_trig(pre_trig),
        .pst_trig(pst_trig),
        .\q_reg[0] (pre_trig_BUFG),
        .\q_reg[0]_0 (pst_trig_BUFG));
  BUFG pre_trig_BUFG_inst
       (.I(pre_trig),
        .O(pre_trig_BUFG));
  BUFG pst_trig_BUFG_inst
       (.I(pst_trig),
        .O(pst_trig_BUFG));
  LUT6 #(
    .INIT(64'hFFFFF0FF00000D00)) 
    \slice[0]_i_1 
       (.I0(\slice_reg_n_0_[2] ),
        .I1(\slice_reg_n_0_[1] ),
        .I2(uart_state[1]),
        .I3(uart_state[2]),
        .I4(uart_state[0]),
        .I5(\slice_reg_n_0_[0] ),
        .O(\slice[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair221" *) 
  LUT5 #(
    .INIT(32'hFFDF0020)) 
    \slice[1]_i_1 
       (.I0(\slice_reg_n_0_[0] ),
        .I1(uart_state[1]),
        .I2(uart_state[2]),
        .I3(uart_state[0]),
        .I4(\slice_reg_n_0_[1] ),
        .O(\slice[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF6FF00000800)) 
    \slice[2]_i_1 
       (.I0(\slice_reg_n_0_[1] ),
        .I1(\slice_reg_n_0_[0] ),
        .I2(uart_state[1]),
        .I3(uart_state[2]),
        .I4(uart_state[0]),
        .I5(\slice_reg_n_0_[2] ),
        .O(\slice[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \slice_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\slice[0]_i_1_n_0 ),
        .Q(\slice_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slice_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\slice[1]_i_1_n_0 ),
        .Q(\slice_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slice_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\slice[2]_i_1_n_0 ),
        .Q(\slice_reg_n_0_[2] ),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h0100)) 
    \tdc_result[31]_i_1 
       (.I0(uart_state[1]),
        .I1(uart_state[0]),
        .I2(uart_state[2]),
        .I3(JD_OBUF[2]),
        .O(tdc_result));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(LED_OBUF[2]),
        .Q(data3[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(LED_OBUF[3]),
        .Q(data3[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[12]),
        .Q(data3[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[13]),
        .Q(data3[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[14]),
        .Q(data3[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[15]),
        .Q(data3[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[16] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[16]),
        .Q(data2[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[17] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[17]),
        .Q(data2[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[18] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[18]),
        .Q(data2[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[19] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[19]),
        .Q(data2[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[20] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[20]),
        .Q(data2[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[21] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[21]),
        .Q(data2[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[22] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[22]),
        .Q(data2[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[23] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[23]),
        .Q(data2[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[24] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[24]),
        .Q(data1[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[25] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[25]),
        .Q(data1[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[26] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[26]),
        .Q(data1[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[27] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[27]),
        .Q(data1[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[28] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[28]),
        .Q(data1[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[29] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[29]),
        .Q(data1[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[30] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[30]),
        .Q(data1[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[31] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(tdc_result0_in[31]),
        .Q(data1[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(LED_OBUF[0]),
        .Q(data3[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \tdc_result_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(tdc_result),
        .D(LED_OBUF[1]),
        .Q(data3[1]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hEF08)) 
    uart_send_i_1
       (.I0(uart_state[0]),
        .I1(uart_state[1]),
        .I2(uart_state[2]),
        .I3(uart_send_reg_n_0),
        .O(uart_send_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    uart_send_reg
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(uart_send_i_1_n_0),
        .Q(uart_send_reg_n_0),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
