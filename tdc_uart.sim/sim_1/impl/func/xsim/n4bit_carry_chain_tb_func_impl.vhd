-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Tue Aug 27 17:39:00 2019
-- Host        : rambo running 64-bit Arch Linux
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/chavdar/tdc_uart/tdc_uart.sim/sim_1/impl/func/xsim/n4bit_carry_chain_tb_func_impl.vhd
-- Design      : top
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35ticsg324-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity UART_TX_CTRL is
  port (
    UART_TXD_OBUF : out STD_LOGIC;
    \FSM_sequential_uart_state_reg[2]\ : out STD_LOGIC;
    \FSM_sequential_txState_reg[0]_0\ : out STD_LOGIC;
    CLK : in STD_LOGIC;
    uart_state : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \FSM_sequential_uart_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_uart_state_reg[0]_0\ : in STD_LOGIC;
    JD_OBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end UART_TX_CTRL;

architecture STRUCTURE of UART_TX_CTRL is
  signal \FSM_sequential_txState[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_txState[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_txState[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_txState[0]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_txState[0]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_txState[0]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_txState[0]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_txState[0]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_sequential_txState[0]_i_9_n_0\ : STD_LOGIC;
  signal \FSM_sequential_txState[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_txState[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_txState[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_uart_state[0]_i_3_n_0\ : STD_LOGIC;
  signal READY : STD_LOGIC;
  signal bitIndex : STD_LOGIC;
  signal \bitIndex[0]_i_2_n_0\ : STD_LOGIC;
  signal bitIndex_reg : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal \bitIndex_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \bitIndex_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \bitIndex_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \bitIndex_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \bitIndex_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \bitIndex_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \bitIndex_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \bitIndex_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \bitIndex_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \bitIndex_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \bitIndex_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \bitIndex_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \bitIndex_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \bitIndex_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \bitIndex_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \bitIndex_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \bitIndex_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \bitIndex_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \bitIndex_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \bitIndex_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \bitIndex_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \bitIndex_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \bitIndex_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \bitIndex_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \bitIndex_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \bitIndex_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \bitIndex_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \bitIndex_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \bitIndex_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \bitIndex_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \bitIndex_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \bitIndex_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \bitIndex_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \bitIndex_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \bitIndex_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \bitIndex_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \bitIndex_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \bitIndex_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal bitTmr : STD_LOGIC;
  signal \bitTmr[0]_i_3_n_0\ : STD_LOGIC;
  signal bitTmr_reg : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \bitTmr_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \bitTmr_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \bitTmr_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \bitTmr_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \bitTmr_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \bitTmr_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \bitTmr_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \bitTmr_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \bitTmr_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \bitTmr_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \bitTmr_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \bitTmr_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \bitTmr_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \bitTmr_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \bitTmr_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \bitTmr_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \bitTmr_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \eqOp__12\ : STD_LOGIC;
  signal txBit_i_3_n_0 : STD_LOGIC;
  signal txBit_i_4_n_0 : STD_LOGIC;
  signal txBit_i_5_n_0 : STD_LOGIC;
  signal txData : STD_LOGIC_VECTOR ( 8 downto 1 );
  signal txState : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_bitIndex_reg[0]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_bitIndex_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_bitIndex_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_bitIndex_reg[20]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_bitIndex_reg[24]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_bitIndex_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_bitIndex_reg[28]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_bitIndex_reg[4]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_bitIndex_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_bitTmr_reg[0]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_bitTmr_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_bitTmr_reg[12]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_bitTmr_reg[4]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_bitTmr_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_txState[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \FSM_sequential_txState[1]_i_1\ : label is "soft_lutpair0";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_txState_reg[0]\ : label is "send_bit:10,load_bit:01,rdy:00";
  attribute FSM_ENCODED_STATES of \FSM_sequential_txState_reg[1]\ : label is "send_bit:10,load_bit:01,rdy:00";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \bitIndex_reg[0]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \bitIndex_reg[12]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \bitIndex_reg[16]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \bitIndex_reg[20]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \bitIndex_reg[24]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \bitIndex_reg[28]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \bitIndex_reg[4]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \bitIndex_reg[8]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \bitTmr_reg[0]_i_2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \bitTmr_reg[12]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \bitTmr_reg[4]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \bitTmr_reg[8]_i_1\ : label is "SWEEP";
begin
\FSM_sequential_txState[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F000F0CA"
    )
        port map (
      I0 => E(0),
      I1 => \eqOp__12\,
      I2 => txState(1),
      I3 => txState(0),
      I4 => \FSM_sequential_txState[0]_i_2_n_0\,
      O => \FSM_sequential_txState[0]_i_1_n_0\
    );
\FSM_sequential_txState[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => \FSM_sequential_txState[0]_i_3_n_0\,
      I1 => \FSM_sequential_txState[0]_i_4_n_0\,
      I2 => bitIndex_reg(0),
      I3 => bitIndex_reg(2),
      I4 => bitIndex_reg(4),
      I5 => \FSM_sequential_txState[0]_i_5_n_0\,
      O => \FSM_sequential_txState[0]_i_2_n_0\
    );
\FSM_sequential_txState[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100000000"
    )
        port map (
      I0 => bitIndex_reg(13),
      I1 => bitIndex_reg(14),
      I2 => bitIndex_reg(15),
      I3 => bitIndex_reg(28),
      I4 => bitIndex_reg(30),
      I5 => txState(1),
      O => \FSM_sequential_txState[0]_i_3_n_0\
    );
\FSM_sequential_txState[0]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => bitIndex_reg(5),
      I1 => bitIndex_reg(6),
      I2 => bitIndex_reg(7),
      I3 => bitIndex_reg(8),
      I4 => \FSM_sequential_txState[0]_i_6_n_0\,
      O => \FSM_sequential_txState[0]_i_4_n_0\
    );
\FSM_sequential_txState[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFEF"
    )
        port map (
      I0 => \FSM_sequential_txState[0]_i_7_n_0\,
      I1 => \FSM_sequential_txState[0]_i_8_n_0\,
      I2 => bitIndex_reg(1),
      I3 => bitIndex_reg(29),
      I4 => bitIndex_reg(17),
      I5 => \FSM_sequential_txState[0]_i_9_n_0\,
      O => \FSM_sequential_txState[0]_i_5_n_0\
    );
\FSM_sequential_txState[0]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => bitIndex_reg(12),
      I1 => bitIndex_reg(11),
      I2 => bitIndex_reg(10),
      I3 => bitIndex_reg(9),
      O => \FSM_sequential_txState[0]_i_6_n_0\
    );
\FSM_sequential_txState[0]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => bitIndex_reg(23),
      I1 => bitIndex_reg(20),
      I2 => bitIndex_reg(25),
      I3 => bitIndex_reg(22),
      O => \FSM_sequential_txState[0]_i_7_n_0\
    );
\FSM_sequential_txState[0]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => bitIndex_reg(19),
      I1 => bitIndex_reg(16),
      I2 => bitIndex_reg(21),
      I3 => bitIndex_reg(18),
      O => \FSM_sequential_txState[0]_i_8_n_0\
    );
\FSM_sequential_txState[0]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => bitIndex_reg(27),
      I1 => bitIndex_reg(24),
      I2 => bitIndex_reg(3),
      I3 => bitIndex_reg(26),
      O => \FSM_sequential_txState[0]_i_9_n_0\
    );
\FSM_sequential_txState[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => \eqOp__12\,
      I1 => txState(1),
      I2 => txState(0),
      O => \FSM_sequential_txState[1]_i_1_n_0\
    );
\FSM_sequential_txState[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008000000000"
    )
        port map (
      I0 => \FSM_sequential_txState[1]_i_3_n_0\,
      I1 => bitTmr_reg(1),
      I2 => bitTmr_reg(0),
      I3 => bitTmr_reg(3),
      I4 => bitTmr_reg(2),
      I5 => \FSM_sequential_txState[1]_i_4_n_0\,
      O => \eqOp__12\
    );
\FSM_sequential_txState[1]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => bitTmr_reg(7),
      I1 => bitTmr_reg(6),
      I2 => bitTmr_reg(4),
      I3 => bitTmr_reg(5),
      O => \FSM_sequential_txState[1]_i_3_n_0\
    );
\FSM_sequential_txState[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000008"
    )
        port map (
      I0 => bitTmr_reg(8),
      I1 => bitTmr_reg(9),
      I2 => bitTmr_reg(10),
      I3 => bitTmr_reg(11),
      I4 => bitTmr_reg(13),
      I5 => bitTmr_reg(12),
      O => \FSM_sequential_txState[1]_i_4_n_0\
    );
\FSM_sequential_txState_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \FSM_sequential_txState[0]_i_1_n_0\,
      Q => txState(0),
      R => '0'
    );
\FSM_sequential_txState_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \FSM_sequential_txState[1]_i_1_n_0\,
      Q => txState(1),
      R => '0'
    );
\FSM_sequential_uart_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF75770000"
    )
        port map (
      I0 => uart_state(2),
      I1 => uart_state(1),
      I2 => \FSM_sequential_uart_state_reg[0]\,
      I3 => \FSM_sequential_uart_state_reg[0]_0\,
      I4 => \FSM_sequential_uart_state[0]_i_3_n_0\,
      I5 => uart_state(0),
      O => \FSM_sequential_uart_state_reg[2]\
    );
\FSM_sequential_uart_state[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF00FF03AA"
    )
        port map (
      I0 => JD_OBUF(0),
      I1 => txState(0),
      I2 => txState(1),
      I3 => uart_state(0),
      I4 => uart_state(2),
      I5 => uart_state(1),
      O => \FSM_sequential_uart_state[0]_i_3_n_0\
    );
\FSM_sequential_uart_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF0F0010"
    )
        port map (
      I0 => txState(0),
      I1 => txState(1),
      I2 => uart_state(0),
      I3 => uart_state(2),
      I4 => uart_state(1),
      O => \FSM_sequential_txState_reg[0]_0\
    );
\bitIndex[0]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => bitIndex_reg(0),
      O => \bitIndex[0]_i_2_n_0\
    );
\bitIndex_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[0]_i_1_n_7\,
      Q => bitIndex_reg(0),
      R => READY
    );
\bitIndex_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \bitIndex_reg[0]_i_1_n_0\,
      CO(2 downto 0) => \NLW_bitIndex_reg[0]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \bitIndex_reg[0]_i_1_n_4\,
      O(2) => \bitIndex_reg[0]_i_1_n_5\,
      O(1) => \bitIndex_reg[0]_i_1_n_6\,
      O(0) => \bitIndex_reg[0]_i_1_n_7\,
      S(3 downto 1) => bitIndex_reg(3 downto 1),
      S(0) => \bitIndex[0]_i_2_n_0\
    );
\bitIndex_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[8]_i_1_n_5\,
      Q => bitIndex_reg(10),
      R => READY
    );
\bitIndex_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[8]_i_1_n_4\,
      Q => bitIndex_reg(11),
      R => READY
    );
\bitIndex_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[12]_i_1_n_7\,
      Q => bitIndex_reg(12),
      R => READY
    );
\bitIndex_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \bitIndex_reg[8]_i_1_n_0\,
      CO(3) => \bitIndex_reg[12]_i_1_n_0\,
      CO(2 downto 0) => \NLW_bitIndex_reg[12]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \bitIndex_reg[12]_i_1_n_4\,
      O(2) => \bitIndex_reg[12]_i_1_n_5\,
      O(1) => \bitIndex_reg[12]_i_1_n_6\,
      O(0) => \bitIndex_reg[12]_i_1_n_7\,
      S(3 downto 0) => bitIndex_reg(15 downto 12)
    );
\bitIndex_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[12]_i_1_n_6\,
      Q => bitIndex_reg(13),
      R => READY
    );
\bitIndex_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[12]_i_1_n_5\,
      Q => bitIndex_reg(14),
      R => READY
    );
\bitIndex_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[12]_i_1_n_4\,
      Q => bitIndex_reg(15),
      R => READY
    );
\bitIndex_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[16]_i_1_n_7\,
      Q => bitIndex_reg(16),
      R => READY
    );
\bitIndex_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \bitIndex_reg[12]_i_1_n_0\,
      CO(3) => \bitIndex_reg[16]_i_1_n_0\,
      CO(2 downto 0) => \NLW_bitIndex_reg[16]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \bitIndex_reg[16]_i_1_n_4\,
      O(2) => \bitIndex_reg[16]_i_1_n_5\,
      O(1) => \bitIndex_reg[16]_i_1_n_6\,
      O(0) => \bitIndex_reg[16]_i_1_n_7\,
      S(3 downto 0) => bitIndex_reg(19 downto 16)
    );
\bitIndex_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[16]_i_1_n_6\,
      Q => bitIndex_reg(17),
      R => READY
    );
\bitIndex_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[16]_i_1_n_5\,
      Q => bitIndex_reg(18),
      R => READY
    );
\bitIndex_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[16]_i_1_n_4\,
      Q => bitIndex_reg(19),
      R => READY
    );
\bitIndex_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[0]_i_1_n_6\,
      Q => bitIndex_reg(1),
      R => READY
    );
\bitIndex_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[20]_i_1_n_7\,
      Q => bitIndex_reg(20),
      R => READY
    );
\bitIndex_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \bitIndex_reg[16]_i_1_n_0\,
      CO(3) => \bitIndex_reg[20]_i_1_n_0\,
      CO(2 downto 0) => \NLW_bitIndex_reg[20]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \bitIndex_reg[20]_i_1_n_4\,
      O(2) => \bitIndex_reg[20]_i_1_n_5\,
      O(1) => \bitIndex_reg[20]_i_1_n_6\,
      O(0) => \bitIndex_reg[20]_i_1_n_7\,
      S(3 downto 0) => bitIndex_reg(23 downto 20)
    );
\bitIndex_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[20]_i_1_n_6\,
      Q => bitIndex_reg(21),
      R => READY
    );
\bitIndex_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[20]_i_1_n_5\,
      Q => bitIndex_reg(22),
      R => READY
    );
\bitIndex_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[20]_i_1_n_4\,
      Q => bitIndex_reg(23),
      R => READY
    );
\bitIndex_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[24]_i_1_n_7\,
      Q => bitIndex_reg(24),
      R => READY
    );
\bitIndex_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \bitIndex_reg[20]_i_1_n_0\,
      CO(3) => \bitIndex_reg[24]_i_1_n_0\,
      CO(2 downto 0) => \NLW_bitIndex_reg[24]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \bitIndex_reg[24]_i_1_n_4\,
      O(2) => \bitIndex_reg[24]_i_1_n_5\,
      O(1) => \bitIndex_reg[24]_i_1_n_6\,
      O(0) => \bitIndex_reg[24]_i_1_n_7\,
      S(3 downto 0) => bitIndex_reg(27 downto 24)
    );
\bitIndex_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[24]_i_1_n_6\,
      Q => bitIndex_reg(25),
      R => READY
    );
\bitIndex_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[24]_i_1_n_5\,
      Q => bitIndex_reg(26),
      R => READY
    );
\bitIndex_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[24]_i_1_n_4\,
      Q => bitIndex_reg(27),
      R => READY
    );
\bitIndex_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[28]_i_1_n_7\,
      Q => bitIndex_reg(28),
      R => READY
    );
\bitIndex_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \bitIndex_reg[24]_i_1_n_0\,
      CO(3 downto 0) => \NLW_bitIndex_reg[28]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_bitIndex_reg[28]_i_1_O_UNCONNECTED\(3),
      O(2) => \bitIndex_reg[28]_i_1_n_5\,
      O(1) => \bitIndex_reg[28]_i_1_n_6\,
      O(0) => \bitIndex_reg[28]_i_1_n_7\,
      S(3) => '0',
      S(2 downto 0) => bitIndex_reg(30 downto 28)
    );
\bitIndex_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[28]_i_1_n_6\,
      Q => bitIndex_reg(29),
      R => READY
    );
\bitIndex_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[0]_i_1_n_5\,
      Q => bitIndex_reg(2),
      R => READY
    );
\bitIndex_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[28]_i_1_n_5\,
      Q => bitIndex_reg(30),
      R => READY
    );
\bitIndex_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[0]_i_1_n_4\,
      Q => bitIndex_reg(3),
      R => READY
    );
\bitIndex_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[4]_i_1_n_7\,
      Q => bitIndex_reg(4),
      R => READY
    );
\bitIndex_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \bitIndex_reg[0]_i_1_n_0\,
      CO(3) => \bitIndex_reg[4]_i_1_n_0\,
      CO(2 downto 0) => \NLW_bitIndex_reg[4]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \bitIndex_reg[4]_i_1_n_4\,
      O(2) => \bitIndex_reg[4]_i_1_n_5\,
      O(1) => \bitIndex_reg[4]_i_1_n_6\,
      O(0) => \bitIndex_reg[4]_i_1_n_7\,
      S(3 downto 0) => bitIndex_reg(7 downto 4)
    );
\bitIndex_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[4]_i_1_n_6\,
      Q => bitIndex_reg(5),
      R => READY
    );
\bitIndex_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[4]_i_1_n_5\,
      Q => bitIndex_reg(6),
      R => READY
    );
\bitIndex_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[4]_i_1_n_4\,
      Q => bitIndex_reg(7),
      R => READY
    );
\bitIndex_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[8]_i_1_n_7\,
      Q => bitIndex_reg(8),
      R => READY
    );
\bitIndex_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \bitIndex_reg[4]_i_1_n_0\,
      CO(3) => \bitIndex_reg[8]_i_1_n_0\,
      CO(2 downto 0) => \NLW_bitIndex_reg[8]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \bitIndex_reg[8]_i_1_n_4\,
      O(2) => \bitIndex_reg[8]_i_1_n_5\,
      O(1) => \bitIndex_reg[8]_i_1_n_6\,
      O(0) => \bitIndex_reg[8]_i_1_n_7\,
      S(3 downto 0) => bitIndex_reg(11 downto 8)
    );
\bitIndex_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => \bitIndex_reg[8]_i_1_n_6\,
      Q => bitIndex_reg(9),
      R => READY
    );
\bitTmr[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => \eqOp__12\,
      I1 => txState(1),
      I2 => txState(0),
      O => bitTmr
    );
\bitTmr[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => bitTmr_reg(0),
      O => \bitTmr[0]_i_3_n_0\
    );
\bitTmr_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[0]_i_2_n_7\,
      Q => bitTmr_reg(0),
      R => bitTmr
    );
\bitTmr_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \bitTmr_reg[0]_i_2_n_0\,
      CO(2 downto 0) => \NLW_bitTmr_reg[0]_i_2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \bitTmr_reg[0]_i_2_n_4\,
      O(2) => \bitTmr_reg[0]_i_2_n_5\,
      O(1) => \bitTmr_reg[0]_i_2_n_6\,
      O(0) => \bitTmr_reg[0]_i_2_n_7\,
      S(3 downto 1) => bitTmr_reg(3 downto 1),
      S(0) => \bitTmr[0]_i_3_n_0\
    );
\bitTmr_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[8]_i_1_n_5\,
      Q => bitTmr_reg(10),
      R => bitTmr
    );
\bitTmr_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[8]_i_1_n_4\,
      Q => bitTmr_reg(11),
      R => bitTmr
    );
\bitTmr_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[12]_i_1_n_7\,
      Q => bitTmr_reg(12),
      R => bitTmr
    );
\bitTmr_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \bitTmr_reg[8]_i_1_n_0\,
      CO(3 downto 0) => \NLW_bitTmr_reg[12]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_bitTmr_reg[12]_i_1_O_UNCONNECTED\(3 downto 2),
      O(1) => \bitTmr_reg[12]_i_1_n_6\,
      O(0) => \bitTmr_reg[12]_i_1_n_7\,
      S(3 downto 2) => B"00",
      S(1 downto 0) => bitTmr_reg(13 downto 12)
    );
\bitTmr_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[12]_i_1_n_6\,
      Q => bitTmr_reg(13),
      R => bitTmr
    );
\bitTmr_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[0]_i_2_n_6\,
      Q => bitTmr_reg(1),
      R => bitTmr
    );
\bitTmr_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[0]_i_2_n_5\,
      Q => bitTmr_reg(2),
      R => bitTmr
    );
\bitTmr_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[0]_i_2_n_4\,
      Q => bitTmr_reg(3),
      R => bitTmr
    );
\bitTmr_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[4]_i_1_n_7\,
      Q => bitTmr_reg(4),
      R => bitTmr
    );
\bitTmr_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \bitTmr_reg[0]_i_2_n_0\,
      CO(3) => \bitTmr_reg[4]_i_1_n_0\,
      CO(2 downto 0) => \NLW_bitTmr_reg[4]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \bitTmr_reg[4]_i_1_n_4\,
      O(2) => \bitTmr_reg[4]_i_1_n_5\,
      O(1) => \bitTmr_reg[4]_i_1_n_6\,
      O(0) => \bitTmr_reg[4]_i_1_n_7\,
      S(3 downto 0) => bitTmr_reg(7 downto 4)
    );
\bitTmr_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[4]_i_1_n_6\,
      Q => bitTmr_reg(5),
      R => bitTmr
    );
\bitTmr_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[4]_i_1_n_5\,
      Q => bitTmr_reg(6),
      R => bitTmr
    );
\bitTmr_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[4]_i_1_n_4\,
      Q => bitTmr_reg(7),
      R => bitTmr
    );
\bitTmr_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[8]_i_1_n_7\,
      Q => bitTmr_reg(8),
      R => bitTmr
    );
\bitTmr_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \bitTmr_reg[4]_i_1_n_0\,
      CO(3) => \bitTmr_reg[8]_i_1_n_0\,
      CO(2 downto 0) => \NLW_bitTmr_reg[8]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \bitTmr_reg[8]_i_1_n_4\,
      O(2) => \bitTmr_reg[8]_i_1_n_5\,
      O(1) => \bitTmr_reg[8]_i_1_n_6\,
      O(0) => \bitTmr_reg[8]_i_1_n_7\,
      S(3 downto 0) => bitTmr_reg(11 downto 8)
    );
\bitTmr_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \bitTmr_reg[8]_i_1_n_6\,
      Q => bitTmr_reg(9),
      R => bitTmr
    );
txBit_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => txState(0),
      I1 => txState(1),
      O => READY
    );
txBit_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => txState(0),
      I1 => txState(1),
      O => bitIndex
    );
txBit_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEFFEEF0EE0FEE00"
    )
        port map (
      I0 => txData(8),
      I1 => bitIndex_reg(0),
      I2 => bitIndex_reg(2),
      I3 => bitIndex_reg(3),
      I4 => txBit_i_4_n_0,
      I5 => txBit_i_5_n_0,
      O => txBit_i_3_n_0
    );
txBit_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FAC00AC0"
    )
        port map (
      I0 => txData(2),
      I1 => txData(1),
      I2 => bitIndex_reg(0),
      I3 => bitIndex_reg(1),
      I4 => txData(3),
      O => txBit_i_4_n_0
    );
txBit_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CACAFFF0CACA0F00"
    )
        port map (
      I0 => txData(5),
      I1 => txData(7),
      I2 => bitIndex_reg(1),
      I3 => txData(4),
      I4 => bitIndex_reg(0),
      I5 => txData(6),
      O => txBit_i_5_n_0
    );
txBit_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => CLK,
      CE => bitIndex,
      D => txBit_i_3_n_0,
      Q => UART_TXD_OBUF,
      S => READY
    );
\txData_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(0),
      Q => txData(1),
      R => '0'
    );
\txData_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(1),
      Q => txData(2),
      R => '0'
    );
\txData_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(2),
      Q => txData(3),
      R => '0'
    );
\txData_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(3),
      Q => txData(4),
      R => '0'
    );
\txData_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(4),
      Q => txData(5),
      R => '0'
    );
\txData_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(5),
      Q => txData(6),
      R => '0'
    );
\txData_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(6),
      Q => txData(7),
      R => '0'
    );
\txData_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      D => D(7),
      Q => txData(8),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity binary_counter is
  port (
    Q : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \count_reg[0]_0\ : out STD_LOGIC;
    JD_OBUF : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC
  );
end binary_counter;

architecture STRUCTURE of binary_counter is
  signal \^q\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal clear : STD_LOGIC;
  signal \plusOp__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cnt_offset[0]_i_1\ : label is "soft_lutpair111";
  attribute SOFT_HLUTNM of \count[1]_i_1\ : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of \count[2]_i_1__0\ : label is "soft_lutpair112";
  attribute SOFT_HLUTNM of \count[3]_i_2\ : label is "soft_lutpair111";
begin
  Q(3 downto 0) <= \^q\(3 downto 0);
\cnt_offset[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(3),
      I3 => \^q\(2),
      O => \count_reg[0]_0\
    );
\count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \plusOp__0\(0)
    );
\count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \plusOp__0\(1)
    );
\count[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(2),
      O => \plusOp__0\(2)
    );
\count[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => JD_OBUF(0),
      I1 => JD_OBUF(1),
      O => clear
    );
\count[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^q\(3),
      O => \plusOp__0\(3)
    );
\count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \plusOp__0\(0),
      Q => \^q\(0),
      R => clear
    );
\count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \plusOp__0\(1),
      Q => \^q\(1),
      R => clear
    );
\count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \plusOp__0\(2),
      Q => \^q\(2),
      R => clear
    );
\count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => \plusOp__0\(3),
      Q => \^q\(3),
      R => clear
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \binary_counter__parameterized0\ is
  port (
    max_cnt : out STD_LOGIC;
    JD_OBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \binary_counter__parameterized0\ : entity is "binary_counter";
end \binary_counter__parameterized0\;

architecture STRUCTURE of \binary_counter__parameterized0\ is
  signal \count[2]_i_1_n_0\ : STD_LOGIC;
  signal \count_reg_n_0_[0]\ : STD_LOGIC;
  signal \count_reg_n_0_[1]\ : STD_LOGIC;
  signal \count_reg_n_0_[2]\ : STD_LOGIC;
  signal max_cnt_i_1_n_0 : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \count[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \count[2]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of max_cnt_i_1 : label is "soft_lutpair1";
begin
\count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \count_reg_n_0_[0]\,
      O => plusOp(0)
    );
\count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \count_reg_n_0_[0]\,
      I1 => \count_reg_n_0_[1]\,
      O => plusOp(1)
    );
\count[2]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => JD_OBUF(0),
      O => \count[2]_i_1_n_0\
    );
\count[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \count_reg_n_0_[0]\,
      I1 => \count_reg_n_0_[1]\,
      I2 => \count_reg_n_0_[2]\,
      O => plusOp(2)
    );
\count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => JD_OBUF(0),
      D => plusOp(0),
      Q => \count_reg_n_0_[0]\,
      R => \count[2]_i_1_n_0\
    );
\count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => JD_OBUF(0),
      D => plusOp(1),
      Q => \count_reg_n_0_[1]\,
      R => \count[2]_i_1_n_0\
    );
\count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => JD_OBUF(0),
      D => plusOp(2),
      Q => \count_reg_n_0_[2]\,
      R => \count[2]_i_1_n_0\
    );
max_cnt_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \count_reg_n_0_[2]\,
      I1 => \count_reg_n_0_[0]\,
      I2 => \count_reg_n_0_[1]\,
      O => max_cnt_i_1_n_0
    );
max_cnt_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => JD_OBUF(0),
      D => max_cnt_i_1_n_0,
      Q => max_cnt,
      R => \count[2]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity hammin_weight_cnter is
  port (
    A : out STD_LOGIC_VECTOR ( 9 downto 0 );
    q_reg_i_33 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    q_reg : in STD_LOGIC_VECTOR ( 1 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S : in STD_LOGIC_VECTOR ( 2 downto 0 );
    q_reg_0 : in STD_LOGIC;
    q_reg_1 : in STD_LOGIC;
    q_reg_2 : in STD_LOGIC;
    q_reg_3 : in STD_LOGIC;
    q_reg_4 : in STD_LOGIC;
    q_reg_5 : in STD_LOGIC;
    q_reg_6 : in STD_LOGIC;
    q_reg_7 : in STD_LOGIC;
    q_reg_8 : in STD_LOGIC;
    q_reg_9 : in STD_LOGIC;
    q_reg_10 : in STD_LOGIC;
    q_reg_11 : in STD_LOGIC;
    q_reg_12 : in STD_LOGIC;
    q_reg_13 : in STD_LOGIC;
    q_reg_14 : in STD_LOGIC;
    q_reg_15 : in STD_LOGIC;
    q_reg_16 : in STD_LOGIC;
    q_reg_17 : in STD_LOGIC;
    q_reg_18 : in STD_LOGIC;
    q_reg_19 : in STD_LOGIC;
    q_reg_20 : in STD_LOGIC;
    q_reg_i_12_0 : in STD_LOGIC;
    q_reg_i_12_1 : in STD_LOGIC;
    q_reg_i_12_2 : in STD_LOGIC;
    q_reg_21 : in STD_LOGIC;
    q_reg_22 : in STD_LOGIC
  );
end hammin_weight_cnter;

architecture STRUCTURE of hammin_weight_cnter is
  signal \^a\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal pre_bit_cnt : STD_LOGIC_VECTOR ( 8 downto 1 );
  signal q_reg_i_10_n_0 : STD_LOGIC;
  signal q_reg_i_12_n_0 : STD_LOGIC;
  signal q_reg_i_13_n_0 : STD_LOGIC;
  signal q_reg_i_16_n_0 : STD_LOGIC;
  signal q_reg_i_17_n_0 : STD_LOGIC;
  signal q_reg_i_20_n_0 : STD_LOGIC;
  signal q_reg_i_21_n_0 : STD_LOGIC;
  signal q_reg_i_22_n_0 : STD_LOGIC;
  signal q_reg_i_27_n_0 : STD_LOGIC;
  signal \^q_reg_i_33\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_q_reg_i_10_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_q_reg_i_11_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_q_reg_i_11_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_q_reg_i_12_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of q_reg_i_10 : label is "SWEEP";
  attribute OPT_MODIFIED of q_reg_i_12 : label is "SWEEP";
begin
  A(9 downto 0) <= \^a\(9 downto 0);
  q_reg_i_33(1 downto 0) <= \^q_reg_i_33\(1 downto 0);
q_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8A88"
    )
        port map (
      I0 => pre_bit_cnt(8),
      I1 => pre_bit_cnt(7),
      I2 => q_reg_i_13_n_0,
      I3 => pre_bit_cnt(6),
      O => \^a\(9)
    );
q_reg_i_10: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => q_reg_i_10_n_0,
      CO(2 downto 0) => NLW_q_reg_i_10_CO_UNCONNECTED(2 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => \^q_reg_i_33\(1 downto 0),
      DI(1) => q_reg_i_16_n_0,
      DI(0) => q_reg_i_17_n_0,
      O(3 downto 1) => pre_bit_cnt(3 downto 1),
      O(0) => \^a\(0),
      S(3 downto 2) => q_reg(1 downto 0),
      S(1) => q_reg_i_20_n_0,
      S(0) => q_reg_i_21_n_0
    );
q_reg_i_11: unisim.vcomponents.CARRY4
     port map (
      CI => q_reg_i_12_n_0,
      CO(3 downto 1) => NLW_q_reg_i_11_CO_UNCONNECTED(3 downto 1),
      CO(0) => pre_bit_cnt(8),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_q_reg_i_11_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => B"0001"
    );
q_reg_i_12: unisim.vcomponents.CARRY4
     port map (
      CI => q_reg_i_10_n_0,
      CO(3) => q_reg_i_12_n_0,
      CO(2 downto 0) => NLW_q_reg_i_12_CO_UNCONNECTED(2 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => q_reg_i_22_n_0,
      DI(1 downto 0) => DI(1 downto 0),
      O(3 downto 0) => pre_bit_cnt(7 downto 4),
      S(3 downto 2) => S(2 downto 1),
      S(1) => q_reg_i_27_n_0,
      S(0) => S(0)
    );
q_reg_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_bit_cnt(4),
      I1 => pre_bit_cnt(2),
      I2 => pre_bit_cnt(1),
      I3 => \^a\(0),
      I4 => pre_bit_cnt(3),
      I5 => pre_bit_cnt(5),
      O => q_reg_i_13_n_0
    );
q_reg_i_14: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => q_reg_16,
      I1 => q_reg_17,
      I2 => q_reg_18,
      I3 => q_reg_19,
      I4 => q_reg_20,
      O => \^q_reg_i_33\(1)
    );
q_reg_i_15: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => q_reg_13,
      I1 => q_reg_14,
      I2 => q_reg_15,
      O => \^q_reg_i_33\(0)
    );
q_reg_i_16: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => q_reg_10,
      I1 => q_reg_11,
      I2 => q_reg_12,
      O => q_reg_i_16_n_0
    );
q_reg_i_17: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555556AA5656"
    )
        port map (
      I0 => q_reg_4,
      I1 => q_reg_5,
      I2 => q_reg_6,
      I3 => q_reg_7,
      I4 => q_reg_8,
      I5 => q_reg_9,
      O => q_reg_i_17_n_0
    );
q_reg_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BA45"
    )
        port map (
      I0 => pre_bit_cnt(7),
      I1 => q_reg_i_13_n_0,
      I2 => pre_bit_cnt(6),
      I3 => pre_bit_cnt(8),
      O => \^a\(8)
    );
q_reg_i_20: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => q_reg_i_16_n_0,
      I1 => q_reg_4,
      I2 => q_reg_21,
      I3 => q_reg_22,
      O => q_reg_i_20_n_0
    );
q_reg_i_21: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99995999"
    )
        port map (
      I0 => q_reg_i_17_n_0,
      I1 => q_reg_0,
      I2 => q_reg_1,
      I3 => q_reg_2,
      I4 => q_reg_3,
      O => q_reg_i_21_n_0
    );
q_reg_i_22: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => q_reg_i_12_0,
      I1 => q_reg_i_12_1,
      I2 => q_reg_i_12_2,
      O => q_reg_i_22_n_0
    );
q_reg_i_27: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => DI(1),
      I1 => q_reg_i_12_0,
      I2 => q_reg_i_12_1,
      O => q_reg_i_27_n_0
    );
q_reg_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => pre_bit_cnt(6),
      I1 => q_reg_i_13_n_0,
      I2 => pre_bit_cnt(7),
      O => \^a\(7)
    );
q_reg_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => q_reg_i_13_n_0,
      I1 => pre_bit_cnt(6),
      O => \^a\(6)
    );
q_reg_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000001FFFFFFFE"
    )
        port map (
      I0 => pre_bit_cnt(4),
      I1 => pre_bit_cnt(2),
      I2 => pre_bit_cnt(1),
      I3 => \^a\(0),
      I4 => pre_bit_cnt(3),
      I5 => pre_bit_cnt(5),
      O => \^a\(5)
    );
q_reg_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFE"
    )
        port map (
      I0 => pre_bit_cnt(3),
      I1 => \^a\(0),
      I2 => pre_bit_cnt(1),
      I3 => pre_bit_cnt(2),
      I4 => pre_bit_cnt(4),
      O => \^a\(4)
    );
q_reg_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"01FE"
    )
        port map (
      I0 => pre_bit_cnt(2),
      I1 => pre_bit_cnt(1),
      I2 => \^a\(0),
      I3 => pre_bit_cnt(3),
      O => \^a\(3)
    );
q_reg_i_8: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => \^a\(0),
      I1 => pre_bit_cnt(1),
      I2 => pre_bit_cnt(2),
      O => \^a\(2)
    );
q_reg_i_9: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^a\(0),
      I1 => pre_bit_cnt(1),
      O => \^a\(1)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
end nbit_register;

architecture STRUCTURE of nbit_register is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_0 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_0 : entity is "nbit_register";
end nbit_register_0;

architecture STRUCTURE of nbit_register_0 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_1 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_1 : entity is "nbit_register";
end nbit_register_1;

architecture STRUCTURE of nbit_register_1 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_10 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_10 : entity is "nbit_register";
end nbit_register_10;

architecture STRUCTURE of nbit_register_10 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_11 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_11 : entity is "nbit_register";
end nbit_register_11;

architecture STRUCTURE of nbit_register_11 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_12 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_12 : entity is "nbit_register";
end nbit_register_12;

architecture STRUCTURE of nbit_register_12 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_13 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_13 : entity is "nbit_register";
end nbit_register_13;

architecture STRUCTURE of nbit_register_13 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_14 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_14 : entity is "nbit_register";
end nbit_register_14;

architecture STRUCTURE of nbit_register_14 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_15 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_15 : entity is "nbit_register";
end nbit_register_15;

architecture STRUCTURE of nbit_register_15 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_16 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_16 : entity is "nbit_register";
end nbit_register_16;

architecture STRUCTURE of nbit_register_16 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_17 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_17 : entity is "nbit_register";
end nbit_register_17;

architecture STRUCTURE of nbit_register_17 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_18 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_18 : entity is "nbit_register";
end nbit_register_18;

architecture STRUCTURE of nbit_register_18 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_19 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_19 : entity is "nbit_register";
end nbit_register_19;

architecture STRUCTURE of nbit_register_19 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_2 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_2 : entity is "nbit_register";
end nbit_register_2;

architecture STRUCTURE of nbit_register_2 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_20 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_20 : entity is "nbit_register";
end nbit_register_20;

architecture STRUCTURE of nbit_register_20 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_21 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_21 : entity is "nbit_register";
end nbit_register_21;

architecture STRUCTURE of nbit_register_21 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_22 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_22 : entity is "nbit_register";
end nbit_register_22;

architecture STRUCTURE of nbit_register_22 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_23 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_23 : entity is "nbit_register";
end nbit_register_23;

architecture STRUCTURE of nbit_register_23 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_24 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_24 : entity is "nbit_register";
end nbit_register_24;

architecture STRUCTURE of nbit_register_24 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_25 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_25 : entity is "nbit_register";
end nbit_register_25;

architecture STRUCTURE of nbit_register_25 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_26 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_26 : entity is "nbit_register";
end nbit_register_26;

architecture STRUCTURE of nbit_register_26 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_27 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_27 : entity is "nbit_register";
end nbit_register_27;

architecture STRUCTURE of nbit_register_27 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_28 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_28 : entity is "nbit_register";
end nbit_register_28;

architecture STRUCTURE of nbit_register_28 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_29 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_29 : entity is "nbit_register";
end nbit_register_29;

architecture STRUCTURE of nbit_register_29 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_3 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_3 : entity is "nbit_register";
end nbit_register_3;

architecture STRUCTURE of nbit_register_3 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_30 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_30 : entity is "nbit_register";
end nbit_register_30;

architecture STRUCTURE of nbit_register_30 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_31 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_31 : entity is "nbit_register";
end nbit_register_31;

architecture STRUCTURE of nbit_register_31 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_32 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_32 : entity is "nbit_register";
end nbit_register_32;

architecture STRUCTURE of nbit_register_32 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_33 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_33 : entity is "nbit_register";
end nbit_register_33;

architecture STRUCTURE of nbit_register_33 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_34 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_34 : entity is "nbit_register";
end nbit_register_34;

architecture STRUCTURE of nbit_register_34 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_35 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_35 : entity is "nbit_register";
end nbit_register_35;

architecture STRUCTURE of nbit_register_35 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_36 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_36 : entity is "nbit_register";
end nbit_register_36;

architecture STRUCTURE of nbit_register_36 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_37 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_37 : entity is "nbit_register";
end nbit_register_37;

architecture STRUCTURE of nbit_register_37 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_38 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_38 : entity is "nbit_register";
end nbit_register_38;

architecture STRUCTURE of nbit_register_38 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_39 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_39 : entity is "nbit_register";
end nbit_register_39;

architecture STRUCTURE of nbit_register_39 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_4 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_4 : entity is "nbit_register";
end nbit_register_4;

architecture STRUCTURE of nbit_register_4 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_40 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_40 : entity is "nbit_register";
end nbit_register_40;

architecture STRUCTURE of nbit_register_40 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_41 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_41 : entity is "nbit_register";
end nbit_register_41;

architecture STRUCTURE of nbit_register_41 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_42 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_42 : entity is "nbit_register";
end nbit_register_42;

architecture STRUCTURE of nbit_register_42 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_43 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_43 : entity is "nbit_register";
end nbit_register_43;

architecture STRUCTURE of nbit_register_43 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_44 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_44 : entity is "nbit_register";
end nbit_register_44;

architecture STRUCTURE of nbit_register_44 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_45 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_45 : entity is "nbit_register";
end nbit_register_45;

architecture STRUCTURE of nbit_register_45 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_46 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_46 : entity is "nbit_register";
end nbit_register_46;

architecture STRUCTURE of nbit_register_46 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_47 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_47 : entity is "nbit_register";
end nbit_register_47;

architecture STRUCTURE of nbit_register_47 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_48 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_48 : entity is "nbit_register";
end nbit_register_48;

architecture STRUCTURE of nbit_register_48 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_49 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_49 : entity is "nbit_register";
end nbit_register_49;

architecture STRUCTURE of nbit_register_49 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_5 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_5 : entity is "nbit_register";
end nbit_register_5;

architecture STRUCTURE of nbit_register_5 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_50 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_50 : entity is "nbit_register";
end nbit_register_50;

architecture STRUCTURE of nbit_register_50 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_51 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_51 : entity is "nbit_register";
end nbit_register_51;

architecture STRUCTURE of nbit_register_51 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_52 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_52 : entity is "nbit_register";
end nbit_register_52;

architecture STRUCTURE of nbit_register_52 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_53 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_53 : entity is "nbit_register";
end nbit_register_53;

architecture STRUCTURE of nbit_register_53 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_54 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_54 : entity is "nbit_register";
end nbit_register_54;

architecture STRUCTURE of nbit_register_54 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_55 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_55 : entity is "nbit_register";
end nbit_register_55;

architecture STRUCTURE of nbit_register_55 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_56 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_56 : entity is "nbit_register";
end nbit_register_56;

architecture STRUCTURE of nbit_register_56 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_57 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_57 : entity is "nbit_register";
end nbit_register_57;

architecture STRUCTURE of nbit_register_57 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_58 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_58 : entity is "nbit_register";
end nbit_register_58;

architecture STRUCTURE of nbit_register_58 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_59 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_59 : entity is "nbit_register";
end nbit_register_59;

architecture STRUCTURE of nbit_register_59 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_6 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_6 : entity is "nbit_register";
end nbit_register_6;

architecture STRUCTURE of nbit_register_6 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_60 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_60 : entity is "nbit_register";
end nbit_register_60;

architecture STRUCTURE of nbit_register_60 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_61 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_61 : entity is "nbit_register";
end nbit_register_61;

architecture STRUCTURE of nbit_register_61 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_62 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_62 : entity is "nbit_register";
end nbit_register_62;

architecture STRUCTURE of nbit_register_62 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_63 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_63 : entity is "nbit_register";
end nbit_register_63;

architecture STRUCTURE of nbit_register_63 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_64 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_64 : entity is "nbit_register";
end nbit_register_64;

architecture STRUCTURE of nbit_register_64 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_65 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_65 : entity is "nbit_register";
end nbit_register_65;

architecture STRUCTURE of nbit_register_65 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_66 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_66 : entity is "nbit_register";
end nbit_register_66;

architecture STRUCTURE of nbit_register_66 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_67 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_67 : entity is "nbit_register";
end nbit_register_67;

architecture STRUCTURE of nbit_register_67 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_68 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_68 : entity is "nbit_register";
end nbit_register_68;

architecture STRUCTURE of nbit_register_68 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_69 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_69 : entity is "nbit_register";
end nbit_register_69;

architecture STRUCTURE of nbit_register_69 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_7 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_7 : entity is "nbit_register";
end nbit_register_7;

architecture STRUCTURE of nbit_register_7 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_70 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_70 : entity is "nbit_register";
end nbit_register_70;

architecture STRUCTURE of nbit_register_70 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_71 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_71 : entity is "nbit_register";
end nbit_register_71;

architecture STRUCTURE of nbit_register_71 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_72 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_72 : entity is "nbit_register";
end nbit_register_72;

architecture STRUCTURE of nbit_register_72 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_73 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_73 : entity is "nbit_register";
end nbit_register_73;

architecture STRUCTURE of nbit_register_73 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_74 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_74 : entity is "nbit_register";
end nbit_register_74;

architecture STRUCTURE of nbit_register_74 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_75 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_75 : entity is "nbit_register";
end nbit_register_75;

architecture STRUCTURE of nbit_register_75 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_76 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_76 : entity is "nbit_register";
end nbit_register_76;

architecture STRUCTURE of nbit_register_76 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_77 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_77 : entity is "nbit_register";
end nbit_register_77;

architecture STRUCTURE of nbit_register_77 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_78 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_78 : entity is "nbit_register";
end nbit_register_78;

architecture STRUCTURE of nbit_register_78 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_79 is
  port (
    S : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[2]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    A : out STD_LOGIC_VECTOR ( 4 downto 0 );
    DI : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \cnt_offset_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    cnt_offset_reg : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_79 : entity is "nbit_register";
end nbit_register_79;

architecture STRUCTURE of nbit_register_79 is
  signal \q_reg_n_0_[0]\ : STD_LOGIC;
  signal \q_reg_n_0_[1]\ : STD_LOGIC;
  signal \q_reg_n_0_[2]\ : STD_LOGIC;
  signal \q_reg_n_0_[3]\ : STD_LOGIC;
begin
\d1_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF10"
    )
        port map (
      I0 => \q_reg_n_0_[1]\,
      I1 => \q_reg_n_0_[0]\,
      I2 => cnt_offset_reg(0),
      I3 => \q_reg_n_0_[2]\,
      O => DI(1)
    );
\d1_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"9A"
    )
        port map (
      I0 => \q_reg_n_0_[1]\,
      I1 => \q_reg_n_0_[0]\,
      I2 => cnt_offset_reg(0),
      O => DI(0)
    );
\d1_carry__0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCC8CCCC"
    )
        port map (
      I0 => \q_reg_n_0_[2]\,
      I1 => \q_reg_n_0_[3]\,
      I2 => \q_reg_n_0_[1]\,
      I3 => \q_reg_n_0_[0]\,
      I4 => cnt_offset_reg(0),
      O => S(2)
    );
\d1_carry__0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"333333D3"
    )
        port map (
      I0 => \q_reg_n_0_[3]\,
      I1 => \q_reg_n_0_[2]\,
      I2 => cnt_offset_reg(0),
      I3 => \q_reg_n_0_[0]\,
      I4 => \q_reg_n_0_[1]\,
      O => S(1)
    );
\d1_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00F0FF1F"
    )
        port map (
      I0 => \q_reg_n_0_[3]\,
      I1 => \q_reg_n_0_[2]\,
      I2 => cnt_offset_reg(0),
      I3 => \q_reg_n_0_[0]\,
      I4 => \q_reg_n_0_[1]\,
      O => S(0)
    );
d1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_offset_reg(0),
      I1 => \q_reg_n_0_[0]\,
      O => \cnt_offset_reg[0]\(0)
    );
d1_carry_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"33C9CC33"
    )
        port map (
      I0 => \q_reg_n_0_[2]\,
      I1 => \q_reg_n_0_[3]\,
      I2 => \q_reg_n_0_[1]\,
      I3 => \q_reg_n_0_[0]\,
      I4 => cnt_offset_reg(0),
      O => \q_reg[2]_0\(3)
    );
d1_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5559"
    )
        port map (
      I0 => \q_reg_n_0_[2]\,
      I1 => cnt_offset_reg(0),
      I2 => \q_reg_n_0_[0]\,
      I3 => \q_reg_n_0_[1]\,
      O => \q_reg[2]_0\(2)
    );
d1_carry_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2D"
    )
        port map (
      I0 => cnt_offset_reg(0),
      I1 => \q_reg_n_0_[0]\,
      I2 => \q_reg_n_0_[1]\,
      O => \q_reg[2]_0\(1)
    );
d1_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_offset_reg(0),
      I1 => \q_reg_n_0_[0]\,
      O => \q_reg[2]_0\(0)
    );
d1_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => \q_reg_n_0_[3]\,
      I1 => \q_reg_n_0_[2]\,
      I2 => cnt_offset_reg(0),
      I3 => \q_reg_n_0_[0]\,
      I4 => \q_reg_n_0_[1]\,
      O => A(4)
    );
d1_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00FD02"
    )
        port map (
      I0 => cnt_offset_reg(0),
      I1 => \q_reg_n_0_[0]\,
      I2 => \q_reg_n_0_[1]\,
      I3 => \q_reg_n_0_[3]\,
      I4 => \q_reg_n_0_[2]\,
      O => A(3)
    );
d1_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF10"
    )
        port map (
      I0 => \q_reg_n_0_[1]\,
      I1 => \q_reg_n_0_[0]\,
      I2 => cnt_offset_reg(0),
      I3 => \q_reg_n_0_[2]\,
      O => A(2)
    );
d1_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"9A"
    )
        port map (
      I0 => \q_reg_n_0_[1]\,
      I1 => \q_reg_n_0_[0]\,
      I2 => cnt_offset_reg(0),
      O => A(1)
    );
d1_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_offset_reg(0),
      I1 => \q_reg_n_0_[0]\,
      O => A(0)
    );
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => Q(0),
      Q => \q_reg_n_0_[0]\,
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => Q(1),
      Q => \q_reg_n_0_[1]\,
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => Q(2),
      Q => \q_reg_n_0_[2]\,
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => Q(3),
      Q => \q_reg_n_0_[3]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_8 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_8 : entity is "nbit_register";
end nbit_register_8;

architecture STRUCTURE of nbit_register_8 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity nbit_register_9 is
  port (
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of nbit_register_9 : entity is "nbit_register";
end nbit_register_9;

architecture STRUCTURE of nbit_register_9 is
begin
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(0),
      Q => \q_reg[3]_0\(0),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(1),
      Q => \q_reg[3]_0\(1),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(2),
      Q => \q_reg[3]_0\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => D(3),
      Q => \q_reg[3]_0\(3),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \nbit_register__parameterized0\ is
  port (
    S : out STD_LOGIC_VECTOR ( 2 downto 0 );
    q_reg_i_64_0 : out STD_LOGIC;
    q_reg_i_163_0 : out STD_LOGIC;
    \q_reg[137]_0\ : out STD_LOGIC;
    DI : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[271]_0\ : out STD_LOGIC;
    \q_reg[109]_0\ : out STD_LOGIC;
    q_reg_i_78_0 : out STD_LOGIC;
    q_reg_i_47_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[34]_0\ : out STD_LOGIC;
    q_reg_i_96_0 : out STD_LOGIC;
    q_reg_i_93_0 : out STD_LOGIC;
    \q_reg[55]_0\ : out STD_LOGIC;
    \q_reg[295]_0\ : out STD_LOGIC;
    \q_reg[43]_0\ : out STD_LOGIC;
    \q_reg[31]_0\ : out STD_LOGIC;
    \q_reg[91]_0\ : out STD_LOGIC;
    q_reg_i_75_0 : out STD_LOGIC;
    \q_reg[188]_0\ : out STD_LOGIC;
    \q_reg[131]_0\ : out STD_LOGIC;
    \q_reg[132]_0\ : out STD_LOGIC;
    \q_reg[295]_1\ : out STD_LOGIC;
    \q_reg[19]_0\ : out STD_LOGIC;
    \q_reg[57]_0\ : out STD_LOGIC;
    \q_reg[77]_0\ : out STD_LOGIC;
    \q_reg[184]_0\ : out STD_LOGIC;
    \q_reg[215]_0\ : out STD_LOGIC;
    \q_reg[247]_0\ : out STD_LOGIC;
    \q_reg[239]_0\ : out STD_LOGIC;
    q_reg : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[319]_0\ : in STD_LOGIC_VECTOR ( 319 downto 0 );
    \q_reg[0]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \nbit_register__parameterized0\ : entity is "nbit_register";
end \nbit_register__parameterized0\;

architecture STRUCTURE of \nbit_register__parameterized0\ is
  signal \^di\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pre_reg_cnt : STD_LOGIC_VECTOR ( 319 downto 0 );
  signal \^q_reg[109]_0\ : STD_LOGIC;
  signal \^q_reg[131]_0\ : STD_LOGIC;
  signal \^q_reg[132]_0\ : STD_LOGIC;
  signal \^q_reg[137]_0\ : STD_LOGIC;
  signal \^q_reg[188]_0\ : STD_LOGIC;
  signal \^q_reg[271]_0\ : STD_LOGIC;
  signal \^q_reg[295]_0\ : STD_LOGIC;
  signal \^q_reg[295]_1\ : STD_LOGIC;
  signal \^q_reg[31]_0\ : STD_LOGIC;
  signal \^q_reg[34]_0\ : STD_LOGIC;
  signal \^q_reg[43]_0\ : STD_LOGIC;
  signal \^q_reg[55]_0\ : STD_LOGIC;
  signal \^q_reg[91]_0\ : STD_LOGIC;
  signal q_reg_i_100_n_0 : STD_LOGIC;
  signal q_reg_i_101_n_0 : STD_LOGIC;
  signal q_reg_i_102_n_0 : STD_LOGIC;
  signal q_reg_i_103_n_0 : STD_LOGIC;
  signal q_reg_i_104_n_0 : STD_LOGIC;
  signal q_reg_i_105_n_0 : STD_LOGIC;
  signal q_reg_i_106_n_0 : STD_LOGIC;
  signal q_reg_i_107_n_0 : STD_LOGIC;
  signal q_reg_i_108_n_0 : STD_LOGIC;
  signal q_reg_i_109_n_0 : STD_LOGIC;
  signal q_reg_i_110_n_0 : STD_LOGIC;
  signal q_reg_i_111_n_0 : STD_LOGIC;
  signal q_reg_i_112_n_0 : STD_LOGIC;
  signal q_reg_i_113_n_0 : STD_LOGIC;
  signal q_reg_i_114_n_0 : STD_LOGIC;
  signal q_reg_i_115_n_0 : STD_LOGIC;
  signal q_reg_i_116_n_0 : STD_LOGIC;
  signal q_reg_i_117_n_0 : STD_LOGIC;
  signal q_reg_i_118_n_0 : STD_LOGIC;
  signal q_reg_i_119_n_0 : STD_LOGIC;
  signal q_reg_i_120_n_0 : STD_LOGIC;
  signal q_reg_i_121_n_0 : STD_LOGIC;
  signal q_reg_i_122_n_0 : STD_LOGIC;
  signal q_reg_i_123_n_0 : STD_LOGIC;
  signal q_reg_i_124_n_0 : STD_LOGIC;
  signal q_reg_i_125_n_0 : STD_LOGIC;
  signal q_reg_i_126_n_0 : STD_LOGIC;
  signal q_reg_i_127_n_0 : STD_LOGIC;
  signal q_reg_i_128_n_0 : STD_LOGIC;
  signal q_reg_i_129_n_0 : STD_LOGIC;
  signal q_reg_i_130_n_0 : STD_LOGIC;
  signal q_reg_i_131_n_0 : STD_LOGIC;
  signal q_reg_i_132_n_0 : STD_LOGIC;
  signal q_reg_i_133_n_0 : STD_LOGIC;
  signal q_reg_i_134_n_0 : STD_LOGIC;
  signal q_reg_i_135_n_0 : STD_LOGIC;
  signal q_reg_i_136_n_0 : STD_LOGIC;
  signal q_reg_i_137_n_0 : STD_LOGIC;
  signal q_reg_i_138_n_0 : STD_LOGIC;
  signal q_reg_i_139_n_0 : STD_LOGIC;
  signal q_reg_i_140_n_0 : STD_LOGIC;
  signal q_reg_i_141_n_0 : STD_LOGIC;
  signal q_reg_i_142_n_0 : STD_LOGIC;
  signal q_reg_i_143_n_0 : STD_LOGIC;
  signal q_reg_i_144_n_0 : STD_LOGIC;
  signal q_reg_i_145_n_0 : STD_LOGIC;
  signal q_reg_i_146_n_0 : STD_LOGIC;
  signal q_reg_i_147_n_0 : STD_LOGIC;
  signal q_reg_i_148_n_0 : STD_LOGIC;
  signal q_reg_i_149_n_0 : STD_LOGIC;
  signal q_reg_i_150_n_0 : STD_LOGIC;
  signal q_reg_i_151_n_0 : STD_LOGIC;
  signal q_reg_i_152_n_0 : STD_LOGIC;
  signal q_reg_i_153_n_0 : STD_LOGIC;
  signal q_reg_i_154_n_0 : STD_LOGIC;
  signal q_reg_i_155_n_0 : STD_LOGIC;
  signal q_reg_i_156_n_0 : STD_LOGIC;
  signal q_reg_i_157_n_0 : STD_LOGIC;
  signal q_reg_i_158_n_0 : STD_LOGIC;
  signal q_reg_i_159_n_0 : STD_LOGIC;
  signal q_reg_i_160_n_0 : STD_LOGIC;
  signal q_reg_i_161_n_0 : STD_LOGIC;
  signal q_reg_i_162_n_0 : STD_LOGIC;
  signal \^q_reg_i_163_0\ : STD_LOGIC;
  signal q_reg_i_163_n_0 : STD_LOGIC;
  signal q_reg_i_164_n_0 : STD_LOGIC;
  signal q_reg_i_165_n_0 : STD_LOGIC;
  signal q_reg_i_166_n_0 : STD_LOGIC;
  signal q_reg_i_167_n_0 : STD_LOGIC;
  signal q_reg_i_168_n_0 : STD_LOGIC;
  signal q_reg_i_169_n_0 : STD_LOGIC;
  signal q_reg_i_170_n_0 : STD_LOGIC;
  signal q_reg_i_171_n_0 : STD_LOGIC;
  signal q_reg_i_172_n_0 : STD_LOGIC;
  signal q_reg_i_173_n_0 : STD_LOGIC;
  signal q_reg_i_174_n_0 : STD_LOGIC;
  signal q_reg_i_175_n_0 : STD_LOGIC;
  signal q_reg_i_176_n_0 : STD_LOGIC;
  signal q_reg_i_177_n_0 : STD_LOGIC;
  signal q_reg_i_178_n_0 : STD_LOGIC;
  signal q_reg_i_179_n_0 : STD_LOGIC;
  signal q_reg_i_180_n_0 : STD_LOGIC;
  signal q_reg_i_181_n_0 : STD_LOGIC;
  signal q_reg_i_182_n_0 : STD_LOGIC;
  signal q_reg_i_183_n_0 : STD_LOGIC;
  signal q_reg_i_184_n_0 : STD_LOGIC;
  signal q_reg_i_185_n_0 : STD_LOGIC;
  signal q_reg_i_186_n_0 : STD_LOGIC;
  signal q_reg_i_187_n_0 : STD_LOGIC;
  signal q_reg_i_188_n_0 : STD_LOGIC;
  signal q_reg_i_189_n_0 : STD_LOGIC;
  signal q_reg_i_190_n_0 : STD_LOGIC;
  signal q_reg_i_191_n_0 : STD_LOGIC;
  signal q_reg_i_192_n_0 : STD_LOGIC;
  signal q_reg_i_193_n_0 : STD_LOGIC;
  signal q_reg_i_194_n_0 : STD_LOGIC;
  signal q_reg_i_195_n_0 : STD_LOGIC;
  signal q_reg_i_196_n_0 : STD_LOGIC;
  signal q_reg_i_197_n_0 : STD_LOGIC;
  signal q_reg_i_198_n_0 : STD_LOGIC;
  signal q_reg_i_199_n_0 : STD_LOGIC;
  signal q_reg_i_200_n_0 : STD_LOGIC;
  signal q_reg_i_201_n_0 : STD_LOGIC;
  signal q_reg_i_202_n_0 : STD_LOGIC;
  signal q_reg_i_203_n_0 : STD_LOGIC;
  signal q_reg_i_204_n_0 : STD_LOGIC;
  signal q_reg_i_205_n_0 : STD_LOGIC;
  signal q_reg_i_206_n_0 : STD_LOGIC;
  signal q_reg_i_207_n_0 : STD_LOGIC;
  signal q_reg_i_208_n_0 : STD_LOGIC;
  signal q_reg_i_209_n_0 : STD_LOGIC;
  signal q_reg_i_210_n_0 : STD_LOGIC;
  signal q_reg_i_211_n_0 : STD_LOGIC;
  signal q_reg_i_212_n_0 : STD_LOGIC;
  signal q_reg_i_213_n_0 : STD_LOGIC;
  signal q_reg_i_214_n_0 : STD_LOGIC;
  signal q_reg_i_215_n_0 : STD_LOGIC;
  signal q_reg_i_216_n_0 : STD_LOGIC;
  signal q_reg_i_217_n_0 : STD_LOGIC;
  signal q_reg_i_218_n_0 : STD_LOGIC;
  signal q_reg_i_219_n_0 : STD_LOGIC;
  signal q_reg_i_220_n_0 : STD_LOGIC;
  signal q_reg_i_221_n_0 : STD_LOGIC;
  signal q_reg_i_222_n_0 : STD_LOGIC;
  signal q_reg_i_223_n_0 : STD_LOGIC;
  signal q_reg_i_224_n_0 : STD_LOGIC;
  signal q_reg_i_225_n_0 : STD_LOGIC;
  signal q_reg_i_226_n_0 : STD_LOGIC;
  signal q_reg_i_227_n_0 : STD_LOGIC;
  signal q_reg_i_228_n_0 : STD_LOGIC;
  signal q_reg_i_229_n_0 : STD_LOGIC;
  signal q_reg_i_230_n_0 : STD_LOGIC;
  signal q_reg_i_231_n_0 : STD_LOGIC;
  signal q_reg_i_232_n_0 : STD_LOGIC;
  signal q_reg_i_233_n_0 : STD_LOGIC;
  signal q_reg_i_234_n_0 : STD_LOGIC;
  signal q_reg_i_235_n_0 : STD_LOGIC;
  signal q_reg_i_236_n_0 : STD_LOGIC;
  signal q_reg_i_237_n_0 : STD_LOGIC;
  signal q_reg_i_238_n_0 : STD_LOGIC;
  signal q_reg_i_239_n_0 : STD_LOGIC;
  signal q_reg_i_240_n_0 : STD_LOGIC;
  signal q_reg_i_241_n_0 : STD_LOGIC;
  signal q_reg_i_242_n_0 : STD_LOGIC;
  signal q_reg_i_243_n_0 : STD_LOGIC;
  signal q_reg_i_244_n_0 : STD_LOGIC;
  signal q_reg_i_245_n_0 : STD_LOGIC;
  signal q_reg_i_246_n_0 : STD_LOGIC;
  signal q_reg_i_247_n_0 : STD_LOGIC;
  signal q_reg_i_248_n_0 : STD_LOGIC;
  signal q_reg_i_249_n_0 : STD_LOGIC;
  signal q_reg_i_250_n_0 : STD_LOGIC;
  signal q_reg_i_251_n_0 : STD_LOGIC;
  signal q_reg_i_252_n_0 : STD_LOGIC;
  signal q_reg_i_253_n_0 : STD_LOGIC;
  signal q_reg_i_254_n_0 : STD_LOGIC;
  signal q_reg_i_255_n_0 : STD_LOGIC;
  signal q_reg_i_256_n_0 : STD_LOGIC;
  signal q_reg_i_257_n_0 : STD_LOGIC;
  signal q_reg_i_258_n_0 : STD_LOGIC;
  signal q_reg_i_259_n_0 : STD_LOGIC;
  signal q_reg_i_260_n_0 : STD_LOGIC;
  signal q_reg_i_261_n_0 : STD_LOGIC;
  signal q_reg_i_262_n_0 : STD_LOGIC;
  signal q_reg_i_263_n_0 : STD_LOGIC;
  signal q_reg_i_264_n_0 : STD_LOGIC;
  signal q_reg_i_265_n_0 : STD_LOGIC;
  signal q_reg_i_266_n_0 : STD_LOGIC;
  signal q_reg_i_267_n_0 : STD_LOGIC;
  signal q_reg_i_268_n_0 : STD_LOGIC;
  signal q_reg_i_269_n_0 : STD_LOGIC;
  signal q_reg_i_270_n_0 : STD_LOGIC;
  signal q_reg_i_271_n_0 : STD_LOGIC;
  signal q_reg_i_272_n_0 : STD_LOGIC;
  signal q_reg_i_273_n_0 : STD_LOGIC;
  signal q_reg_i_274_n_0 : STD_LOGIC;
  signal q_reg_i_275_n_0 : STD_LOGIC;
  signal q_reg_i_276_n_0 : STD_LOGIC;
  signal q_reg_i_277_n_0 : STD_LOGIC;
  signal q_reg_i_278_n_0 : STD_LOGIC;
  signal q_reg_i_279_n_0 : STD_LOGIC;
  signal q_reg_i_280_n_0 : STD_LOGIC;
  signal q_reg_i_281_n_0 : STD_LOGIC;
  signal q_reg_i_282_n_0 : STD_LOGIC;
  signal q_reg_i_283_n_0 : STD_LOGIC;
  signal q_reg_i_284_n_0 : STD_LOGIC;
  signal q_reg_i_285_n_0 : STD_LOGIC;
  signal q_reg_i_286_n_0 : STD_LOGIC;
  signal q_reg_i_287_n_0 : STD_LOGIC;
  signal q_reg_i_288_n_0 : STD_LOGIC;
  signal q_reg_i_289_n_0 : STD_LOGIC;
  signal q_reg_i_290_n_0 : STD_LOGIC;
  signal q_reg_i_291_n_0 : STD_LOGIC;
  signal q_reg_i_292_n_0 : STD_LOGIC;
  signal q_reg_i_293_n_0 : STD_LOGIC;
  signal q_reg_i_294_n_0 : STD_LOGIC;
  signal q_reg_i_295_n_0 : STD_LOGIC;
  signal q_reg_i_296_n_0 : STD_LOGIC;
  signal q_reg_i_297_n_0 : STD_LOGIC;
  signal q_reg_i_298_n_0 : STD_LOGIC;
  signal q_reg_i_299_n_0 : STD_LOGIC;
  signal q_reg_i_300_n_0 : STD_LOGIC;
  signal q_reg_i_301_n_0 : STD_LOGIC;
  signal q_reg_i_302_n_0 : STD_LOGIC;
  signal q_reg_i_303_n_0 : STD_LOGIC;
  signal q_reg_i_304_n_0 : STD_LOGIC;
  signal q_reg_i_305_n_0 : STD_LOGIC;
  signal q_reg_i_306_n_0 : STD_LOGIC;
  signal q_reg_i_307_n_0 : STD_LOGIC;
  signal q_reg_i_308_n_0 : STD_LOGIC;
  signal q_reg_i_309_n_0 : STD_LOGIC;
  signal q_reg_i_310_n_0 : STD_LOGIC;
  signal q_reg_i_311_n_0 : STD_LOGIC;
  signal q_reg_i_312_n_0 : STD_LOGIC;
  signal q_reg_i_313_n_0 : STD_LOGIC;
  signal q_reg_i_314_n_0 : STD_LOGIC;
  signal q_reg_i_315_n_0 : STD_LOGIC;
  signal q_reg_i_316_n_0 : STD_LOGIC;
  signal q_reg_i_317_n_0 : STD_LOGIC;
  signal q_reg_i_318_n_0 : STD_LOGIC;
  signal q_reg_i_319_n_0 : STD_LOGIC;
  signal q_reg_i_320_n_0 : STD_LOGIC;
  signal q_reg_i_321_n_0 : STD_LOGIC;
  signal q_reg_i_322_n_0 : STD_LOGIC;
  signal q_reg_i_323_n_0 : STD_LOGIC;
  signal q_reg_i_324_n_0 : STD_LOGIC;
  signal q_reg_i_325_n_0 : STD_LOGIC;
  signal q_reg_i_326_n_0 : STD_LOGIC;
  signal q_reg_i_327_n_0 : STD_LOGIC;
  signal q_reg_i_328_n_0 : STD_LOGIC;
  signal q_reg_i_329_n_0 : STD_LOGIC;
  signal q_reg_i_330_n_0 : STD_LOGIC;
  signal q_reg_i_331_n_0 : STD_LOGIC;
  signal q_reg_i_332_n_0 : STD_LOGIC;
  signal q_reg_i_333_n_0 : STD_LOGIC;
  signal q_reg_i_334_n_0 : STD_LOGIC;
  signal q_reg_i_335_n_0 : STD_LOGIC;
  signal q_reg_i_336_n_0 : STD_LOGIC;
  signal q_reg_i_337_n_0 : STD_LOGIC;
  signal q_reg_i_338_n_0 : STD_LOGIC;
  signal q_reg_i_339_n_0 : STD_LOGIC;
  signal q_reg_i_340_n_0 : STD_LOGIC;
  signal q_reg_i_341_n_0 : STD_LOGIC;
  signal q_reg_i_342_n_0 : STD_LOGIC;
  signal q_reg_i_343_n_0 : STD_LOGIC;
  signal q_reg_i_344_n_0 : STD_LOGIC;
  signal q_reg_i_345_n_0 : STD_LOGIC;
  signal q_reg_i_346_n_0 : STD_LOGIC;
  signal q_reg_i_347_n_0 : STD_LOGIC;
  signal q_reg_i_348_n_0 : STD_LOGIC;
  signal q_reg_i_349_n_0 : STD_LOGIC;
  signal q_reg_i_350_n_0 : STD_LOGIC;
  signal q_reg_i_351_n_0 : STD_LOGIC;
  signal q_reg_i_352_n_0 : STD_LOGIC;
  signal q_reg_i_353_n_0 : STD_LOGIC;
  signal q_reg_i_354_n_0 : STD_LOGIC;
  signal q_reg_i_355_n_0 : STD_LOGIC;
  signal q_reg_i_356_n_0 : STD_LOGIC;
  signal q_reg_i_357_n_0 : STD_LOGIC;
  signal q_reg_i_358_n_0 : STD_LOGIC;
  signal q_reg_i_359_n_0 : STD_LOGIC;
  signal q_reg_i_360_n_0 : STD_LOGIC;
  signal q_reg_i_361_n_0 : STD_LOGIC;
  signal q_reg_i_362_n_0 : STD_LOGIC;
  signal q_reg_i_363_n_0 : STD_LOGIC;
  signal q_reg_i_364_n_0 : STD_LOGIC;
  signal q_reg_i_365_n_0 : STD_LOGIC;
  signal q_reg_i_366_n_0 : STD_LOGIC;
  signal q_reg_i_367_n_0 : STD_LOGIC;
  signal q_reg_i_368_n_0 : STD_LOGIC;
  signal q_reg_i_369_n_0 : STD_LOGIC;
  signal q_reg_i_370_n_0 : STD_LOGIC;
  signal q_reg_i_371_n_0 : STD_LOGIC;
  signal q_reg_i_372_n_0 : STD_LOGIC;
  signal q_reg_i_373_n_0 : STD_LOGIC;
  signal q_reg_i_374_n_0 : STD_LOGIC;
  signal q_reg_i_375_n_0 : STD_LOGIC;
  signal q_reg_i_376_n_0 : STD_LOGIC;
  signal q_reg_i_377_n_0 : STD_LOGIC;
  signal q_reg_i_378_n_0 : STD_LOGIC;
  signal q_reg_i_379_n_0 : STD_LOGIC;
  signal q_reg_i_380_n_0 : STD_LOGIC;
  signal q_reg_i_381_n_0 : STD_LOGIC;
  signal q_reg_i_382_n_0 : STD_LOGIC;
  signal q_reg_i_383_n_0 : STD_LOGIC;
  signal q_reg_i_384_n_0 : STD_LOGIC;
  signal q_reg_i_385_n_0 : STD_LOGIC;
  signal q_reg_i_386_n_0 : STD_LOGIC;
  signal q_reg_i_387_n_0 : STD_LOGIC;
  signal q_reg_i_388_n_0 : STD_LOGIC;
  signal q_reg_i_389_n_0 : STD_LOGIC;
  signal q_reg_i_390_n_0 : STD_LOGIC;
  signal q_reg_i_391_n_0 : STD_LOGIC;
  signal q_reg_i_392_n_0 : STD_LOGIC;
  signal q_reg_i_393_n_0 : STD_LOGIC;
  signal q_reg_i_394_n_0 : STD_LOGIC;
  signal q_reg_i_395_n_0 : STD_LOGIC;
  signal q_reg_i_396_n_0 : STD_LOGIC;
  signal q_reg_i_397_n_0 : STD_LOGIC;
  signal q_reg_i_398_n_0 : STD_LOGIC;
  signal q_reg_i_399_n_0 : STD_LOGIC;
  signal q_reg_i_400_n_0 : STD_LOGIC;
  signal q_reg_i_401_n_0 : STD_LOGIC;
  signal q_reg_i_402_n_0 : STD_LOGIC;
  signal q_reg_i_403_n_0 : STD_LOGIC;
  signal q_reg_i_404_n_0 : STD_LOGIC;
  signal q_reg_i_405_n_0 : STD_LOGIC;
  signal q_reg_i_406_n_0 : STD_LOGIC;
  signal q_reg_i_407_n_0 : STD_LOGIC;
  signal q_reg_i_408_n_0 : STD_LOGIC;
  signal q_reg_i_409_n_0 : STD_LOGIC;
  signal q_reg_i_410_n_0 : STD_LOGIC;
  signal q_reg_i_411_n_0 : STD_LOGIC;
  signal q_reg_i_412_n_0 : STD_LOGIC;
  signal q_reg_i_413_n_0 : STD_LOGIC;
  signal q_reg_i_414_n_0 : STD_LOGIC;
  signal q_reg_i_415_n_0 : STD_LOGIC;
  signal q_reg_i_416_n_0 : STD_LOGIC;
  signal q_reg_i_417_n_0 : STD_LOGIC;
  signal q_reg_i_418_n_0 : STD_LOGIC;
  signal q_reg_i_419_n_0 : STD_LOGIC;
  signal q_reg_i_420_n_0 : STD_LOGIC;
  signal q_reg_i_421_n_0 : STD_LOGIC;
  signal q_reg_i_422_n_0 : STD_LOGIC;
  signal q_reg_i_423_n_0 : STD_LOGIC;
  signal q_reg_i_424_n_0 : STD_LOGIC;
  signal q_reg_i_425_n_0 : STD_LOGIC;
  signal q_reg_i_426_n_0 : STD_LOGIC;
  signal q_reg_i_427_n_0 : STD_LOGIC;
  signal q_reg_i_428_n_0 : STD_LOGIC;
  signal q_reg_i_429_n_0 : STD_LOGIC;
  signal q_reg_i_430_n_0 : STD_LOGIC;
  signal q_reg_i_431_n_0 : STD_LOGIC;
  signal q_reg_i_432_n_0 : STD_LOGIC;
  signal q_reg_i_433_n_0 : STD_LOGIC;
  signal q_reg_i_434_n_0 : STD_LOGIC;
  signal q_reg_i_435_n_0 : STD_LOGIC;
  signal q_reg_i_436_n_0 : STD_LOGIC;
  signal q_reg_i_437_n_0 : STD_LOGIC;
  signal q_reg_i_438_n_0 : STD_LOGIC;
  signal q_reg_i_439_n_0 : STD_LOGIC;
  signal q_reg_i_440_n_0 : STD_LOGIC;
  signal q_reg_i_441_n_0 : STD_LOGIC;
  signal q_reg_i_442_n_0 : STD_LOGIC;
  signal q_reg_i_443_n_0 : STD_LOGIC;
  signal q_reg_i_444_n_0 : STD_LOGIC;
  signal q_reg_i_445_n_0 : STD_LOGIC;
  signal q_reg_i_446_n_0 : STD_LOGIC;
  signal q_reg_i_447_n_0 : STD_LOGIC;
  signal q_reg_i_448_n_0 : STD_LOGIC;
  signal q_reg_i_449_n_0 : STD_LOGIC;
  signal q_reg_i_450_n_0 : STD_LOGIC;
  signal q_reg_i_451_n_0 : STD_LOGIC;
  signal q_reg_i_452_n_0 : STD_LOGIC;
  signal q_reg_i_453_n_0 : STD_LOGIC;
  signal q_reg_i_454_n_0 : STD_LOGIC;
  signal q_reg_i_455_n_0 : STD_LOGIC;
  signal q_reg_i_456_n_0 : STD_LOGIC;
  signal q_reg_i_457_n_0 : STD_LOGIC;
  signal q_reg_i_458_n_0 : STD_LOGIC;
  signal q_reg_i_459_n_0 : STD_LOGIC;
  signal q_reg_i_460_n_0 : STD_LOGIC;
  signal q_reg_i_461_n_0 : STD_LOGIC;
  signal q_reg_i_462_n_0 : STD_LOGIC;
  signal q_reg_i_463_n_0 : STD_LOGIC;
  signal q_reg_i_464_n_0 : STD_LOGIC;
  signal q_reg_i_465_n_0 : STD_LOGIC;
  signal q_reg_i_466_n_0 : STD_LOGIC;
  signal q_reg_i_467_n_0 : STD_LOGIC;
  signal q_reg_i_468_n_0 : STD_LOGIC;
  signal q_reg_i_469_n_0 : STD_LOGIC;
  signal q_reg_i_46_n_0 : STD_LOGIC;
  signal q_reg_i_470_n_0 : STD_LOGIC;
  signal q_reg_i_471_n_0 : STD_LOGIC;
  signal q_reg_i_472_n_0 : STD_LOGIC;
  signal q_reg_i_473_n_0 : STD_LOGIC;
  signal q_reg_i_474_n_0 : STD_LOGIC;
  signal q_reg_i_475_n_0 : STD_LOGIC;
  signal q_reg_i_476_n_0 : STD_LOGIC;
  signal q_reg_i_477_n_0 : STD_LOGIC;
  signal q_reg_i_478_n_0 : STD_LOGIC;
  signal q_reg_i_479_n_0 : STD_LOGIC;
  signal q_reg_i_47_n_0 : STD_LOGIC;
  signal q_reg_i_480_n_0 : STD_LOGIC;
  signal q_reg_i_481_n_0 : STD_LOGIC;
  signal q_reg_i_482_n_0 : STD_LOGIC;
  signal q_reg_i_483_n_0 : STD_LOGIC;
  signal q_reg_i_484_n_0 : STD_LOGIC;
  signal q_reg_i_485_n_0 : STD_LOGIC;
  signal q_reg_i_486_n_0 : STD_LOGIC;
  signal q_reg_i_487_n_0 : STD_LOGIC;
  signal q_reg_i_488_n_0 : STD_LOGIC;
  signal q_reg_i_489_n_0 : STD_LOGIC;
  signal q_reg_i_48_n_0 : STD_LOGIC;
  signal q_reg_i_490_n_0 : STD_LOGIC;
  signal q_reg_i_491_n_0 : STD_LOGIC;
  signal q_reg_i_492_n_0 : STD_LOGIC;
  signal q_reg_i_493_n_0 : STD_LOGIC;
  signal q_reg_i_494_n_0 : STD_LOGIC;
  signal q_reg_i_495_n_0 : STD_LOGIC;
  signal q_reg_i_496_n_0 : STD_LOGIC;
  signal q_reg_i_497_n_0 : STD_LOGIC;
  signal q_reg_i_498_n_0 : STD_LOGIC;
  signal q_reg_i_499_n_0 : STD_LOGIC;
  signal q_reg_i_49_n_0 : STD_LOGIC;
  signal q_reg_i_500_n_0 : STD_LOGIC;
  signal q_reg_i_501_n_0 : STD_LOGIC;
  signal q_reg_i_502_n_0 : STD_LOGIC;
  signal q_reg_i_503_n_0 : STD_LOGIC;
  signal q_reg_i_504_n_0 : STD_LOGIC;
  signal q_reg_i_505_n_0 : STD_LOGIC;
  signal q_reg_i_506_n_0 : STD_LOGIC;
  signal q_reg_i_507_n_0 : STD_LOGIC;
  signal q_reg_i_508_n_0 : STD_LOGIC;
  signal q_reg_i_509_n_0 : STD_LOGIC;
  signal q_reg_i_510_n_0 : STD_LOGIC;
  signal q_reg_i_511_n_0 : STD_LOGIC;
  signal q_reg_i_512_n_0 : STD_LOGIC;
  signal q_reg_i_513_n_0 : STD_LOGIC;
  signal q_reg_i_514_n_0 : STD_LOGIC;
  signal q_reg_i_515_n_0 : STD_LOGIC;
  signal q_reg_i_516_n_0 : STD_LOGIC;
  signal q_reg_i_517_n_0 : STD_LOGIC;
  signal q_reg_i_518_n_0 : STD_LOGIC;
  signal q_reg_i_519_n_0 : STD_LOGIC;
  signal q_reg_i_520_n_0 : STD_LOGIC;
  signal q_reg_i_521_n_0 : STD_LOGIC;
  signal q_reg_i_522_n_0 : STD_LOGIC;
  signal q_reg_i_523_n_0 : STD_LOGIC;
  signal q_reg_i_524_n_0 : STD_LOGIC;
  signal q_reg_i_525_n_0 : STD_LOGIC;
  signal q_reg_i_526_n_0 : STD_LOGIC;
  signal q_reg_i_527_n_0 : STD_LOGIC;
  signal q_reg_i_528_n_0 : STD_LOGIC;
  signal q_reg_i_529_n_0 : STD_LOGIC;
  signal q_reg_i_530_n_0 : STD_LOGIC;
  signal q_reg_i_531_n_0 : STD_LOGIC;
  signal q_reg_i_532_n_0 : STD_LOGIC;
  signal q_reg_i_533_n_0 : STD_LOGIC;
  signal q_reg_i_534_n_0 : STD_LOGIC;
  signal q_reg_i_535_n_0 : STD_LOGIC;
  signal q_reg_i_536_n_0 : STD_LOGIC;
  signal q_reg_i_537_n_0 : STD_LOGIC;
  signal q_reg_i_538_n_0 : STD_LOGIC;
  signal q_reg_i_539_n_0 : STD_LOGIC;
  signal q_reg_i_540_n_0 : STD_LOGIC;
  signal q_reg_i_541_n_0 : STD_LOGIC;
  signal q_reg_i_542_n_0 : STD_LOGIC;
  signal q_reg_i_543_n_0 : STD_LOGIC;
  signal q_reg_i_544_n_0 : STD_LOGIC;
  signal q_reg_i_545_n_0 : STD_LOGIC;
  signal q_reg_i_546_n_0 : STD_LOGIC;
  signal q_reg_i_547_n_0 : STD_LOGIC;
  signal q_reg_i_548_n_0 : STD_LOGIC;
  signal q_reg_i_549_n_0 : STD_LOGIC;
  signal q_reg_i_550_n_0 : STD_LOGIC;
  signal q_reg_i_551_n_0 : STD_LOGIC;
  signal q_reg_i_552_n_0 : STD_LOGIC;
  signal q_reg_i_553_n_0 : STD_LOGIC;
  signal q_reg_i_554_n_0 : STD_LOGIC;
  signal q_reg_i_555_n_0 : STD_LOGIC;
  signal q_reg_i_556_n_0 : STD_LOGIC;
  signal q_reg_i_557_n_0 : STD_LOGIC;
  signal q_reg_i_558_n_0 : STD_LOGIC;
  signal q_reg_i_559_n_0 : STD_LOGIC;
  signal q_reg_i_560_n_0 : STD_LOGIC;
  signal q_reg_i_561_n_0 : STD_LOGIC;
  signal q_reg_i_562_n_0 : STD_LOGIC;
  signal q_reg_i_563_n_0 : STD_LOGIC;
  signal q_reg_i_564_n_0 : STD_LOGIC;
  signal q_reg_i_565_n_0 : STD_LOGIC;
  signal q_reg_i_566_n_0 : STD_LOGIC;
  signal q_reg_i_567_n_0 : STD_LOGIC;
  signal q_reg_i_568_n_0 : STD_LOGIC;
  signal q_reg_i_569_n_0 : STD_LOGIC;
  signal q_reg_i_570_n_0 : STD_LOGIC;
  signal q_reg_i_571_n_0 : STD_LOGIC;
  signal q_reg_i_59_n_0 : STD_LOGIC;
  signal q_reg_i_60_n_0 : STD_LOGIC;
  signal q_reg_i_61_n_0 : STD_LOGIC;
  signal q_reg_i_62_n_0 : STD_LOGIC;
  signal q_reg_i_63_n_0 : STD_LOGIC;
  signal \^q_reg_i_64_0\ : STD_LOGIC;
  signal q_reg_i_64_n_0 : STD_LOGIC;
  signal q_reg_i_65_n_0 : STD_LOGIC;
  signal q_reg_i_66_n_0 : STD_LOGIC;
  signal q_reg_i_67_n_0 : STD_LOGIC;
  signal q_reg_i_68_n_0 : STD_LOGIC;
  signal q_reg_i_69_n_0 : STD_LOGIC;
  signal q_reg_i_70_n_0 : STD_LOGIC;
  signal q_reg_i_71_n_0 : STD_LOGIC;
  signal q_reg_i_72_n_0 : STD_LOGIC;
  signal q_reg_i_73_n_0 : STD_LOGIC;
  signal q_reg_i_74_n_0 : STD_LOGIC;
  signal q_reg_i_75_n_0 : STD_LOGIC;
  signal q_reg_i_76_n_0 : STD_LOGIC;
  signal q_reg_i_77_n_0 : STD_LOGIC;
  signal \^q_reg_i_78_0\ : STD_LOGIC;
  signal q_reg_i_78_n_0 : STD_LOGIC;
  signal q_reg_i_79_n_0 : STD_LOGIC;
  signal q_reg_i_80_n_0 : STD_LOGIC;
  signal q_reg_i_81_n_0 : STD_LOGIC;
  signal q_reg_i_82_n_0 : STD_LOGIC;
  signal q_reg_i_83_n_0 : STD_LOGIC;
  signal q_reg_i_84_n_0 : STD_LOGIC;
  signal q_reg_i_85_n_0 : STD_LOGIC;
  signal q_reg_i_86_n_0 : STD_LOGIC;
  signal q_reg_i_87_n_0 : STD_LOGIC;
  signal q_reg_i_88_n_0 : STD_LOGIC;
  signal q_reg_i_89_n_0 : STD_LOGIC;
  signal q_reg_i_90_n_0 : STD_LOGIC;
  signal q_reg_i_91_n_0 : STD_LOGIC;
  signal q_reg_i_92_n_0 : STD_LOGIC;
  signal \^q_reg_i_93_0\ : STD_LOGIC;
  signal q_reg_i_93_n_0 : STD_LOGIC;
  signal q_reg_i_94_n_0 : STD_LOGIC;
  signal q_reg_i_95_n_0 : STD_LOGIC;
  signal \^q_reg_i_96_0\ : STD_LOGIC;
  signal q_reg_i_96_n_0 : STD_LOGIC;
  signal q_reg_i_97_n_0 : STD_LOGIC;
  signal q_reg_i_98_n_0 : STD_LOGIC;
  signal q_reg_i_99_n_0 : STD_LOGIC;
begin
  DI(1 downto 0) <= \^di\(1 downto 0);
  \q_reg[109]_0\ <= \^q_reg[109]_0\;
  \q_reg[131]_0\ <= \^q_reg[131]_0\;
  \q_reg[132]_0\ <= \^q_reg[132]_0\;
  \q_reg[137]_0\ <= \^q_reg[137]_0\;
  \q_reg[188]_0\ <= \^q_reg[188]_0\;
  \q_reg[271]_0\ <= \^q_reg[271]_0\;
  \q_reg[295]_0\ <= \^q_reg[295]_0\;
  \q_reg[295]_1\ <= \^q_reg[295]_1\;
  \q_reg[31]_0\ <= \^q_reg[31]_0\;
  \q_reg[34]_0\ <= \^q_reg[34]_0\;
  \q_reg[43]_0\ <= \^q_reg[43]_0\;
  \q_reg[55]_0\ <= \^q_reg[55]_0\;
  \q_reg[91]_0\ <= \^q_reg[91]_0\;
  q_reg_i_163_0 <= \^q_reg_i_163_0\;
  q_reg_i_64_0 <= \^q_reg_i_64_0\;
  q_reg_i_78_0 <= \^q_reg_i_78_0\;
  q_reg_i_93_0 <= \^q_reg_i_93_0\;
  q_reg_i_96_0 <= \^q_reg_i_96_0\;
\q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(0),
      Q => pre_reg_cnt(0),
      R => '0'
    );
\q_reg[100]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(100),
      Q => pre_reg_cnt(100),
      R => '0'
    );
\q_reg[101]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(101),
      Q => pre_reg_cnt(101),
      R => '0'
    );
\q_reg[102]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(102),
      Q => pre_reg_cnt(102),
      R => '0'
    );
\q_reg[103]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(103),
      Q => pre_reg_cnt(103),
      R => '0'
    );
\q_reg[104]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(104),
      Q => pre_reg_cnt(104),
      R => '0'
    );
\q_reg[105]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(105),
      Q => pre_reg_cnt(105),
      R => '0'
    );
\q_reg[106]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(106),
      Q => pre_reg_cnt(106),
      R => '0'
    );
\q_reg[107]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(107),
      Q => pre_reg_cnt(107),
      R => '0'
    );
\q_reg[108]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(108),
      Q => pre_reg_cnt(108),
      R => '0'
    );
\q_reg[109]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(109),
      Q => pre_reg_cnt(109),
      R => '0'
    );
\q_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(10),
      Q => pre_reg_cnt(10),
      R => '0'
    );
\q_reg[110]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(110),
      Q => pre_reg_cnt(110),
      R => '0'
    );
\q_reg[111]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(111),
      Q => pre_reg_cnt(111),
      R => '0'
    );
\q_reg[112]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(112),
      Q => pre_reg_cnt(112),
      R => '0'
    );
\q_reg[113]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(113),
      Q => pre_reg_cnt(113),
      R => '0'
    );
\q_reg[114]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(114),
      Q => pre_reg_cnt(114),
      R => '0'
    );
\q_reg[115]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(115),
      Q => pre_reg_cnt(115),
      R => '0'
    );
\q_reg[116]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(116),
      Q => pre_reg_cnt(116),
      R => '0'
    );
\q_reg[117]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(117),
      Q => pre_reg_cnt(117),
      R => '0'
    );
\q_reg[118]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(118),
      Q => pre_reg_cnt(118),
      R => '0'
    );
\q_reg[119]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(119),
      Q => pre_reg_cnt(119),
      R => '0'
    );
\q_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(11),
      Q => pre_reg_cnt(11),
      R => '0'
    );
\q_reg[120]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(120),
      Q => pre_reg_cnt(120),
      R => '0'
    );
\q_reg[121]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(121),
      Q => pre_reg_cnt(121),
      R => '0'
    );
\q_reg[122]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(122),
      Q => pre_reg_cnt(122),
      R => '0'
    );
\q_reg[123]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(123),
      Q => pre_reg_cnt(123),
      R => '0'
    );
\q_reg[124]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(124),
      Q => pre_reg_cnt(124),
      R => '0'
    );
\q_reg[125]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(125),
      Q => pre_reg_cnt(125),
      R => '0'
    );
\q_reg[126]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(126),
      Q => pre_reg_cnt(126),
      R => '0'
    );
\q_reg[127]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(127),
      Q => pre_reg_cnt(127),
      R => '0'
    );
\q_reg[128]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(128),
      Q => pre_reg_cnt(128),
      R => '0'
    );
\q_reg[129]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(129),
      Q => pre_reg_cnt(129),
      R => '0'
    );
\q_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(12),
      Q => pre_reg_cnt(12),
      R => '0'
    );
\q_reg[130]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(130),
      Q => pre_reg_cnt(130),
      R => '0'
    );
\q_reg[131]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(131),
      Q => pre_reg_cnt(131),
      R => '0'
    );
\q_reg[132]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(132),
      Q => pre_reg_cnt(132),
      R => '0'
    );
\q_reg[133]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(133),
      Q => pre_reg_cnt(133),
      R => '0'
    );
\q_reg[134]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(134),
      Q => pre_reg_cnt(134),
      R => '0'
    );
\q_reg[135]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(135),
      Q => pre_reg_cnt(135),
      R => '0'
    );
\q_reg[136]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(136),
      Q => pre_reg_cnt(136),
      R => '0'
    );
\q_reg[137]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(137),
      Q => pre_reg_cnt(137),
      R => '0'
    );
\q_reg[138]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(138),
      Q => pre_reg_cnt(138),
      R => '0'
    );
\q_reg[139]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(139),
      Q => pre_reg_cnt(139),
      R => '0'
    );
\q_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(13),
      Q => pre_reg_cnt(13),
      R => '0'
    );
\q_reg[140]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(140),
      Q => pre_reg_cnt(140),
      R => '0'
    );
\q_reg[141]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(141),
      Q => pre_reg_cnt(141),
      R => '0'
    );
\q_reg[142]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(142),
      Q => pre_reg_cnt(142),
      R => '0'
    );
\q_reg[143]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(143),
      Q => pre_reg_cnt(143),
      R => '0'
    );
\q_reg[144]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(144),
      Q => pre_reg_cnt(144),
      R => '0'
    );
\q_reg[145]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(145),
      Q => pre_reg_cnt(145),
      R => '0'
    );
\q_reg[146]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(146),
      Q => pre_reg_cnt(146),
      R => '0'
    );
\q_reg[147]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(147),
      Q => pre_reg_cnt(147),
      R => '0'
    );
\q_reg[148]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(148),
      Q => pre_reg_cnt(148),
      R => '0'
    );
\q_reg[149]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(149),
      Q => pre_reg_cnt(149),
      R => '0'
    );
\q_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(14),
      Q => pre_reg_cnt(14),
      R => '0'
    );
\q_reg[150]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(150),
      Q => pre_reg_cnt(150),
      R => '0'
    );
\q_reg[151]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(151),
      Q => pre_reg_cnt(151),
      R => '0'
    );
\q_reg[152]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(152),
      Q => pre_reg_cnt(152),
      R => '0'
    );
\q_reg[153]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(153),
      Q => pre_reg_cnt(153),
      R => '0'
    );
\q_reg[154]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(154),
      Q => pre_reg_cnt(154),
      R => '0'
    );
\q_reg[155]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(155),
      Q => pre_reg_cnt(155),
      R => '0'
    );
\q_reg[156]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(156),
      Q => pre_reg_cnt(156),
      R => '0'
    );
\q_reg[157]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(157),
      Q => pre_reg_cnt(157),
      R => '0'
    );
\q_reg[158]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(158),
      Q => pre_reg_cnt(158),
      R => '0'
    );
\q_reg[159]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(159),
      Q => pre_reg_cnt(159),
      R => '0'
    );
\q_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(15),
      Q => pre_reg_cnt(15),
      R => '0'
    );
\q_reg[160]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(160),
      Q => pre_reg_cnt(160),
      R => '0'
    );
\q_reg[161]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(161),
      Q => pre_reg_cnt(161),
      R => '0'
    );
\q_reg[162]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(162),
      Q => pre_reg_cnt(162),
      R => '0'
    );
\q_reg[163]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(163),
      Q => pre_reg_cnt(163),
      R => '0'
    );
\q_reg[164]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(164),
      Q => pre_reg_cnt(164),
      R => '0'
    );
\q_reg[165]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(165),
      Q => pre_reg_cnt(165),
      R => '0'
    );
\q_reg[166]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(166),
      Q => pre_reg_cnt(166),
      R => '0'
    );
\q_reg[167]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(167),
      Q => pre_reg_cnt(167),
      R => '0'
    );
\q_reg[168]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(168),
      Q => pre_reg_cnt(168),
      R => '0'
    );
\q_reg[169]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(169),
      Q => pre_reg_cnt(169),
      R => '0'
    );
\q_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(16),
      Q => pre_reg_cnt(16),
      R => '0'
    );
\q_reg[170]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(170),
      Q => pre_reg_cnt(170),
      R => '0'
    );
\q_reg[171]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(171),
      Q => pre_reg_cnt(171),
      R => '0'
    );
\q_reg[172]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(172),
      Q => pre_reg_cnt(172),
      R => '0'
    );
\q_reg[173]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(173),
      Q => pre_reg_cnt(173),
      R => '0'
    );
\q_reg[174]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(174),
      Q => pre_reg_cnt(174),
      R => '0'
    );
\q_reg[175]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(175),
      Q => pre_reg_cnt(175),
      R => '0'
    );
\q_reg[176]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(176),
      Q => pre_reg_cnt(176),
      R => '0'
    );
\q_reg[177]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(177),
      Q => pre_reg_cnt(177),
      R => '0'
    );
\q_reg[178]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(178),
      Q => pre_reg_cnt(178),
      R => '0'
    );
\q_reg[179]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(179),
      Q => pre_reg_cnt(179),
      R => '0'
    );
\q_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(17),
      Q => pre_reg_cnt(17),
      R => '0'
    );
\q_reg[180]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(180),
      Q => pre_reg_cnt(180),
      R => '0'
    );
\q_reg[181]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(181),
      Q => pre_reg_cnt(181),
      R => '0'
    );
\q_reg[182]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(182),
      Q => pre_reg_cnt(182),
      R => '0'
    );
\q_reg[183]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(183),
      Q => pre_reg_cnt(183),
      R => '0'
    );
\q_reg[184]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(184),
      Q => pre_reg_cnt(184),
      R => '0'
    );
\q_reg[185]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(185),
      Q => pre_reg_cnt(185),
      R => '0'
    );
\q_reg[186]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(186),
      Q => pre_reg_cnt(186),
      R => '0'
    );
\q_reg[187]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(187),
      Q => pre_reg_cnt(187),
      R => '0'
    );
\q_reg[188]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(188),
      Q => pre_reg_cnt(188),
      R => '0'
    );
\q_reg[189]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(189),
      Q => pre_reg_cnt(189),
      R => '0'
    );
\q_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(18),
      Q => pre_reg_cnt(18),
      R => '0'
    );
\q_reg[190]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(190),
      Q => pre_reg_cnt(190),
      R => '0'
    );
\q_reg[191]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(191),
      Q => pre_reg_cnt(191),
      R => '0'
    );
\q_reg[192]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(192),
      Q => pre_reg_cnt(192),
      R => '0'
    );
\q_reg[193]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(193),
      Q => pre_reg_cnt(193),
      R => '0'
    );
\q_reg[194]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(194),
      Q => pre_reg_cnt(194),
      R => '0'
    );
\q_reg[195]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(195),
      Q => pre_reg_cnt(195),
      R => '0'
    );
\q_reg[196]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(196),
      Q => pre_reg_cnt(196),
      R => '0'
    );
\q_reg[197]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(197),
      Q => pre_reg_cnt(197),
      R => '0'
    );
\q_reg[198]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(198),
      Q => pre_reg_cnt(198),
      R => '0'
    );
\q_reg[199]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(199),
      Q => pre_reg_cnt(199),
      R => '0'
    );
\q_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(19),
      Q => pre_reg_cnt(19),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(1),
      Q => pre_reg_cnt(1),
      R => '0'
    );
\q_reg[200]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(200),
      Q => pre_reg_cnt(200),
      R => '0'
    );
\q_reg[201]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(201),
      Q => pre_reg_cnt(201),
      R => '0'
    );
\q_reg[202]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(202),
      Q => pre_reg_cnt(202),
      R => '0'
    );
\q_reg[203]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(203),
      Q => pre_reg_cnt(203),
      R => '0'
    );
\q_reg[204]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(204),
      Q => pre_reg_cnt(204),
      R => '0'
    );
\q_reg[205]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(205),
      Q => pre_reg_cnt(205),
      R => '0'
    );
\q_reg[206]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(206),
      Q => pre_reg_cnt(206),
      R => '0'
    );
\q_reg[207]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(207),
      Q => pre_reg_cnt(207),
      R => '0'
    );
\q_reg[208]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(208),
      Q => pre_reg_cnt(208),
      R => '0'
    );
\q_reg[209]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(209),
      Q => pre_reg_cnt(209),
      R => '0'
    );
\q_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(20),
      Q => pre_reg_cnt(20),
      R => '0'
    );
\q_reg[210]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(210),
      Q => pre_reg_cnt(210),
      R => '0'
    );
\q_reg[211]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(211),
      Q => pre_reg_cnt(211),
      R => '0'
    );
\q_reg[212]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(212),
      Q => pre_reg_cnt(212),
      R => '0'
    );
\q_reg[213]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(213),
      Q => pre_reg_cnt(213),
      R => '0'
    );
\q_reg[214]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(214),
      Q => pre_reg_cnt(214),
      R => '0'
    );
\q_reg[215]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(215),
      Q => pre_reg_cnt(215),
      R => '0'
    );
\q_reg[216]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(216),
      Q => pre_reg_cnt(216),
      R => '0'
    );
\q_reg[217]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(217),
      Q => pre_reg_cnt(217),
      R => '0'
    );
\q_reg[218]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(218),
      Q => pre_reg_cnt(218),
      R => '0'
    );
\q_reg[219]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(219),
      Q => pre_reg_cnt(219),
      R => '0'
    );
\q_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(21),
      Q => pre_reg_cnt(21),
      R => '0'
    );
\q_reg[220]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(220),
      Q => pre_reg_cnt(220),
      R => '0'
    );
\q_reg[221]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(221),
      Q => pre_reg_cnt(221),
      R => '0'
    );
\q_reg[222]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(222),
      Q => pre_reg_cnt(222),
      R => '0'
    );
\q_reg[223]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(223),
      Q => pre_reg_cnt(223),
      R => '0'
    );
\q_reg[224]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(224),
      Q => pre_reg_cnt(224),
      R => '0'
    );
\q_reg[225]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(225),
      Q => pre_reg_cnt(225),
      R => '0'
    );
\q_reg[226]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(226),
      Q => pre_reg_cnt(226),
      R => '0'
    );
\q_reg[227]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(227),
      Q => pre_reg_cnt(227),
      R => '0'
    );
\q_reg[228]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(228),
      Q => pre_reg_cnt(228),
      R => '0'
    );
\q_reg[229]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(229),
      Q => pre_reg_cnt(229),
      R => '0'
    );
\q_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(22),
      Q => pre_reg_cnt(22),
      R => '0'
    );
\q_reg[230]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(230),
      Q => pre_reg_cnt(230),
      R => '0'
    );
\q_reg[231]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(231),
      Q => pre_reg_cnt(231),
      R => '0'
    );
\q_reg[232]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(232),
      Q => pre_reg_cnt(232),
      R => '0'
    );
\q_reg[233]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(233),
      Q => pre_reg_cnt(233),
      R => '0'
    );
\q_reg[234]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(234),
      Q => pre_reg_cnt(234),
      R => '0'
    );
\q_reg[235]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(235),
      Q => pre_reg_cnt(235),
      R => '0'
    );
\q_reg[236]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(236),
      Q => pre_reg_cnt(236),
      R => '0'
    );
\q_reg[237]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(237),
      Q => pre_reg_cnt(237),
      R => '0'
    );
\q_reg[238]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(238),
      Q => pre_reg_cnt(238),
      R => '0'
    );
\q_reg[239]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(239),
      Q => pre_reg_cnt(239),
      R => '0'
    );
\q_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(23),
      Q => pre_reg_cnt(23),
      R => '0'
    );
\q_reg[240]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(240),
      Q => pre_reg_cnt(240),
      R => '0'
    );
\q_reg[241]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(241),
      Q => pre_reg_cnt(241),
      R => '0'
    );
\q_reg[242]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(242),
      Q => pre_reg_cnt(242),
      R => '0'
    );
\q_reg[243]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(243),
      Q => pre_reg_cnt(243),
      R => '0'
    );
\q_reg[244]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(244),
      Q => pre_reg_cnt(244),
      R => '0'
    );
\q_reg[245]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(245),
      Q => pre_reg_cnt(245),
      R => '0'
    );
\q_reg[246]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(246),
      Q => pre_reg_cnt(246),
      R => '0'
    );
\q_reg[247]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(247),
      Q => pre_reg_cnt(247),
      R => '0'
    );
\q_reg[248]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(248),
      Q => pre_reg_cnt(248),
      R => '0'
    );
\q_reg[249]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(249),
      Q => pre_reg_cnt(249),
      R => '0'
    );
\q_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(24),
      Q => pre_reg_cnt(24),
      R => '0'
    );
\q_reg[250]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(250),
      Q => pre_reg_cnt(250),
      R => '0'
    );
\q_reg[251]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(251),
      Q => pre_reg_cnt(251),
      R => '0'
    );
\q_reg[252]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(252),
      Q => pre_reg_cnt(252),
      R => '0'
    );
\q_reg[253]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(253),
      Q => pre_reg_cnt(253),
      R => '0'
    );
\q_reg[254]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(254),
      Q => pre_reg_cnt(254),
      R => '0'
    );
\q_reg[255]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(255),
      Q => pre_reg_cnt(255),
      R => '0'
    );
\q_reg[256]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(256),
      Q => pre_reg_cnt(256),
      R => '0'
    );
\q_reg[257]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(257),
      Q => pre_reg_cnt(257),
      R => '0'
    );
\q_reg[258]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(258),
      Q => pre_reg_cnt(258),
      R => '0'
    );
\q_reg[259]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(259),
      Q => pre_reg_cnt(259),
      R => '0'
    );
\q_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(25),
      Q => pre_reg_cnt(25),
      R => '0'
    );
\q_reg[260]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(260),
      Q => pre_reg_cnt(260),
      R => '0'
    );
\q_reg[261]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(261),
      Q => pre_reg_cnt(261),
      R => '0'
    );
\q_reg[262]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(262),
      Q => pre_reg_cnt(262),
      R => '0'
    );
\q_reg[263]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(263),
      Q => pre_reg_cnt(263),
      R => '0'
    );
\q_reg[264]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(264),
      Q => pre_reg_cnt(264),
      R => '0'
    );
\q_reg[265]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(265),
      Q => pre_reg_cnt(265),
      R => '0'
    );
\q_reg[266]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(266),
      Q => pre_reg_cnt(266),
      R => '0'
    );
\q_reg[267]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(267),
      Q => pre_reg_cnt(267),
      R => '0'
    );
\q_reg[268]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(268),
      Q => pre_reg_cnt(268),
      R => '0'
    );
\q_reg[269]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(269),
      Q => pre_reg_cnt(269),
      R => '0'
    );
\q_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(26),
      Q => pre_reg_cnt(26),
      R => '0'
    );
\q_reg[270]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(270),
      Q => pre_reg_cnt(270),
      R => '0'
    );
\q_reg[271]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(271),
      Q => pre_reg_cnt(271),
      R => '0'
    );
\q_reg[272]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(272),
      Q => pre_reg_cnt(272),
      R => '0'
    );
\q_reg[273]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(273),
      Q => pre_reg_cnt(273),
      R => '0'
    );
\q_reg[274]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(274),
      Q => pre_reg_cnt(274),
      R => '0'
    );
\q_reg[275]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(275),
      Q => pre_reg_cnt(275),
      R => '0'
    );
\q_reg[276]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(276),
      Q => pre_reg_cnt(276),
      R => '0'
    );
\q_reg[277]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(277),
      Q => pre_reg_cnt(277),
      R => '0'
    );
\q_reg[278]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(278),
      Q => pre_reg_cnt(278),
      R => '0'
    );
\q_reg[279]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(279),
      Q => pre_reg_cnt(279),
      R => '0'
    );
\q_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(27),
      Q => pre_reg_cnt(27),
      R => '0'
    );
\q_reg[280]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(280),
      Q => pre_reg_cnt(280),
      R => '0'
    );
\q_reg[281]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(281),
      Q => pre_reg_cnt(281),
      R => '0'
    );
\q_reg[282]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(282),
      Q => pre_reg_cnt(282),
      R => '0'
    );
\q_reg[283]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(283),
      Q => pre_reg_cnt(283),
      R => '0'
    );
\q_reg[284]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(284),
      Q => pre_reg_cnt(284),
      R => '0'
    );
\q_reg[285]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(285),
      Q => pre_reg_cnt(285),
      R => '0'
    );
\q_reg[286]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(286),
      Q => pre_reg_cnt(286),
      R => '0'
    );
\q_reg[287]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(287),
      Q => pre_reg_cnt(287),
      R => '0'
    );
\q_reg[288]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(288),
      Q => pre_reg_cnt(288),
      R => '0'
    );
\q_reg[289]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(289),
      Q => pre_reg_cnt(289),
      R => '0'
    );
\q_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(28),
      Q => pre_reg_cnt(28),
      R => '0'
    );
\q_reg[290]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(290),
      Q => pre_reg_cnt(290),
      R => '0'
    );
\q_reg[291]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(291),
      Q => pre_reg_cnt(291),
      R => '0'
    );
\q_reg[292]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(292),
      Q => pre_reg_cnt(292),
      R => '0'
    );
\q_reg[293]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(293),
      Q => pre_reg_cnt(293),
      R => '0'
    );
\q_reg[294]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(294),
      Q => pre_reg_cnt(294),
      R => '0'
    );
\q_reg[295]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(295),
      Q => pre_reg_cnt(295),
      R => '0'
    );
\q_reg[296]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(296),
      Q => pre_reg_cnt(296),
      R => '0'
    );
\q_reg[297]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(297),
      Q => pre_reg_cnt(297),
      R => '0'
    );
\q_reg[298]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(298),
      Q => pre_reg_cnt(298),
      R => '0'
    );
\q_reg[299]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(299),
      Q => pre_reg_cnt(299),
      R => '0'
    );
\q_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(29),
      Q => pre_reg_cnt(29),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(2),
      Q => pre_reg_cnt(2),
      R => '0'
    );
\q_reg[300]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(300),
      Q => pre_reg_cnt(300),
      R => '0'
    );
\q_reg[301]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(301),
      Q => pre_reg_cnt(301),
      R => '0'
    );
\q_reg[302]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(302),
      Q => pre_reg_cnt(302),
      R => '0'
    );
\q_reg[303]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(303),
      Q => pre_reg_cnt(303),
      R => '0'
    );
\q_reg[304]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(304),
      Q => pre_reg_cnt(304),
      R => '0'
    );
\q_reg[305]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(305),
      Q => pre_reg_cnt(305),
      R => '0'
    );
\q_reg[306]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(306),
      Q => pre_reg_cnt(306),
      R => '0'
    );
\q_reg[307]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(307),
      Q => pre_reg_cnt(307),
      R => '0'
    );
\q_reg[308]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(308),
      Q => pre_reg_cnt(308),
      R => '0'
    );
\q_reg[309]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(309),
      Q => pre_reg_cnt(309),
      R => '0'
    );
\q_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(30),
      Q => pre_reg_cnt(30),
      R => '0'
    );
\q_reg[310]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(310),
      Q => pre_reg_cnt(310),
      R => '0'
    );
\q_reg[311]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(311),
      Q => pre_reg_cnt(311),
      R => '0'
    );
\q_reg[312]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(312),
      Q => pre_reg_cnt(312),
      R => '0'
    );
\q_reg[313]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(313),
      Q => pre_reg_cnt(313),
      R => '0'
    );
\q_reg[314]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(314),
      Q => pre_reg_cnt(314),
      R => '0'
    );
\q_reg[315]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(315),
      Q => pre_reg_cnt(315),
      R => '0'
    );
\q_reg[316]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(316),
      Q => pre_reg_cnt(316),
      R => '0'
    );
\q_reg[317]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(317),
      Q => pre_reg_cnt(317),
      R => '0'
    );
\q_reg[318]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(318),
      Q => pre_reg_cnt(318),
      R => '0'
    );
\q_reg[319]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(319),
      Q => pre_reg_cnt(319),
      R => '0'
    );
\q_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(31),
      Q => pre_reg_cnt(31),
      R => '0'
    );
\q_reg[32]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(32),
      Q => pre_reg_cnt(32),
      R => '0'
    );
\q_reg[33]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(33),
      Q => pre_reg_cnt(33),
      R => '0'
    );
\q_reg[34]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(34),
      Q => pre_reg_cnt(34),
      R => '0'
    );
\q_reg[35]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(35),
      Q => pre_reg_cnt(35),
      R => '0'
    );
\q_reg[36]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(36),
      Q => pre_reg_cnt(36),
      R => '0'
    );
\q_reg[37]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(37),
      Q => pre_reg_cnt(37),
      R => '0'
    );
\q_reg[38]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(38),
      Q => pre_reg_cnt(38),
      R => '0'
    );
\q_reg[39]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(39),
      Q => pre_reg_cnt(39),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(3),
      Q => pre_reg_cnt(3),
      R => '0'
    );
\q_reg[40]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(40),
      Q => pre_reg_cnt(40),
      R => '0'
    );
\q_reg[41]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(41),
      Q => pre_reg_cnt(41),
      R => '0'
    );
\q_reg[42]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(42),
      Q => pre_reg_cnt(42),
      R => '0'
    );
\q_reg[43]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(43),
      Q => pre_reg_cnt(43),
      R => '0'
    );
\q_reg[44]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(44),
      Q => pre_reg_cnt(44),
      R => '0'
    );
\q_reg[45]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(45),
      Q => pre_reg_cnt(45),
      R => '0'
    );
\q_reg[46]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(46),
      Q => pre_reg_cnt(46),
      R => '0'
    );
\q_reg[47]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(47),
      Q => pre_reg_cnt(47),
      R => '0'
    );
\q_reg[48]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(48),
      Q => pre_reg_cnt(48),
      R => '0'
    );
\q_reg[49]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(49),
      Q => pre_reg_cnt(49),
      R => '0'
    );
\q_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(4),
      Q => pre_reg_cnt(4),
      R => '0'
    );
\q_reg[50]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(50),
      Q => pre_reg_cnt(50),
      R => '0'
    );
\q_reg[51]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(51),
      Q => pre_reg_cnt(51),
      R => '0'
    );
\q_reg[52]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(52),
      Q => pre_reg_cnt(52),
      R => '0'
    );
\q_reg[53]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(53),
      Q => pre_reg_cnt(53),
      R => '0'
    );
\q_reg[54]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(54),
      Q => pre_reg_cnt(54),
      R => '0'
    );
\q_reg[55]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(55),
      Q => pre_reg_cnt(55),
      R => '0'
    );
\q_reg[56]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(56),
      Q => pre_reg_cnt(56),
      R => '0'
    );
\q_reg[57]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(57),
      Q => pre_reg_cnt(57),
      R => '0'
    );
\q_reg[58]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(58),
      Q => pre_reg_cnt(58),
      R => '0'
    );
\q_reg[59]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(59),
      Q => pre_reg_cnt(59),
      R => '0'
    );
\q_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(5),
      Q => pre_reg_cnt(5),
      R => '0'
    );
\q_reg[60]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(60),
      Q => pre_reg_cnt(60),
      R => '0'
    );
\q_reg[61]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(61),
      Q => pre_reg_cnt(61),
      R => '0'
    );
\q_reg[62]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(62),
      Q => pre_reg_cnt(62),
      R => '0'
    );
\q_reg[63]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(63),
      Q => pre_reg_cnt(63),
      R => '0'
    );
\q_reg[64]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(64),
      Q => pre_reg_cnt(64),
      R => '0'
    );
\q_reg[65]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(65),
      Q => pre_reg_cnt(65),
      R => '0'
    );
\q_reg[66]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(66),
      Q => pre_reg_cnt(66),
      R => '0'
    );
\q_reg[67]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(67),
      Q => pre_reg_cnt(67),
      R => '0'
    );
\q_reg[68]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(68),
      Q => pre_reg_cnt(68),
      R => '0'
    );
\q_reg[69]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(69),
      Q => pre_reg_cnt(69),
      R => '0'
    );
\q_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(6),
      Q => pre_reg_cnt(6),
      R => '0'
    );
\q_reg[70]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(70),
      Q => pre_reg_cnt(70),
      R => '0'
    );
\q_reg[71]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(71),
      Q => pre_reg_cnt(71),
      R => '0'
    );
\q_reg[72]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(72),
      Q => pre_reg_cnt(72),
      R => '0'
    );
\q_reg[73]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(73),
      Q => pre_reg_cnt(73),
      R => '0'
    );
\q_reg[74]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(74),
      Q => pre_reg_cnt(74),
      R => '0'
    );
\q_reg[75]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(75),
      Q => pre_reg_cnt(75),
      R => '0'
    );
\q_reg[76]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(76),
      Q => pre_reg_cnt(76),
      R => '0'
    );
\q_reg[77]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(77),
      Q => pre_reg_cnt(77),
      R => '0'
    );
\q_reg[78]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(78),
      Q => pre_reg_cnt(78),
      R => '0'
    );
\q_reg[79]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(79),
      Q => pre_reg_cnt(79),
      R => '0'
    );
\q_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(7),
      Q => pre_reg_cnt(7),
      R => '0'
    );
\q_reg[80]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(80),
      Q => pre_reg_cnt(80),
      R => '0'
    );
\q_reg[81]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(81),
      Q => pre_reg_cnt(81),
      R => '0'
    );
\q_reg[82]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(82),
      Q => pre_reg_cnt(82),
      R => '0'
    );
\q_reg[83]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(83),
      Q => pre_reg_cnt(83),
      R => '0'
    );
\q_reg[84]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(84),
      Q => pre_reg_cnt(84),
      R => '0'
    );
\q_reg[85]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(85),
      Q => pre_reg_cnt(85),
      R => '0'
    );
\q_reg[86]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(86),
      Q => pre_reg_cnt(86),
      R => '0'
    );
\q_reg[87]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(87),
      Q => pre_reg_cnt(87),
      R => '0'
    );
\q_reg[88]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(88),
      Q => pre_reg_cnt(88),
      R => '0'
    );
\q_reg[89]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(89),
      Q => pre_reg_cnt(89),
      R => '0'
    );
\q_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(8),
      Q => pre_reg_cnt(8),
      R => '0'
    );
\q_reg[90]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(90),
      Q => pre_reg_cnt(90),
      R => '0'
    );
\q_reg[91]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(91),
      Q => pre_reg_cnt(91),
      R => '0'
    );
\q_reg[92]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(92),
      Q => pre_reg_cnt(92),
      R => '0'
    );
\q_reg[93]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(93),
      Q => pre_reg_cnt(93),
      R => '0'
    );
\q_reg[94]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(94),
      Q => pre_reg_cnt(94),
      R => '0'
    );
\q_reg[95]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(95),
      Q => pre_reg_cnt(95),
      R => '0'
    );
\q_reg[96]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(96),
      Q => pre_reg_cnt(96),
      R => '0'
    );
\q_reg[97]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(97),
      Q => pre_reg_cnt(97),
      R => '0'
    );
\q_reg[98]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(98),
      Q => pre_reg_cnt(98),
      R => '0'
    );
\q_reg[99]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(99),
      Q => pre_reg_cnt(99),
      R => '0'
    );
\q_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]_0\,
      CE => '1',
      D => \q_reg[319]_0\(9),
      Q => pre_reg_cnt(9),
      R => '0'
    );
q_reg_i_100: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8A88AAAA"
    )
        port map (
      I0 => q_reg_i_274_n_0,
      I1 => q_reg_i_275_n_0,
      I2 => pre_reg_cnt(307),
      I3 => pre_reg_cnt(306),
      I4 => q_reg_i_276_n_0,
      O => q_reg_i_100_n_0
    );
q_reg_i_101: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8AAA8A8A"
    )
        port map (
      I0 => q_reg_i_277_n_0,
      I1 => q_reg_i_71_n_0,
      I2 => q_reg_i_117_n_0,
      I3 => q_reg_i_278_n_0,
      I4 => q_reg_i_128_n_0,
      O => q_reg_i_101_n_0
    );
q_reg_i_102: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E0EE"
    )
        port map (
      I0 => q_reg_i_279_n_0,
      I1 => q_reg_i_280_n_0,
      I2 => q_reg_i_144_n_0,
      I3 => q_reg_i_281_n_0,
      O => q_reg_i_102_n_0
    );
q_reg_i_103: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFBBAAAA"
    )
        port map (
      I0 => q_reg_i_282_n_0,
      I1 => q_reg_i_283_n_0,
      I2 => q_reg_i_242_n_0,
      I3 => q_reg_i_261_n_0,
      I4 => q_reg_i_248_n_0,
      O => q_reg_i_103_n_0
    );
q_reg_i_104: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000BABB"
    )
        port map (
      I0 => q_reg_i_276_n_0,
      I1 => q_reg_i_284_n_0,
      I2 => pre_reg_cnt(295),
      I3 => pre_reg_cnt(294),
      I4 => q_reg_i_285_n_0,
      O => q_reg_i_104_n_0
    );
q_reg_i_105: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00A8AAAA"
    )
        port map (
      I0 => q_reg_i_134_n_0,
      I1 => q_reg_i_278_n_0,
      I2 => q_reg_i_286_n_0,
      I3 => q_reg_i_287_n_0,
      I4 => q_reg_i_258_n_0,
      O => q_reg_i_105_n_0
    );
q_reg_i_106: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888A8888888A888A"
    )
        port map (
      I0 => q_reg_i_288_n_0,
      I1 => q_reg_i_289_n_0,
      I2 => q_reg_i_237_n_0,
      I3 => q_reg_i_216_n_0,
      I4 => pre_reg_cnt(55),
      I5 => pre_reg_cnt(54),
      O => q_reg_i_106_n_0
    );
q_reg_i_107: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF2FFFFFFFF"
    )
        port map (
      I0 => pre_reg_cnt(117),
      I1 => pre_reg_cnt(118),
      I2 => pre_reg_cnt(119),
      I3 => q_reg_i_200_n_0,
      I4 => q_reg_i_290_n_0,
      I5 => q_reg_i_291_n_0,
      O => q_reg_i_107_n_0
    );
q_reg_i_108: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000010001"
    )
        port map (
      I0 => q_reg_i_292_n_0,
      I1 => pre_reg_cnt(101),
      I2 => pre_reg_cnt(102),
      I3 => pre_reg_cnt(103),
      I4 => pre_reg_cnt(100),
      I5 => pre_reg_cnt(99),
      O => q_reg_i_108_n_0
    );
q_reg_i_109: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0045FFFF00450045"
    )
        port map (
      I0 => q_reg_i_293_n_0,
      I1 => pre_reg_cnt(247),
      I2 => pre_reg_cnt(246),
      I3 => q_reg_i_294_n_0,
      I4 => q_reg_i_258_n_0,
      I5 => q_reg_i_295_n_0,
      O => q_reg_i_109_n_0
    );
q_reg_i_110: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000AAFB"
    )
        port map (
      I0 => q_reg_i_202_n_0,
      I1 => pre_reg_cnt(132),
      I2 => pre_reg_cnt(133),
      I3 => q_reg_i_296_n_0,
      I4 => q_reg_i_297_n_0,
      O => q_reg_i_110_n_0
    );
q_reg_i_111: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000A2AAAA00A2"
    )
        port map (
      I0 => q_reg_i_209_n_0,
      I1 => pre_reg_cnt(141),
      I2 => pre_reg_cnt(142),
      I3 => pre_reg_cnt(143),
      I4 => q_reg_i_165_n_0,
      I5 => q_reg_i_211_n_0,
      O => q_reg_i_111_n_0
    );
q_reg_i_112: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100010000000100"
    )
        port map (
      I0 => pre_reg_cnt(158),
      I1 => pre_reg_cnt(159),
      I2 => q_reg_i_298_n_0,
      I3 => q_reg_i_299_n_0,
      I4 => pre_reg_cnt(156),
      I5 => pre_reg_cnt(157),
      O => q_reg_i_112_n_0
    );
q_reg_i_113: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00007707"
    )
        port map (
      I0 => q_reg_i_184_n_0,
      I1 => q_reg_i_300_n_0,
      I2 => pre_reg_cnt(84),
      I3 => pre_reg_cnt(85),
      I4 => q_reg_i_301_n_0,
      O => q_reg_i_113_n_0
    );
q_reg_i_114: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000011155555"
    )
        port map (
      I0 => q_reg_i_302_n_0,
      I1 => q_reg_i_108_n_0,
      I2 => q_reg_i_303_n_0,
      I3 => q_reg_i_304_n_0,
      I4 => q_reg_i_305_n_0,
      I5 => q_reg_i_257_n_0,
      O => q_reg_i_114_n_0
    );
q_reg_i_115: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000FF0E"
    )
        port map (
      I0 => q_reg_i_306_n_0,
      I1 => pre_reg_cnt(188),
      I2 => pre_reg_cnt(189),
      I3 => pre_reg_cnt(190),
      I4 => pre_reg_cnt(191),
      I5 => q_reg_i_194_n_0,
      O => q_reg_i_115_n_0
    );
q_reg_i_116: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00010000FFFFFFFF"
    )
        port map (
      I0 => pre_reg_cnt(194),
      I1 => pre_reg_cnt(195),
      I2 => q_reg_i_307_n_0,
      I3 => q_reg_i_308_n_0,
      I4 => pre_reg_cnt(193),
      I5 => q_reg_i_194_n_0,
      O => q_reg_i_116_n_0
    );
q_reg_i_117: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFEFE"
    )
        port map (
      I0 => pre_reg_cnt(209),
      I1 => q_reg_i_309_n_0,
      I2 => q_reg_i_310_n_0,
      I3 => q_reg_i_311_n_0,
      I4 => pre_reg_cnt(207),
      O => q_reg_i_117_n_0
    );
q_reg_i_118: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFEFE"
    )
        port map (
      I0 => q_reg_i_311_n_0,
      I1 => pre_reg_cnt(207),
      I2 => pre_reg_cnt(206),
      I3 => pre_reg_cnt(205),
      I4 => pre_reg_cnt(204),
      O => q_reg_i_118_n_0
    );
q_reg_i_119: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFEFE"
    )
        port map (
      I0 => q_reg_i_310_n_0,
      I1 => pre_reg_cnt(213),
      I2 => pre_reg_cnt(212),
      I3 => pre_reg_cnt(211),
      I4 => pre_reg_cnt(210),
      O => q_reg_i_119_n_0
    );
q_reg_i_120: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF4544"
    )
        port map (
      I0 => q_reg_i_307_n_0,
      I1 => pre_reg_cnt(199),
      I2 => pre_reg_cnt(198),
      I3 => q_reg_i_312_n_0,
      I4 => q_reg_i_313_n_0,
      O => q_reg_i_120_n_0
    );
q_reg_i_121: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000BBBF"
    )
        port map (
      I0 => q_reg_i_314_n_0,
      I1 => q_reg_i_315_n_0,
      I2 => q_reg_i_316_n_0,
      I3 => q_reg_i_317_n_0,
      I4 => q_reg_i_318_n_0,
      I5 => q_reg_i_319_n_0,
      O => q_reg_i_121_n_0
    );
q_reg_i_122: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_194_n_0,
      I1 => q_reg_i_320_n_0,
      I2 => pre_reg_cnt(187),
      I3 => pre_reg_cnt(185),
      I4 => pre_reg_cnt(186),
      O => q_reg_i_122_n_0
    );
q_reg_i_123: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000F1"
    )
        port map (
      I0 => q_reg_i_321_n_0,
      I1 => pre_reg_cnt(180),
      I2 => pre_reg_cnt(181),
      I3 => q_reg_i_233_n_0,
      I4 => q_reg_i_193_n_0,
      I5 => q_reg_i_194_n_0,
      O => q_reg_i_123_n_0
    );
q_reg_i_124: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002022"
    )
        port map (
      I0 => q_reg_i_322_n_0,
      I1 => pre_reg_cnt(167),
      I2 => pre_reg_cnt(166),
      I3 => pre_reg_cnt(165),
      I4 => q_reg_i_323_n_0,
      O => q_reg_i_124_n_0
    );
q_reg_i_125: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_324_n_0,
      I1 => q_reg_i_325_n_0,
      I2 => q_reg_i_326_n_0,
      I3 => q_reg_i_327_n_0,
      I4 => q_reg_i_328_n_0,
      I5 => q_reg_i_329_n_0,
      O => q_reg_i_125_n_0
    );
q_reg_i_126: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFFF00F2"
    )
        port map (
      I0 => pre_reg_cnt(231),
      I1 => pre_reg_cnt(232),
      I2 => pre_reg_cnt(233),
      I3 => pre_reg_cnt(234),
      I4 => pre_reg_cnt(235),
      I5 => q_reg_i_187_n_0,
      O => q_reg_i_126_n_0
    );
q_reg_i_127: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000BABB"
    )
        port map (
      I0 => q_reg_i_278_n_0,
      I1 => q_reg_i_132_n_0,
      I2 => pre_reg_cnt(223),
      I3 => pre_reg_cnt(222),
      I4 => q_reg_i_287_n_0,
      O => q_reg_i_127_n_0
    );
q_reg_i_128: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFFFFFEFFFE"
    )
        port map (
      I0 => q_reg_i_330_n_0,
      I1 => q_reg_i_132_n_0,
      I2 => pre_reg_cnt(219),
      I3 => pre_reg_cnt(218),
      I4 => pre_reg_cnt(217),
      I5 => pre_reg_cnt(216),
      O => q_reg_i_128_n_0
    );
q_reg_i_129: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_330_n_0,
      I1 => pre_reg_cnt(217),
      I2 => pre_reg_cnt(216),
      I3 => pre_reg_cnt(219),
      I4 => pre_reg_cnt(218),
      O => q_reg_i_129_n_0
    );
q_reg_i_130: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(213),
      I1 => pre_reg_cnt(214),
      O => q_reg_i_130_n_0
    );
q_reg_i_131: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEEFE"
    )
        port map (
      I0 => pre_reg_cnt(222),
      I1 => pre_reg_cnt(223),
      I2 => pre_reg_cnt(219),
      I3 => pre_reg_cnt(220),
      I4 => pre_reg_cnt(221),
      O => q_reg_i_131_n_0
    );
q_reg_i_132: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_324_n_0,
      I1 => q_reg_i_325_n_0,
      I2 => q_reg_i_326_n_0,
      I3 => q_reg_i_331_n_0,
      I4 => q_reg_i_329_n_0,
      I5 => q_reg_i_332_n_0,
      O => q_reg_i_132_n_0
    );
q_reg_i_133: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000A2"
    )
        port map (
      I0 => q_reg_i_333_n_0,
      I1 => pre_reg_cnt(273),
      I2 => pre_reg_cnt(274),
      I3 => pre_reg_cnt(275),
      I4 => q_reg_i_334_n_0,
      O => q_reg_i_133_n_0
    );
q_reg_i_134: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAEEAE"
    )
        port map (
      I0 => q_reg_i_335_n_0,
      I1 => q_reg_i_336_n_0,
      I2 => pre_reg_cnt(276),
      I3 => pre_reg_cnt(277),
      I4 => pre_reg_cnt(278),
      I5 => q_reg_i_337_n_0,
      O => q_reg_i_134_n_0
    );
q_reg_i_135: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF54555454"
    )
        port map (
      I0 => q_reg_i_274_n_0,
      I1 => q_reg_i_338_n_0,
      I2 => pre_reg_cnt(287),
      I3 => pre_reg_cnt(286),
      I4 => pre_reg_cnt(285),
      I5 => q_reg_i_339_n_0,
      O => q_reg_i_135_n_0
    );
q_reg_i_136: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABAAABABAAAAAAAA"
    )
        port map (
      I0 => q_reg_i_340_n_0,
      I1 => q_reg_i_341_n_0,
      I2 => pre_reg_cnt(311),
      I3 => pre_reg_cnt(310),
      I4 => pre_reg_cnt(309),
      I5 => q_reg_i_342_n_0,
      O => q_reg_i_136_n_0
    );
q_reg_i_137: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFFFFFFD"
    )
        port map (
      I0 => q_reg_i_179_n_0,
      I1 => q_reg_i_343_n_0,
      I2 => pre_reg_cnt(131),
      I3 => pre_reg_cnt(130),
      I4 => q_reg_i_344_n_0,
      I5 => q_reg_i_345_n_0,
      O => q_reg_i_137_n_0
    );
q_reg_i_138: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444444FFFFFFFFF"
    )
        port map (
      I0 => q_reg_i_176_n_0,
      I1 => q_reg_i_251_n_0,
      I2 => q_reg_i_222_n_0,
      I3 => q_reg_i_301_n_0,
      I4 => pre_reg_cnt(85),
      I5 => q_reg_i_155_n_0,
      O => q_reg_i_138_n_0
    );
q_reg_i_139: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => q_reg_i_346_n_0,
      I1 => q_reg_i_347_n_0,
      I2 => q_reg_i_80_n_0,
      O => q_reg_i_139_n_0
    );
q_reg_i_140: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAFFFE"
    )
        port map (
      I0 => q_reg_i_217_n_0,
      I1 => pre_reg_cnt(287),
      I2 => pre_reg_cnt(286),
      I3 => q_reg_i_338_n_0,
      I4 => q_reg_i_348_n_0,
      I5 => q_reg_i_349_n_0,
      O => q_reg_i_140_n_0
    );
q_reg_i_141: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000D0D0D000"
    )
        port map (
      I0 => q_reg_i_350_n_0,
      I1 => q_reg_i_221_n_0,
      I2 => q_reg_i_310_n_0,
      I3 => q_reg_i_175_n_0,
      I4 => q_reg_i_351_n_0,
      I5 => q_reg_i_89_n_0,
      O => q_reg_i_141_n_0
    );
q_reg_i_142: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000057FFFFFFFF"
    )
        port map (
      I0 => q_reg_i_218_n_0,
      I1 => q_reg_i_284_n_0,
      I2 => pre_reg_cnt(295),
      I3 => pre_reg_cnt(301),
      I4 => q_reg_i_352_n_0,
      I5 => q_reg_i_353_n_0,
      O => q_reg_i_142_n_0
    );
q_reg_i_143: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000AE00000000"
    )
        port map (
      I0 => q_reg_i_248_n_0,
      I1 => q_reg_i_354_n_0,
      I2 => q_reg_i_355_n_0,
      I3 => q_reg_i_356_n_0,
      I4 => q_reg_i_357_n_0,
      I5 => q_reg_i_358_n_0,
      O => q_reg_i_143_n_0
    );
q_reg_i_144: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000AB"
    )
        port map (
      I0 => q_reg_i_359_n_0,
      I1 => q_reg_i_360_n_0,
      I2 => q_reg_i_361_n_0,
      I3 => q_reg_i_362_n_0,
      I4 => q_reg_i_363_n_0,
      I5 => q_reg_i_200_n_0,
      O => q_reg_i_144_n_0
    );
q_reg_i_145: unisim.vcomponents.LUT6
    generic map(
      INIT => X"11110001FFFFFFFF"
    )
        port map (
      I0 => q_reg_i_364_n_0,
      I1 => q_reg_i_365_n_0,
      I2 => q_reg_i_366_n_0,
      I3 => q_reg_i_367_n_0,
      I4 => q_reg_i_289_n_0,
      I5 => q_reg_i_257_n_0,
      O => q_reg_i_145_n_0
    );
q_reg_i_146: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0E0E0E0E0E0F0E0E"
    )
        port map (
      I0 => q_reg_i_270_n_0,
      I1 => q_reg_i_368_n_0,
      I2 => q_reg_i_369_n_0,
      I3 => q_reg_i_370_n_0,
      I4 => q_reg_i_371_n_0,
      I5 => q_reg_i_372_n_0,
      O => q_reg_i_146_n_0
    );
q_reg_i_147: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000FFBA"
    )
        port map (
      I0 => q_reg_i_373_n_0,
      I1 => pre_reg_cnt(19),
      I2 => pre_reg_cnt(18),
      I3 => q_reg_i_374_n_0,
      I4 => q_reg_i_375_n_0,
      I5 => q_reg_i_376_n_0,
      O => q_reg_i_147_n_0
    );
q_reg_i_148: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => q_reg_i_269_n_0,
      I1 => pre_reg_cnt(31),
      I2 => pre_reg_cnt(30),
      O => q_reg_i_148_n_0
    );
q_reg_i_149: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000000E"
    )
        port map (
      I0 => q_reg_i_377_n_0,
      I1 => q_reg_i_378_n_0,
      I2 => q_reg_i_379_n_0,
      I3 => q_reg_i_380_n_0,
      I4 => q_reg_i_269_n_0,
      I5 => pre_reg_cnt(31),
      O => q_reg_i_149_n_0
    );
q_reg_i_150: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000FF0E"
    )
        port map (
      I0 => q_reg_i_381_n_0,
      I1 => pre_reg_cnt(35),
      I2 => pre_reg_cnt(36),
      I3 => pre_reg_cnt(37),
      I4 => q_reg_i_382_n_0,
      I5 => q_reg_i_383_n_0,
      O => q_reg_i_150_n_0
    );
q_reg_i_151: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFBFFFFFFFAFFFF"
    )
        port map (
      I0 => q_reg_i_384_n_0,
      I1 => pre_reg_cnt(40),
      I2 => pre_reg_cnt(41),
      I3 => q_reg_i_385_n_0,
      I4 => q_reg_i_386_n_0,
      I5 => pre_reg_cnt(39),
      O => q_reg_i_151_n_0
    );
q_reg_i_152: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEF0"
    )
        port map (
      I0 => q_reg_i_387_n_0,
      I1 => pre_reg_cnt(51),
      I2 => q_reg_i_237_n_0,
      I3 => q_reg_i_388_n_0,
      I4 => q_reg_i_389_n_0,
      I5 => q_reg_i_216_n_0,
      O => q_reg_i_152_n_0
    );
q_reg_i_153: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FF0E00000000"
    )
        port map (
      I0 => q_reg_i_390_n_0,
      I1 => pre_reg_cnt(44),
      I2 => pre_reg_cnt(45),
      I3 => pre_reg_cnt(46),
      I4 => pre_reg_cnt(47),
      I5 => q_reg_i_386_n_0,
      O => q_reg_i_153_n_0
    );
q_reg_i_154: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF0FE"
    )
        port map (
      I0 => q_reg_i_391_n_0,
      I1 => pre_reg_cnt(72),
      I2 => q_reg_i_392_n_0,
      I3 => pre_reg_cnt(73),
      I4 => q_reg_i_393_n_0,
      I5 => q_reg_i_394_n_0,
      O => q_reg_i_154_n_0
    );
q_reg_i_155: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => pre_reg_cnt(79),
      I1 => q_reg_i_395_n_0,
      I2 => q_reg_i_305_n_0,
      O => q_reg_i_155_n_0
    );
q_reg_i_156: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(61),
      I1 => pre_reg_cnt(60),
      I2 => pre_reg_cnt(63),
      I3 => pre_reg_cnt(62),
      I4 => q_reg_i_216_n_0,
      O => q_reg_i_156_n_0
    );
q_reg_i_157: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000CC08"
    )
        port map (
      I0 => q_reg_i_396_n_0,
      I1 => q_reg_i_266_n_0,
      I2 => pre_reg_cnt(54),
      I3 => pre_reg_cnt(55),
      I4 => q_reg_i_216_n_0,
      I5 => q_reg_i_237_n_0,
      O => q_reg_i_157_n_0
    );
q_reg_i_158: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF4544"
    )
        port map (
      I0 => q_reg_i_289_n_0,
      I1 => q_reg_i_367_n_0,
      I2 => pre_reg_cnt(61),
      I3 => pre_reg_cnt(60),
      I4 => q_reg_i_365_n_0,
      I5 => q_reg_i_364_n_0,
      O => q_reg_i_158_n_0
    );
q_reg_i_159: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => q_reg_i_72_n_0,
      I1 => q_reg_i_170_n_0,
      I2 => q_reg_i_171_n_0,
      O => q_reg_i_159_n_0
    );
q_reg_i_160: unisim.vcomponents.LUT5
    generic map(
      INIT => X"757575FF"
    )
        port map (
      I0 => q_reg_i_79_n_0,
      I1 => q_reg_i_63_n_0,
      I2 => q_reg_i_80_n_0,
      I3 => q_reg_i_81_n_0,
      I4 => q_reg_i_82_n_0,
      O => q_reg_i_160_n_0
    );
q_reg_i_161: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFD0D0D0"
    )
        port map (
      I0 => q_reg_i_85_n_0,
      I1 => q_reg_i_84_n_0,
      I2 => q_reg_i_83_n_0,
      I3 => q_reg_i_87_n_0,
      I4 => q_reg_i_86_n_0,
      O => q_reg_i_161_n_0
    );
q_reg_i_162: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => q_reg_i_172_n_0,
      I1 => q_reg_i_67_n_0,
      I2 => q_reg_i_219_n_0,
      O => q_reg_i_162_n_0
    );
q_reg_i_163: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => q_reg_i_73_n_0,
      I1 => q_reg_i_74_n_0,
      I2 => q_reg_i_75_n_0,
      O => q_reg_i_163_n_0
    );
q_reg_i_164: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(142),
      I1 => pre_reg_cnt(143),
      I2 => pre_reg_cnt(140),
      I3 => pre_reg_cnt(141),
      O => q_reg_i_164_n_0
    );
q_reg_i_165: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_397_n_0,
      I1 => q_reg_i_398_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_400_n_0,
      I4 => q_reg_i_401_n_0,
      I5 => q_reg_i_402_n_0,
      O => q_reg_i_165_n_0
    );
q_reg_i_166: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(139),
      I1 => pre_reg_cnt(138),
      O => q_reg_i_166_n_0
    );
q_reg_i_167: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CD32CD32CD3232CD"
    )
        port map (
      I0 => q_reg_i_219_n_0,
      I1 => q_reg_i_67_n_0,
      I2 => q_reg_i_172_n_0,
      I3 => q_reg_i_161_n_0,
      I4 => q_reg_i_403_n_0,
      I5 => q_reg_i_404_n_0,
      O => q_reg_i_167_n_0
    );
q_reg_i_168: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696966996696969"
    )
        port map (
      I0 => \^q_reg[109]_0\,
      I1 => q_reg_i_81_n_0,
      I2 => q_reg_i_405_n_0,
      I3 => q_reg_i_78_n_0,
      I4 => q_reg_i_77_n_0,
      I5 => q_reg_i_76_n_0,
      O => q_reg_i_168_n_0
    );
q_reg_i_169: unisim.vcomponents.LUT4
    generic map(
      INIT => X"888A"
    )
        port map (
      I0 => q_reg_i_175_n_0,
      I1 => q_reg_i_63_n_0,
      I2 => pre_reg_cnt(135),
      I3 => q_reg_i_241_n_0,
      O => q_reg_i_169_n_0
    );
q_reg_i_170: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555100055555555"
    )
        port map (
      I0 => q_reg_i_180_n_0,
      I1 => q_reg_i_85_n_0,
      I2 => q_reg_i_181_n_0,
      I3 => q_reg_i_182_n_0,
      I4 => q_reg_i_183_n_0,
      I5 => q_reg_i_121_n_0,
      O => q_reg_i_170_n_0
    );
q_reg_i_171: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => q_reg_i_85_n_0,
      I1 => q_reg_i_406_n_0,
      O => q_reg_i_171_n_0
    );
q_reg_i_172: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D02FD0D0"
    )
        port map (
      I0 => q_reg_i_407_n_0,
      I1 => q_reg_i_241_n_0,
      I2 => q_reg_i_408_n_0,
      I3 => q_reg_i_63_n_0,
      I4 => q_reg_i_202_n_0,
      O => q_reg_i_172_n_0
    );
q_reg_i_173: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_409_n_0,
      I1 => q_reg_i_402_n_0,
      I2 => q_reg_i_410_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_398_n_0,
      I5 => q_reg_i_411_n_0,
      O => q_reg_i_173_n_0
    );
q_reg_i_174: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(147),
      I1 => pre_reg_cnt(146),
      O => q_reg_i_174_n_0
    );
q_reg_i_175: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => pre_reg_cnt(217),
      I1 => q_reg_i_330_n_0,
      I2 => q_reg_i_132_n_0,
      I3 => pre_reg_cnt(219),
      I4 => pre_reg_cnt(218),
      O => q_reg_i_175_n_0
    );
q_reg_i_176: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0011001000110011"
    )
        port map (
      I0 => pre_reg_cnt(95),
      I1 => pre_reg_cnt(94),
      I2 => pre_reg_cnt(91),
      I3 => q_reg_i_305_n_0,
      I4 => q_reg_i_412_n_0,
      I5 => q_reg_i_413_n_0,
      O => q_reg_i_176_n_0
    );
q_reg_i_177: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3020302030203030"
    )
        port map (
      I0 => pre_reg_cnt(37),
      I1 => q_reg_i_414_n_0,
      I2 => q_reg_i_386_n_0,
      I3 => q_reg_i_382_n_0,
      I4 => pre_reg_cnt(34),
      I5 => q_reg_i_415_n_0,
      O => q_reg_i_177_n_0
    );
q_reg_i_178: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0055005000550054"
    )
        port map (
      I0 => pre_reg_cnt(175),
      I1 => q_reg_i_416_n_0,
      I2 => pre_reg_cnt(172),
      I3 => q_reg_i_319_n_0,
      I4 => q_reg_i_417_n_0,
      I5 => pre_reg_cnt(171),
      O => q_reg_i_178_n_0
    );
q_reg_i_179: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF1110FFFFFFFF"
    )
        port map (
      I0 => q_reg_i_418_n_0,
      I1 => q_reg_i_419_n_0,
      I2 => pre_reg_cnt(115),
      I3 => q_reg_i_420_n_0,
      I4 => q_reg_i_421_n_0,
      I5 => q_reg_i_422_n_0,
      O => q_reg_i_179_n_0
    );
q_reg_i_18: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2BD4D42BD42B2BD4"
    )
        port map (
      I0 => \^q_reg[34]_0\,
      I1 => \^q_reg_i_96_0\,
      I2 => \^q_reg_i_93_0\,
      I3 => q_reg(1),
      I4 => q_reg_i_46_n_0,
      I5 => q_reg_i_47_n_0,
      O => q_reg_i_47_0(1)
    );
q_reg_i_180: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D0DDD000DDDDDDDD"
    )
        port map (
      I0 => q_reg_i_423_n_0,
      I1 => q_reg_i_424_n_0,
      I2 => q_reg_i_425_n_0,
      I3 => q_reg_i_241_n_0,
      I4 => pre_reg_cnt(135),
      I5 => q_reg_i_291_n_0,
      O => q_reg_i_180_n_0
    );
q_reg_i_181: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(154),
      I1 => pre_reg_cnt(155),
      I2 => q_reg_i_298_n_0,
      I3 => q_reg_i_426_n_0,
      O => q_reg_i_181_n_0
    );
q_reg_i_182: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFF01"
    )
        port map (
      I0 => q_reg_i_427_n_0,
      I1 => q_reg_i_315_n_0,
      I2 => pre_reg_cnt(163),
      I3 => q_reg_i_417_n_0,
      I4 => q_reg_i_319_n_0,
      I5 => pre_reg_cnt(172),
      O => q_reg_i_182_n_0
    );
q_reg_i_183: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA8"
    )
        port map (
      I0 => q_reg_i_311_n_0,
      I1 => pre_reg_cnt(226),
      I2 => pre_reg_cnt(227),
      I3 => q_reg_i_428_n_0,
      I4 => q_reg_i_429_n_0,
      I5 => q_reg_i_175_n_0,
      O => q_reg_i_183_n_0
    );
q_reg_i_184: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000000B"
    )
        port map (
      I0 => pre_reg_cnt(82),
      I1 => pre_reg_cnt(81),
      I2 => q_reg_i_430_n_0,
      I3 => q_reg_i_413_n_0,
      I4 => q_reg_i_305_n_0,
      I5 => pre_reg_cnt(83),
      O => q_reg_i_184_n_0
    );
q_reg_i_185: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(93),
      I1 => pre_reg_cnt(92),
      I2 => pre_reg_cnt(95),
      I3 => pre_reg_cnt(94),
      I4 => q_reg_i_305_n_0,
      O => q_reg_i_185_n_0
    );
q_reg_i_186: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEFFFFFFFEE"
    )
        port map (
      I0 => q_reg_i_394_n_0,
      I1 => q_reg_i_393_n_0,
      I2 => pre_reg_cnt(73),
      I3 => pre_reg_cnt(74),
      I4 => pre_reg_cnt(75),
      I5 => pre_reg_cnt(72),
      O => q_reg_i_186_n_0
    );
q_reg_i_187: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(239),
      I1 => pre_reg_cnt(238),
      I2 => pre_reg_cnt(237),
      I3 => pre_reg_cnt(236),
      I4 => q_reg_i_125_n_0,
      O => q_reg_i_187_n_0
    );
q_reg_i_188: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFEFE"
    )
        port map (
      I0 => pre_reg_cnt(255),
      I1 => pre_reg_cnt(254),
      I2 => q_reg_i_399_n_0,
      I3 => pre_reg_cnt(253),
      I4 => pre_reg_cnt(252),
      O => q_reg_i_188_n_0
    );
q_reg_i_189: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001010001"
    )
        port map (
      I0 => q_reg_i_293_n_0,
      I1 => pre_reg_cnt(247),
      I2 => pre_reg_cnt(246),
      I3 => pre_reg_cnt(243),
      I4 => pre_reg_cnt(244),
      I5 => pre_reg_cnt(245),
      O => q_reg_i_189_n_0
    );
q_reg_i_19: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8E71718E718E8E71"
    )
        port map (
      I0 => \^q_reg[55]_0\,
      I1 => \^q_reg[295]_0\,
      I2 => \^q_reg[43]_0\,
      I3 => q_reg(0),
      I4 => q_reg_i_48_n_0,
      I5 => q_reg_i_49_n_0,
      O => q_reg_i_47_0(0)
    );
q_reg_i_190: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0011001000110011"
    )
        port map (
      I0 => pre_reg_cnt(167),
      I1 => pre_reg_cnt(166),
      I2 => pre_reg_cnt(163),
      I3 => q_reg_i_323_n_0,
      I4 => q_reg_i_427_n_0,
      I5 => q_reg_i_298_n_0,
      O => q_reg_i_190_n_0
    );
q_reg_i_191: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => pre_reg_cnt(180),
      I1 => pre_reg_cnt(178),
      I2 => pre_reg_cnt(179),
      O => q_reg_i_191_n_0
    );
q_reg_i_192: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(181),
      I1 => q_reg_i_402_n_0,
      I2 => q_reg_i_410_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_193_n_0,
      I5 => q_reg_i_233_n_0,
      O => q_reg_i_192_n_0
    );
q_reg_i_193: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_320_n_0,
      I1 => pre_reg_cnt(185),
      I2 => pre_reg_cnt(184),
      I3 => pre_reg_cnt(187),
      I4 => pre_reg_cnt(186),
      O => q_reg_i_193_n_0
    );
q_reg_i_194: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => q_reg_i_399_n_0,
      I1 => q_reg_i_400_n_0,
      I2 => q_reg_i_401_n_0,
      I3 => q_reg_i_402_n_0,
      O => q_reg_i_194_n_0
    );
q_reg_i_195: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000BBFB"
    )
        port map (
      I0 => q_reg_i_85_n_0,
      I1 => q_reg_i_181_n_0,
      I2 => q_reg_i_431_n_0,
      I3 => q_reg_i_408_n_0,
      I4 => q_reg_i_183_n_0,
      O => q_reg_i_195_n_0
    );
q_reg_i_196: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DDD0"
    )
        port map (
      I0 => q_reg_i_241_n_0,
      I1 => q_reg_i_432_n_0,
      I2 => q_reg_i_200_n_0,
      I3 => pre_reg_cnt(127),
      O => q_reg_i_196_n_0
    );
q_reg_i_197: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF1"
    )
        port map (
      I0 => q_reg_i_433_n_0,
      I1 => q_reg_i_222_n_0,
      I2 => q_reg_i_434_n_0,
      I3 => q_reg_i_83_n_0,
      O => q_reg_i_197_n_0
    );
q_reg_i_198: unisim.vcomponents.LUT6
    generic map(
      INIT => X"10101055FFFFFFFF"
    )
        port map (
      I0 => q_reg_i_86_n_0,
      I1 => q_reg_i_220_n_0,
      I2 => q_reg_i_435_n_0,
      I3 => q_reg_i_436_n_0,
      I4 => q_reg_i_437_n_0,
      I5 => q_reg_i_259_n_0,
      O => q_reg_i_198_n_0
    );
q_reg_i_199: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(126),
      I1 => pre_reg_cnt(127),
      O => q_reg_i_199_n_0
    );
q_reg_i_200: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_402_n_0,
      I1 => q_reg_i_401_n_0,
      I2 => q_reg_i_400_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_438_n_0,
      O => q_reg_i_200_n_0
    );
q_reg_i_201: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFCFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(144),
      I1 => q_reg_i_174_n_0,
      I2 => q_reg_i_411_n_0,
      I3 => q_reg_i_298_n_0,
      I4 => q_reg_i_409_n_0,
      I5 => pre_reg_cnt(145),
      O => q_reg_i_201_n_0
    );
q_reg_i_202: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEFFFFFFFEE"
    )
        port map (
      I0 => q_reg_i_164_n_0,
      I1 => q_reg_i_166_n_0,
      I2 => pre_reg_cnt(136),
      I3 => pre_reg_cnt(137),
      I4 => q_reg_i_165_n_0,
      I5 => pre_reg_cnt(135),
      O => q_reg_i_202_n_0
    );
q_reg_i_203: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA02000202"
    )
        port map (
      I0 => q_reg_i_439_n_0,
      I1 => q_reg_i_427_n_0,
      I2 => q_reg_i_323_n_0,
      I3 => pre_reg_cnt(163),
      I4 => pre_reg_cnt(162),
      I5 => q_reg_i_314_n_0,
      O => q_reg_i_203_n_0
    );
q_reg_i_204: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A8AAAA"
    )
        port map (
      I0 => q_reg_i_440_n_0,
      I1 => pre_reg_cnt(198),
      I2 => q_reg_i_307_n_0,
      I3 => pre_reg_cnt(199),
      I4 => q_reg_i_441_n_0,
      O => q_reg_i_204_n_0
    );
q_reg_i_205: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFBF0000FFBFFFBF"
    )
        port map (
      I0 => q_reg_i_442_n_0,
      I1 => q_reg_i_443_n_0,
      I2 => q_reg_i_444_n_0,
      I3 => q_reg_i_445_n_0,
      I4 => q_reg_i_446_n_0,
      I5 => q_reg_i_447_n_0,
      O => q_reg_i_205_n_0
    );
q_reg_i_206: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAA80A0"
    )
        port map (
      I0 => q_reg_i_448_n_0,
      I1 => pre_reg_cnt(171),
      I2 => q_reg_i_431_n_0,
      I3 => q_reg_i_416_n_0,
      I4 => pre_reg_cnt(175),
      I5 => q_reg_i_319_n_0,
      O => q_reg_i_206_n_0
    );
q_reg_i_207: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000F000E"
    )
        port map (
      I0 => q_reg_i_449_n_0,
      I1 => q_reg_i_450_n_0,
      I2 => q_reg_i_298_n_0,
      I3 => q_reg_i_451_n_0,
      I4 => pre_reg_cnt(156),
      I5 => pre_reg_cnt(157),
      O => q_reg_i_207_n_0
    );
q_reg_i_208: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010001000100011"
    )
        port map (
      I0 => q_reg_i_164_n_0,
      I1 => pre_reg_cnt(139),
      I2 => q_reg_i_452_n_0,
      I3 => q_reg_i_165_n_0,
      I4 => pre_reg_cnt(133),
      I5 => q_reg_i_453_n_0,
      O => q_reg_i_208_n_0
    );
q_reg_i_209: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000301"
    )
        port map (
      I0 => pre_reg_cnt(147),
      I1 => q_reg_i_411_n_0,
      I2 => q_reg_i_298_n_0,
      I3 => pre_reg_cnt(148),
      I4 => pre_reg_cnt(151),
      I5 => q_reg_i_454_n_0,
      O => q_reg_i_209_n_0
    );
q_reg_i_210: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => pre_reg_cnt(143),
      I1 => pre_reg_cnt(142),
      I2 => pre_reg_cnt(141),
      O => q_reg_i_210_n_0
    );
q_reg_i_211: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => pre_reg_cnt(145),
      I1 => q_reg_i_409_n_0,
      I2 => q_reg_i_298_n_0,
      I3 => q_reg_i_411_n_0,
      I4 => pre_reg_cnt(147),
      I5 => pre_reg_cnt(146),
      O => q_reg_i_211_n_0
    );
q_reg_i_212: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => q_reg_i_328_n_0,
      I1 => q_reg_i_327_n_0,
      I2 => q_reg_i_326_n_0,
      I3 => q_reg_i_324_n_0,
      O => q_reg_i_212_n_0
    );
q_reg_i_213: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_395_n_0,
      I1 => q_reg_i_455_n_0,
      I2 => q_reg_i_438_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_456_n_0,
      I5 => q_reg_i_457_n_0,
      O => q_reg_i_213_n_0
    );
q_reg_i_214: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(70),
      I1 => pre_reg_cnt(71),
      I2 => pre_reg_cnt(68),
      I3 => pre_reg_cnt(69),
      O => q_reg_i_214_n_0
    );
q_reg_i_215: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(63),
      I1 => pre_reg_cnt(62),
      O => q_reg_i_215_n_0
    );
q_reg_i_216: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_402_n_0,
      I1 => q_reg_i_410_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_455_n_0,
      I4 => q_reg_i_458_n_0,
      I5 => q_reg_i_438_n_0,
      O => q_reg_i_216_n_0
    );
q_reg_i_217: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1010101010101011"
    )
        port map (
      I0 => q_reg_i_459_n_0,
      I1 => pre_reg_cnt(283),
      I2 => q_reg_i_231_n_0,
      I3 => pre_reg_cnt(279),
      I4 => pre_reg_cnt(277),
      I5 => pre_reg_cnt(278),
      O => q_reg_i_217_n_0
    );
q_reg_i_218: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => pre_reg_cnt(298),
      I1 => pre_reg_cnt(299),
      I2 => q_reg_i_460_n_0,
      O => q_reg_i_218_n_0
    );
q_reg_i_219: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DDDDDDD5"
    )
        port map (
      I0 => q_reg_i_461_n_0,
      I1 => q_reg_i_175_n_0,
      I2 => q_reg_i_194_n_0,
      I3 => pre_reg_cnt(190),
      I4 => pre_reg_cnt(191),
      O => q_reg_i_219_n_0
    );
q_reg_i_220: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(10),
      I1 => pre_reg_cnt(11),
      I2 => q_reg_i_462_n_0,
      I3 => q_reg_i_463_n_0,
      I4 => pre_reg_cnt(16),
      I5 => q_reg_i_376_n_0,
      O => q_reg_i_220_n_0
    );
q_reg_i_221: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000003200000033"
    )
        port map (
      I0 => q_reg_i_464_n_0,
      I1 => q_reg_i_428_n_0,
      I2 => pre_reg_cnt(228),
      I3 => pre_reg_cnt(229),
      I4 => q_reg_i_465_n_0,
      I5 => q_reg_i_466_n_0,
      O => q_reg_i_221_n_0
    );
q_reg_i_222: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => pre_reg_cnt(82),
      I1 => pre_reg_cnt(83),
      I2 => q_reg_i_305_n_0,
      I3 => q_reg_i_413_n_0,
      I4 => q_reg_i_430_n_0,
      O => q_reg_i_222_n_0
    );
q_reg_i_223: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => q_reg_i_269_n_0,
      I1 => pre_reg_cnt(31),
      I2 => pre_reg_cnt(28),
      I3 => pre_reg_cnt(30),
      I4 => pre_reg_cnt(29),
      O => q_reg_i_223_n_0
    );
q_reg_i_224: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(55),
      I1 => q_reg_i_237_n_0,
      I2 => q_reg_i_456_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_467_n_0,
      I5 => q_reg_i_438_n_0,
      O => q_reg_i_224_n_0
    );
q_reg_i_225: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1010101010101011"
    )
        port map (
      I0 => q_reg_i_468_n_0,
      I1 => pre_reg_cnt(163),
      I2 => q_reg_i_241_n_0,
      I3 => pre_reg_cnt(109),
      I4 => q_reg_i_229_n_0,
      I5 => q_reg_i_230_n_0,
      O => q_reg_i_225_n_0
    );
q_reg_i_226: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_469_n_0,
      I1 => pre_reg_cnt(107),
      I2 => pre_reg_cnt(106),
      I3 => pre_reg_cnt(105),
      I4 => pre_reg_cnt(104),
      O => q_reg_i_226_n_0
    );
q_reg_i_227: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(102),
      I1 => pre_reg_cnt(103),
      I2 => pre_reg_cnt(100),
      I3 => pre_reg_cnt(101),
      O => q_reg_i_227_n_0
    );
q_reg_i_228: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_418_n_0,
      I1 => q_reg_i_402_n_0,
      I2 => q_reg_i_410_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_438_n_0,
      I5 => q_reg_i_290_n_0,
      O => q_reg_i_228_n_0
    );
q_reg_i_229: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_470_n_0,
      I1 => q_reg_i_438_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_400_n_0,
      I4 => q_reg_i_401_n_0,
      I5 => q_reg_i_402_n_0,
      O => q_reg_i_229_n_0
    );
q_reg_i_23: unisim.vcomponents.LUT6
    generic map(
      INIT => X"65A6FFFF000065A6"
    )
        port map (
      I0 => q_reg_i_59_n_0,
      I1 => \^q_reg[271]_0\,
      I2 => \^q_reg[109]_0\,
      I3 => \^q_reg_i_78_0\,
      I4 => q_reg_i_60_n_0,
      I5 => q_reg_i_61_n_0,
      O => \^di\(1)
    );
q_reg_i_230: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(111),
      I1 => pre_reg_cnt(110),
      O => q_reg_i_230_n_0
    );
q_reg_i_231: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => q_reg_i_328_n_0,
      I1 => q_reg_i_327_n_0,
      I2 => q_reg_i_326_n_0,
      I3 => q_reg_i_471_n_0,
      O => q_reg_i_231_n_0
    );
q_reg_i_232: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(265),
      I1 => pre_reg_cnt(264),
      I2 => pre_reg_cnt(267),
      I3 => pre_reg_cnt(266),
      I4 => q_reg_i_472_n_0,
      O => q_reg_i_232_n_0
    );
q_reg_i_233: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(183),
      I1 => pre_reg_cnt(182),
      O => q_reg_i_233_n_0
    );
q_reg_i_234: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(199),
      I1 => q_reg_i_400_n_0,
      I2 => q_reg_i_132_n_0,
      I3 => q_reg_i_473_n_0,
      O => q_reg_i_234_n_0
    );
q_reg_i_235: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(191),
      I1 => pre_reg_cnt(190),
      O => q_reg_i_235_n_0
    );
q_reg_i_236: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_389_n_0,
      I1 => pre_reg_cnt(51),
      I2 => pre_reg_cnt(48),
      I3 => pre_reg_cnt(50),
      I4 => pre_reg_cnt(49),
      O => q_reg_i_236_n_0
    );
q_reg_i_237: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(57),
      I1 => pre_reg_cnt(56),
      I2 => pre_reg_cnt(59),
      I3 => pre_reg_cnt(58),
      I4 => q_reg_i_474_n_0,
      O => q_reg_i_237_n_0
    );
q_reg_i_238: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_237_n_0,
      I1 => q_reg_i_456_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_467_n_0,
      I4 => q_reg_i_438_n_0,
      I5 => q_reg_i_475_n_0,
      O => q_reg_i_238_n_0
    );
q_reg_i_239: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => pre_reg_cnt(137),
      I1 => pre_reg_cnt(138),
      I2 => pre_reg_cnt(139),
      O => q_reg_i_239_n_0
    );
q_reg_i_24: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => q_reg_i_59_n_0,
      I1 => \^q_reg[271]_0\,
      I2 => \^q_reg[109]_0\,
      I3 => \^q_reg_i_78_0\,
      I4 => q_reg_i_61_n_0,
      I5 => q_reg_i_60_n_0,
      O => \^di\(0)
    );
q_reg_i_240: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_164_n_0,
      I1 => q_reg_i_402_n_0,
      I2 => q_reg_i_410_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_398_n_0,
      I5 => q_reg_i_397_n_0,
      O => q_reg_i_240_n_0
    );
q_reg_i_241: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_452_n_0,
      I1 => q_reg_i_402_n_0,
      I2 => q_reg_i_410_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_398_n_0,
      I5 => q_reg_i_397_n_0,
      O => q_reg_i_241_n_0
    );
q_reg_i_242: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFFFFFEFFFE"
    )
        port map (
      I0 => q_reg_i_414_n_0,
      I1 => q_reg_i_476_n_0,
      I2 => q_reg_i_216_n_0,
      I3 => q_reg_i_382_n_0,
      I4 => pre_reg_cnt(37),
      I5 => pre_reg_cnt(36),
      O => q_reg_i_242_n_0
    );
q_reg_i_243: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_216_n_0,
      I1 => q_reg_i_476_n_0,
      I2 => q_reg_i_414_n_0,
      I3 => q_reg_i_477_n_0,
      I4 => pre_reg_cnt(35),
      O => q_reg_i_243_n_0
    );
q_reg_i_244: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFAAFE"
    )
        port map (
      I0 => pre_reg_cnt(247),
      I1 => pre_reg_cnt(241),
      I2 => q_reg_i_478_n_0,
      I3 => q_reg_i_479_n_0,
      I4 => q_reg_i_480_n_0,
      I5 => q_reg_i_399_n_0,
      O => q_reg_i_244_n_0
    );
q_reg_i_245: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0E0E0E0E0E0E0E0F"
    )
        port map (
      I0 => pre_reg_cnt(253),
      I1 => q_reg_i_481_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => pre_reg_cnt(250),
      I4 => pre_reg_cnt(251),
      I5 => pre_reg_cnt(252),
      O => q_reg_i_245_n_0
    );
q_reg_i_246: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(266),
      I1 => pre_reg_cnt(267),
      I2 => q_reg_i_212_n_0,
      I3 => q_reg_i_472_n_0,
      O => q_reg_i_246_n_0
    );
q_reg_i_247: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000FE"
    )
        port map (
      I0 => pre_reg_cnt(259),
      I1 => pre_reg_cnt(261),
      I2 => pre_reg_cnt(260),
      I3 => pre_reg_cnt(263),
      I4 => pre_reg_cnt(262),
      I5 => q_reg_i_482_n_0,
      O => q_reg_i_247_n_0
    );
q_reg_i_248: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF2"
    )
        port map (
      I0 => pre_reg_cnt(9),
      I1 => pre_reg_cnt(10),
      I2 => pre_reg_cnt(11),
      I3 => q_reg_i_462_n_0,
      I4 => q_reg_i_483_n_0,
      I5 => q_reg_i_376_n_0,
      O => q_reg_i_248_n_0
    );
q_reg_i_249: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1011"
    )
        port map (
      I0 => q_reg_i_305_n_0,
      I1 => q_reg_i_412_n_0,
      I2 => pre_reg_cnt(91),
      I3 => pre_reg_cnt(90),
      O => q_reg_i_249_n_0
    );
q_reg_i_25: unisim.vcomponents.LUT4
    generic map(
      INIT => X"40FF"
    )
        port map (
      I0 => q_reg_i_62_n_0,
      I1 => \^q_reg_i_64_0\,
      I2 => \^q_reg_i_163_0\,
      I3 => q_reg_i_63_n_0,
      O => S(2)
    );
q_reg_i_250: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF1"
    )
        port map (
      I0 => pre_reg_cnt(84),
      I1 => q_reg_i_484_n_0,
      I2 => q_reg_i_485_n_0,
      I3 => q_reg_i_413_n_0,
      I4 => q_reg_i_305_n_0,
      I5 => pre_reg_cnt(85),
      O => q_reg_i_250_n_0
    );
q_reg_i_251: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040404040405"
    )
        port map (
      I0 => pre_reg_cnt(103),
      I1 => q_reg_i_227_n_0,
      I2 => q_reg_i_292_n_0,
      I3 => pre_reg_cnt(97),
      I4 => pre_reg_cnt(98),
      I5 => pre_reg_cnt(99),
      O => q_reg_i_251_n_0
    );
q_reg_i_252: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => q_reg_i_79_n_0,
      I1 => q_reg_i_276_n_0,
      O => q_reg_i_252_n_0
    );
q_reg_i_253: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF55FF55FF55FFFD"
    )
        port map (
      I0 => q_reg_i_486_n_0,
      I1 => q_reg_i_463_n_0,
      I2 => pre_reg_cnt(16),
      I3 => q_reg_i_376_n_0,
      I4 => pre_reg_cnt(19),
      I5 => q_reg_i_373_n_0,
      O => q_reg_i_253_n_0
    );
q_reg_i_254: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888A8888888A888A"
    )
        port map (
      I0 => q_reg_i_189_n_0,
      I1 => q_reg_i_128_n_0,
      I2 => q_reg_i_194_n_0,
      I3 => pre_reg_cnt(191),
      I4 => pre_reg_cnt(190),
      I5 => pre_reg_cnt(189),
      O => q_reg_i_254_n_0
    );
q_reg_i_255: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000D0000"
    )
        port map (
      I0 => q_reg_i_466_n_0,
      I1 => q_reg_i_487_n_0,
      I2 => pre_reg_cnt(229),
      I3 => q_reg_i_488_n_0,
      I4 => q_reg_i_443_n_0,
      I5 => q_reg_i_442_n_0,
      O => q_reg_i_255_n_0
    );
q_reg_i_256: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => q_reg_i_217_n_0,
      I1 => q_reg_i_489_n_0,
      O => q_reg_i_256_n_0
    );
q_reg_i_257: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000BABB"
    )
        port map (
      I0 => q_reg_i_412_n_0,
      I1 => pre_reg_cnt(91),
      I2 => pre_reg_cnt(90),
      I3 => q_reg_i_490_n_0,
      I4 => q_reg_i_491_n_0,
      I5 => q_reg_i_305_n_0,
      O => q_reg_i_257_n_0
    );
q_reg_i_258: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00AE000000000000"
    )
        port map (
      I0 => q_reg_i_188_n_0,
      I1 => q_reg_i_492_n_0,
      I2 => q_reg_i_493_n_0,
      I3 => q_reg_i_494_n_0,
      I4 => q_reg_i_495_n_0,
      I5 => q_reg_i_496_n_0,
      O => q_reg_i_258_n_0
    );
q_reg_i_259: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(317),
      I1 => pre_reg_cnt(316),
      I2 => pre_reg_cnt(318),
      I3 => pre_reg_cnt(319),
      I4 => pre_reg_cnt(314),
      I5 => pre_reg_cnt(315),
      O => q_reg_i_259_n_0
    );
q_reg_i_26: unisim.vcomponents.LUT6
    generic map(
      INIT => X"870F0F7887F078F0"
    )
        port map (
      I0 => q_reg_i_59_n_0,
      I1 => q_reg_i_64_n_0,
      I2 => \^q_reg[137]_0\,
      I3 => \^q_reg_i_163_0\,
      I4 => q_reg_i_65_n_0,
      I5 => q_reg_i_66_n_0,
      O => S(1)
    );
q_reg_i_260: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40F040F040F04040"
    )
        port map (
      I0 => q_reg_i_220_n_0,
      I1 => q_reg_i_435_n_0,
      I2 => q_reg_i_259_n_0,
      I3 => q_reg_i_436_n_0,
      I4 => q_reg_i_497_n_0,
      I5 => q_reg_i_223_n_0,
      O => q_reg_i_260_n_0
    );
q_reg_i_261: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFFFFFEFFFE"
    )
        port map (
      I0 => pre_reg_cnt(31),
      I1 => q_reg_i_269_n_0,
      I2 => pre_reg_cnt(30),
      I3 => pre_reg_cnt(29),
      I4 => pre_reg_cnt(28),
      I5 => pre_reg_cnt(27),
      O => q_reg_i_261_n_0
    );
q_reg_i_262: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF01FF"
    )
        port map (
      I0 => q_reg_i_461_n_0,
      I1 => q_reg_i_498_n_0,
      I2 => pre_reg_cnt(253),
      I3 => q_reg_i_499_n_0,
      I4 => q_reg_i_84_n_0,
      O => q_reg_i_262_n_0
    );
q_reg_i_263: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDDDDDDDDDD0DDDD"
    )
        port map (
      I0 => q_reg_i_500_n_0,
      I1 => q_reg_i_218_n_0,
      I2 => pre_reg_cnt(290),
      I3 => pre_reg_cnt(291),
      I4 => q_reg_i_501_n_0,
      I5 => pre_reg_cnt(289),
      O => q_reg_i_263_n_0
    );
q_reg_i_264: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(45),
      I1 => pre_reg_cnt(44),
      I2 => pre_reg_cnt(47),
      I3 => pre_reg_cnt(46),
      I4 => q_reg_i_476_n_0,
      I5 => q_reg_i_216_n_0,
      O => q_reg_i_264_n_0
    );
q_reg_i_265: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(46),
      I1 => pre_reg_cnt(47),
      I2 => q_reg_i_476_n_0,
      I3 => q_reg_i_216_n_0,
      O => q_reg_i_265_n_0
    );
q_reg_i_266: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(51),
      I1 => q_reg_i_216_n_0,
      I2 => q_reg_i_237_n_0,
      I3 => q_reg_i_389_n_0,
      O => q_reg_i_266_n_0
    );
q_reg_i_267: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(50),
      I1 => pre_reg_cnt(49),
      O => q_reg_i_267_n_0
    );
q_reg_i_268: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(29),
      I1 => pre_reg_cnt(30),
      I2 => pre_reg_cnt(28),
      I3 => pre_reg_cnt(31),
      O => q_reg_i_268_n_0
    );
q_reg_i_269: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_438_n_0,
      I1 => q_reg_i_467_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_410_n_0,
      I4 => q_reg_i_402_n_0,
      I5 => q_reg_i_502_n_0,
      O => q_reg_i_269_n_0
    );
q_reg_i_270: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => q_reg_i_355_n_0,
      I1 => q_reg_i_376_n_0,
      I2 => pre_reg_cnt(16),
      I3 => q_reg_i_463_n_0,
      O => q_reg_i_270_n_0
    );
q_reg_i_271: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000000E"
    )
        port map (
      I0 => pre_reg_cnt(12),
      I1 => q_reg_i_503_n_0,
      I2 => q_reg_i_357_n_0,
      I3 => q_reg_i_376_n_0,
      I4 => q_reg_i_483_n_0,
      I5 => pre_reg_cnt(13),
      O => q_reg_i_271_n_0
    );
q_reg_i_272: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF4"
    )
        port map (
      I0 => pre_reg_cnt(1),
      I1 => q_reg_i_371_n_0,
      I2 => q_reg_i_372_n_0,
      I3 => q_reg_i_355_n_0,
      I4 => q_reg_i_376_n_0,
      I5 => q_reg_i_483_n_0,
      O => q_reg_i_272_n_0
    );
q_reg_i_273: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pre_reg_cnt(318),
      I1 => pre_reg_cnt(319),
      O => q_reg_i_273_n_0
    );
q_reg_i_274: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA8AAAAAAAA"
    )
        port map (
      I0 => q_reg_i_338_n_0,
      I1 => pre_reg_cnt(290),
      I2 => pre_reg_cnt(291),
      I3 => q_reg_i_284_n_0,
      I4 => q_reg_i_504_n_0,
      I5 => pre_reg_cnt(289),
      O => q_reg_i_274_n_0
    );
q_reg_i_275: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(308),
      I1 => pre_reg_cnt(309),
      I2 => pre_reg_cnt(311),
      I3 => pre_reg_cnt(310),
      I4 => q_reg_i_341_n_0,
      O => q_reg_i_275_n_0
    );
q_reg_i_276: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF2"
    )
        port map (
      I0 => pre_reg_cnt(297),
      I1 => pre_reg_cnt(298),
      I2 => pre_reg_cnt(299),
      I3 => q_reg_i_460_n_0,
      O => q_reg_i_276_n_0
    );
q_reg_i_277: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => q_reg_i_505_n_0,
      I1 => q_reg_i_335_n_0,
      I2 => q_reg_i_79_n_0,
      O => q_reg_i_277_n_0
    );
q_reg_i_278: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFEFE"
    )
        port map (
      I0 => q_reg_i_429_n_0,
      I1 => q_reg_i_428_n_0,
      I2 => pre_reg_cnt(227),
      I3 => pre_reg_cnt(226),
      I4 => pre_reg_cnt(225),
      O => q_reg_i_278_n_0
    );
q_reg_i_279: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => q_reg_i_290_n_0,
      I1 => q_reg_i_200_n_0,
      I2 => q_reg_i_420_n_0,
      I3 => q_reg_i_506_n_0,
      O => q_reg_i_279_n_0
    );
q_reg_i_28: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D42B00FFFF00D42B"
    )
        port map (
      I0 => \^q_reg[34]_0\,
      I1 => \^q_reg_i_96_0\,
      I2 => \^q_reg_i_93_0\,
      I3 => \^di\(0),
      I4 => q_reg_i_47_n_0,
      I5 => q_reg_i_46_n_0,
      O => S(0)
    );
q_reg_i_280: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000F2"
    )
        port map (
      I0 => q_reg_i_507_n_0,
      I1 => pre_reg_cnt(108),
      I2 => pre_reg_cnt(109),
      I3 => pre_reg_cnt(110),
      I4 => pre_reg_cnt(111),
      I5 => q_reg_i_229_n_0,
      O => q_reg_i_280_n_0
    );
q_reg_i_281: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000A2"
    )
        port map (
      I0 => q_reg_i_508_n_0,
      I1 => pre_reg_cnt(129),
      I2 => pre_reg_cnt(130),
      I3 => pre_reg_cnt(131),
      I4 => q_reg_i_241_n_0,
      I5 => q_reg_i_509_n_0,
      O => q_reg_i_281_n_0
    );
q_reg_i_282: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000010000"
    )
        port map (
      I0 => q_reg_i_483_n_0,
      I1 => q_reg_i_376_n_0,
      I2 => q_reg_i_355_n_0,
      I3 => q_reg_i_372_n_0,
      I4 => q_reg_i_371_n_0,
      I5 => q_reg_i_370_n_0,
      O => q_reg_i_282_n_0
    );
q_reg_i_283: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFEE"
    )
        port map (
      I0 => q_reg_i_376_n_0,
      I1 => q_reg_i_373_n_0,
      I2 => pre_reg_cnt(19),
      I3 => pre_reg_cnt(18),
      O => q_reg_i_283_n_0
    );
q_reg_i_284: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_326_n_0,
      I1 => pre_reg_cnt(298),
      I2 => pre_reg_cnt(299),
      I3 => pre_reg_cnt(296),
      I4 => pre_reg_cnt(297),
      I5 => q_reg_i_510_n_0,
      O => q_reg_i_284_n_0
    );
q_reg_i_285: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFEFE"
    )
        port map (
      I0 => pre_reg_cnt(303),
      I1 => pre_reg_cnt(302),
      I2 => q_reg_i_326_n_0,
      I3 => pre_reg_cnt(301),
      I4 => pre_reg_cnt(300),
      O => q_reg_i_285_n_0
    );
q_reg_i_286: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => q_reg_i_132_n_0,
      I1 => pre_reg_cnt(223),
      I2 => pre_reg_cnt(222),
      O => q_reg_i_286_n_0
    );
q_reg_i_287: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFEFE"
    )
        port map (
      I0 => q_reg_i_428_n_0,
      I1 => pre_reg_cnt(231),
      I2 => pre_reg_cnt(230),
      I3 => pre_reg_cnt(229),
      I4 => pre_reg_cnt(228),
      O => q_reg_i_287_n_0
    );
q_reg_i_288: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFEFE"
    )
        port map (
      I0 => q_reg_i_216_n_0,
      I1 => q_reg_i_476_n_0,
      I2 => pre_reg_cnt(47),
      I3 => pre_reg_cnt(46),
      I4 => pre_reg_cnt(45),
      O => q_reg_i_288_n_0
    );
q_reg_i_289: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFFFFFEFFFE"
    )
        port map (
      I0 => q_reg_i_511_n_0,
      I1 => q_reg_i_214_n_0,
      I2 => q_reg_i_213_n_0,
      I3 => pre_reg_cnt(65),
      I4 => q_reg_i_216_n_0,
      I5 => pre_reg_cnt(63),
      O => q_reg_i_289_n_0
    );
q_reg_i_29: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B4B44B4BB4B44BB4"
    )
        port map (
      I0 => q_reg_i_67_n_0,
      I1 => q_reg_i_68_n_0,
      I2 => q_reg_i_69_n_0,
      I3 => q_reg_i_70_n_0,
      I4 => q_reg_i_71_n_0,
      I5 => q_reg_i_72_n_0,
      O => \^q_reg[91]_0\
    );
q_reg_i_290: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(121),
      I1 => pre_reg_cnt(120),
      I2 => pre_reg_cnt(123),
      I3 => pre_reg_cnt(122),
      I4 => q_reg_i_363_n_0,
      O => q_reg_i_290_n_0
    );
q_reg_i_291: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_470_n_0,
      I1 => q_reg_i_438_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_456_n_0,
      I4 => q_reg_i_230_n_0,
      I5 => q_reg_i_512_n_0,
      O => q_reg_i_291_n_0
    );
q_reg_i_292: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_226_n_0,
      I1 => q_reg_i_402_n_0,
      I2 => q_reg_i_410_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_438_n_0,
      I5 => q_reg_i_470_n_0,
      O => q_reg_i_292_n_0
    );
q_reg_i_293: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_324_n_0,
      I1 => q_reg_i_325_n_0,
      I2 => q_reg_i_326_n_0,
      I3 => q_reg_i_327_n_0,
      I4 => q_reg_i_328_n_0,
      I5 => q_reg_i_480_n_0,
      O => q_reg_i_293_n_0
    );
q_reg_i_294: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA2AAAA00000000"
    )
        port map (
      I0 => q_reg_i_189_n_0,
      I1 => q_reg_i_461_n_0,
      I2 => pre_reg_cnt(243),
      I3 => pre_reg_cnt(242),
      I4 => pre_reg_cnt(241),
      I5 => q_reg_i_125_n_0,
      O => q_reg_i_294_n_0
    );
q_reg_i_295: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000DF55"
    )
        port map (
      I0 => q_reg_i_505_n_0,
      I1 => pre_reg_cnt(259),
      I2 => pre_reg_cnt(258),
      I3 => q_reg_i_495_n_0,
      I4 => q_reg_i_513_n_0,
      O => q_reg_i_295_n_0
    );
q_reg_i_296: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => pre_reg_cnt(134),
      I1 => pre_reg_cnt(135),
      I2 => q_reg_i_241_n_0,
      O => q_reg_i_296_n_0
    );
q_reg_i_297: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFEE"
    )
        port map (
      I0 => q_reg_i_165_n_0,
      I1 => q_reg_i_164_n_0,
      I2 => pre_reg_cnt(139),
      I3 => pre_reg_cnt(138),
      O => q_reg_i_297_n_0
    );
q_reg_i_298: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_402_n_0,
      I1 => q_reg_i_401_n_0,
      I2 => q_reg_i_400_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_398_n_0,
      O => q_reg_i_298_n_0
    );
q_reg_i_299: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFF45"
    )
        port map (
      I0 => q_reg_i_411_n_0,
      I1 => pre_reg_cnt(151),
      I2 => pre_reg_cnt(150),
      I3 => q_reg_i_514_n_0,
      I4 => q_reg_i_426_n_0,
      I5 => q_reg_i_298_n_0,
      O => q_reg_i_299_n_0
    );
q_reg_i_30: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => q_reg_i_73_n_0,
      I1 => q_reg_i_74_n_0,
      I2 => q_reg_i_75_n_0,
      O => q_reg_i_75_0
    );
q_reg_i_300: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFEE"
    )
        port map (
      I0 => q_reg_i_395_n_0,
      I1 => q_reg_i_305_n_0,
      I2 => pre_reg_cnt(79),
      I3 => pre_reg_cnt(78),
      O => q_reg_i_300_n_0
    );
q_reg_i_301: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(86),
      I1 => pre_reg_cnt(87),
      I2 => q_reg_i_413_n_0,
      I3 => q_reg_i_305_n_0,
      O => q_reg_i_301_n_0
    );
q_reg_i_302: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => q_reg_i_292_n_0,
      I1 => pre_reg_cnt(103),
      I2 => pre_reg_cnt(102),
      O => q_reg_i_302_n_0
    );
q_reg_i_303: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(101),
      I1 => pre_reg_cnt(100),
      I2 => pre_reg_cnt(103),
      I3 => pre_reg_cnt(102),
      I4 => q_reg_i_292_n_0,
      O => q_reg_i_303_n_0
    );
q_reg_i_304: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => pre_reg_cnt(98),
      I1 => pre_reg_cnt(99),
      I2 => pre_reg_cnt(97),
      O => q_reg_i_304_n_0
    );
q_reg_i_305: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_455_n_0,
      I1 => q_reg_i_438_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_400_n_0,
      I4 => q_reg_i_401_n_0,
      I5 => q_reg_i_402_n_0,
      O => q_reg_i_305_n_0
    );
q_reg_i_306: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(186),
      I1 => pre_reg_cnt(187),
      O => q_reg_i_306_n_0
    );
q_reg_i_307: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => q_reg_i_473_n_0,
      I1 => q_reg_i_132_n_0,
      I2 => q_reg_i_400_n_0,
      O => q_reg_i_307_n_0
    );
q_reg_i_308: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(196),
      I1 => pre_reg_cnt(197),
      I2 => pre_reg_cnt(198),
      I3 => pre_reg_cnt(199),
      O => q_reg_i_308_n_0
    );
q_reg_i_309: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(212),
      I1 => pre_reg_cnt(213),
      I2 => pre_reg_cnt(210),
      I3 => pre_reg_cnt(211),
      O => q_reg_i_309_n_0
    );
q_reg_i_31: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => q_reg_i_76_n_0,
      I1 => q_reg_i_77_n_0,
      I2 => q_reg_i_78_n_0,
      O => \^q_reg_i_78_0\
    );
q_reg_i_310: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(214),
      I1 => pre_reg_cnt(215),
      I2 => q_reg_i_132_n_0,
      I3 => q_reg_i_129_n_0,
      O => q_reg_i_310_n_0
    );
q_reg_i_311: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => q_reg_i_400_n_0,
      I1 => q_reg_i_132_n_0,
      O => q_reg_i_311_n_0
    );
q_reg_i_312: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => pre_reg_cnt(197),
      I1 => pre_reg_cnt(196),
      I2 => pre_reg_cnt(195),
      O => q_reg_i_312_n_0
    );
q_reg_i_313: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFDFFFFFFFC"
    )
        port map (
      I0 => pre_reg_cnt(202),
      I1 => pre_reg_cnt(203),
      I2 => q_reg_i_400_n_0,
      I3 => q_reg_i_132_n_0,
      I4 => q_reg_i_515_n_0,
      I5 => pre_reg_cnt(201),
      O => q_reg_i_313_n_0
    );
q_reg_i_314: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_402_n_0,
      I1 => q_reg_i_410_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_516_n_0,
      I4 => q_reg_i_417_n_0,
      I5 => q_reg_i_517_n_0,
      O => q_reg_i_314_n_0
    );
q_reg_i_315: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(170),
      I1 => pre_reg_cnt(169),
      I2 => pre_reg_cnt(171),
      I3 => pre_reg_cnt(168),
      I4 => q_reg_i_518_n_0,
      O => q_reg_i_315_n_0
    );
q_reg_i_316: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => pre_reg_cnt(170),
      I1 => pre_reg_cnt(169),
      O => q_reg_i_316_n_0
    );
q_reg_i_317: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(171),
      I1 => q_reg_i_417_n_0,
      I2 => q_reg_i_516_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_456_n_0,
      I5 => pre_reg_cnt(172),
      O => q_reg_i_317_n_0
    );
q_reg_i_318: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(174),
      I1 => pre_reg_cnt(175),
      O => q_reg_i_318_n_0
    );
q_reg_i_319: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_402_n_0,
      I1 => q_reg_i_401_n_0,
      I2 => q_reg_i_400_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_516_n_0,
      O => q_reg_i_319_n_0
    );
q_reg_i_32: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8A75758A"
    )
        port map (
      I0 => q_reg_i_79_n_0,
      I1 => q_reg_i_63_n_0,
      I2 => q_reg_i_80_n_0,
      I3 => q_reg_i_81_n_0,
      I4 => q_reg_i_82_n_0,
      O => \^q_reg[271]_0\
    );
q_reg_i_320: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(188),
      I1 => pre_reg_cnt(189),
      I2 => pre_reg_cnt(190),
      I3 => pre_reg_cnt(191),
      O => q_reg_i_320_n_0
    );
q_reg_i_321: unisim.vcomponents.LUT4
    generic map(
      INIT => X"000B"
    )
        port map (
      I0 => pre_reg_cnt(178),
      I1 => pre_reg_cnt(177),
      I2 => pre_reg_cnt(179),
      I3 => pre_reg_cnt(180),
      O => q_reg_i_321_n_0
    );
q_reg_i_322: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCDCFCDCFCCCFCD"
    )
        port map (
      I0 => pre_reg_cnt(161),
      I1 => q_reg_i_468_n_0,
      I2 => pre_reg_cnt(163),
      I3 => pre_reg_cnt(162),
      I4 => pre_reg_cnt(159),
      I5 => q_reg_i_298_n_0,
      O => q_reg_i_322_n_0
    );
q_reg_i_323: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_315_n_0,
      I1 => q_reg_i_516_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_400_n_0,
      I4 => q_reg_i_401_n_0,
      I5 => q_reg_i_402_n_0,
      O => q_reg_i_323_n_0
    );
q_reg_i_324: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(272),
      I1 => pre_reg_cnt(273),
      I2 => q_reg_i_519_n_0,
      I3 => q_reg_i_520_n_0,
      I4 => q_reg_i_521_n_0,
      I5 => q_reg_i_522_n_0,
      O => q_reg_i_324_n_0
    );
q_reg_i_325: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_472_n_0,
      I1 => q_reg_i_523_n_0,
      I2 => pre_reg_cnt(264),
      I3 => pre_reg_cnt(265),
      I4 => q_reg_i_524_n_0,
      I5 => q_reg_i_525_n_0,
      O => q_reg_i_325_n_0
    );
q_reg_i_326: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFD"
    )
        port map (
      I0 => q_reg_i_526_n_0,
      I1 => q_reg_i_527_n_0,
      I2 => pre_reg_cnt(312),
      I3 => pre_reg_cnt(313),
      I4 => q_reg_i_528_n_0,
      I5 => q_reg_i_529_n_0,
      O => q_reg_i_326_n_0
    );
q_reg_i_327: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_504_n_0,
      I1 => pre_reg_cnt(291),
      I2 => pre_reg_cnt(290),
      I3 => pre_reg_cnt(289),
      I4 => pre_reg_cnt(288),
      O => q_reg_i_327_n_0
    );
q_reg_i_328: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_510_n_0,
      I1 => pre_reg_cnt(297),
      I2 => pre_reg_cnt(296),
      I3 => pre_reg_cnt(299),
      I4 => pre_reg_cnt(298),
      O => q_reg_i_328_n_0
    );
q_reg_i_329: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_530_n_0,
      I1 => q_reg_i_531_n_0,
      I2 => q_reg_i_479_n_0,
      I3 => q_reg_i_478_n_0,
      I4 => pre_reg_cnt(240),
      I5 => pre_reg_cnt(241),
      O => q_reg_i_329_n_0
    );
q_reg_i_33: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8AFF7500"
    )
        port map (
      I0 => q_reg_i_83_n_0,
      I1 => q_reg_i_84_n_0,
      I2 => q_reg_i_85_n_0,
      I3 => q_reg_i_86_n_0,
      I4 => q_reg_i_87_n_0,
      O => \^q_reg[109]_0\
    );
q_reg_i_330: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(222),
      I1 => pre_reg_cnt(223),
      I2 => pre_reg_cnt(220),
      I3 => pre_reg_cnt(221),
      O => q_reg_i_330_n_0
    );
q_reg_i_331: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_532_n_0,
      I1 => pre_reg_cnt(296),
      I2 => pre_reg_cnt(297),
      I3 => q_reg_i_510_n_0,
      I4 => q_reg_i_533_n_0,
      I5 => q_reg_i_504_n_0,
      O => q_reg_i_331_n_0
    );
q_reg_i_332: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_534_n_0,
      I1 => q_reg_i_535_n_0,
      I2 => pre_reg_cnt(224),
      I3 => pre_reg_cnt(225),
      I4 => q_reg_i_464_n_0,
      I5 => q_reg_i_429_n_0,
      O => q_reg_i_332_n_0
    );
q_reg_i_333: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF0000FF0D"
    )
        port map (
      I0 => pre_reg_cnt(267),
      I1 => pre_reg_cnt(268),
      I2 => pre_reg_cnt(269),
      I3 => pre_reg_cnt(270),
      I4 => pre_reg_cnt(271),
      I5 => q_reg_i_212_n_0,
      O => q_reg_i_333_n_0
    );
q_reg_i_334: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(277),
      I1 => pre_reg_cnt(276),
      I2 => pre_reg_cnt(279),
      I3 => pre_reg_cnt(278),
      I4 => q_reg_i_231_n_0,
      O => q_reg_i_334_n_0
    );
q_reg_i_335: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFFFFFEFFFE"
    )
        port map (
      I0 => pre_reg_cnt(282),
      I1 => pre_reg_cnt(283),
      I2 => q_reg_i_459_n_0,
      I3 => pre_reg_cnt(281),
      I4 => pre_reg_cnt(280),
      I5 => pre_reg_cnt(279),
      O => q_reg_i_335_n_0
    );
q_reg_i_336: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pre_reg_cnt(279),
      I1 => q_reg_i_231_n_0,
      O => q_reg_i_336_n_0
    );
q_reg_i_337: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFEE"
    )
        port map (
      I0 => q_reg_i_522_n_0,
      I1 => q_reg_i_338_n_0,
      I2 => pre_reg_cnt(283),
      I3 => pre_reg_cnt(282),
      O => q_reg_i_337_n_0
    );
q_reg_i_338: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_326_n_0,
      I1 => q_reg_i_504_n_0,
      I2 => q_reg_i_536_n_0,
      I3 => pre_reg_cnt(289),
      I4 => pre_reg_cnt(288),
      I5 => q_reg_i_328_n_0,
      O => q_reg_i_338_n_0
    );
q_reg_i_339: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFFFFFEFFFE"
    )
        port map (
      I0 => pre_reg_cnt(293),
      I1 => pre_reg_cnt(294),
      I2 => pre_reg_cnt(295),
      I3 => q_reg_i_284_n_0,
      I4 => pre_reg_cnt(292),
      I5 => pre_reg_cnt(291),
      O => q_reg_i_339_n_0
    );
q_reg_i_34: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BA4545BA"
    )
        port map (
      I0 => q_reg_i_88_n_0,
      I1 => q_reg_i_89_n_0,
      I2 => q_reg_i_72_n_0,
      I3 => q_reg_i_90_n_0,
      I4 => q_reg_i_91_n_0,
      O => \^q_reg[34]_0\
    );
q_reg_i_340: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000045"
    )
        port map (
      I0 => pre_reg_cnt(317),
      I1 => pre_reg_cnt(316),
      I2 => pre_reg_cnt(315),
      I3 => pre_reg_cnt(318),
      I4 => pre_reg_cnt(319),
      I5 => q_reg_i_537_n_0,
      O => q_reg_i_340_n_0
    );
q_reg_i_341: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEFFFF"
    )
        port map (
      I0 => pre_reg_cnt(313),
      I1 => pre_reg_cnt(312),
      I2 => pre_reg_cnt(315),
      I3 => pre_reg_cnt(314),
      I4 => q_reg_i_526_n_0,
      O => q_reg_i_341_n_0
    );
q_reg_i_342: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFFFF0D"
    )
        port map (
      I0 => pre_reg_cnt(303),
      I1 => q_reg_i_326_n_0,
      I2 => pre_reg_cnt(305),
      I3 => pre_reg_cnt(306),
      I4 => q_reg_i_275_n_0,
      I5 => pre_reg_cnt(307),
      O => q_reg_i_342_n_0
    );
q_reg_i_343: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(133),
      I1 => pre_reg_cnt(132),
      I2 => pre_reg_cnt(135),
      I3 => pre_reg_cnt(134),
      I4 => q_reg_i_241_n_0,
      O => q_reg_i_343_n_0
    );
q_reg_i_344: unisim.vcomponents.LUT5
    generic map(
      INIT => X"05050504"
    )
        port map (
      I0 => q_reg_i_200_n_0,
      I1 => pre_reg_cnt(126),
      I2 => pre_reg_cnt(127),
      I3 => pre_reg_cnt(124),
      I4 => pre_reg_cnt(125),
      O => q_reg_i_344_n_0
    );
q_reg_i_345: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FF00FF00FF0001"
    )
        port map (
      I0 => pre_reg_cnt(108),
      I1 => pre_reg_cnt(107),
      I2 => pre_reg_cnt(106),
      I3 => q_reg_i_229_n_0,
      I4 => pre_reg_cnt(109),
      I5 => q_reg_i_230_n_0,
      O => q_reg_i_345_n_0
    );
q_reg_i_346: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000111000001111"
    )
        port map (
      I0 => q_reg_i_474_n_0,
      I1 => q_reg_i_538_n_0,
      I2 => pre_reg_cnt(55),
      I3 => q_reg_i_237_n_0,
      I4 => q_reg_i_216_n_0,
      I5 => q_reg_i_389_n_0,
      O => q_reg_i_346_n_0
    );
q_reg_i_347: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF00FFFFFF0E"
    )
        port map (
      I0 => q_reg_i_539_n_0,
      I1 => pre_reg_cnt(72),
      I2 => pre_reg_cnt(73),
      I3 => q_reg_i_394_n_0,
      I4 => q_reg_i_393_n_0,
      I5 => q_reg_i_392_n_0,
      O => q_reg_i_347_n_0
    );
q_reg_i_348: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F0E0"
    )
        port map (
      I0 => pre_reg_cnt(290),
      I1 => pre_reg_cnt(291),
      I2 => q_reg_i_501_n_0,
      I3 => pre_reg_cnt(289),
      O => q_reg_i_348_n_0
    );
q_reg_i_349: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000F0000000E"
    )
        port map (
      I0 => pre_reg_cnt(271),
      I1 => q_reg_i_212_n_0,
      I2 => q_reg_i_334_n_0,
      I3 => pre_reg_cnt(275),
      I4 => pre_reg_cnt(274),
      I5 => q_reg_i_540_n_0,
      O => q_reg_i_349_n_0
    );
q_reg_i_35: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^q_reg[188]_0\,
      I1 => q_reg_i_92_n_0,
      I2 => q_reg_i_93_n_0,
      O => \^q_reg_i_93_0\
    );
q_reg_i_350: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0011001000110011"
    )
        port map (
      I0 => pre_reg_cnt(239),
      I1 => pre_reg_cnt(238),
      I2 => pre_reg_cnt(235),
      I3 => q_reg_i_125_n_0,
      I4 => q_reg_i_535_n_0,
      I5 => q_reg_i_541_n_0,
      O => q_reg_i_350_n_0
    );
q_reg_i_351: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(221),
      I1 => pre_reg_cnt(220),
      I2 => pre_reg_cnt(223),
      I3 => pre_reg_cnt(222),
      I4 => q_reg_i_132_n_0,
      O => q_reg_i_351_n_0
    );
q_reg_i_352: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => q_reg_i_326_n_0,
      I1 => pre_reg_cnt(302),
      I2 => pre_reg_cnt(303),
      O => q_reg_i_352_n_0
    );
q_reg_i_353: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFFAAFFAAFFFEFF"
    )
        port map (
      I0 => q_reg_i_489_n_0,
      I1 => pre_reg_cnt(312),
      I2 => pre_reg_cnt(313),
      I3 => q_reg_i_526_n_0,
      I4 => pre_reg_cnt(314),
      I5 => pre_reg_cnt(315),
      O => q_reg_i_353_n_0
    );
q_reg_i_354: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => pre_reg_cnt(7),
      I1 => pre_reg_cnt(6),
      O => q_reg_i_354_n_0
    );
q_reg_i_355: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_462_n_0,
      I1 => pre_reg_cnt(9),
      I2 => pre_reg_cnt(8),
      I3 => pre_reg_cnt(11),
      I4 => pre_reg_cnt(10),
      O => q_reg_i_355_n_0
    );
q_reg_i_356: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(12),
      I1 => pre_reg_cnt(13),
      O => q_reg_i_356_n_0
    );
q_reg_i_357: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(15),
      I1 => pre_reg_cnt(14),
      O => q_reg_i_357_n_0
    );
q_reg_i_358: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => q_reg_i_463_n_0,
      I1 => pre_reg_cnt(16),
      I2 => pre_reg_cnt(24),
      I3 => pre_reg_cnt(25),
      I4 => q_reg_i_378_n_0,
      I5 => q_reg_i_269_n_0,
      O => q_reg_i_358_n_0
    );
q_reg_i_359: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_290_n_0,
      I1 => q_reg_i_438_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_456_n_0,
      I4 => pre_reg_cnt(119),
      I5 => q_reg_i_542_n_0,
      O => q_reg_i_359_n_0
    );
q_reg_i_36: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => q_reg_i_94_n_0,
      I1 => q_reg_i_95_n_0,
      I2 => q_reg_i_96_n_0,
      O => \^q_reg_i_96_0\
    );
q_reg_i_360: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_420_n_0,
      I1 => q_reg_i_402_n_0,
      I2 => q_reg_i_410_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_438_n_0,
      I5 => q_reg_i_290_n_0,
      O => q_reg_i_360_n_0
    );
q_reg_i_361: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(114),
      I1 => pre_reg_cnt(115),
      O => q_reg_i_361_n_0
    );
q_reg_i_362: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FCFE"
    )
        port map (
      I0 => pre_reg_cnt(120),
      I1 => pre_reg_cnt(123),
      I2 => pre_reg_cnt(122),
      I3 => pre_reg_cnt(121),
      O => q_reg_i_362_n_0
    );
q_reg_i_363: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(126),
      I1 => pre_reg_cnt(127),
      I2 => pre_reg_cnt(124),
      I3 => pre_reg_cnt(125),
      O => q_reg_i_363_n_0
    );
q_reg_i_364: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_457_n_0,
      I1 => q_reg_i_393_n_0,
      I2 => pre_reg_cnt(69),
      I3 => pre_reg_cnt(68),
      I4 => pre_reg_cnt(71),
      I5 => pre_reg_cnt(70),
      O => q_reg_i_364_n_0
    );
q_reg_i_365: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(66),
      I1 => pre_reg_cnt(67),
      O => q_reg_i_365_n_0
    );
q_reg_i_366: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(60),
      I1 => pre_reg_cnt(61),
      O => q_reg_i_366_n_0
    );
q_reg_i_367: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => q_reg_i_216_n_0,
      I1 => pre_reg_cnt(62),
      I2 => pre_reg_cnt(63),
      O => q_reg_i_367_n_0
    );
q_reg_i_368: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFEFE"
    )
        port map (
      I0 => pre_reg_cnt(7),
      I1 => pre_reg_cnt(6),
      I2 => pre_reg_cnt(5),
      I3 => pre_reg_cnt(4),
      I4 => pre_reg_cnt(3),
      O => q_reg_i_368_n_0
    );
q_reg_i_369: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(318),
      I1 => pre_reg_cnt(319),
      O => q_reg_i_369_n_0
    );
q_reg_i_37: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55FF55FD00FF0000"
    )
        port map (
      I0 => q_reg_i_76_n_0,
      I1 => q_reg_i_97_n_0,
      I2 => q_reg_i_98_n_0,
      I3 => q_reg_i_99_n_0,
      I4 => q_reg_i_100_n_0,
      I5 => q_reg_i_101_n_0,
      O => \^q_reg[43]_0\
    );
q_reg_i_370: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(0),
      I1 => pre_reg_cnt(1),
      O => q_reg_i_370_n_0
    );
q_reg_i_371: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pre_reg_cnt(3),
      I1 => pre_reg_cnt(2),
      O => q_reg_i_371_n_0
    );
q_reg_i_372: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(4),
      I1 => pre_reg_cnt(7),
      I2 => pre_reg_cnt(6),
      I3 => pre_reg_cnt(5),
      O => q_reg_i_372_n_0
    );
q_reg_i_373: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(22),
      I1 => pre_reg_cnt(23),
      I2 => pre_reg_cnt(20),
      I3 => pre_reg_cnt(21),
      O => q_reg_i_373_n_0
    );
q_reg_i_374: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000010001"
    )
        port map (
      I0 => pre_reg_cnt(18),
      I1 => pre_reg_cnt(17),
      I2 => pre_reg_cnt(19),
      I3 => q_reg_i_373_n_0,
      I4 => pre_reg_cnt(16),
      I5 => pre_reg_cnt(15),
      O => q_reg_i_374_n_0
    );
q_reg_i_375: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => pre_reg_cnt(23),
      I1 => pre_reg_cnt(22),
      I2 => pre_reg_cnt(21),
      O => q_reg_i_375_n_0
    );
q_reg_i_376: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_502_n_0,
      I1 => q_reg_i_456_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_467_n_0,
      I4 => q_reg_i_438_n_0,
      I5 => q_reg_i_543_n_0,
      O => q_reg_i_376_n_0
    );
q_reg_i_377: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(24),
      I1 => pre_reg_cnt(25),
      O => q_reg_i_377_n_0
    );
q_reg_i_378: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(31),
      I1 => pre_reg_cnt(28),
      I2 => pre_reg_cnt(30),
      I3 => pre_reg_cnt(29),
      I4 => pre_reg_cnt(26),
      I5 => pre_reg_cnt(27),
      O => q_reg_i_378_n_0
    );
q_reg_i_379: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(27),
      I1 => pre_reg_cnt(28),
      O => q_reg_i_379_n_0
    );
q_reg_i_38: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2D2D222D"
    )
        port map (
      I0 => \^q_reg[31]_0\,
      I1 => q_reg_i_102_n_0,
      I2 => q_reg_i_103_n_0,
      I3 => q_reg_i_104_n_0,
      I4 => q_reg_i_105_n_0,
      O => \^q_reg[295]_0\
    );
q_reg_i_380: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(30),
      I1 => pre_reg_cnt(29),
      O => q_reg_i_380_n_0
    );
q_reg_i_381: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(33),
      I1 => pre_reg_cnt(34),
      O => q_reg_i_381_n_0
    );
q_reg_i_382: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(39),
      I1 => pre_reg_cnt(38),
      O => q_reg_i_382_n_0
    );
q_reg_i_383: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_414_n_0,
      I1 => q_reg_i_476_n_0,
      I2 => q_reg_i_456_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_467_n_0,
      I5 => q_reg_i_438_n_0,
      O => q_reg_i_383_n_0
    );
q_reg_i_384: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(43),
      I1 => pre_reg_cnt(42),
      O => q_reg_i_384_n_0
    );
q_reg_i_385: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(46),
      I1 => pre_reg_cnt(47),
      I2 => pre_reg_cnt(44),
      I3 => pre_reg_cnt(45),
      O => q_reg_i_385_n_0
    );
q_reg_i_386: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => q_reg_i_438_n_0,
      I1 => q_reg_i_467_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_410_n_0,
      I4 => q_reg_i_402_n_0,
      I5 => q_reg_i_476_n_0,
      O => q_reg_i_386_n_0
    );
q_reg_i_387: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => pre_reg_cnt(50),
      I1 => pre_reg_cnt(49),
      O => q_reg_i_387_n_0
    );
q_reg_i_388: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(49),
      I1 => pre_reg_cnt(50),
      I2 => pre_reg_cnt(48),
      I3 => pre_reg_cnt(51),
      O => q_reg_i_388_n_0
    );
q_reg_i_389: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(53),
      I1 => pre_reg_cnt(54),
      I2 => pre_reg_cnt(52),
      I3 => pre_reg_cnt(55),
      O => q_reg_i_389_n_0
    );
q_reg_i_39: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000088F888F888F8"
    )
        port map (
      I0 => q_reg_i_106_n_0,
      I1 => q_reg_i_70_n_0,
      I2 => q_reg_i_107_n_0,
      I3 => q_reg_i_108_n_0,
      I4 => \^q_reg[188]_0\,
      I5 => q_reg_i_109_n_0,
      O => \^q_reg[55]_0\
    );
q_reg_i_390: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(42),
      I1 => pre_reg_cnt(43),
      O => q_reg_i_390_n_0
    );
q_reg_i_391: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => pre_reg_cnt(71),
      I1 => pre_reg_cnt(70),
      I2 => pre_reg_cnt(69),
      O => q_reg_i_391_n_0
    );
q_reg_i_392: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(75),
      I1 => pre_reg_cnt(74),
      O => q_reg_i_392_n_0
    );
q_reg_i_393: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_402_n_0,
      I1 => q_reg_i_410_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_438_n_0,
      I4 => q_reg_i_455_n_0,
      I5 => q_reg_i_395_n_0,
      O => q_reg_i_393_n_0
    );
q_reg_i_394: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(77),
      I1 => pre_reg_cnt(78),
      I2 => pre_reg_cnt(76),
      I3 => pre_reg_cnt(79),
      O => q_reg_i_394_n_0
    );
q_reg_i_395: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_544_n_0,
      I1 => q_reg_i_412_n_0,
      I2 => pre_reg_cnt(80),
      I3 => pre_reg_cnt(81),
      I4 => q_reg_i_484_n_0,
      I5 => q_reg_i_430_n_0,
      O => q_reg_i_395_n_0
    );
q_reg_i_396: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => pre_reg_cnt(53),
      I1 => pre_reg_cnt(54),
      I2 => pre_reg_cnt(52),
      O => q_reg_i_396_n_0
    );
q_reg_i_397: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_426_n_0,
      I1 => pre_reg_cnt(152),
      I2 => pre_reg_cnt(153),
      I3 => q_reg_i_450_n_0,
      I4 => q_reg_i_545_n_0,
      I5 => q_reg_i_409_n_0,
      O => q_reg_i_397_n_0
    );
q_reg_i_398: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_427_n_0,
      I1 => pre_reg_cnt(161),
      I2 => pre_reg_cnt(160),
      I3 => q_reg_i_546_n_0,
      I4 => q_reg_i_315_n_0,
      I5 => q_reg_i_516_n_0,
      O => q_reg_i_398_n_0
    );
q_reg_i_399: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_328_n_0,
      I1 => q_reg_i_327_n_0,
      I2 => q_reg_i_326_n_0,
      I3 => q_reg_i_232_n_0,
      I4 => q_reg_i_547_n_0,
      I5 => q_reg_i_324_n_0,
      O => q_reg_i_399_n_0
    );
q_reg_i_40: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFBA00"
    )
        port map (
      I0 => q_reg_i_110_n_0,
      I1 => q_reg_i_111_n_0,
      I2 => q_reg_i_112_n_0,
      I3 => q_reg_i_102_n_0,
      I4 => q_reg_i_113_n_0,
      I5 => q_reg_i_114_n_0,
      O => \^q_reg[132]_0\
    );
q_reg_i_400: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_548_n_0,
      I1 => pre_reg_cnt(216),
      I2 => pre_reg_cnt(217),
      I3 => q_reg_i_330_n_0,
      I4 => q_reg_i_549_n_0,
      I5 => q_reg_i_309_n_0,
      O => q_reg_i_400_n_0
    );
q_reg_i_401: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_308_n_0,
      I1 => pre_reg_cnt(194),
      I2 => pre_reg_cnt(195),
      I3 => pre_reg_cnt(192),
      I4 => pre_reg_cnt(193),
      I5 => q_reg_i_473_n_0,
      O => q_reg_i_401_n_0
    );
q_reg_i_402: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_429_n_0,
      I1 => q_reg_i_464_n_0,
      I2 => pre_reg_cnt(225),
      I3 => pre_reg_cnt(224),
      I4 => q_reg_i_541_n_0,
      I5 => q_reg_i_329_n_0,
      O => q_reg_i_402_n_0
    );
q_reg_i_403: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000D0D000D0"
    )
        port map (
      I0 => q_reg_i_80_n_0,
      I1 => q_reg_i_63_n_0,
      I2 => q_reg_i_79_n_0,
      I3 => q_reg_i_220_n_0,
      I4 => q_reg_i_219_n_0,
      I5 => q_reg_i_550_n_0,
      O => q_reg_i_403_n_0
    );
q_reg_i_404: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D0D000D000000000"
    )
        port map (
      I0 => q_reg_i_80_n_0,
      I1 => q_reg_i_63_n_0,
      I2 => q_reg_i_79_n_0,
      I3 => q_reg_i_225_n_0,
      I4 => q_reg_i_551_n_0,
      I5 => q_reg_i_221_n_0,
      O => q_reg_i_404_n_0
    );
q_reg_i_405: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2F2FD02FD0D0D0D0"
    )
        port map (
      I0 => q_reg_i_80_n_0,
      I1 => q_reg_i_63_n_0,
      I2 => q_reg_i_79_n_0,
      I3 => q_reg_i_225_n_0,
      I4 => q_reg_i_551_n_0,
      I5 => q_reg_i_221_n_0,
      O => q_reg_i_405_n_0
    );
q_reg_i_406: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2A2A2A2A2A2A2A00"
    )
        port map (
      I0 => q_reg_i_217_n_0,
      I1 => q_reg_i_241_n_0,
      I2 => q_reg_i_175_n_0,
      I3 => pre_reg_cnt(55),
      I4 => q_reg_i_237_n_0,
      I5 => q_reg_i_216_n_0,
      O => q_reg_i_406_n_0
    );
q_reg_i_407: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(109),
      I1 => q_reg_i_229_n_0,
      I2 => pre_reg_cnt(111),
      I3 => pre_reg_cnt(110),
      O => q_reg_i_407_n_0
    );
q_reg_i_408: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(163),
      I1 => q_reg_i_323_n_0,
      I2 => pre_reg_cnt(166),
      I3 => pre_reg_cnt(167),
      I4 => pre_reg_cnt(164),
      I5 => pre_reg_cnt(165),
      O => q_reg_i_408_n_0
    );
q_reg_i_409: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(149),
      I1 => pre_reg_cnt(150),
      I2 => pre_reg_cnt(148),
      I3 => pre_reg_cnt(151),
      O => q_reg_i_409_n_0
    );
q_reg_i_41: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBB0B00BBBBBBBB"
    )
        port map (
      I0 => q_reg_i_115_n_0,
      I1 => q_reg_i_116_n_0,
      I2 => q_reg_i_117_n_0,
      I3 => q_reg_i_118_n_0,
      I4 => q_reg_i_119_n_0,
      I5 => q_reg_i_120_n_0,
      O => \^q_reg[188]_0\
    );
q_reg_i_410: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_400_n_0,
      I1 => q_reg_i_473_n_0,
      I2 => pre_reg_cnt(193),
      I3 => pre_reg_cnt(192),
      I4 => q_reg_i_552_n_0,
      I5 => q_reg_i_308_n_0,
      O => q_reg_i_410_n_0
    );
q_reg_i_411: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(155),
      I1 => pre_reg_cnt(154),
      I2 => pre_reg_cnt(153),
      I3 => pre_reg_cnt(152),
      I4 => q_reg_i_426_n_0,
      O => q_reg_i_411_n_0
    );
q_reg_i_412: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(94),
      I1 => pre_reg_cnt(95),
      I2 => pre_reg_cnt(92),
      I3 => pre_reg_cnt(93),
      O => q_reg_i_412_n_0
    );
q_reg_i_413: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_412_n_0,
      I1 => pre_reg_cnt(89),
      I2 => pre_reg_cnt(88),
      I3 => pre_reg_cnt(91),
      I4 => pre_reg_cnt(90),
      O => q_reg_i_413_n_0
    );
q_reg_i_414: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_385_n_0,
      I1 => pre_reg_cnt(41),
      I2 => pre_reg_cnt(40),
      I3 => pre_reg_cnt(43),
      I4 => pre_reg_cnt(42),
      O => q_reg_i_414_n_0
    );
q_reg_i_415: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(35),
      I1 => pre_reg_cnt(37),
      I2 => pre_reg_cnt(36),
      I3 => pre_reg_cnt(39),
      I4 => pre_reg_cnt(38),
      O => q_reg_i_415_n_0
    );
q_reg_i_416: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pre_reg_cnt(170),
      I1 => pre_reg_cnt(169),
      O => q_reg_i_416_n_0
    );
q_reg_i_417: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => pre_reg_cnt(173),
      I1 => pre_reg_cnt(174),
      I2 => pre_reg_cnt(175),
      O => q_reg_i_417_n_0
    );
q_reg_i_418: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(119),
      I1 => pre_reg_cnt(118),
      O => q_reg_i_418_n_0
    );
q_reg_i_419: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_290_n_0,
      I1 => q_reg_i_438_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_400_n_0,
      I4 => q_reg_i_401_n_0,
      I5 => q_reg_i_402_n_0,
      O => q_reg_i_419_n_0
    );
q_reg_i_42: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00001011"
    )
        port map (
      I0 => q_reg_i_121_n_0,
      I1 => q_reg_i_122_n_0,
      I2 => pre_reg_cnt(184),
      I3 => pre_reg_cnt(183),
      I4 => q_reg_i_123_n_0,
      I5 => q_reg_i_124_n_0,
      O => \q_reg[184]_0\
    );
q_reg_i_420: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(116),
      I1 => pre_reg_cnt(117),
      I2 => pre_reg_cnt(118),
      I3 => pre_reg_cnt(119),
      O => q_reg_i_420_n_0
    );
q_reg_i_421: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_363_n_0,
      I1 => q_reg_i_438_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_400_n_0,
      I4 => q_reg_i_401_n_0,
      I5 => q_reg_i_402_n_0,
      O => q_reg_i_421_n_0
    );
q_reg_i_422: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => pre_reg_cnt(121),
      I1 => pre_reg_cnt(122),
      I2 => pre_reg_cnt(123),
      O => q_reg_i_422_n_0
    );
q_reg_i_423: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_546_n_0,
      I1 => q_reg_i_315_n_0,
      I2 => q_reg_i_516_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_456_n_0,
      I5 => q_reg_i_427_n_0,
      O => q_reg_i_423_n_0
    );
q_reg_i_424: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => pre_reg_cnt(163),
      I1 => q_reg_i_315_n_0,
      I2 => q_reg_i_516_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_456_n_0,
      I5 => q_reg_i_427_n_0,
      O => q_reg_i_424_n_0
    );
q_reg_i_425: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_239_n_0,
      I1 => q_reg_i_397_n_0,
      I2 => q_reg_i_398_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_456_n_0,
      I5 => q_reg_i_164_n_0,
      O => q_reg_i_425_n_0
    );
q_reg_i_426: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(158),
      I1 => pre_reg_cnt(159),
      I2 => pre_reg_cnt(156),
      I3 => pre_reg_cnt(157),
      O => q_reg_i_426_n_0
    );
q_reg_i_427: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(166),
      I1 => pre_reg_cnt(167),
      I2 => pre_reg_cnt(164),
      I3 => pre_reg_cnt(165),
      O => q_reg_i_427_n_0
    );
q_reg_i_428: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(234),
      I1 => pre_reg_cnt(235),
      I2 => pre_reg_cnt(232),
      I3 => pre_reg_cnt(233),
      I4 => q_reg_i_535_n_0,
      I5 => q_reg_i_125_n_0,
      O => q_reg_i_428_n_0
    );
q_reg_i_429: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(228),
      I1 => pre_reg_cnt(229),
      I2 => pre_reg_cnt(230),
      I3 => pre_reg_cnt(231),
      O => q_reg_i_429_n_0
    );
q_reg_i_43: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001011"
    )
        port map (
      I0 => q_reg_i_125_n_0,
      I1 => pre_reg_cnt(239),
      I2 => pre_reg_cnt(238),
      I3 => pre_reg_cnt(237),
      I4 => q_reg_i_126_n_0,
      I5 => q_reg_i_127_n_0,
      O => \q_reg[239]_0\
    );
q_reg_i_430: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(86),
      I1 => pre_reg_cnt(87),
      I2 => pre_reg_cnt(84),
      I3 => pre_reg_cnt(85),
      O => q_reg_i_430_n_0
    );
q_reg_i_431: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(172),
      I1 => q_reg_i_402_n_0,
      I2 => q_reg_i_410_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_516_n_0,
      I5 => q_reg_i_417_n_0,
      O => q_reg_i_431_n_0
    );
q_reg_i_432: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(145),
      I1 => q_reg_i_409_n_0,
      I2 => q_reg_i_298_n_0,
      I3 => q_reg_i_411_n_0,
      I4 => pre_reg_cnt(147),
      I5 => pre_reg_cnt(146),
      O => q_reg_i_432_n_0
    );
q_reg_i_433: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(91),
      I1 => q_reg_i_305_n_0,
      I2 => pre_reg_cnt(94),
      I3 => pre_reg_cnt(95),
      I4 => pre_reg_cnt(92),
      I5 => pre_reg_cnt(93),
      O => q_reg_i_433_n_0
    );
q_reg_i_434: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(73),
      I1 => q_reg_i_394_n_0,
      I2 => q_reg_i_395_n_0,
      I3 => q_reg_i_305_n_0,
      I4 => pre_reg_cnt(75),
      I5 => pre_reg_cnt(74),
      O => q_reg_i_434_n_0
    );
q_reg_i_435: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_463_n_0,
      I1 => pre_reg_cnt(16),
      I2 => q_reg_i_376_n_0,
      I3 => q_reg_i_355_n_0,
      I4 => q_reg_i_553_n_0,
      I5 => pre_reg_cnt(4),
      O => q_reg_i_435_n_0
    );
q_reg_i_436: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(19),
      I1 => q_reg_i_376_n_0,
      I2 => pre_reg_cnt(22),
      I3 => pre_reg_cnt(23),
      I4 => pre_reg_cnt(20),
      I5 => pre_reg_cnt(21),
      O => q_reg_i_436_n_0
    );
q_reg_i_437: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000E00"
    )
        port map (
      I0 => q_reg_i_268_n_0,
      I1 => q_reg_i_269_n_0,
      I2 => q_reg_i_382_n_0,
      I3 => q_reg_i_386_n_0,
      I4 => q_reg_i_414_n_0,
      I5 => pre_reg_cnt(37),
      O => q_reg_i_437_n_0
    );
q_reg_i_438: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_516_n_0,
      I1 => q_reg_i_315_n_0,
      I2 => q_reg_i_554_n_0,
      I3 => q_reg_i_397_n_0,
      I4 => q_reg_i_452_n_0,
      I5 => q_reg_i_555_n_0,
      O => q_reg_i_438_n_0
    );
q_reg_i_439: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_402_n_0,
      I1 => q_reg_i_410_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_398_n_0,
      I4 => q_reg_i_426_n_0,
      I5 => q_reg_i_514_n_0,
      O => q_reg_i_439_n_0
    );
q_reg_i_44: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF5554"
    )
        port map (
      I0 => q_reg_i_128_n_0,
      I1 => q_reg_i_129_n_0,
      I2 => pre_reg_cnt(215),
      I3 => q_reg_i_130_n_0,
      I4 => q_reg_i_131_n_0,
      I5 => q_reg_i_132_n_0,
      O => \q_reg[215]_0\
    );
q_reg_i_440: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_402_n_0,
      I1 => q_reg_i_410_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_193_n_0,
      I4 => q_reg_i_233_n_0,
      I5 => q_reg_i_556_n_0,
      O => q_reg_i_440_n_0
    );
q_reg_i_441: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFFFFFEFFFE"
    )
        port map (
      I0 => q_reg_i_399_n_0,
      I1 => q_reg_i_410_n_0,
      I2 => q_reg_i_402_n_0,
      I3 => pre_reg_cnt(191),
      I4 => pre_reg_cnt(190),
      I5 => pre_reg_cnt(189),
      O => q_reg_i_441_n_0
    );
q_reg_i_442: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000000E"
    )
        port map (
      I0 => q_reg_i_473_n_0,
      I1 => pre_reg_cnt(199),
      I2 => q_reg_i_515_n_0,
      I3 => q_reg_i_132_n_0,
      I4 => q_reg_i_400_n_0,
      I5 => q_reg_i_557_n_0,
      O => q_reg_i_442_n_0
    );
q_reg_i_443: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => q_reg_i_308_n_0,
      I1 => q_reg_i_400_n_0,
      I2 => q_reg_i_132_n_0,
      I3 => q_reg_i_473_n_0,
      O => q_reg_i_443_n_0
    );
q_reg_i_444: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(211),
      I1 => q_reg_i_558_n_0,
      I2 => q_reg_i_132_n_0,
      I3 => q_reg_i_129_n_0,
      I4 => pre_reg_cnt(213),
      I5 => pre_reg_cnt(212),
      O => q_reg_i_444_n_0
    );
q_reg_i_445: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000FE"
    )
        port map (
      I0 => pre_reg_cnt(206),
      I1 => pre_reg_cnt(207),
      I2 => pre_reg_cnt(205),
      I3 => q_reg_i_132_n_0,
      I4 => q_reg_i_400_n_0,
      O => q_reg_i_445_n_0
    );
q_reg_i_446: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000000E"
    )
        port map (
      I0 => q_reg_i_320_n_0,
      I1 => pre_reg_cnt(187),
      I2 => q_reg_i_235_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_410_n_0,
      I5 => q_reg_i_402_n_0,
      O => q_reg_i_446_n_0
    );
q_reg_i_447: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(193),
      I1 => q_reg_i_308_n_0,
      I2 => q_reg_i_400_n_0,
      I3 => q_reg_i_132_n_0,
      I4 => q_reg_i_473_n_0,
      I5 => q_reg_i_552_n_0,
      O => q_reg_i_447_n_0
    );
q_reg_i_448: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000FD"
    )
        port map (
      I0 => q_reg_i_191_n_0,
      I1 => pre_reg_cnt(181),
      I2 => q_reg_i_233_n_0,
      I3 => q_reg_i_193_n_0,
      I4 => q_reg_i_399_n_0,
      I5 => q_reg_i_456_n_0,
      O => q_reg_i_448_n_0
    );
q_reg_i_449: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(151),
      I1 => q_reg_i_402_n_0,
      I2 => q_reg_i_410_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_398_n_0,
      I5 => q_reg_i_411_n_0,
      O => q_reg_i_449_n_0
    );
q_reg_i_45: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4445FFFF44454445"
    )
        port map (
      I0 => q_reg_i_109_n_0,
      I1 => q_reg_i_133_n_0,
      I2 => q_reg_i_134_n_0,
      I3 => q_reg_i_135_n_0,
      I4 => q_reg_i_104_n_0,
      I5 => q_reg_i_136_n_0,
      O => \q_reg[247]_0\
    );
q_reg_i_450: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(155),
      I1 => pre_reg_cnt(154),
      O => q_reg_i_450_n_0
    );
q_reg_i_451: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(159),
      I1 => pre_reg_cnt(158),
      O => q_reg_i_451_n_0
    );
q_reg_i_452: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(137),
      I1 => pre_reg_cnt(136),
      I2 => pre_reg_cnt(139),
      I3 => pre_reg_cnt(138),
      I4 => q_reg_i_164_n_0,
      O => q_reg_i_452_n_0
    );
q_reg_i_453: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(135),
      I1 => pre_reg_cnt(134),
      O => q_reg_i_453_n_0
    );
q_reg_i_454: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(150),
      I1 => pre_reg_cnt(149),
      O => q_reg_i_454_n_0
    );
q_reg_i_455: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_470_n_0,
      I1 => q_reg_i_226_n_0,
      I2 => q_reg_i_227_n_0,
      I3 => pre_reg_cnt(97),
      I4 => pre_reg_cnt(96),
      I5 => q_reg_i_559_n_0,
      O => q_reg_i_455_n_0
    );
q_reg_i_456: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_329_n_0,
      I1 => q_reg_i_541_n_0,
      I2 => q_reg_i_560_n_0,
      I3 => q_reg_i_561_n_0,
      I4 => q_reg_i_473_n_0,
      I5 => q_reg_i_400_n_0,
      O => q_reg_i_456_n_0
    );
q_reg_i_457: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(73),
      I1 => pre_reg_cnt(72),
      I2 => pre_reg_cnt(75),
      I3 => pre_reg_cnt(74),
      I4 => q_reg_i_394_n_0,
      O => q_reg_i_457_n_0
    );
q_reg_i_458: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_395_n_0,
      I1 => q_reg_i_214_n_0,
      I2 => pre_reg_cnt(65),
      I3 => pre_reg_cnt(64),
      I4 => q_reg_i_511_n_0,
      I5 => q_reg_i_457_n_0,
      O => q_reg_i_458_n_0
    );
q_reg_i_459: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => q_reg_i_328_n_0,
      I1 => q_reg_i_327_n_0,
      I2 => q_reg_i_326_n_0,
      I3 => q_reg_i_522_n_0,
      O => q_reg_i_459_n_0
    );
q_reg_i_46: unisim.vcomponents.LUT5
    generic map(
      INIT => X"007171FF"
    )
        port map (
      I0 => \^q_reg[55]_0\,
      I1 => \^q_reg[295]_0\,
      I2 => \^q_reg[43]_0\,
      I3 => q_reg_i_49_n_0,
      I4 => q_reg_i_48_n_0,
      O => q_reg_i_46_n_0
    );
q_reg_i_460: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_326_n_0,
      I1 => pre_reg_cnt(303),
      I2 => pre_reg_cnt(302),
      I3 => pre_reg_cnt(301),
      I4 => pre_reg_cnt(300),
      O => q_reg_i_460_n_0
    );
q_reg_i_461: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(247),
      I1 => pre_reg_cnt(246),
      I2 => pre_reg_cnt(245),
      I3 => pre_reg_cnt(244),
      I4 => q_reg_i_480_n_0,
      I5 => q_reg_i_399_n_0,
      O => q_reg_i_461_n_0
    );
q_reg_i_462: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(14),
      I1 => pre_reg_cnt(15),
      I2 => pre_reg_cnt(12),
      I3 => pre_reg_cnt(13),
      O => q_reg_i_462_n_0
    );
q_reg_i_463: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => q_reg_i_373_n_0,
      I1 => pre_reg_cnt(19),
      I2 => pre_reg_cnt(17),
      I3 => pre_reg_cnt(18),
      O => q_reg_i_463_n_0
    );
q_reg_i_464: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(227),
      I1 => pre_reg_cnt(226),
      O => q_reg_i_464_n_0
    );
q_reg_i_465: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(231),
      I1 => pre_reg_cnt(230),
      O => q_reg_i_465_n_0
    );
q_reg_i_466: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(223),
      I1 => q_reg_i_132_n_0,
      O => q_reg_i_466_n_0
    );
q_reg_i_467: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_562_n_0,
      I1 => q_reg_i_226_n_0,
      I2 => q_reg_i_470_n_0,
      I3 => q_reg_i_457_n_0,
      I4 => q_reg_i_563_n_0,
      I5 => q_reg_i_395_n_0,
      O => q_reg_i_467_n_0
    );
q_reg_i_468: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_427_n_0,
      I1 => q_reg_i_402_n_0,
      I2 => q_reg_i_410_n_0,
      I3 => q_reg_i_399_n_0,
      I4 => q_reg_i_516_n_0,
      I5 => q_reg_i_315_n_0,
      O => q_reg_i_468_n_0
    );
q_reg_i_469: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(108),
      I1 => pre_reg_cnt(109),
      I2 => pre_reg_cnt(110),
      I3 => pre_reg_cnt(111),
      O => q_reg_i_469_n_0
    );
q_reg_i_47: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => q_reg_i_78_n_0,
      I1 => q_reg_i_77_n_0,
      I2 => q_reg_i_76_n_0,
      O => q_reg_i_47_n_0
    );
q_reg_i_470: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_363_n_0,
      I1 => q_reg_i_564_n_0,
      I2 => q_reg_i_420_n_0,
      I3 => q_reg_i_565_n_0,
      I4 => pre_reg_cnt(112),
      I5 => pre_reg_cnt(113),
      O => q_reg_i_470_n_0
    );
q_reg_i_471: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_522_n_0,
      I1 => pre_reg_cnt(281),
      I2 => pre_reg_cnt(280),
      I3 => pre_reg_cnt(283),
      I4 => pre_reg_cnt(282),
      O => q_reg_i_471_n_0
    );
q_reg_i_472: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(268),
      I1 => pre_reg_cnt(269),
      I2 => pre_reg_cnt(270),
      I3 => pre_reg_cnt(271),
      O => q_reg_i_472_n_0
    );
q_reg_i_473: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_515_n_0,
      I1 => pre_reg_cnt(201),
      I2 => pre_reg_cnt(200),
      I3 => pre_reg_cnt(203),
      I4 => pre_reg_cnt(202),
      O => q_reg_i_473_n_0
    );
q_reg_i_474: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(62),
      I1 => pre_reg_cnt(63),
      I2 => pre_reg_cnt(60),
      I3 => pre_reg_cnt(61),
      O => q_reg_i_474_n_0
    );
q_reg_i_475: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(54),
      I1 => pre_reg_cnt(55),
      O => q_reg_i_475_n_0
    );
q_reg_i_476: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_474_n_0,
      I1 => q_reg_i_538_n_0,
      I2 => pre_reg_cnt(56),
      I3 => pre_reg_cnt(57),
      I4 => q_reg_i_388_n_0,
      I5 => q_reg_i_389_n_0,
      O => q_reg_i_476_n_0
    );
q_reg_i_477: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(38),
      I1 => pre_reg_cnt(39),
      I2 => pre_reg_cnt(36),
      I3 => pre_reg_cnt(37),
      O => q_reg_i_477_n_0
    );
q_reg_i_478: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(243),
      I1 => pre_reg_cnt(242),
      O => q_reg_i_478_n_0
    );
q_reg_i_479: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(244),
      I1 => pre_reg_cnt(245),
      I2 => pre_reg_cnt(246),
      I3 => pre_reg_cnt(247),
      O => q_reg_i_479_n_0
    );
q_reg_i_48: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \^q_reg[131]_0\,
      I1 => \^q_reg[132]_0\,
      I2 => \^q_reg[295]_1\,
      O => q_reg_i_48_n_0
    );
q_reg_i_480: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(249),
      I1 => pre_reg_cnt(248),
      I2 => pre_reg_cnt(251),
      I3 => pre_reg_cnt(250),
      I4 => q_reg_i_530_n_0,
      O => q_reg_i_480_n_0
    );
q_reg_i_481: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(255),
      I1 => pre_reg_cnt(254),
      O => q_reg_i_481_n_0
    );
q_reg_i_482: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_232_n_0,
      I1 => q_reg_i_324_n_0,
      I2 => q_reg_i_326_n_0,
      I3 => q_reg_i_327_n_0,
      I4 => q_reg_i_328_n_0,
      O => q_reg_i_482_n_0
    );
q_reg_i_483: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(16),
      I1 => pre_reg_cnt(18),
      I2 => pre_reg_cnt(17),
      I3 => pre_reg_cnt(19),
      I4 => q_reg_i_373_n_0,
      O => q_reg_i_483_n_0
    );
q_reg_i_484: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(83),
      I1 => pre_reg_cnt(82),
      O => q_reg_i_484_n_0
    );
q_reg_i_485: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(87),
      I1 => pre_reg_cnt(86),
      O => q_reg_i_485_n_0
    );
q_reg_i_486: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pre_reg_cnt(23),
      I1 => pre_reg_cnt(22),
      O => q_reg_i_486_n_0
    );
q_reg_i_487: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(226),
      I1 => pre_reg_cnt(227),
      I2 => q_reg_i_541_n_0,
      I3 => q_reg_i_125_n_0,
      I4 => q_reg_i_429_n_0,
      O => q_reg_i_487_n_0
    );
q_reg_i_488: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(230),
      I1 => pre_reg_cnt(231),
      I2 => q_reg_i_125_n_0,
      I3 => q_reg_i_541_n_0,
      O => q_reg_i_488_n_0
    );
q_reg_i_489: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCC8FFFF"
    )
        port map (
      I0 => pre_reg_cnt(307),
      I1 => q_reg_i_566_n_0,
      I2 => pre_reg_cnt(309),
      I3 => pre_reg_cnt(308),
      I4 => q_reg_i_326_n_0,
      O => q_reg_i_489_n_0
    );
q_reg_i_49: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0444"
    )
        port map (
      I0 => q_reg_i_103_n_0,
      I1 => \^q_reg[31]_0\,
      I2 => q_reg_i_47_n_0,
      I3 => q_reg_i_105_n_0,
      O => q_reg_i_49_n_0
    );
q_reg_i_490: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFEE"
    )
        port map (
      I0 => pre_reg_cnt(89),
      I1 => pre_reg_cnt(90),
      I2 => pre_reg_cnt(88),
      I3 => pre_reg_cnt(87),
      O => q_reg_i_490_n_0
    );
q_reg_i_491: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => pre_reg_cnt(95),
      I1 => pre_reg_cnt(94),
      I2 => pre_reg_cnt(93),
      O => q_reg_i_491_n_0
    );
q_reg_i_492: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(255),
      I1 => pre_reg_cnt(254),
      I2 => q_reg_i_399_n_0,
      I3 => pre_reg_cnt(253),
      I4 => pre_reg_cnt(252),
      I5 => pre_reg_cnt(251),
      O => q_reg_i_492_n_0
    );
q_reg_i_493: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(249),
      I1 => pre_reg_cnt(250),
      O => q_reg_i_493_n_0
    );
q_reg_i_494: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(255),
      I1 => q_reg_i_399_n_0,
      O => q_reg_i_494_n_0
    );
q_reg_i_495: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => pre_reg_cnt(261),
      I1 => pre_reg_cnt(260),
      I2 => pre_reg_cnt(263),
      I3 => pre_reg_cnt(262),
      I4 => q_reg_i_482_n_0,
      O => q_reg_i_495_n_0
    );
q_reg_i_496: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => pre_reg_cnt(257),
      I1 => pre_reg_cnt(258),
      I2 => pre_reg_cnt(259),
      O => q_reg_i_496_n_0
    );
q_reg_i_497: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(37),
      I1 => q_reg_i_414_n_0,
      I2 => q_reg_i_476_n_0,
      I3 => q_reg_i_216_n_0,
      I4 => pre_reg_cnt(39),
      I5 => pre_reg_cnt(38),
      O => q_reg_i_497_n_0
    );
q_reg_i_498: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => q_reg_i_399_n_0,
      I1 => pre_reg_cnt(254),
      I2 => pre_reg_cnt(255),
      O => q_reg_i_498_n_0
    );
q_reg_i_499: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(235),
      I1 => q_reg_i_125_n_0,
      I2 => pre_reg_cnt(236),
      I3 => pre_reg_cnt(237),
      I4 => pre_reg_cnt(238),
      I5 => pre_reg_cnt(239),
      O => q_reg_i_499_n_0
    );
q_reg_i_50: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAEAEAEAEAEAE00"
    )
        port map (
      I0 => q_reg_i_137_n_0,
      I1 => q_reg_i_138_n_0,
      I2 => q_reg_i_139_n_0,
      I3 => q_reg_i_77_n_0,
      I4 => q_reg_i_140_n_0,
      I5 => q_reg_i_141_n_0,
      O => \^q_reg[131]_0\
    );
q_reg_i_500: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => pre_reg_cnt(307),
      I1 => q_reg_i_341_n_0,
      I2 => pre_reg_cnt(310),
      I3 => pre_reg_cnt(311),
      I4 => pre_reg_cnt(309),
      I5 => pre_reg_cnt(308),
      O => q_reg_i_500_n_0
    );
q_reg_i_501: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => pre_reg_cnt(295),
      I1 => pre_reg_cnt(294),
      I2 => pre_reg_cnt(293),
      I3 => pre_reg_cnt(292),
      I4 => q_reg_i_284_n_0,
      O => q_reg_i_501_n_0
    );
q_reg_i_502: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => q_reg_i_476_n_0,
      I1 => q_reg_i_567_n_0,
      I2 => q_reg_i_414_n_0,
      O => q_reg_i_502_n_0
    );
q_reg_i_503: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(11),
      I1 => pre_reg_cnt(10),
      O => q_reg_i_503_n_0
    );
q_reg_i_504: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(292),
      I1 => pre_reg_cnt(293),
      I2 => pre_reg_cnt(294),
      I3 => pre_reg_cnt(295),
      O => q_reg_i_504_n_0
    );
q_reg_i_505: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0051"
    )
        port map (
      I0 => q_reg_i_482_n_0,
      I1 => pre_reg_cnt(261),
      I2 => pre_reg_cnt(262),
      I3 => pre_reg_cnt(263),
      O => q_reg_i_505_n_0
    );
q_reg_i_506: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFFFEFE"
    )
        port map (
      I0 => pre_reg_cnt(115),
      I1 => pre_reg_cnt(114),
      I2 => pre_reg_cnt(113),
      I3 => pre_reg_cnt(112),
      I4 => pre_reg_cnt(111),
      O => q_reg_i_506_n_0
    );
q_reg_i_507: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => pre_reg_cnt(107),
      I1 => pre_reg_cnt(106),
      I2 => pre_reg_cnt(105),
      O => q_reg_i_507_n_0
    );
q_reg_i_508: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFF0D"
    )
        port map (
      I0 => q_reg_i_568_n_0,
      I1 => pre_reg_cnt(126),
      I2 => pre_reg_cnt(127),
      I3 => q_reg_i_456_n_0,
      I4 => q_reg_i_399_n_0,
      I5 => q_reg_i_438_n_0,
      O => q_reg_i_508_n_0
    );
q_reg_i_509: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(134),
      I1 => pre_reg_cnt(135),
      I2 => pre_reg_cnt(132),
      I3 => pre_reg_cnt(133),
      O => q_reg_i_509_n_0
    );
q_reg_i_51: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F2F200F200F200F2"
    )
        port map (
      I0 => q_reg_i_142_n_0,
      I1 => q_reg_i_88_n_0,
      I2 => q_reg_i_143_n_0,
      I3 => q_reg_i_78_n_0,
      I4 => q_reg_i_144_n_0,
      I5 => q_reg_i_145_n_0,
      O => \^q_reg[295]_1\
    );
q_reg_i_510: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(300),
      I1 => pre_reg_cnt(301),
      I2 => pre_reg_cnt(302),
      I3 => pre_reg_cnt(303),
      O => q_reg_i_510_n_0
    );
q_reg_i_511: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(67),
      I1 => pre_reg_cnt(66),
      O => q_reg_i_511_n_0
    );
q_reg_i_512: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(108),
      I1 => pre_reg_cnt(109),
      O => q_reg_i_512_n_0
    );
q_reg_i_513: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFCFFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(264),
      I1 => pre_reg_cnt(266),
      I2 => pre_reg_cnt(267),
      I3 => q_reg_i_212_n_0,
      I4 => q_reg_i_472_n_0,
      I5 => pre_reg_cnt(265),
      O => q_reg_i_513_n_0
    );
q_reg_i_514: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => pre_reg_cnt(155),
      I1 => pre_reg_cnt(154),
      I2 => pre_reg_cnt(153),
      O => q_reg_i_514_n_0
    );
q_reg_i_515: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(204),
      I1 => pre_reg_cnt(205),
      I2 => pre_reg_cnt(206),
      I3 => pre_reg_cnt(207),
      O => q_reg_i_515_n_0
    );
q_reg_i_516: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_569_n_0,
      I1 => q_reg_i_320_n_0,
      I2 => q_reg_i_570_n_0,
      I3 => q_reg_i_233_n_0,
      I4 => pre_reg_cnt(180),
      I5 => pre_reg_cnt(181),
      O => q_reg_i_516_n_0
    );
q_reg_i_517: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(171),
      I1 => pre_reg_cnt(172),
      O => q_reg_i_517_n_0
    );
q_reg_i_518: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(174),
      I1 => pre_reg_cnt(175),
      I2 => pre_reg_cnt(172),
      I3 => pre_reg_cnt(173),
      O => q_reg_i_518_n_0
    );
q_reg_i_519: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(275),
      I1 => pre_reg_cnt(274),
      O => q_reg_i_519_n_0
    );
q_reg_i_52: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => q_reg_i_146_n_0,
      I1 => q_reg_i_143_n_0,
      I2 => q_reg_i_147_n_0,
      O => \q_reg[19]_0\
    );
q_reg_i_520: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(278),
      I1 => pre_reg_cnt(279),
      I2 => pre_reg_cnt(276),
      I3 => pre_reg_cnt(277),
      O => q_reg_i_520_n_0
    );
q_reg_i_521: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(282),
      I1 => pre_reg_cnt(283),
      I2 => pre_reg_cnt(280),
      I3 => pre_reg_cnt(281),
      O => q_reg_i_521_n_0
    );
q_reg_i_522: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(286),
      I1 => pre_reg_cnt(287),
      I2 => pre_reg_cnt(284),
      I3 => pre_reg_cnt(285),
      O => q_reg_i_522_n_0
    );
q_reg_i_523: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(267),
      I1 => pre_reg_cnt(266),
      O => q_reg_i_523_n_0
    );
q_reg_i_524: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(258),
      I1 => pre_reg_cnt(259),
      I2 => pre_reg_cnt(256),
      I3 => pre_reg_cnt(257),
      O => q_reg_i_524_n_0
    );
q_reg_i_525: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(262),
      I1 => pre_reg_cnt(263),
      I2 => pre_reg_cnt(260),
      I3 => pre_reg_cnt(261),
      O => q_reg_i_525_n_0
    );
q_reg_i_526: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => pre_reg_cnt(319),
      I1 => pre_reg_cnt(318),
      I2 => pre_reg_cnt(316),
      I3 => pre_reg_cnt(317),
      O => q_reg_i_526_n_0
    );
q_reg_i_527: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(315),
      I1 => pre_reg_cnt(314),
      O => q_reg_i_527_n_0
    );
q_reg_i_528: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(308),
      I1 => pre_reg_cnt(309),
      I2 => pre_reg_cnt(305),
      I3 => pre_reg_cnt(306),
      O => q_reg_i_528_n_0
    );
q_reg_i_529: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(310),
      I1 => pre_reg_cnt(311),
      I2 => pre_reg_cnt(304),
      I3 => pre_reg_cnt(307),
      O => q_reg_i_529_n_0
    );
q_reg_i_53: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEEEEEE000E"
    )
        port map (
      I0 => q_reg_i_148_n_0,
      I1 => q_reg_i_149_n_0,
      I2 => q_reg_i_150_n_0,
      I3 => q_reg_i_151_n_0,
      I4 => q_reg_i_152_n_0,
      I5 => q_reg_i_153_n_0,
      O => \^q_reg[31]_0\
    );
q_reg_i_530: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(254),
      I1 => pre_reg_cnt(255),
      I2 => pre_reg_cnt(252),
      I3 => pre_reg_cnt(253),
      O => q_reg_i_530_n_0
    );
q_reg_i_531: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(250),
      I1 => pre_reg_cnt(251),
      I2 => pre_reg_cnt(248),
      I3 => pre_reg_cnt(249),
      O => q_reg_i_531_n_0
    );
q_reg_i_532: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(299),
      I1 => pre_reg_cnt(298),
      O => q_reg_i_532_n_0
    );
q_reg_i_533: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(288),
      I1 => pre_reg_cnt(289),
      I2 => pre_reg_cnt(290),
      I3 => pre_reg_cnt(291),
      O => q_reg_i_533_n_0
    );
q_reg_i_534: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(234),
      I1 => pre_reg_cnt(235),
      I2 => pre_reg_cnt(232),
      I3 => pre_reg_cnt(233),
      O => q_reg_i_534_n_0
    );
q_reg_i_535: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(236),
      I1 => pre_reg_cnt(237),
      I2 => pre_reg_cnt(238),
      I3 => pre_reg_cnt(239),
      O => q_reg_i_535_n_0
    );
q_reg_i_536: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(291),
      I1 => pre_reg_cnt(290),
      O => q_reg_i_536_n_0
    );
q_reg_i_537: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => q_reg_i_259_n_0,
      I1 => pre_reg_cnt(313),
      I2 => pre_reg_cnt(312),
      O => q_reg_i_537_n_0
    );
q_reg_i_538: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(59),
      I1 => pre_reg_cnt(58),
      O => q_reg_i_538_n_0
    );
q_reg_i_539: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(71),
      I1 => pre_reg_cnt(70),
      O => q_reg_i_539_n_0
    );
q_reg_i_54: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000002000202"
    )
        port map (
      I0 => q_reg_i_154_n_0,
      I1 => pre_reg_cnt(77),
      I2 => pre_reg_cnt(78),
      I3 => pre_reg_cnt(76),
      I4 => pre_reg_cnt(75),
      I5 => q_reg_i_155_n_0,
      O => \q_reg[77]_0\
    );
q_reg_i_540: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => pre_reg_cnt(271),
      I1 => pre_reg_cnt(270),
      I2 => pre_reg_cnt(269),
      I3 => pre_reg_cnt(268),
      I4 => q_reg_i_212_n_0,
      O => q_reg_i_540_n_0
    );
q_reg_i_541: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_535_n_0,
      I1 => pre_reg_cnt(233),
      I2 => pre_reg_cnt(232),
      I3 => pre_reg_cnt(235),
      I4 => pre_reg_cnt(234),
      O => q_reg_i_541_n_0
    );
q_reg_i_542: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(117),
      I1 => pre_reg_cnt(118),
      O => q_reg_i_542_n_0
    );
q_reg_i_543: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => pre_reg_cnt(24),
      I1 => pre_reg_cnt(25),
      I2 => q_reg_i_378_n_0,
      O => q_reg_i_543_n_0
    );
q_reg_i_544: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(90),
      I1 => pre_reg_cnt(91),
      I2 => pre_reg_cnt(88),
      I3 => pre_reg_cnt(89),
      O => q_reg_i_544_n_0
    );
q_reg_i_545: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(144),
      I1 => pre_reg_cnt(145),
      I2 => pre_reg_cnt(146),
      I3 => pre_reg_cnt(147),
      O => q_reg_i_545_n_0
    );
q_reg_i_546: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(163),
      I1 => pre_reg_cnt(162),
      O => q_reg_i_546_n_0
    );
q_reg_i_547: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_525_n_0,
      I1 => pre_reg_cnt(257),
      I2 => pre_reg_cnt(256),
      I3 => pre_reg_cnt(259),
      I4 => pre_reg_cnt(258),
      O => q_reg_i_547_n_0
    );
q_reg_i_548: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(219),
      I1 => pre_reg_cnt(218),
      O => q_reg_i_548_n_0
    );
q_reg_i_549: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(214),
      I1 => pre_reg_cnt(215),
      I2 => pre_reg_cnt(208),
      I3 => pre_reg_cnt(209),
      O => q_reg_i_549_n_0
    );
q_reg_i_55: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFFFFFAE"
    )
        port map (
      I0 => q_reg_i_156_n_0,
      I1 => pre_reg_cnt(57),
      I2 => pre_reg_cnt(58),
      I3 => pre_reg_cnt(59),
      I4 => q_reg_i_157_n_0,
      I5 => q_reg_i_158_n_0,
      O => \q_reg[57]_0\
    );
q_reg_i_550: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EE0E"
    )
        port map (
      I0 => pre_reg_cnt(271),
      I1 => q_reg_i_212_n_0,
      I2 => q_reg_i_218_n_0,
      I3 => q_reg_i_217_n_0,
      O => q_reg_i_550_n_0
    );
q_reg_i_551: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000101010101"
    )
        port map (
      I0 => q_reg_i_430_n_0,
      I1 => q_reg_i_571_n_0,
      I2 => q_reg_i_484_n_0,
      I3 => q_reg_i_269_n_0,
      I4 => q_reg_i_268_n_0,
      I5 => q_reg_i_224_n_0,
      O => q_reg_i_551_n_0
    );
q_reg_i_552: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(195),
      I1 => pre_reg_cnt(194),
      O => q_reg_i_552_n_0
    );
q_reg_i_553: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => pre_reg_cnt(5),
      I1 => pre_reg_cnt(6),
      I2 => pre_reg_cnt(7),
      O => q_reg_i_553_n_0
    );
q_reg_i_554: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_427_n_0,
      I1 => pre_reg_cnt(161),
      I2 => pre_reg_cnt(160),
      I3 => pre_reg_cnt(163),
      I4 => pre_reg_cnt(162),
      O => q_reg_i_554_n_0
    );
q_reg_i_555: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_509_n_0,
      I1 => pre_reg_cnt(131),
      I2 => pre_reg_cnt(130),
      I3 => pre_reg_cnt(129),
      I4 => pre_reg_cnt(128),
      O => q_reg_i_555_n_0
    );
q_reg_i_556: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pre_reg_cnt(180),
      I1 => pre_reg_cnt(181),
      O => q_reg_i_556_n_0
    );
q_reg_i_557: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(203),
      I1 => pre_reg_cnt(202),
      O => q_reg_i_557_n_0
    );
q_reg_i_558: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(215),
      I1 => pre_reg_cnt(214),
      O => q_reg_i_558_n_0
    );
q_reg_i_559: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(99),
      I1 => pre_reg_cnt(98),
      O => q_reg_i_559_n_0
    );
q_reg_i_56: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7DD71441"
    )
        port map (
      I0 => q_reg_i_159_n_0,
      I1 => q_reg_i_160_n_0,
      I2 => q_reg_i_161_n_0,
      I3 => q_reg_i_162_n_0,
      I4 => q_reg_i_163_n_0,
      O => \^q_reg_i_163_0\
    );
q_reg_i_560: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_429_n_0,
      I1 => pre_reg_cnt(227),
      I2 => pre_reg_cnt(226),
      I3 => pre_reg_cnt(225),
      I4 => pre_reg_cnt(224),
      O => q_reg_i_560_n_0
    );
q_reg_i_561: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(193),
      I1 => pre_reg_cnt(192),
      I2 => pre_reg_cnt(195),
      I3 => pre_reg_cnt(194),
      I4 => q_reg_i_308_n_0,
      O => q_reg_i_561_n_0
    );
q_reg_i_562: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_227_n_0,
      I1 => pre_reg_cnt(97),
      I2 => pre_reg_cnt(96),
      I3 => pre_reg_cnt(99),
      I4 => pre_reg_cnt(98),
      O => q_reg_i_562_n_0
    );
q_reg_i_563: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => q_reg_i_214_n_0,
      I1 => pre_reg_cnt(65),
      I2 => pre_reg_cnt(64),
      I3 => pre_reg_cnt(67),
      I4 => pre_reg_cnt(66),
      O => q_reg_i_563_n_0
    );
q_reg_i_564: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(122),
      I1 => pre_reg_cnt(123),
      I2 => pre_reg_cnt(120),
      I3 => pre_reg_cnt(121),
      O => q_reg_i_564_n_0
    );
q_reg_i_565: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pre_reg_cnt(115),
      I1 => pre_reg_cnt(114),
      O => q_reg_i_565_n_0
    );
q_reg_i_566: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => q_reg_i_341_n_0,
      I1 => pre_reg_cnt(310),
      I2 => pre_reg_cnt(311),
      O => q_reg_i_566_n_0
    );
q_reg_i_567: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => pre_reg_cnt(33),
      I1 => pre_reg_cnt(32),
      I2 => pre_reg_cnt(35),
      I3 => pre_reg_cnt(34),
      I4 => q_reg_i_477_n_0,
      O => q_reg_i_567_n_0
    );
q_reg_i_568: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFEE"
    )
        port map (
      I0 => pre_reg_cnt(125),
      I1 => pre_reg_cnt(126),
      I2 => pre_reg_cnt(124),
      I3 => pre_reg_cnt(123),
      O => q_reg_i_568_n_0
    );
q_reg_i_569: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(186),
      I1 => pre_reg_cnt(187),
      I2 => pre_reg_cnt(184),
      I3 => pre_reg_cnt(185),
      O => q_reg_i_569_n_0
    );
q_reg_i_57: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => q_reg_i_66_n_0,
      I1 => q_reg_i_65_n_0,
      I2 => q_reg_i_59_n_0,
      I3 => q_reg_i_64_n_0,
      O => \^q_reg_i_64_0\
    );
q_reg_i_570: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => pre_reg_cnt(178),
      I1 => pre_reg_cnt(179),
      I2 => pre_reg_cnt(176),
      I3 => pre_reg_cnt(177),
      O => q_reg_i_570_n_0
    );
q_reg_i_571: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_402_n_0,
      I1 => q_reg_i_410_n_0,
      I2 => q_reg_i_399_n_0,
      I3 => q_reg_i_438_n_0,
      I4 => q_reg_i_455_n_0,
      I5 => q_reg_i_413_n_0,
      O => q_reg_i_571_n_0
    );
q_reg_i_58: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFEFFFEFFFE"
    )
        port map (
      I0 => q_reg_i_164_n_0,
      I1 => q_reg_i_165_n_0,
      I2 => pre_reg_cnt(137),
      I3 => q_reg_i_166_n_0,
      I4 => q_reg_i_63_n_0,
      I5 => q_reg_i_62_n_0,
      O => \^q_reg[137]_0\
    );
q_reg_i_59: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32FF323200320000"
    )
        port map (
      I0 => q_reg_i_72_n_0,
      I1 => q_reg_i_71_n_0,
      I2 => q_reg_i_70_n_0,
      I3 => q_reg_i_67_n_0,
      I4 => q_reg_i_68_n_0,
      I5 => q_reg_i_69_n_0,
      O => q_reg_i_59_n_0
    );
q_reg_i_60: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2BD4D42B"
    )
        port map (
      I0 => q_reg_i_75_n_0,
      I1 => q_reg_i_74_n_0,
      I2 => q_reg_i_73_n_0,
      I3 => q_reg_i_159_n_0,
      I4 => q_reg_i_167_n_0,
      O => q_reg_i_60_n_0
    );
q_reg_i_61: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF696900"
    )
        port map (
      I0 => q_reg_i_75_n_0,
      I1 => q_reg_i_74_n_0,
      I2 => q_reg_i_73_n_0,
      I3 => q_reg_i_168_n_0,
      I4 => \^q_reg[91]_0\,
      O => q_reg_i_61_n_0
    );
q_reg_i_62: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000FDD5"
    )
        port map (
      I0 => q_reg_i_169_n_0,
      I1 => q_reg_i_72_n_0,
      I2 => q_reg_i_170_n_0,
      I3 => q_reg_i_171_n_0,
      I4 => q_reg_i_172_n_0,
      O => q_reg_i_62_n_0
    );
q_reg_i_63: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FE00FE00FE00FEFE"
    )
        port map (
      I0 => q_reg_i_165_n_0,
      I1 => pre_reg_cnt(143),
      I2 => pre_reg_cnt(142),
      I3 => q_reg_i_173_n_0,
      I4 => pre_reg_cnt(145),
      I5 => q_reg_i_174_n_0,
      O => q_reg_i_63_n_0
    );
q_reg_i_64: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \^q_reg_i_78_0\,
      I1 => \^q_reg[109]_0\,
      I2 => \^q_reg[271]_0\,
      O => q_reg_i_64_n_0
    );
q_reg_i_65: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => q_reg_i_160_n_0,
      I1 => q_reg_i_161_n_0,
      I2 => q_reg_i_162_n_0,
      O => q_reg_i_65_n_0
    );
q_reg_i_66: unisim.vcomponents.LUT6
    generic map(
      INIT => X"022AFDD5FFFF0000"
    )
        port map (
      I0 => q_reg_i_169_n_0,
      I1 => q_reg_i_72_n_0,
      I2 => q_reg_i_170_n_0,
      I3 => q_reg_i_171_n_0,
      I4 => q_reg_i_172_n_0,
      I5 => q_reg_i_175_n_0,
      O => q_reg_i_66_n_0
    );
q_reg_i_67: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => q_reg_i_176_n_0,
      I1 => q_reg_i_80_n_0,
      I2 => q_reg_i_177_n_0,
      O => q_reg_i_67_n_0
    );
q_reg_i_68: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => q_reg_i_178_n_0,
      I1 => q_reg_i_63_n_0,
      I2 => q_reg_i_179_n_0,
      O => q_reg_i_68_n_0
    );
q_reg_i_69: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55559AAAAAAA6555"
    )
        port map (
      I0 => q_reg_i_180_n_0,
      I1 => q_reg_i_85_n_0,
      I2 => q_reg_i_181_n_0,
      I3 => q_reg_i_182_n_0,
      I4 => q_reg_i_183_n_0,
      I5 => q_reg_i_121_n_0,
      O => q_reg_i_69_n_0
    );
q_reg_i_70: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1011FFFF"
    )
        port map (
      I0 => q_reg_i_184_n_0,
      I1 => q_reg_i_185_n_0,
      I2 => pre_reg_cnt(91),
      I3 => pre_reg_cnt(90),
      I4 => q_reg_i_186_n_0,
      O => q_reg_i_70_n_0
    );
q_reg_i_71: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F2F2F200"
    )
        port map (
      I0 => pre_reg_cnt(234),
      I1 => pre_reg_cnt(235),
      I2 => q_reg_i_187_n_0,
      I3 => q_reg_i_188_n_0,
      I4 => q_reg_i_189_n_0,
      O => q_reg_i_71_n_0
    );
q_reg_i_72: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555555555444"
    )
        port map (
      I0 => q_reg_i_190_n_0,
      I1 => q_reg_i_178_n_0,
      I2 => q_reg_i_191_n_0,
      I3 => q_reg_i_192_n_0,
      I4 => q_reg_i_193_n_0,
      I5 => q_reg_i_194_n_0,
      O => q_reg_i_72_n_0
    );
q_reg_i_73: unisim.vcomponents.LUT5
    generic map(
      INIT => X"71777171"
    )
        port map (
      I0 => q_reg_i_91_n_0,
      I1 => q_reg_i_90_n_0,
      I2 => q_reg_i_88_n_0,
      I3 => q_reg_i_89_n_0,
      I4 => q_reg_i_72_n_0,
      O => q_reg_i_73_n_0
    );
q_reg_i_74: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000750075FFFF"
    )
        port map (
      I0 => q_reg_i_195_n_0,
      I1 => q_reg_i_196_n_0,
      I2 => q_reg_i_197_n_0,
      I3 => q_reg_i_198_n_0,
      I4 => q_reg_i_94_n_0,
      I5 => q_reg_i_96_n_0,
      O => q_reg_i_74_n_0
    );
q_reg_i_75: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => q_reg_i_93_n_0,
      I1 => q_reg_i_92_n_0,
      I2 => \^q_reg[188]_0\,
      O => q_reg_i_75_n_0
    );
q_reg_i_76: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFFF1F11"
    )
        port map (
      I0 => q_reg_i_199_n_0,
      I1 => q_reg_i_200_n_0,
      I2 => q_reg_i_201_n_0,
      I3 => q_reg_i_202_n_0,
      I4 => q_reg_i_203_n_0,
      I5 => q_reg_i_204_n_0,
      O => q_reg_i_76_n_0
    );
q_reg_i_77: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555555010101"
    )
        port map (
      I0 => q_reg_i_205_n_0,
      I1 => q_reg_i_190_n_0,
      I2 => q_reg_i_206_n_0,
      I3 => q_reg_i_207_n_0,
      I4 => q_reg_i_63_n_0,
      I5 => q_reg_i_208_n_0,
      O => q_reg_i_77_n_0
    );
q_reg_i_78: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004440455555555"
    )
        port map (
      I0 => q_reg_i_120_n_0,
      I1 => q_reg_i_209_n_0,
      I2 => q_reg_i_210_n_0,
      I3 => q_reg_i_165_n_0,
      I4 => q_reg_i_211_n_0,
      I5 => q_reg_i_121_n_0,
      O => q_reg_i_78_n_0
    );
q_reg_i_79: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => q_reg_i_212_n_0,
      I1 => pre_reg_cnt(271),
      I2 => pre_reg_cnt(270),
      O => q_reg_i_79_n_0
    );
q_reg_i_80: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFEFEFEFFFFFFFE"
    )
        port map (
      I0 => q_reg_i_213_n_0,
      I1 => q_reg_i_214_n_0,
      I2 => pre_reg_cnt(67),
      I3 => pre_reg_cnt(61),
      I4 => q_reg_i_215_n_0,
      I5 => q_reg_i_216_n_0,
      O => q_reg_i_80_n_0
    );
q_reg_i_81: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1F1100001F111F11"
    )
        port map (
      I0 => pre_reg_cnt(271),
      I1 => q_reg_i_212_n_0,
      I2 => q_reg_i_217_n_0,
      I3 => q_reg_i_218_n_0,
      I4 => q_reg_i_219_n_0,
      I5 => q_reg_i_220_n_0,
      O => q_reg_i_81_n_0
    );
q_reg_i_82: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8088AAAA"
    )
        port map (
      I0 => q_reg_i_221_n_0,
      I1 => q_reg_i_222_n_0,
      I2 => q_reg_i_223_n_0,
      I3 => q_reg_i_224_n_0,
      I4 => q_reg_i_225_n_0,
      O => q_reg_i_82_n_0
    );
q_reg_i_83: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0E0E0F0F0E0EE"
    )
        port map (
      I0 => q_reg_i_226_n_0,
      I1 => q_reg_i_227_n_0,
      I2 => q_reg_i_228_n_0,
      I3 => pre_reg_cnt(109),
      I4 => q_reg_i_229_n_0,
      I5 => q_reg_i_230_n_0,
      O => q_reg_i_83_n_0
    );
q_reg_i_84: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCDDCCDDCCDDCCD0"
    )
        port map (
      I0 => pre_reg_cnt(271),
      I1 => q_reg_i_231_n_0,
      I2 => q_reg_i_232_n_0,
      I3 => q_reg_i_212_n_0,
      I4 => pre_reg_cnt(263),
      I5 => pre_reg_cnt(262),
      O => q_reg_i_84_n_0
    );
q_reg_i_85: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FE00FF00FEFE"
    )
        port map (
      I0 => q_reg_i_233_n_0,
      I1 => q_reg_i_193_n_0,
      I2 => pre_reg_cnt(181),
      I3 => q_reg_i_234_n_0,
      I4 => q_reg_i_194_n_0,
      I5 => q_reg_i_235_n_0,
      O => q_reg_i_85_n_0
    );
q_reg_i_86: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF000000FE"
    )
        port map (
      I0 => q_reg_i_236_n_0,
      I1 => pre_reg_cnt(47),
      I2 => pre_reg_cnt(46),
      I3 => pre_reg_cnt(55),
      I4 => q_reg_i_237_n_0,
      I5 => q_reg_i_216_n_0,
      O => q_reg_i_86_n_0
    );
q_reg_i_87: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888A8888888AAAAA"
    )
        port map (
      I0 => q_reg_i_238_n_0,
      I1 => q_reg_i_128_n_0,
      I2 => q_reg_i_239_n_0,
      I3 => q_reg_i_240_n_0,
      I4 => q_reg_i_241_n_0,
      I5 => pre_reg_cnt(135),
      O => q_reg_i_87_n_0
    );
q_reg_i_88: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000BABB"
    )
        port map (
      I0 => q_reg_i_242_n_0,
      I1 => q_reg_i_243_n_0,
      I2 => pre_reg_cnt(34),
      I3 => pre_reg_cnt(33),
      I4 => q_reg_i_151_n_0,
      O => q_reg_i_88_n_0
    );
q_reg_i_89: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA8"
    )
        port map (
      I0 => q_reg_i_244_n_0,
      I1 => q_reg_i_245_n_0,
      I2 => q_reg_i_246_n_0,
      I3 => pre_reg_cnt(265),
      I4 => q_reg_i_247_n_0,
      O => q_reg_i_89_n_0
    );
q_reg_i_90: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA20AA"
    )
        port map (
      I0 => q_reg_i_248_n_0,
      I1 => q_reg_i_184_n_0,
      I2 => q_reg_i_249_n_0,
      I3 => q_reg_i_186_n_0,
      I4 => q_reg_i_203_n_0,
      O => q_reg_i_90_n_0
    );
q_reg_i_91: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DCCCDCCCCCCCDCCC"
    )
        port map (
      I0 => q_reg_i_71_n_0,
      I1 => q_reg_i_100_n_0,
      I2 => q_reg_i_155_n_0,
      I3 => q_reg_i_250_n_0,
      I4 => q_reg_i_251_n_0,
      I5 => q_reg_i_176_n_0,
      O => q_reg_i_91_n_0
    );
q_reg_i_92: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4040400044444444"
    )
        port map (
      I0 => q_reg_i_252_n_0,
      I1 => q_reg_i_253_n_0,
      I2 => q_reg_i_176_n_0,
      I3 => q_reg_i_80_n_0,
      I4 => q_reg_i_177_n_0,
      I5 => q_reg_i_254_n_0,
      O => q_reg_i_92_n_0
    );
q_reg_i_93: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFD0FFD0D0D0D0"
    )
        port map (
      I0 => q_reg_i_245_n_0,
      I1 => q_reg_i_255_n_0,
      I2 => q_reg_i_256_n_0,
      I3 => q_reg_i_179_n_0,
      I4 => q_reg_i_63_n_0,
      I5 => q_reg_i_178_n_0,
      O => q_reg_i_93_n_0
    );
q_reg_i_94: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => q_reg_i_257_n_0,
      I1 => q_reg_i_121_n_0,
      I2 => q_reg_i_258_n_0,
      O => q_reg_i_94_n_0
    );
q_reg_i_95: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF5DFF5D5D5D5D"
    )
        port map (
      I0 => q_reg_i_259_n_0,
      I1 => q_reg_i_260_n_0,
      I2 => q_reg_i_86_n_0,
      I3 => q_reg_i_197_n_0,
      I4 => q_reg_i_196_n_0,
      I5 => q_reg_i_195_n_0,
      O => q_reg_i_95_n_0
    );
q_reg_i_96: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2F2F2F2F2F002F2F"
    )
        port map (
      I0 => q_reg_i_261_n_0,
      I1 => q_reg_i_238_n_0,
      I2 => q_reg_i_184_n_0,
      I3 => q_reg_i_180_n_0,
      I4 => q_reg_i_262_n_0,
      I5 => q_reg_i_263_n_0,
      O => q_reg_i_96_n_0
    );
q_reg_i_97: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000000000F1"
    )
        port map (
      I0 => q_reg_i_264_n_0,
      I1 => pre_reg_cnt(43),
      I2 => q_reg_i_265_n_0,
      I3 => q_reg_i_266_n_0,
      I4 => q_reg_i_267_n_0,
      I5 => q_reg_i_177_n_0,
      O => q_reg_i_97_n_0
    );
q_reg_i_98: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000050505051"
    )
        port map (
      I0 => pre_reg_cnt(31),
      I1 => pre_reg_cnt(25),
      I2 => q_reg_i_268_n_0,
      I3 => pre_reg_cnt(26),
      I4 => pre_reg_cnt(27),
      I5 => q_reg_i_269_n_0,
      O => q_reg_i_98_n_0
    );
q_reg_i_99: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0054FFFFFFFFFFFF"
    )
        port map (
      I0 => q_reg_i_253_n_0,
      I1 => q_reg_i_270_n_0,
      I2 => pre_reg_cnt(7),
      I3 => q_reg_i_271_n_0,
      I4 => q_reg_i_272_n_0,
      I5 => q_reg_i_273_n_0,
      O => q_reg_i_99_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \nbit_register__parameterized1\ is
  port (
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    S : out STD_LOGIC_VECTOR ( 2 downto 0 );
    d1 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    max_cnt : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 9 downto 0 );
    C : in STD_LOGIC_VECTOR ( 23 downto 0 );
    P : in STD_LOGIC_VECTOR ( 6 downto 0 );
    O : in STD_LOGIC_VECTOR ( 2 downto 0 );
    q_reg_0 : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \nbit_register__parameterized1\ : entity is "nbit_register";
end \nbit_register__parameterized1\;

architecture STRUCTURE of \nbit_register__parameterized1\ is
  signal NLW_q_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_q_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_q_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_q_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_q_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_q_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_q_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_q_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_q_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_q_reg_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 24 );
  signal NLW_q_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of q_reg : label is "{SYNTH-12 {cell *THIS*}}";
begin
\i__carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => P(6),
      I1 => O(2),
      O => S(2)
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(5),
      I1 => O(1),
      O => S(1)
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(4),
      I1 => O(0),
      O => S(0)
    );
\i__carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(3),
      I1 => q_reg_0(3),
      O => d1(3)
    );
\i__carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(2),
      I1 => q_reg_0(2),
      O => d1(2)
    );
\i__carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(1),
      I1 => q_reg_0(1),
      O => d1(1)
    );
\i__carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => P(0),
      I1 => q_reg_0(0),
      O => d1(0)
    );
q_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 0,
      BREG => 0,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => A(9),
      A(28) => A(9),
      A(27) => A(9),
      A(26) => A(9),
      A(25) => A(9),
      A(24) => A(9),
      A(23) => A(9),
      A(22) => A(9),
      A(21) => A(9),
      A(20) => A(9),
      A(19) => A(9),
      A(18) => A(9),
      A(17) => A(9),
      A(16) => A(9),
      A(15) => A(9),
      A(14) => A(9),
      A(13) => A(9),
      A(12) => A(9),
      A(11) => A(9),
      A(10) => A(9),
      A(9 downto 0) => A(9 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_q_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17 downto 0) => B"000000100100001000",
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_q_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47) => C(23),
      C(46) => C(23),
      C(45) => C(23),
      C(44) => C(23),
      C(43) => C(23),
      C(42) => C(23),
      C(41) => C(23),
      C(40) => C(23),
      C(39) => C(23),
      C(38) => C(23),
      C(37) => C(23),
      C(36) => C(23),
      C(35) => C(23),
      C(34) => C(23),
      C(33) => C(23),
      C(32) => C(23),
      C(31) => C(23),
      C(30) => C(23),
      C(29) => C(23),
      C(28) => C(23),
      C(27) => C(23),
      C(26) => C(23),
      C(25) => C(23),
      C(24) => C(23),
      C(23 downto 0) => C(23 downto 0),
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_q_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_q_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '0',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '1',
      CLK => max_cnt,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_q_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0110101",
      OVERFLOW => NLW_q_reg_OVERFLOW_UNCONNECTED,
      P(47 downto 24) => NLW_q_reg_P_UNCONNECTED(47 downto 24),
      P(23 downto 0) => D(23 downto 0),
      PATTERNBDETECT => NLW_q_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_q_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_q_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_q_reg_UNDERFLOW_UNCONNECTED
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity calc_unit is
  port (
    pre_trig : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    JD_OBUF : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    \cnt_offset_reg[0]_0\ : in STD_LOGIC;
    \q_reg[0]\ : in STD_LOGIC;
    \q_reg[319]\ : in STD_LOGIC_VECTOR ( 319 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end calc_unit;

architecture STRUCTURE of calc_unit is
  signal A : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal cnt_offset_reg : STD_LOGIC_VECTOR ( 0 to 0 );
  signal crs_reg_n_0 : STD_LOGIC;
  signal crs_reg_n_1 : STD_LOGIC;
  signal crs_reg_n_12 : STD_LOGIC;
  signal crs_reg_n_13 : STD_LOGIC;
  signal crs_reg_n_14 : STD_LOGIC;
  signal crs_reg_n_2 : STD_LOGIC;
  signal crs_reg_n_3 : STD_LOGIC;
  signal crs_reg_n_4 : STD_LOGIC;
  signal crs_reg_n_5 : STD_LOGIC;
  signal crs_reg_n_6 : STD_LOGIC;
  signal crs_reg_n_8 : STD_LOGIC;
  signal crs_reg_n_9 : STD_LOGIC;
  signal d10_out : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \d1__0\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \d1_carry__0_n_5\ : STD_LOGIC;
  signal d1_carry_n_0 : STD_LOGIC;
  signal \d1_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal d1_n_100 : STD_LOGIC;
  signal d1_n_101 : STD_LOGIC;
  signal d1_n_102 : STD_LOGIC;
  signal d1_n_103 : STD_LOGIC;
  signal d1_n_104 : STD_LOGIC;
  signal d1_n_105 : STD_LOGIC;
  signal d1_n_82 : STD_LOGIC;
  signal d1_n_83 : STD_LOGIC;
  signal d1_n_84 : STD_LOGIC;
  signal d1_n_85 : STD_LOGIC;
  signal d1_n_86 : STD_LOGIC;
  signal d1_n_87 : STD_LOGIC;
  signal d1_n_88 : STD_LOGIC;
  signal d1_n_89 : STD_LOGIC;
  signal d1_n_90 : STD_LOGIC;
  signal d1_n_91 : STD_LOGIC;
  signal d1_n_92 : STD_LOGIC;
  signal d1_n_93 : STD_LOGIC;
  signal d1_n_94 : STD_LOGIC;
  signal d1_n_95 : STD_LOGIC;
  signal d1_n_96 : STD_LOGIC;
  signal d1_n_97 : STD_LOGIC;
  signal d1_n_98 : STD_LOGIC;
  signal d1_n_99 : STD_LOGIC;
  signal max_cnt : STD_LOGIC;
  signal output_reg_n_24 : STD_LOGIC;
  signal output_reg_n_25 : STD_LOGIC;
  signal output_reg_n_26 : STD_LOGIC;
  signal output_reg_n_27 : STD_LOGIC;
  signal output_reg_n_28 : STD_LOGIC;
  signal output_reg_n_29 : STD_LOGIC;
  signal output_reg_n_30 : STD_LOGIC;
  signal pre_bit_cnt : STD_LOGIC_VECTOR ( 0 to 0 );
  signal pre_reg_n_0 : STD_LOGIC;
  signal pre_reg_n_1 : STD_LOGIC;
  signal pre_reg_n_10 : STD_LOGIC;
  signal pre_reg_n_11 : STD_LOGIC;
  signal pre_reg_n_12 : STD_LOGIC;
  signal pre_reg_n_13 : STD_LOGIC;
  signal pre_reg_n_14 : STD_LOGIC;
  signal pre_reg_n_15 : STD_LOGIC;
  signal pre_reg_n_16 : STD_LOGIC;
  signal pre_reg_n_17 : STD_LOGIC;
  signal pre_reg_n_18 : STD_LOGIC;
  signal pre_reg_n_19 : STD_LOGIC;
  signal pre_reg_n_2 : STD_LOGIC;
  signal pre_reg_n_20 : STD_LOGIC;
  signal pre_reg_n_21 : STD_LOGIC;
  signal pre_reg_n_22 : STD_LOGIC;
  signal pre_reg_n_23 : STD_LOGIC;
  signal pre_reg_n_24 : STD_LOGIC;
  signal pre_reg_n_25 : STD_LOGIC;
  signal pre_reg_n_26 : STD_LOGIC;
  signal pre_reg_n_27 : STD_LOGIC;
  signal pre_reg_n_28 : STD_LOGIC;
  signal pre_reg_n_29 : STD_LOGIC;
  signal pre_reg_n_3 : STD_LOGIC;
  signal pre_reg_n_30 : STD_LOGIC;
  signal pre_reg_n_31 : STD_LOGIC;
  signal pre_reg_n_32 : STD_LOGIC;
  signal pre_reg_n_4 : STD_LOGIC;
  signal pre_reg_n_5 : STD_LOGIC;
  signal pre_reg_n_6 : STD_LOGIC;
  signal pre_reg_n_7 : STD_LOGIC;
  signal pre_reg_n_8 : STD_LOGIC;
  signal pre_reg_n_9 : STD_LOGIC;
  signal pre_tdl_bit_cnt_n_0 : STD_LOGIC;
  signal pre_tdl_bit_cnt_n_1 : STD_LOGIC;
  signal pre_tdl_bit_cnt_n_10 : STD_LOGIC;
  signal pre_tdl_bit_cnt_n_11 : STD_LOGIC;
  signal pre_tdl_bit_cnt_n_2 : STD_LOGIC;
  signal pre_tdl_bit_cnt_n_3 : STD_LOGIC;
  signal pre_tdl_bit_cnt_n_4 : STD_LOGIC;
  signal pre_tdl_bit_cnt_n_5 : STD_LOGIC;
  signal pre_tdl_bit_cnt_n_6 : STD_LOGIC;
  signal pre_tdl_bit_cnt_n_7 : STD_LOGIC;
  signal pre_tdl_bit_cnt_n_8 : STD_LOGIC;
  signal pst_trig : STD_LOGIC;
  signal NLW_d1_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_d1_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_d1_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_d1_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_d1_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_d1_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_d1_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_d1_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_d1_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_d1_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 24 );
  signal NLW_d1_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_d1_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_d1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_d1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_d1_inferred__0/i__carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_d1_inferred__0/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_d1_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of d1 : label is "{SYNTH-13 {cell *THIS*}}";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of d1 : label is "SWEEP";
  attribute OPT_MODIFIED of d1_carry : label is "SWEEP";
  attribute OPT_MODIFIED of \d1_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \d1_inferred__0/i__carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \d1_inferred__0/i__carry__0\ : label is "SWEEP";
begin
\cnt_offset_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \q_reg[0]\,
      CE => '1',
      D => \cnt_offset_reg[0]_0\,
      Q => cnt_offset_reg(0),
      R => '0'
    );
crs_reg: entity work.nbit_register_79
     port map (
      A(4) => A(6),
      A(3) => crs_reg_n_8,
      A(2) => crs_reg_n_9,
      A(1 downto 0) => A(1 downto 0),
      CLK => pst_trig,
      DI(1) => crs_reg_n_12,
      DI(0) => crs_reg_n_13,
      Q(3 downto 0) => Q(3 downto 0),
      S(2) => crs_reg_n_0,
      S(1) => crs_reg_n_1,
      S(0) => crs_reg_n_2,
      cnt_offset_reg(0) => cnt_offset_reg(0),
      \cnt_offset_reg[0]\(0) => crs_reg_n_14,
      \q_reg[2]_0\(3) => crs_reg_n_3,
      \q_reg[2]_0\(2) => crs_reg_n_4,
      \q_reg[2]_0\(1) => crs_reg_n_5,
      \q_reg[2]_0\(0) => crs_reg_n_6
    );
d1: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 0,
      BREG => 0,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => A(6),
      A(28) => A(6),
      A(27) => A(6),
      A(26) => A(6),
      A(25) => A(6),
      A(24) => A(6),
      A(23) => A(6),
      A(22) => A(6),
      A(21) => A(6),
      A(20) => A(6),
      A(19) => A(6),
      A(18) => A(6),
      A(17) => A(6),
      A(16) => A(6),
      A(15) => A(6),
      A(14) => A(6),
      A(13) => A(6),
      A(12) => A(6),
      A(11) => A(6),
      A(10) => A(6),
      A(9) => A(6),
      A(8) => A(6),
      A(7) => A(6),
      A(6) => A(6),
      A(5) => A(6),
      A(4) => A(6),
      A(3) => crs_reg_n_8,
      A(2) => crs_reg_n_9,
      A(1 downto 0) => A(1 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_d1_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17 downto 0) => B"010100001001000000",
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_d1_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_d1_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_d1_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '0',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => '0',
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_d1_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_d1_OVERFLOW_UNCONNECTED,
      P(47 downto 24) => NLW_d1_P_UNCONNECTED(47 downto 24),
      P(23) => d1_n_82,
      P(22) => d1_n_83,
      P(21) => d1_n_84,
      P(20) => d1_n_85,
      P(19) => d1_n_86,
      P(18) => d1_n_87,
      P(17) => d1_n_88,
      P(16) => d1_n_89,
      P(15) => d1_n_90,
      P(14) => d1_n_91,
      P(13) => d1_n_92,
      P(12) => d1_n_93,
      P(11) => d1_n_94,
      P(10) => d1_n_95,
      P(9) => d1_n_96,
      P(8) => d1_n_97,
      P(7) => d1_n_98,
      P(6) => d1_n_99,
      P(5) => d1_n_100,
      P(4) => d1_n_101,
      P(3) => d1_n_102,
      P(2) => d1_n_103,
      P(1) => d1_n_104,
      P(0) => d1_n_105,
      PATTERNBDETECT => NLW_d1_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_d1_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_d1_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_d1_UNDERFLOW_UNCONNECTED
    );
d1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => d1_carry_n_0,
      CO(2 downto 0) => NLW_d1_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => '0',
      DI(3) => crs_reg_n_14,
      DI(2 downto 0) => B"001",
      O(3 downto 0) => \d1__0\(3 downto 0),
      S(3) => crs_reg_n_3,
      S(2) => crs_reg_n_4,
      S(1) => crs_reg_n_5,
      S(0) => crs_reg_n_6
    );
\d1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => d1_carry_n_0,
      CO(3 downto 0) => \NLW_d1_carry__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => crs_reg_n_12,
      DI(0) => crs_reg_n_13,
      O(3) => \NLW_d1_carry__0_O_UNCONNECTED\(3),
      O(2) => \d1_carry__0_n_5\,
      O(1 downto 0) => \d1__0\(5 downto 4),
      S(3) => '0',
      S(2) => crs_reg_n_0,
      S(1) => crs_reg_n_1,
      S(0) => crs_reg_n_2
    );
\d1_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \d1_inferred__0/i__carry_n_0\,
      CO(2 downto 0) => \NLW_d1_inferred__0/i__carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => d1_n_85,
      DI(2) => d1_n_86,
      DI(1) => d1_n_87,
      DI(0) => d1_n_88,
      O(3 downto 0) => d10_out(3 downto 0),
      S(3) => output_reg_n_27,
      S(2) => output_reg_n_28,
      S(1) => output_reg_n_29,
      S(0) => output_reg_n_30
    );
\d1_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \d1_inferred__0/i__carry_n_0\,
      CO(3 downto 0) => \NLW_d1_inferred__0/i__carry__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => d1_n_83,
      DI(0) => d1_n_84,
      O(3) => \NLW_d1_inferred__0/i__carry__0_O_UNCONNECTED\(3),
      O(2 downto 0) => d10_out(6 downto 4),
      S(3) => '0',
      S(2) => output_reg_n_24,
      S(1) => output_reg_n_25,
      S(0) => output_reg_n_26
    );
out_reg_trig: entity work.\binary_counter__parameterized0\
     port map (
      CLK => CLK,
      JD_OBUF(0) => JD_OBUF(1),
      max_cnt => max_cnt
    );
output_reg: entity work.\nbit_register__parameterized1\
     port map (
      A(9) => pre_tdl_bit_cnt_n_0,
      A(8) => pre_tdl_bit_cnt_n_1,
      A(7) => pre_tdl_bit_cnt_n_2,
      A(6) => pre_tdl_bit_cnt_n_3,
      A(5) => pre_tdl_bit_cnt_n_4,
      A(4) => pre_tdl_bit_cnt_n_5,
      A(3) => pre_tdl_bit_cnt_n_6,
      A(2) => pre_tdl_bit_cnt_n_7,
      A(1) => pre_tdl_bit_cnt_n_8,
      A(0) => pre_bit_cnt(0),
      C(23 downto 17) => d10_out(6 downto 0),
      C(16) => d1_n_89,
      C(15) => d1_n_90,
      C(14) => d1_n_91,
      C(13) => d1_n_92,
      C(12) => d1_n_93,
      C(11) => d1_n_94,
      C(10) => d1_n_95,
      C(9) => d1_n_96,
      C(8) => d1_n_97,
      C(7) => d1_n_98,
      C(6) => d1_n_99,
      C(5) => d1_n_100,
      C(4) => d1_n_101,
      C(3) => d1_n_102,
      C(2) => d1_n_103,
      C(1) => d1_n_104,
      C(0) => d1_n_105,
      D(23 downto 0) => D(23 downto 0),
      O(2) => \d1_carry__0_n_5\,
      O(1 downto 0) => \d1__0\(5 downto 4),
      P(6) => d1_n_82,
      P(5) => d1_n_83,
      P(4) => d1_n_84,
      P(3) => d1_n_85,
      P(2) => d1_n_86,
      P(1) => d1_n_87,
      P(0) => d1_n_88,
      S(2) => output_reg_n_24,
      S(1) => output_reg_n_25,
      S(0) => output_reg_n_26,
      d1(3) => output_reg_n_27,
      d1(2) => output_reg_n_28,
      d1(1) => output_reg_n_29,
      d1(0) => output_reg_n_30,
      max_cnt => max_cnt,
      q_reg_0(3 downto 0) => \d1__0\(3 downto 0)
    );
pre_reg: entity work.\nbit_register__parameterized0\
     port map (
      DI(1) => pre_reg_n_6,
      DI(0) => pre_reg_n_7,
      S(2) => pre_reg_n_0,
      S(1) => pre_reg_n_1,
      S(0) => pre_reg_n_2,
      q_reg(1) => pre_tdl_bit_cnt_n_10,
      q_reg(0) => pre_tdl_bit_cnt_n_11,
      \q_reg[0]_0\ => \q_reg[0]\,
      \q_reg[109]_0\ => pre_reg_n_9,
      \q_reg[131]_0\ => pre_reg_n_23,
      \q_reg[132]_0\ => pre_reg_n_24,
      \q_reg[137]_0\ => pre_reg_n_5,
      \q_reg[184]_0\ => pre_reg_n_29,
      \q_reg[188]_0\ => pre_reg_n_22,
      \q_reg[19]_0\ => pre_reg_n_26,
      \q_reg[215]_0\ => pre_reg_n_30,
      \q_reg[239]_0\ => pre_reg_n_32,
      \q_reg[247]_0\ => pre_reg_n_31,
      \q_reg[271]_0\ => pre_reg_n_8,
      \q_reg[295]_0\ => pre_reg_n_17,
      \q_reg[295]_1\ => pre_reg_n_25,
      \q_reg[319]_0\(319 downto 0) => \q_reg[319]\(319 downto 0),
      \q_reg[31]_0\ => pre_reg_n_19,
      \q_reg[34]_0\ => pre_reg_n_13,
      \q_reg[43]_0\ => pre_reg_n_18,
      \q_reg[55]_0\ => pre_reg_n_16,
      \q_reg[57]_0\ => pre_reg_n_27,
      \q_reg[77]_0\ => pre_reg_n_28,
      \q_reg[91]_0\ => pre_reg_n_20,
      q_reg_i_163_0 => pre_reg_n_4,
      q_reg_i_47_0(1) => pre_reg_n_11,
      q_reg_i_47_0(0) => pre_reg_n_12,
      q_reg_i_64_0 => pre_reg_n_3,
      q_reg_i_75_0 => pre_reg_n_21,
      q_reg_i_78_0 => pre_reg_n_10,
      q_reg_i_93_0 => pre_reg_n_15,
      q_reg_i_96_0 => pre_reg_n_14
    );
pre_tdl_bit_cnt: entity work.hammin_weight_cnter
     port map (
      A(9) => pre_tdl_bit_cnt_n_0,
      A(8) => pre_tdl_bit_cnt_n_1,
      A(7) => pre_tdl_bit_cnt_n_2,
      A(6) => pre_tdl_bit_cnt_n_3,
      A(5) => pre_tdl_bit_cnt_n_4,
      A(4) => pre_tdl_bit_cnt_n_5,
      A(3) => pre_tdl_bit_cnt_n_6,
      A(2) => pre_tdl_bit_cnt_n_7,
      A(1) => pre_tdl_bit_cnt_n_8,
      A(0) => pre_bit_cnt(0),
      DI(1) => pre_reg_n_6,
      DI(0) => pre_reg_n_7,
      S(2) => pre_reg_n_0,
      S(1) => pre_reg_n_1,
      S(0) => pre_reg_n_2,
      q_reg(1) => pre_reg_n_11,
      q_reg(0) => pre_reg_n_12,
      q_reg_0 => pre_reg_n_26,
      q_reg_1 => pre_reg_n_19,
      q_reg_10 => pre_reg_n_18,
      q_reg_11 => pre_reg_n_17,
      q_reg_12 => pre_reg_n_16,
      q_reg_13 => pre_reg_n_13,
      q_reg_14 => pre_reg_n_15,
      q_reg_15 => pre_reg_n_14,
      q_reg_16 => pre_reg_n_20,
      q_reg_17 => pre_reg_n_21,
      q_reg_18 => pre_reg_n_10,
      q_reg_19 => pre_reg_n_8,
      q_reg_2 => pre_reg_n_28,
      q_reg_20 => pre_reg_n_9,
      q_reg_21 => pre_reg_n_23,
      q_reg_22 => pre_reg_n_25,
      q_reg_3 => pre_reg_n_27,
      q_reg_4 => pre_reg_n_24,
      q_reg_5 => pre_reg_n_22,
      q_reg_6 => pre_reg_n_29,
      q_reg_7 => pre_reg_n_32,
      q_reg_8 => pre_reg_n_30,
      q_reg_9 => pre_reg_n_31,
      q_reg_i_12_0 => pre_reg_n_4,
      q_reg_i_12_1 => pre_reg_n_3,
      q_reg_i_12_2 => pre_reg_n_5,
      q_reg_i_33(1) => pre_tdl_bit_cnt_n_10,
      q_reg_i_33(0) => pre_tdl_bit_cnt_n_11
    );
pre_trig_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => JD_OBUF(0),
      Q => pre_trig,
      R => '0'
    );
pst_trig_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      D => JD_OBUF(1),
      Q => pst_trig,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity n4bit_carry_chain is
  port (
    \q_reg[3]\ : out STD_LOGIC_VECTOR ( 319 downto 0 );
    JD_OBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC
  );
end n4bit_carry_chain;

architecture STRUCTURE of n4bit_carry_chain is
  signal carry_chain_0 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_100 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_104 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_108 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_112 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_116 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_12 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_120 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_124 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_128 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_132 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_136 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_140 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_144 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_148 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_152 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_156 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_16 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_160 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_164 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_168 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_172 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_176 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_180 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_184 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_188 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_192 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_196 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_20 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_200 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_204 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_208 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_212 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_216 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_220 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_224 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_228 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_232 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_236 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_24 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_240 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_244 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_248 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_252 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_256 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_260 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_264 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_268 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_272 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_276 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_28 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_280 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_284 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_288 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_292 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_296 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_300 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_304 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_308 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_312 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_316 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_32 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_36 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_4 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_40 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_44 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_48 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_52 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_56 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_60 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_64 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_68 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_72 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_76 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_8 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_80 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_84 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_88 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_92 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal carry_chain_96 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_CARRY4_inst_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[10].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[11].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[12].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[13].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[14].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[15].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[16].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[17].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[18].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[19].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[1].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[20].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[21].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[22].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[23].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[24].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[25].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[26].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[27].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[28].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[29].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[2].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[30].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[31].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[32].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[33].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[34].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[35].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[36].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[37].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[38].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[39].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[3].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[40].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[41].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[42].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[43].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[44].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[45].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[46].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[47].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[48].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[49].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[4].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[50].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[51].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[52].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[53].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[54].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[55].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[56].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[57].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[58].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[59].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[5].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[60].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[61].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[62].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[63].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[64].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[65].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[66].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[67].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[68].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[69].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[6].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[70].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[71].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[72].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[73].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[74].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[75].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[76].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[77].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[78].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[79].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[7].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[8].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tdl_gen[9].carry_x_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute box_type : string;
  attribute box_type of CARRY4_inst : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[10].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[11].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[12].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[13].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[14].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[15].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[16].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[17].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[18].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[19].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[1].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[20].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[21].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[22].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[23].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[24].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[25].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[26].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[27].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[28].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[29].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[2].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[30].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[31].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[32].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[33].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[34].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[35].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[36].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[37].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[38].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[39].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[3].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[40].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[41].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[42].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[43].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[44].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[45].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[46].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[47].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[48].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[49].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[4].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[50].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[51].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[52].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[53].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[54].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[55].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[56].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[57].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[58].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[59].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[5].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[60].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[61].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[62].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[63].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[64].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[65].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[66].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[67].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[68].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[69].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[6].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[70].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[71].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[72].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[73].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[74].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[75].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[76].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[77].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[78].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[79].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[7].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[8].carry_x\ : label is "PRIMITIVE";
  attribute box_type of \tdl_gen[9].carry_x\ : label is "PRIMITIVE";
begin
CARRY4_inst: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 0) => carry_chain_0(3 downto 0),
      CYINIT => JD_OBUF(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_CARRY4_inst_O_UNCONNECTED(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\register_gen[0].dff\: entity work.nbit_register
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_0(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(3 downto 0)
    );
\register_gen[10].dff\: entity work.nbit_register_0
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_40(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(43 downto 40)
    );
\register_gen[11].dff\: entity work.nbit_register_1
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_44(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(47 downto 44)
    );
\register_gen[12].dff\: entity work.nbit_register_2
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_48(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(51 downto 48)
    );
\register_gen[13].dff\: entity work.nbit_register_3
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_52(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(55 downto 52)
    );
\register_gen[14].dff\: entity work.nbit_register_4
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_56(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(59 downto 56)
    );
\register_gen[15].dff\: entity work.nbit_register_5
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_60(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(63 downto 60)
    );
\register_gen[16].dff\: entity work.nbit_register_6
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_64(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(67 downto 64)
    );
\register_gen[17].dff\: entity work.nbit_register_7
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_68(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(71 downto 68)
    );
\register_gen[18].dff\: entity work.nbit_register_8
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_72(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(75 downto 72)
    );
\register_gen[19].dff\: entity work.nbit_register_9
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_76(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(79 downto 76)
    );
\register_gen[1].dff\: entity work.nbit_register_10
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_4(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(7 downto 4)
    );
\register_gen[20].dff\: entity work.nbit_register_11
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_80(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(83 downto 80)
    );
\register_gen[21].dff\: entity work.nbit_register_12
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_84(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(87 downto 84)
    );
\register_gen[22].dff\: entity work.nbit_register_13
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_88(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(91 downto 88)
    );
\register_gen[23].dff\: entity work.nbit_register_14
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_92(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(95 downto 92)
    );
\register_gen[24].dff\: entity work.nbit_register_15
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_96(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(99 downto 96)
    );
\register_gen[25].dff\: entity work.nbit_register_16
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_100(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(103 downto 100)
    );
\register_gen[26].dff\: entity work.nbit_register_17
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_104(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(107 downto 104)
    );
\register_gen[27].dff\: entity work.nbit_register_18
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_108(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(111 downto 108)
    );
\register_gen[28].dff\: entity work.nbit_register_19
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_112(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(115 downto 112)
    );
\register_gen[29].dff\: entity work.nbit_register_20
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_116(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(119 downto 116)
    );
\register_gen[2].dff\: entity work.nbit_register_21
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_8(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(11 downto 8)
    );
\register_gen[30].dff\: entity work.nbit_register_22
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_120(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(123 downto 120)
    );
\register_gen[31].dff\: entity work.nbit_register_23
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_124(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(127 downto 124)
    );
\register_gen[32].dff\: entity work.nbit_register_24
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_128(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(131 downto 128)
    );
\register_gen[33].dff\: entity work.nbit_register_25
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_132(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(135 downto 132)
    );
\register_gen[34].dff\: entity work.nbit_register_26
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_136(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(139 downto 136)
    );
\register_gen[35].dff\: entity work.nbit_register_27
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_140(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(143 downto 140)
    );
\register_gen[36].dff\: entity work.nbit_register_28
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_144(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(147 downto 144)
    );
\register_gen[37].dff\: entity work.nbit_register_29
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_148(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(151 downto 148)
    );
\register_gen[38].dff\: entity work.nbit_register_30
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_152(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(155 downto 152)
    );
\register_gen[39].dff\: entity work.nbit_register_31
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_156(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(159 downto 156)
    );
\register_gen[3].dff\: entity work.nbit_register_32
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_12(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(15 downto 12)
    );
\register_gen[40].dff\: entity work.nbit_register_33
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_160(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(163 downto 160)
    );
\register_gen[41].dff\: entity work.nbit_register_34
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_164(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(167 downto 164)
    );
\register_gen[42].dff\: entity work.nbit_register_35
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_168(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(171 downto 168)
    );
\register_gen[43].dff\: entity work.nbit_register_36
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_172(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(175 downto 172)
    );
\register_gen[44].dff\: entity work.nbit_register_37
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_176(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(179 downto 176)
    );
\register_gen[45].dff\: entity work.nbit_register_38
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_180(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(183 downto 180)
    );
\register_gen[46].dff\: entity work.nbit_register_39
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_184(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(187 downto 184)
    );
\register_gen[47].dff\: entity work.nbit_register_40
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_188(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(191 downto 188)
    );
\register_gen[48].dff\: entity work.nbit_register_41
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_192(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(195 downto 192)
    );
\register_gen[49].dff\: entity work.nbit_register_42
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_196(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(199 downto 196)
    );
\register_gen[4].dff\: entity work.nbit_register_43
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_16(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(19 downto 16)
    );
\register_gen[50].dff\: entity work.nbit_register_44
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_200(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(203 downto 200)
    );
\register_gen[51].dff\: entity work.nbit_register_45
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_204(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(207 downto 204)
    );
\register_gen[52].dff\: entity work.nbit_register_46
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_208(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(211 downto 208)
    );
\register_gen[53].dff\: entity work.nbit_register_47
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_212(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(215 downto 212)
    );
\register_gen[54].dff\: entity work.nbit_register_48
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_216(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(219 downto 216)
    );
\register_gen[55].dff\: entity work.nbit_register_49
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_220(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(223 downto 220)
    );
\register_gen[56].dff\: entity work.nbit_register_50
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_224(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(227 downto 224)
    );
\register_gen[57].dff\: entity work.nbit_register_51
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_228(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(231 downto 228)
    );
\register_gen[58].dff\: entity work.nbit_register_52
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_232(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(235 downto 232)
    );
\register_gen[59].dff\: entity work.nbit_register_53
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_236(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(239 downto 236)
    );
\register_gen[5].dff\: entity work.nbit_register_54
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_20(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(23 downto 20)
    );
\register_gen[60].dff\: entity work.nbit_register_55
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_240(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(243 downto 240)
    );
\register_gen[61].dff\: entity work.nbit_register_56
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_244(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(247 downto 244)
    );
\register_gen[62].dff\: entity work.nbit_register_57
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_248(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(251 downto 248)
    );
\register_gen[63].dff\: entity work.nbit_register_58
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_252(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(255 downto 252)
    );
\register_gen[64].dff\: entity work.nbit_register_59
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_256(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(259 downto 256)
    );
\register_gen[65].dff\: entity work.nbit_register_60
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_260(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(263 downto 260)
    );
\register_gen[66].dff\: entity work.nbit_register_61
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_264(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(267 downto 264)
    );
\register_gen[67].dff\: entity work.nbit_register_62
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_268(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(271 downto 268)
    );
\register_gen[68].dff\: entity work.nbit_register_63
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_272(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(275 downto 272)
    );
\register_gen[69].dff\: entity work.nbit_register_64
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_276(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(279 downto 276)
    );
\register_gen[6].dff\: entity work.nbit_register_65
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_24(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(27 downto 24)
    );
\register_gen[70].dff\: entity work.nbit_register_66
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_280(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(283 downto 280)
    );
\register_gen[71].dff\: entity work.nbit_register_67
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_284(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(287 downto 284)
    );
\register_gen[72].dff\: entity work.nbit_register_68
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_288(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(291 downto 288)
    );
\register_gen[73].dff\: entity work.nbit_register_69
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_292(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(295 downto 292)
    );
\register_gen[74].dff\: entity work.nbit_register_70
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_296(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(299 downto 296)
    );
\register_gen[75].dff\: entity work.nbit_register_71
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_300(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(303 downto 300)
    );
\register_gen[76].dff\: entity work.nbit_register_72
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_304(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(307 downto 304)
    );
\register_gen[77].dff\: entity work.nbit_register_73
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_308(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(311 downto 308)
    );
\register_gen[78].dff\: entity work.nbit_register_74
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_312(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(315 downto 312)
    );
\register_gen[79].dff\: entity work.nbit_register_75
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_316(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(319 downto 316)
    );
\register_gen[7].dff\: entity work.nbit_register_76
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_28(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(31 downto 28)
    );
\register_gen[8].dff\: entity work.nbit_register_77
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_32(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(35 downto 32)
    );
\register_gen[9].dff\: entity work.nbit_register_78
     port map (
      CLK => CLK,
      D(3 downto 0) => carry_chain_36(3 downto 0),
      \q_reg[3]_0\(3 downto 0) => \q_reg[3]\(39 downto 36)
    );
\tdl_gen[10].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_36(3),
      CO(3 downto 0) => carry_chain_40(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[10].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[11].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_40(3),
      CO(3 downto 0) => carry_chain_44(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[11].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[12].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_44(3),
      CO(3 downto 0) => carry_chain_48(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[12].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[13].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_48(3),
      CO(3 downto 0) => carry_chain_52(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[13].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[14].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_52(3),
      CO(3 downto 0) => carry_chain_56(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[14].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[15].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_56(3),
      CO(3 downto 0) => carry_chain_60(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[15].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[16].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_60(3),
      CO(3 downto 0) => carry_chain_64(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[16].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[17].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_64(3),
      CO(3 downto 0) => carry_chain_68(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[17].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[18].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_68(3),
      CO(3 downto 0) => carry_chain_72(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[18].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[19].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_72(3),
      CO(3 downto 0) => carry_chain_76(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[19].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[1].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_0(3),
      CO(3 downto 0) => carry_chain_4(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[1].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[20].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_76(3),
      CO(3 downto 0) => carry_chain_80(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[20].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[21].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_80(3),
      CO(3 downto 0) => carry_chain_84(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[21].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[22].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_84(3),
      CO(3 downto 0) => carry_chain_88(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[22].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[23].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_88(3),
      CO(3 downto 0) => carry_chain_92(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[23].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[24].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_92(3),
      CO(3 downto 0) => carry_chain_96(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[24].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[25].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_96(3),
      CO(3 downto 0) => carry_chain_100(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[25].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[26].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_100(3),
      CO(3 downto 0) => carry_chain_104(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[26].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[27].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_104(3),
      CO(3 downto 0) => carry_chain_108(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[27].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[28].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_108(3),
      CO(3 downto 0) => carry_chain_112(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[28].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[29].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_112(3),
      CO(3 downto 0) => carry_chain_116(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[29].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[2].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_4(3),
      CO(3 downto 0) => carry_chain_8(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[2].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[30].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_116(3),
      CO(3 downto 0) => carry_chain_120(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[30].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[31].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_120(3),
      CO(3 downto 0) => carry_chain_124(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[31].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[32].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_124(3),
      CO(3 downto 0) => carry_chain_128(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[32].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[33].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_128(3),
      CO(3 downto 0) => carry_chain_132(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[33].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[34].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_132(3),
      CO(3 downto 0) => carry_chain_136(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[34].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[35].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_136(3),
      CO(3 downto 0) => carry_chain_140(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[35].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[36].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_140(3),
      CO(3 downto 0) => carry_chain_144(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[36].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[37].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_144(3),
      CO(3 downto 0) => carry_chain_148(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[37].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[38].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_148(3),
      CO(3 downto 0) => carry_chain_152(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[38].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[39].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_152(3),
      CO(3 downto 0) => carry_chain_156(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[39].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[3].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_8(3),
      CO(3 downto 0) => carry_chain_12(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[3].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[40].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_156(3),
      CO(3 downto 0) => carry_chain_160(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[40].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[41].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_160(3),
      CO(3 downto 0) => carry_chain_164(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[41].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[42].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_164(3),
      CO(3 downto 0) => carry_chain_168(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[42].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[43].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_168(3),
      CO(3 downto 0) => carry_chain_172(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[43].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[44].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_172(3),
      CO(3 downto 0) => carry_chain_176(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[44].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[45].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_176(3),
      CO(3 downto 0) => carry_chain_180(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[45].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[46].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_180(3),
      CO(3 downto 0) => carry_chain_184(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[46].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[47].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_184(3),
      CO(3 downto 0) => carry_chain_188(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[47].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[48].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_188(3),
      CO(3 downto 0) => carry_chain_192(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[48].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[49].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_192(3),
      CO(3 downto 0) => carry_chain_196(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[49].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[4].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_12(3),
      CO(3 downto 0) => carry_chain_16(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[4].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[50].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_196(3),
      CO(3 downto 0) => carry_chain_200(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[50].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[51].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_200(3),
      CO(3 downto 0) => carry_chain_204(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[51].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[52].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_204(3),
      CO(3 downto 0) => carry_chain_208(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[52].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[53].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_208(3),
      CO(3 downto 0) => carry_chain_212(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[53].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[54].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_212(3),
      CO(3 downto 0) => carry_chain_216(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[54].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[55].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_216(3),
      CO(3 downto 0) => carry_chain_220(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[55].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[56].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_220(3),
      CO(3 downto 0) => carry_chain_224(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[56].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[57].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_224(3),
      CO(3 downto 0) => carry_chain_228(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[57].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[58].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_228(3),
      CO(3 downto 0) => carry_chain_232(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[58].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[59].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_232(3),
      CO(3 downto 0) => carry_chain_236(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[59].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[5].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_16(3),
      CO(3 downto 0) => carry_chain_20(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[5].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[60].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_236(3),
      CO(3 downto 0) => carry_chain_240(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[60].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[61].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_240(3),
      CO(3 downto 0) => carry_chain_244(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[61].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[62].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_244(3),
      CO(3 downto 0) => carry_chain_248(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[62].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[63].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_248(3),
      CO(3 downto 0) => carry_chain_252(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[63].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[64].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_252(3),
      CO(3 downto 0) => carry_chain_256(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[64].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[65].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_256(3),
      CO(3 downto 0) => carry_chain_260(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[65].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[66].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_260(3),
      CO(3 downto 0) => carry_chain_264(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[66].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[67].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_264(3),
      CO(3 downto 0) => carry_chain_268(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[67].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[68].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_268(3),
      CO(3 downto 0) => carry_chain_272(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[68].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[69].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_272(3),
      CO(3 downto 0) => carry_chain_276(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[69].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[6].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_20(3),
      CO(3 downto 0) => carry_chain_24(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[6].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[70].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_276(3),
      CO(3 downto 0) => carry_chain_280(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[70].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[71].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_280(3),
      CO(3 downto 0) => carry_chain_284(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[71].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[72].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_284(3),
      CO(3 downto 0) => carry_chain_288(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[72].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[73].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_288(3),
      CO(3 downto 0) => carry_chain_292(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[73].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[74].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_292(3),
      CO(3 downto 0) => carry_chain_296(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[74].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[75].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_296(3),
      CO(3 downto 0) => carry_chain_300(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[75].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[76].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_300(3),
      CO(3 downto 0) => carry_chain_304(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[76].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[77].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_304(3),
      CO(3 downto 0) => carry_chain_308(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[77].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[78].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_308(3),
      CO(3 downto 0) => carry_chain_312(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[78].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[79].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_312(3),
      CO(3 downto 0) => carry_chain_316(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[79].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[7].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_24(3),
      CO(3 downto 0) => carry_chain_28(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[7].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[8].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_28(3),
      CO(3 downto 0) => carry_chain_32(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[8].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
\tdl_gen[9].carry_x\: unisim.vcomponents.CARRY4
     port map (
      CI => carry_chain_32(3),
      CO(3 downto 0) => carry_chain_36(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tdl_gen[9].carry_x_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"1111"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tdc is
  port (
    pre_trig : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    JD_OBUF : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    \q_reg[0]\ : in STD_LOGIC
  );
end tdc;

architecture STRUCTURE of tdc is
  signal count_reg : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal crs_cnt_n_4 : STD_LOGIC;
  signal pre_tdl_n_0 : STD_LOGIC;
  signal pre_tdl_n_1 : STD_LOGIC;
  signal pre_tdl_n_10 : STD_LOGIC;
  signal pre_tdl_n_100 : STD_LOGIC;
  signal pre_tdl_n_101 : STD_LOGIC;
  signal pre_tdl_n_102 : STD_LOGIC;
  signal pre_tdl_n_103 : STD_LOGIC;
  signal pre_tdl_n_104 : STD_LOGIC;
  signal pre_tdl_n_105 : STD_LOGIC;
  signal pre_tdl_n_106 : STD_LOGIC;
  signal pre_tdl_n_107 : STD_LOGIC;
  signal pre_tdl_n_108 : STD_LOGIC;
  signal pre_tdl_n_109 : STD_LOGIC;
  signal pre_tdl_n_11 : STD_LOGIC;
  signal pre_tdl_n_110 : STD_LOGIC;
  signal pre_tdl_n_111 : STD_LOGIC;
  signal pre_tdl_n_112 : STD_LOGIC;
  signal pre_tdl_n_113 : STD_LOGIC;
  signal pre_tdl_n_114 : STD_LOGIC;
  signal pre_tdl_n_115 : STD_LOGIC;
  signal pre_tdl_n_116 : STD_LOGIC;
  signal pre_tdl_n_117 : STD_LOGIC;
  signal pre_tdl_n_118 : STD_LOGIC;
  signal pre_tdl_n_119 : STD_LOGIC;
  signal pre_tdl_n_12 : STD_LOGIC;
  signal pre_tdl_n_120 : STD_LOGIC;
  signal pre_tdl_n_121 : STD_LOGIC;
  signal pre_tdl_n_122 : STD_LOGIC;
  signal pre_tdl_n_123 : STD_LOGIC;
  signal pre_tdl_n_124 : STD_LOGIC;
  signal pre_tdl_n_125 : STD_LOGIC;
  signal pre_tdl_n_126 : STD_LOGIC;
  signal pre_tdl_n_127 : STD_LOGIC;
  signal pre_tdl_n_128 : STD_LOGIC;
  signal pre_tdl_n_129 : STD_LOGIC;
  signal pre_tdl_n_13 : STD_LOGIC;
  signal pre_tdl_n_130 : STD_LOGIC;
  signal pre_tdl_n_131 : STD_LOGIC;
  signal pre_tdl_n_132 : STD_LOGIC;
  signal pre_tdl_n_133 : STD_LOGIC;
  signal pre_tdl_n_134 : STD_LOGIC;
  signal pre_tdl_n_135 : STD_LOGIC;
  signal pre_tdl_n_136 : STD_LOGIC;
  signal pre_tdl_n_137 : STD_LOGIC;
  signal pre_tdl_n_138 : STD_LOGIC;
  signal pre_tdl_n_139 : STD_LOGIC;
  signal pre_tdl_n_14 : STD_LOGIC;
  signal pre_tdl_n_140 : STD_LOGIC;
  signal pre_tdl_n_141 : STD_LOGIC;
  signal pre_tdl_n_142 : STD_LOGIC;
  signal pre_tdl_n_143 : STD_LOGIC;
  signal pre_tdl_n_144 : STD_LOGIC;
  signal pre_tdl_n_145 : STD_LOGIC;
  signal pre_tdl_n_146 : STD_LOGIC;
  signal pre_tdl_n_147 : STD_LOGIC;
  signal pre_tdl_n_148 : STD_LOGIC;
  signal pre_tdl_n_149 : STD_LOGIC;
  signal pre_tdl_n_15 : STD_LOGIC;
  signal pre_tdl_n_150 : STD_LOGIC;
  signal pre_tdl_n_151 : STD_LOGIC;
  signal pre_tdl_n_152 : STD_LOGIC;
  signal pre_tdl_n_153 : STD_LOGIC;
  signal pre_tdl_n_154 : STD_LOGIC;
  signal pre_tdl_n_155 : STD_LOGIC;
  signal pre_tdl_n_156 : STD_LOGIC;
  signal pre_tdl_n_157 : STD_LOGIC;
  signal pre_tdl_n_158 : STD_LOGIC;
  signal pre_tdl_n_159 : STD_LOGIC;
  signal pre_tdl_n_16 : STD_LOGIC;
  signal pre_tdl_n_160 : STD_LOGIC;
  signal pre_tdl_n_161 : STD_LOGIC;
  signal pre_tdl_n_162 : STD_LOGIC;
  signal pre_tdl_n_163 : STD_LOGIC;
  signal pre_tdl_n_164 : STD_LOGIC;
  signal pre_tdl_n_165 : STD_LOGIC;
  signal pre_tdl_n_166 : STD_LOGIC;
  signal pre_tdl_n_167 : STD_LOGIC;
  signal pre_tdl_n_168 : STD_LOGIC;
  signal pre_tdl_n_169 : STD_LOGIC;
  signal pre_tdl_n_17 : STD_LOGIC;
  signal pre_tdl_n_170 : STD_LOGIC;
  signal pre_tdl_n_171 : STD_LOGIC;
  signal pre_tdl_n_172 : STD_LOGIC;
  signal pre_tdl_n_173 : STD_LOGIC;
  signal pre_tdl_n_174 : STD_LOGIC;
  signal pre_tdl_n_175 : STD_LOGIC;
  signal pre_tdl_n_176 : STD_LOGIC;
  signal pre_tdl_n_177 : STD_LOGIC;
  signal pre_tdl_n_178 : STD_LOGIC;
  signal pre_tdl_n_179 : STD_LOGIC;
  signal pre_tdl_n_18 : STD_LOGIC;
  signal pre_tdl_n_180 : STD_LOGIC;
  signal pre_tdl_n_181 : STD_LOGIC;
  signal pre_tdl_n_182 : STD_LOGIC;
  signal pre_tdl_n_183 : STD_LOGIC;
  signal pre_tdl_n_184 : STD_LOGIC;
  signal pre_tdl_n_185 : STD_LOGIC;
  signal pre_tdl_n_186 : STD_LOGIC;
  signal pre_tdl_n_187 : STD_LOGIC;
  signal pre_tdl_n_188 : STD_LOGIC;
  signal pre_tdl_n_189 : STD_LOGIC;
  signal pre_tdl_n_19 : STD_LOGIC;
  signal pre_tdl_n_190 : STD_LOGIC;
  signal pre_tdl_n_191 : STD_LOGIC;
  signal pre_tdl_n_192 : STD_LOGIC;
  signal pre_tdl_n_193 : STD_LOGIC;
  signal pre_tdl_n_194 : STD_LOGIC;
  signal pre_tdl_n_195 : STD_LOGIC;
  signal pre_tdl_n_196 : STD_LOGIC;
  signal pre_tdl_n_197 : STD_LOGIC;
  signal pre_tdl_n_198 : STD_LOGIC;
  signal pre_tdl_n_199 : STD_LOGIC;
  signal pre_tdl_n_2 : STD_LOGIC;
  signal pre_tdl_n_20 : STD_LOGIC;
  signal pre_tdl_n_200 : STD_LOGIC;
  signal pre_tdl_n_201 : STD_LOGIC;
  signal pre_tdl_n_202 : STD_LOGIC;
  signal pre_tdl_n_203 : STD_LOGIC;
  signal pre_tdl_n_204 : STD_LOGIC;
  signal pre_tdl_n_205 : STD_LOGIC;
  signal pre_tdl_n_206 : STD_LOGIC;
  signal pre_tdl_n_207 : STD_LOGIC;
  signal pre_tdl_n_208 : STD_LOGIC;
  signal pre_tdl_n_209 : STD_LOGIC;
  signal pre_tdl_n_21 : STD_LOGIC;
  signal pre_tdl_n_210 : STD_LOGIC;
  signal pre_tdl_n_211 : STD_LOGIC;
  signal pre_tdl_n_212 : STD_LOGIC;
  signal pre_tdl_n_213 : STD_LOGIC;
  signal pre_tdl_n_214 : STD_LOGIC;
  signal pre_tdl_n_215 : STD_LOGIC;
  signal pre_tdl_n_216 : STD_LOGIC;
  signal pre_tdl_n_217 : STD_LOGIC;
  signal pre_tdl_n_218 : STD_LOGIC;
  signal pre_tdl_n_219 : STD_LOGIC;
  signal pre_tdl_n_22 : STD_LOGIC;
  signal pre_tdl_n_220 : STD_LOGIC;
  signal pre_tdl_n_221 : STD_LOGIC;
  signal pre_tdl_n_222 : STD_LOGIC;
  signal pre_tdl_n_223 : STD_LOGIC;
  signal pre_tdl_n_224 : STD_LOGIC;
  signal pre_tdl_n_225 : STD_LOGIC;
  signal pre_tdl_n_226 : STD_LOGIC;
  signal pre_tdl_n_227 : STD_LOGIC;
  signal pre_tdl_n_228 : STD_LOGIC;
  signal pre_tdl_n_229 : STD_LOGIC;
  signal pre_tdl_n_23 : STD_LOGIC;
  signal pre_tdl_n_230 : STD_LOGIC;
  signal pre_tdl_n_231 : STD_LOGIC;
  signal pre_tdl_n_232 : STD_LOGIC;
  signal pre_tdl_n_233 : STD_LOGIC;
  signal pre_tdl_n_234 : STD_LOGIC;
  signal pre_tdl_n_235 : STD_LOGIC;
  signal pre_tdl_n_236 : STD_LOGIC;
  signal pre_tdl_n_237 : STD_LOGIC;
  signal pre_tdl_n_238 : STD_LOGIC;
  signal pre_tdl_n_239 : STD_LOGIC;
  signal pre_tdl_n_24 : STD_LOGIC;
  signal pre_tdl_n_240 : STD_LOGIC;
  signal pre_tdl_n_241 : STD_LOGIC;
  signal pre_tdl_n_242 : STD_LOGIC;
  signal pre_tdl_n_243 : STD_LOGIC;
  signal pre_tdl_n_244 : STD_LOGIC;
  signal pre_tdl_n_245 : STD_LOGIC;
  signal pre_tdl_n_246 : STD_LOGIC;
  signal pre_tdl_n_247 : STD_LOGIC;
  signal pre_tdl_n_248 : STD_LOGIC;
  signal pre_tdl_n_249 : STD_LOGIC;
  signal pre_tdl_n_25 : STD_LOGIC;
  signal pre_tdl_n_250 : STD_LOGIC;
  signal pre_tdl_n_251 : STD_LOGIC;
  signal pre_tdl_n_252 : STD_LOGIC;
  signal pre_tdl_n_253 : STD_LOGIC;
  signal pre_tdl_n_254 : STD_LOGIC;
  signal pre_tdl_n_255 : STD_LOGIC;
  signal pre_tdl_n_256 : STD_LOGIC;
  signal pre_tdl_n_257 : STD_LOGIC;
  signal pre_tdl_n_258 : STD_LOGIC;
  signal pre_tdl_n_259 : STD_LOGIC;
  signal pre_tdl_n_26 : STD_LOGIC;
  signal pre_tdl_n_260 : STD_LOGIC;
  signal pre_tdl_n_261 : STD_LOGIC;
  signal pre_tdl_n_262 : STD_LOGIC;
  signal pre_tdl_n_263 : STD_LOGIC;
  signal pre_tdl_n_264 : STD_LOGIC;
  signal pre_tdl_n_265 : STD_LOGIC;
  signal pre_tdl_n_266 : STD_LOGIC;
  signal pre_tdl_n_267 : STD_LOGIC;
  signal pre_tdl_n_268 : STD_LOGIC;
  signal pre_tdl_n_269 : STD_LOGIC;
  signal pre_tdl_n_27 : STD_LOGIC;
  signal pre_tdl_n_270 : STD_LOGIC;
  signal pre_tdl_n_271 : STD_LOGIC;
  signal pre_tdl_n_272 : STD_LOGIC;
  signal pre_tdl_n_273 : STD_LOGIC;
  signal pre_tdl_n_274 : STD_LOGIC;
  signal pre_tdl_n_275 : STD_LOGIC;
  signal pre_tdl_n_276 : STD_LOGIC;
  signal pre_tdl_n_277 : STD_LOGIC;
  signal pre_tdl_n_278 : STD_LOGIC;
  signal pre_tdl_n_279 : STD_LOGIC;
  signal pre_tdl_n_28 : STD_LOGIC;
  signal pre_tdl_n_280 : STD_LOGIC;
  signal pre_tdl_n_281 : STD_LOGIC;
  signal pre_tdl_n_282 : STD_LOGIC;
  signal pre_tdl_n_283 : STD_LOGIC;
  signal pre_tdl_n_284 : STD_LOGIC;
  signal pre_tdl_n_285 : STD_LOGIC;
  signal pre_tdl_n_286 : STD_LOGIC;
  signal pre_tdl_n_287 : STD_LOGIC;
  signal pre_tdl_n_288 : STD_LOGIC;
  signal pre_tdl_n_289 : STD_LOGIC;
  signal pre_tdl_n_29 : STD_LOGIC;
  signal pre_tdl_n_290 : STD_LOGIC;
  signal pre_tdl_n_291 : STD_LOGIC;
  signal pre_tdl_n_292 : STD_LOGIC;
  signal pre_tdl_n_293 : STD_LOGIC;
  signal pre_tdl_n_294 : STD_LOGIC;
  signal pre_tdl_n_295 : STD_LOGIC;
  signal pre_tdl_n_296 : STD_LOGIC;
  signal pre_tdl_n_297 : STD_LOGIC;
  signal pre_tdl_n_298 : STD_LOGIC;
  signal pre_tdl_n_299 : STD_LOGIC;
  signal pre_tdl_n_3 : STD_LOGIC;
  signal pre_tdl_n_30 : STD_LOGIC;
  signal pre_tdl_n_300 : STD_LOGIC;
  signal pre_tdl_n_301 : STD_LOGIC;
  signal pre_tdl_n_302 : STD_LOGIC;
  signal pre_tdl_n_303 : STD_LOGIC;
  signal pre_tdl_n_304 : STD_LOGIC;
  signal pre_tdl_n_305 : STD_LOGIC;
  signal pre_tdl_n_306 : STD_LOGIC;
  signal pre_tdl_n_307 : STD_LOGIC;
  signal pre_tdl_n_308 : STD_LOGIC;
  signal pre_tdl_n_309 : STD_LOGIC;
  signal pre_tdl_n_31 : STD_LOGIC;
  signal pre_tdl_n_310 : STD_LOGIC;
  signal pre_tdl_n_311 : STD_LOGIC;
  signal pre_tdl_n_312 : STD_LOGIC;
  signal pre_tdl_n_313 : STD_LOGIC;
  signal pre_tdl_n_314 : STD_LOGIC;
  signal pre_tdl_n_315 : STD_LOGIC;
  signal pre_tdl_n_316 : STD_LOGIC;
  signal pre_tdl_n_317 : STD_LOGIC;
  signal pre_tdl_n_318 : STD_LOGIC;
  signal pre_tdl_n_319 : STD_LOGIC;
  signal pre_tdl_n_32 : STD_LOGIC;
  signal pre_tdl_n_33 : STD_LOGIC;
  signal pre_tdl_n_34 : STD_LOGIC;
  signal pre_tdl_n_35 : STD_LOGIC;
  signal pre_tdl_n_36 : STD_LOGIC;
  signal pre_tdl_n_37 : STD_LOGIC;
  signal pre_tdl_n_38 : STD_LOGIC;
  signal pre_tdl_n_39 : STD_LOGIC;
  signal pre_tdl_n_4 : STD_LOGIC;
  signal pre_tdl_n_40 : STD_LOGIC;
  signal pre_tdl_n_41 : STD_LOGIC;
  signal pre_tdl_n_42 : STD_LOGIC;
  signal pre_tdl_n_43 : STD_LOGIC;
  signal pre_tdl_n_44 : STD_LOGIC;
  signal pre_tdl_n_45 : STD_LOGIC;
  signal pre_tdl_n_46 : STD_LOGIC;
  signal pre_tdl_n_47 : STD_LOGIC;
  signal pre_tdl_n_48 : STD_LOGIC;
  signal pre_tdl_n_49 : STD_LOGIC;
  signal pre_tdl_n_5 : STD_LOGIC;
  signal pre_tdl_n_50 : STD_LOGIC;
  signal pre_tdl_n_51 : STD_LOGIC;
  signal pre_tdl_n_52 : STD_LOGIC;
  signal pre_tdl_n_53 : STD_LOGIC;
  signal pre_tdl_n_54 : STD_LOGIC;
  signal pre_tdl_n_55 : STD_LOGIC;
  signal pre_tdl_n_56 : STD_LOGIC;
  signal pre_tdl_n_57 : STD_LOGIC;
  signal pre_tdl_n_58 : STD_LOGIC;
  signal pre_tdl_n_59 : STD_LOGIC;
  signal pre_tdl_n_6 : STD_LOGIC;
  signal pre_tdl_n_60 : STD_LOGIC;
  signal pre_tdl_n_61 : STD_LOGIC;
  signal pre_tdl_n_62 : STD_LOGIC;
  signal pre_tdl_n_63 : STD_LOGIC;
  signal pre_tdl_n_64 : STD_LOGIC;
  signal pre_tdl_n_65 : STD_LOGIC;
  signal pre_tdl_n_66 : STD_LOGIC;
  signal pre_tdl_n_67 : STD_LOGIC;
  signal pre_tdl_n_68 : STD_LOGIC;
  signal pre_tdl_n_69 : STD_LOGIC;
  signal pre_tdl_n_7 : STD_LOGIC;
  signal pre_tdl_n_70 : STD_LOGIC;
  signal pre_tdl_n_71 : STD_LOGIC;
  signal pre_tdl_n_72 : STD_LOGIC;
  signal pre_tdl_n_73 : STD_LOGIC;
  signal pre_tdl_n_74 : STD_LOGIC;
  signal pre_tdl_n_75 : STD_LOGIC;
  signal pre_tdl_n_76 : STD_LOGIC;
  signal pre_tdl_n_77 : STD_LOGIC;
  signal pre_tdl_n_78 : STD_LOGIC;
  signal pre_tdl_n_79 : STD_LOGIC;
  signal pre_tdl_n_8 : STD_LOGIC;
  signal pre_tdl_n_80 : STD_LOGIC;
  signal pre_tdl_n_81 : STD_LOGIC;
  signal pre_tdl_n_82 : STD_LOGIC;
  signal pre_tdl_n_83 : STD_LOGIC;
  signal pre_tdl_n_84 : STD_LOGIC;
  signal pre_tdl_n_85 : STD_LOGIC;
  signal pre_tdl_n_86 : STD_LOGIC;
  signal pre_tdl_n_87 : STD_LOGIC;
  signal pre_tdl_n_88 : STD_LOGIC;
  signal pre_tdl_n_89 : STD_LOGIC;
  signal pre_tdl_n_9 : STD_LOGIC;
  signal pre_tdl_n_90 : STD_LOGIC;
  signal pre_tdl_n_91 : STD_LOGIC;
  signal pre_tdl_n_92 : STD_LOGIC;
  signal pre_tdl_n_93 : STD_LOGIC;
  signal pre_tdl_n_94 : STD_LOGIC;
  signal pre_tdl_n_95 : STD_LOGIC;
  signal pre_tdl_n_96 : STD_LOGIC;
  signal pre_tdl_n_97 : STD_LOGIC;
  signal pre_tdl_n_98 : STD_LOGIC;
  signal pre_tdl_n_99 : STD_LOGIC;
begin
calc_unit: entity work.calc_unit
     port map (
      CLK => CLK,
      D(23 downto 0) => D(23 downto 0),
      JD_OBUF(1 downto 0) => JD_OBUF(1 downto 0),
      Q(3 downto 0) => count_reg(3 downto 0),
      \cnt_offset_reg[0]_0\ => crs_cnt_n_4,
      pre_trig => pre_trig,
      \q_reg[0]\ => \q_reg[0]\,
      \q_reg[319]\(319) => pre_tdl_n_0,
      \q_reg[319]\(318) => pre_tdl_n_1,
      \q_reg[319]\(317) => pre_tdl_n_2,
      \q_reg[319]\(316) => pre_tdl_n_3,
      \q_reg[319]\(315) => pre_tdl_n_4,
      \q_reg[319]\(314) => pre_tdl_n_5,
      \q_reg[319]\(313) => pre_tdl_n_6,
      \q_reg[319]\(312) => pre_tdl_n_7,
      \q_reg[319]\(311) => pre_tdl_n_8,
      \q_reg[319]\(310) => pre_tdl_n_9,
      \q_reg[319]\(309) => pre_tdl_n_10,
      \q_reg[319]\(308) => pre_tdl_n_11,
      \q_reg[319]\(307) => pre_tdl_n_12,
      \q_reg[319]\(306) => pre_tdl_n_13,
      \q_reg[319]\(305) => pre_tdl_n_14,
      \q_reg[319]\(304) => pre_tdl_n_15,
      \q_reg[319]\(303) => pre_tdl_n_16,
      \q_reg[319]\(302) => pre_tdl_n_17,
      \q_reg[319]\(301) => pre_tdl_n_18,
      \q_reg[319]\(300) => pre_tdl_n_19,
      \q_reg[319]\(299) => pre_tdl_n_20,
      \q_reg[319]\(298) => pre_tdl_n_21,
      \q_reg[319]\(297) => pre_tdl_n_22,
      \q_reg[319]\(296) => pre_tdl_n_23,
      \q_reg[319]\(295) => pre_tdl_n_24,
      \q_reg[319]\(294) => pre_tdl_n_25,
      \q_reg[319]\(293) => pre_tdl_n_26,
      \q_reg[319]\(292) => pre_tdl_n_27,
      \q_reg[319]\(291) => pre_tdl_n_28,
      \q_reg[319]\(290) => pre_tdl_n_29,
      \q_reg[319]\(289) => pre_tdl_n_30,
      \q_reg[319]\(288) => pre_tdl_n_31,
      \q_reg[319]\(287) => pre_tdl_n_32,
      \q_reg[319]\(286) => pre_tdl_n_33,
      \q_reg[319]\(285) => pre_tdl_n_34,
      \q_reg[319]\(284) => pre_tdl_n_35,
      \q_reg[319]\(283) => pre_tdl_n_36,
      \q_reg[319]\(282) => pre_tdl_n_37,
      \q_reg[319]\(281) => pre_tdl_n_38,
      \q_reg[319]\(280) => pre_tdl_n_39,
      \q_reg[319]\(279) => pre_tdl_n_40,
      \q_reg[319]\(278) => pre_tdl_n_41,
      \q_reg[319]\(277) => pre_tdl_n_42,
      \q_reg[319]\(276) => pre_tdl_n_43,
      \q_reg[319]\(275) => pre_tdl_n_44,
      \q_reg[319]\(274) => pre_tdl_n_45,
      \q_reg[319]\(273) => pre_tdl_n_46,
      \q_reg[319]\(272) => pre_tdl_n_47,
      \q_reg[319]\(271) => pre_tdl_n_48,
      \q_reg[319]\(270) => pre_tdl_n_49,
      \q_reg[319]\(269) => pre_tdl_n_50,
      \q_reg[319]\(268) => pre_tdl_n_51,
      \q_reg[319]\(267) => pre_tdl_n_52,
      \q_reg[319]\(266) => pre_tdl_n_53,
      \q_reg[319]\(265) => pre_tdl_n_54,
      \q_reg[319]\(264) => pre_tdl_n_55,
      \q_reg[319]\(263) => pre_tdl_n_56,
      \q_reg[319]\(262) => pre_tdl_n_57,
      \q_reg[319]\(261) => pre_tdl_n_58,
      \q_reg[319]\(260) => pre_tdl_n_59,
      \q_reg[319]\(259) => pre_tdl_n_60,
      \q_reg[319]\(258) => pre_tdl_n_61,
      \q_reg[319]\(257) => pre_tdl_n_62,
      \q_reg[319]\(256) => pre_tdl_n_63,
      \q_reg[319]\(255) => pre_tdl_n_64,
      \q_reg[319]\(254) => pre_tdl_n_65,
      \q_reg[319]\(253) => pre_tdl_n_66,
      \q_reg[319]\(252) => pre_tdl_n_67,
      \q_reg[319]\(251) => pre_tdl_n_68,
      \q_reg[319]\(250) => pre_tdl_n_69,
      \q_reg[319]\(249) => pre_tdl_n_70,
      \q_reg[319]\(248) => pre_tdl_n_71,
      \q_reg[319]\(247) => pre_tdl_n_72,
      \q_reg[319]\(246) => pre_tdl_n_73,
      \q_reg[319]\(245) => pre_tdl_n_74,
      \q_reg[319]\(244) => pre_tdl_n_75,
      \q_reg[319]\(243) => pre_tdl_n_76,
      \q_reg[319]\(242) => pre_tdl_n_77,
      \q_reg[319]\(241) => pre_tdl_n_78,
      \q_reg[319]\(240) => pre_tdl_n_79,
      \q_reg[319]\(239) => pre_tdl_n_80,
      \q_reg[319]\(238) => pre_tdl_n_81,
      \q_reg[319]\(237) => pre_tdl_n_82,
      \q_reg[319]\(236) => pre_tdl_n_83,
      \q_reg[319]\(235) => pre_tdl_n_84,
      \q_reg[319]\(234) => pre_tdl_n_85,
      \q_reg[319]\(233) => pre_tdl_n_86,
      \q_reg[319]\(232) => pre_tdl_n_87,
      \q_reg[319]\(231) => pre_tdl_n_88,
      \q_reg[319]\(230) => pre_tdl_n_89,
      \q_reg[319]\(229) => pre_tdl_n_90,
      \q_reg[319]\(228) => pre_tdl_n_91,
      \q_reg[319]\(227) => pre_tdl_n_92,
      \q_reg[319]\(226) => pre_tdl_n_93,
      \q_reg[319]\(225) => pre_tdl_n_94,
      \q_reg[319]\(224) => pre_tdl_n_95,
      \q_reg[319]\(223) => pre_tdl_n_96,
      \q_reg[319]\(222) => pre_tdl_n_97,
      \q_reg[319]\(221) => pre_tdl_n_98,
      \q_reg[319]\(220) => pre_tdl_n_99,
      \q_reg[319]\(219) => pre_tdl_n_100,
      \q_reg[319]\(218) => pre_tdl_n_101,
      \q_reg[319]\(217) => pre_tdl_n_102,
      \q_reg[319]\(216) => pre_tdl_n_103,
      \q_reg[319]\(215) => pre_tdl_n_104,
      \q_reg[319]\(214) => pre_tdl_n_105,
      \q_reg[319]\(213) => pre_tdl_n_106,
      \q_reg[319]\(212) => pre_tdl_n_107,
      \q_reg[319]\(211) => pre_tdl_n_108,
      \q_reg[319]\(210) => pre_tdl_n_109,
      \q_reg[319]\(209) => pre_tdl_n_110,
      \q_reg[319]\(208) => pre_tdl_n_111,
      \q_reg[319]\(207) => pre_tdl_n_112,
      \q_reg[319]\(206) => pre_tdl_n_113,
      \q_reg[319]\(205) => pre_tdl_n_114,
      \q_reg[319]\(204) => pre_tdl_n_115,
      \q_reg[319]\(203) => pre_tdl_n_116,
      \q_reg[319]\(202) => pre_tdl_n_117,
      \q_reg[319]\(201) => pre_tdl_n_118,
      \q_reg[319]\(200) => pre_tdl_n_119,
      \q_reg[319]\(199) => pre_tdl_n_120,
      \q_reg[319]\(198) => pre_tdl_n_121,
      \q_reg[319]\(197) => pre_tdl_n_122,
      \q_reg[319]\(196) => pre_tdl_n_123,
      \q_reg[319]\(195) => pre_tdl_n_124,
      \q_reg[319]\(194) => pre_tdl_n_125,
      \q_reg[319]\(193) => pre_tdl_n_126,
      \q_reg[319]\(192) => pre_tdl_n_127,
      \q_reg[319]\(191) => pre_tdl_n_128,
      \q_reg[319]\(190) => pre_tdl_n_129,
      \q_reg[319]\(189) => pre_tdl_n_130,
      \q_reg[319]\(188) => pre_tdl_n_131,
      \q_reg[319]\(187) => pre_tdl_n_132,
      \q_reg[319]\(186) => pre_tdl_n_133,
      \q_reg[319]\(185) => pre_tdl_n_134,
      \q_reg[319]\(184) => pre_tdl_n_135,
      \q_reg[319]\(183) => pre_tdl_n_136,
      \q_reg[319]\(182) => pre_tdl_n_137,
      \q_reg[319]\(181) => pre_tdl_n_138,
      \q_reg[319]\(180) => pre_tdl_n_139,
      \q_reg[319]\(179) => pre_tdl_n_140,
      \q_reg[319]\(178) => pre_tdl_n_141,
      \q_reg[319]\(177) => pre_tdl_n_142,
      \q_reg[319]\(176) => pre_tdl_n_143,
      \q_reg[319]\(175) => pre_tdl_n_144,
      \q_reg[319]\(174) => pre_tdl_n_145,
      \q_reg[319]\(173) => pre_tdl_n_146,
      \q_reg[319]\(172) => pre_tdl_n_147,
      \q_reg[319]\(171) => pre_tdl_n_148,
      \q_reg[319]\(170) => pre_tdl_n_149,
      \q_reg[319]\(169) => pre_tdl_n_150,
      \q_reg[319]\(168) => pre_tdl_n_151,
      \q_reg[319]\(167) => pre_tdl_n_152,
      \q_reg[319]\(166) => pre_tdl_n_153,
      \q_reg[319]\(165) => pre_tdl_n_154,
      \q_reg[319]\(164) => pre_tdl_n_155,
      \q_reg[319]\(163) => pre_tdl_n_156,
      \q_reg[319]\(162) => pre_tdl_n_157,
      \q_reg[319]\(161) => pre_tdl_n_158,
      \q_reg[319]\(160) => pre_tdl_n_159,
      \q_reg[319]\(159) => pre_tdl_n_160,
      \q_reg[319]\(158) => pre_tdl_n_161,
      \q_reg[319]\(157) => pre_tdl_n_162,
      \q_reg[319]\(156) => pre_tdl_n_163,
      \q_reg[319]\(155) => pre_tdl_n_164,
      \q_reg[319]\(154) => pre_tdl_n_165,
      \q_reg[319]\(153) => pre_tdl_n_166,
      \q_reg[319]\(152) => pre_tdl_n_167,
      \q_reg[319]\(151) => pre_tdl_n_168,
      \q_reg[319]\(150) => pre_tdl_n_169,
      \q_reg[319]\(149) => pre_tdl_n_170,
      \q_reg[319]\(148) => pre_tdl_n_171,
      \q_reg[319]\(147) => pre_tdl_n_172,
      \q_reg[319]\(146) => pre_tdl_n_173,
      \q_reg[319]\(145) => pre_tdl_n_174,
      \q_reg[319]\(144) => pre_tdl_n_175,
      \q_reg[319]\(143) => pre_tdl_n_176,
      \q_reg[319]\(142) => pre_tdl_n_177,
      \q_reg[319]\(141) => pre_tdl_n_178,
      \q_reg[319]\(140) => pre_tdl_n_179,
      \q_reg[319]\(139) => pre_tdl_n_180,
      \q_reg[319]\(138) => pre_tdl_n_181,
      \q_reg[319]\(137) => pre_tdl_n_182,
      \q_reg[319]\(136) => pre_tdl_n_183,
      \q_reg[319]\(135) => pre_tdl_n_184,
      \q_reg[319]\(134) => pre_tdl_n_185,
      \q_reg[319]\(133) => pre_tdl_n_186,
      \q_reg[319]\(132) => pre_tdl_n_187,
      \q_reg[319]\(131) => pre_tdl_n_188,
      \q_reg[319]\(130) => pre_tdl_n_189,
      \q_reg[319]\(129) => pre_tdl_n_190,
      \q_reg[319]\(128) => pre_tdl_n_191,
      \q_reg[319]\(127) => pre_tdl_n_192,
      \q_reg[319]\(126) => pre_tdl_n_193,
      \q_reg[319]\(125) => pre_tdl_n_194,
      \q_reg[319]\(124) => pre_tdl_n_195,
      \q_reg[319]\(123) => pre_tdl_n_196,
      \q_reg[319]\(122) => pre_tdl_n_197,
      \q_reg[319]\(121) => pre_tdl_n_198,
      \q_reg[319]\(120) => pre_tdl_n_199,
      \q_reg[319]\(119) => pre_tdl_n_200,
      \q_reg[319]\(118) => pre_tdl_n_201,
      \q_reg[319]\(117) => pre_tdl_n_202,
      \q_reg[319]\(116) => pre_tdl_n_203,
      \q_reg[319]\(115) => pre_tdl_n_204,
      \q_reg[319]\(114) => pre_tdl_n_205,
      \q_reg[319]\(113) => pre_tdl_n_206,
      \q_reg[319]\(112) => pre_tdl_n_207,
      \q_reg[319]\(111) => pre_tdl_n_208,
      \q_reg[319]\(110) => pre_tdl_n_209,
      \q_reg[319]\(109) => pre_tdl_n_210,
      \q_reg[319]\(108) => pre_tdl_n_211,
      \q_reg[319]\(107) => pre_tdl_n_212,
      \q_reg[319]\(106) => pre_tdl_n_213,
      \q_reg[319]\(105) => pre_tdl_n_214,
      \q_reg[319]\(104) => pre_tdl_n_215,
      \q_reg[319]\(103) => pre_tdl_n_216,
      \q_reg[319]\(102) => pre_tdl_n_217,
      \q_reg[319]\(101) => pre_tdl_n_218,
      \q_reg[319]\(100) => pre_tdl_n_219,
      \q_reg[319]\(99) => pre_tdl_n_220,
      \q_reg[319]\(98) => pre_tdl_n_221,
      \q_reg[319]\(97) => pre_tdl_n_222,
      \q_reg[319]\(96) => pre_tdl_n_223,
      \q_reg[319]\(95) => pre_tdl_n_224,
      \q_reg[319]\(94) => pre_tdl_n_225,
      \q_reg[319]\(93) => pre_tdl_n_226,
      \q_reg[319]\(92) => pre_tdl_n_227,
      \q_reg[319]\(91) => pre_tdl_n_228,
      \q_reg[319]\(90) => pre_tdl_n_229,
      \q_reg[319]\(89) => pre_tdl_n_230,
      \q_reg[319]\(88) => pre_tdl_n_231,
      \q_reg[319]\(87) => pre_tdl_n_232,
      \q_reg[319]\(86) => pre_tdl_n_233,
      \q_reg[319]\(85) => pre_tdl_n_234,
      \q_reg[319]\(84) => pre_tdl_n_235,
      \q_reg[319]\(83) => pre_tdl_n_236,
      \q_reg[319]\(82) => pre_tdl_n_237,
      \q_reg[319]\(81) => pre_tdl_n_238,
      \q_reg[319]\(80) => pre_tdl_n_239,
      \q_reg[319]\(79) => pre_tdl_n_240,
      \q_reg[319]\(78) => pre_tdl_n_241,
      \q_reg[319]\(77) => pre_tdl_n_242,
      \q_reg[319]\(76) => pre_tdl_n_243,
      \q_reg[319]\(75) => pre_tdl_n_244,
      \q_reg[319]\(74) => pre_tdl_n_245,
      \q_reg[319]\(73) => pre_tdl_n_246,
      \q_reg[319]\(72) => pre_tdl_n_247,
      \q_reg[319]\(71) => pre_tdl_n_248,
      \q_reg[319]\(70) => pre_tdl_n_249,
      \q_reg[319]\(69) => pre_tdl_n_250,
      \q_reg[319]\(68) => pre_tdl_n_251,
      \q_reg[319]\(67) => pre_tdl_n_252,
      \q_reg[319]\(66) => pre_tdl_n_253,
      \q_reg[319]\(65) => pre_tdl_n_254,
      \q_reg[319]\(64) => pre_tdl_n_255,
      \q_reg[319]\(63) => pre_tdl_n_256,
      \q_reg[319]\(62) => pre_tdl_n_257,
      \q_reg[319]\(61) => pre_tdl_n_258,
      \q_reg[319]\(60) => pre_tdl_n_259,
      \q_reg[319]\(59) => pre_tdl_n_260,
      \q_reg[319]\(58) => pre_tdl_n_261,
      \q_reg[319]\(57) => pre_tdl_n_262,
      \q_reg[319]\(56) => pre_tdl_n_263,
      \q_reg[319]\(55) => pre_tdl_n_264,
      \q_reg[319]\(54) => pre_tdl_n_265,
      \q_reg[319]\(53) => pre_tdl_n_266,
      \q_reg[319]\(52) => pre_tdl_n_267,
      \q_reg[319]\(51) => pre_tdl_n_268,
      \q_reg[319]\(50) => pre_tdl_n_269,
      \q_reg[319]\(49) => pre_tdl_n_270,
      \q_reg[319]\(48) => pre_tdl_n_271,
      \q_reg[319]\(47) => pre_tdl_n_272,
      \q_reg[319]\(46) => pre_tdl_n_273,
      \q_reg[319]\(45) => pre_tdl_n_274,
      \q_reg[319]\(44) => pre_tdl_n_275,
      \q_reg[319]\(43) => pre_tdl_n_276,
      \q_reg[319]\(42) => pre_tdl_n_277,
      \q_reg[319]\(41) => pre_tdl_n_278,
      \q_reg[319]\(40) => pre_tdl_n_279,
      \q_reg[319]\(39) => pre_tdl_n_280,
      \q_reg[319]\(38) => pre_tdl_n_281,
      \q_reg[319]\(37) => pre_tdl_n_282,
      \q_reg[319]\(36) => pre_tdl_n_283,
      \q_reg[319]\(35) => pre_tdl_n_284,
      \q_reg[319]\(34) => pre_tdl_n_285,
      \q_reg[319]\(33) => pre_tdl_n_286,
      \q_reg[319]\(32) => pre_tdl_n_287,
      \q_reg[319]\(31) => pre_tdl_n_288,
      \q_reg[319]\(30) => pre_tdl_n_289,
      \q_reg[319]\(29) => pre_tdl_n_290,
      \q_reg[319]\(28) => pre_tdl_n_291,
      \q_reg[319]\(27) => pre_tdl_n_292,
      \q_reg[319]\(26) => pre_tdl_n_293,
      \q_reg[319]\(25) => pre_tdl_n_294,
      \q_reg[319]\(24) => pre_tdl_n_295,
      \q_reg[319]\(23) => pre_tdl_n_296,
      \q_reg[319]\(22) => pre_tdl_n_297,
      \q_reg[319]\(21) => pre_tdl_n_298,
      \q_reg[319]\(20) => pre_tdl_n_299,
      \q_reg[319]\(19) => pre_tdl_n_300,
      \q_reg[319]\(18) => pre_tdl_n_301,
      \q_reg[319]\(17) => pre_tdl_n_302,
      \q_reg[319]\(16) => pre_tdl_n_303,
      \q_reg[319]\(15) => pre_tdl_n_304,
      \q_reg[319]\(14) => pre_tdl_n_305,
      \q_reg[319]\(13) => pre_tdl_n_306,
      \q_reg[319]\(12) => pre_tdl_n_307,
      \q_reg[319]\(11) => pre_tdl_n_308,
      \q_reg[319]\(10) => pre_tdl_n_309,
      \q_reg[319]\(9) => pre_tdl_n_310,
      \q_reg[319]\(8) => pre_tdl_n_311,
      \q_reg[319]\(7) => pre_tdl_n_312,
      \q_reg[319]\(6) => pre_tdl_n_313,
      \q_reg[319]\(5) => pre_tdl_n_314,
      \q_reg[319]\(4) => pre_tdl_n_315,
      \q_reg[319]\(3) => pre_tdl_n_316,
      \q_reg[319]\(2) => pre_tdl_n_317,
      \q_reg[319]\(1) => pre_tdl_n_318,
      \q_reg[319]\(0) => pre_tdl_n_319
    );
crs_cnt: entity work.binary_counter
     port map (
      CLK => CLK,
      JD_OBUF(1 downto 0) => JD_OBUF(1 downto 0),
      Q(3 downto 0) => count_reg(3 downto 0),
      \count_reg[0]_0\ => crs_cnt_n_4
    );
pre_tdl: entity work.n4bit_carry_chain
     port map (
      CLK => CLK,
      JD_OBUF(0) => JD_OBUF(0),
      \q_reg[3]\(319) => pre_tdl_n_0,
      \q_reg[3]\(318) => pre_tdl_n_1,
      \q_reg[3]\(317) => pre_tdl_n_2,
      \q_reg[3]\(316) => pre_tdl_n_3,
      \q_reg[3]\(315) => pre_tdl_n_4,
      \q_reg[3]\(314) => pre_tdl_n_5,
      \q_reg[3]\(313) => pre_tdl_n_6,
      \q_reg[3]\(312) => pre_tdl_n_7,
      \q_reg[3]\(311) => pre_tdl_n_8,
      \q_reg[3]\(310) => pre_tdl_n_9,
      \q_reg[3]\(309) => pre_tdl_n_10,
      \q_reg[3]\(308) => pre_tdl_n_11,
      \q_reg[3]\(307) => pre_tdl_n_12,
      \q_reg[3]\(306) => pre_tdl_n_13,
      \q_reg[3]\(305) => pre_tdl_n_14,
      \q_reg[3]\(304) => pre_tdl_n_15,
      \q_reg[3]\(303) => pre_tdl_n_16,
      \q_reg[3]\(302) => pre_tdl_n_17,
      \q_reg[3]\(301) => pre_tdl_n_18,
      \q_reg[3]\(300) => pre_tdl_n_19,
      \q_reg[3]\(299) => pre_tdl_n_20,
      \q_reg[3]\(298) => pre_tdl_n_21,
      \q_reg[3]\(297) => pre_tdl_n_22,
      \q_reg[3]\(296) => pre_tdl_n_23,
      \q_reg[3]\(295) => pre_tdl_n_24,
      \q_reg[3]\(294) => pre_tdl_n_25,
      \q_reg[3]\(293) => pre_tdl_n_26,
      \q_reg[3]\(292) => pre_tdl_n_27,
      \q_reg[3]\(291) => pre_tdl_n_28,
      \q_reg[3]\(290) => pre_tdl_n_29,
      \q_reg[3]\(289) => pre_tdl_n_30,
      \q_reg[3]\(288) => pre_tdl_n_31,
      \q_reg[3]\(287) => pre_tdl_n_32,
      \q_reg[3]\(286) => pre_tdl_n_33,
      \q_reg[3]\(285) => pre_tdl_n_34,
      \q_reg[3]\(284) => pre_tdl_n_35,
      \q_reg[3]\(283) => pre_tdl_n_36,
      \q_reg[3]\(282) => pre_tdl_n_37,
      \q_reg[3]\(281) => pre_tdl_n_38,
      \q_reg[3]\(280) => pre_tdl_n_39,
      \q_reg[3]\(279) => pre_tdl_n_40,
      \q_reg[3]\(278) => pre_tdl_n_41,
      \q_reg[3]\(277) => pre_tdl_n_42,
      \q_reg[3]\(276) => pre_tdl_n_43,
      \q_reg[3]\(275) => pre_tdl_n_44,
      \q_reg[3]\(274) => pre_tdl_n_45,
      \q_reg[3]\(273) => pre_tdl_n_46,
      \q_reg[3]\(272) => pre_tdl_n_47,
      \q_reg[3]\(271) => pre_tdl_n_48,
      \q_reg[3]\(270) => pre_tdl_n_49,
      \q_reg[3]\(269) => pre_tdl_n_50,
      \q_reg[3]\(268) => pre_tdl_n_51,
      \q_reg[3]\(267) => pre_tdl_n_52,
      \q_reg[3]\(266) => pre_tdl_n_53,
      \q_reg[3]\(265) => pre_tdl_n_54,
      \q_reg[3]\(264) => pre_tdl_n_55,
      \q_reg[3]\(263) => pre_tdl_n_56,
      \q_reg[3]\(262) => pre_tdl_n_57,
      \q_reg[3]\(261) => pre_tdl_n_58,
      \q_reg[3]\(260) => pre_tdl_n_59,
      \q_reg[3]\(259) => pre_tdl_n_60,
      \q_reg[3]\(258) => pre_tdl_n_61,
      \q_reg[3]\(257) => pre_tdl_n_62,
      \q_reg[3]\(256) => pre_tdl_n_63,
      \q_reg[3]\(255) => pre_tdl_n_64,
      \q_reg[3]\(254) => pre_tdl_n_65,
      \q_reg[3]\(253) => pre_tdl_n_66,
      \q_reg[3]\(252) => pre_tdl_n_67,
      \q_reg[3]\(251) => pre_tdl_n_68,
      \q_reg[3]\(250) => pre_tdl_n_69,
      \q_reg[3]\(249) => pre_tdl_n_70,
      \q_reg[3]\(248) => pre_tdl_n_71,
      \q_reg[3]\(247) => pre_tdl_n_72,
      \q_reg[3]\(246) => pre_tdl_n_73,
      \q_reg[3]\(245) => pre_tdl_n_74,
      \q_reg[3]\(244) => pre_tdl_n_75,
      \q_reg[3]\(243) => pre_tdl_n_76,
      \q_reg[3]\(242) => pre_tdl_n_77,
      \q_reg[3]\(241) => pre_tdl_n_78,
      \q_reg[3]\(240) => pre_tdl_n_79,
      \q_reg[3]\(239) => pre_tdl_n_80,
      \q_reg[3]\(238) => pre_tdl_n_81,
      \q_reg[3]\(237) => pre_tdl_n_82,
      \q_reg[3]\(236) => pre_tdl_n_83,
      \q_reg[3]\(235) => pre_tdl_n_84,
      \q_reg[3]\(234) => pre_tdl_n_85,
      \q_reg[3]\(233) => pre_tdl_n_86,
      \q_reg[3]\(232) => pre_tdl_n_87,
      \q_reg[3]\(231) => pre_tdl_n_88,
      \q_reg[3]\(230) => pre_tdl_n_89,
      \q_reg[3]\(229) => pre_tdl_n_90,
      \q_reg[3]\(228) => pre_tdl_n_91,
      \q_reg[3]\(227) => pre_tdl_n_92,
      \q_reg[3]\(226) => pre_tdl_n_93,
      \q_reg[3]\(225) => pre_tdl_n_94,
      \q_reg[3]\(224) => pre_tdl_n_95,
      \q_reg[3]\(223) => pre_tdl_n_96,
      \q_reg[3]\(222) => pre_tdl_n_97,
      \q_reg[3]\(221) => pre_tdl_n_98,
      \q_reg[3]\(220) => pre_tdl_n_99,
      \q_reg[3]\(219) => pre_tdl_n_100,
      \q_reg[3]\(218) => pre_tdl_n_101,
      \q_reg[3]\(217) => pre_tdl_n_102,
      \q_reg[3]\(216) => pre_tdl_n_103,
      \q_reg[3]\(215) => pre_tdl_n_104,
      \q_reg[3]\(214) => pre_tdl_n_105,
      \q_reg[3]\(213) => pre_tdl_n_106,
      \q_reg[3]\(212) => pre_tdl_n_107,
      \q_reg[3]\(211) => pre_tdl_n_108,
      \q_reg[3]\(210) => pre_tdl_n_109,
      \q_reg[3]\(209) => pre_tdl_n_110,
      \q_reg[3]\(208) => pre_tdl_n_111,
      \q_reg[3]\(207) => pre_tdl_n_112,
      \q_reg[3]\(206) => pre_tdl_n_113,
      \q_reg[3]\(205) => pre_tdl_n_114,
      \q_reg[3]\(204) => pre_tdl_n_115,
      \q_reg[3]\(203) => pre_tdl_n_116,
      \q_reg[3]\(202) => pre_tdl_n_117,
      \q_reg[3]\(201) => pre_tdl_n_118,
      \q_reg[3]\(200) => pre_tdl_n_119,
      \q_reg[3]\(199) => pre_tdl_n_120,
      \q_reg[3]\(198) => pre_tdl_n_121,
      \q_reg[3]\(197) => pre_tdl_n_122,
      \q_reg[3]\(196) => pre_tdl_n_123,
      \q_reg[3]\(195) => pre_tdl_n_124,
      \q_reg[3]\(194) => pre_tdl_n_125,
      \q_reg[3]\(193) => pre_tdl_n_126,
      \q_reg[3]\(192) => pre_tdl_n_127,
      \q_reg[3]\(191) => pre_tdl_n_128,
      \q_reg[3]\(190) => pre_tdl_n_129,
      \q_reg[3]\(189) => pre_tdl_n_130,
      \q_reg[3]\(188) => pre_tdl_n_131,
      \q_reg[3]\(187) => pre_tdl_n_132,
      \q_reg[3]\(186) => pre_tdl_n_133,
      \q_reg[3]\(185) => pre_tdl_n_134,
      \q_reg[3]\(184) => pre_tdl_n_135,
      \q_reg[3]\(183) => pre_tdl_n_136,
      \q_reg[3]\(182) => pre_tdl_n_137,
      \q_reg[3]\(181) => pre_tdl_n_138,
      \q_reg[3]\(180) => pre_tdl_n_139,
      \q_reg[3]\(179) => pre_tdl_n_140,
      \q_reg[3]\(178) => pre_tdl_n_141,
      \q_reg[3]\(177) => pre_tdl_n_142,
      \q_reg[3]\(176) => pre_tdl_n_143,
      \q_reg[3]\(175) => pre_tdl_n_144,
      \q_reg[3]\(174) => pre_tdl_n_145,
      \q_reg[3]\(173) => pre_tdl_n_146,
      \q_reg[3]\(172) => pre_tdl_n_147,
      \q_reg[3]\(171) => pre_tdl_n_148,
      \q_reg[3]\(170) => pre_tdl_n_149,
      \q_reg[3]\(169) => pre_tdl_n_150,
      \q_reg[3]\(168) => pre_tdl_n_151,
      \q_reg[3]\(167) => pre_tdl_n_152,
      \q_reg[3]\(166) => pre_tdl_n_153,
      \q_reg[3]\(165) => pre_tdl_n_154,
      \q_reg[3]\(164) => pre_tdl_n_155,
      \q_reg[3]\(163) => pre_tdl_n_156,
      \q_reg[3]\(162) => pre_tdl_n_157,
      \q_reg[3]\(161) => pre_tdl_n_158,
      \q_reg[3]\(160) => pre_tdl_n_159,
      \q_reg[3]\(159) => pre_tdl_n_160,
      \q_reg[3]\(158) => pre_tdl_n_161,
      \q_reg[3]\(157) => pre_tdl_n_162,
      \q_reg[3]\(156) => pre_tdl_n_163,
      \q_reg[3]\(155) => pre_tdl_n_164,
      \q_reg[3]\(154) => pre_tdl_n_165,
      \q_reg[3]\(153) => pre_tdl_n_166,
      \q_reg[3]\(152) => pre_tdl_n_167,
      \q_reg[3]\(151) => pre_tdl_n_168,
      \q_reg[3]\(150) => pre_tdl_n_169,
      \q_reg[3]\(149) => pre_tdl_n_170,
      \q_reg[3]\(148) => pre_tdl_n_171,
      \q_reg[3]\(147) => pre_tdl_n_172,
      \q_reg[3]\(146) => pre_tdl_n_173,
      \q_reg[3]\(145) => pre_tdl_n_174,
      \q_reg[3]\(144) => pre_tdl_n_175,
      \q_reg[3]\(143) => pre_tdl_n_176,
      \q_reg[3]\(142) => pre_tdl_n_177,
      \q_reg[3]\(141) => pre_tdl_n_178,
      \q_reg[3]\(140) => pre_tdl_n_179,
      \q_reg[3]\(139) => pre_tdl_n_180,
      \q_reg[3]\(138) => pre_tdl_n_181,
      \q_reg[3]\(137) => pre_tdl_n_182,
      \q_reg[3]\(136) => pre_tdl_n_183,
      \q_reg[3]\(135) => pre_tdl_n_184,
      \q_reg[3]\(134) => pre_tdl_n_185,
      \q_reg[3]\(133) => pre_tdl_n_186,
      \q_reg[3]\(132) => pre_tdl_n_187,
      \q_reg[3]\(131) => pre_tdl_n_188,
      \q_reg[3]\(130) => pre_tdl_n_189,
      \q_reg[3]\(129) => pre_tdl_n_190,
      \q_reg[3]\(128) => pre_tdl_n_191,
      \q_reg[3]\(127) => pre_tdl_n_192,
      \q_reg[3]\(126) => pre_tdl_n_193,
      \q_reg[3]\(125) => pre_tdl_n_194,
      \q_reg[3]\(124) => pre_tdl_n_195,
      \q_reg[3]\(123) => pre_tdl_n_196,
      \q_reg[3]\(122) => pre_tdl_n_197,
      \q_reg[3]\(121) => pre_tdl_n_198,
      \q_reg[3]\(120) => pre_tdl_n_199,
      \q_reg[3]\(119) => pre_tdl_n_200,
      \q_reg[3]\(118) => pre_tdl_n_201,
      \q_reg[3]\(117) => pre_tdl_n_202,
      \q_reg[3]\(116) => pre_tdl_n_203,
      \q_reg[3]\(115) => pre_tdl_n_204,
      \q_reg[3]\(114) => pre_tdl_n_205,
      \q_reg[3]\(113) => pre_tdl_n_206,
      \q_reg[3]\(112) => pre_tdl_n_207,
      \q_reg[3]\(111) => pre_tdl_n_208,
      \q_reg[3]\(110) => pre_tdl_n_209,
      \q_reg[3]\(109) => pre_tdl_n_210,
      \q_reg[3]\(108) => pre_tdl_n_211,
      \q_reg[3]\(107) => pre_tdl_n_212,
      \q_reg[3]\(106) => pre_tdl_n_213,
      \q_reg[3]\(105) => pre_tdl_n_214,
      \q_reg[3]\(104) => pre_tdl_n_215,
      \q_reg[3]\(103) => pre_tdl_n_216,
      \q_reg[3]\(102) => pre_tdl_n_217,
      \q_reg[3]\(101) => pre_tdl_n_218,
      \q_reg[3]\(100) => pre_tdl_n_219,
      \q_reg[3]\(99) => pre_tdl_n_220,
      \q_reg[3]\(98) => pre_tdl_n_221,
      \q_reg[3]\(97) => pre_tdl_n_222,
      \q_reg[3]\(96) => pre_tdl_n_223,
      \q_reg[3]\(95) => pre_tdl_n_224,
      \q_reg[3]\(94) => pre_tdl_n_225,
      \q_reg[3]\(93) => pre_tdl_n_226,
      \q_reg[3]\(92) => pre_tdl_n_227,
      \q_reg[3]\(91) => pre_tdl_n_228,
      \q_reg[3]\(90) => pre_tdl_n_229,
      \q_reg[3]\(89) => pre_tdl_n_230,
      \q_reg[3]\(88) => pre_tdl_n_231,
      \q_reg[3]\(87) => pre_tdl_n_232,
      \q_reg[3]\(86) => pre_tdl_n_233,
      \q_reg[3]\(85) => pre_tdl_n_234,
      \q_reg[3]\(84) => pre_tdl_n_235,
      \q_reg[3]\(83) => pre_tdl_n_236,
      \q_reg[3]\(82) => pre_tdl_n_237,
      \q_reg[3]\(81) => pre_tdl_n_238,
      \q_reg[3]\(80) => pre_tdl_n_239,
      \q_reg[3]\(79) => pre_tdl_n_240,
      \q_reg[3]\(78) => pre_tdl_n_241,
      \q_reg[3]\(77) => pre_tdl_n_242,
      \q_reg[3]\(76) => pre_tdl_n_243,
      \q_reg[3]\(75) => pre_tdl_n_244,
      \q_reg[3]\(74) => pre_tdl_n_245,
      \q_reg[3]\(73) => pre_tdl_n_246,
      \q_reg[3]\(72) => pre_tdl_n_247,
      \q_reg[3]\(71) => pre_tdl_n_248,
      \q_reg[3]\(70) => pre_tdl_n_249,
      \q_reg[3]\(69) => pre_tdl_n_250,
      \q_reg[3]\(68) => pre_tdl_n_251,
      \q_reg[3]\(67) => pre_tdl_n_252,
      \q_reg[3]\(66) => pre_tdl_n_253,
      \q_reg[3]\(65) => pre_tdl_n_254,
      \q_reg[3]\(64) => pre_tdl_n_255,
      \q_reg[3]\(63) => pre_tdl_n_256,
      \q_reg[3]\(62) => pre_tdl_n_257,
      \q_reg[3]\(61) => pre_tdl_n_258,
      \q_reg[3]\(60) => pre_tdl_n_259,
      \q_reg[3]\(59) => pre_tdl_n_260,
      \q_reg[3]\(58) => pre_tdl_n_261,
      \q_reg[3]\(57) => pre_tdl_n_262,
      \q_reg[3]\(56) => pre_tdl_n_263,
      \q_reg[3]\(55) => pre_tdl_n_264,
      \q_reg[3]\(54) => pre_tdl_n_265,
      \q_reg[3]\(53) => pre_tdl_n_266,
      \q_reg[3]\(52) => pre_tdl_n_267,
      \q_reg[3]\(51) => pre_tdl_n_268,
      \q_reg[3]\(50) => pre_tdl_n_269,
      \q_reg[3]\(49) => pre_tdl_n_270,
      \q_reg[3]\(48) => pre_tdl_n_271,
      \q_reg[3]\(47) => pre_tdl_n_272,
      \q_reg[3]\(46) => pre_tdl_n_273,
      \q_reg[3]\(45) => pre_tdl_n_274,
      \q_reg[3]\(44) => pre_tdl_n_275,
      \q_reg[3]\(43) => pre_tdl_n_276,
      \q_reg[3]\(42) => pre_tdl_n_277,
      \q_reg[3]\(41) => pre_tdl_n_278,
      \q_reg[3]\(40) => pre_tdl_n_279,
      \q_reg[3]\(39) => pre_tdl_n_280,
      \q_reg[3]\(38) => pre_tdl_n_281,
      \q_reg[3]\(37) => pre_tdl_n_282,
      \q_reg[3]\(36) => pre_tdl_n_283,
      \q_reg[3]\(35) => pre_tdl_n_284,
      \q_reg[3]\(34) => pre_tdl_n_285,
      \q_reg[3]\(33) => pre_tdl_n_286,
      \q_reg[3]\(32) => pre_tdl_n_287,
      \q_reg[3]\(31) => pre_tdl_n_288,
      \q_reg[3]\(30) => pre_tdl_n_289,
      \q_reg[3]\(29) => pre_tdl_n_290,
      \q_reg[3]\(28) => pre_tdl_n_291,
      \q_reg[3]\(27) => pre_tdl_n_292,
      \q_reg[3]\(26) => pre_tdl_n_293,
      \q_reg[3]\(25) => pre_tdl_n_294,
      \q_reg[3]\(24) => pre_tdl_n_295,
      \q_reg[3]\(23) => pre_tdl_n_296,
      \q_reg[3]\(22) => pre_tdl_n_297,
      \q_reg[3]\(21) => pre_tdl_n_298,
      \q_reg[3]\(20) => pre_tdl_n_299,
      \q_reg[3]\(19) => pre_tdl_n_300,
      \q_reg[3]\(18) => pre_tdl_n_301,
      \q_reg[3]\(17) => pre_tdl_n_302,
      \q_reg[3]\(16) => pre_tdl_n_303,
      \q_reg[3]\(15) => pre_tdl_n_304,
      \q_reg[3]\(14) => pre_tdl_n_305,
      \q_reg[3]\(13) => pre_tdl_n_306,
      \q_reg[3]\(12) => pre_tdl_n_307,
      \q_reg[3]\(11) => pre_tdl_n_308,
      \q_reg[3]\(10) => pre_tdl_n_309,
      \q_reg[3]\(9) => pre_tdl_n_310,
      \q_reg[3]\(8) => pre_tdl_n_311,
      \q_reg[3]\(7) => pre_tdl_n_312,
      \q_reg[3]\(6) => pre_tdl_n_313,
      \q_reg[3]\(5) => pre_tdl_n_314,
      \q_reg[3]\(4) => pre_tdl_n_315,
      \q_reg[3]\(3) => pre_tdl_n_316,
      \q_reg[3]\(2) => pre_tdl_n_317,
      \q_reg[3]\(1) => pre_tdl_n_318,
      \q_reg[3]\(0) => pre_tdl_n_319
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top is
  port (
    BTN : in STD_LOGIC_VECTOR ( 3 downto 0 );
    JD : out STD_LOGIC_VECTOR ( 7 downto 0 );
    LED : out STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC;
    UART_TXD : out STD_LOGIC;
    START : in STD_LOGIC;
    STOP : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of top : entity is true;
  attribute ECO_CHECKSUM : string;
  attribute ECO_CHECKSUM of top : entity is "3cb53189";
end top;

architecture STRUCTURE of top is
  signal CLK_IBUF : STD_LOGIC;
  signal CLK_IBUF_BUFG : STD_LOGIC;
  signal DATA : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal DATA_UART : STD_LOGIC;
  signal \DATA_UART[0]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_UART[1]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_UART[2]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_UART[3]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_UART[4]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_UART[5]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_UART[6]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_UART[7]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_UART[7]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_uart_state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_uart_state[2]_i_1_n_0\ : STD_LOGIC;
  signal Inst_UART_TX_CTRL_n_1 : STD_LOGIC;
  signal Inst_UART_TX_CTRL_n_2 : STD_LOGIC;
  signal JD_OBUF : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal LED_OBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal UART_TXD_OBUF : STD_LOGIC;
  signal data1 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal data2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal data3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pre_trig : STD_LOGIC;
  signal pre_trig_BUFG : STD_LOGIC;
  signal \slice[0]_i_1_n_0\ : STD_LOGIC;
  signal \slice[1]_i_1_n_0\ : STD_LOGIC;
  signal \slice[2]_i_1_n_0\ : STD_LOGIC;
  signal \slice_reg_n_0_[0]\ : STD_LOGIC;
  signal \slice_reg_n_0_[1]\ : STD_LOGIC;
  signal \slice_reg_n_0_[2]\ : STD_LOGIC;
  signal tdc_result : STD_LOGIC_VECTOR ( 24 to 24 );
  signal tdc_result0_in : STD_LOGIC_VECTOR ( 31 downto 12 );
  signal uart_send_i_1_n_0 : STD_LOGIC;
  signal uart_send_reg_n_0 : STD_LOGIC;
  signal uart_state : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute LOPT_BUFG_CLOCK : boolean;
  attribute LOPT_BUFG_CLOCK of CLK_IBUF_BUFG_inst : label is std.standard.true;
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of CLK_IBUF_BUFG_inst : label is "BUFG_OPT";
  attribute OPT_INSERTED : boolean;
  attribute OPT_INSERTED of CLK_IBUF_inst : label is std.standard.true;
  attribute OPT_MODIFIED of CLK_IBUF_inst : label is "MLO BUFG_OPT";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_uart_state[2]_i_1\ : label is "soft_lutpair113";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_uart_state_reg[0]\ : label is "prepare_send:010,send_data:011,busy:100,load_data:001,ready:000";
  attribute FSM_ENCODED_STATES of \FSM_sequential_uart_state_reg[1]\ : label is "prepare_send:010,send_data:011,busy:100,load_data:001,ready:000";
  attribute FSM_ENCODED_STATES of \FSM_sequential_uart_state_reg[2]\ : label is "prepare_send:010,send_data:011,busy:100,load_data:001,ready:000";
  attribute SOFT_HLUTNM of \slice[1]_i_1\ : label is "soft_lutpair113";
begin
CLK_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => CLK_IBUF,
      O => CLK_IBUF_BUFG
    );
CLK_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => CLK,
      O => CLK_IBUF
    );
\DATA_UART[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAA00"
    )
        port map (
      I0 => data2(0),
      I1 => data1(0),
      I2 => data3(0),
      I3 => \slice_reg_n_0_[1]\,
      I4 => \slice_reg_n_0_[0]\,
      O => \DATA_UART[0]_i_1_n_0\
    );
\DATA_UART[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAA00"
    )
        port map (
      I0 => data2(1),
      I1 => data1(1),
      I2 => data3(1),
      I3 => \slice_reg_n_0_[1]\,
      I4 => \slice_reg_n_0_[0]\,
      O => \DATA_UART[1]_i_1_n_0\
    );
\DATA_UART[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAA00"
    )
        port map (
      I0 => data2(2),
      I1 => data1(2),
      I2 => data3(2),
      I3 => \slice_reg_n_0_[1]\,
      I4 => \slice_reg_n_0_[0]\,
      O => \DATA_UART[2]_i_1_n_0\
    );
\DATA_UART[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAA00"
    )
        port map (
      I0 => data2(3),
      I1 => data1(3),
      I2 => data3(3),
      I3 => \slice_reg_n_0_[1]\,
      I4 => \slice_reg_n_0_[0]\,
      O => \DATA_UART[3]_i_1_n_0\
    );
\DATA_UART[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAA00"
    )
        port map (
      I0 => data2(4),
      I1 => data1(4),
      I2 => data3(4),
      I3 => \slice_reg_n_0_[1]\,
      I4 => \slice_reg_n_0_[0]\,
      O => \DATA_UART[4]_i_1_n_0\
    );
\DATA_UART[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAA00"
    )
        port map (
      I0 => data2(5),
      I1 => data1(5),
      I2 => data3(5),
      I3 => \slice_reg_n_0_[1]\,
      I4 => \slice_reg_n_0_[0]\,
      O => \DATA_UART[5]_i_1_n_0\
    );
\DATA_UART[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAA00"
    )
        port map (
      I0 => data2(6),
      I1 => data1(6),
      I2 => data3(6),
      I3 => \slice_reg_n_0_[1]\,
      I4 => \slice_reg_n_0_[0]\,
      O => \DATA_UART[6]_i_1_n_0\
    );
\DATA_UART[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => \slice_reg_n_0_[2]\,
      I1 => uart_state(0),
      I2 => uart_state(1),
      I3 => uart_state(2),
      O => \DATA_UART[7]_i_1_n_0\
    );
\DATA_UART[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => uart_state(2),
      I1 => uart_state(1),
      I2 => uart_state(0),
      O => DATA_UART
    );
\DATA_UART[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0CCAA00"
    )
        port map (
      I0 => data2(7),
      I1 => data1(7),
      I2 => data3(7),
      I3 => \slice_reg_n_0_[1]\,
      I4 => \slice_reg_n_0_[0]\,
      O => \DATA_UART[7]_i_3_n_0\
    );
\DATA_UART_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => DATA_UART,
      D => \DATA_UART[0]_i_1_n_0\,
      Q => DATA(0),
      R => \DATA_UART[7]_i_1_n_0\
    );
\DATA_UART_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => DATA_UART,
      D => \DATA_UART[1]_i_1_n_0\,
      Q => DATA(1),
      R => \DATA_UART[7]_i_1_n_0\
    );
\DATA_UART_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => DATA_UART,
      D => \DATA_UART[2]_i_1_n_0\,
      Q => DATA(2),
      R => \DATA_UART[7]_i_1_n_0\
    );
\DATA_UART_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => DATA_UART,
      D => \DATA_UART[3]_i_1_n_0\,
      Q => DATA(3),
      R => \DATA_UART[7]_i_1_n_0\
    );
\DATA_UART_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => DATA_UART,
      D => \DATA_UART[4]_i_1_n_0\,
      Q => DATA(4),
      R => \DATA_UART[7]_i_1_n_0\
    );
\DATA_UART_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => DATA_UART,
      D => \DATA_UART[5]_i_1_n_0\,
      Q => DATA(5),
      R => \DATA_UART[7]_i_1_n_0\
    );
\DATA_UART_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => DATA_UART,
      D => \DATA_UART[6]_i_1_n_0\,
      Q => DATA(6),
      R => \DATA_UART[7]_i_1_n_0\
    );
\DATA_UART_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => DATA_UART,
      D => \DATA_UART[7]_i_3_n_0\,
      Q => DATA(7),
      R => \DATA_UART[7]_i_1_n_0\
    );
\FSM_sequential_uart_state[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \slice_reg_n_0_[2]\,
      I1 => \slice_reg_n_0_[1]\,
      O => \FSM_sequential_uart_state[0]_i_2_n_0\
    );
\FSM_sequential_uart_state[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => uart_state(0),
      I1 => uart_state(2),
      I2 => uart_state(1),
      O => \FSM_sequential_uart_state[2]_i_1_n_0\
    );
\FSM_sequential_uart_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => Inst_UART_TX_CTRL_n_1,
      Q => uart_state(0),
      R => '0'
    );
\FSM_sequential_uart_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => Inst_UART_TX_CTRL_n_2,
      Q => uart_state(1),
      R => '0'
    );
\FSM_sequential_uart_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \FSM_sequential_uart_state[2]_i_1_n_0\,
      Q => uart_state(2),
      R => '0'
    );
Inst_UART_TX_CTRL: entity work.UART_TX_CTRL
     port map (
      CLK => CLK_IBUF_BUFG,
      D(7 downto 0) => DATA(7 downto 0),
      E(0) => uart_send_reg_n_0,
      \FSM_sequential_txState_reg[0]_0\ => Inst_UART_TX_CTRL_n_2,
      \FSM_sequential_uart_state_reg[0]\ => \slice_reg_n_0_[0]\,
      \FSM_sequential_uart_state_reg[0]_0\ => \FSM_sequential_uart_state[0]_i_2_n_0\,
      \FSM_sequential_uart_state_reg[2]\ => Inst_UART_TX_CTRL_n_1,
      JD_OBUF(0) => JD_OBUF(2),
      UART_TXD_OBUF => UART_TXD_OBUF,
      uart_state(2 downto 0) => uart_state(2 downto 0)
    );
\JD_OBUF[0]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => '0',
      O => JD(0),
      T => '1'
    );
\JD_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => JD(1)
    );
\JD_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => JD_OBUF(2),
      O => JD(2)
    );
\JD_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => JD_OBUF(3),
      O => JD(3)
    );
\JD_OBUF[4]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => '0',
      O => JD(4),
      T => '1'
    );
\JD_OBUF[5]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => '0',
      O => JD(5),
      T => '1'
    );
\JD_OBUF[6]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => '0',
      O => JD(6),
      T => '1'
    );
\JD_OBUF[7]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => '0',
      O => JD(7),
      T => '1'
    );
\LED_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => LED_OBUF(0),
      O => LED(0)
    );
\LED_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => LED_OBUF(1),
      O => LED(1)
    );
\LED_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => LED_OBUF(2),
      O => LED(2)
    );
\LED_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => LED_OBUF(3),
      O => LED(3)
    );
START_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => START,
      O => JD_OBUF(2)
    );
STOP_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => STOP,
      O => JD_OBUF(3)
    );
UART_TXD_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => UART_TXD_OBUF,
      O => UART_TXD
    );
inst_tdc: entity work.tdc
     port map (
      CLK => CLK_IBUF_BUFG,
      D(23 downto 4) => tdc_result0_in(31 downto 12),
      D(3 downto 0) => LED_OBUF(3 downto 0),
      JD_OBUF(1 downto 0) => JD_OBUF(3 downto 2),
      pre_trig => pre_trig,
      \q_reg[0]\ => pre_trig_BUFG
    );
pre_trig_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => pre_trig,
      O => pre_trig_BUFG
    );
\slice[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF0FF00000D00"
    )
        port map (
      I0 => \slice_reg_n_0_[2]\,
      I1 => \slice_reg_n_0_[1]\,
      I2 => uart_state(1),
      I3 => uart_state(2),
      I4 => uart_state(0),
      I5 => \slice_reg_n_0_[0]\,
      O => \slice[0]_i_1_n_0\
    );
\slice[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFDF0020"
    )
        port map (
      I0 => \slice_reg_n_0_[0]\,
      I1 => uart_state(1),
      I2 => uart_state(2),
      I3 => uart_state(0),
      I4 => \slice_reg_n_0_[1]\,
      O => \slice[1]_i_1_n_0\
    );
\slice[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF6FF00000800"
    )
        port map (
      I0 => \slice_reg_n_0_[1]\,
      I1 => \slice_reg_n_0_[0]\,
      I2 => uart_state(1),
      I3 => uart_state(2),
      I4 => uart_state(0),
      I5 => \slice_reg_n_0_[2]\,
      O => \slice[2]_i_1_n_0\
    );
\slice_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \slice[0]_i_1_n_0\,
      Q => \slice_reg_n_0_[0]\,
      R => '0'
    );
\slice_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \slice[1]_i_1_n_0\,
      Q => \slice_reg_n_0_[1]\,
      R => '0'
    );
\slice_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => \slice[2]_i_1_n_0\,
      Q => \slice_reg_n_0_[2]\,
      R => '0'
    );
\tdc_result[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => uart_state(1),
      I1 => uart_state(0),
      I2 => uart_state(2),
      I3 => JD_OBUF(2),
      O => tdc_result(24)
    );
\tdc_result_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => LED_OBUF(2),
      Q => data3(2),
      R => '0'
    );
\tdc_result_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => LED_OBUF(3),
      Q => data3(3),
      R => '0'
    );
\tdc_result_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(12),
      Q => data3(4),
      R => '0'
    );
\tdc_result_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(13),
      Q => data3(5),
      R => '0'
    );
\tdc_result_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(14),
      Q => data3(6),
      R => '0'
    );
\tdc_result_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(15),
      Q => data3(7),
      R => '0'
    );
\tdc_result_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(16),
      Q => data2(0),
      R => '0'
    );
\tdc_result_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(17),
      Q => data2(1),
      R => '0'
    );
\tdc_result_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(18),
      Q => data2(2),
      R => '0'
    );
\tdc_result_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(19),
      Q => data2(3),
      R => '0'
    );
\tdc_result_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(20),
      Q => data2(4),
      R => '0'
    );
\tdc_result_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(21),
      Q => data2(5),
      R => '0'
    );
\tdc_result_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(22),
      Q => data2(6),
      R => '0'
    );
\tdc_result_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(23),
      Q => data2(7),
      R => '0'
    );
\tdc_result_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(24),
      Q => data1(0),
      R => '0'
    );
\tdc_result_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(25),
      Q => data1(1),
      R => '0'
    );
\tdc_result_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(26),
      Q => data1(2),
      R => '0'
    );
\tdc_result_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(27),
      Q => data1(3),
      R => '0'
    );
\tdc_result_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(28),
      Q => data1(4),
      R => '0'
    );
\tdc_result_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(29),
      Q => data1(5),
      R => '0'
    );
\tdc_result_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(30),
      Q => data1(6),
      R => '0'
    );
\tdc_result_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => tdc_result0_in(31),
      Q => data1(7),
      R => '0'
    );
\tdc_result_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => LED_OBUF(0),
      Q => data3(0),
      R => '0'
    );
\tdc_result_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => tdc_result(24),
      D => LED_OBUF(1),
      Q => data3(1),
      R => '0'
    );
uart_send_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF08"
    )
        port map (
      I0 => uart_state(0),
      I1 => uart_state(1),
      I2 => uart_state(2),
      I3 => uart_send_reg_n_0,
      O => uart_send_i_1_n_0
    );
uart_send_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK_IBUF_BUFG,
      CE => '1',
      D => uart_send_i_1_n_0,
      Q => uart_send_reg_n_0,
      R => '0'
    );
end STRUCTURE;
