
p
Command: %s
53*	vivadotcl2?
+synth_design -top top -part xc7a35tcsg324-12default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
�The '%s' target of the following IPs are stale, please generate the output products using the generate_target or synth_ip command before running synth_design.
%s160*	vivadotcl2
	Synthesis2default:default2v
b/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci
2default:defaultZ4-393h px� 
�
�The '%s' target of the following IPs are stale, please generate the output products using the generate_target or synth_ip command before running synth_design.
%s160*	vivadotcl2"
Implementation2default:default2v
b/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci
2default:defaultZ4-393h px� 
�
IP '%s' is locked:
%s
1260*coregen2
	clk_wiz_02default:default2�
�* This IP has board specific outputs. Current project board 'unset' and the board 'digilentinc.com:arty-a7-35:part0:1.0' used to customize the IP 'clk_wiz_0' do not match. * Current project part 'xc7a35tcsg324-1' and the part 'xc7a35ticsg324-1L' used to customize the IP 'clk_wiz_0' do not match.2default:defaultZ19-2162h px�
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7a35t2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7a35t2default:defaultZ17-349h px� 
V
Loading part %s157*device2#
xc7a35tcsg324-12default:defaultZ21-403h px� 
�
%s*synth2�
�Starting RTL Elaboration : Time (s): cpu = 00:00:02 ; elapsed = 00:00:03 . Memory (MB): peak = 1662.223 ; gain = 154.688 ; free physical = 455 ; free virtual = 20835
2default:defaulth px� 
�
synthesizing module '%s'638*oasys2
top2default:default2h
R/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/new/top.vhd2default:default2
442default:default8@Z8-638h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2!
edge_detector2default:default2x
d/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/new/edge_detector.vhd2default:default2
52default:default2%
Inst_btn_debounce2default:default2!
edge_detector2default:default2h
R/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/new/top.vhd2default:default2
1812default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2!
edge_detector2default:default2z
d/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/new/edge_detector.vhd2default:default2
132default:default8@Z8-638h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2!
edge_detector2default:default2
12default:default2
12default:default2z
d/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/new/edge_detector.vhd2default:default2
132default:default8@Z8-256h px� 
c
%s
*synth2K
7	Parameter output_length bound to: 40 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter cnter_length bound to: 8 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter tdl_length bound to: 104 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter bit_cnt_length bound to: 10 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
tdc2default:default2n
Z/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/tdc.vhd2default:default2
72default:default2
inst_tdc2default:default2
tdc2default:default2h
R/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/new/top.vhd2default:default2
2142default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
tdc2default:default2p
Z/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/tdc.vhd2default:default2
282default:default8@Z8-638h px� 
c
%s
*synth2K
7	Parameter output_length bound to: 40 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter cnter_length bound to: 8 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter tdl_length bound to: 104 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter bit_cnt_length bound to: 10 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2%
n4bit_carry_chain2default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
292default:default8@Z8-638h px� 
X
%s
*synth2@
,	Parameter n bound to: 104 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter xoff bound to: 26 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter yoff bound to: 90 - type: integer 
2default:defaulth p
x
� 
�
,binding component instance '%s' to cell '%s'113*oasys2
CARRY4_inst2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
512default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
synthesizing module '%s'638*oasys2!
nbit_register2default:default2z
d/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/nbit_register.vhd2default:default2
182default:default8@Z8-638h px� 
V
%s
*synth2>
*	Parameter n bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2!
nbit_register2default:default2
22default:default2
12default:default2z
d/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/nbit_register.vhd2default:default2
182default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2%
n4bit_carry_chain2default:default2
32default:default2
12default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
292default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys25
!n4bit_carry_chain__parameterized02default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
292default:default8@Z8-638h px� 
X
%s
*synth2@
,	Parameter n bound to: 104 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter xoff bound to: 25 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter yoff bound to: 90 - type: integer 
2default:defaulth p
x
� 
�
,binding component instance '%s' to cell '%s'113*oasys2
CARRY4_inst2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
512default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
,binding component instance '%s' to cell '%s'113*oasys2
carry_x2default:default2
CARRY42default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
692default:default8@Z8-113h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys25
!n4bit_carry_chain__parameterized02default:default2
32default:default2
12default:default2~
h/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/n4bit_carry_chain.vhd2default:default2
292default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2"
binary_counter2default:default2{
e/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/binary_counter.vhd2default:default2
212default:default8@Z8-638h px� 
V
%s
*synth2>
*	Parameter n bound to: 8 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2"
binary_counter2default:default2
42default:default2
12default:default2{
e/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/binary_counter.vhd2default:default2
212default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2
	calc_unit2default:default2v
`/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/calc_unit.vhd2default:default2
282default:default8@Z8-638h px� 
a
%s
*synth2I
5	Parameter tdl_length bound to: 104 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter cnter_length bound to: 8 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter bit_cnt_length bound to: 10 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter output_length bound to: 40 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys22
binary_counter__parameterized02default:default2{
e/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/binary_counter.vhd2default:default2
212default:default8@Z8-638h px� 
V
%s
*synth2>
*	Parameter n bound to: 5 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys22
binary_counter__parameterized02default:default2
42default:default2
12default:default2{
e/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/binary_counter.vhd2default:default2
212default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys21
nbit_register__parameterized02default:default2z
d/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/nbit_register.vhd2default:default2
182default:default8@Z8-638h px� 
X
%s
*synth2@
,	Parameter n bound to: 104 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys21
nbit_register__parameterized02default:default2
42default:default2
12default:default2z
d/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/nbit_register.vhd2default:default2
182default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys21
nbit_register__parameterized12default:default2z
d/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/nbit_register.vhd2default:default2
182default:default8@Z8-638h px� 
V
%s
*synth2>
*	Parameter n bound to: 8 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys21
nbit_register__parameterized12default:default2
42default:default2
12default:default2z
d/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/nbit_register.vhd2default:default2
182default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys21
nbit_register__parameterized22default:default2z
d/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/nbit_register.vhd2default:default2
182default:default8@Z8-638h px� 
W
%s
*synth2?
+	Parameter n bound to: 40 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys21
nbit_register__parameterized22default:default2
42default:default2
12default:default2z
d/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/nbit_register.vhd2default:default2
182default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2'
hammin_weight_cnter2default:default2�
j/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/hammin_weight_cnter.vhd2default:default2
212default:default8@Z8-638h px� 
c
%s
*synth2K
7	Parameter input_length bound to: 104 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter output_length bound to: 10 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2'
hammin_weight_cnter2default:default2
52default:default2
12default:default2�
j/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/hammin_weight_cnter.vhd2default:default2
212default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
	calc_unit2default:default2
62default:default2
12default:default2v
`/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/calc_unit.vhd2default:default2
282default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
tdc2default:default2
72default:default2
12default:default2p
Z/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/tdc.vhd2default:default2
282default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
	clk_wiz_02default:default2�
�/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.runs/impl_1/synth_1/.Xil/Vivado-266625-rambo/realtime/clk_wiz_0_stub.vhdl2default:default2
52default:default2
inst_clk2default:default2
	clk_wiz_02default:default2h
R/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/new/top.vhd2default:default2
2222default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
	clk_wiz_02default:default2�
�/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.runs/impl_1/synth_1/.Xil/Vivado-266625-rambo/realtime/clk_wiz_0_stub.vhdl2default:default2
142default:default8@Z8-638h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
addf2default:default2t
`/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/new/ddf_async.vhd2default:default2
72default:default2
	inst_dff12default:default2
addf2default:default2h
R/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/new/top.vhd2default:default2
2292default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2
addf2default:default2v
`/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/new/ddf_async.vhd2default:default2
182default:default8@Z8-638h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2
CE2default:default2v
`/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/new/ddf_async.vhd2default:default2
222default:default8@Z8-614h px� 
�
�Detected dual asynchronous set and preset for register %s in module %s. This is not a recommended register style for Xilinx devices 
4257*oasys2
Q_reg2default:default2
addf2default:default2v
`/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/new/ddf_async.vhd2default:default2
262default:default8@Z8-5837h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
addf2default:default2
82default:default2
12default:default2v
`/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/new/ddf_async.vhd2default:default2
182default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2
addf2default:default2t
`/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/new/ddf_async.vhd2default:default2
72default:default2
	inst_dff22default:default2
addf2default:default2h
R/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/new/top.vhd2default:default2
2392default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys22
binary_counter__parameterized12default:default2{
e/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/binary_counter.vhd2default:default2
212default:default8@Z8-638h px� 
V
%s
*synth2>
*	Parameter n bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys22
binary_counter__parameterized12default:default2
82default:default2
12default:default2{
e/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/binary_counter.vhd2default:default2
212default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys22
binary_counter__parameterized22default:default2{
e/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/binary_counter.vhd2default:default2
212default:default8@Z8-638h px� 
V
%s
*synth2>
*	Parameter n bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys22
binary_counter__parameterized22default:default2
82default:default2
12default:default2{
e/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/tdc/binary_counter.vhd2default:default2
212default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2 
UART_TX_CTRL2default:default2w
c/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/hdl/UART_TX_CTRL.vhd2default:default2
432default:default2%
Inst_UART_TX_CTRL2default:default2 
UART_TX_CTRL2default:default2h
R/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/new/top.vhd2default:default2
3452default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2 
UART_TX_CTRL2default:default2y
c/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/hdl/UART_TX_CTRL.vhd2default:default2
512default:default8@Z8-638h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2 
UART_TX_CTRL2default:default2
92default:default2
12default:default2y
c/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/imports/hdl/UART_TX_CTRL.vhd2default:default2
512default:default8@Z8-256h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2"
start_uart_reg2default:default2h
R/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/new/top.vhd2default:default2
2002default:default8@Z8-6014h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
top2default:default2
102default:default2
12default:default2h
R/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/new/top.vhd2default:default2
442default:default8@Z8-256h px� 
x
!design %s has unconnected port %s3331*oasys2
top2default:default2
BTN[3]2default:defaultZ8-3331h px� 
x
!design %s has unconnected port %s3331*oasys2
top2default:default2
BTN[2]2default:defaultZ8-3331h px� 
�
%s*synth2�
�Finished RTL Elaboration : Time (s): cpu = 00:00:03 ; elapsed = 00:00:04 . Memory (MB): peak = 1716.973 ; gain = 209.438 ; free physical = 474 ; free virtual = 20856
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:04 ; elapsed = 00:00:05 . Memory (MB): peak = 1722.910 ; gain = 215.375 ; free physical = 502 ; free virtual = 20883
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:04 ; elapsed = 00:00:05 . Memory (MB): peak = 1722.910 ; gain = 215.375 ; free physical = 502 ; free virtual = 20883
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
f
-Analyzing %s Unisim elements for replacement
17*netlist2
522default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
v/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0/clk_wiz_0_in_context.xdc2default:default2
inst_clk	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
v/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0/clk_wiz_0_in_context.xdc2default:default2
inst_clk	2default:default8Z20-847h px� 
�
Parsing XDC File [%s]
179*designutils2�
j/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/constrs_1/imports/constraints/Arty_Master.xdc2default:default8Z20-179h px� 
�
No nets matched '%s'.
507*	planAhead2(
inst_dff1/JA_OBUF[0]2default:default2�
j/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/constrs_1/imports/constraints/Arty_Master.xdc2default:default2
2302default:default8@Z12-507h px�
�
No nets matched '%s'.
507*	planAhead2(
inst_dff1/JA_OBUF[1]2default:default2�
j/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/constrs_1/imports/constraints/Arty_Master.xdc2default:default2
2312default:default8@Z12-507h px�
�
Finished Parsing XDC File [%s]
178*designutils2�
j/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/constrs_1/imports/constraints/Arty_Master.xdc2default:default8Z20-178h px� 
�
�One or more constraints failed evaluation while reading constraint file [%s] and the design contains unresolved black boxes. These constraints will be read post-synthesis (as long as their source constraint file is marked as used_in_implementation) and should be applied correctly then. You should review the constraints listed in the file [%s] and check the run log file to verify that these constraints were correctly applied.301*project2~
j/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/constrs_1/imports/constraints/Arty_Master.xdc2default:default2)
.Xil/top_propImpl.xdc2default:defaultZ1-498h px� 
�
�Implementation specific constraints were found while reading constraint file [%s]. These constraints will be ignored for synthesis but will be used in implementation. Impacted constraints are listed in the file [%s].
233*project2~
j/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.srcs/constrs_1/imports/constraints/Arty_Master.xdc2default:default2)
.Xil/top_propImpl.xdc2default:defaultZ1-236h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
1908.4412default:default2
0.0002default:default2
3772default:default2
207772default:defaultZ17-722h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common24
 Constraint Validation Runtime : 2default:default2
00:00:00.012default:default2
00:00:00.012default:default2
1908.4412default:default2
0.0002default:default2
3772default:default2
207772default:defaultZ17-722h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Constraint Validation : Time (s): cpu = 00:00:08 ; elapsed = 00:00:10 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 458 ; free virtual = 20864
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Loading part: xc7a35tcsg324-1
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:08 ; elapsed = 00:00:10 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 458 ; free virtual = 20864
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:00:09 ; elapsed = 00:00:10 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 458 ; free virtual = 20864
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2
	state_reg2default:default2!
edge_detector2default:defaultZ8-802h px� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2
txState_reg2default:default2 
UART_TX_CTRL2default:defaultZ8-802h px� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2"
uart_state_reg2default:default2
top2default:defaultZ8-802h px� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2t
`                   State |                     New Encoding |                Previous Encoding 
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2s
_                    idle |                               00 |                               00
2default:defaulth p
x
� 
�
%s
*synth2s
_               wait_time |                               01 |                               01
2default:defaulth p
x
� 
�
%s
*synth2s
_                wait_low |                               10 |                               10
2default:defaulth p
x
� 
�
%s
*synth2s
_                  iSTATE |                               11 |                               11
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
Gencoded FSM with state register '%s' using encoding '%s' in module '%s'3353*oasys2
	state_reg2default:default2

sequential2default:default2!
edge_detector2default:defaultZ8-3354h px� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2t
`                   State |                     New Encoding |                Previous Encoding 
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2s
_                     rdy |                               00 |                               00
2default:defaulth p
x
� 
�
%s
*synth2s
_                load_bit |                               01 |                               01
2default:defaulth p
x
� 
�
%s
*synth2s
_                send_bit |                               10 |                               10
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
Gencoded FSM with state register '%s' using encoding '%s' in module '%s'3353*oasys2
txState_reg2default:default2

sequential2default:default2 
UART_TX_CTRL2default:defaultZ8-3354h px� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2t
`                   State |                     New Encoding |                Previous Encoding 
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2s
_                   ready |                              000 |                              000
2default:defaulth p
x
� 
�
%s
*synth2s
_               load_data |                              001 |                              001
2default:defaulth p
x
� 
�
%s
*synth2s
_            prepare_send |                              010 |                              010
2default:defaulth p
x
� 
�
%s
*synth2s
_               send_data |                              011 |                              011
2default:defaulth p
x
� 
�
%s
*synth2s
_                    busy |                              100 |                              100
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
Gencoded FSM with state register '%s' using encoding '%s' in module '%s'3353*oasys2"
uart_state_reg2default:default2

sequential2default:default2
top2default:defaultZ8-3354h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:09 ; elapsed = 00:00:11 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 451 ; free virtual = 20858
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	 103 Input      9 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      8 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              104 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               56 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               40 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 53    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 13    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input     56 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      2 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 207   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      1 Bit        Muxes := 5     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Hierarchical RTL Component report 
2default:defaulth p
x
� 
8
%s
*synth2 
Module top 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               56 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input     56 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      1 Bit        Muxes := 5     
2default:defaulth p
x
� 
B
%s
*synth2*
Module edge_detector 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
B
%s
*synth2*
Module nbit_register 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
C
%s
*synth2+
Module binary_counter 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module binary_counter__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module nbit_register__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              104 Bit    Registers := 1     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module nbit_register__parameterized1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 1     
2default:defaulth p
x
� 
R
%s
*synth2:
&Module nbit_register__parameterized2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               40 Bit    Registers := 1     
2default:defaulth p
x
� 
H
%s
*synth20
Module hammin_weight_cnter 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	 103 Input      9 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 102   
2default:defaulth p
x
� 
>
%s
*synth2&
Module calc_unit 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      8 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
Module addf 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module binary_counter__parameterized1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module binary_counter__parameterized2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
A
%s
*synth2)
Module UART_TX_CTRL 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2j
VPart Resources:
DSPs: 90 (col length:60)
BRAMs: 100 (col length: RAMB18 60 RAMB36 30)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
]
%s
*synth2E
1Warning: Parallel synthesis criteria is not met 
2default:defaulth p
x
� 
x
!design %s has unconnected port %s3331*oasys2
top2default:default2
BTN[3]2default:defaultZ8-3331h px� 
x
!design %s has unconnected port %s3331*oasys2
top2default:default2
BTN[2]2default:defaultZ8-3331h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2A
-Inst_btn_debounce/FSM_sequential_state_reg[1]2default:default2
top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2A
-Inst_btn_debounce/FSM_sequential_state_reg[0]2default:default2
top2default:defaultZ8-3332h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'inst_tdc/calc_unit/output_reg/q_reg[15]2default:default2
FD2default:default2;
'inst_tdc/calc_unit/output_reg/q_reg[33]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'inst_tdc/calc_unit/output_reg/q_reg[31]2default:default2
FD2default:default2;
'inst_tdc/calc_unit/output_reg/q_reg[33]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'inst_tdc/calc_unit/output_reg/q_reg[17]2default:default2
FD2default:default2;
'inst_tdc/calc_unit/output_reg/q_reg[33]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'inst_tdc/calc_unit/output_reg/q_reg[33]2default:default2
FD2default:default2;
'inst_tdc/calc_unit/output_reg/q_reg[32]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'inst_tdc/calc_unit/output_reg/q_reg[16]2default:default2
FD2default:default2;
'inst_tdc/calc_unit/output_reg/q_reg[32]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2=
)\inst_tdc/calc_unit/output_reg/q_reg[32] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2&
tdc_result_reg[23]2default:default2
FDE2default:default2&
tdc_result_reg[39]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2&
tdc_result_reg[39]2default:default2
FDE2default:default2&
tdc_result_reg[25]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2&
tdc_result_reg[25]2default:default2
FDE2default:default2&
tdc_result_reg[41]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2&
tdc_result_reg[41]2default:default2
FDE2default:default2&
tdc_result_reg[24]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2&
tdc_result_reg[24]2default:default2
FDE2default:default2&
tdc_result_reg[40]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default25
!\Inst_UART_TX_CTRL/txData_reg[9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default25
!\Inst_UART_TX_CTRL/txData_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2(
\tdc_result_reg[40] 2default:defaultZ8-3333h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:00:14 ; elapsed = 00:00:16 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 590 ; free virtual = 20759
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
2Moved timing constraint from pin '%s' to pin '%s'
4028*oasys2%
inst_clk/clk_out12default:default2.
inst_clk/bbstub_clk_out1/O2default:defaultZ8-5578h px� 
�
SMoved %s constraints on hierarchical pins to their respective driving/loading pins
4235*oasys2
12default:defaultZ8-5819h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:00:19 ; elapsed = 00:00:21 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 555 ; free virtual = 20733
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Timing Optimization : Time (s): cpu = 00:00:19 ; elapsed = 00:00:21 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 553 ; free virtual = 20731
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Technology Mapping : Time (s): cpu = 00:00:19 ; elapsed = 00:00:22 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 552 ; free virtual = 20730
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished IO Insertion : Time (s): cpu = 00:00:20 ; elapsed = 00:00:23 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 566 ; free virtual = 20745
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:00:20 ; elapsed = 00:00:23 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 566 ; free virtual = 20745
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:00:20 ; elapsed = 00:00:23 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 565 ; free virtual = 20744
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:00:20 ; elapsed = 00:00:23 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 565 ; free virtual = 20744
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:20 ; elapsed = 00:00:23 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 566 ; free virtual = 20745
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:00:20 ; elapsed = 00:00:23 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 566 ; free virtual = 20745
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
O
%s
*synth27
#+------+--------------+----------+
2default:defaulth p
x
� 
O
%s
*synth27
#|      |BlackBox name |Instances |
2default:defaulth p
x
� 
O
%s
*synth27
#+------+--------------+----------+
2default:defaulth p
x
� 
O
%s
*synth27
#|1     |clk_wiz_0     |         1|
2default:defaulth p
x
� 
O
%s
*synth27
#+------+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
N
%s*synth26
"+------+-----------------+------+
2default:defaulth px� 
N
%s*synth26
"|      |Cell             |Count |
2default:defaulth px� 
N
%s*synth26
"+------+-----------------+------+
2default:defaulth px� 
N
%s*synth26
"|1     |clk_wiz_0_bbox_0 |     1|
2default:defaulth px� 
N
%s*synth26
"|2     |BUFG             |     2|
2default:defaulth px� 
N
%s*synth26
"|3     |CARRY4           |    68|
2default:defaulth px� 
N
%s*synth26
"|4     |LUT1             |     8|
2default:defaulth px� 
N
%s*synth26
"|5     |LUT2             |    52|
2default:defaulth px� 
N
%s*synth26
"|6     |LUT3             |    33|
2default:defaulth px� 
N
%s*synth26
"|7     |LUT4             |    79|
2default:defaulth px� 
N
%s*synth26
"|8     |LUT5             |    77|
2default:defaulth px� 
N
%s*synth26
"|9     |LUT6             |   190|
2default:defaulth px� 
N
%s*synth26
"|10    |FDCE             |     1|
2default:defaulth px� 
N
%s*synth26
"|11    |FDRE             |   569|
2default:defaulth px� 
N
%s*synth26
"|12    |FDSE             |     1|
2default:defaulth px� 
N
%s*synth26
"|13    |IBUF             |     2|
2default:defaulth px� 
N
%s*synth26
"|14    |OBUF             |     5|
2default:defaulth px� 
N
%s*synth26
"+------+-----------------+------+
2default:defaulth px� 
E
%s
*synth2-

Report Instance Areas: 
2default:defaulth p
x
� 
}
%s
*synth2e
Q+------+-----------------------------+----------------------------------+------+
2default:defaulth p
x
� 
}
%s
*synth2e
Q|      |Instance                     |Module                            |Cells |
2default:defaulth p
x
� 
}
%s
*synth2e
Q+------+-----------------------------+----------------------------------+------+
2default:defaulth p
x
� 
}
%s
*synth2e
Q|1     |top                          |                                  |  1088|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|2     |  Inst_UART_TX_CTRL          |UART_TX_CTRL                      |    91|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|3     |  inst_dff1                  |addf                              |     3|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|4     |  inst_tdc                   |tdc                               |   901|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|5     |    calc_unit                |calc_unit                         |   621|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|6     |      crs_reg                |nbit_register__parameterized1     |    17|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|7     |      out_reg_trig           |binary_counter__parameterized0    |    13|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|8     |      output_reg             |nbit_register__parameterized2     |    22|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|9     |      pre_reg                |nbit_register__parameterized0     |   281|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|10    |      pre_tdl_bit_cnt        |hammin_weight_cnter               |     2|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|11    |      pst_reg                |nbit_register__parameterized0_52  |   281|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|12    |      pst_tdl_bit_cnt        |hammin_weight_cnter_53            |     2|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|13    |    crs_cnt                  |binary_counter_0                  |    20|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|14    |    pre_tdl                  |n4bit_carry_chain                 |   130|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|15    |      \register_gen[0].dff   |nbit_register_26                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|16    |      \register_gen[10].dff  |nbit_register_27                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|17    |      \register_gen[11].dff  |nbit_register_28                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|18    |      \register_gen[12].dff  |nbit_register_29                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|19    |      \register_gen[13].dff  |nbit_register_30                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|20    |      \register_gen[14].dff  |nbit_register_31                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|21    |      \register_gen[15].dff  |nbit_register_32                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|22    |      \register_gen[16].dff  |nbit_register_33                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|23    |      \register_gen[17].dff  |nbit_register_34                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|24    |      \register_gen[18].dff  |nbit_register_35                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|25    |      \register_gen[19].dff  |nbit_register_36                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|26    |      \register_gen[1].dff   |nbit_register_37                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|27    |      \register_gen[20].dff  |nbit_register_38                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|28    |      \register_gen[21].dff  |nbit_register_39                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|29    |      \register_gen[22].dff  |nbit_register_40                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|30    |      \register_gen[23].dff  |nbit_register_41                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|31    |      \register_gen[24].dff  |nbit_register_42                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|32    |      \register_gen[25].dff  |nbit_register_43                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|33    |      \register_gen[2].dff   |nbit_register_44                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|34    |      \register_gen[3].dff   |nbit_register_45                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|35    |      \register_gen[4].dff   |nbit_register_46                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|36    |      \register_gen[5].dff   |nbit_register_47                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|37    |      \register_gen[6].dff   |nbit_register_48                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|38    |      \register_gen[7].dff   |nbit_register_49                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|39    |      \register_gen[8].dff   |nbit_register_50                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|40    |      \register_gen[9].dff   |nbit_register_51                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|41    |    pst_tdl                  |n4bit_carry_chain__parameterized0 |   130|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|42    |      \register_gen[0].dff   |nbit_register                     |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|43    |      \register_gen[10].dff  |nbit_register_1                   |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|44    |      \register_gen[11].dff  |nbit_register_2                   |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|45    |      \register_gen[12].dff  |nbit_register_3                   |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|46    |      \register_gen[13].dff  |nbit_register_4                   |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|47    |      \register_gen[14].dff  |nbit_register_5                   |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|48    |      \register_gen[15].dff  |nbit_register_6                   |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|49    |      \register_gen[16].dff  |nbit_register_7                   |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|50    |      \register_gen[17].dff  |nbit_register_8                   |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|51    |      \register_gen[18].dff  |nbit_register_9                   |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|52    |      \register_gen[19].dff  |nbit_register_10                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|53    |      \register_gen[1].dff   |nbit_register_11                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|54    |      \register_gen[20].dff  |nbit_register_12                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|55    |      \register_gen[21].dff  |nbit_register_13                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|56    |      \register_gen[22].dff  |nbit_register_14                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|57    |      \register_gen[23].dff  |nbit_register_15                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|58    |      \register_gen[24].dff  |nbit_register_16                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|59    |      \register_gen[25].dff  |nbit_register_17                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|60    |      \register_gen[2].dff   |nbit_register_18                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|61    |      \register_gen[3].dff   |nbit_register_19                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|62    |      \register_gen[4].dff   |nbit_register_20                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|63    |      \register_gen[5].dff   |nbit_register_21                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|64    |      \register_gen[6].dff   |nbit_register_22                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|65    |      \register_gen[7].dff   |nbit_register_23                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|66    |      \register_gen[8].dff   |nbit_register_24                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|67    |      \register_gen[9].dff   |nbit_register_25                  |     4|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|68    |  start_counter              |binary_counter                    |    19|
2default:defaulth p
x
� 
}
%s
*synth2e
Q|69    |  stop_counter               |binary_counter__parameterized2    |    12|
2default:defaulth p
x
� 
}
%s
*synth2e
Q+------+-----------------------------+----------------------------------+------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:00:20 ; elapsed = 00:00:23 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 566 ; free virtual = 20745
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
r
%s
*synth2Z
FSynthesis finished with 0 errors, 0 critical warnings and 4 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Runtime : Time (s): cpu = 00:00:18 ; elapsed = 00:00:21 . Memory (MB): peak = 1908.441 ; gain = 215.375 ; free physical = 617 ; free virtual = 20796
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Complete : Time (s): cpu = 00:00:21 ; elapsed = 00:00:23 . Memory (MB): peak = 1908.441 ; gain = 400.906 ; free physical = 618 ; free virtual = 20797
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
f
-Analyzing %s Unisim elements for replacement
17*netlist2
682default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
g
1Inserted %s IBUFs to IO ports without IO buffers.100*opt2
12default:defaultZ31-140h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y902default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y802default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y792default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y782default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y772default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y762default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y752default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y742default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y732default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y722default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y712default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y892default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y702default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y692default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y682default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y672default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y662default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y652default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y882default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y872default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y862default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y852default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y842default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y832default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y822default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X26Y812default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y902default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y802default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y792default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y782default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y772default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y762default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y752default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y742default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y732default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y722default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y712default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y892default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y702default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y692default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y682default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y672default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y662default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y652default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y882default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y872default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y862default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y852default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y842default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y832default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y822default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
�
iCould not create '%s' constraint because the specified site '%s' could not be found in current part '%s'
825*constraints2
LOC2default:default2 
SLICE_X25Y812default:default2#
xc7a35tcsg324-12default:defaultZ18-4383h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
1908.4412default:default2
0.0002default:default2
5852default:default2
207462default:defaultZ17-722h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
1322default:default2
132default:default2
522default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2"
synth_design: 2default:default2
00:00:242default:default2
00:00:262default:default2
1908.4412default:default2
548.2272default:default2
6812default:default2
208422default:defaultZ17-722h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
1908.4412default:default2
0.0002default:default2
6812default:default2
208422default:defaultZ17-722h px� 
K
"No constraints selected for write.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2g
S/home/chavdar/Documents/Projects/Arty/tdc_uart/tdc_uart.runs/impl_1/synth_1/top.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2p
\Executing : report_utilization -file top_utilization_synth.rpt -pb top_utilization_synth.pb
2default:defaulth px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Sun Jun  6 08:36:47 20212default:defaultZ17-206h px� 


End Record